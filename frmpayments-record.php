<?php
	require_once("controller/session.php");
	require_once("controller/payments-record.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Punto de Venta</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">

		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body class="overflow-hidden" onLoad="_default();">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<p hidden id="ticketDeVenta"></p>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Registros</li>
						<li class="active">Pagos</li>
					</ul>
				</div><!-- /breadcrumb-->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading textCaption-1"><span class="text-info">Tipos de Pagos</span></div>
								<div class="panel-body">
									<div class="row">
										<div style="text-align:right" class="col-md-12 form-group">
											<label for="cmrproviders" class="label-radio inline">
												<input type="radio" id="cmrproviders" name="cmrpaymentType" value="1" onclick="setMovementsData();"/>
												<span class="custom-radio"></span>
												Proveedores
											</label>
											<label for="cmrcustomers" class="label-radio inline">
												<input type="radio" id="cmrcustomers" name="cmrpaymentType" value="2" onclick="setMovementsData();" checked="checked"/>
												<span class="custom-radio"></span>
												Clientes
											</label>
											<label for="cmrdeliverers" class="label-radio inline">
												<input type="radio" id="cmrdeliverers" name="cmrpaymentType" value="3" onclick="setMovementsData();" />
												<span class="custom-radio"></span>
												Repartidores
											</label>
										</div>
									</div>
								</div>
							</div><!-- /panel -->
						</div><!-- /.col -->
					</div><!-- /.row -->
<p hidden id="ticketDePago"></p>

						<div class="panel-heading textCaption-1">
						<span class="text-info">Deuda Total</span>
						<h1 style="color:black;" align="center" class="text-info" id="deudaTotal"></h1>
						<button type="button" id="cmdprintPayments" class="k-button" title="Imprimir Pagos" onclick="printPayments();">

						<span class="fa fa-print fa-lg"></span>&nbsp;Imprimir

						</button>
						<button type="button" id="cmdprintPayments" class="k-button" title="Imprimir Pagos" onclick="printPayments();">

						<span class="fa fa-clock-o fa-lg"></span>&nbsp;Historial

						</button>

						</div>

						<div id="vmovements" class="row">&nbsp;</div>



						<div id="vpayments" class="col-md-5">&nbsp;</div>

			</div><!-- /main-container -->

		</div><!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>

		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>

		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>

		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/js/cultures/kendo.culture.es-MX.min.js"></script>
		<!-- DatePicker -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.calendar.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.datepicker.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Grid -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.columnsorter.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.pager.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.grid.js"></script>

		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/payments-record.js"></script>

        <?php $vxajax->printJavascript(); ?>

	</body>
</html>
