<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");


require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");



date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotalSalePrice=0;
    $arrayFoliosContado = [];
    $arrayNombresContado = [];
    $arrayFoliosCredito = [];
    $arrayNombresCredito = [];
    $totalVendido = 0;
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="ventas-productos-ruta-cantidad";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsSales= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsSales->addPage();

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);

    $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    if ( (int)($_GET["vidRoute"])!=0 ){
        $vfilter.="AND c_route.id_route=" . (int)($_GET["vidRoute"]);;


        $vroutes= new clscFLRoute();
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal=clscBLRoute::total($vroutes);
        for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
            if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            }
            $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
            $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
            $vfilter.="AND c_routecustomer.factura=0 ";
            $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
            $vfilter.="AND p_productsale.fldcanceled=0 ";
            $vfilter.="AND p_productsale.id_operationType=1";
            //$vfilter.="AND p_productsale.id_operationType=1";
            //$vfilter.=" AND p_productsale.id_operationType=1";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
                $vsaleAmountTotal=0;
                $vsaleWeightInKg=0;
                $vprecioPromedio = 0;
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
                $vrptProductsSales->printMultiLine(120, 15, "Ventas de contado Ruta:" , 9, "B", "L", 0);
                $vrptProductsSales->x=170;
                $vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
                $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
                for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                    if( $vrptProductsSales->getY()>=640 ){
                        $vrptProductsSales->addPage();
                        showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                        $vpageNew=true;
                    }
                    if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                        $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                        $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                        $vrptProductsSales->x=100;
                        $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                        $vrptProductsSales->x=230;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=300;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=400;
                        $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=490;
                        $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                        $vpageNew=false;
                    }


                      //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                      $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                                      $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

                      $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                      $vrptProductsSales->x=100;
                      $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                      $vrptProductsSales->x=230;
                      $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                      $vrptProductsSales->x=300;
                      $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                      $vrptProductsSales->x=380;
                      //$vproductSaleDetail->salePrice
                      $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                      $vrptProductsSales->x=480;
                      $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                      $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                      $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                      $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                      $totalVendido += $vsaleAmountTotal;



                }
                if( $vproductsSalesDetailsTotal>0 ){
                    $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                    $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);
                    

                }

                unset($vsaleAmountTotal, $vsaleWeightInKg);
            }

            //unset($vproductsSalesDetails);
        }


        for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
            if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            }
            $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
            $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
            $vfilter.="AND c_routecustomer.factura=0 ";
            $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
            $vfilter.="AND p_productsale.fldcanceled=0 ";
            $vfilter.="AND p_productsale.id_operationType=2";
            //$vfilter.=" AND p_productsale.id_operationType=1";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
                $vsaleAmountTotal=0;
                $vsaleWeightInKg=0;
                $vprecioPromedio = 0;
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
                $vrptProductsSales->printMultiLine(120, 15, "Ventas a credito Ruta:" , 9, "B", "L", 0);
                $vrptProductsSales->x=170;
                $vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
                $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
                for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                    if( $vrptProductsSales->getY()>=640 ){
                        $vrptProductsSales->addPage();
                        showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                        $vpageNew=true;
                    }
                    if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){

                        $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                        $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                        $vrptProductsSales->x=100;
                        $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                        $vrptProductsSales->x=230;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=300;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=400;
                        $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=490;
                        $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                        $vpageNew=false;
                    }
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                    $vrptProductsSales->printMultiLine(50, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key) , 7, "", "C", 0);
                    $vrptProductsSales->x=100;
                    $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=380;
                    //$vproductSaleDetail->salePrice
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                    $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                    $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;

                      $totalVendido += $vsaleAmountTotal;


                }
                if( $vproductsSalesDetailsTotal>0 ){
                    $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                    $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


                }

                unset($vsaleAmountTotal, $vsaleWeightInKg);
            }

            //unset($vproductsSalesDetails);
        }

        for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
            if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            }
            $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
            $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
            $vfilter.="AND c_routecustomer.factura=1 ";
            $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
            $vfilter.="AND p_productsale.fldcanceled=0 ";
            $vfilter.="AND p_productsale.id_operationType=1";
            //$vfilter.=" AND p_productsale.id_operationType=1";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
                $vsaleAmountTotal=0;
                $vsaleWeightInKg=0;
                $vprecioPromedio = 0;
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
                $vrptProductsSales->printMultiLine(200, 15, "Ventas de contado Facturadas Ruta:" , 9, "B", "L", 0);
                $vrptProductsSales->x=250;
                $vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
                $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
                for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                    if( $vrptProductsSales->getY()>=640 ){
                        $vrptProductsSales->addPage();
                        showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                        $vpageNew=true;
                    }
                    if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                        $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                        $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                        $vrptProductsSales->x=100;
                        $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                        $vrptProductsSales->x=230;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=300;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=400;
                        $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=490;
                        $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                        $vpageNew=false;
                    }

                    //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

                    $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                    $vrptProductsSales->x=100;
                    $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=380;
                    //$vproductSaleDetail->salePrice
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                    $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                    $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                      $totalVendido += $vsaleAmountTotal;

                }
                if( $vproductsSalesDetailsTotal>0 ){
                    $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                    $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


                }

                unset($vsaleAmountTotal, $vsaleWeightInKg);
            }

            //unset($vproductsSalesDetails);
        }




        for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
            if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            }
            $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
            $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
            $vfilter.="AND c_routecustomer.factura=1 ";
            $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
            $vfilter.="AND p_productsale.fldcanceled=0 ";
            $vfilter.="AND p_productsale.id_operationType=2";
            //$vfilter.=" AND p_productsale.id_operationType=1";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
                $vsaleAmountTotal=0;
                $vsaleWeightInKg=0;
                $vprecioPromedio = 0;
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
                $vrptProductsSales->printMultiLine(200, 15, "Ventas a credito facturadas Ruta:" , 9, "B", "L", 0);
                $vrptProductsSales->x=250;
                $vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
                $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
                for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                    if( $vrptProductsSales->getY()>=640 ){
                        $vrptProductsSales->addPage();
                        showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                        $vpageNew=true;
                    }
                    if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                        $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                        $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                        $vrptProductsSales->x=100;
                        $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                        $vrptProductsSales->x=230;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=300;
                        $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=400;
                        $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                        $vrptProductsSales->x=490;
                        $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                        $vpageNew=false;
                    }

                    //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

                    $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                    $vrptProductsSales->x=100;
                    $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=380;
                    //$vproductSaleDetail->salePrice
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                    $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                    $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                      $totalVendido += $vsaleAmountTotal;


                }
                if( $vproductsSalesDetailsTotal>0 ){
                    $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                    $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);
			



  
                }

                unset($vsaleAmountTotal, $vsaleWeightInKg);
            }

            unset($vproductsSalesDetails);
        }


                    $vrptProductsSales->x = 40;
                    $vrptProductsSales->y += 20; 
                    $vrptProductsSales->printMultiLine(400,15,"Total Vendido -> ".$totalVendido."",8,"B","R",0);





        $vrptProductsSales->showPage();

        unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
              $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
    }else{
// Inicio venta por cantidad total de todas las rutas
      $vroutes= new clscFLRoute();
      clscBLRoute::queryToDataBase($vroutes, $vfilter);
      $vroutesTotal=clscBLRoute::total($vroutes);
      for ($vrouteNumber=0; $vrouteNumber<1; $vrouteNumber++){
          if( $vrptProductsSales->getY()>=640 ){
              $vrptProductsSales->addPage();
              showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
          }
          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
          //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
          $vfilter.="AND c_routecustomer.factura=0 ";
          $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
          $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          $vfilter.="AND p_productsale.id_operationType=1";
          //$vfilter.="AND p_productsale.id_operationType=1";
          //$vfilter.=" AND p_productsale.id_operationType=1";
          $vproductsSalesDetails= new clscFLProductSaleDetail();
          if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
              $vsaleAmountTotal=0;
              $vsaleWeightInKg=0;
              $vprecioPromedio = 0;
              $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
              $vrptProductsSales->printMultiLine(180, 15, "Ventas de contado no facturadas (total):" , 9, "B", "L", 0);
              $vrptProductsSales->x=170;
              //$vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
              $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
              for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                  if( $vrptProductsSales->getY()>=640 ){
                      $vrptProductsSales->addPage();
                      showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                      $vpageNew=true;
                  }
                  if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                      $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                      $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                      $vrptProductsSales->x=100;
                      $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                      $vrptProductsSales->x=230;
                      $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=300;
                      $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=400;
                      $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=490;
                      $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                      $vpageNew=false;
                  }


                    //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

                    $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                    $vrptProductsSales->x=100;
                    $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=380;
                    //$vproductSaleDetail->salePrice
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                    $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                    $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $totalVendido += $vsaleAmountTotal;



              }
              if( $vproductsSalesDetailsTotal>0 ){
                  $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                  $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                  $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                  $vrptProductsSales->x=480;
                  $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


              }

              unset($vsaleAmountTotal, $vsaleWeightInKg);
          }

          //unset($vproductsSalesDetails);
      }






      for ($vrouteNumber=0; $vrouteNumber<1; $vrouteNumber++){
          if( $vrptProductsSales->getY()>=640 ){
              $vrptProductsSales->addPage();
              showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
          }
          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
          //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
          $vfilter.="AND c_routecustomer.factura=0 ";
          $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
          $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          $vfilter.="AND p_productsale.id_operationType=2";
          //$vfilter.="AND p_productsale.id_operationType=1";
          //$vfilter.=" AND p_productsale.id_operationType=1";
          $vproductsSalesDetails= new clscFLProductSaleDetail();
          if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
              $vsaleAmountTotal=0;
              $vsaleWeightInKg=0;
              $vprecioPromedio = 0;
              $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
              $vrptProductsSales->printMultiLine(180, 15, "Ventas de credito no facturadas (total):" , 9, "B", "L", 0);
              $vrptProductsSales->x=170;
              //$vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
              $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
              for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                  if( $vrptProductsSales->getY()>=640 ){
                      $vrptProductsSales->addPage();
                      showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                      $vpageNew=true;
                  }
                  if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                      $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                      $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                      $vrptProductsSales->x=100;
                      $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                      $vrptProductsSales->x=230;
                      $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=300;
                      $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=400;
                      $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=490;
                      $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                      $vpageNew=false;
                  }


                    //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                                    $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

                    $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                    $vrptProductsSales->x=100;
                    $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                    $vrptProductsSales->x=380;
                    //$vproductSaleDetail->salePrice
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                    $vrptProductsSales->x=480;
                    $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                    $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                    $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                    $totalVendido += $vsaleAmountTotal;



              }
              if( $vproductsSalesDetailsTotal>0 ){
                  $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                  $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                  $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                  $vrptProductsSales->x=480;
                  $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


              }

              unset($vsaleAmountTotal, $vsaleWeightInKg);
          }

          //unset($vproductsSalesDetails);
      }




      ////////////////////////////////////
      $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";



      $vroutes= new clscFLRoute();
      clscBLRoute::queryToDataBase($vroutes, $vfilter);
      $vroutesTotal=clscBLRoute::total($vroutes);
      for ($vrouteNumber=0; $vrouteNumber<1; $vrouteNumber++){
          if( $vrptProductsSales->getY()>=640 ){
              $vrptProductsSales->addPage();
              showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
          }
          /*
          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
          $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
          $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          */


          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
          //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
          $vfilter.="AND c_routecustomer.factura=1 ";
          $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
          $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          $vfilter.="AND p_productsale.id_operationType=1";

          $vproductsSalesDetails= new clscFLProductSaleDetail();
          if ( clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1) ){
              $vsaleTotal=0;
              $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
              //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
              //$vrptProductsSales->x=80;
              //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
              $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
              for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                  if( $vrptProductsSales->getY()>=640 ){
                      $vrptProductsSales->addPage();
                      showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                      $vpageNew=true;
                  }

                  /*

                  $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                  $vrptProductsSales->printMultiLine(75, 15, "Folio de Venta:" , 9, "B", "L", 0);
                  $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                  $vrptProductsSales->x=125;
                  $vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale, 8, "", "L", 0);
                  $vrptProductsSales->x=250;
                  $vrptProductsSales->printMultiLine(70, 15, "Tipo de Venta:" , 9, "B", "L", 0);
                  $vrptProductsSales->x=320;
                  $vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->operationType->operationType, 8, "", "L", 0);
                  $vrptProductsSales->x=420;
                  $vrptProductsSales->printMultiLine(80, 15, "Total:" , 9, "B", "R", 0);
                  $vrptProductsSales->x=500;
                  $vrptProductsSales->printMultiLine(70, 15, "$ " . number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice, 2, ".", ","), 8, "", "R", 0);
                  $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                  $vrptProductsSales->printMultiLine(40, 15, "Cliente:" , 9, "B", "L", 0);

                  */

                  array_push($arrayFoliosContado,$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale);

                  if ( $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->personType->idPersonType==1 ){
                      $vflCustomer= new clspFLIndividualCustomer();
                      $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                      $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                      clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                      $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                      array_push($arrayNombresContado,$vcustomerName);
                  }
                  else{
                      $vflCustomer= new clspFLBusinessCustomer();
                      $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                      $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                      clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                      $vcustomerName=$vflCustomer->businessName;
                      array_push($arrayNombresContado,$vcustomerName);
                  }
                  /*
                  $vrptProductsSales->x=90;
                  $vrptProductsSales->printMultiLine(330, 15, $vcustomerName, 8, "", "L", 0);
                  $vrptProductsSales->x=420;
                  $vrptProductsSales->printMultiLine(80, 15, "Fecha de Venta:", 9, "B", "R", 0);
                  $vrptProductsSales->x=500;
                  $vrptProductsSales->printMultiLine(70, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->productSaleDate, 8, "", "R", 0);
                  $vsaleTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                  */



              }


              //unset($vsaleTotal);
          }

          //unset($vproductsSalesDetails);
      }



      //inicio imprimir facturacion


      for ($vrouteNumber=0; $vrouteNumber<count($arrayFoliosContado); $vrouteNumber++){
          if( $vrptProductsSales->getY()>=640 ){
              $vrptProductsSales->addPage();
              showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
          }
          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
          //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
          $vfilter.="AND c_routecustomer.factura=1 ";
          $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
          $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          $vfilter.="AND p_productsale.id_operationType=1";
          $vfilter.=" AND p_productsale.id_productSale='".$arrayFoliosContado[$vrouteNumber]."'";
          $vproductsSalesDetails= new clscFLProductSaleDetail();
          if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
              $vsaleAmountTotal=0;
              $vsaleWeightInKg=0;
              $vprecioPromedio = 0;
              $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
              $vrptProductsSales->printMultiLine(400, 15, "Ventas de contado con factura a:".$arrayNombresContado[$vrouteNumber]."" , 9, "B", "L", 0);
              $vrptProductsSales->x=250;
              //$vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
              $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
              for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                  if( $vrptProductsSales->getY()>=640 ){
                      $vrptProductsSales->addPage();
                      showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                      $vpageNew=true;
                  }
                  if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                      $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                      $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                      $vrptProductsSales->x=100;
                      $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                      $vrptProductsSales->x=230;
                      $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=300;
                      $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=400;
                      $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                      $vrptProductsSales->x=490;
                      $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                      $vpageNew=false;
                  }

                  //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                  $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                                  $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

                  $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                  $vrptProductsSales->x=100;
                  $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                  $vrptProductsSales->x=230;
                  $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                  $vrptProductsSales->x=300;
                  $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
                  $vrptProductsSales->x=380;
                  //$vproductSaleDetail->salePrice
                  $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                  $vrptProductsSales->x=480;
                  $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

                  $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                  $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                  $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                  $totalVendido += $vsaleAmountTotal;
   

              }
              if( $vproductsSalesDetailsTotal>0 ){
                  $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                  $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                  $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
                  $vrptProductsSales->x=480;
                  $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


              }

              unset($vsaleAmountTotal, $vsaleWeightInKg);
          }

          unset($vproductsSalesDetails);
      }
      //fin imprimir facturacion
      //$vrptProductsSales->showPage();

      //unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
        //    $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
      ////////////////////////////////////


////////////////////////////////////
$vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";



$vroutes= new clscFLRoute();
clscBLRoute::queryToDataBase($vroutes, $vfilter);
$vroutesTotal=clscBLRoute::total($vroutes);
for ($vrouteNumber=0; $vrouteNumber<1; $vrouteNumber++){
    if( $vrptProductsSales->getY()>=640 ){
        $vrptProductsSales->addPage();
        showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    }
    /*
    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
    $vfilter.="AND p_productsale.fldcanceled=0 ";
    */


    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
    //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
    $vfilter.="AND c_routecustomer.factura=1 ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
    $vfilter.="AND p_productsale.fldcanceled=0 ";
    $vfilter.="AND p_productsale.id_operationType=2";

    $vproductsSalesDetails= new clscFLProductSaleDetail();
    if ( clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1) ){
        $vsaleTotal=0;
        $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
        //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
        //$vrptProductsSales->x=80;
        //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
        $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
        for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
            if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                $vpageNew=true;
            }

            /*

            $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
            $vrptProductsSales->printMultiLine(75, 15, "Folio de Venta:" , 9, "B", "L", 0);
            $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
            $vrptProductsSales->x=125;
            $vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale, 8, "", "L", 0);
            $vrptProductsSales->x=250;
            $vrptProductsSales->printMultiLine(70, 15, "Tipo de Venta:" , 9, "B", "L", 0);
            $vrptProductsSales->x=320;
            $vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->operationType->operationType, 8, "", "L", 0);
            $vrptProductsSales->x=420;
            $vrptProductsSales->printMultiLine(80, 15, "Total:" , 9, "B", "R", 0);
            $vrptProductsSales->x=500;
            $vrptProductsSales->printMultiLine(70, 15, "$ " . number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice, 2, ".", ","), 8, "", "R", 0);
            $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
            $vrptProductsSales->printMultiLine(40, 15, "Cliente:" , 9, "B", "L", 0);

            */

            array_push($arrayFoliosContado,$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale);

            if ( $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                array_push($arrayNombresContado,$vcustomerName);
            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->businessName;
                array_push($arrayNombresContado,$vcustomerName);
            }
            /*
            $vrptProductsSales->x=90;
            $vrptProductsSales->printMultiLine(330, 15, $vcustomerName, 8, "", "L", 0);
            $vrptProductsSales->x=420;
            $vrptProductsSales->printMultiLine(80, 15, "Fecha de Venta:", 9, "B", "R", 0);
            $vrptProductsSales->x=500;
            $vrptProductsSales->printMultiLine(70, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->productSaleDate, 8, "", "R", 0);
            $vsaleTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
            */



        }


        //unset($vsaleTotal);
    }

    //unset($vproductsSalesDetails);
}



//inicio imprimir facturacion


for ($vrouteNumber=0; $vrouteNumber<count($arrayFoliosContado); $vrouteNumber++){
    if( $vrptProductsSales->getY()>=640 ){
        $vrptProductsSales->addPage();
        showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    }
    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND p_productsaledetail.id_product= " . trim($_GET["vidProduct"]) . " ";
    //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
    $vfilter.="AND c_routecustomer.factura=1 ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
    $vfilter.="AND p_productsale.fldcanceled=0 ";
    $vfilter.="AND p_productsale.id_operationType=2";
    $vfilter.=" AND p_productsale.id_productSale='".$arrayFoliosContado[$vrouteNumber]."'";
    $vproductsSalesDetails= new clscFLProductSaleDetail();
    if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
        $vsaleAmountTotal=0;
        $vsaleWeightInKg=0;
        $vprecioPromedio = 0;
        $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
        $vrptProductsSales->printMultiLine(400, 15, "Ventas de credito con factura a:".$arrayNombresContado[$vrouteNumber]."" , 9, "B", "L", 0);
        $vrptProductsSales->x=250;
        //$vrptProductsSales->printMultiLine(200, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
        $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
        for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
            if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                $vpageNew=true;
            }
            if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                $vrptProductsSales->x=100;
                $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                $vrptProductsSales->x=230;
                $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                $vrptProductsSales->x=300;
                $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                $vrptProductsSales->x=400;
                $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                $vrptProductsSales->x=490;
                $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                $vpageNew=false;
            }

            //$vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
            $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                            $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);

            $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
            $vrptProductsSales->x=100;
            $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
            $vrptProductsSales->x=230;
            $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
            $vrptProductsSales->x=300;
            $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 3, ".", ","), 7, "", "R", 0);
            $vrptProductsSales->x=380;
            //$vproductSaleDetail->salePrice
            $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
            $vrptProductsSales->x=480;
            $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);

            $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
            $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
            $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
            $totalVendido += $vsaleAmountTotal;



        }
        if( $vproductsSalesDetailsTotal>0 ){
            $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
            $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
            $vrptProductsSales->printMultiLine(60, 15, "", 9, "B", "R", 0);
            $vrptProductsSales->x=480;
            $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


        }

        unset($vsaleAmountTotal, $vsaleWeightInKg);
    }

    unset($vproductsSalesDetails);
}
//fin imprimir facturacion
//$vrptProductsSales->showPage();

//unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
  //    $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
////////////////////////////////////


                    $vrptProductsSales->x = 40;
                    $vrptProductsSales->y += 20;
                    $vrptProductsSales->printMultiLine(400,15,"Total Vendido -> ".$totalVendido."",8,"B","R",0);


      $vrptProductsSales->showPage();

      unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
            $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
    }

}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de ventas de productos (cantidades)";
}

function showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsSales, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsSales->x=50;	$vrptProductsSales->y=60;
        $vrptProductsSales->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Reporte de Ventas de Productos (Ruta) por Cantidades\"" , 10, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Periodo del ". trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]) . "\"" , 8, "B", "C", 0);

        $vrptProductsSales->x=50;	$vrptProductsSales->y+=12;
        $vrptProductsSales->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showFooter($vrptProductsSales, $vpageNumber)
 {
    try{
        $vrptProductsSales->x=50;  $vrptProductsSales->y=715;
        $vrptProductsSales->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsSales->y=105;
        $vrptProductsSales->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

?>
