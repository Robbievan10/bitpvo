<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspCheckbox.php");
date_default_timezone_set('America/Mexico_City');


function showDeliverersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vdeliverers= new clscFLDealer();
        clscBLDealer::queryToDataBase($vdeliverers, "WHERE c_dealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vdeliverersTotal=clscBLDealer::total($vdeliverers);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vdeliverersTotal; $vi++){
            $vJSON.=', {"text":"' . $vdeliverers->deliverers[$vi]->name . ' ' .
                                    $vdeliverers->deliverers[$vi]->firstName . ' ' .
                                    $vdeliverers->deliverers[$vi]->lastName .
                    '", "value":' . $vdeliverers->deliverers[$vi]->idDealer . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vdeliverers, $vdeliverersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los repartidores, intente de nuevo");
	}

	return $vresponse;
 }

function showCustomersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vindividualsCustomers->individualsCustomers[$vi]->name . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->firstName . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName . ' - ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->municipality->municipality . '/' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->locality .
                    '", "value":' . $vindividualsCustomers->individualsCustomers[$vi]->idCustomer . 
                    ', "key":"' . $vindividualsCustomers->individualsCustomers[$vi]->idCustomer . '"}';
        }
        
        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessCustomers->businessCustomers[$vi]->businessName . ' - ' .
                                    $vbusinessCustomers->businessCustomers[$vi]->municipality->municipality . '/' .
                                    $vbusinessCustomers->businessCustomers[$vi]->locality .
                    '", "value":' . $vbusinessCustomers->businessCustomers[$vi]->idCustomer . 
                    ', "key":"' . $vbusinessCustomers->businessCustomers[$vi]->idCustomer . '"}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vindividualsCustomers, $vfilter, $vindividualsCustomersTotal, $vJSON, $vi, $vbusinessCustomers, $vbusinessCustomersTotal);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
	}
    
	return $vresponse;
 }

function getRouteName($vidRoute)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vflRoute= new clspFLRoute();
		$vflRoute->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRoute->idRoute=$vidRoute;
        clspBLRoute::queryToDataBase($vflRoute);
        $vresponse->setReturnValue($vflRoute->route);
        
		unset($vflRoute);
 	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de obtener el nombre de la ruta, intente de nuevo");
        $vresponse->setReturnValue("");
	}
    
    unset($vidRoute);
	return $vresponse;
 }

function showAssignedRouteDealer($vidRoute)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflRouteDealer= new clspFLRouteDealer();
		$vflRouteDealer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteDealer->route->idRoute=$vidRoute;
        if( clspBLRouteDealer::queryToDataBase($vflRouteDealer)==1 ){
            $vresponse->script("vcmbDeliverersList.value(" . $vflRouteDealer->dealer->idDealer . ");");
            $vresponse->script("vcmbDeliverersList.enable(false);
                                vcmdDeallocateDealerToRoute.enable(true);");
        }
        else{
            $vresponse->script("vcmdAssignDealerToRoute.enable(true);");
        }
        
       unset($vflRouteDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar el repartidor asignado a la ruta, intente de nuevo");
	}
    
    unset($vidRoute);
	return $vresponse;
 }

function showAssignedRouteCustomersList($vidRoute)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter ="WHERE c_routecustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_routecustomer.id_route=" . $vidRoute;
        $vroutesCustomers= new clscFLRouteCustomer();
        clscBLRouteCustomer::queryToDataBase($vroutesCustomers, $vfilter);
        $vroutesCustomersTotal=clscBLRouteCustomer::total($vroutesCustomers);
        for ($vrouteCustomerNumber=0; $vrouteCustomerNumber<$vroutesCustomersTotal; $vrouteCustomerNumber++){
            if ( $vrouteCustomerNumber==0 ){
                $vdata='<table id="vgrdrouteCustomersList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vcustomerKey" style="text-align:center; font-weight:bold;">Número</th>
                                    <th data-field="vcustomerName" style="text-align:center; font-weight:bold;">Cliente</th>
                                    <th data-field="vcustomerMunicipalityName" style="text-align:center;font-weight:bold;">Municipio</th>
                                    <th data-field="vcustomerLocality" style="text-align:center;font-weight:bold;">Localidad</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            if ( $vroutesCustomers->routesCustomers[$vrouteCustomerNumber]->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vroutesCustomers->routesCustomers[$vrouteCustomerNumber]->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vroutesCustomers->routesCustomers[$vrouteCustomerNumber]->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer);
                $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$vroutesCustomers->routesCustomers[$vrouteCustomerNumber]->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vroutesCustomers->routesCustomers[$vrouteCustomerNumber]->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer);
                $vcustomerName=$vflCustomer->businessName;
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrcustomer" onClick="setCustomerId('. $vflCustomer->idCustomer . ');" title="Seleccionar Cliente" />
                            <span class="custom-radio">&nbsp;</span>
                        </td>';
            $vdata.='	<td>' . $vflCustomer->idCustomer. '</td>';
            $vdata.='	<td>' . $vcustomerName . '</td>';
            $vdata.='	<td>' . $vflCustomer->municipality->municipality . '</td>';
            $vdata.='	<td>' . $vlocality=$vflCustomer->locality . '</td>';
            $vdata.="</tr>";
            
            unset($vflCustomer, $vcustomerName);
        }
        if ( $vrouteCustomerNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vrouteCustomersList", "innerHTML", $vdata);
            $vresponse->script("setRouteCustomersList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen clientes asignados a la ruta.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vrouteCustomersList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vroutesCustomers, $vroutesCustomersTotal, $vrouteCustomerNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los clientes asignados a la ruta, intente de nuevo");
	}
	
    unset($vidRoute);
	return $vresponse;
 }

function showCustomerVisitDayAssignData($vidRoute, $vidCustomer)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflRouteCustomer= new clspFLRouteCustomer();
		$vflRouteCustomer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteCustomer->route->idRoute=$vidRoute;
        $vflRouteCustomer->customer->idCustomer=$vidCustomer;
        switch(clspBLRouteCustomer::queryToDataBase($vflRouteCustomer)){
            case 0: $vresponse->alert("Imposible mostrar los días de visita al cliente, intente de nuevo");
                    break;
            case 1: if ( $vflRouteCustomer->customer->personType->idPersonType==1 ){
                        $vflCustomer= new clspFLIndividualCustomer();
                        $vflCustomer->enterprise->idEnterprise=$vflRouteCustomer->customer->enterprise->idEnterprise;
                        $vflCustomer->idCustomer=$vflRouteCustomer->customer->idCustomer;
                        clspBLIndividualCustomer::queryToDataBase($vflCustomer);
                        $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                    }
                    else{
                        $vflCustomer= new clspFLBusinessCustomer();
                        $vflCustomer->enterprise->idEnterprise=$vflRouteCustomer->customer->enterprise->idEnterprise;
                        $vflCustomer->idCustomer=$vflRouteCustomer->customer->idCustomer;
                        clspBLBusinessCustomer::queryToDataBase($vflCustomer);
                        $vcustomerName=$vflCustomer->businessName;
                    }
                    $vresponse->assign("vcustomerName", "innerHTML", $vcustomerName);
                    
                    $vcheckbox= new clspCheckbox("chkmonday", $vresponse);
                    if ( $vflRouteCustomer->monday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    $vcheckbox= new clspCheckbox("chktuesday", $vresponse);
                    if ( $vflRouteCustomer->tuesday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    $vcheckbox= new clspCheckbox("chkwednesday", $vresponse);
                    if ( $vflRouteCustomer->wednesday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    $vcheckbox= new clspCheckbox("chkthursday", $vresponse);
                    if ( $vflRouteCustomer->thursday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    $vcheckbox= new clspCheckbox("chkfriday", $vresponse);
                    if ( $vflRouteCustomer->friday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    $vcheckbox= new clspCheckbox("chksaturday", $vresponse);
                    if ( $vflRouteCustomer->saturday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    $vcheckbox= new clspCheckbox("chksunday", $vresponse);
                    if ( $vflRouteCustomer->sunday==1 ){
                        $vcheckbox->checked();
                    }
                    else{
                        $vcheckbox->unchecked();
                    }
                    
                    unset($vflCustomer, $vcustomerName, $vcheckbox);
                    break;
        }
        
        unset($vflRouteCustomer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los días de visita al cliente, intente de nuevo");
	}
    
    unset($vidRoute, $vidCustomer);
	return $vresponse;
 }

function assignDealerToRoute($vidRoute, $vidDealer)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflRouteDealer= new clspFLRouteDealer();
		$vflRouteDealer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteDealer->route->idRoute=$vidRoute;
        $vflRouteDealer->dealer->idDealer=$vidDealer;
        switch(clspBLRouteDealer::addToDataBase($vflRouteDealer)){
            case 0:  $vresponse->alert("Imposible asignar el repartidor a la ruta, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vcmbDeliverersList.enable(false);
                                         vcmdAssignDealerToRoute.enable(false);
                                         vcmdDeallocateDealerToRoute.enable(true);");
                     $vresponse->alert("El repartidor ha sido asignado a la ruta correctamente");
                     break;
        }
        
        unset($vflRouteDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de asignar el repartidor a la ruta, intente de nuevo");
	}
    
    unset($vidRoute, $vidDealer);
	return $vresponse;
 }

function addCustomerToRoute($vidRoute, $vidCustomer)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflRouteCustomer= new clspFLRouteCustomer();
		$vflRouteCustomer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteCustomer->route->idRoute=$vidRoute;
        $vflRouteCustomer->customer->idCustomer=$vidCustomer;
        switch(clspBLRouteCustomer::addToDataBase($vflRouteCustomer)){
            case 0:  $vresponse->alert("Imposible asignar el cliente a la ruta, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showAssignedRouteCustomersList();");
                     $vresponse->alert("El cliente ha sido asignado a la ruta correctamente");
                     break;
        }
        
        unset($vflRouteCustomer);
	}
	catch (Exception $vexception){
        if ( $vexception->getCode()==1062 ){
            $vresponse->alert("Imposible asignar el cliente a la ruta, ya se encuentra asignado");    
        }
        else{
            $vresponse->alert("Ocurrió un error al tratar de asignar el cliente a la ruta, intente de nuevo");
        }
	}
    
    unset($vidRoute, $vidCustomer);
	return $vresponse;
 }
 
function assignVisitDayToCustomer($vidRoute, $vidCustomer, $vassignForm)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflRouteCustomer= new clspFLRouteCustomer();
		$vflRouteCustomer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteCustomer->route->idRoute=$vidRoute;
        $vflRouteCustomer->customer->idCustomer=$vidCustomer;
        if ( isset($vassignForm["chksunday"]) ){
            $vflRouteCustomer->sunday=1;	
        }
        if ( isset($vassignForm["chkmonday"]) ){
            $vflRouteCustomer->monday=1;	
        }
        if ( isset($vassignForm["chktuesday"]) ){
            $vflRouteCustomer->tuesday=1;	
        }
        if ( isset($vassignForm["chkwednesday"]) ){
            $vflRouteCustomer->wednesday=1;	
        }
        if ( isset($vassignForm["chkthursday"]) ){
            $vflRouteCustomer->thursday=1;	
        }
        if ( isset($vassignForm["chkfriday"]) ){
            $vflRouteCustomer->friday=1;	
        }
        if ( isset($vassignForm["chksaturday"]) ){
            $vflRouteCustomer->saturday=1;	
        }
        switch(clspBLRouteCustomer::recordVisitDayInDataBase($vflRouteCustomer)){
            case 0:  $vresponse->alert("Ningún día de visita al cliente ha sido modificado");
                     break;
            case 1:  $vresponse->alert("Los días de visita al cliente han sido registrados correctamente");
                     break;
        }
        
        unset($vflRouteCustomer);
	}
	catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de registrar los días de visita al cliente, intente de nuevo");
	}
    
    unset($vidRoute, $vidCustomer, $vassignForm);
	return $vresponse;
 }

function deallocateDealerToRoute($vidRoute, $vidDealer)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflRouteDealer= new clspFLRouteDealer();
		$vflRouteDealer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteDealer->route->idRoute=$vidRoute;
        switch(clspBLRouteDealer::deleteInDataBase($vflRouteDealer)){
            case 0:  $vresponse->alert("Imposible desasignar el repartidor de la ruta, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vcmbDeliverersList.value(0);
                                         vcmbDeliverersList.enable(true);
                                         vcmdAssignDealerToRoute.enable(true);
                                         vcmdDeallocateDealerToRoute.enable(false);");
                     $vresponse->alert("El repartidor ha sido desasignado a la ruta correctamente");
                     break;
        }
        
        unset($vflRouteDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de desasignar el repartidor de la ruta, intente de nuevo");
	}
	
    unset($vidRoute, $vidDealer);
	return $vresponse;
 }

function deleteCustomerToRoute($vidRoute, $vidCustomer)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflRouteCustomer= new clspFLRouteCustomer();
		$vflRouteCustomer->route->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRouteCustomer->route->idRoute=$vidRoute;
        $vflRouteCustomer->customer->idCustomer=$vidCustomer;
        switch(clspBLRouteCustomer::deleteInDataBase($vflRouteCustomer)){
            case 0:  $vresponse->alert("Imposible eliminar al cliente de la ruta, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showAssignedRouteCustomersList();");
                     $vresponse->alert("El cliente ha sido eliminado de la ruta correctamente");
                     break;
        }
        
        unset($vflRouteCustomer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar al cliente de la ruta, intente de nuevo");
	}
	
    unset($vidRoute, $vidCustomer);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showDeliverersList");
$vxajax->register(XAJAX_FUNCTION, "showCustomersList");
$vxajax->register(XAJAX_FUNCTION, "getRouteName");
$vxajax->register(XAJAX_FUNCTION, "showAssignedRouteDealer");
$vxajax->register(XAJAX_FUNCTION, "showAssignedRouteCustomersList");
$vxajax->register(XAJAX_FUNCTION, "showCustomerVisitDayAssignData");
$vxajax->register(XAJAX_FUNCTION, "assignDealerToRoute");
$vxajax->register(XAJAX_FUNCTION, "addCustomerToRoute");
$vxajax->register(XAJAX_FUNCTION, "assignVisitDayToCustomer");
$vxajax->register(XAJAX_FUNCTION, "deallocateDealerToRoute");
$vxajax->register(XAJAX_FUNCTION, "deleteCustomerToRoute");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>