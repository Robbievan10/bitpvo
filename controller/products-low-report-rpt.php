<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
date_default_timezone_set('America/Mexico_City');


try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $totalPrice = 0;

    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="baja-productos";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsLow= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsLow->addPage();
    
    $vreportSubtitle="Del " . trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]);
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    $vfilter ="WHERE p_productlow.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND (p_productlow.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
    if ( (int)($_GET["vproductLowType"])!=0 ){
        $vfilter.="AND p_productlow.id_productLowType=" . (int)($_GET["vproductLowType"]);
        $vflProductLowType= new clspFLProductLowType();
        $vflProductLowType->idProductLowType=(int)($_GET["vproductLowType"]);
        clspBLProductLowType::queryToDataBase($vflProductLowType, 1);
        $vreportSubtitle.=" (" . $vflProductLowType->productLowType . ")";
        
        unset($vflProductLowType);
    }
    
    showPage($vrptProductsLow, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
    $vproductsLowsDetails= new clscFLProductLowDetail();
    clscBLProductLowDetail::queryLowGroupByNameToDataBase($vproductsLowsDetails, $vfilter);
    $vproductsLowsDetailsTotal=clscBLProductLowDetail::total($vproductsLowsDetails);
    for ($vproductLowNumber=0; $vproductLowNumber<$vproductsLowsDetailsTotal; $vproductLowNumber++){
        if( $vrptProductsLow->getY()>=500 ){
            $vrptProductsLow->addPage();
            showPage($vrptProductsLow, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if( ($vproductLowNumber==0) || ($vpageNew) ){
            $vrptProductsLow->x=50;   $vrptProductsLow->y+=60;
            $vrptProductsLow->printMultiLine(250, 20, "Producto" , 8, "B", "C", 1);
            $vrptProductsLow->x=300;
            $vrptProductsLow->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 1);
            $vrptProductsLow->x=370;
            $vrptProductsLow->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 1);
            $vrptProductsLow->x=440;
            $vrptProductsLow->printMultiLine(70, 20, "Precio" , 8, "B", "C", 1);
            $vrptProductsLow->x=510;
            $vrptProductsLow->printMultiLine(70, 20, "Total" , 8, "B", "C", 1);
            $vpageNew=false;
        }
        $vrptProductsLow->x=50;   $vrptProductsLow->y+=20;
        $vrptProductsLow->printMultiLine(250, 20, strtoupper($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->product->name) , 7, "", "L", 1);
        $vrptProductsLow->x=300;
        $vrptProductsLow->printMultiLine(70, 20, number_format($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->lowAmount, 2, ".", ","), 7, "", "C", 1);
        $vrptProductsLow->x=370;
        $vrptProductsLow->printMultiLine(70, 20, number_format($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->lowWeightInKg*$vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->lowAmount, 2, ".", ","), 7, "", "C", 1);
        $vrptProductsLow->x=440;
        $vrptProductsLow->printMultiLine(70, 20, number_format(obtenerPrecio($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->product->name), 2, ".", ","), 7, "", "C", 1);
        $vrptProductsLow->x=510;
        $vrptProductsLow->printMultiLine(70, 20, number_format($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->lowAmount*obtenerPrecio($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->product->name), 2, ".", ","), 7, "", "C", 1);
        $totalPrice+= $vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->lowAmount*obtenerPrecio($vproductsLowsDetails->productsLowDetails[$vproductLowNumber]->product->name);
    }

        if( $vrptProductsLow->getY()>=500 ){
            $vrptProductsLow->addPage();
            showPage($vrptProductsLow, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vrptProductsLow->x=50;
            $vrptProductsLow->y+=20;
            $vrptProductsLow->printMultiLine(460, 20, "Precio total del inventario" , 8, "B", "C", 1);
            $vrptProductsLow->x=510;
            $vrptProductsLow->printMultiLine(70, 20, number_format($totalPrice, 2, ".", ","), 7, "", "C", 1);
            $vpageNew=true;
        }
        else{
            $vrptProductsLow->x=50;
            $vrptProductsLow->y+=20;
            $vrptProductsLow->printMultiLine(460, 20, "Valor total de las bajas de productos" , 8, "B", "C", 1);
            $vrptProductsLow->x=510;
            $vrptProductsLow->printMultiLine(70, 20, number_format($totalPrice, 2, ".", ","), 7, "B", "C", 1);
            $vpageNew=false;
        }

    $vrptProductsLow->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsLow, $vreportSubtitle,
          $vflEnterpriseUser, $vfilter, $vproductsLowsDetails, $vproductsLowsDetailsTotal, $vproductLowNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de baja de productos";
}

function showPage($vrptProductsLow, $vflEnterpriseUser, $vdate, $vreportSubtitle, $vpageNumber)
 {
    try{
        showHeader($vrptProductsLow, $vflEnterpriseUser, $vdate, $vreportSubtitle);
        showFooter($vrptProductsLow, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsLow, $vflEnterpriseUser, $vdate, $vreportSubtitle)
 {
    try{
        $vrptProductsLow->x=50;	$vrptProductsLow->y=60;
        $vrptProductsLow->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsLow->x=50;	$vrptProductsLow->y+=14;
        $vrptProductsLow->printMultiLine(520, 15, "\"Reporte de Bajas de Productos\"" , 10, "B", "C", 0);
        $vrptProductsLow->x=50;	$vrptProductsLow->y+=14;
        $vrptProductsLow->printMultiLine(520, 15, $vreportSubtitle, 10, "B", "C", 0);
        $vrptProductsLow->x=50;	$vrptProductsLow->y+=12;
        $vrptProductsLow->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsLow, $vpageNumber)
 {
    try{
        $vrptProductsLow->x=50;  $vrptProductsLow->y=715;
        $vrptProductsLow->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsLow->y=86;
        $vrptProductsLow->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }



 function obtenerPrecio($idProducto){
    $Precio = 0;
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    $vfilter ="WHERE c_product.id_enterprise='" . $vflEnterpriseUser->enterprise->idEnterprise . "'";
    $vfilter .= " AND c_product.fldname ='" . $idProducto ."'";
    $vproducts= new clscFLProduct();
    clscBLProduct::queryToDataBase($vproducts, $vfilter, 1);
    $vproductsTotal=clscBLProduct::total($vproducts);
    for ($vproductNumber=0; $vproductNumber<$vproductsTotal; $vproductNumber++){
        $Precio = $vproducts->products[$vproductNumber]->purchasePrice;
    }
    return $Precio;
 }


?>