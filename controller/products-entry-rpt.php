<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vpurchaseGTotal=0;
    $vrealGTotal=0;
    $vdifferenceGTotal=0;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/legal-landscape.pdf";
    $voutFileName="entrada-productos";
    $vorientation="L";
    $vpageSize="Legal";
    $vrptProductsEntry= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsEntry->addPage();
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    
    showPage($vrptProductsEntry, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    
    $vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND p_productentrydetail.id_productEntry='" . $_GET["vidProductEntry"] . "'";
    $vproductsEntriesDetails= new clscFLProductEntryDetail();
    clscBLProductEntryDetail::queryToDataBase($vproductsEntriesDetails, $vfilter, 1);
    $ventryProductsTotal=clscBLProductEntryDetail::total($vproductsEntriesDetails);
    for ($vproductNumber=0; $vproductNumber<$ventryProductsTotal; $vproductNumber++){
        if( $vrptProductsEntry->getY()>=460 ){
            $vrptProductsEntry->addPage();
            showPage($vrptProductsEntry, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if( ($vproductNumber==0) || ($vpageNew) ){
            $vrptProductsEntry->x=50;   $vrptProductsEntry->y+=30;
            $vrptProductsEntry->printMultiLine(90, 20, "Folio de Entrada:", 10, "B", "L", 0);
            $vrptProductsEntry->x=140;
            $vrptProductsEntry->printMultiLine(70, 20, $vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->idProductEntry, 9, "", "L", 0);
            $vrptProductsEntry->x=330;
            $vrptProductsEntry->printMultiLine(90, 20, "Folio de Compra:", 10, "B", "L", 0);
            $vrptProductsEntry->x=420;
            $vrptProductsEntry->printMultiLine(70, 20, $vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->saleFolio, 9, "", "L", 0);
            $vrptProductsEntry->x=560;
            $vrptProductsEntry->printMultiLine(90, 20, "Tipo de Entrada:", 10, "B", "L", 0);
            $vrptProductsEntry->x=650;
            $vrptProductsEntry->printMultiLine(70, 20, $vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->operationType->operationType, 9, "", "L", 0);
            $vrptProductsEntry->x=805;
            $vrptProductsEntry->printMultiLine(95, 20, "Fecha de Entrada:", 10, "B", "R", 0);
            $vrptProductsEntry->x=900;
            $vrptProductsEntry->printMultiLine(60, 20, $vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->productEntryDate, 9, "", "R", 0);
            $vrptProductsEntry->x=50;   $vrptProductsEntry->y+=20;
            $vrptProductsEntry->printMultiLine(60, 20, "Proveedor:", 10, "B", "L", 0);
            if ( $vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->provider->personType->idPersonType==1 ){
                $vflProvider= new clspFLIndividualProvider();
                $vflProvider->enterprise->idEnterprise=$vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->provider->enterprise->idEnterprise;
                $vflProvider->idProvider=$vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->provider->idProvider;
                clspBLIndividualProvider::queryToDataBase($vflProvider, 1);
                $vproviderName=$vflProvider->name . ' ' . $vflProvider->firstName . ' ' . $vflProvider->lastName;
            }
            else{
                $vflProvider= new clspFLBusinessProvider();
                $vflProvider->enterprise->idEnterprise=$vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->provider->enterprise->idEnterprise;
                $vflProvider->idProvider=$vproductsEntriesDetails->productsEntriesDetails[0]->productEntry->provider->idProvider;
                clspBLBusinessProvider::queryToDataBase($vflProvider, 1);
                $vproviderName=$vflProvider->businessName;
            }
            $vrptProductsEntry->x=110;
            $vrptProductsEntry->printMultiLine(640, 20, $vproviderName, 9, "", "L", 0);
            $vrptProductsEntry->x=50;   $vrptProductsEntry->y+=30;
            $vrptProductsEntry->printMultiLine(330, 20, "Producto" , 10, "B", "C", 1);
            $vrptProductsEntry->x=380;
            $vrptProductsEntry->printMultiLine(70, 20, "P. Unitario" , 10, "B", "C", 1);
            $vrptProductsEntry->x=450;
            $vrptProductsEntry->printMultiLine(50, 20, "Cpra. Pz" , 10, "B", "C", 1);
            $vrptProductsEntry->x=500;
            $vrptProductsEntry->printMultiLine(50, 20, "Real Pz" , 10, "B", "C", 1);
            $vrptProductsEntry->x=550;
            $vrptProductsEntry->printMultiLine(50, 20, "Dif. Pz" , 10, "B", "C", 1);
            $vrptProductsEntry->x=600;
            $vrptProductsEntry->printMultiLine(50, 20, "Cpra. Kg" , 10, "B", "C", 1);
            $vrptProductsEntry->x=650;
            $vrptProductsEntry->printMultiLine(50, 20, "Real Kg" , 10, "B", "C", 1);
            $vrptProductsEntry->x=700;
            $vrptProductsEntry->printMultiLine(50, 20, "Dif. Kg" , 10, "B", "C", 1);
            $vrptProductsEntry->x=750;
            $vrptProductsEntry->printMultiLine(70, 20, "Total Cpra." , 10, "B", "C", 1);
            $vrptProductsEntry->x=820;
            $vrptProductsEntry->printMultiLine(70, 20, "Total Real" , 10, "B", "C", 1);
            $vrptProductsEntry->x=890;
            $vrptProductsEntry->printMultiLine(70, 20, "Total Dif." , 10, "B", "C", 1);
            $vpageNew=false;
            
            unset($vflProvider, $vproviderName);
        }
        $vrptProductsEntry->x=50;   $vrptProductsEntry->y+=20;
        $vrptProductsEntry->printMultiLine(330, 20,strtoupper($vproductsEntriesDetails->productsEntriesDetails[$vproductNumber]->product->name) , 6, "", "L", 1);
        $vrptProductsEntry->x=380;
        $vpurchasePrice=$vproductsEntriesDetails->productsEntriesDetails[$vproductNumber]->purchasePrice;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vpurchasePrice, 2, ".", ",") , 7, "", "R", 1);
        $vrptProductsEntry->x=450;
        $vpurchaseAmountInPt=$vproductsEntriesDetails->productsEntriesDetails[$vproductNumber]->purchaseAmountInPt;
        $vrptProductsEntry->printMultiLine(50, 20, number_format($vpurchaseAmountInPt, 2, ".", ",") , 6, "", "R", 1);
        $vrptProductsEntry->x=500;
        $vrealAmountInPt=$vproductsEntriesDetails->productsEntriesDetails[$vproductNumber]->realAmountInPt;
        $vrptProductsEntry->printMultiLine(50, 20, number_format($vrealAmountInPt, 2, ".", ",") , 6, "", "R", 1);
        $vrptProductsEntry->x=550;
        $vrptProductsEntry->printMultiLine(50, 20, number_format($vpurchaseAmountInPt-$vrealAmountInPt, 2, ".", ",") , 6, "", "R", 1);
        $vrptProductsEntry->x=600;
        $vpurchaseAmountInKg=$vproductsEntriesDetails->productsEntriesDetails[$vproductNumber]->purchaseAmountInKg;
        $vrptProductsEntry->printMultiLine(50, 20, number_format($vpurchaseAmountInKg, 2, ".", ",") , 6, "", "R", 1);
        $vrptProductsEntry->x=650;
        $vrealAmountInKg=$vproductsEntriesDetails->productsEntriesDetails[$vproductNumber]->realAmountInKg;
        $vrptProductsEntry->printMultiLine(50, 20, number_format($vrealAmountInKg, 2, ".", ",") , 6, "", "R", 1);
        $vrptProductsEntry->x=700;
        $vrptProductsEntry->printMultiLine(50, 20, number_format($vpurchaseAmountInKg-$vrealAmountInKg, 2, ".", ",") , 6, "", "R", 1);
        $vpurchaseTotal=$vpurchaseAmountInPt*$vpurchasePrice;
        $vrptProductsEntry->x=750;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vpurchaseTotal, 2, ".", ",") , 7, "", "R", 1);
        $vrealTotal=$vrealAmountInPt*$vpurchasePrice;
        $vrptProductsEntry->x=820;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vrealTotal, 2, ".", ",") , 7, "", "R", 1);
        $vdifferenceTotal=$vpurchaseTotal-$vrealTotal;
        $vrptProductsEntry->x=890;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vdifferenceTotal, 2, ".", ",") , 7, "", "R", 1);
        $vpurchaseGTotal+=$vpurchaseTotal;
        $vrealGTotal+=$vrealTotal;
        $vdifferenceGTotal+=$vdifferenceTotal;
        
        unset($vpurchasePrice, $vpurchaseAmountInPt, $vrealAmountInPt, $vpurchaseAmountInKg, $vrealAmountInKg, $vpurchaseTotal, $vrealTotal,
              $vdifferenceTotal);
    }
    if( $ventryProductsTotal>0 ){
        $vrptProductsEntry->x=750;  $vrptProductsEntry->y+=20;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vpurchaseGTotal, 2, ".", ",") , 7, "B", "R", 1);
        $vrptProductsEntry->x=820;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vrealGTotal, 2, ".", ",") , 7, "B", "R", 1);
        $vrptProductsEntry->x=890;
        $vrptProductsEntry->printMultiLine(70, 20, "$ " . number_format($vpurchaseGTotal-$vrealGTotal, 2, ".", ",") , 7, "B", "R", 1);
    }
    
    $vrptProductsEntry->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vpurchaseGTotal, $vrealGTotal, $vdifferenceGTotal, $vsourceFile, $voutFileName, $vorientation, 
          $vpageSize, $vrptProductsEntry, $vflEnterpriseUser, $vfilter, $vproductsEntriesDetails, $ventryProductsTotal, $vproductNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de entrada de productos";
}

function showPage($vrptProductsEntry, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsEntry, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsEntry, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsEntry, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsEntry->x=50;	$vrptProductsEntry->y=50;
        $vrptProductsEntry->printMultiLine(910, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsEntry->x=50;	$vrptProductsEntry->y+=14;
        $vrptProductsEntry->printMultiLine(910, 15, "\"Reporte de Entrada de Productos\"" , 10, "B", "C", 0);
        $vrptProductsEntry->x=50;	$vrptProductsEntry->y+=12;
        $vrptProductsEntry->printMultiLine(910, 15, "Fecha de Impresi�n: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsEntry, $vpageNumber)
 {
    try{
        $vrptProductsEntry->x=50;  $vrptProductsEntry->y=535;
        $vrptProductsEntry->printMultiLine(910, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsEntry->y=86;
        $vrptProductsEntry->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>