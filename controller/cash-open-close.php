<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovementStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovementStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashMovement.php");
date_default_timezone_set('America/Mexico_City');


function showCashList($vtype)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, "");
        $vcashTotal=clscBLCash::total($vcash);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        if ( $vtype==2 ){
            $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        }
        for ($vi=0; $vi<$vcashTotal; $vi++){
            $vJSON.=', {"text":"' . $vcash->cash[$vi]->cash .
                    '", "value":' . $vcash->cash[$vi]->idCash . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vcash, $vcashTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar las cajas, intente de nuevo");
	}

	return $vresponse;
 }

function showCashiersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter ="WHERE c_enterpriseuser.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_user.id_userType=2 OR c_user.id_userType=3 OR c_user.id_userType=6 OR c_user.id_userType=7";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);
        
        $vJSON='[{"text":"--Todos--", "value":""}';
        for ($vi=0; $vi<$venterpriseUsersTotal; $vi++){
            $vJSON.=', {"text":"' . $venterpriseUsers->enterpriseUsers[$vi]->name . ' ' .
                                    $venterpriseUsers->enterpriseUsers[$vi]->firstName . ' ' .
                                    $venterpriseUsers->enterpriseUsers[$vi]->lastName .
                    '", "value":"' . $venterpriseUsers->enterpriseUsers[$vi]->idUser . '"}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vfilter, $venterpriseUsers, $venterpriseUsersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los usuarios, intente de nuevo");
	}

	return $vresponse;
 }

function showCashMovementStatusList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vcashMovementStatuses= new clscFLCashMovementStatus();
        clscBLCashMovementStatus::queryToDataBase($vcashMovementStatuses, "");
        $vcashMovementStatusesTotal=clscBLCashMovementStatus::total($vcashMovementStatuses);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vcashMovementStatusesTotal; $vi++){
            $vJSON.=', {"text":"' . $vcashMovementStatuses->cashMovementStatuses[$vi]->cashMovementStatus .
                    '", "value":' . $vcashMovementStatuses->cashMovementStatuses[$vi]->idCashMovementStatus . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vcashMovementStatuses, $vcashMovementStatusesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados de movimiento de caja, intente de nuevo");
	}

	return $vresponse;
 }

function showCashMovementList($vfilterForm)
 {
	$vresponse= new xajaxResponse();
	                $vdata='<table id="vgrdcashMovementsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vrecordDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vopenTime" style="text-align:center;font-weight:bold;">Hr. Apertura</th>
                                    <th data-field="vopenAmount" style="text-align:center;font-weight:bold;">C. Apertura</th>
                                    <th data-field="vcloseTime" style="text-align:center;font-weight:bold;">Hr. Cierre</th>
                                    <th data-field="vcloseAmount" style="text-align:center;font-weight:bold;">C. Cierre</th>
                                    <th data-field="vcashName" style="text-align:center; font-weight:bold;">Caja</th>
                                    <th data-field="vcashierName" style="text-align:center;font-weight:bold;">Cajero</th>
                                    <th data-field="vcashMovementStatusName" style="text-align:center;font-weight:bold;">Estado</th>
                                </tr>
                            </thead>
                            <tbody>';

	try{
	    $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }
        if ( (int)($vfilterForm["cmbcashList"])!=0 ){
            $vfilter.="AND p_cashmovement.id_cash=" . (int)($vfilterForm["cmbcashList"]) . " ";
        }
        if ( strcmp(trim($vfilterForm["cmbcashiersList"]), "")!=0 ){
            $vfilter.="AND p_cashmovement.id_user='" . trim($vfilterForm["cmbcashiersList"]) . "' ";
        }
        if ( (int)($vfilterForm["cmbcashMovementStatusList"])!=0  ){
            $vfilter.="AND p_cashmovement.id_cashMovementStatus=" . (int)($vfilterForm["cmbcashMovementStatusList"]);
        }
        $vcashMovements= new clscFLCashMovement();
        clscBLCashMovement::queryToDataBase($vcashMovements, $vfilter);
        $vcashMovementsTotal=clscBLCashMovement::total($vcashMovements);
        for ($vcashMovementsNumber=0; $vcashMovementsNumber<$vcashMovementsTotal; $vcashMovementsNumber++){
            if($vcashMovements->cashMovements[$vcashMovementsNumber]->cashMovementStatus->cashMovementStatus == "Abierta")
{


            if ( $vcashMovementsNumber==0 ){
                $vdata='<table id="vgrdcashMovementsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vrecordDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vopenTime" style="text-align:center;font-weight:bold;">Hr. Apertura</th>
                                    <th data-field="vopenAmount" style="text-align:center;font-weight:bold;">C. Apertura</th>
                                    <th data-field="vcloseTime" style="text-align:center;font-weight:bold;">Hr. Cierre</th>
                                    <th data-field="vcloseAmount" style="text-align:center;font-weight:bold;">C. Cierre</th>
                                    <th data-field="vcashName" style="text-align:center; font-weight:bold;">Caja</th>
                                    <th data-field="vcashierName" style="text-align:center;font-weight:bold;">Cajero</th>
                                    <th data-field="vcashMovementStatusName" style="text-align:center;font-weight:bold;">Estado</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrcashMovement" onClick="setCashMovementIds('. 
                            $vcashMovements->cashMovements[$vcashMovementsNumber]->cash->idCash . ',' .
                            $vcashMovements->cashMovements[$vcashMovementsNumber]->idCashMovement . ');" title="Seleccionar Apertura/Cierre de Caja" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vcashMovements->cashMovements[$vcashMovementsNumber]->recordDate . '</td>';
            $vdata.='	<td>' . $vcashMovements->cashMovements[$vcashMovementsNumber]->openTime . '</td>';
            $vdata.='	<td>$ ' . number_format($vcashMovements->cashMovements[$vcashMovementsNumber]->openAmount, 2, ".", ",") . '</td>';
            if ( $vcashMovements->cashMovements[$vcashMovementsNumber]->cashMovementStatus->idCashMovementStatus==2 ){
                $vdata.='	<td>' . $vcashMovements->cashMovements[$vcashMovementsNumber]->closeTime . '</td>';
                $vdata.='	<td>$ ' . number_format($vcashMovements->cashMovements[$vcashMovementsNumber]->closeAmount, 2, ".", ",") . '</td>';
            }
            else{
                $vdata.='	<td>&nbsp;</td>';
                $vdata.='	<td>&nbsp;</td>';
            }
            $vdata.='	<td>' . $vcashMovements->cashMovements[$vcashMovementsNumber]->cash->cash . '</td>';
            $vdata.='	<td>' . $vcashMovements->cashMovements[$vcashMovementsNumber]->user->name . " " .
                                $vcashMovements->cashMovements[$vcashMovementsNumber]->user->firstName . " " .
                                $vcashMovements->cashMovements[$vcashMovementsNumber]->user->lastName . '</td>';
            $vdata.='	<td>' . $vcashMovements->cashMovements[$vcashMovementsNumber]->cashMovementStatus->cashMovementStatus . '</td>';
            $vdata.="</tr>";
        }
    }
        if ( $vcashMovementsNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcashMovementsList", "innerHTML", $vdata);
            $vresponse->script("setCashMovementList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen aperturas/cierres de cajas registradas.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcashMovementsList", "innerHTML", $vdata);
            
        }
        
        unset($vfilter, $vcashMovements, $vcashMovementsTotal, $vcashMovementsNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar las aperturas/cierres de cajas, intente de nuevo"  . $vexception->getMessage());
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function showOpenCashMovementData($vidCash, $vidCashMovement)
 {
    $vresponse= new xajaxResponse();
    
	try{
	    $vflCashMovement= new clspFLCashMovement();
        $vflCashMovement->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashMovement->cash->idCash=$vidCash;
        $vflCashMovement->idCashMovement=$vidCashMovement;
        switch(clspBLCashMovement::queryToDataBase($vflCashMovement)){
            case 0: $vresponse->alert("Los datos de la apertura de caja no se encuentran registradas");
                    break;
            case 1: $vresponse->assign("vrecordDate", "innerHTML", $vflCashMovement->recordDate);
                    $vresponse->assign("vopenTime", "innerHTML", $vflCashMovement->openTime);
                    $vresponse->assign("vopenAmount", "innerHTML", number_format($vflCashMovement->openAmount, 2, ".", ","));
                    $vresponse->assign("vcashName", "innerHTML", $vflCashMovement->cash->cash);
                    $vresponse->assign("vcashierName", "innerHTML", $vflCashMovement->user->name . " " .
                                                                 $vflCashMovement->user->firstName . " " .
                                                                 $vflCashMovement->user->lastName);
                    break;
        }
        
        unset($vflCashMovement);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la apertura de caja, intente de nuevo");
	}
    
    unset($vidCash, $vidCashMovement);
	return $vresponse;
 }

function addOpenCashMovement($vopenCashForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflCashMovement= new clspFLCashMovement();
        $vflCashMovement->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashMovement->cash->idCash=$vopenCashForm["cmbcash1List"];
        $vflCashMovement->cashMovementStatus->idCashMovementStatus=1;
        $vflCashMovement->user->idUser=$vopenCashForm["cmbcashiers1List"];
        $vflCashMovement->openAmount=(float)(str_replace(",", "", trim($vopenCashForm["txtopenAmount"])));
        switch(clspBLCashMovement::addOpenToDataBase($vflCashMovement)){
            case -2: $vresponse->alert("Imposible registrar la apertura de caja, intente de nuevo");
                     break;
            case -1: $vresponse->alert("Imposible registrar la apertura de caja, el cajero tiene abierta otra caja.");
                     break;
            case 0:  $vresponse->alert("Imposible registrar la apertura de caja, la caja se encuentra abierta por otro cajero.");
                     break;
            case 1:  $vresponse->script("vidCash=" . $vflCashMovement->cash->idCash . ";
                                         vidCashMovement=" .$vflCashMovement->idCashMovement . ";
                                         showCashMovementsList();");
                     $vresponse->alert("Los datos de la apertura de caja han sido registrados correctamente");
                     break;
        }
        
        unset($vflCashMovement);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar la apertura de caja, intente de nuevo");
	}
	
    unset($vopenCashForm);
	return $vresponse;
 }
 
function recordCloseCashMovement($vidCash, $vidCashMovement, $vcloseCashForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflCashMovement= new clspFLCashMovement();
        $vflCashMovement->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashMovement->cash->idCash=$vidCash;
        $vflCashMovement->idCashMovement=$vidCashMovement;
        $vflCashMovement->closeAmount=(float)(str_replace(",", "", trim($vcloseCashForm["txtcloseAmount"])));
        switch(clspBLCashMovement::recordCloseToDataBase($vflCashMovement)){
            case 0:  $vresponse->alert("Imposible registrar el cierre de caja, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showCashMovementsList();");
                     $vresponse->alert("Los datos del cierre de caja han sido registrados correctamente");
                     break;
        }
        
        unset($vflCashMovement);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el cierre de caja, intente de nuevo");
	}
	
    unset($vidCash, $vidCashMovement, $vcloseCashForm);
	return $vresponse;
 }

function deleteCashMovementData($vidCash, $vidCashMovement)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflCashMovement= new clspFLCashMovement();
        $vflCashMovement->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashMovement->cash->idCash=$vidCash;
        $vflCashMovement->idCashMovement=$vidCashMovement;
        switch(clspBLCashMovement::deleteInDataBase($vflCashMovement)){
            case 0:  $vresponse->alert("Imposible eliminar la apertura de caja, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showCashMovementsList();");
                     $vresponse->alert("Los datos de la apertura de caja han sido eliminados correctamente");
                     break;
        }
        
        unset($vflCashMovement);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos de la apertura, intente de nuevo");
	}
	
    unset($vidCash, $vidCashMovement);
	return $vresponse;
 }

function getIdCashMovementStatus($vidCash, $vidCashMovement)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vflCashMovement= new clspFLCashMovement();
        $vflCashMovement->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashMovement->cash->idCash=$vidCash;
        $vflCashMovement->idCashMovement=$vidCashMovement;
        clspBLCashMovement::queryToDataBase($vflCashMovement);
        $vresponse->setReturnValue($vflCashMovement->cashMovementStatus->idCashMovementStatus);
        
		unset($vflCashMovement);
 	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de obtener el estatus de la caja, intente de nuevo");
        $vresponse->setReturnValue(-1);
	}
    
    unset($vproductKey);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }

$vxajax->register(XAJAX_FUNCTION, "showCashList");
$vxajax->register(XAJAX_FUNCTION, "showCashiersList");
$vxajax->register(XAJAX_FUNCTION, "showCashMovementStatusList");
$vxajax->register(XAJAX_FUNCTION, "showCashMovementList");
$vxajax->register(XAJAX_FUNCTION, "showOpenCashMovementData");
$vxajax->register(XAJAX_FUNCTION, "addOpenCashMovement");
$vxajax->register(XAJAX_FUNCTION, "recordCloseCashMovement");
$vxajax->register(XAJAX_FUNCTION, "deleteCashMovementData");
$vxajax->register(XAJAX_FUNCTION, "getIdCashMovementStatus");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>