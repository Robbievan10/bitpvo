<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLDealer.php");
date_default_timezone_set('America/Mexico_City');


function showDeliverersList($vdealerKey, $vfilterForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vfilter ="WHERE c_dealer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        
        if ( strcmp(trim($vdealerKey), "")!=0 ){
            $vfilter.="AND c_dealer.fldkey LIKE '%" . trim($vdealerKey) . "%' ";
        }
        if ( strcmp(trim($vfilterForm["txtname"]), "")!=0 ){
            $vfilter.="AND c_dealer.fldname LIKE '%" . trim($vfilterForm["txtname"]) . "%' ";
        }
        if ( strcmp(trim($vfilterForm["txtfirstName"]), "")!=0 ){
            $vfilter.="AND c_dealer.fldfirstName LIKE '%" . trim($vfilterForm["txtfirstName"]) . "%' ";
        }
        if ( strcmp(trim($vfilterForm["txtlastName"]), "")!=0 ){
            $vfilter.="AND c_dealer.fldlastName LIKE '%" . trim($vfilterForm["txtlastName"]) . "%' ";
        }
        $vdeliverers= new clscFLDealer();
        clscBLDealer::queryToDataBase($vdeliverers, $vfilter);
        $vdeliverersTotal=clscBLDealer::total($vdeliverers);
        
        for ($vdealerNumber=0; $vdealerNumber<$vdeliverersTotal; $vdealerNumber++){
            if ( $vdealerNumber==0 ){
                $vdata='<table id="vgrddeliverersList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vdealerKey" style="text-align:center; font-weight:bold;">Número</th>
                                    <th data-field="vdealerName" style="text-align:center; font-weight:bold;">Repartidor</th>
                                    <th data-field="vdealerRoueName" style="text-align:center;font-weight:bold;">Ruta</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrdealer" onClick="setDealerId('. $vdeliverers->deliverers[$vdealerNumber]->idDealer . ');" title="Seleccionar Repartidor" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vdeliverers->deliverers[$vdealerNumber]->key . '</td>';
            $vdata.='	<td>' . $vdeliverers->deliverers[$vdealerNumber]->name . ' ' .
                                $vdeliverers->deliverers[$vdealerNumber]->firstName . ' ' .
                                $vdeliverers->deliverers[$vdealerNumber]->lastName . '</td>';

            $vrouteName="";
            $vfilter ="WHERE c_routedealer.id_enterprise=" . $vdeliverers->deliverers[$vdealerNumber]->enterprise->idEnterprise . " ";
            $vfilter.="AND c_routedealer.id_dealer=" . $vdeliverers->deliverers[$vdealerNumber]->idDealer;
            $vroutesDeliverers= new clscFLRouteDealer();
            clscBLRouteDealer::queryToDataBase($vroutesDeliverers, $vfilter);
            if ( clscBLRouteDealer::total($vroutesDeliverers)>=1 ){
                $vrouteName=$vroutesDeliverers->routesDeliverers[0]->route->route;
            }
            $vdata.='	<td>' . $vrouteName . '</td>';
            $vdata.="</tr>";
            
            unset($vrouteName, $vfilter, $vroutesDeliverers);
        }
        if ( $vdealerNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vdeliverersList", "innerHTML", $vdata);
            $vresponse->script("setDeliverersList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen repartidores registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vdeliverersList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vdeliverers, $vdeliverersTotal, $vdealerNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los repartidores, intente de nuevo");
	}
	
    unset($vdealerKey, $vfilterForm);
	return $vresponse;
 }

function deleteDealerData($vidDealer)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflDealer= new clspFLDealer();
		$vflDealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflDealer->idDealer=$vidDealer;
        switch(clspBLDealer::deleteInDataBase($vflDealer)){
            case 0:  $vresponse->alert("Imposible eliminar al repartidor, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showDeliverersList();");
                     $vresponse->alert("Los datos del repartidor han sido eliminados correctamente");
                     break;
        }
        
        unset($vflDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del repartidor, intente de nuevo");
	}
	
    unset($vidDealer);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showDeliverersList");
$vxajax->register(XAJAX_FUNCTION, "deleteDealerData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>