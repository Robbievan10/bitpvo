<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductLowType.php");

date_default_timezone_set('America/Mexico_City');




function showProductLowTypesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vproductLowTypes= new clscFLProductLowType();
        clscBLProductLowType::queryToDataBase($vproductLowTypes,"");
        $vproductLowTypesTotal=clscBLProductLowType::total($vproductLowTypes);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vproductLowTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vproductLowTypes->productLowTypes[$vi]->productLowType .
                    '", "value":' . $vproductLowTypes->productLowTypes[$vi]->idProductLowType . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vtype, $vproductLowTypes, $vproductLowTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de bajas de productos, intente de nuevo");
	}
    
	return $vresponse;
 }



function printProductsLow($vfilterForm)
 {
    $vresponse= new xajaxResponse();
	   
	try{
        $vurl ="./controller/products-low-report-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
        $vurl.="&vproductLowType=" . $vfilterForm["cmbproductLowTypeList"];
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vurl);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir los pagos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProductLowTypesList");
$vxajax->register(XAJAX_FUNCTION, "printProductsLow");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>