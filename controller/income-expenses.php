<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");


function printIncomeExpenses($vfilterForm)
 {
    $vresponse= new xajaxResponse();
	   
	try{
        $vurl ="./controller/income-expenses-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vurl);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir los pagos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }

$vxajax->register(XAJAX_FUNCTION, "printIncomeExpenses");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>