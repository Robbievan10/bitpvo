<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");

require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");


require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");

date_default_timezone_set('America/Mexico_City');


function showProvidersList($vtype)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vindividualsProviders= new clscFLIndividualProvider();
        $vfilter="WHERE c_individualprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualProvider::queryToDataBase($vindividualsProviders, $vfilter);
        $vindividualsProvidersTotal=clscBLIndividualProvider::total($vindividualsProviders);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        if ( $vtype==2 ){
            $vJSON='[{"text":"--Todos--", "value":"0"}';
        }
        for ($vi=0; $vi<$vindividualsProvidersTotal; $vi++){
            $vJSON.=', {"text":"' . $vindividualsProviders->individualsProviders[$vi]->name . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->firstName . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->lastName .
                    '", "value":' . $vindividualsProviders->individualsProviders[$vi]->idProvider . '}';
        }
        
        $vbusinessProviders= new clscFLBusinessProvider();
        $vfilter="WHERE c_businessprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessProvider::queryToDataBase($vbusinessProviders, $vfilter);
        $vbusinessProvidersTotal=clscBLBusinessProvider::total($vbusinessProviders);
        for ($vi=0; $vi<$vbusinessProvidersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessProviders->businessProviders[$vi]->businessName .
                    '", "value":' . $vbusinessProviders->businessProviders[$vi]->idProvider . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vindividualsProviders, $vfilter, $vindividualsProvidersTotal, $vJSON, $vi, $vbusinessProviders, $vbusinessProvidersTotal);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los provedores, intente de nuevo");
	}

	return $vresponse;
 }

function showProductsList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vproducts= new clscFLProduct();
        clscBLProduct::queryToDataBase($vproducts, "WHERE c_product.id_enterprise=" . $_SESSION['idEnterprise']);
        $vproductsTotal=clscBLProduct::total($vproducts);
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vproductsTotal; $vi++){
            $vJSON.=', {"text":"' .strtoupper($vproducts->products[$vi]->name).
                    '", "value":' . $vproducts->products[$vi]->idProduct .
                    ', "key":"' . $vproducts->products[$vi]->key .
                    '", "barCode":"' . $vproducts->products[$vi]->barCode . '"}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vproducts, $vproductsTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los productos, intente de nuevo");
	}

	return $vresponse;
 }

function showProductsEntriesList($vfilterForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter ="WHERE p_productentry.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_productentry.fldproductEntryDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }else{
            $vfilter.="AND (p_productentry.fldproductEntryDate BETWEEN '" . date("Y-m-d")  . "' ";
            $vfilter.="AND '" . date("Y-m-d") . "') ";            
        }
        if ( strcmp(trim($vfilterForm["txtproductEntryFolio"]), "")!=0 ){
            $vfilter.="AND p_productentry.id_productEntry LIKE '%" . trim($vfilterForm["txtproductEntryFolio"]) . "%' ";
        }
        if ( (int)($vfilterForm["cmbprovidersList1"])!=0 ){
            $vfilter.="AND p_productentry.id_provider=" . (int)($vfilterForm["cmbprovidersList1"]) . " ";
        }
        $vproductsEntries= new clscFLProductEntry();
        clscBLProductEntry::queryToDataBase($vproductsEntries, $vfilter);
        $vproductsEntriesTotal=clscBLProductEntry::total($vproductsEntries);
        for ($vproductsEntryNumber=0; $vproductsEntryNumber<$vproductsEntriesTotal; $vproductsEntryNumber++){
            if ( $vproductsEntryNumber==0 ){
                $vdata='<table id="vgrdproductsEntriesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductEntryFolio" style="text-align:center; font-weight:bold;">Folio</th>
                                    <th data-field="vproductEntryDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vproductEntryProvider" style="text-align:center; font-weight:bold;">Proveedor</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproductsEntry" onClick="showProductsEntryData(\''. $vproductsEntries->productsEntries[$vproductsEntryNumber]->idProductEntry . '\');" title="Seleccionar Entrada de Productos" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vproductsEntries->productsEntries[$vproductsEntryNumber]->idProductEntry. '</td>';
            $vdata.='	<td>' . $vproductsEntries->productsEntries[$vproductsEntryNumber]->productEntryDate . '</td>';
            if ( $vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->personType->idPersonType==1 ){
                $vflProvider= new clspFLIndividualProvider();
                $vflProvider->enterprise->idEnterprise=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->enterprise->idEnterprise;
                $vflProvider->idProvider=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->idProvider;
                clspBLIndividualProvider::queryToDataBase($vflProvider);
                $vproviderName=$vflProvider->name . ' ' . $vflProvider->firstName . ' ' . $vflProvider->lastName;
            }
            else{
                $vflProvider= new clspFLBusinessProvider();
                $vflProvider->enterprise->idEnterprise=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->enterprise->idEnterprise;
                $vflProvider->idProvider=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->idProvider;
                clspBLBusinessProvider::queryToDataBase($vflProvider);
                $vproviderName=$vflProvider->businessName;
            }
            $vdata.='	<td>' . $vproviderName . '</td>';
            $vdata.="</tr>";
            
            unset($vflProvider, $vproviderName);
        }
        if ( $vproductsEntryNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsEntriesList", "innerHTML", $vdata);
            $vresponse->script("setProductsEntriesList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen entradas de productos registradas.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsEntriesList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vproductsEntries, $vproductsEntriesTotal, $vproductsEntryNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar las entradas de productos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function showProductsEntryList($vresponse)
 {
    try{
        $vtotals=0;
        $vproductsEntry=$_SESSION['vproductsEntryList'];
        $vproductsEntryTotal=count($vproductsEntry);
        for ($vi=0; $vi<$vproductsEntryTotal; $vi++){
            if ( $vi==0 ){
                $vdata='<table id="vgrdproductsEntryList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductName" style="text-align:center; font-weight:bold;">Producto</th>
                                    <th data-field="vproductAmount" style="text-align:center; font-weight:bold;">Cantidad</th>
                                    <th data-field="vproductPrice" style="text-align:center; font-weight:bold;">Precio</th>
                                    <th data-field="vtotalPrice" style="text-align:center; font-weight:bold;">Total</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vamount=$vproductsEntry[$vi][3];
            $vprice=$vproductsEntry[$vi][6];
            $vtotal=round($vamount*$vprice, 2);
            $vtotals+=$vtotal;
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproduct" onClick="setProductId('. $vproductsEntry[$vi][0] . ');" title="Seleccionar Producto" />
                            <span class="custom-radio"></span>
                            </td>';
            $vdata.='	<td>' . $vproductsEntry[$vi][1]. '</td>';
            $vdata.='	<td>' . number_format($vamount, 2, ".", ",") . '</td>';
            $vdata.='	<td>' . "$ " . number_format($vprice, 2, ".", ",") . '</td>';
            $vdata.='	<td>' . "$ " . number_format($vtotal, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        
            unset($vamount, $vprice, $vtotal);
        }
        if ( $vproductsEntryTotal>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsEntryList", "innerHTML", $vdata);
            $vresponse->assign("vproductsEntryTotalPrice", "innerHTML", "$ " . number_format($vtotals, 2, ".", ","));
            $vresponse->script("setProductsEntryList();
                                visThereListProducts=true;
                                vcmdDeleteProduct.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen productos en la entrada.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsEntryTotalPrice", "innerHTML", "$ 0.00");
            $vresponse->assign("vproductsEntryList", "innerHTML", $vdata);
            $vresponse->script("visThereListProducts=false;");
        }
    
        unset($vtotals, $vproductsEntry, $vproductsEntryTotal, $vi, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los productos de la entrada, intente de nuevo");
	}  
 }

function showProductsEntryData($vidProductEntry)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflProductEntry= new clspFLProductEntry();
        $vflProductEntry->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductEntry->idProductEntry=$vidProductEntry;
        switch(clspBLProductEntry::queryToDataBase($vflProductEntry)){
            case 0: $vresponse->alert("Los datos de la entrada de productos no se encuentran registrados");
                    break;
            case 1: $vresponse->script("vidAction=0;
                                        xajax.$('cmdnewSaveProductsEntry').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";                                
                                        enableProductsEntryDataButtons(0);
                                        cleanProductsEntryFormFields(1);
                                        disableProductsEntryFormFields();
                                        disableAddProductButtons(1);
                                        vidProductEntry='" . $vflProductEntry->idProductEntry . "';");
                    $vresponse->assign("vproductsEntryFolio", "innerHTML", $vflProductEntry->idProductEntry);
                    if ( $vflProductEntry->operationType->idOperationType==1 ){
                        $vresponse->script('xajax.$("cmrcash").checked=true;');
                    }
                    else{
                        $vresponse->script('xajax.$("cmrcredit").checked=true;');
                    }
                    $vtext= new clspText("txtpurchaseFolio", $vresponse);
                    $vtext->setValue($vflProductEntry->saleFolio);
                    $vtext= new clspText("dtpckrproductsEntryDate", $vresponse);
                    $vtext->setValue($vflProductEntry->productEntryDate);                    
                    $vresponse->script("vcmbProvidersList.value(" . $vflProductEntry->provider->idProvider . ");");
                    
                    $vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
                    $vfilter.="AND p_productentrydetail.id_productEntry='" . $vflProductEntry->idProductEntry . "'";
                    $vproductsEntriesDetails= new clscFLProductEntryDetail();
                    clscBLProductEntryDetail::queryToDataBase($vproductsEntriesDetails, $vfilter);
                    $ventryProductsTotal=clscBLProductEntryDetail::total($vproductsEntriesDetails);
                    $vproductsEntry=array();
                    for($vi=0; $vi<$ventryProductsTotal; $vi++){
                        $vproduct=array();
                        array_push($vproduct, $vproductsEntriesDetails->productsEntriesDetails[$vi]->product->idProduct);
                        array_push($vproduct, strtoupper($vproductsEntriesDetails->productsEntriesDetails[$vi]->product->name));
                        array_push($vproduct, $vproductsEntriesDetails->productsEntriesDetails[$vi]->purchaseAmountInPt);
                        array_push($vproduct, $vproductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInPt);
                        array_push($vproduct, $vproductsEntriesDetails->productsEntriesDetails[$vi]->purchaseAmountInKg);
                        array_push($vproduct, $vproductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInKg);
                        array_push($vproduct, $vproductsEntriesDetails->productsEntriesDetails[$vi]->purchasePrice);
                        array_push($vproductsEntry, $vproduct);
                        
                        unset($vproduct);    
                    }
                    $_SESSION['vproductsEntryList']=$vproductsEntry;
                    showProductsEntryList($vresponse);
                    
                    unset($vtext, $vfilter, $vproductsEntriesDetails, $ventryProductsTotal, $vproductsEntry, $vi);
                    break;
        }
	   
       unset($vflProductEntry);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la entrada de productos, intente de nuevo");
	}
    
    unset($vidProductEntry);
	return $vresponse;
 }

function showProductAmountData($vidProduct)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vtext= new clspText("txtproductPurchaseAmountInPt", $vresponse);
        $vtext->setValue("");
        $vtext= new clspText("txtproductRealAmountInPt", $vresponse);
        $vtext->setValue("");
        $vresponse->assign("vdifferenceAmountInPt", "innerHTML", "0");
        $vtext= new clspText("txtproductPurchaseAmountInKg", $vresponse);
        $vtext->setValue("");
        $vtext= new clspText("txtproductRealAmountInKg", $vresponse);
        $vtext->setValue("");
        $vresponse->assign("vdifferenceAmountInKg", "innerHTML", "0");
        $vtext= new clspText("txtproductPurchasePrice", $vresponse);
        $vtext->setValue("");
        $vtext= new clspText("txtproductWeightInKg", $vresponse);
        $vtext->setValue("");
        $vresponse->assign("vtotalPrice", "innerHTML", "$ 0.00");
                    
        $vflProduct= new clspFLProduct();
        $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->idProduct=$vidProduct;
        switch(clspBLProduct::queryToDataBase($vflProduct)){
            case 0: $vresponse->alert("El producto no se encuentra registrado");
                    break;
            case 1: $vtext= new clspText("txtproductPurchasePrice", $vresponse);
                    $vtext->setValue(number_format($vflProduct->purchasePrice, 2, ".", ","));
                    $vtext= new clspText("txtproductWeightInKg", $vresponse);
                    $vtext->setValue(number_format($vflProduct->weightInKg, 2, ".", ","));
                    break;
        }
	   
       unset($vflProduct, $vtext);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos (Precio/Peso) del producto, intente de nuevo");
	}
    
    unset($vidProduct);
	return $vresponse;
 }

function printProductsEntry($vidProductEntry)
 {
    $vresponse= new xajaxResponse();
	   
	try{
        $vurl="./controller/products-entry-rpt.php?vidProductEntry=" . $vidProductEntry ;
		$vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir la entrada de productos, intente de nuevo");
	}
	
    unset($vidProductEntry);
	return $vresponse;
 }

function addProductsEntryData($vproductsEntryForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflProductEntry= new clspFLProductEntry();
        $vflProductEntry->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductEntry->provider->idProvider=(int)($vproductsEntryForm["cmbprovidersList"]);
        $vflProductEntry->operationType->idOperationType=(int)($vproductsEntryForm["cmrproductsEntryType"]);
        if ( $vflProductEntry->operationType->idOperationType==1 ){
            $vflProductEntry->operationStatus->idOperationStatus=2;
        }
        $vflProductEntry->saleFolio=trim($vproductsEntryForm["txtpurchaseFolio"]);
        $vflProductEntry->productEntryDate=trim($vproductsEntryForm["dtpckrproductsEntryDate"]);
        
        $vproductsEntry=$_SESSION['vproductsEntryList'];
        $vproductsEntryTotal=count($vproductsEntry);
        $vproductsEntriesDetails= new clscFLProductEntryDetail();
        for($vi=0; $vi<$vproductsEntryTotal; $vi++){
            $vflProductEntryDetail= new clspFLProductEntryDetail();
            $vflProductEntryDetail->product->idProduct=(int)($vproductsEntry[$vi][0]);
            $vflProductEntryDetail->purchaseAmountInPt=(float)($vproductsEntry[$vi][2]);
            $vflProductEntryDetail->purchaseAmountInKg=(float)($vproductsEntry[$vi][4]);
            $vflProductEntryDetail->realAmountInPt=(float)($vproductsEntry[$vi][3]);
            $vflProductEntryDetail->realAmountInKg=(float)($vproductsEntry[$vi][5]);
            $vflProductEntryDetail->purchasePrice=(float)($vproductsEntry[$vi][6]);
            clscBLProductEntryDetail::add($vproductsEntriesDetails, $vflProductEntryDetail);
            unset($vflProductEntryDetail);
        }
        if ( clspBLProductEntry::addToDataBase($vflProductEntry, $vproductsEntriesDetails)==1 ){
            $vresponse->script("vidAction=0;
                                vidProductEntry='" . $vflProductEntry->idProductEntry . "';
                                xajax.$('cmdnewSaveProductsEntry').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";
                                enableProductsEntryDataButtons(0);
                                disableProductsEntryFormFields();
                                disableAddProductButtons(1);");
            $vresponse->assign("vproductsEntryFolio", "innerHTML", $vflProductEntry->idProductEntry);
            $vresponse->alert("La entrada de productos ha sido registrada correctamente"); 
        }
        else{
            $vresponse->alert("Imposible registrar la entrada de productos, intente de nuevo");
        }
        
        unset($vflProductEntry, $vproductsEntry, $vproductsEntryTotal, $vproductsEntriesDetails, $vi);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar la entrada de productos, intente de nuevo");
	}
	
    unset($vproductsEntryForm);
	return $vresponse;
 }

function addProductToProductsEntryList($vproductsAddForm)
 {
	$vresponse= new xajaxResponse();
    	   
	try{
	    $vproductsEntry=$_SESSION['vproductsEntryList'];
        $vproductsEntryTotal=count($vproductsEntry);
        $vproductFound=false;
        for ($vi=0; $vi<$vproductsEntryTotal; $vi++){
            if ( ((int)$vproductsAddForm["cmbproductsList"])==$vproductsEntry[$vi][0] ){
               $vproductFound=true;
               $vi=$vproductsEntryTotal;
            }
        }
        if ( $vproductFound==false ){
            $vflProduct= new clspFLProduct();
            $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
            $vflProduct->idProduct=(int)($vproductsAddForm["cmbproductsList"]);
            clspBLProduct::queryToDataBase($vflProduct);
            $vproduct= array();
            array_push($vproduct, (int)($vproductsAddForm["cmbproductsList"]));
            array_push($vproduct, strtoupper($vflProduct->name));
            array_push($vproduct, (float)(str_replace(",", "", trim($vproductsAddForm["txtproductPurchaseAmountInPt"]))));
            array_push($vproduct, (float)(str_replace(",", "", trim($vproductsAddForm["txtproductRealAmountInPt"]))));
            array_push($vproduct, (float)(str_replace(",", "", trim($vproductsAddForm["txtproductPurchaseAmountInKg"]))));
            array_push($vproduct, (float)(str_replace(",", "", trim($vproductsAddForm["txtproductRealAmountInKg"]))));
            array_push($vproduct, (float)(str_replace(",", "", trim($vproductsAddForm["txtproductPurchasePrice"]))));
            
            array_push($vproductsEntry, $vproduct);
            $_SESSION['vproductsEntryList']=$vproductsEntry;
            showProductsEntryList($vresponse);
            //$vresponse->alert("El producto <". $vflProduct->name . "> fué agregado a lista de entrada de productos");
            
            unset($vflProduct, $vproduct);    
        }
        else{
            $vresponse->alert("El producto ya se encuentra agregado a la lista de entrada de productos.");
        }
        
        unset($vproductsEntry, $vproductsEntryTotal, $vproductFound, $vi);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de agregar el producto a la entrada, intente de nuevo");
	}
	
    unset($vproductsAddForm);
	return $vresponse;
 }

function updateProductsEntryData($vidProductEntry, $vproductsEntryForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProductEntry= new clspFLProductEntry();
        $vflProductEntry->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductEntry->idProductEntry=$vidProductEntry;
        $vflProductEntry->provider->idProvider=(int)($vproductsEntryForm["cmbprovidersList"]);
        $vflProductEntry->operationType->idOperationType=(int)($vproductsEntryForm["cmrproductsEntryType"]);
        if ( $vflProductEntry->operationType->idOperationType==1 ){
            $vflProductEntry->operationStatus->idOperationStatus=2;
        }
        $vflProductEntry->saleFolio=trim($vproductsEntryForm["txtpurchaseFolio"]);
        $vflProductEntry->productEntryDate=trim($vproductsEntryForm["dtpckrproductsEntryDate"]);
        
        $vproductsEntry=$_SESSION['vproductsEntryList'];
        $vproductsEntryTotal=count($vproductsEntry);
        $vproductsEntriesDetails= new clscFLProductEntryDetail();
        for($vi=0; $vi<$vproductsEntryTotal; $vi++){
            $vflProductEntryDetail= new clspFLProductEntryDetail();
            $vflProductEntryDetail->product->idProduct=(int)($vproductsEntry[$vi][0]);
            $vflProductEntryDetail->purchaseAmountInPt=(float)($vproductsEntry[$vi][2]);
            $vflProductEntryDetail->purchaseAmountInKg=(float)($vproductsEntry[$vi][4]);
            $vflProductEntryDetail->realAmountInPt=(float)($vproductsEntry[$vi][3]);
            $vflProductEntryDetail->realAmountInKg=(float)($vproductsEntry[$vi][5]);
            $vflProductEntryDetail->purchasePrice=(float)($vproductsEntry[$vi][6]);
            clscBLProductEntryDetail::add($vproductsEntriesDetails, $vflProductEntryDetail);
            unset($vflProductEntryDetail);
        }
        switch(clspBLProductEntry::updateInDataBase($vflProductEntry, $vproductsEntriesDetails)){
            case -2: $vresponse->alert("Imposible modificar los datos de la entrada de productos, tiene pagos registrados.\nPara modificar, debe de eliminar primero esos pagos en el módulo correspondiente");
                     break;
            case -1: $vresponse->alert("Imposible modificar los datos de la entrada de productos, ya se encuentra pagada y no puede ser editada");
                     break;
            case 1:  $vresponse->alert("Los datos de la entrada de productos han sido modificados correctamente");
                     $vresponse->script("vidAction=0;
                                         xajax.$('cmdnewSaveProductsEntry').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";                                
                                         enableProductsEntryDataButtons(0);
                                         disableProductsEntryFormFields();
                                         disableAddProductButtons(1);");
                     break;
            default: $vresponse->alert("Imposible modificar los datos de la entrada de productos, intente de nuevo");
                     break;
        }
        
        unset($vflProductEntry, $vproductsEntry, $vproductsEntryTotal, $vproductsEntriesDetails, $vi);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos de la entrada de productos, intente de nuevo");
	}
	
    unset($vidProductEntry, $vproductsEntryForm);
	return $vresponse;
 }
 
function deleteProductsEntryData($vidProductEntry)
 {
	$vresponse= new xajaxResponse();


        $userType = 0;
try {

        $vmenu='<ul class="nav-notification clearfix">';
    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}



	
	try{
        $vflProductEntry= new clspFLProductEntry();
		$vflProductEntry->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductEntry->idProductEntry=$vidProductEntry;

        if ($userType == 0){
        if ( clspBLProductEntry::deleteInDataBase($vflProductEntry) == 1){
            $vresponse->script("vidProductEntry='';
                                cancelNewSaveProductsEntry();");
            $vresponse->alert("Los datos de la entrada de productos ha sido eliminados correctamente"); 
        }
        else{
            $vresponse->alert("Imposible eliminar la entrada de productos, intente de nuevo");
        }
        }else{
            $vresponse->alert("No tienes permiso para eliminar esta entrada");
        }

        
        unset($vflProductEntry);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos de la entrada de productos, intente de nuevo");
	}
	
    unset($vidProductEntry);
	return $vresponse;
 }

function deleteProductOfProductsEntryList($vidProduct)
 {
    $vresponse= new xajaxResponse();
    	   
	try{
        $vproductsEntry=$_SESSION['vproductsEntryList'];
	    $vproductsEntryTotal=count($vproductsEntry);
        $vproductDeleted=false;
        for ($vi=0; $vi<$vproductsEntryTotal; $vi++){
            if ( $vidProduct==$vproductsEntry[$vi][0] ){
                array_splice($vproductsEntry, $vi, 1);
                $vproductDeleted=true;
                $vi=$vproductsEntryTotal;
            }
        }
        if ( $vproductDeleted ){
            $_SESSION['vproductsEntryList']=$vproductsEntry;
            showProductsEntryList($vresponse);
            $vresponse->alert("El producto ha sido eliminado correctamente de la entrada");    
        }
        unset($vproductsEntry, $vproductsEntryTotal, $vproductDeleted, $vi);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar al producto de la entrada, intente de nuevo");
	}
	
    unset($vidProduct);
	return $vresponse;
 }

function cleanProductsEntryList()
 {
	$vresponse= new xajaxResponse();
    
    $_SESSION['vproductsEntryList']=array();
    $vresponse->assign("vproductsEntryList", "innerHTML", "");
    $vresponse->script("visThereListProducts=false;");
        
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProvidersList");
$vxajax->register(XAJAX_FUNCTION, "showProductsList");
$vxajax->register(XAJAX_FUNCTION, "showProductsEntriesList");
$vxajax->register(XAJAX_FUNCTION, "showProductsEntryData");
$vxajax->register(XAJAX_FUNCTION, "showProductAmountData");
$vxajax->register(XAJAX_FUNCTION, "printProductsEntry");
$vxajax->register(XAJAX_FUNCTION, "addProductsEntryData");
$vxajax->register(XAJAX_FUNCTION, "addProductToProductsEntryList");
$vxajax->register(XAJAX_FUNCTION, "updateProductsEntryData");
$vxajax->register(XAJAX_FUNCTION, "deleteProductsEntryData");
$vxajax->register(XAJAX_FUNCTION, "deleteProductOfProductsEntryList");
$vxajax->register(XAJAX_FUNCTION, "cleanProductsEntryList");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>