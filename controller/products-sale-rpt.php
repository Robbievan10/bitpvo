<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
date_default_timezone_set('America/Mexico_City');


try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="existencia-productos";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsSale= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsSale->addPage();
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    
    showPage($vrptProductsSale, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    
    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND p_productsaledetail.id_productSale='" . $_GET["vidProductSale"]. "'";
    $vproductsSalesDetails= new clscFLProductSaleDetail();
    clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter, 1);
    $vsaleProductsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
    for ($vproductNumber=0; $vproductNumber<$vsaleProductsTotal; $vproductNumber++){
        if( $vrptProductsSale->getY()>=690 ){
            $vrptProductsSale->addPage();
            showPage($vrptProductsSale, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if( ($vproductNumber==0) || ($vpageNew) ){
            $vrptProductsSale->x=50;   $vrptProductsSale->y+=30;
            $vrptProductsSale->printMultiLine(65, 20, "Folio de Venta:", 8, "B", "L", 0);
            $vrptProductsSale->x=115;
            $vrptProductsSale->printMultiLine(60, 20, $vproductsSalesDetails->productsSalesDetails[0]->productSale->idProductSale, 8, "", "L", 0);
            $vrptProductsSale->x=250;
            $vrptProductsSale->printMultiLine(63, 20, "Tipo de Venta:", 8, "B", "L", 0);
            $vrptProductsSale->x=313;
            $vrptProductsSale->printMultiLine(60, 20, $vproductsSalesDetails->productsSalesDetails[0]->productSale->operationType->operationType, 8, "", "L", 0);
            $vrptProductsSale->x=448;
            $vrptProductsSale->printMultiLine(70, 20, "Fecha de Venta:", 8, "B", "R", 0);
            $vrptProductsSale->x=518;
            $vrptProductsSale->printMultiLine(52, 20, $vproductsSalesDetails->productsSalesDetails[0]->productSale->productSaleDate, 8, "", "R", 0);
            $vrptProductsSale->x=50;   $vrptProductsSale->y+=20;
            $vrptProductsSale->printMultiLine(38, 20, "Cliente:", 8, "B", "L", 0);
            if ( $vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->businessName;
            }
            $vrptProductsSale->x=88;
            $vrptProductsSale->printMultiLine(482, 20, $vcustomerName, 8, "", "L", 0);
            $vrptProductsSale->x=50;   $vrptProductsSale->y+=30;
            $vrptProductsSale->printMultiLine(340, 20, "Producto" , 8, "B", "C", 1);
            $vrptProductsSale->x=390;
            $vrptProductsSale->printMultiLine(50, 20, "Cantidad" , 8, "B", "C", 1);
            $vrptProductsSale->x=440;
            $vrptProductsSale->printMultiLine(60, 20, "Precio" , 8, "B", "C", 1);
            $vrptProductsSale->x=500;
            $vrptProductsSale->printMultiLine(70, 20, "Total" , 8, "B", "C", 1);
            $vpageNew=false;
            
            unset($vflCustomer, $vcustomerName);
        }
        $vrptProductsSale->x=50;   $vrptProductsSale->y+=20;
        $vrptProductsSale->printMultiLine(340, 20, $vproductsSalesDetails->productsSalesDetails[$vproductNumber]->product->name , 6, "", "L", 1);
        $vrptProductsSale->x=390;
        $vrptProductsSale->printMultiLine(50, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductNumber]->saleAmount, 2, ".", ",") , 7, "", "R", 1);
        $vrptProductsSale->x=440;
        $vrptProductsSale->printMultiLine(60, 20, "$ " . number_format($vproductsSalesDetails->productsSalesDetails[$vproductNumber]->salePrice, 2, ".", ",") , 7, "", "R", 1);
        $vrptProductsSale->x=500;
        $vrptProductsSale->printMultiLine(70, 20, "$ " . number_format(clspBLProductSaleDetail::totalSalePrice($vproductsSalesDetails->productsSalesDetails[$vproductNumber]), 2, ".", ",") , 7, "", "R", 1);
    }
    if( $vsaleProductsTotal>0 ){
        $vrptProductsSale->x=500;  $vrptProductsSale->y+=20;
        $vrptProductsSale->printMultiLine(70, 20, "$ " . number_format(clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails), 2, ".", ",") , 7, "B", "R", 1);
    }
    $vrptProductsSale->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSale, $vflEnterpriseUser, $vfilter, 
          $vproductsSalesDetails, $vsaleProductsTotal, $vproductNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de venta de productos";
}

function showPage($vrptProductsSale, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsSale, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsSale, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsStock, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsStock->x=50;	$vrptProductsStock->y=60;
        $vrptProductsStock->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsStock->x=50;	$vrptProductsStock->y+=14;
        $vrptProductsStock->printMultiLine(520, 15, "\"Reporte de Venta de Productos\"", 10, "B", "C", 0);
        $vrptProductsStock->x=50;	$vrptProductsStock->y+=12;
        $vrptProductsStock->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsStock, $vpageNumber)
 {
    try{
        $vrptProductsStock->x=50;  $vrptProductsStock->y=715;
        $vrptProductsStock->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsStock->y=86;
        $vrptProductsStock->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>