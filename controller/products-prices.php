<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");
$vxajax->configure( 'defaultMode', 'synchronous' );

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");

date_default_timezone_set('America/Mexico_City');

function showProductTypesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter="WHERE c_producttype.id_enterprise=" . $_SESSION['idEnterprise'];
        $vproductTypes= new clscFLProductType();
        clscBLProductType::queryToDataBase($vproductTypes, $vfilter);
        $vproductTypesTotal=clscBLProductType::total($vproductTypes);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vproductTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vproductTypes->productTypes[$vi]->productType .
                    '", "value":' . $vproductTypes->productTypes[$vi]->idProductType . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vfilter, $vproductTypes, $vproductTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de producto, intente de nuevo");
	}

	return $vresponse;
 }

function showProductsList($vfilterForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter ="WHERE c_product.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["txtkey"]), "")!=0 ){
            $vfilter.="AND c_product.fldkey LIKE '%" . trim($vfilterForm["txtkey"]) . "%' ";
        }
        if ( (int)($vfilterForm["cmbproductTypesList"])!=0 ){
            $vfilter.="AND c_product.id_productType=" . (int)($vfilterForm["cmbproductTypesList"]) . " ";
        }
        if ( strcmp(trim($vfilterForm["txtname"]), "")!=0 ){
            $vfilter.="AND c_product.fldname LIKE '%" . trim($vfilterForm["txtname"]) . "%' ";
        }
        $vproducts= new clscFLProduct();
        clscBLProduct::queryToDataBase($vproducts, $vfilter);
        $vproductsTotal=clscBLProduct::total($vproducts);
        for ($vproductNumber=0; $vproductNumber<$vproductsTotal; $vproductNumber++){
            if ( $vproductNumber==0 ){
                $vdata='<table id="vgrdproductsPricesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductId" style="text-align:center;font-weight:bold;">Clave</th>
                                    <th data-field="vproductName" style="text-align:center; font-weight:bold;">Producto</th>
                                    <th data-field="vproductSalePrice" style="text-align:center;font-weight:bold;">Precio</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproduct" onClick="setProductId('. $vproducts->products[$vproductNumber]->idProduct . ');" title="Seleccionar Producto" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vproducts->products[$vproductNumber]->key. '</td>';
            $vdata.='	<td>' . strtoupper($vproducts->products[$vproductNumber]->name) . '</td>';
            $vdata.='	<td>' . "$ " . number_format($vproducts->products[$vproductNumber]->salePrice, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vproductNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsList", "innerHTML", $vdata);
            $vresponse->script("setProductsPricesList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen productos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vproducts, $vproductsTotal, $vproductNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los productos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function getProductName($vidProduct)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vflProduct= new clspFLProduct();
		$vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->idProduct=$vidProduct;
        clspBLProduct::queryToDataBase($vflProduct);
        $vresponse->setReturnValue($vflProduct->name);
        
		unset($vflProduct);
 	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de obtener el nombre del producto, intente de nuevo");
        $vresponse->setReturnValue("");
	}
    
    unset($vidProduct);
	return $vresponse;
 }

function recordProductSalePrice($vidProduct, $vprice)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProduct= new clspFLProduct();
        $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->idProduct=$vidProduct;
        $vflProduct->salePrice=(float)($vprice);
        switch(clspBLProduct::recordSalePriceInDataBase($vflProduct)){
            case -1:$vresponse->alert("El precio del producto no ha sufrido cambio alguno, intente de nuevo");
                    break;
            case 0: $vresponse->alert("El precio del producto no ha sufrido cambio alguno");
                    break;
            case 1: $vresponse->script("showProductsList();");
                    $vresponse->alert("El precio del producto ha sido registrado correctamente");
                    break;
        }
        
        unset($vflProduct);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el precio al producto, intente de nuevo");
	}
	
    unset($vidProduct, $vprice);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }

$vxajax->register(XAJAX_FUNCTION, "showProductTypesList");
$vxajax->register(XAJAX_FUNCTION, "showProductsList");
$vxajax->register(XAJAX_FUNCTION, "getProductName");
$vxajax->register(XAJAX_FUNCTION, "recordProductSalePrice");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>