<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");

date_default_timezone_set('America/Mexico_City');

function showExpensesTypesList()
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter="WHERE c_expensetype.id_enterprise=" . $_SESSION['idEnterprise'];
        $vexpenseTypes= new clscFLExpenseType();
        clscBLExpenseType::queryToDataBase($vexpenseTypes, $vfilter);
        $vexpenseTypesTotal=clscBLExpenseType::total($vexpenseTypes);
        for ($vexpenseTypeNumber=0; $vexpenseTypeNumber<$vexpenseTypesTotal; $vexpenseTypeNumber++){
            if ( $vexpenseTypeNumber==0 ){
                $vdata='<table id="vgrdexpensesTypesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vexpenseTypeName" style="text-align:center; font-weight:bold;">Tipo de Gasto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrexpenseType" onClick="showExpenseTypeData('. $vexpenseTypes->expenseTypes[$vexpenseTypeNumber]->idExpenseType . ');" title="Seleccionar Tipo de Gasto" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vexpenseTypes->expenseTypes[$vexpenseTypeNumber]->expenseType . '</td>';
            $vdata.="</tr>";
        }
        if ( $vexpenseTypeNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vexpensesTypesList", "innerHTML", $vdata);
            $vresponse->script("setExpenseTypesList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen tipos de gastos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vexpensesTypesList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vexpenseTypes, $vexpenseTypesTotal, $vexpenseTypeNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los tipos de gastos, intente de nuevo");
	}
	
	return $vresponse;
 }

function showExpenseTypeData($vidExpenseType)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflExpenseType= new clspFLExpenseType();
        $vflExpenseType->idExpenseType=$vidExpenseType;
        $vflExpenseType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLExpenseType::queryToDataBase($vflExpenseType)){
            case 0: $vresponse->alert("Los datos del tipo de gasto no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtexpenseType", $vresponse);
                    $vtext->setValue($vflExpenseType->expenseType);
                    $vresponse->script("enableExpenseTypeButtons();");
                    
                    unset($vtext);
                    break;
        }
	   
       unset($vflExpenseType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del tipo de gasto, intente de nuevo");
	}
    
    unset($vidExpenseType);
	return $vresponse;
 }

function addExpenseTypeData($vexpenseTypeForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflExpenseType= new clspFLExpenseType();
        $vflExpenseType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpenseType->expenseType=trim($vexpenseTypeForm["txtexpenseType"]);
        switch(clspBLExpenseType::addToDataBase($vflExpenseType)){
            case 0:  $vresponse->alert("Imposible registrar el tipo de gasto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidExpenseType=" . $vflExpenseType->idExpenseType);
                     $vresponse->script("showExpensesTypesList(0);");
                     $vresponse->alert("Los datos del tipo de gasto <" . $vflExpenseType->expenseType . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflExpenseType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el tipo de gasto, intente de nuevo");
	}
	
    unset($vexpenseTypeForm);
	return $vresponse;
 }

function updateExpenseTypeData($vidExpenseType, $vexpenseTypeForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vflExpenseType= new clspFLExpenseType();
        $vflExpenseType->idExpenseType=$vidExpenseType;
        $vflExpenseType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpenseType->expenseType=trim($vexpenseTypeForm["txtexpenseType"]);
        switch(clspBLExpenseType::updateInDataBase($vflExpenseType)){
            case 0: $vresponse->alert("Ningún dato se ha modificado del tipo de gasto");
                    break;
            case 1: $vresponse->script("showExpensesTypesList(1);");
                    $vresponse->alert("Los datos del tipo de gasto han sido modificados correctamente");
                    break;
        }
        
        unset($vflExpenseType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del tipo de gasto, intente de nuevo");
	}
	
    unset($vidExpenseType, $vexpenseTypeForm);
	return $vresponse;
 }

function deleteExpenseTypeData($vidExpenseType)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflExpenseType= new clspFLExpenseType();
        $vflExpenseType->idExpenseType=$vidExpenseType;
        $vflExpenseType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLExpenseType::deleteInDataBase($vflExpenseType)){
            case 0:  $vresponse->alert("Imposible eliminar el tipo de gasto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidExpenseType=0;");
                     $vresponse->script("cleanExpenseTypeFormFields();");
                     $vresponse->script("showExpensesTypesList(0);");
                     $vresponse->alert("Los datos del tipo de gasto han sido eliminados correctamente");
                     break;
        }
        
        unset($vflExpenseType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del tipo gasto, intente de nuevo");
	}
	
    unset($vidExpenseType);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showExpensesTypesList");
$vxajax->register(XAJAX_FUNCTION, "showExpenseTypeData");
$vxajax->register(XAJAX_FUNCTION, "addExpenseTypeData");
$vxajax->register(XAJAX_FUNCTION, "updateExpenseTypeData");
$vxajax->register(XAJAX_FUNCTION, "deleteExpenseTypeData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>