<?php


require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="ventas-cajas";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptIncomeExpenses= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptIncomeExpenses->addPage();
    
    $vreportSubtitle="Del " . trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]);
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    //------------------------------------------------------Ingresos----------------------------------------------------
    showPage($vrptIncomeExpenses, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
    $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=30;
    //------------------------------------------------------Ventas------------------------------------------------------
    $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=15;
    $vrptIncomeExpenses->printMultiLine(520, 15, "Ventas" , 10, "B", "L", 1);
    $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=15;
    $vrptIncomeExpenses->printMultiLine(100, 15, "Concepto" , 8, "B", "L", 1);
    $vrptIncomeExpenses->x=150;
    $vrptIncomeExpenses->printMultiLine(210, 15, "Responsable" , 8, "B", "L", 1);
    $vrptIncomeExpenses->x=360;
    $vrptIncomeExpenses->printMultiLine(70, 15, "Efectivo Inicial" , 8, "B", "C", 1);
    $vrptIncomeExpenses->x=430;
    $vrptIncomeExpenses->printMultiLine(70, 15, "Contado" , 8, "B", "C", 1);
    $vrptIncomeExpenses->x=500;
    $vrptIncomeExpenses->printMultiLine(70, 15, utf8_decode('Crédito') , 8, "B", "C", 1);
    //------------------------------------------------------Ventas de Cajas---------------------------------------------
    $vinitialCashByCash=0;
    $vcashSaleAmountByCash=0;
    $vcreditSaleAmountByCash=0;
    $vfilter ="WHERE c_routecustomer.id_route IS NULL ";
    $vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
    $vfilter.="AND p_productsale.fldcanceled=0";
    $vproductsSalesDetails= new clscFLProductSaleDetail();
    clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsSalesDetails, $vfilter, 1);
    $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
    for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; 
        $vproductsSalesDetailNumber++){
        if( $vrptIncomeExpenses->getY()>=690 ){
            $vrptIncomeExpenses->addPage();
            showPage($vrptIncomeExpenses, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
            $vpageNew=true;
        }
        if( $vpageNew ){
            $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=60;
            $vrptIncomeExpenses->printMultiLine(100, 15, "Concepto" , 8, "B", "L", 1);
            $vrptIncomeExpenses->x=150;
            $vrptIncomeExpenses->printMultiLine(210, 15, "Responsable" , 8, "B", "L", 1);
            $vrptIncomeExpenses->x=360;
            $vrptIncomeExpenses->printMultiLine(70, 15, "Efectivo Inicial" , 8, "B", "C", 1);
            $vrptIncomeExpenses->x=430;
            $vrptIncomeExpenses->printMultiLine(70, 15, "Contado" , 8, "B", "C", 1);
            $vrptIncomeExpenses->x=500;
            $vrptIncomeExpenses->printMultiLine(70, 15, utf8_decode("Crédito") , 8, "B", "C", 1);
            $vpageNew=false;
        }
        $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=15;
        $vrptIncomeExpenses->printMultiLine(100, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->cash->cash , 7, "", "L", 1);
        $vrptIncomeExpenses->x=150;
        $vresponsibleName=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->user->name . " " .
                          $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->user->firstName . " " .
                          $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->user->lastName;
        $vrptIncomeExpenses->printMultiLine(210, 15, $vresponsibleName, 7, "", "L", 1);
        
        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_cashmovement.id_cash= " . $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->cash->idCash . " ";
        $vfilter.="AND p_cashmovement.id_user='" . $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->user->idUser . "'";
        $vinitialCash=0;
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vcashMovements, $vfilter) ){
            $vinitialCash=$vcashMovements->cashMovements[0]->openAmount;
            $vinitialCashByCash+=$vinitialCash;
        }
        $vrptIncomeExpenses->x=360;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vinitialCash, 2, ".", ","), 7, "", "R", 1);
        
        $vfilter ="WHERE c_routecustomer.id_route IS NULL ";
        $vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.id_cash= " . $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->cash->idCash . " ";
        $vfilter.="AND p_productsale.id_user='" . $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->user->idUser . "' ";
        $vfilter.="AND p_productsale.id_operationType=1 ";
        $vfilter.="AND p_productsale.fldcanceled=0";
        $vcashSaleAmount=0;
        $vproductsCashSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter) ){
            $vcashSaleAmount=$vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
            $vcashSaleAmountByCash+=$vcashSaleAmount;
        }
        $vrptIncomeExpenses->x=430;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcashSaleAmount, 2, ".", ","), 7, "", "R", 1);
        
        $vfilter ="WHERE c_routecustomer.id_route IS NULL ";
        $vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.id_cash= " . $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->cash->idCash . " ";
        $vfilter.="AND p_productsale.id_user='" . $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->user->idUser . "' ";
        $vfilter.="AND p_productsale.id_operationType=2 ";
        $vfilter.="AND p_productsale.fldcanceled=0";
        $vcreditSaleAmount=0;
        $vproductsCashSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter) ){
            $vcreditSaleAmount=$vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
            $vcreditSaleAmountByCash+=$vcreditSaleAmount;    
        }
        $vrptIncomeExpenses->x=500;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcreditSaleAmount, 2, ".", ","), 7, "", "R", 1);
        
        unset($vresponsibleName, $vfilter, $vinitialCash, $vcashMovements, $vcashSaleAmount, $vproductsCashSalesDetails, $vcreditSaleAmount);
    }
    if( $vproductsSalesDetailsTotal>0 ){
        $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=15;
        $vrptIncomeExpenses->printMultiLine(310, 15, "Subtotal:" , 8, "B", "R", 1);
        $vrptIncomeExpenses->x=360;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vinitialCashByCash, 2, ".", ","), 7, "B", "R", 1);
        $vrptIncomeExpenses->x=430;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcashSaleAmountByCash, 2, ".", ","), 7, "B", "R", 1);
        $vrptIncomeExpenses->x=500;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcreditSaleAmountByCash, 2, ".", ","), 7, "B", "R", 1);
    }

    //------------------------------------------------------Resumen-----------------------------------------------------
    if( $vrptIncomeExpenses->getY()>=680 ){
        $vrptIncomeExpenses->addPage();
        showPage($vrptIncomeExpenses, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
        $vpageNew=true;
    }
    $vrptIncomeExpenses->x=50;   $vrptIncomeExpenses->y+=30;
    if( $vpageNew ){
        $vrptIncomeExpenses->y+=20;
        $vpageNew=false;
    }
    $vrptIncomeExpenses->printMultiLine(400, 15, "Total Ventas de Contado:" , 10, "B", "L", 0);
    $vrptIncomeExpenses->x=250;

    $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcashSaleAmountByCash, 2, ".", ","), 7, "B", "R", 0);
    $vrptIncomeExpenses->x=430;

    $vrptIncomeExpenses->x=50;  $vrptIncomeExpenses->y+=15;
    $vrptIncomeExpenses->printMultiLine(400, 15, utf8_decode("Total Ventas a Crédito:") , 10, "B", "L", 0);
    $vrptIncomeExpenses->x=150;

    $vrptIncomeExpenses->x=250;
        $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcreditSaleAmountByCash, 2, ".", ","), 7, "B", "R", 0);

    $vrptIncomeExpenses->x=50;  $vrptIncomeExpenses->y+=15;
    $vrptIncomeExpenses->printMultiLine(100, 15, "Total Ventas:" , 10, "B", "L", 0);
    $vrptIncomeExpenses->x=250;

    $vrptIncomeExpenses->printMultiLine(70, 15,"$ " . number_format($vcreditSaleAmountByCash + $vcashSaleAmountByCash, 2, ".", ","), 7, "B", "R", 0);

    
    $vrptIncomeExpenses->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptIncomeExpenses, $vreportSubtitle,
          $vflEnterpriseUser, $vinitialCashByCash, $vcashSaleAmountByCash, $vcreditSaleAmountByCash, $vfilter, $vproductsSalesDetails,
          $vproductsSalesDetailsTotal, $vproductsSalesDetailNumber, $vcashSaleAmountByRoutes, $vcreditSaleAmountByRoutes, $vcashSaleAmountTotal,
          $vcreditSaleAmountTotal, $vsaleAmountTotal, $vpaymentAmountByCustomers, $vproductsSalesPayments, $vproductsSalesPaymentsTotal,
          $vproductsSalesPaymentsNumber, $vpaymentAmountByDeliverers, $vloansPayments, $vloansPaymentsTotal, $vloansPaymentsNumber, $vpaymentAmountTotal,
          $vincomeAmountTotal, $vexpenseAmountByName, $vexpensesDetailsTotal, $vexpensesDetailsNumber, $vpaymentAmountToProviders,
          $vproductsEntriesPayments, $vproductsEntriesPaymentsTotal, $vproductsEntriesPaymentsNumber, $vexpensesTotal);
}
catch (Exception $vexception){
    echo "Ocurrió un error al tratar de mostrar el reporte de ingresos-egresos";
}

function showPage($vrptIncomeExpenses, $vflEnterpriseUser, $vdate, $vreportSubtitle, $vpageNumber)
 {
    try{
        showHeader($vrptIncomeExpenses, $vflEnterpriseUser, $vdate, $vreportSubtitle);
        showFooter($vrptIncomeExpenses, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptIncomeExpenses, $vflEnterpriseUser, $vdate, $vreportSubtitle)
 {
    try{
        $vrptIncomeExpenses->x=50;	$vrptIncomeExpenses->y=60;
        $vrptIncomeExpenses->printMultiLine(520, 20, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptIncomeExpenses->x=50;	$vrptIncomeExpenses->y+=14;
        $vrptIncomeExpenses->printMultiLine(520, 20, "\"Reporte de Ventas en cajas\"" , 10, "B", "C", 0);
        $vrptIncomeExpenses->x=50;	$vrptIncomeExpenses->y+=14;
        $vrptIncomeExpenses->printMultiLine(520, 20, $vreportSubtitle, 10, "B", "C", 0);
        $vrptIncomeExpenses->x=50;	$vrptIncomeExpenses->y+=12;
        $vrptIncomeExpenses->printMultiLine(520, 20, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptIncomeExpenses, $vpageNumber)
 {
    try{
        $vrptIncomeExpenses->x=50;  $vrptIncomeExpenses->y=715;
        $vrptIncomeExpenses->printMultiLine(520, 20, "Pagina Numero " . $vpageNumber , 6, "", "R", 0);
        $vrptIncomeExpenses->y=86;
        $vrptIncomeExpenses->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }






?>