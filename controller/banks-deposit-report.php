<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");
date_default_timezone_set('America/Mexico_City');

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBank.php");


function showBanksList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter ="WHERE c_bank.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vbanks= new clscFLBank();
        clscBLBank::queryToDataBase($vbanks, $vfilter);
        $vbanksTotal=clscBLBank::total($vbanks);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vbanksTotal; $vi++){
            $vJSON.=', {"text":"' . $vbanks->banks[$vi]->name .
                    '", "value":' . $vbanks->banks[$vi]->idBank  . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vbanks, $vbanksTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los bancos, intente de nuevo");
	}

	return $vresponse;
 }

function printBanksDeposit($vfilterForm)
 {
    $vresponse= new xajaxResponse();
	   
	try{
        $vurl ="./controller/banks-deposit-report-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
        $vurl.="&vbank=" . $vfilterForm["cmbbanksList"];
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vurl);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir los depósitos bancarios, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showBanksList");
$vxajax->register(XAJAX_FUNCTION, "printBanksDeposit");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>