<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");

date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotalSalePrice=0;


    $vKilos = 0;

    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="ventas-productos-ruta-cantidad";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsSales= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsSales->addPage();

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);

    $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    if ( (int)($_GET["vidRoute"])!=0 ){
        $vfilter.="AND c_route.id_route=" . (int)($_GET["vidRoute"]);;
    }




    //$vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    //$vfilter.="AND p_productsale.fldcanceled=0 ";
    /*if ( strcmp(trim($vproductsSalesProfitsForm["dtpckrstartDate"]), "")!=0 ){
        $vfilter.="AND (p_productsale.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vproductsSalesProfitsForm["dtpckrstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vproductsSalesProfitsForm["dtpckrendDate"]))) . "') ";
    }*/



    $vroutes= new clscFLRoute();
    clscBLRoute::queryToDataBase($vroutes, $vfilter);
    $vroutesTotal=clscBLRoute::total($vroutes);
    for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
        if( $vrptProductsSales->getY()>=500 ){
            $vrptProductsSales->addPage();
            showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
        }
        $vcashTotal=1;
        $vtotalSalePrice = 0;
        $vcreditGrantedTotal=0;
        $vchargeTotal=0;
        $inicio = date("Y-m-d", strtotime(trim($_GET["vstartDate"])));
        $fin = date("Y-m-d", strtotime(trim($_GET["vendDate"])));
        $caja = obtenerCaja($vroutes->routes[$vrouteNumber]->route);
        $ruta = obtenerRuta($vroutes->routes[$vrouteNumber]->route);



        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";


        $vfilter1=$vfilter . "AND p_productsale.fldcanceled=0 ";
        $vfilter1.="AND p_productsale.id_cash='".$caja."'";


        //$vproductsSalesDetails= new clscFLProductSaleDetail();
        //clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
        //$vtotalSalePrice=clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails);


        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryByRouteToDataBaseCost($vproductsSalesDetails, $vfilter1)==1 ){
              $vtotalSalePrice=$vproductsSalesDetails->productsSalesDetails[0]->realPrice;
        }




        /*
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryByRouteToDataBase($vproductsSalesDetails, $vfilter1)==1 ){
              //$vKilos+=$vproductsSalesDetails->productsSalesDetails[0]->realPrice;

              $vKilos = $vproductsSalesDetails->productsSalesDetails[0]->saleWeightInKg;
        }
        */
        

            $vfilter3 ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            //$vfilter3.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
            $vfilter3.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
            $vfilter3.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
            $vfilter3.="AND p_productsale.fldcanceled=0 ";
            $vfilter3.="AND p_productsale.id_cash='".$caja."'";

            $vproductsSalesDetails= new clscFLProductSaleDetail();
            if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ){
                $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);

                for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){

                    $vKilos+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                }
            }















        $vfilter1=$vfilter . "AND p_productsale.id_operationType=1 ";
        $vfilter1.="AND p_productsale.fldcanceled=0 ";
        $vfilter1.="AND p_productsale.id_cash='".$caja."'";

        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryByRouteToDataBase($vproductsSalesDetails, $vfilter1)==1 ){
            $vcashTotal=$vproductsSalesDetails->productsSalesDetails[0]->salePrice;
        }
        $vfilter2=$vfilter . "AND p_productsale.id_operationType=2 ";
        $vfilter2.="AND p_productsale.fldcanceled=0 ";
        $vfilter2.="AND p_productsale.id_cash='".$caja."'";
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryByRouteToDataBase($vproductsSalesDetails, $vfilter2)==1 ){
            $vcreditGrantedTotal=$vproductsSalesDetails->productsSalesDetails[0]->salePrice;
        }
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
        $vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.id_operationType=2 ";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        $vfilter.="AND p_productsale.id_cash='".$caja."'";
        $vproductsSalesPayments= new clscFLProductSalePayment();
        if ( clscBLProductSalePayment::queryByRouteToDataBase($vproductsSalesPayments, $vfilter)==1 ){
            $vchargeTotal=$vproductsSalesPayments->productsSalesPayments[0]->amount;
        }
        $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
        $vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
        $vrptProductsSales->x=80;
        $vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
        $vrptProductsSales->y+=20;
        $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total Contado:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcashTotal   , 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, utf8_decode("Total Crédito:"), 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcreditGrantedTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Kilogramos vendidos:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15,number_format($vKilos, 2, ".", ","), 8, "B", "R", 0);


        $vKilos = 0;
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total de ventas:" , 8, "B", "R", 0);


        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcashTotal + $vcreditGrantedTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total de Cobranzas:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vchargeTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total por Entregar:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcashTotal + $vchargeTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $utilidadBruta= profit($inicio,$fin,$caja);

        $vrptProductsSales->printMultiLine(110, 15, "Total utilidad bruta:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($utilidadBruta, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;


        $vrptProductsSales->printMultiLine(110, 15, "Costo de lo vendido:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vtotalSalePrice, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=150;



        $vrptProductsSales->printMultiLine(160, 15, "Porcentaje de utilidad bruta:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        if ($vtotalSalePrice==0) {
          $vrptProductsSales->printMultiLine(80, 15, "" . number_format((0)*100, 2, ".", ",")." %", 8, "B", "R", 0);
        }else{
          $vrptProductsSales->printMultiLine(80, 15, "" . number_format(($utilidadBruta/$vtotalSalePrice)*100, 2, ".", ",")." %", 8, "B", "R", 0);
        }
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $totalGastos = gastos($inicio,$fin,$ruta);



        $vrptProductsSales->printMultiLine(110, 15, "Total gastos:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($totalGastos, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total utilidad:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($utilidadBruta-$totalGastos, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=150;
        $vrptProductsSales->printMultiLine(160, 15, "Porcentaje de utilidad neta:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        if ($vtotalSalePrice==0) {
          $vrptProductsSales->printMultiLine(80, 15, "" . number_format((0)*100, 2, ".", ",")." %", 8, "B", "R", 0);
        }else{
          $vrptProductsSales->printMultiLine(80, 15, "" . number_format((($utilidadBruta-$totalGastos)/$vtotalSalePrice)*100, 2, ".", ",")." %", 8, "B", "R", 0);
        }



        unset($vproductsSalesDetails);
    }
    $vrptProductsSales->showPage();

    unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
          $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de ventas de productos (entregas)";
}



function profit($fechaInicio,$fechaFin,$idcaja)
 {
    $vresponse = NULL;

    try{
        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        $vfilter .="AND p_productsale.id_cash='".$idcaja."' ";
        if ( strcmp(trim($fechaInicio), "")!=0 ){
            $vfilter.="AND (p_productsale.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($fechaInicio)))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($fechaFin))) . "') ";
        }
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
        $vtotalSalePrice=clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails);
        //$vresponse->assign("vsalesTotals", "innerHTML", "$ " . number_format($vtotalSalePrice, 2, ".", ","));
        $vtotalPurchasePrice=clscBLProductSaleDetail::totalPurchasePrice($vproductsSalesDetails);
        //$vresponse->assign("vsalesCosts", "innerHTML", "$ " . number_format($vtotalPurchasePrice, 2, ".", ","));
        $vgrossProfit=$vtotalSalePrice - $vtotalPurchasePrice;
        //$vresponse->assign("vgrossProfit", "innerHTML", "$ " . number_format($vgrossProfit, 2, ".", ","));
        $vresponse = $vgrossProfit;

        $vfilter ="WHERE p_expense.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($fechaInicio), "")!=0 ){
            $vfilter.="AND (p_expense.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($fechaInicio)))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($fechaFin))) . "') ";
        }
        $vexpensesDetails= new clscFLExpenseDetail();
        clscBLExpenseDetail::queryToDataBase($vexpensesDetails, $vfilter);
        $vexpenseTotalAmount=clscBLExpenseDetail::totalAmount($vexpensesDetails);
        //$vresponse->assign("vexpensesTotals", "innerHTML", "$ " . number_format($vexpenseTotalAmount, 2, ".", ","));
        //$vresponse->assign("vnetIncome", "innerHTML", "$ " . number_format($vgrossProfit - $vexpenseTotalAmount, 2, ".", ","));

        unset($vfilter, $vproductsSalesDetails, $vtotalSalePrice, $vtotalPurchasePrice, $vgrossProfit, $vexpenseTotalAmount);
    }
    catch (Exception $vexception){
        //$vresponse->alert("Ocurri� un error al tratar de mostrar los datos de las utilidades de ventas, intente de nuevo");
    }

    unset($vproductsSalesProfitsForm);
    return $vresponse;
 }

function gastos($fechaInicio,$fechaFin,$idruta)
 {
    $vresponse = NULL;

    try{
        /*
        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        $vfilter .="AND p_productsale.id_cash='".$idcaja."' ";
        if ( strcmp(trim($fechaInicio), "")!=0 ){
            $vfilter.="AND (p_productsale.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($fechaInicio)))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($fechaFin))) . "') ";
        }
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
        $vtotalSalePrice=clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails);
        //$vresponse->assign("vsalesTotals", "innerHTML", "$ " . number_format($vtotalSalePrice, 2, ".", ","));
        $vtotalPurchasePrice=clscBLProductSaleDetail::totalPurchasePrice($vproductsSalesDetails);
        //$vresponse->assign("vsalesCosts", "innerHTML", "$ " . number_format($vtotalPurchasePrice, 2, ".", ","));
        $vgrossProfit=$vtotalSalePrice - $vtotalPurchasePrice;
        //$vresponse->assign("vgrossProfit", "innerHTML", "$ " . number_format($vgrossProfit, 2, ".", ","));
        $vresponse = $vgrossProfit;
        */

        $vfilter ="WHERE p_expense.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_expense.id_route='".$idruta."'";
        $vfilter.="AND (p_expense.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($fechaInicio))). "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($fechaFin))). "') ";

        $vexpensesDetails= new clscFLExpenseDetail();
        clscBLExpenseDetail::queryToDataBase($vexpensesDetails, $vfilter);
        $vexpenseTotalAmount=clscBLExpenseDetail::totalAmount($vexpensesDetails);
        $vresponse = $vexpenseTotalAmount;
        //$vresponse->assign("vexpensesTotals", "innerHTML", "$ " . number_format($vexpenseTotalAmount, 2, ".", ","));
        //$vresponse->assign("vnetIncome", "innerHTML", "$ " . number_format($vgrossProfit - $vexpenseTotalAmount, 2, ".", ","));

        unset($vfilter, $vproductsSalesDetails, $vtotalSalePrice, $vtotalPurchasePrice, $vgrossProfit, $vexpenseTotalAmount);
    }
    catch (Exception $vexception){
        //$vresponse->alert("Ocurri� un error al tratar de mostrar los datos de las utilidades de ventas, intente de nuevo");
    }

    unset($vproductsSalesProfitsForm);
    return $vresponse;
 }


function obtenerCaja($nombreRuta)
 {
    $vresponse = NULL;

    try{
        $vfilter = "WHERE c_cash.fldcash='".$nombreRuta."'";
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal=clscBLCash::total($vcash);


            $vresponse = $vcash->cash[0]->idCash;

        unset($vcash, $vcashTotal, $vJSON, $vi);
    }
    catch (Exception $vexception){
    }

    return $vresponse;
 }


function obtenerRuta($nombreCaja)
{
    $vroutes = new clscFLRoute();
    $vroute  = NULL;
    $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    $vfilter .= "AND c_route.fldroute='" . strval($nombreCaja) . "'";
    clscBLRoute::queryToDataBase($vroutes, $vfilter);
    $vroutesTotal = clscBLRoute::total($vroutes);
    for ($vi = 0; $vi < $vroutesTotal; $vi++) {
        $vroute = $vroutes->routes[$vi]->idRoute;
    }
    return $vroute;
}


function showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsSales, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsSales->x=50;   $vrptProductsSales->y=60;
        $vrptProductsSales->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsSales->x=50;   $vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Reporte de Ventas de Productos (Ruta) por Entregas\"" , 10, "B", "C", 0);
        $vrptProductsSales->x=50;   $vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Periodo del ". trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]) . "\"" , 8, "B", "C", 0);

        $vrptProductsSales->x=50;   $vrptProductsSales->y+=12;
        $vrptProductsSales->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showFooter($vrptProductsSales, $vpageNumber)
 {
    try{
        $vrptProductsSales->x=50;  $vrptProductsSales->y=715;
        $vrptProductsSales->printMultiLine(520, 20, "Página Número " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsSales->y=105;
        $vrptProductsSales->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

?>
