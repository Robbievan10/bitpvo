<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashCount.php");
date_default_timezone_set('America/Mexico_City');


function showCashList()
 {
	$vresponse= new xajaxResponse();

    try{
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, "");
        $vcashTotal=clscBLCash::total($vcash);

        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vcashTotal; $vi++){
            $vJSON.=', {"text":"' . $vcash->cash[$vi]->cash .
                    '", "value":' . $vcash->cash[$vi]->idCash . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
		unset($vcash, $vcashTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar las cajas, intente de nuevo");
	}

	return $vresponse;
 }

function showCashiersList()
 {
	$vresponse= new xajaxResponse();

    try{
        $vfilter ="WHERE c_enterpriseuser.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_user.id_userType=2 OR c_user.id_userType=3 OR c_user.id_userType=6 OR c_user.id_userType=7";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $vJSON='[{"text":"--Todos--", "value":""}';
        for ($vi=0; $vi<$venterpriseUsersTotal; $vi++){
            $vJSON.=', {"text":"' . $venterpriseUsers->enterpriseUsers[$vi]->name . ' ' .
                                    $venterpriseUsers->enterpriseUsers[$vi]->firstName . ' ' .
                                    $venterpriseUsers->enterpriseUsers[$vi]->lastName .
                    '", "value":"' . $venterpriseUsers->enterpriseUsers[$vi]->idUser . '"}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
		unset($vfilter, $venterpriseUsers, $venterpriseUsersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los usuarios, intente de nuevo");
	}

	return $vresponse;
 }

function showCashCountList($vfilterForm)
 {
	$vresponse= new xajaxResponse();

	try{
	    $vfilter ="WHERE p_cashcount.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_cashcount.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        } elseif ((int)($vfilterForm["cmbcashList"])!=0) {
            $vfilter.="AND p_cashcount.id_cash=" . (int)($vfilterForm["cmbcashList"]) . " ";        }
            elseif (strcmp(trim($vfilterForm["cmbcashiersList"]), "")!=0) {
            $vfilter.="AND p_cashcount.id_userCashier='" . trim($vfilterForm["cmbcashiersList"]) . "' ";            }
            else{
            $vfilter.= "AND p_cashcount.fldrecordDate='" .date("Y-m-d"). "' ";

            }



        $vcashCount= new clscFLCashCount();
        clscBLCashCount::queryToDataBase($vcashCount, $vfilter);
        $vcashCountTotal=clscBLCashCount::total($vcashCount);
        for ($vcashCountNumber=0; $vcashCountNumber<$vcashCountTotal; $vcashCountNumber++){
            if ( $vcashCountNumber==0 ){
                $vdata='<table id="vgrdcashCountList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vrecordDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vcashName" style="text-align:center; font-weight:bold;">Caja</th>
                                    <th data-field="vcashierName" style="text-align:center;font-weight:bold;">Cajero</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrcashCount" onClick="setCashCountIds('.
                            $vcashCount->cashCount[$vcashCountNumber]->cash->idCash . ',' .
                            $vcashCount->cashCount[$vcashCountNumber]->idCashCount . ');" title="Seleccionar Arqueo de Caja" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vcashCount->cashCount[$vcashCountNumber]->recordDate. '</td>';
            $vdata.='	<td>' . $vcashCount->cashCount[$vcashCountNumber]->cash->cash . '</td>';
            $vdata.='	<td>' . $vcashCount->cashCount[$vcashCountNumber]->userCashier->name . " " .
                                $vcashCount->cashCount[$vcashCountNumber]->userCashier->firstName . " " .
                                $vcashCount->cashCount[$vcashCountNumber]->userCashier->lastName . '</td>';
            $vdata.="</tr>";
        }
        if ( $vcashCountNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcashCountList", "innerHTML", $vdata);
            $vresponse->script("setCashCountList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen arqueos de cajas registrados este día.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcashCountList", "innerHTML", $vdata);

        }

        unset($vfilter, $vcashCount, $vcashCountTotal, $vcashCountNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los arqueos de cajas, intente de nuevo");
	}

    unset($vfilterForm);
	return $vresponse;
 }

function deleteCashCountData($vidCash, $vidCashCount)
 {
	$vresponse= new xajaxResponse();

	try{
	    $vflCashCount= new clspFLCashCount();
        $vflCashCount->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashCount->cash->idCash=$vidCash;
        $vflCashCount->idCashCount=$vidCashCount;
        switch(clspBLCashCount::deleteInDataBase($vflCashCount)){
            case 0:  $vresponse->alert("Imposible eliminar el arqueo de caja, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showCashCountList();");
                     $vresponse->alert("Los datos del arqueo de caja han sido eliminados correctamente");
                     break;
        }

        unset($vflCashCount);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del arqueo de caja, intente de nuevo");
	}

    unset($vidCash, $vidCashCount);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();

	session_destroy();
	$vresponse->redirect("./");

	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showCashList");
$vxajax->register(XAJAX_FUNCTION, "showCashiersList");
$vxajax->register(XAJAX_FUNCTION, "showCashCountList");
$vxajax->register(XAJAX_FUNCTION, "deleteCashCountData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>
