<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");
require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
date_default_timezone_set('America/Mexico_City');

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLDealer.php");
date_default_timezone_set('America/Mexico_City');

function rutas($option){
  $vresponse= new xajaxResponse();

  try{
        //Inicio obtener nombres de rutas
        $vroutesDeliverers= new clscFLRouteDealer();
        clscBLRouteDealer::queryToDataBase($vroutesDeliverers, "WHERE c_routedealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vroutesDeliverersTotal=clscBLRouteDealer::total($vroutesDeliverers);
        $listaRutas = array();
        $listaID = array();
        for ($vi=0; $vi<$vroutesDeliverersTotal; $vi++){
            $listaRutas[] = $vroutesDeliverers->routesDeliverers[$vi]->route->route;
            $listaID[] = $vroutesDeliverers->routesDeliverers[$vi]->route->idRoute;
        }
        //Fin obtener nombres de rutas


        //Inicio obtener venta semanal
        $listaVenta = array();
        for ($i=0; $i < count($listaRutas); $i++) {
          $ventaSemanal = array();
          for ($j=1; $j <= 7; $j++) {
            $ventaSemanal[] = ventaPorDia($listaID[$i],$j);
            }
          $listaVenta[] = $ventaSemanal;
        }

        $listaVentaPasada = array();
        for ($i=0; $i < count($listaRutas); $i++) {
          $ventaSemanal = array();
          for ($j=1; $j <= 7; $j++) {
            $ventaSemanal[] = ventaPorDiaPasado($listaID[$i],$j);
            }
          $listaVentaPasada[] = $ventaSemanal;
        }



        //Fin obtener venta semanal

        //Inicio generacion de JSON
        $arrayVentas = array();
        $z = 0;
        for ($i=0; $i < count($listaRutas) ; $i++) {
          $arrayVentas[] = array('name'=>$listaRutas[$z],'data'=>$listaVenta[$z]);
          $z+=1;
        }
        $vJSON = json_encode($arrayVentas);


        $arrayVentasPasadas = array();
        $w = 0;
        for ($i=0; $i < count($listaRutas) ; $i++) {
          $arrayVentasPasadas[] = array('name'=>$listaRutas[$w],'data'=>$listaVentaPasada[$w]);
          $w+=1;
        }
        $vJSON2 = json_encode($arrayVentasPasadas);
        //Fin generacion de JSON



// Retorno de JSON estructura [{name:'501',data:[1,2,3,4,5,6,7]},{name:'502',data:[8,9,10,11,12,13,14]}]
        //$vJSON = "[{name:'501',data:[1,2,3,4,5,6,7]},{name:'502',data:[8,9,10,11,12,13,14]}]";
        switch ($option) {
          case 1:
          $vresponse->script("rutas_semanal(".$vJSON.",".$vJSON2.");");
          $vresponse->setReturnValue($listaVenta);
            break;
          case 2:
          $vresponse->script("rutas_semanal(".$vJSON.");");
            break;
          case 3:
          $vresponse->script("rutas_semanal(".$vJSON.");");
            break;
          default:
          $vresponse->script("rutas_semanal(".$vJSON.");");
            break;
        }

    }
  catch (Exception $vexception){
    $vresponse->alert("Ocurrió un error al tratar de listar las rutas asignadas a repartidores, intente de nuevo");
  }
	return $vresponse;
}

function ventaPorDia($idRuta,$dia){
  $ventaTotal = 0;
  try{
      $vgTotalSalePrice=0;
      $vflEnterpriseUser= new clspFLEnterpriseUser();
      $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
      clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);

      $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
      $vfilter.="AND c_route.id_route=" . $idRuta ."";

      $vroutes= new clscFLRoute();
      clscBLRoute::queryToDataBase($vroutes, $vfilter);
      $vroutesTotal=clscBLRoute::total($vroutes);
      for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
          switch ($dia) {
            case 1:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Monday this week'))."' ";
              break;
            case 2:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Tuesday this week'))."' ";
              break;
            case 3:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Wednesday this week'))."' ";
              break;
            case 4:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Thursday this week'))."' ";
              break;
            case 5:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Friday this week'))."' ";
              break;
            case 6:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Saturday this week'))."' ";
              break;
            case 7:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Sunday this week'))."' ";
              break;
            default:
              break;
          }
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          $vproductsSalesDetails= new clscFLProductSaleDetail();
          if ( clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1) ){
              $vsaleTotal=0;
              $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
              for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                  $vsaleTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
              }
              if( $vproductsSalesDetailsTotal>0 ){
                  $ventaTotal = $vsaleTotal;
              }

              unset($vsaleTotal);
          }

          unset($vproductsSalesDetails);
      }
      unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
            $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
  }
  catch (Exception $vexception){
  }
  return $ventaTotal;
}


function ventaPorDiaPasado($idRuta,$dia){
  $ventaTotal = 0;
  try{
      $vgTotalSalePrice=0;
      $vflEnterpriseUser= new clspFLEnterpriseUser();
      $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
      clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);

      $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
      $vfilter.="AND c_route.id_route=" . $idRuta ."";

      $vroutes= new clscFLRoute();
      clscBLRoute::queryToDataBase($vroutes, $vfilter);
      $vroutesTotal=clscBLRoute::total($vroutes);
      for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
          $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
          $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
          switch ($dia) {
            case 1:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Monday last week'))."' ";
              break;
            case 2:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Tuesday last week'))."' ";
              break;
            case 3:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Wednesday last week'))."' ";
              break;
            case 4:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Thursday last week'))."' ";
              break;
            case 5:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Friday last week'))."' ";
              break;
            case 6:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Saturday last week'))."' ";
              break;
            case 7:
            $vfilter.="AND p_productsale.fldproductSaleDate = '".date('Y-m-d', strtotime('Sunday last week'))."' ";
              break;
            default:
              break;
          }
          $vfilter.="AND p_productsale.fldcanceled=0 ";
          $vproductsSalesDetails= new clscFLProductSaleDetail();
          if ( clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1) ){
              $vsaleTotal=0;
              $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
              for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                  $vsaleTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
              }
              if( $vproductsSalesDetailsTotal>0 ){
                  $ventaTotal = $vsaleTotal;
              }

              unset($vsaleTotal);
          }

          unset($vproductsSalesDetails);
      }
      unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
            $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
  }
  catch (Exception $vexception){
  }
  return $ventaTotal;
}


$vxajax->register(XAJAX_FUNCTION,"rutas");
$vxajax->processRequest();

?>
