<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");
date_default_timezone_set('America/Mexico_City');

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBankDeposit.php");


function showBanksList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter ="WHERE c_bank.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vbanks= new clscFLBank();
        clscBLBank::queryToDataBase($vbanks, $vfilter);
        $vbanksTotal=clscBLBank::total($vbanks);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vbanksTotal; $vi++){
            $vJSON.=', {"text":"' . $vbanks->banks[$vi]->name .
                    '", "value":' . $vbanks->banks[$vi]->idBank  . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vbanks, $vbanksTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los bancos, intente de nuevo");
	}

	return $vresponse;
 }

function showBanksDepositsList($vfilterForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vfilter ="WHERE p_bankdeposit.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_bankdeposit.flddepositDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }
        if ( (int)($vfilterForm["cmbbanksList"])!=0 ){
            $vfilter.="AND p_bankdeposit.id_bank=" . (int)($vfilterForm["cmbbanksList"]) . " ";
        }
        if ( strcmp(trim($vfilterForm["txtaccountNumber"]), "")!=0 ){
            $vfilter.="AND p_bankdeposit.fldaccountNumber='" . (int)($vfilterForm["txtaccountNumber"]) . "' ";
        }
        $vbanksDeposits= new clscFLBankDeposit();
        clscBLBankDeposit::queryToDataBase($vbanksDeposits, $vfilter);
        $vbanksDepositsTotal=clscBLBankDeposit::total($vbanksDeposits);
        for ($vbankDepositNumber=0; $vbankDepositNumber<$vbanksDepositsTotal; $vbankDepositNumber++){
            if ( $vbankDepositNumber==0 ){
                $vdata='<table id="vgrdbanksDepositsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vdepositDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vbankName" style="text-align:center; font-weight:bold;">Banco</th>
                                    <th data-field="vaccountNumber" style="text-align:center; font-weight:bold;">Número de Cuenta</th>
                                    <th data-field="vamount" style="text-align:center;font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrbankDeposit" onClick="setBankDepositIds('. 
                            $vbanksDeposits->banksDeposits[$vbankDepositNumber]->bank->idBank . ',' .
                            $vbanksDeposits->banksDeposits[$vbankDepositNumber]->idBankDeposit . ');" title="Seleccionar Depósito Bancario" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vbanksDeposits->banksDeposits[$vbankDepositNumber]->depositDate. '</td>';
            $vdata.='	<td>' . $vbanksDeposits->banksDeposits[$vbankDepositNumber]->bank->name . '</td>';
            $vdata.='	<td>' . $vbanksDeposits->banksDeposits[$vbankDepositNumber]->accountNumber . '</td>';
            $vdata.='	<td>' . "$ " . number_format($vbanksDeposits->banksDeposits[$vbankDepositNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vbankDepositNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vbanksDepositsList", "innerHTML", $vdata);
            $vresponse->script("setBankDepositList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen depósitos bancarios registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vbanksDepositsList", "innerHTML", $vdata);
        }    
        
        unset($vfilter, $vbanksDeposits, $vbanksDepositsTotal, $vexpenseDetailNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los depósitos bancarios, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function deleteBankDepositData($vidBank, $vidBankDeposit)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflBankDeposit= new clspFLBankDeposit();
        $vflBankDeposit->bank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflBankDeposit->bank->idBank=$vidBank;
        $vflBankDeposit->idBankDeposit=$vidBankDeposit;
        switch(clspBLBankDeposit::deleteInDataBase($vflBankDeposit)){
            case 0:  $vresponse->alert("Imposible eliminar el depósito bancario, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showBanksDepositsList();");
                     $vresponse->alert("Los datos del depósito bancario han sido eliminados correctamente");
                     break;
        }
        
        unset($vflBankDeposit);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del depósito bancario, intente de nuevo");
	}
	
    unset($vidBank, $vidBankDeposit);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showBanksList");
$vxajax->register(XAJAX_FUNCTION, "showBanksDepositsList");
$vxajax->register(XAJAX_FUNCTION, "deleteBankDepositData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>