<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");

date_default_timezone_set('America/Mexico_City');

function showCashList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, "");
        $vcashTotal=clscBLCash::total($vcash);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vcashTotal; $vi++){
            $vJSON.=', {"text":"' . $vcash->cash[$vi]->cash .
                    '", "value":' . $vcash->cash[$vi]->idCash . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vcash, $vcashTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar las cajas, intente de nuevo");
	}

	return $vresponse;
 }

function printCashCount($vfilterForm)
 {
    $vresponse= new xajaxResponse();
	   
	try{
        $vurl ="./controller/cash-count-report-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
        $vurl.="&vcash=" . $vfilterForm["cmbcashList"];
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vurl);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir los arqueos de caja, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showCashList");
$vxajax->register(XAJAX_FUNCTION, "printCashCount");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>