<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");
date_default_timezone_set('America/Mexico_City');

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");


function showBanksList()
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vfilter="WHERE c_bank.id_enterprise=" . $_SESSION['idEnterprise'];
        $vbanks= new clscFLBank();
        clscBLBank::queryToDataBase($vbanks, $vfilter);
        $vbanksTotal=clscBLBank::total($vbanks);
        for ($vbankNumber=0; $vbankNumber<$vbanksTotal; $vbankNumber++){
            if ( $vbankNumber==0 ){
                $vdata='<table id="vgrdbanksList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vbankName" style="text-align:center; font-weight:bold;">Banco</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrbank" onClick="showBankData('. $vbanks->banks[$vbankNumber]->idBank . ');" title="Seleccionar Banco" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vbanks->banks[$vbankNumber]->name . '</td>';
            $vdata.="</tr>";
        }
        if ( $vbankNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vbanksList", "innerHTML", $vdata);
            $vresponse->script("setBanksList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen bancos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vbanksList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vbanks, $vbanksTotal, $vbankNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los bancos, intente de nuevo");
	}
	
	return $vresponse;
 }

function showBankData($vidBank)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflBank= new clspFLBank();
        $vflBank->idBank=$vidBank;
        $vflBank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLBank::queryToDataBase($vflBank)){
            case 0: $vresponse->alert("Los datos del banco no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtname", $vresponse);
                    $vtext->setValue($vflBank->name);
                    $vresponse->script("enableBankButtons();");
                    
                    unset($vtext);
                    break;
        }
	   
       unset($vflBank);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del banco, intente de nuevo");
	}
    
    unset($vidBank);
	return $vresponse;
 }

function addBankData($vbankForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflBank= new clspFLBank();
        $vflBank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflBank->name=trim($vbankForm["txtname"]);
        switch(clspBLBank::addToDataBase($vflBank)){
            case 0:  $vresponse->alert("Imposible registrar el banco, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidBank=" . $vflBank->idBank);
                     $vresponse->script("showBanksList(0);");
                     $vresponse->alert("Los datos del banco <" . $vflBank->name . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflBank);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el banco, intente de nuevo");
	}
	
    unset($vbankForm);
	return $vresponse;
 }

function updateBankData($vidBank, $vbankForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflBank= new clspFLBank();
        $vflBank->idBank=$vidBank;
        $vflBank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflBank->name=trim($vbankForm["txtname"]);
        switch(clspBLBank::updateInDataBase($vflBank)){
            case 0: $vresponse->alert("Ningún dato se ha modificado del banco");
                    break;
            case 1: $vresponse->script("showBanksList(1);");
                    $vresponse->alert("Los datos del banco han sido modificados correctamente");
                    break;
        }
        
        unset($vflBank);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del banco, intente de nuevo");
	}
	
    unset($vidBank, $vbankForm);
	return $vresponse;
 }

function deleteBankData($vidBank)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflBank= new clspFLBank();
        $vflBank->idBank=$vidBank;
        $vflBank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLBank::deleteInDataBase($vflBank)){
            case 0:  $vresponse->alert("Imposible eliminar el banco, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidBank=0;");
                     $vresponse->script("cleanBankFormFields();");
                     $vresponse->script("showBanksList(0);");
                     $vresponse->alert("Los datos del banco han sido eliminados correctamente");
                     break;
        }
        
        unset($vflBank);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del banco, intente de nuevo");
	}
	
    unset($vidBank);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showBanksList");
$vxajax->register(XAJAX_FUNCTION, "showBankData");
$vxajax->register(XAJAX_FUNCTION, "addBankData");
$vxajax->register(XAJAX_FUNCTION, "updateBankData");
$vxajax->register(XAJAX_FUNCTION, "deleteBankData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>