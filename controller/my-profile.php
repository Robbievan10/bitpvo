<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");

require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");


function showProfileData()
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflUser= new clspFLUser();
		$vflUser->idUser=trim($_SESSION['idUser']);
        switch(clspBLUser::queryToDataBase($vflUser)){
            case 0: $vresponse->alert("Su perfil no se encuentran registrado.");
				    break;
            case 1: $vtext= new clspText("txtname", $vresponse);
                    $vtext->setValue($vflUser->name);
				    $vtext= new clspText("txtfirstName", $vresponse);
				    $vtext->setValue($vflUser->firstName);
				    $vtext= new clspText("txtlastName", $vresponse);
				    $vtext->setValue($vflUser->lastName);
                    
                    $vresponse->assign("vemail", "innerHTML", $vflUser->idUser);
                    $vresponse->assign("vuserType", "innerHTML", $vflUser->userType->userType);
                    
                    if ( $vflUser->userType->idUserType!=1 ){
                        $vflEnterpriseUser= new clspFLEnterpriseUser();
                        $vflEnterpriseUser->idUser=$vflUser->idUser;
                        clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser);
                        $vresponse->assign("venterpriseName", "innerHTML", $vflEnterpriseUser->enterprise->enterprise);
                        $vresponse->assign("venterpriseAddress", "innerHTML", "Localidad: " . $vflEnterpriseUser->enterprise->locality .
                                                                              ", Calle: " . $vflEnterpriseUser->enterprise->street . 
                                                                              ", Número: " . $vflEnterpriseUser->enterprise->number .
                                                                              ", " . $vflEnterpriseUser->enterprise->municipality->municipality .
                                                                              ", " . $vflEnterpriseUser->enterprise->municipality->state->state . ".");                                                                              
                        unset($vflEnterpriseUser);
                    }
                    else{
                        $vresponse->assign("venterprise", "innerHTML", "");
                    }
                    unset($vtext);
				    break;
        }
        unset($vflUser);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar su perfil, intente de nuevo");
	}
	
	return $vresponse;
 }

function updateProfileData($vuserForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflUser= new clspFLUser();
		$vflUser->idUser=trim($_SESSION['idUser']);
        $vflUser->name=trim($vuserForm["txtname"]);
        $vflUser->firstName=trim($vuserForm["txtfirstName"]);
        $vflUser->lastName=trim($vuserForm["txtlastName"]);
        switch(clspBLUser::updateInDataBase($vflUser)){
            case 0: $vresponse->alert("Ningún dato se ha modificado de su perfil");
                    break;
            case 1: $vresponse->alert("Los datos de su perfil han sido modificados correctamente");
                    break;
        }
        unset($vflUser);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos de su perfil, intente de nuevo");
	}
	
    unset($vuserForm);
	return $vresponse;
 }

function updateUserPassword($vuserPassword)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflUser= new clspFLUser();
		$vflUser->idUser=trim($_SESSION['idUser']);
        switch(clspBLUser::updatePasswordInDataBase($vflUser, $vuserPassword)){
            case 0: $vresponse->alert("Su password no fué modificado");
                    break;
            case 1: $vresponse->alert("Su password ha sido modificado correctamente");
                    $vresponse->script("vwndwPasswordUpdate.data('kendoWindow').close();");
				 break;
        }
        unset($vflUser);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar su contraseña, intente de nuevo");
	}
    
    unset($vuserPassword);
	return $vresponse;
 }

function verifyUserPassword($vpassword)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflUser= new clspFLUser();
		$vflUser->idUser=trim($_SESSION['idUser']);
        if( clspBLUser::verifyPasswordToDataBase($vflUser, $vpassword)==1 ){
            $vresponse->setReturnValue(1);
        }
        else{
            $vresponse->setReturnValue(0);
        }
        unset($vflUser);
    }
	catch (Exception $vexception){
		$vresponse->setReturnValue(-1);
	}
    
    unset($vpassword);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProfileData");
$vxajax->register(XAJAX_FUNCTION, "updateProfileData");
$vxajax->register(XAJAX_FUNCTION, "updateUserPassword");
$vxajax->register(XAJAX_FUNCTION, "verifyUserPassword");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>