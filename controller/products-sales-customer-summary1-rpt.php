<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotalSalePrice=0;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="ventas-productos-ruta-cantidad";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsSales= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsSales->addPage();
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    
    $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    if ( (int)($_GET["vidRoute"])!=0 ){
        $vfilter.="AND c_route.id_route=" . (int)($_GET["vidRoute"]);;
    }
    $vroutes= new clscFLRoute();
    clscBLRoute::queryToDataBase($vroutes, $vfilter);
    $vroutesTotal=clscBLRoute::total($vroutes);
    for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
        if( $vrptProductsSales->getY()>=640 ){
            $vrptProductsSales->addPage();
            showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
        }
        $vcashTotal=1;
        $vcreditGrantedTotal=0;
        $vchargeTotal=0;
        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter1=$vfilter . "AND p_productsale.id_operationType=1 ";
        $vfilter1.="AND p_productsale.fldcanceled=0";
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryByRouteToDataBase($vproductsSalesDetails, $vfilter1)==1 ){
            $vcashTotal=$vproductsSalesDetails->productsSalesDetails[0]->salePrice;
        }
        $vfilter2=$vfilter . "AND p_productsale.id_operationType=2";
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryByRouteToDataBase($vproductsSalesDetails, $vfilter2)==1 ){
            $vcreditGrantedTotal=$vproductsSalesDetails->productsSalesDetails[0]->salePrice;
        }
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
        $vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.id_operationType=2 ";
        $vfilter.="AND p_productsale.fldcanceled=0";
        $vproductsSalesPayments= new clscFLProductSalePayment();
        if ( clscBLProductSalePayment::queryByRouteToDataBase($vproductsSalesPayments, $vfilter)==1 ){
            $vchargeTotal=$vproductsSalesPayments->productsSalesPayments[0]->amount;
        }
        $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
        $vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
        $vrptProductsSales->x=80;
        $vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
        $vrptProductsSales->y+=20;
        $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total de ventas a Contado:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcashTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total de ventas a Crédito:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcreditGrantedTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total de ventas:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcashTotal + $vcreditGrantedTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total de Cobranzas:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vchargeTotal, 2, ".", ","), 8, "B", "R", 0);
        $vrptProductsSales->y+=15;
        $vrptProductsSales->x=200;
        $vrptProductsSales->printMultiLine(110, 15, "Total por Entregar:" , 8, "B", "R", 0);
        $vrptProductsSales->x=310;
        $vrptProductsSales->printMultiLine(80, 15, "$ " . number_format($vcashTotal + $vchargeTotal, 2, ".", ","), 8, "B", "R", 0);
        
        unset($vproductsSalesDetails);
    }
    $vrptProductsSales->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
          $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
}
catch (Exception $vexception){
    echo "Ocurrió un error al tratar de mostrar el reporte de ventas de productos (entregas)";
}

function showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsSales, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsSales->x=50;	$vrptProductsSales->y=60;
        $vrptProductsSales->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Reporte de Ventas de Productos (Ruta) por Entregas\"" , 10, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Periodo del ". trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]) . "\"" , 8, "B", "C", 0);
        
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=12;
        $vrptProductsSales->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsSales, $vpageNumber)
 {
    try{
        $vrptProductsSales->x=50;  $vrptProductsSales->y=715;
        $vrptProductsSales->printMultiLine(520, 20, "Página Número " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsSales->y=105;
        $vrptProductsSales->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>