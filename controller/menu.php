<?php

require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
date_default_timezone_set('America/Mexico_City');

function getMenuHeader()
 {
	$vuserIdentified=false;
	$vmenu='<ul class="nav-notification clearfix">';
	if( isset($_SESSION['idUser']) ){
	    $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
		$vmenu_content='
						<li class="profile dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<strong>' . $vflUser->name . ' ' . $vflUser->firstName . ' ' . $vflUser->lastName . '</strong>
								<span><i class="fa fa-chevron-down"></i></span>
							</a>
							<ul class="dropdown-menu">
								<li>
									<a class="clearfix" href="#">';
                                        $vavatarSrc="./view/img/user.jpg";
                                        if ( $vflUser->userType->idUserType!=1 ){
                                            $vflEnterpriseUser= new clspFLEnterpriseUser();
                                            $vflEnterpriseUser->idUser=$vflUser->idUser;
                                            clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser);
                                            if ( ($vflEnterpriseUser->enterprise->avatarImage!=null) && (strcmp($vflEnterpriseUser->enterprise->avatarImage, "")!=0) ){
                                                $vavatarSrc='./uploads/images/enterprises/' . $vflEnterpriseUser->enterprise->avatarImage;
                                            }
                                        }
        $vmenu_content.='
										<img src="' . $vavatarSrc . '" alt="Avatar">
										<div class="detail">';
                                            $venterpriseName=$vflUser->userType->userType;
                                            if ( $vflUser->userType->idUserType!=1 ){
                                                $venterpriseName=$vflEnterpriseUser->enterprise->enterprise;
                                                unset($vflEnterpriseUser);
                                            }
        $vmenu_content.='
											<strong>' . $venterpriseName . '</strong>
											<p class="grey">' . $vflUser->idUser. '</p>
										</div>
									</a>
								</li>
								<li><a tabindex="-1" href="./frmmy-profile.php" class="main-link"><i class="fa fa-edit fa-lg"></i>&nbsp;Mi Perfil</a></li>
								<li class="divider"></li>
								<li><a tabindex="-1" class="main-link logoutConfirm_open" href="#logoutConfirm"><i class="fa fa-sign-out"></i>&nbsp;Salir</a></li>
							</ul>
						</li>';
		$vuserIdentified=true;

        unset($vflUser);
	}
	if ( ! $vuserIdentified ){
		$vmenu_content='
						<li class="profile">
							<a href="./frmlogin.php">
								<i class="fa fa-lock fa-lg"></i>&nbsp;<strong>Ingresar</strong>
							</a>
						</li>';
	}
	$vmenu.=$vmenu_content;
	$vmenu.='
					</ul>
';

	echo $vmenu;
    unset($vuserIdentified, $vmenu, $vmenu_content);
 }

function getMenuSideBar()
 {
	$vmenu ='<div class="size-toggle">';
	$vmenu.=getMenuSideBarControls();
	$vmenu.='</div><!-- /size-toggle -->
					';
	$vmenu.='<div class="main-menu">';
	$vmenu.=getMenuSideBarOptions();
	$vmenu.='</div><!-- /main-menu -->
';

	echo $vmenu;
    unset($vmenu);
 }

function getMenuSideBarControls()
 {
	$vmenu='';
	$vmenu_content='
						<a class="btn btn-sm" id="sizeToggle">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</a>';
	if( isset($_SESSION['idUser']) ){
		$vmenu_content.='
						<a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
							<i class="fa fa-power-off"></i>
						</a>
					';
	}
	$vmenu.=$vmenu_content;

    unset($vmenu_content);
	return $vmenu;
 }

function getMenuSideBarOptions()
 {
	$vmenu='
						<ul>';
	$vmenu_content='
							<li id="vmenu-index">
								<a href="./">
									<span class="menu-icon"><i class="fa fa-home fa-lg"></i></span>
									<span class="text">Inicio</span>
									<span class="menu-hover"></span>
								</a>
							</li>';
	if( isset($_SESSION['idUser']) ){
	    $vaccessPrivileges=getAaccessPrivileges();
        if ( count($vaccessPrivileges)>0 ){
            if ( count($vaccessPrivileges[0])>0 ){
                $vmenu_content.='
							<li id="vmenu-catalogs" class="openable">
								<a href="#">
									<span class="menu-icon"><i class="fa fa-folder-open fa-lg"></i></span>
									<span class="text">Catálogos</span>
									<span class="menu-hover"></span>
								</a>
								<ul class="submenu">';
                if ( $vaccessPrivileges[0][0][0]==1){
                    $vmenu_content.='
									<li id="vmenu-customers"><a href="./frmcustomers.php"><span class="submenu-label">Clientes</span></a></li>';
                }
                if ( $vaccessPrivileges[0][1][0]==1){
                    $vmenu_content.='
									<li id="vmenu-providers"><a href="./frmproviders.php"><span class="submenu-label">Proveedores</span></a></li>';
                }
                if ( $vaccessPrivileges[0][2][0]==1){
                    $vmenu_content.='
									<li id="vmenu-deliverers"><a href="./frmdeliverers.php"><span class="submenu-label">Repartidores</span></a></li>';
                }
                if ( $vaccessPrivileges[0][3][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products"><a href="./frmproducts.php"><span class="submenu-label">Productos</span></a></li>';
                }
                if ( $vaccessPrivileges[0][4][0]==1){
                    $vmenu_content.='
									<li id="vmenu-routes"><a href="./frmroutes.php"><span class="submenu-label">Rutas</span></a></li>';
                }
                if ( $vaccessPrivileges[0][5][0]==1){
                    $vmenu_content.='
									<li id="vmenu-expenses"><a href="./frmexpenses.php"><span class="submenu-label">Gastos</span></a></li>';
                }
                if ( $vaccessPrivileges[0][6][0]==1){
                    $vmenu_content.='
									<li id="vmenu-banks"><a href="./frmbanks.php"><span class="submenu-label">Bancos</span></a></li>';
                }
                if ( $vaccessPrivileges[0][7][0]==1){
                    $vmenu_content.='
									<li id="vmenu-cash"><a href="./frmcash.php"><span class="submenu-label">Cajas</span></a></li>';
                }
                if ( $vaccessPrivileges[0][8][0]==1){
                    $vmenu_content.='
									<li id="vmenu-others" class="openable">
										<a href="#">
											<span class="submenu-label">Otros</span>
										</a>
										<ul class="submenu third-level">';
                    if ( $vaccessPrivileges[0][8][1]==1){
                        $vmenu_content.='
											<li id="vmenu-others-expensesTypes"><a href="./frmexpenses-types.php"><span class="submenu-label">Tipos de Gastos</span></a></li>';
                    }
                    if ( $vaccessPrivileges[0][8][2]==1){
                        $vmenu_content.='
											<li id="vmenu-others-productsTypes"><a href="./frmproducts-types.php"><span class="submenu-label">Tipos de Productos</span></a></li>';
                    }
                    $vmenu_content.='
										</ul>
									</li>';
                }
                $vmenu_content.='
								</ul>
							</li>';
            }
            if ( count($vaccessPrivileges[1])>0 ){
                $vmenu_content.='
							<li id="vmenu-movements" class="openable">
								<a href="#">
									<span class="menu-icon"><i class="fa fa-cogs fa-lg"></i></span>
									<span class="text">Movimientos</span>
									<span class="menu-hover"></span>
								</a>
								<ul class="submenu">';
                if ( $vaccessPrivileges[1][0][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-entry"><a href="./frmproducts-entry.php"><span class="submenu-label">Entrada de Productos</span></a></li>';
                }
                if ( $vaccessPrivileges[1][1][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-distribution" class="openable">
										<a href="#">
											<span class="submenu-label">Distribución de Productos</span>
										</a>
										<ul class="submenu third-level">';
                    if ( $vaccessPrivileges[1][1][1]==1){
                        $vmenu_content.='
											<li id="vmenu-products-distribution-record"><a href="./frmproducts-distribution-record.php"><span class="submenu-label">Registros</span></a></li>';
                    }
                    if ( $vaccessPrivileges[1][1][2]==1){
                        $vmenu_content.='
											<li id="vmenu-products-distribution-cutting"><a href="./frmproducts-distribution-cutting.php"><span class="submenu-label">Cortes</span></a></li>';
                    }
                    $vmenu_content.='
										</ul>
									</li>';
                }
                if ( $vaccessPrivileges[1][2][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-sale"><a href="./frmproducts-sale.php"><span class="submenu-label">Venta de Productos</span></a></li>';
                }
                if ( $vaccessPrivileges[1][3][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-prices"><a href="./frmproducts-prices.php"><span class="submenu-label">Precios de Productos</span></a></li>';
                }
                if ( $vaccessPrivileges[1][4][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-low"><a href="./frmproducts-low.php"><span class="submenu-label">Baja de Productos</span></a></li>';
                }
                $vmenu_content.='
								</ul>
							</li>';
            }
            if ( count($vaccessPrivileges[2])>0 ){
                $vmenu_content.='
							<li id="vmenu-records" class="openable">
								<a href="#">
									<span class="menu-icon"><i class="fa fa-briefcase fa-lg"></i></span>
									<span class="text">Registros</span>
									<span class="menu-hover"></span>
								</a>
								<ul class="submenu">';
                if ( $vaccessPrivileges[2][0][0]==1){
                    $vmenu_content.='
									<li id="vmenu-payments-record"><a href="./frmpayments-record.php"><span class="submenu-label">Pagos</span></a></li>';
                }
                if ( $vaccessPrivileges[2][1][0]==1){
                    $vmenu_content.='
									<li id="vmenu-expenses-record"><a href="./frmexpenses-record.php"><span class="submenu-label">Gastos</span></a></li>';
                }
                if ( $vaccessPrivileges[2][2][0]==1){
                    $vmenu_content.='
									<li id="vmenu-loans-record"><a href="./frmloans-record.php"><span class="submenu-label">Prestamos</span></a></li>';
                }
                if ( $vaccessPrivileges[2][3][0]==1){
                    $vmenu_content.='
									<li id="vmenu-banks-deposit"><a href="./frmbanks-deposit.php"><span class="submenu-label">Depósitos Bancarios</span></a></li>';
                }
                if ( $vaccessPrivileges[2][4][0]==1){
                    $vmenu_content.='
									<li id="vmenu-cash-count"><a href="./frmcash-count.php"><span class="submenu-label">Arqueos de Caja</span></a></li>';
                }
                if ( $vaccessPrivileges[2][5][0]==1){
                    $vmenu_content.='
									<li id="vmenu-cash-open-close"><a href="./frmcash-open-close.php"><span class="submenu-label">Apertura/Cierre de Caja</span></a></li>';
                }
                if ( $vaccessPrivileges[2][6][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-return"><a href="./frmproducts-return.php"><span class="submenu-label">Devoluciones</span></a></li>';
                }
                $vmenu_content.='
								</ul>
							</li>';
            }
            if ( count($vaccessPrivileges[3])>0 ){
                $vmenu_content.='
							<li id="vmenu-reports" class="openable">
								<a href="#">
									<span class="menu-icon"><i class="fa fa-book fa-lg"></i></span>
									<span class="text">Reportes</span>
									<span class="menu-hover"></span>
								</a>
								<ul class="submenu">';
                if ( $vaccessPrivileges[3][0][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-stock"><a href="./controller/products-stock-rpt.php" target="_blank"><span class="submenu-label">Existencia de Productos</span></a></li>';
                }
                if ( $vaccessPrivileges[3][1][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-low-report"><a href="./frmproducts-low-report.php"><span class="submenu-label">Baja de Productos</span></a></li>';
                }
                if ( $vaccessPrivileges[3][2][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-sales" class="openable">
										<a href="#">
											<span class="submenu-label">Ventas de Productos</span>
										</a>
										<ul class="submenu third-level">';
                    if ( $vaccessPrivileges[3][2][1]==1){
                        $vmenu_content.='
											<li id="vmenu-products-sales-route"><a href="./frmproducts-sales-route.php"><span class="submenu-label">Por rutas</span></a></li>';
                    }

                    if ( $vaccessPrivileges[3][2][1]==1){
                        $vmenu_content.='
											<li id="vmenu-products-sales-cash"><a href="./frm-cash-income-report.php"><span class="submenu-label">Por cliente</span></a></li>';
                    }

                    if ( $vaccessPrivileges[3][2][1]==1){
                        $vmenu_content.='
                      <li id="vmenu-products-sales-cash"><a href="./frmproducts-sales-product.php"><span class="submenu-label">Por producto</span></a></li>';
                    }

                    $vmenu_content.='
										</ul>
									</li>';
                }
                if ( $vaccessPrivileges[3][3][0]==1){
                    $vmenu_content.='
									<li id="vmenu-products-sales-profits"><a href="./frmproducts-sales-profits.php"><span class="submenu-label">Utilidad de Ventas</span></a></li>';
                }
                if ( $vaccessPrivileges[3][4][0]==1){
                    $vmenu_content.='
									<li id="vmenu-income-expenses"><a href="./frmincome-expenses.php"><span class="submenu-label">Ingresos-Egresos</span></a></li>';
                }
                if ( $vaccessPrivileges[3][5][0]==1){
                    $vmenu_content.='
									<li id="vmenu-payments-report"><a href="./frmpayments-report.php"><span class="submenu-label">Pagos</span></a></li>';
                }
                if ( $vaccessPrivileges[3][6][0]==1){
                    $vmenu_content.='
									<li id="vmenu-banks-deposit-report"><a href="./frmbanks-deposit-report.php"><span class="submenu-label">Depósitos Bancarios</span></a></li>';
                }
                if ( $vaccessPrivileges[3][7][0]==1){
                    $vmenu_content.='
									<li id="vmenu-cash-count-report"><a href="./frmcash-count-report.php"><span class="submenu-label">Arqueos de Caja</span></a></li>';
                }

                if ( $vaccessPrivileges[3][3][0]==1){
                    $vmenu_content.='
									<li id="vmenu-route-sale-chart"><a target="_blank" href="./frmroutes-chart.php"><span class="submenu-label">Grafico de ventas por rutas</span></a></li>';
                }
                $vmenu_content.='
								</ul>
							</li>';
            }
        }

        unset($vaccessPrivileges);
    }
	$vmenu.=$vmenu_content;
	$vmenu.='
						</ul>
					';

    unset($vmenu_content);
	return $vmenu;
 }

function getAaccessPrivileges()
 {

    $vaccessPrivileges=array();

    $vflUser= new clspFLUser();
    $vflUser->idUser=trim($_SESSION['idUser']);
    if ( clspBLUser::queryToDataBase($vflUser)==1 ){
        $vcatalogs=array();
        $vmovements=array();
        $vrecords=array();
        $vreports=array();
        switch ($vflUser->userType->idUserType){

        	case 0: array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 1, 1));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 1, 1));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 1, 1));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;


            case 1: array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 1, 1));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 1, 1));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 1, 1));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;            
            case 2: array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 1, 1));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 1, 1));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 1, 1));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;
            case 3:
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vaccessPrivileges, $vreports);
                    break;
            case 4: array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(1, 1, 1));
                    array_push($vmovements, array(1, 1, 1));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;

            case 5: array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 1, 0));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(1, 1, 1));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(1, 1, 1));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;
            case 6: array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vcatalogs, array(1, 0, 0));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(0, 1, 1));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 1, 1));
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vrecords, array(1, 1, 1));
                    array_push($vrecords, array(1, 1, 1));
                    array_push($vrecords, array(1, 1, 1));
                    array_push($vrecords, array(1, 1, 1));
                    array_push($vrecords, array(0, 1, 1));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;
            case 7: array_push($vcatalogs, array(1, 1, 1));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vcatalogs, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vcatalogs);
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vaccessPrivileges, $vreports);
                    break;



            case 8: 
                    array_push($vaccessPrivileges, $vcatalogs);



                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(1, 0, 0));
                    array_push($vmovements, array(0, 0, 0));
                    array_push($vmovements, array(0, 0, 0));


                    array_push($vaccessPrivileges, $vmovements);



                    array_push($vrecords, array(1, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));
                    array_push($vrecords, array(0, 0, 0));

                    array_push($vaccessPrivileges, $vrecords);

                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(1, 1, 1));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));


                    array_push($vaccessPrivileges, $vreports);
                    break;


case 9:
                    array_push($vaccessPrivileges, $vcatalogs);                
                    array_push($vaccessPrivileges, $vmovements);
                    array_push($vaccessPrivileges, $vrecords);
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(1, 1, 1));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(1, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vreports, array(0, 0, 0));
                    array_push($vaccessPrivileges, $vreports);
                    break;

        }

        unset($vcatalogs, $vmovements, $vrecords, $vreports);
    }

    unset($vflUser);
    return $vaccessPrivileges;
 }

?>
