<?php

ini_set('max_execution_time', 300); //300 seconds = 5 minutes


require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
date_default_timezone_set('America/Mexico_City');

    $lista = array();
    $listaRutas = array();
    $listaFechaInicio = array();
    $listaVentaProductos = array();
    $distribucion = false;

    $totalAlmacen = 0;
    $totalRuta = 0;

    $listaDistribuidos = array();
    $listaVendidos = array();

    $fechaInicio;



    //Obtener lista de distribuciones
    try{
        $vfilter ="WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        //$vfilter.="AND (p_productdistribution.fldproductDistributionDate BETWEEN '" . date("Y-m-d", strtotime(trim(date("Y-m-d"))))  . "' ";
        //$vfilter.="AND '" . date("Y-m-d", strtotime(trim(date("Y-m-d")))) . "') ";
        $vfilter .= "AND p_productdistribution.fldproductDistributionCuttingDate IS NULL";
        $vproductsDistributions= new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal=clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber=0; $vproductsDistributionNumber<$vproductsDistributionsTotal; $vproductsDistributionNumber++){

           $lista[] = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
           $listaRutas[] = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->routeDealer->route->route;
           $listaFechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;
        }
        if ( $vproductsDistributionNumber>=1 ){
            $distribucion = true;
        }
        else{
            $distribucion = false;
        }
        unset($vfilter, $vproductsDistributions, $vproductsDistributionsTotal, $vproductsDistributionNumber, $vdata);
    }
    catch (Exception $vexception){

    }

    unset($vfilterForm);






//obtener productos distribuidos
if($distribucion == true){
for($i=0;$i<count($lista);$i++){

try{

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);

    $vfilter ="WHERE p_productdistributiondetail.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND p_productdistributiondetail.id_productDistribution='" . $lista[$i] . "'";
    $vproductsDistributionsDetails= new clscFLProductDistributionDetail();
    clscBLProductDistributionDetail::queryToDataBase($vproductsDistributionsDetails, $vfilter, 1);
    $vdistributionProductsTotal=clscBLProductDistributionDetail::total($vproductsDistributionsDetails);
    for ($vproductNumber=0; $vproductNumber<$vdistributionProductsTotal; $vproductNumber++){

        $nombre = $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->product->name;
        $idVendido = $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->product->idProduct;
        $vdistributionAmount=$vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->distributionAmount;
        $cantidad = $vdistributionAmount - $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->returnedAmount;
        $cantidad = $vdistributionAmount;


        if (array_key_exists($nombre,$listaDistribuidos))
        {
            $listaDistribuidos[$nombre] += $cantidad;
        }
        else{
            $listaDistribuidos[$nombre] = $cantidad;
        }

        if (array_key_exists($nombre,$listaVentaProductos))
        {
            $listaVentaProductos[$nombre] += localStock($idVendido,$listaRutas[$i],$lista[$i]);
        }
        else{
            $listaVentaProductos[$nombre] = localStock($idVendido,$listaRutas[$i],$lista[$i]);
        }
        unset($vdistributionAmount, $vdistributedAmount);
    }
    if( $vdistributionProductsTotal>0 ){

    }

}
catch (Exception $vexception){
}
}
}





try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $totalPrice = 0;
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="existencia-productos";
    $vorientation="L";
    $vpageSize="Letter";
    $vrptProductsStock= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsStock->addPage();

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    $vfilter ="WHERE c_product.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    showPage($vrptProductsStock, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    $vproducts= new clscFLProduct();
    clscBLProduct::queryToDataBase($vproducts, $vfilter, 1);
    $vproductsTotal=clscBLProduct::total($vproducts);
    for ($vproductNumber=0; $vproductNumber<$vproductsTotal; $vproductNumber++){
        if( $vrptProductsStock->getY()>=460 ){
            $vrptProductsStock->addPage();
            showPage($vrptProductsStock, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if( ($vproductNumber==0) || ($vpageNew) ){
            $vrptProductsStock->x=50;   $vrptProductsStock->y+=30;
            $vrptProductsStock->printMultiLine(70, 20, "Código" , 8, "B", "C", 1);

            $vrptProductsStock->x=120;
            $vrptProductsStock->printMultiLine(200, 20, "Producto" , 8, "B", "C", 1);

            $vrptProductsStock->x=320;
            $vrptProductsStock->printMultiLine(70, 20, "Almacen (Pz)" , 8, "B", "C", 1);
            $vrptProductsStock->x=390;
            $vrptProductsStock->printMultiLine(70, 20, "Almacen (Kg)" , 8, "B", "C", 1);
            $vrptProductsStock->x=460;
            $vrptProductsStock->printMultiLine(70, 20, "En ruta (pz)" , 8, "B", "C", 1);
            $vrptProductsStock->x=530;
            $vrptProductsStock->printMultiLine(70, 20, "En ruta (Kg)" , 8, "B", "C", 1);
            $vrptProductsStock->x=600;
            $vrptProductsStock->printMultiLine(70, 20, "Precio (pz)" , 8, "B", "C", 1);
            $vrptProductsStock->x=670;
            $vrptProductsStock->printMultiLine(120, 20, "Total Almacen/Ruta" , 8, "B", "C", 1);
            $vpageNew=false;
        }

	

        

	if($vproducts->products[$vproductNumber]->stock == 0 && asd ==0)
	{
		continue;
	}
        $vrptProductsStock->x=50;   $vrptProductsStock->y+=20;
                $vrptProductsStock->printMultiLine(70, 20, $vproducts->products[$vproductNumber]->key , 7, "", "C", 1);
        $vrptProductsStock->x=120;
        $vrptProductsStock->printMultiLine(200, 20,strtoupper($vproducts->products[$vproductNumber]->name) , 7, "", "L", 1);


        if (array_key_exists($vproducts->products[$vproductNumber]->name,$listaDistribuidos))
        {
        $vrptProductsStock->x=320;
        $vrptProductsStock->printMultiLine(70, 20, $vproducts->products[$vproductNumber]->stock - $listaDistribuidos[$vproducts->products[$vproductNumber]->name]+$listaVentaProductos[$vproducts->products[$vproductNumber]->name], 7, "", "C", 1);
        }
        else{
        $vrptProductsStock->x=320;
        $vrptProductsStock->printMultiLine(70, 20,$vproducts->products[$vproductNumber]->stock, 7, "", "C", 1);
        }


        if (array_key_exists($vproducts->products[$vproductNumber]->name,$listaDistribuidos))
        {
        $vrptProductsStock->x=390;
        if ($vproducts->products[$vproductNumber]->stock == 0) {
          $vrptProductsStock->printMultiLine(70, 20,0, 7, "", "C", 1);
        }else{
          $vrptProductsStock->printMultiLine(70, 20,$vproducts->products[$vproductNumber]->stockInKg - (($vproducts->products[$vproductNumber]->stockInKg/$vproducts->products[$vproductNumber]->stock)*($listaDistribuidos[$vproducts->products[$vproductNumber]->name])) + (($vproducts->products[$vproductNumber]->stockInKg/$vproducts->products[$vproductNumber]->stock)*($listaVentaProductos[$vproducts->products[$vproductNumber]->name])), 7, "", "C", 1);
        }
        }
        else{
        $vrptProductsStock->x=390;
        $vrptProductsStock->printMultiLine(70, 20, $vproducts->products[$vproductNumber]->stockInKg, 7, "", "C", 1);
        }









        if (array_key_exists($vproducts->products[$vproductNumber]->name,$listaDistribuidos))
        {
        $vrptProductsStock->x=460;
        $vrptProductsStock->printMultiLine(70, 20,$listaDistribuidos[$vproducts->products[$vproductNumber]->name] - $listaVentaProductos[$vproducts->products[$vproductNumber]->name], 7, "", "C", 1);
        }
        else{
        $vrptProductsStock->x=460;
        $vrptProductsStock->printMultiLine(70, 20, 0, 7, "", "C", 1);
        }


        if (array_key_exists($vproducts->products[$vproductNumber]->name,$listaDistribuidos))
        {
        $vrptProductsStock->x=530;

        if ($vproducts->products[$vproductNumber]->stock == 0) {
          $vrptProductsStock->printMultiLine(70, 20,0, 7, "", "C", 1);
        }else{
          $vrptProductsStock->printMultiLine(70, 20,(($vproducts->products[$vproductNumber]->stockInKg/$vproducts->products[$vproductNumber]->stock)*($listaDistribuidos[$vproducts->products[$vproductNumber]->name])) - (($vproducts->products[$vproductNumber]->stockInKg/$vproducts->products[$vproductNumber]->stock)*($listaVentaProductos[$vproducts->products[$vproductNumber]->name])), 7, "", "C", 1);
        }

        }
        else{
        $vrptProductsStock->x=530;
        $vrptProductsStock->printMultiLine(70, 20, 0, 7, "", "C", 1);
        }

        $vrptProductsStock->x=600;
        $vrptProductsStock->printMultiLine(70, 20, number_format($vproducts->products[$vproductNumber]->purchasePrice, 2, ".", ","), 7, "", "C", 1);
        $vrptProductsStock->x=670;




        if (array_key_exists($vproducts->products[$vproductNumber]->name,$listaDistribuidos))
        {
        $vrptProductsStock->x=670;
        $vrptProductsStock->printMultiLine(60, 20, number_format($vproducts->products[$vproductNumber]->purchasePrice*($vproducts->products[$vproductNumber]->stock - $listaDistribuidos[$vproducts->products[$vproductNumber]->name]+$listaVentaProductos[$vproducts->products[$vproductNumber]->name]),2,".",","), 7, "", "C", 1);
        $totalAlmacen+=$vproducts->products[$vproductNumber]->purchasePrice*($vproducts->products[$vproductNumber]->stock - $listaDistribuidos[$vproducts->products[$vproductNumber]->name]+$listaVentaProductos[$vproducts->products[$vproductNumber]->name]);
        }
        else{
        $vrptProductsStock->x=670;
        $vrptProductsStock->printMultiLine(60, 20,number_format($vproducts->products[$vproductNumber]->purchasePrice*($vproducts->products[$vproductNumber]->stock),2,".",","), 7, "", "C", 1);
        $totalAlmacen+=$vproducts->products[$vproductNumber]->purchasePrice*($vproducts->products[$vproductNumber]->stock);
        }

        if (array_key_exists($vproducts->products[$vproductNumber]->name,$listaDistribuidos))
        {
        $vrptProductsStock->x=730;
        $vrptProductsStock->printMultiLine(60, 20, number_format($vproducts->products[$vproductNumber]->purchasePrice*($listaDistribuidos[$vproducts->products[$vproductNumber]->name]-$listaVentaProductos[$vproducts->products[$vproductNumber]->name]),2,".",","), 7, "", "C", 1);
        $totalRuta+=$vproducts->products[$vproductNumber]->purchasePrice*($listaDistribuidos[$vproducts->products[$vproductNumber]->name]-$listaVentaProductos[$vproducts->products[$vproductNumber]->name]);
        }
        else{
        $vrptProductsStock->x=730;
        $vrptProductsStock->printMultiLine(60, 20,number_format($vproducts->products[$vproductNumber]->purchasePrice*(0),2,".",","), 7, "", "C", 1);
        $totalRuta+=(0);
        }



        $totalPrice += $vproducts->products[$vproductNumber]->purchasePrice * $vproducts->products[$vproductNumber]->stock;

    }
    if( $vrptProductsStock->getY()>=460 ){
            $vrptProductsStock->addPage();
            showPage($vrptProductsStock, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vrptProductsStock->x=50;
            $vrptProductsStock->y+=20;
            $vrptProductsStock->printMultiLine(550, 20, "Total Almacen / Ruta ", 8, "B" , "C", 1);
            $vrptProductsStock->x=600;
            $vrptProductsStock->printMultiLine(95, 20, number_format($totalAlmacen,2,".",","),7,"","C",1);
            $vrptProductsStock->x=695;
            $vrptProductsStock->printMultiLine(95, 20, number_format($totalRuta,2,".",","),7,"","C",1);
            $vrptProductsStock->x=50;
            $vrptProductsStock->y+=20;
            $vrptProductsStock->printMultiLine(550, 20, "Precio total del inventario" , 8, "B", "C", 1);
            $vrptProductsStock->x=600;
            $vrptProductsStock->printMultiLine(190, 20, number_format($totalPrice, 2, ".", ","), 7, "", "C", 1);
            $vpageNew=true;
        }
        else{


            $vrptProductsStock->x=50;
            $vrptProductsStock->y+=20;
            $vrptProductsStock->printMultiLine(550, 20, "Total Almacen / Ruta ", 8, "B" , "C", 1);
            $vrptProductsStock->x=600;
            $vrptProductsStock->printMultiLine(95, 20, number_format($totalAlmacen,2,".",","),7,"","C",1);
            $vrptProductsStock->x=695;
            $vrptProductsStock->printMultiLine(95, 20, number_format($totalRuta,2,".",","),7,"","C",1);

            $vrptProductsStock->x=50;
            $vrptProductsStock->y+=20;
            $vrptProductsStock->printMultiLine(550, 20, "Precio total del inventario" , 8, "B", "C", 1);
            $vrptProductsStock->x=600;
            $vrptProductsStock->printMultiLine(190, 20, number_format($totalPrice, 2, ".", ","), 7, "B", "C", 1);
            $vpageNew=false;
        }
    $vrptProductsStock->showPage();

    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsStock, $vflEnterpriseUser, $vfilter,
          $vproducts, $vproductsTotal, $vproductNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de existencia de productos";
}

function showPage($vrptProductsStock, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsStock, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsStock, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsStock, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsStock->x=50;	$vrptProductsStock->y=50;
        $vrptProductsStock->printMultiLine(700, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsStock->x=50;	$vrptProductsStock->y+=14;
        $vrptProductsStock->printMultiLine(720, 15, "\"Reporte de Existencia de Productos\"" , 10, "B", "C", 0);
        $vrptProductsStock->x=50;	$vrptProductsStock->y+=12;
        $vrptProductsStock->printMultiLine(700, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }



 }

function showFooter($vrptProductsStock, $vpageNumber)
 {
    try{
        $vrptProductsStock->x=50;  $vrptProductsStock->y=535;
        $vrptProductsStock->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsStock->y=86;
        $vrptProductsStock->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }



function localStock($product_id,$nombre_ruta,$folio){

    $vproductStock = 0;
    $vroute = 0;
    $idcaja = 0;
    $folio_dist;
    $fechaInicio;
    $fechaFin = NULL;

try {

        //$vfilter="WHERE c_cash.id_enterprise='" . $_SESSION['idEnterprise']."' ";
        //$vfilter.="AND c_cash.fldcash=".$caja."' ";
        //$vfilter.="AND c_cash.fldcash=" . $caja;

        $vfilter ="WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_cash.fldcash='" . strval($nombre_ruta) . "'";
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal=clscBLCash::total($vcash);
        for ($vi=0; $vi<$vcashTotal; $vi++){
                    $idcaja = $vcash->cash[$vi]->idCash;
        }

        $vroutes= new clscFLRoute();
        $vroute;
        $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_route.fldroute='" . strval($nombre_ruta) . "'";
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal=clscBLRoute::total($vroutes);
        for ($vi=0; $vi<$vroutesTotal; $vi++){
                    $vroute = $vroutes->routes[$vi]->idRoute;
        }



        $vfilter ="WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter.="AND p_productdistribution.id_productDistribution='".$folio."'";
        //$vfilter.="ORDER BY p_productdistribution.idProductDistribution";

        $vproductsDistributions= new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal=clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber=0; $vproductsDistributionNumber<$vproductsDistributionsTotal; $vproductsDistributionNumber++){
            if($folio == $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution){
            $folio_dist = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
            $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;

            if($vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate != NULL && $vproductsDistributionNumber < $vproductsDistributionsTotal-1){
                $fechaFin = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber + 1]->CreatedOn;
            }

            }







        }

        if($vproductsDistributionNumber == 0){
                $vresponse->alert("No hay distribucion para esta ruta.");
        }



        $vfilter ="WHERE p_productsale.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        //$vfilter.="AND p_productsale.id_user='" . $_SESSION['idUser'] . "' ";
        //$vfilter.="AND p_productsale.id_cash=" . $vcashMovement . " ";
        $vfilter.="AND p_productsale.id_cash=" . $idcaja . " ";
        $vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";

        if($fechaFin != NULL){
        $vfilter.="AND p_productsale.CreatedOn < '" .$fechaFin."' ";
        }
        $vfilter.="AND p_productsale.fldcanceled = 0";

        $vproductsSales= new clscFLProductSale();
        clscBLProductSale::queryToDataBase($vproductsSales, $vfilter);
        $vproductsSalesTotal=clscBLProductSale::total($vproductsSales);

        for ($vproductsSaleNumber=0; $vproductsSaleNumber<$vproductsSalesTotal; $vproductsSaleNumber++){

            $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION['idEnterprise']. " ";
             $vfilter.="AND p_productsaledetail.id_productSale='" . $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . "'";
                    $vproductsSalesDetails= new clscFLProductSaleDetail();
                    clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
                    $vsaleProductsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
                    $vproductsSale=array();
                    for($vi=0; $vi<$vsaleProductsTotal; $vi++){

                        if($product_id == $vproductsSalesDetails->productsSalesDetails[$vi]->product->idProduct){

                            $vproductStock+=$vproductsSalesDetails->productsSalesDetails[$vi]->saleAmount;

                        }
                    }


        }






} catch (Exception $vexception) {
}
    $stock = $vproductStock;
    return $stock;
}





?>
