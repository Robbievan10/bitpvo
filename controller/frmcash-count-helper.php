<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
date_default_timezone_set('America/Mexico_City');

    $vresponse= new xajaxResponse();

        //------------------------------------------------------Ventas de Cajas---------------------------------------------
    $vinitialCashByCash=0;
    $vcashSaleAmountByCash=0;
    $vcreditSaleAmountByCash=0;
    $vfilter ="WHERE c_routecustomer.id_route IS NULL ";
    $vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
    $vfilter.="AND p_productsale.fldcanceled=0";
    $vproductsSalesDetails= new clscFLProductSaleDetail();
    clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsSalesDetails, $vfilter, 1);
    $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);


        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_cashmovement.id_cash= " . $_GET["idCaja"] . " ";
        $vinitialCash=0;
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vcashMovements, $vfilter) ){
            $vinitialCash=$vcashMovements->cashMovements[0]->openAmount;
            $vinitialCashByCash+=$vinitialCash;
        }

        
        $vfilter ="WHERE c_routecustomer.id_route IS NULL ";
        $vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.id_cash= " . $_GET["idCaja"] . " ";
        $vfilter.="AND p_productsale.id_operationType=1 ";
        $vfilter.="AND p_productsale.fldcanceled=0";
        $vcashSaleAmount=0;
        $vproductsCashSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter) ){
            $vcashSaleAmount=$vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
            $vcashSaleAmountByCash+=$vcashSaleAmount;
        }

        
        $vfilter ="WHERE c_routecustomer.id_route IS NULL ";
        $vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.id_cash= " . $_GET["idCaja"]. " ";
        $vfilter.="AND p_productsale.id_operationType=2 ";
        $vfilter.="AND p_productsale.fldcanceled=0";
        $vcreditSaleAmount=0;
        $vproductsCashSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter) ){
            $vcreditSaleAmount=$vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
            $vcreditSaleAmountByCash+=$vcreditSaleAmount;    
        }

        
        unset($vresponsibleName, $vfilter, $vinitialCash, $vcashMovements, $vcashSaleAmount, $vproductsCashSalesDetails, $vcreditSaleAmount);
    
    if( $vproductsSalesDetailsTotal>0 ){


    }   

    return $vcashSaleAmountByCash;

    

    
?>