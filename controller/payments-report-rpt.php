<?php
date_default_timezone_set('America/Mexico_City');

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotalAmount=0;
    $vpaymentAmountTotal=0;
    $vidPriorSource="";
    $vidPriorDealer=0;
    $vidSource;
    $fechaInicial;
    $fechaFinal;
    $totalCompra=0;
    $totalAbonos=0;
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="pagos";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptPaymentsReport= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptPaymentsReport->addPage();
    $folioAnterior;
    $mismoFolio = false;
    $pagoAnterior;

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    $nombreCliente;




   if ( $_GET["vpaymentType"]==2 ){

        try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_individualcustomer.id_customer='".$_GET["vidListData"]."' ";
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);
        if($vindividualsCustomersTotal>0){
               $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){
                    $nombreCliente = $vindividualsCustomers->individualsCustomers[$vi]->name . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->firstName . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName;

                    $nombreCliente = strtoupper($nombreCliente);
        }
        }

        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_businesscustomer.id_customer='".$_GET["vidListData"]."'";
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);

       if($vbusinessCustomersTotal>0){
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){

                    $nombreCliente = $vbusinessCustomers->businessCustomers[$vi]->businessName;
                    $nombreCliente = strtoupper($nombreCliente);
        }
       }

    }
    catch (Exception $vexception){
        //$vresponse->alert("Ocurri� un error al tratar de listar los clientes, intente de nuevo");
        //$nombreCliente = $vexception;
    }

       showPage($vrptPaymentsReport, $vflEnterpriseUser, $vdate, ++$vpageNumber, $nombreCliente);
   }else{
       showPage($vrptPaymentsReport, $vflEnterpriseUser, $vdate, ++$vpageNumber);
   }





    $fechaInicial = date("Y-m-d", strtotime(trim($_GET["vstartDate"])));
    $fechaFinal = date("Y-m-d", strtotime(trim($_GET["vendDate"])));
    if ( $_GET["vpaymentType"]==1 ){
        $vfilter ="WHERE p_productentrypayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        if ( (int)($_GET["vidListData"])!=0 ){
            $vfilter.="AND p_productentry.id_provider=" . (int)($_GET["vidListData"]) . " ";
        }
        $vpayments= new clscFLProductEntryPayment();
        clscBLProductEntryPayment::queryToDataBase($vpayments, $vfilter);
        $vpaymentsTotal=clscBLProductEntryPayment::total($vpayments);
    }
    else if ( $_GET["vpaymentType"]==2 ){
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        if ( (int)($_GET["vidListData"])!=0 ){
            $vfilter.="AND p_productsale.id_customer=" . (int)($_GET["vidListData"]) . " ";
        }
        $vfilter.="AND p_productsale.id_operationType=2";
		$vpayments= new clscFLProductSalePayment();
		clscBLProductSalePayment::queryToDataBase($vpayments, $vfilter);
        $vpaymentsTotal=clscBLProductSalePayment::total($vpayments);
    }
    else{
        $vfilter ="WHERE p_loanpayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        if ( (int)($_GET["vidListData"])!=0 ){
            $vfilter.="AND p_loan.id_dealer=" . (int)($_GET["vidListData"]) . " ";
        }
		$vpayments= new clscFLLoanPayment();
        clscBLLoanPayment::queryToDataBase($vpayments, $vfilter, 1);
        $vpaymentsTotal=clscBLLoanPayment::total($vpayments);
    }

    for ($vpaymentNumber=0; $vpaymentNumber<$vpaymentsTotal; $vpaymentNumber++){
        if( $vrptPaymentsReport->getY()>=640 ){
            $vrptPaymentsReport->addPage();
            showPage($vrptPaymentsReport, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if ( $_GET["vpaymentType"]==1 ){
            $vpaymentDate=$vpayments->productsEntriesPayments[$vpaymentNumber]->paymentDate;
            $vpaymentAmount=$vpayments->productsEntriesPayments[$vpaymentNumber]->amount;
            $vlabel1="Proveedor:";
            if ( $vpayments->productsEntriesPayments[$vpaymentNumber]->productEntry->provider->personType->idPersonType==1 ){
                $vflProvider= new clspFLIndividualProvider();
                $vflProvider->enterprise->idEnterprise=$vpayments->productsEntriesPayments[$vpaymentNumber]->enterprise->idEnterprise;
                $vflProvider->idProvider=$vpayments->productsEntriesPayments[$vpaymentNumber]->productEntry->provider->idProvider;
                clspBLIndividualProvider::queryToDataBase($vflProvider, 1);
                $vlabel2=$vflProvider->name . ' ' . $vflProvider->firstName . ' ' . $vflProvider->lastName;
            }
            else{
                $vflProvider= new clspFLBusinessProvider();
                $vflProvider->enterprise->idEnterprise=$vpayments->productsEntriesPayments[$vpaymentNumber]->enterprise->idEnterprise;
                $vflProvider->idProvider=$vpayments->productsEntriesPayments[$vpaymentNumber]->productEntry->provider->idProvider;
                clspBLBusinessProvider::queryToDataBase($vflProvider, 1);
                $vlabel2=$vflProvider->businessName;
            }
            $vlabel3="Entrada de Producto:";
            $vidSource=$vpayments->productsEntriesPayments[$vpaymentNumber]->productEntry->idProductEntry;

            $vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vpayments->productsEntriesPayments[$vpaymentNumber]->enterprise->idEnterprise . " ";
            $vfilter.="AND p_productentrydetail.id_productEntry='" . $vpayments->productsEntriesPayments[$vpaymentNumber]->productEntry->idProductEntry . "'";
            $vproductsEntriesDetails= new clscFLProductEntryDetail();
            clscBLProductEntryDetail::queryToDataBase($vproductsEntriesDetails, $vfilter);
            $vglobal=clscBLProductEntryDetail::totalPrice($vproductsEntriesDetails);
        }
        else if ( $_GET["vpaymentType"]==2 ){
            $vpaymentDate=$vpayments->productsSalesPayments[$vpaymentNumber]->paymentDate;
            $vpaymentAmount=$vpayments->productsSalesPayments[$vpaymentNumber]->amount;
            $vlabel1="Cliente:";
            if ( $vpayments->productsSalesPayments[$vpaymentNumber]->productSale->customer->personType->idPersonType==1 ){
                if($vpayments->productsSalesPayments[$vpaymentNumber]->productSale->operationType->idOperationType != 2){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vpayments->productsSalesPayments[$vpaymentNumber]->enterprise->idEnterprise;;
                $vflCustomer->idCustomer=$vpayments->productsSalesPayments[$vpaymentNumber]->productSale->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                $vlabel2=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                }
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vpayments->productsSalesPayments[$vpaymentNumber]->enterprise->idEnterprise;;
                $vflCustomer->idCustomer=$vpayments->productsSalesPayments[$vpaymentNumber]->productSale->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                $vlabel2=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
            }
            else{
                if($vpayments->productsSalesPayments[$vpaymentNumber]->productSale->operationType->idOperationType != 2){
                                    $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$vpayments->productsSalesPayments[$vpaymentNumber]->enterprise->idEnterprise;;
                $vflCustomer->idCustomer=$vpayments->productsSalesPayments[$vpaymentNumber]->productSale->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                $vlabel2=$vflCustomer->businessName;
                    }

            }


            if($vpayments->productsSalesPayments[$vpaymentNumber]->productSale->operationStatus->operationStatus != 2){
            $vlabel3="Folio:";
            $vidSource=$vpayments->productsSalesPayments[$vpaymentNumber]->productSale->idProductSale;

            $vfilter ="WHERE p_productsaledetail.id_enterprise=". $vpayments->productsSalesPayments[$vpaymentNumber]->enterprise->idEnterprise ." ";
            $vfilter.="AND p_productsaledetail.id_productSale='" . $vpayments->productsSalesPayments[$vpaymentNumber]->productSale->idProductSale . "' ";
            //$vfilter.="AND p_productsale.id_operationStatus=0'" . $vpayments->productsSalesPayments[$vpaymentNumber]->productSale->idProductSale . "'";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
            $vglobal=clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails);
            }




        $vrptPaymentsReport->x=100;   $vrptPaymentsReport->y+=0;
        $vrptPaymentsReport->printMultiLine(90, 15, "Fecha" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=150;
        $vrptPaymentsReport->printMultiLine(90, 15, "Compra" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=200;
        $vrptPaymentsReport->printMultiLine(90, 15, "Abono" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=250;
        $vrptPaymentsReport->printMultiLine(90, 15, "Referencia" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=300;
        $vrptPaymentsReport->printMultiLine(90, 15, "Saldo" , 9, "B", "L", 0);



        }
        else{
            $vpaymentDate=$vpayments->loansPayments[$vpaymentNumber]->paymentDate;
            $vpaymentAmount=$vpayments->loansPayments[$vpaymentNumber]->amount;
            $vlabel1="Repartidor:";
            $vlabel2=$vpayments->loansPayments[$vpaymentNumber]->loan->dealer->name . ' ' .
                     $vpayments->loansPayments[$vpaymentNumber]->loan->dealer->firstName . ' ' .
                     $vpayments->loansPayments[$vpaymentNumber]->loan->dealer->lastName;
            $vidSource=$vpayments->loansPayments[$vpaymentNumber]->loan->idLoan;
            $vglobal=$vpayments->loansPayments[$vpaymentNumber]->loan->amount;
        }

        $vrptPaymentsReport->x=50;   $vrptPaymentsReport->y+=10;
        $vrptPaymentsReport->printMultiLine(75, 15, "Fecha de Pago:" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=125;
        $vrptPaymentsReport->printMultiLine(60, 15, $vpaymentDate , 8, "", "L", 0);
        $vrptPaymentsReport->x=460;
        $vrptPaymentsReport->printMultiLine(40, 15, "Abono:" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=500;
        $vrptPaymentsReport->printMultiLine(70, 15, "$" . number_format($vpaymentAmount, 2, ".", ","), 8, "", "R", 0);
        $vrptPaymentsReport->x=50;   $vrptPaymentsReport->y+=15;
        $vrptPaymentsReport->printMultiLine(75, 15, $vlabel1, 9, "B", "L", 0);
        $vrptPaymentsReport->x=125;
        if(isset($vlabel2)){
            $vrptPaymentsReport->printMultiLine(445, 15, $vlabel2, 8, "", "L", 0);
        }
        $vrptPaymentsReport->y+=15;
        if ( $_GET["vpaymentType"]!=3 ){
            if($_GET["vpaymentType"]==2 && $vpayments->productsSalesPayments[$vpaymentNumber]->productSale->operationStatus->operationStatus != 2){
            $vrptPaymentsReport->x=50;
            $vrptPaymentsReport->printMultiLine(100, 15, $vlabel3, 9, "B", "L", 0);
            $vrptPaymentsReport->x=150;
            $vrptPaymentsReport->printMultiLine(60, 15, $vidSource, 8, "", "L", 0);
            }else{
            $vrptPaymentsReport->x=50;
            $vrptPaymentsReport->printMultiLine(100, 15, $vlabel3, 9, "B", "L", 0);
            $vrptPaymentsReport->x=150;
            $vrptPaymentsReport->printMultiLine(60, 15, $vidSource, 8, "", "L", 0);
            }

        }
        $vrptPaymentsReport->x=200;
        $vrptPaymentsReport->printMultiLine(160, 15, "Saldo pendiente a la fecha de pago:" , 9, "B", "L", 0);
        $vrptPaymentsReport->x=350;


        if ( $_GET["vpaymentType"]!=3 ){
            if ( strcmp($vidPriorSource, $vidSource)==0 ){
        $vrptPaymentsReport->printMultiLine(70, 15, "$" . number_format($vglobal-$pagoAnterior, 2, ".", ","), 8, "", "R", 0);
            }
            else{
        $vrptPaymentsReport->printMultiLine(70, 15, "$" . number_format($vglobal, 2, ".", ","), 8, "", "R", 0);
            }

        }

        //$vrptPaymentsReport->printMultiLine(70, 15, "$" . number_format($vglobal, 2, ".", ","), 8, "", "R", 0);
        $totalCompra+=$vglobal;
        $totalAbonos+=consultarAbonosVenta($vidSource,$fechaInicial,$fechaFinal);
        //$vrptPaymentsReport->printMultiLine(70, 15, "$" . number_format($vglobal-consultarAbonosVenta($vidSource,$fechaInicial,$fechaFinal), 2, ".", ","), 8, "", "R", 0);
        $vrptPaymentsReport->x=450;
        $vrptPaymentsReport->printMultiLine(150, 15, "Saldo pendiente:" , 9, "B", "L", 0);
        if ( $_GET["vpaymentType"]!=3 ){
            if ( strcmp($vidPriorSource, $vidSource)==0 ){
                $vpaymentAmountTotal+=$vpaymentAmount;
            }
            else{
                $vpaymentAmountTotal=$vpaymentAmount;
            }

            $pagoAnterior = $vpaymentAmountTotal;
        }
        else if( ($vidPriorSource==$vidSource) && ($vidPriorDealer==$vpayments->loansPayments[$vpaymentNumber]->loan->dealer->idDealer) ){
            $vpaymentAmountTotal+=$vpaymentAmount;
        }
        else{
            $vpaymentAmountTotal=$vpaymentAmount;
        }
        $vrptPaymentsReport->x=500;
        $vrptPaymentsReport->printMultiLine(70, 15, "$" . number_format($vglobal-$vpaymentAmountTotal, 2, ".", ","), 8, "", "R", 0);
        $vrptPaymentsReport->y+=20;
        $vrptPaymentsReport->printLine(50, $vrptPaymentsReport->y, 570, $vrptPaymentsReport->y, 0.2);
        $vidPriorSource=$vidSource;
        if ( $_GET["vpaymentType"]==3 ){
            $vidPriorDealer=$vpayments->loansPayments[$vpaymentNumber]->loan->dealer->idDealer;
        }
        $vgTotalAmount+=$vpaymentAmount;

        unset($vpaymentDate, $vpaymentAmount, $vlabel1,  $vlabel2, $vlabel3, $vidSource, $vglobal);
    }
    if( $vpaymentsTotal>0 ){
        /*
        $vrptPaymentsReport->x=370;  $vrptPaymentsReport->y+=15;
        $vrptPaymentsReport->printMultiLine(120, 15, "Total Compra:", 9, "B", "R", 0);
        $vrptPaymentsReport->x=430;
        $vrptPaymentsReport->printMultiLine(120, 15, "$" . number_format($totalCompra, 2, ".", ","), 8, "B", "R", 0);
        */

        $vrptPaymentsReport->x=370;  $vrptPaymentsReport->y+=15;
        $vrptPaymentsReport->printMultiLine(120, 15, "Total saldo pendiente:", 9, "B", "R", 0);
        $vrptPaymentsReport->x=430;
        $vrptPaymentsReport->printMultiLine(120, 15, "$" . number_format($totalCompra-$totalAbonos, 2, ".", ","), 8, "B", "R", 0);



    }
    $vrptPaymentsReport->showPage();

    unset($vdate, $vpageNumber, $vpageNew, $vgTotalAmount, $vpaymentAmountTotal, $vidPriorSource, $vidPriorDealer, $vsourceFile, $voutFileName,
          $vorientation, $vpageSize, $vrptPaymentsReport, $vflEnterpriseUser, $vfilter, $vpayments, $vpaymentsTotal, $vpaymentNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de pagos";
}

function showPage($vrptPaymentsReport, $vflEnterpriseUser, $vdate, $vpageNumber,$nombreCliente=null)
 {

    if ($nombreCliente == null) {
        $nombreCliente = "";
    }
    try{
        showHeader($vrptPaymentsReport, $vflEnterpriseUser, $vdate, $nombreCliente);
        showFooter($vrptPaymentsReport, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptPaymentsReport, $vflEnterpriseUser, $vdate, $nombreCliente)
 {
    try{
        $vrptPaymentsReport->x=50;	$vrptPaymentsReport->y=10;
        $vrptPaymentsReport->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vheaderTitle='"Reporte de Pagos de Repartidores"';
        if ( $_GET["vpaymentType"]==1 ){
            $vheaderTitle='"Reporte de Pagos a Proveedores"';
        }
        else if ( $_GET["vpaymentType"]==2 ){
            $vheaderTitle='Estado de cuenta ';
            if ($nombreCliente!=null) {
                $vheaderTitle.= $nombreCliente;
            }

        }

        $vrptPaymentsReport->x=50;	$vrptPaymentsReport->y+=14;
        $vrptPaymentsReport->printMultiLine(520, 15, $vheaderTitle, 10, "B", "C", 0);
        $vrptPaymentsReport->x=50;	$vrptPaymentsReport->y+=0;


        $vrptPaymentsReport->x=50;	$vrptPaymentsReport->y+=0;
        $vrptPaymentsReport->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);

        unset($vheaderTitle);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showFooter($vrptPaymentsReport, $vpageNumber)
 {
    try{
        $vrptPaymentsReport->x=50;  $vrptPaymentsReport->y=715;
        $vrptPaymentsReport->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptPaymentsReport->y=120;
        $vrptPaymentsReport->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }


 function consultarAbonosVenta($folio,$fechaInicial,$fechaFinal){
    $vpaymentAmountByCustomers=0;
    $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND p_productsalepayment.id_productSale='".$folio."' ";
    $vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter.="AND p_productsale.id_operationType=2";
    //$vfilter.=" AND p_productsale.id_cash= " . $idCaja . " ";
    $vproductsSalesPayments= new clscFLProductSalePayment();
    clscBLProductSalePayment::queryGroupByCustomerToDataBase($vproductsSalesPayments, $vfilter, 1);
    $vproductsSalesPaymentsTotal=clscBLProductSalePayment::total($vproductsSalesPayments);
    for ($vproductsSalesPaymentsNumber=0; $vproductsSalesPaymentsNumber<$vproductsSalesPaymentsTotal; $vproductsSalesPaymentsNumber++){
        $vname=$vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->property["name"];
        if ( $vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->personType->idPersonType==1 ){
            $vname.=" " . $vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->property["firstName"] .
                    " " . $vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->property["lastName"];
        }
        $vpaymentAmount=$vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->amount;
        //if($vproductsSalesPaymentsNumber<($vproductsSalesPaymentsTotal)){
            $vpaymentAmountByCustomers+=$vpaymentAmount;
        //}

        }
        return $vpaymentAmountByCustomers;
 }

?>
