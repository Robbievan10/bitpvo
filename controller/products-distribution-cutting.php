<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once(dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCash.php");
date_default_timezone_set('America/Mexico_City');

function showRoutesList()
{
    $vresponse = new xajaxResponse();
    
    try {
        $vroutesDeliverers = new clscFLRouteDealer();
        clscBLRouteDealer::queryToDataBase($vroutesDeliverers, "WHERE c_routedealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vroutesDeliverersTotal = clscBLRouteDealer::total($vroutesDeliverers);
        $vJSON                  = '[{"text":"--Todos--", "value":"0"}';
        for ($vi = 0; $vi < $vroutesDeliverersTotal; $vi++) {
            $vJSON .= ', {"text":"' . $vroutesDeliverers->routesDeliverers[$vi]->route->route . '", "value":' . $vroutesDeliverers->routesDeliverers[$vi]->route->idRoute . '}';
        }
        $vJSON .= "]";
        
        $vresponse->setReturnValue($vJSON);
        unset($vroutesDeliverers, $vroutesDeliverersTotal, $vJSON, $vi);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de listar las rutas asignadas a los distribuidores, intente de nuevo");
    }
    
    return $vresponse;
}

function showProductsDistributionsList($vfilterForm)
{
    $vresponse = new xajaxResponse();
    
    try {
        $vfilter = "WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if (strcmp(trim($vfilterForm["dtpckrstartDate"]), "") != 0) {
            $vfilter .= "AND (p_productdistribution.fldproductDistributionDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"]))) . "' ";
            $vfilter .= "AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        } elseif (strcmp(trim($vfilterForm["txtproductDistributionFolio"]), "") != 0) {
            $vfilter .= "AND p_productdistribution.id_productDistribution LIKE '%" . trim($vfilterForm["txtproductDistributionFolio"]) . "%' ";
        } elseif ((int) ($vfilterForm["cmbroutesList"]) != 0) {
            $vfilter .= "AND p_productdistribution.id_route=" . (int) ($vfilterForm["cmbroutesList"]) . " ";
        } else {
            $vfilter .= "AND p_productdistribution.fldproductDistributionCuttingDate IS NULL ";
        }
        $vproductsDistributions = new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal = clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber = 0; $vproductsDistributionNumber < $vproductsDistributionsTotal; $vproductsDistributionNumber++) {
            if ($vproductsDistributionNumber == 0) {
                $vdata = '<table id="vgrdproductsDistributionsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductDistributionFolio" style="text-align:center; font-weight:bold;">Folio</th>
                                    <th data-field="vproductDistributionDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vproductDistributionRoute" style="text-align:center; font-weight:bold;">Ruta/Repartidor</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata .= '<tr>
                        <td>
                            <input type="radio" name="cmrproductsDistribution" onClick="showProductsDistributionData(\'' . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution . '\');" title="Seleccionar Reparto de Productos" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata .= ' <td>' . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution . '</td>';
            $vdata .= ' <td>' . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionDate . '</td>';
            $vdata .= ' <td>' . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->routeDealer->route->route . " / " . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->routeDealer->dealer->name . ' ' . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->routeDealer->dealer->firstName . ' ' . $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->routeDealer->dealer->lastName . ' ';
            $vdata .= "</tr>";
        }
        if ($vproductsDistributionNumber >= 1) {
            $vdata .= '</tbody>
                </table>';
            $vresponse->assign("vproductsDistributionsList", "innerHTML", $vdata);
            $vresponse->script("setProductsDistributionsList();");
            
        } else {
            $vdata = '<p>&nbsp;</p>
                    <p class="textCaption-4">No existen distribuciones de productos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsDistributionsList", "innerHTML", $vdata);
            
        }
        unset($vfilter, $vproductsDistributions, $vproductsDistributionsTotal, $vproductsDistributionNumber, $vdata);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de mostrar las distribuciones de productos, intente de nuevo");
    }
    
    unset($vfilterForm);
    return $vresponse;
}

function showProductsDistributionList($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $nombre_ruta, $folio)
{
    try {
        $vinitialInventoryTotal     = 0;
        $vdistributedGrandTotal     = 0;
        $vdistributionProductsTotal = clscBLProductDistributionDetail::total($vproductsDistributionsDetails);
        for ($vi = 0; $vi < $vdistributionProductsTotal; $vi++) {
            if ($vi == 0) {
                $vdata = '<table id="vgrdproductsDistributionCuttingList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductName" style="text-align:center; font-weight:bold;">Producto</th>
                                    <th data-field="vproductDistributionAmount" style="text-align:center; font-weight:bold;">Inv. Inicial</th>
                                    <th data-field="vproductReturnedAmount" style="text-align:center; font-weight:bold;">Distribuido</th>
                                    <th data-field="vproductDistributedAmount" style="text-align:center; font-weight:bold;">Inv. Final</th>
                                    <th data-field="vtotalPrice" style="text-align:center; font-weight:bold;">Total</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            
            
            $stock = localStock($vproductsDistributionsDetails->productsDistributionsDetails[$vi]->product->idProduct, $nombre_ruta, $folio, $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionAmount);
            
            
            $vdistributionAmount = $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionAmount;
            $vdistributedAmount  = $stock;
            $vdistributedTotal   = $vdistributedAmount * $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionPrice;
            $vinitialInventoryTotal += $vdistributionAmount * $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionPrice;
            $vdistributedGrandTotal += $vdistributedTotal;
            $vdata .= '<tr>
                        <td>
                            <input type="radio" name="cmrproduct" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata .= '   <td>' . strtoupper($vproductsDistributionsDetails->productsDistributionsDetails[$vi]->product->name) . '</td>';
            $vdata .= '   <td>' . number_format($vdistributionAmount, 2, ".", ",") . '</td>';
            $vdata .= '   <td>' . number_format($vdistributedAmount, 2, ".", ",") . '</td>';
            $vdata .= '   <td>' . number_format($vdistributionAmount - $vdistributedAmount, 2, ".", ",") . '</td>';
            $vdata .= '   <td>$ ' . number_format($vdistributedTotal, 2, ".", ",") . '</td>';
            
            unset($vdistributionAmount, $vdistributedAmount, $vdistributedTotal);
        }
        if ($vdistributionProductsTotal >= 1) {
            $vdata .= '</tbody>
                </table>';


            $vpreliminaryTotal = $vdistributedGrandTotal - ($vflProductDistribution->creditTotal + $vflProductDistribution->discountTotal + $vflProductDistribution->devolutionTotal);



            $vresponse->assign("vinitialInventoryTotal", "innerHTML", "$ " . number_format($vinitialInventoryTotal, 2, ".", ","));
            $vresponse->assign("vdistributedTotal", "innerHTML", "$ " . number_format($vdistributedGrandTotal, 2, ".", ","));
            $vresponse->assign("vpreliminaryTotal", "innerHTML", "$ " . number_format($vpreliminaryTotal, 2, ".", ","));
            $vresponse->script("vdistributedGrandTotal=" . $vdistributedGrandTotal . ";
                                vcmdrecordProductDistributedAmount.enable(false);");
            $vresponse->assign("vproductsDistributionList", "innerHTML", $vdata);
            $vresponse->script("setProductsDistributionCuttingList();");
            $vresponse->script("getdeliveryTotal();");
            
            
        } else {
            $vdata = '<p>&nbsp;</p>
                    <p class="textCaption-4">No existen productos en la distribución.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsDistributionList", "innerHTML", $vdata);
            $vresponse->script("getdeliveryTotal();");
            
        }
        
        unset($vinitialInventoryTotal, $vdistributedGrandTotal, $vdistributionProductsTotal, $vi, $vdata);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de listar los productos de la distribución, intente de nuevo");
    }
}

function printProductsDistribution($vidProductDistribution)
{
    $vresponse = new xajaxResponse();
    
    try {
        $vurl = "./controller/products-distribution-cutting-rpt.php?vidProductDistribution=" . $vidProductDistribution;
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de imprimir la distribución de productos, intente de nuevo");
    }
    
    unset($vidProductDistribution);
    return $vresponse;
}






/////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////






function showProductsDistributionData($vidProductDistribution)
{
    $ejecutar = false;
    $vresponse = new xajaxResponse();
    
    
    try {

        $vflProductDistribution = new clspFLProductDistribution();
        $vflProductDistribution->enterprise->idEnterprise = $_SESSION['idEnterprise'];
        $vflProductDistribution->idProductDistribution    = $vidProductDistribution;
        switch (clspBLProductDistribution::queryToDataBase($vflProductDistribution)) {
            case 0:
                $vresponse->alert("Los datos del reparto de productos no se encuentran registrados");
                break;
            case 1:
                $vresponse->script("enableProductsDistributionDataButtons();
                                        cleanProductsDistributionFormFields();
                                        enableProductsDistributionFormFields();
                                        vidProductDistribution='" . $vflProductDistribution->idProductDistribution . "';");
                $vresponse->assign("vproductsDistributionFolio", "innerHTML", $vflProductDistribution->idProductDistribution);
                $vresponse->assign("vproductsDistributionDate", "innerHTML", $vflProductDistribution->productDistributionDate);
                $vresponse->assign("vproductsDistributionRouteName", "innerHTML", $vflProductDistribution->routeDealer->route->route);
                $vresponse->assign("vproductsDistributionRouteDealerName", "innerHTML", $vflProductDistribution->routeDealer->dealer->name . " " . $vflProductDistribution->routeDealer->dealer->firstName . " " . $vflProductDistribution->routeDealer->dealer->lastName);

                $timeStamp = $vflProductDistribution->CreatedOn;

                //$vflProductDistribution->creditTotal = consultarVentaCredito($vflProductDistribution->routeDealer->route->route);

                $vtext = new clspText("txtcreditTotal", $vresponse);
                $vtext->setValue(consultarVentaCredito($vflProductDistribution->routeDealer->route->route,$vflProductDistribution->idProductDistribution, 2, ".", ","));
                

                $vtext = new clspText("txtdevolutionTotal", $vresponse);
                $vtext->setValue(number_format($vflProductDistribution->devolutionTotal, 2, ".", ","));
                
                $vfilter = "WHERE p_productdistributiondetail.id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
                $vfilter .= "AND p_productdistributiondetail.id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
                $vproductsDistributionsDetails = new clscFLProductDistributionDetail();
                clscBLProductDistributionDetail::queryToDataBase($vproductsDistributionsDetails, $vfilter);
                showProductsDistributionList($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $vflProductDistribution->routeDealer->route->route, $vflProductDistribution->idProductDistribution);



                //$totalDescuento = number_format(totalVenta($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $vflProductDistribution->routeDealer->route->route, $vflProductDistribution->idProductDistribution) - consultarVentaContado($vflProductDistribution->routeDealer->route->route) - consultarVentaCredito($vflProductDistribution->routeDealer->route->route), 2, ".", ",");


                $totalDescuento = abs(totalVenta($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $vflProductDistribution->routeDealer->route->route, $vflProductDistribution->idProductDistribution) - consultarVentaContado($vflProductDistribution->routeDealer->route->route,$vflProductDistribution->idProductDistribution) - consultarVentaCredito($vflProductDistribution->routeDealer->route->route,$vflProductDistribution->idProductDistribution));

                $vtext = new clspText("txtdiscountTotal", $vresponse);
                $vtext->setValue(number_format($totalDescuento,2,".",","));


                if($vflProductDistribution->productDistributionCuttingDate != NULL){
                    
                    $ejecutar = true;
                }else{
                    $ejecutar = false;
                }




                $vresponse->assign("vpreliminaryTotal", "innerHTML", "$ " . number_format(totalVenta($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $vflProductDistribution->routeDealer->route->route, $vflProductDistribution->idProductDistribution), 2, ".", ",") - number_format($totalDescuento, 2, ".", ",") - number_format(consultarVentaCredito($vflProductDistribution->routeDealer->route->route,$vflProductDistribution->idProductDistribution), 2, ".", ","));



                //$vresponse->assign("vpreliminaryTotal", "innerHTML", "$ " . number_format(totalVenta($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $vflProductDistribution->routeDealer->route->route, $vflProductDistribution->idProductDistribution) - $totalDescuento - consultarVentaCredito($vflProductDistribution->routeDealer->route->route), 2, ".", ","));
                
                //$vresponse->script("xajax.$('txtcreditTotal').focus();");
                $vresponse->script("cobranza($vflProductDistribution->observation)");
                $vresponse->script("vcmbBanksList.value(" . $vflProductDistribution->observation . ");");
                
                
                
                if ($vflProductDistribution->observation === NULL || $vflProductDistribution->observation == "") {
                    $vdata = "";
                    $vresponse->assign("divCobranza", "innerHTML", $vdata);
                } else {
                    $vdata = '<table id="vgrdproductsSalesList">
                            <thead>
                                <tr>
                                    <th style="text-align:center; font-weight:bold;">Folio</th>
                                    <th data-field="vproductSaleCustomer" style="text-align:center; font-weight:bold;">Cliente</th>
                                    <th data-field="vtotalAmount" style="text-align:center; font-weight:bold;">Monto</th>
                                    <th data-field="vproductSaleDate" style="text-align:center; font-weight:bold;">Abono</th>
                                 
                                </tr>
                            </thead>';
                    
                    
                    
                    $json = $vflProductDistribution->observation;
                    
                    
                    $json = utf8_encode($json);
                    $data = json_decode($json, true);
                    
                    
                    
                    foreach ($data as $dataArr) {
                        $cliente = utf8_decode($dataArr['cliente']);
                        $fecha   = $dataArr['fecha'];
                        $folio   = $dataArr['folio'];
                        $folio   = preg_replace('/\s+/', '', $folio);
                        $folio   = str_replace(' ', '', $folio);
                        $monto   = $dataArr['monto'];
                        $monto   = str_replace('$', '', $monto);
                        
                        $vdata .= '<tr>
                        <td data-field="vproductSaleDate">
                            ' . $folio . '
                        </td>';
                        
                        
                        $vdata .= '   <td data-field="vproductSaleCustomer">' . $cliente . '</td>';
                        
                        $vdata .= '   <td data-field="vproductSaleDate">$ ' . $monto . '</td>';
                        

                        $abono = ProductsSalePaymentsList($folio,$vflProductDistribution->CreatedOn);
                        $vdata .= '   <td data-field="vproductSaleDate"><input onblur="getdeliveryTotal()" type="text"   value="' . number_format($abono, 2, ".", ","). '"readonly>
</td>';


                        $vdata .= "</tr>";
                        
                    }
                    
                    
                    $vdata .= '</tbody>
                </table>';
                    $vresponse->assign("divCobranza", "innerHTML", $vdata);
                    $vresponse->script("setProductsSalesList();
                                setPaymentsSection();");
                    $vresponse->script("getdeliveryTotal();");

                }
                
                
                
                
                unset($vtext, $vfilter, $vproductsDistributionsDetails);
                break;
        }
        
        unset($vflProductDistribution);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la distribución de productos, intente de nuevo");
    }

    unset($vidProductDistribution); 
    if($ejecutar){
        $vresponse->script("activarBoton();");
    }else{
        $vresponse->script("anularBoton();");
    }
    $vresponse->script("getdeliveryTotal();");
    return $vresponse;
}



function ProductsSalePaymentsList($vidProductSale,$timeStamp)
 {
    $vresponse= new xajaxResponse();
    $pagosTotal = 0;
    
    try{
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productsalepayment.id_productSale='" . $vidProductSale . "' ";
        $vfilter.="AND p_productsalepayment.CreatedOn>'" . $timeStamp . "'";
        $vproductsSalesPayments= new clscFLProductSalePayment();
        clscBLProductSalePayment::queryToDataBase($vproductsSalesPayments, $vfilter);
        $vproductsSalePaymentsTotal=clscBLProductSalePayment::total($vproductsSalesPayments);
        for ($vproductSalePaymentNumber=0; $vproductSalePaymentNumber<$vproductsSalePaymentsTotal; $vproductSalePaymentNumber++){

            $pagosTotal += $vproductsSalesPayments->productsSalesPayments[$vproductSalePaymentNumber]->amount;

        }

        unset($vfilter, $vproductsSalesPayments, $vproductsSalePaymentsTotal, $vproductSalePaymentNumber, $vdata);
    }
    catch (Exception $vexception){

    }
    
    unset($vidProductSale);
    return $pagosTotal;
 } 

function showProductDistributionDetailData($vidProductDistribution, $vidProduct)
{
    $vresponse = new xajaxResponse();
    
    try {
        $vflProductDistributionDetail                                                = new clspFLProductDistributionDetail();
        $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise = $_SESSION['idEnterprise'];
        $vflProductDistributionDetail->productDistribution->idProductDistribution    = $vidProductDistribution;
        $vflProductDistributionDetail->product->idProduct                            = $vidProduct;
        switch (clspBLProductDistributionDetail::queryToDataBase($vflProductDistributionDetail)) {
            case 0:
                $vresponse->alert("Los datos de distribución del producto no se encuentran registrados");
                break;
            case 1:
                $vresponse->script("cleanRecordProductReturnedAmountFormFields();
                                        vproductDistributionAmount=" . $vflProductDistributionDetail->distributionAmount . ";");
                $vresponse->assign("vproductName", "innerHTML", $vflProductDistributionDetail->product->name);
                $vtext = new clspText("txtreturnedAmount", $vresponse);
                $vtext->setValue(number_format($vflProductDistributionDetail->returnedAmount, 2, ".", ","));
                $vresponse->assign("vdistributionAmount", "innerHTML", number_format($vflProductDistributionDetail->distributionAmount, 2, ".", ","));
                $vresponse->assign("vdistributedAmount", "innerHTML", number_format($vflProductDistributionDetail->distributionAmount - $vflProductDistributionDetail->returnedAmount, 2, ".", ","));
                
                unset($vtext);
                break;
        }
        
        unset($vflProductDistributionDetail);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de mostrar los datos de distribución del producto, intente de nuevo");
    }
    
    unset($vidProductDistribution, $vidProduct);
    return $vresponse;
}

function recordProductDistributionCutting($vidProductDistribution, $vproductsDistributionForm, $json)
{
    $vresponse = new xajaxResponse();
    
    try {
        $vflProductDistribution                           = new clspFLProductDistribution();
        $vflProductDistribution->enterprise->idEnterprise = $_SESSION['idEnterprise'];
        $vflProductDistribution->idProductDistribution    = $vidProductDistribution;
        $vflProductDistribution->creditTotal              = (float) (str_replace(",", "", trim($vproductsDistributionForm["txtcreditTotal"])));
        $vflProductDistribution->discountTotal            = (float) (str_replace(",", "", trim($vproductsDistributionForm["txtdiscountTotal"])));
        $vflProductDistribution->devolutionTotal          = (float) (str_replace(",", "", trim($vproductsDistributionForm["txtdevolutionTotal"])));
        $vflProductDistribution->observation              = $json;
        switch (clspBLProductDistribution::recordCuttingInDataBase($vflProductDistribution, $json)) {
            case 0:
                $vresponse->alert("El corte de la distribución no ha tenido cambios");
                $vresponse->script("anularBoton();");
                break;
            case 1:
                $vresponse->alert("El corte de la distribución de productos ha sido registrado correctamente");
                $vresponse->script("anularBoton();");
                break;
        }
        
        unset($vflProductDistribution);
        
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de registrar el corte de la distribución de productos, intente de nuevo");
    }
    
    unset($vidProductDistribution, $vproductsDistributionForm);
    return $vresponse;
}




function updateProductsDistributionData($vidProductDistribution, $vproductsDistributionForm, $vproductsDistributionForm1)
{
    $vresponse = new xajaxResponse();
    
    try {
        $vflProductDistribution                              = new clspFLProductDistribution();
        $vflProductDistribution->enterprise->idEnterprise    = $_SESSION['idEnterprise'];
        $vflProductDistribution->idProductDistribution       = $vidProductDistribution;
        $vflProductDistribution->routeDealer->route->idRoute = (int) ($vproductsDistributionForm["cmbproductsDistributionRoute"]);
        $vflProductDistribution->productDistributionDate     = trim($vproductsDistributionForm["dtpckrproductsDistributionDate"]);
        $vflProductDistribution->observation                 = trim($vproductsDistributionForm1["txtobservation"]);
        
        $vproductsDistribution         = $_SESSION['vproductsDistributionList'];
        $vproductsDistributionTotal    = count($vproductsDistribution);
        $vproductsDistributionsDetails = new clscFLProductDistributionDetail();
        for ($vi = 0; $vi < $vproductsDistributionTotal; $vi++) {
            $vflProductDistributionDetail                         = new clspFLProductDistributionDetail();
            $vflProductDistributionDetail->product->idProduct     = (int) ($vproductsDistribution[$vi][0]);
            $vflProductDistributionDetail->distributionAmount     = (float) ($vproductsDistribution[$vi][2]);
            $vflProductDistributionDetail->distributionWeightInkg = (float) ($vproductsDistribution[$vi][3]);
            $vflProductDistributionDetail->distributionPrice      = (float) ($vproductsDistribution[$vi][4]);
            clscBLProductDistributionDetail::add($vproductsDistributionsDetails, $vflProductDistributionDetail);
            unset($vflProductDistributionDetail);
        }
        if (clspBLProductDistribution::updateInDataBase($vflProductDistribution, $vproductsDistributionsDetails) == 1) {
            $vresponse->alert("Los datos de la distribución de productos han sido modificados correctamente");
            $vresponse->script("vidAction=0;
                                xajax.$('cmdnewSaveProductsDistribution').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";                                
                                enableProductsDistributionDataButtons(0);
                                disableProductsDistributionFormFields();
                                disableAddProductButtons(1);");
        } else {
            $vresponse->alert("Imposible modificar los datos de la distribución de productos, intente de nuevo");
        }
        
        unset($vflProductDistribution, $vproductsDistribution, $vproductsDistributionTotal, $vproductsDistributionsDetails, $vi);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de modificar los datos de la distribución de productos, intente de nuevo");
    }
    
    unset($vidProductDistribution, $vproductsDistributionForm, $vproductsDistributionForm1);
    return $vresponse;
}



function obtenerIDCaja($nombreCaja)
{
    $vresponse = NULL;
    
    try {
        $vfilter = "WHERE c_cash.id_cash='" . $idcaja . "'";
        $vcash   = new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal = clscBLCash::total($vcash);
        
        
        //$vresponse = $vcash->cash[0]->cash;
        $vresponse = $vcash->cash[0]->idCash;
        unset($vcash, $vcashTotal, $vJSON, $vi);
    }
    catch (Exception $vexception) {
    }
    
    return $vresponse;
}



function obtenerTotalCaja($nombreCaja)
{
    $vresponse = new xajaxResponse();
    
    //------------------------------------------------------Ventas de Cajas---------------------------------------------
    $vinitialCashByCash      = 0;
    $vcashSaleAmountByCash   = 0;
    $vcreditSaleAmountByCash = 0;
    $fechaInicio             = NULL;
    $fechaFin                = NULL;
    $nombreCaja              = $nombreCaja;
    $fechaInicial            = NULL;
    $fechaFinal              = NULL;
    //$vfilter ="WHERE c_routecustomer.id_route IS NULL ";
    //$vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    
    $idCaja = obtenerIDCaja($nombreCaja);
    
    $vfilter = "WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    $vfilter .= "AND c_cash.fldcash='" . strval($nombreCaja) . "'";
    $vcash = new clscFLCash();
    clscBLCash::queryToDataBase($vcash, $vfilter);
    $vcashTotal = clscBLCash::total($vcash);
    for ($vi = 0; $vi < $vcashTotal; $vi++) {
        $idcaja = $vcash->cash[$vi]->idCash;
    }
    
    $vroutes = new clscFLRoute();
    $vroute;
    $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    $vfilter .= "AND c_route.fldroute='" . strval($nombreCaja) . "'";
    clscBLRoute::queryToDataBase($vroutes, $vfilter);
    $vroutesTotal = clscBLRoute::total($vroutes);
    for ($vi = 0; $vi < $vroutesTotal; $vi++) {
        $vroute = $vroutes->routes[$vi]->idRoute;
    }
    
    
    
    $vfilter = "WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    $vfilter .= "AND p_productdistribution.id_route=" . $vroute . " ";
    //$vfilter.="AND p_productdistribution.id_productDistribution='".$folio."'";
    //$vfilter.="ORDER BY p_productdistribution.idProductDistribution";
    
    $vproductsDistributions = new clscFLProductDistribution();
    clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
    $vproductsDistributionsTotal = clscBLProductDistribution::total($vproductsDistributions);
    
    
    for ($vproductsDistributionNumber = 0; $vproductsDistributionNumber < $vproductsDistributionsTotal; $vproductsDistributionNumber++) {
        if ($folio == $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution) {
            $folio_dist  = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
            $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;
            
            if ($vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate != NULL && $vproductsDistributionNumber < $vproductsDistributionsTotal - 1) {
                $fechaFin = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber + 1]->CreatedOn;
            }
            
        }
    }
    
    
    
    
    $vfilter = "WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter .= "AND p_productsale.CreatedOn > '" . $fechaInicio . "' ";
    if ($fechaFin != NULL) {
        $vfilter .= "AND p_productsale.CreatedOn< '" . $fechaFin . "' ";
    }
    $vfilter .= "AND p_productsale.fldcanceled=0";
    $vproductsSalesDetails = new clscFLProductSaleDetail();
    clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsSalesDetails, $vfilter, 1);
    $vproductsSalesDetailsTotal = clscBLProductSaleDetail::total($vproductsSalesDetails);
    
    
    $vfilter = "WHERE p_cashmovement.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter .= "AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial)) . "' ";
    $vfilter .= "AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    //$vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";
    $vfilter .= "AND p_cashmovement.id_cash= " . $idCaja . " ";
    $vinitialCash   = 0;
    $vcashMovements = new clscFLCashMovement();
    if (clscBLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vcashMovements, $vfilter)) {
        $vinitialCash = $vcashMovements->cashMovements[0]->openAmount;
        $vinitialCashByCash += $vinitialCash;
    }
    
    
    //$vfilter ="WHERE c_routecustomer.id_route IS NULL ";
    $vfilter = "WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaInicial)) . "') ";
    $vfilter .= "AND p_productsale.CreatedOn > '" . $fechaInicio . "' ";
    if ($fechaFin != NULL) {
        $vfilter .= "AND p_productsale.CreatedOn< '" . $fechaFin . "' ";
    }
    $vfilter .= "AND p_productsale.id_cash= " . $idCaja . " ";
    $vfilter .= "AND p_productsale.id_operationType=1 ";
    $vfilter .= "AND p_productsale.fldcanceled=0";
    $vcashSaleAmount           = 0;
    $vproductsCashSalesDetails = new clscFLProductSaleDetail();
    if (clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter)) {
        $vcashSaleAmount = $vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
        $vcashSaleAmountByCash += $vcashSaleAmount;
    }
    
    
    //$vfilter ="WHERE c_routecustomer.id_route IS NULL ";
    $vfilter = "WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaInicial)) . "') ";
    $vfilter .= "AND p_productsale.CreatedOn > '" . $fechaInicio . "' ";
    if ($fechaFin != NULL) {
        $vfilter .= "AND p_productsale.CreatedOn< '" . $fechaFin . "' ";
    }
    $vfilter .= "AND p_productsale.id_cash= " . $idCaja . " ";
    $vfilter .= "AND p_productsale.id_operationType=2 ";
    $vfilter .= "AND p_productsale.fldcanceled=0";
    $vcreditSaleAmount         = 0;
    $vproductsCashSalesDetails = new clscFLProductSaleDetail();
    if (clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter)) {
        $vcreditSaleAmount = $vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
        $vcreditSaleAmountByCash += $vcreditSaleAmount;
    }
    
    
    unset($vresponsibleName, $vfilter, $vinitialCash, $vcashMovements, $vcashSaleAmount, $vproductsCashSalesDetails, $vcreditSaleAmount);
    
    if ($vproductsSalesDetailsTotal > 0) {
        
        
    }
    
    $a    = array(
        'contado' => $vcashSaleAmountByCash,
        'credito' => $vcreditSaleAmountByCash,
        'saldoInicial' => $vinitialCashByCash,
        'Total' => $vcashSaleAmountByCash + $vcreditSaleAmountByCash + $vinitialCashByCash
    );
    $json = json_encode($a);
    
    $vresponse->setReturnValue($json);
    return $vresponse;
    
}



function recordProductReturnedAmount($vidProductDistribution, $vidProduct, $cantidad)
{
    $vresponse = new xajaxResponse();
    
    try {
        $vflProductDistributionDetail                                                = new clspFLProductDistributionDetail();
        $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise = $_SESSION['idEnterprise'];
        $vflProductDistributionDetail->productDistribution->idProductDistribution    = $vidProductDistribution;
        $vflProductDistributionDetail->product->idProduct                            = $vidProduct;
        $vflProductDistributionDetail->returnedAmount                                = $cantidad;
        switch (clspBLProductDistributionDetail::updateReturnedAmountInDataBase($vflProductDistributionDetail)) {
            case 0:
                $vresponse->alert("La cantidad no distribuida del producto no ha sufrido cambio alguno");
                break;
            case 1:
                $vflProductDistribution                           = new clspFLProductDistribution();
                $vflProductDistribution->enterprise->idEnterprise = $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise;
                $vflProductDistribution->idProductDistribution    = $vflProductDistributionDetail->productDistribution->idProductDistribution;
                /*
                $vflProductDistribution->creditTotal=(float)(str_replace(",", "", trim($vproductsDistributionForm["txtcreditTotal"])));
                $vflProductDistribution->discountTotal=(float)(str_replace(",", "", trim($vproductsDistributionForm["txtdiscountTotal"])));
                $vflProductDistribution->devolutionTotal=(float)(str_replace(",", "", trim($vproductsDistributionForm["txtdevolutionTotal"])));
                */
                
                $vfilter = "WHERE p_productdistributiondetail.id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
                $vfilter .= "AND p_productdistributiondetail.id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
                $vproductsDistributionsDetails = new clscFLProductDistributionDetail();
                clscBLProductDistributionDetail::queryToDataBase($vproductsDistributionsDetails, $vfilter);
                
                $vresponse->alert("La cantidad no distribuida del producto ha sido registrado correctamente");
                
                unset($vflProductDistribution, $vfilter, $vproductsDistributionsDetails);
                break;
        }
        
        unset($vflProductDistributionDetail);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de registrar la cantidad no distribuida del producto, intente de nuevo");
    }
    
    unset($vidProductDistribution, $vidProduct, $vproductsDistributionForm, $vproductSaleCutRecordForm);
    return $vresponse;
}

function exit_()
{
    $vresponse = new xajaxResponse();
    
    session_destroy();
    $vresponse->redirect("./");
    
    return $vresponse;
}


function localStock($product_id, $nombre_ruta, $folio, $cantidad_distribucion)
{
    $vresponse = new xajaxResponse();
    
    $vproductStock = 0;
    $vroute        = 0;
    $idcaja        = 0;
    $folio_dist;
    $fechaInicio;
    $fechaFin = NULL;
    
    try {
        
        //$vfilter="WHERE c_cash.id_enterprise='" . $_SESSION['idEnterprise']."' ";
        //$vfilter.="AND c_cash.fldcash=".$caja."' ";
        //$vfilter.="AND c_cash.fldcash=" . $caja;
        
        $vfilter = "WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter .= "AND c_cash.fldcash='" . strval($nombre_ruta) . "'";
        $vcash = new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal = clscBLCash::total($vcash);
        for ($vi = 0; $vi < $vcashTotal; $vi++) {
            $idcaja = $vcash->cash[$vi]->idCash;
        }
        
        $vroutes = new clscFLRoute();
        $vroute;
        $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter .= "AND c_route.fldroute='" . strval($nombre_ruta) . "'";
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal = clscBLRoute::total($vroutes);
        for ($vi = 0; $vi < $vroutesTotal; $vi++) {
            $vroute = $vroutes->routes[$vi]->idRoute;
        }
        
        
        
        $vfilter = "WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter .= "AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter.="AND p_productdistribution.id_productDistribution='".$folio."'";
        //$vfilter.="ORDER BY p_productdistribution.idProductDistribution";
        
        $vproductsDistributions = new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal = clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber = 0; $vproductsDistributionNumber < $vproductsDistributionsTotal; $vproductsDistributionNumber++) {
            if ($folio == $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution) {
                $folio_dist  = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
                $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;
                
                if ($vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate != NULL && $vproductsDistributionNumber < $vproductsDistributionsTotal - 1) {
                    $fechaFin = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber + 1]->CreatedOn;
                }
                
            }
            
            
            
            
            
            
            
        }
        
        if ($vproductsDistributionNumber == 0) {
            $vresponse->alert("No hay distribucion para esta ruta.");
        }
        
        
        
        $vfilter = "WHERE p_productsale.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        //$vfilter.="AND p_productsale.id_user='" . $_SESSION['idUser'] . "' ";
        //$vfilter.="AND p_productsale.id_cash=" . $vcashMovement . " ";
        $vfilter .= "AND p_productsale.id_cash=" . $idcaja . " ";
        $vfilter .= "AND p_productsale.CreatedOn > '" . $fechaInicio . "' ";
        
        if ($fechaFin != NULL) {
            $vfilter .= "AND p_productsale.CreatedOn < '" . $fechaFin . "' ";
        }
        $vfilter .= "AND p_productsale.fldcanceled = 0";
        
        $vproductsSales = new clscFLProductSale();
        clscBLProductSale::queryToDataBase($vproductsSales, $vfilter);
        $vproductsSalesTotal = clscBLProductSale::total($vproductsSales);
        
        for ($vproductsSaleNumber = 0; $vproductsSaleNumber < $vproductsSalesTotal; $vproductsSaleNumber++) {
            
            $vfilter = "WHERE p_productsaledetail.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
            $vfilter .= "AND p_productsaledetail.id_productSale='" . $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . "'";
            $vproductsSalesDetails = new clscFLProductSaleDetail();
            clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
            $vsaleProductsTotal = clscBLProductSaleDetail::total($vproductsSalesDetails);
            $vproductsSale      = array();
            for ($vi = 0; $vi < $vsaleProductsTotal; $vi++) {
                
                if ($product_id == $vproductsSalesDetails->productsSalesDetails[$vi]->product->idProduct) {
                    
                    $vproductStock += $vproductsSalesDetails->productsSalesDetails[$vi]->saleAmount;
                    
                }
            }
            
            
        }
        
        
        
        
        
        
    }
    catch (Exception $vexception) {
        $vresponse->alert($vexception->getMessage());
    }
    $stock = $vproductStock;
    recordProductReturnedAmount($folio, $product_id, $cantidad_distribucion - $stock);
    return $stock;
}



function consultarVentaCredito($nombreRuta,$folio)
{
    $vgTotalSalePrice = 0;
    $vsaleTotal       = 0;
    $totalCredito     = 0;
    $totalContado     = 0;
    $fechaInicio             = NULL;
    $fechaFin                = NULL;
    $vresponse        = new xajaxResponse();
    try {
        
        $vroute        = 0;
        $vcashMovement = $nombreRuta;
        
        
        try {
            
            $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            $vfilter .= "AND c_route.fldroute= '" . $vcashMovement . "' ";
            
            $vroutes = new clscFLRoute();
            clscBLRoute::queryToDataBase($vroutes, $vfilter, 1);
            $vroutesTotal = clscBLRoute::total($vroutes);
            
            $vroute = $vroutes->routes[0]->idRoute;
            
        }
        catch (Exception $vexception) {
            $vresponse->setReturnValue("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
        }
        
        $vflEnterpriseUser         = new clspFLEnterpriseUser();
        $vflEnterpriseUser->idUser = trim($_SESSION['idUser']);
        clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
        
        
        $vfilter = "WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter .= "AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter .= "AND p_productdistribution.fldproductDistributionCuttingDate IS NULL";
        $folio_dist;
        //$fechaInicio            = NULL;
        $vproductsDistributions = new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal = clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber = 0; $vproductsDistributionNumber < $vproductsDistributionsTotal; $vproductsDistributionNumber++) {
            
        if ($folio == $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution) {
            $folio_dist  = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
            $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;
            
            if ($vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate != NULL && $vproductsDistributionNumber < $vproductsDistributionsTotal - 1) {
                $fechaFin = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber + 1]->CreatedOn;
            }
            
        }

        }
        
        
        
        if ($fechaInicio != NULL) {
            
            $vflEnterpriseUser         = new clspFLEnterpriseUser();
            $vflEnterpriseUser->idUser = trim($_SESSION['idUser']);
            clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
            //showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            
            $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            //$vfilter.="AND c_route.id_route=" . $vroute .;
            $vfilter .= "AND c_route.id_route=" . $vroute . " ";
            
            
            $vroutes = new clscFLRoute();
            clscBLRoute::queryToDataBase($vroutes, $vfilter);
            $vroutesTotal = clscBLRoute::total($vroutes);
            for ($vrouteNumber = 0; $vrouteNumber < $vroutesTotal; $vrouteNumber++) {
                /*if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                }*/
                $vfilter = "WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
                $vfilter .= "AND c_routecustomer.id_route= " . $vroute . " ";
                //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . $fechaInicio . "' ";
                //$vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
                $vfilter .= "AND p_productsale.CreatedOn > '" . $fechaInicio . "' ";
                if($fechaFin != NULL){
                $vfilter .= "AND p_productsale.CreatedOn < '" . $fechaFin . "' ";
                }
                $vfilter .= "AND p_productsale.fldcanceled=0 ";
                $vcashMovement = getOpenCash($nombreRuta);
                $vfilter .= "AND p_productsale.id_cash='" . $vcashMovement . "'";
                
                
                $vproductsSalesDetails = new clscFLProductSaleDetail();
                if (clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1)) {
                    //$vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
                    //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
                    //$vrptProductsSales->x=80;
                    //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
                    $vproductsSalesDetailsTotal = clscBLProductSaleDetail::total($vproductsSalesDetails);
                    for ($vproductsSalesDetailNumber = 0; $vproductsSalesDetailNumber < $vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++) {
                        $vsaleTotal += $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                        if ($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->operationType->operationType == "Contado") {
                            $totalContado += $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                        } else {
                            $totalCredito += $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                        }
                    }
                }
                
                unset($vproductsSalesDetails);
            }
        }
        
        
        unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales, $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
    }
    catch (Exception $vexception) {
    }
    return $totalCredito;
}

function consultarVentaContado($nombreRuta,$folio)
{
    $vgTotalSalePrice = 0;
    $vsaleTotal       = 0;
    $totalCredito     = 0;
    $totalContado     = 0;
    $fechaInicio             = NULL;
    $fechaFin                = NULL;
    $vresponse        = new xajaxResponse();
    try {
        
        $vroute        = 0;
        $vcashMovement = $nombreRuta;
        
        
        try {
            
            $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            $vfilter .= "AND c_route.fldroute= '" . $vcashMovement . "' ";
            
            $vroutes = new clscFLRoute();
            clscBLRoute::queryToDataBase($vroutes, $vfilter, 1);
            $vroutesTotal = clscBLRoute::total($vroutes);
            
            $vroute = $vroutes->routes[0]->idRoute;
            
        }
        catch (Exception $vexception) {
            $vresponse->setReturnValue("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
        }
        
        $vflEnterpriseUser         = new clspFLEnterpriseUser();
        $vflEnterpriseUser->idUser = trim($_SESSION['idUser']);
        clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
        
        
        $vfilter = "WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter .= "AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter .= "AND p_productdistribution.fldproductDistributionCuttingDate IS NULL";
        $folio_dist;
        //$fechaInicio            = NULL;
        $vproductsDistributions = new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal = clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber = 0; $vproductsDistributionNumber < $vproductsDistributionsTotal; $vproductsDistributionNumber++) {
            
        if ($folio == $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution) {
            $folio_dist  = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
            $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;
            
            if ($vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate != NULL && $vproductsDistributionNumber < $vproductsDistributionsTotal - 1) {
                $fechaFin = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber + 1]->CreatedOn;
            }
            
        }

        }
        
        
        
        if ($fechaInicio != NULL) {
            
            $vflEnterpriseUser         = new clspFLEnterpriseUser();
            $vflEnterpriseUser->idUser = trim($_SESSION['idUser']);
            clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
            //showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            
            $vfilter = "WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
            //$vfilter.="AND c_route.id_route=" . $vroute .;
            $vfilter .= "AND c_route.id_route=" . $vroute . " ";
            
            
            $vroutes = new clscFLRoute();
            clscBLRoute::queryToDataBase($vroutes, $vfilter);
            $vroutesTotal = clscBLRoute::total($vroutes);
            for ($vrouteNumber = 0; $vrouteNumber < $vroutesTotal; $vrouteNumber++) {
                /*if( $vrptProductsSales->getY()>=640 ){
                $vrptProductsSales->addPage();
                showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                }*/
                $vfilter = "WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
                $vfilter .= "AND c_routecustomer.id_route= " . $vroute . " ";
                //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . $fechaInicio . "' ";
                //$vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
                $vfilter .= "AND p_productsale.CreatedOn > '" . $fechaInicio . "' ";
                if($fechaFin != NULL){
                $vfilter .= "AND p_productsale.CreatedOn < '" . $fechaFin . "' ";
                }
                $vfilter .= "AND p_productsale.fldcanceled=0 ";
                $vcashMovement = getOpenCash($nombreRuta);
                $vfilter .= "AND p_productsale.id_cash='" . $vcashMovement . "'";
                
                
                $vproductsSalesDetails = new clscFLProductSaleDetail();
                if (clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1)) {
                    //$vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
                    //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
                    //$vrptProductsSales->x=80;
                    //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
                    $vproductsSalesDetailsTotal = clscBLProductSaleDetail::total($vproductsSalesDetails);
                    for ($vproductsSalesDetailNumber = 0; $vproductsSalesDetailNumber < $vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++) {
                        $vsaleTotal += $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                        if ($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->operationType->operationType == "Contado") {
                            $totalContado += $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                        } else {
                            $totalCredito += $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
                        }
                    }
                }
                
                unset($vproductsSalesDetails);
            }
        }
        
        
        unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales, $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
    }
    catch (Exception $vexception) {
    }
    return $totalContado;
}

function getOpenCash($nombreRuta)
{
    $caja;
    try {
        $vfilter = "WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise'] . ' ';
        $vfilter .= "AND c_cash.fldcash = " . $nombreRuta . ' ';
        $vcash = new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal = clscBLCash::total($vcash);
        for ($vcashNumber = 0; $vcashNumber < $vcashTotal; $vcashNumber++) {
            
            $caja = $vcash->cash[$vcashNumber]->idCash;
        }
        
        
        unset($vfilter, $vcash, $vcashTotal, $vcashNumber, $vdata);
    }
    catch (Exception $vexception) {
    }
    
    return $caja;
}







function totalVenta($vflProductDistribution, $vproductsDistributionsDetails, $vresponse, $nombre_ruta, $folio)
{
    try {
        $vinitialInventoryTotal     = 0;
        $vdistributedGrandTotal     = 0;
        $vdistributionProductsTotal = clscBLProductDistributionDetail::total($vproductsDistributionsDetails);
        for ($vi = 0; $vi < $vdistributionProductsTotal; $vi++) {
            
            
            $stock = localStock($vproductsDistributionsDetails->productsDistributionsDetails[$vi]->product->idProduct, $nombre_ruta, $folio, $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionAmount);
            
            
            $vdistributionAmount = $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionAmount;
            $vdistributedAmount  = $stock;
            $vdistributedTotal   = $vdistributedAmount * $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionPrice;
            $vinitialInventoryTotal += $vdistributionAmount * $vproductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionPrice;
            $vdistributedGrandTotal += $vdistributedTotal;
            
            unset($vdistributionAmount, $vdistributedAmount, $vdistributedTotal);
        }

        
    }
    catch (Exception $vexception) {
    }
    return $vdistributedGrandTotal;
}

function openDistribution($a){
        $vresponse = new xajaxResponse();


        try {
        $vflProductDistribution                           = new clspFLProductDistribution();
        $vflProductDistribution->idProductDistribution    = $a;
        $vflProductDistribution->enterprise->idEnterprise = $_SESSION['idEnterprise'];
        switch (clspBLProductDistribution::openCuttingInDataBase($vflProductDistribution)) {
            case 0:
                    //$vresponse->alert(0);
                break;
            case 1:
                    //$vresponse->alert(1);
                break;
        }
        
        unset($vflProductDistribution);
        
    }
    catch (Exception $vexception) {
                //$vresponse->alert($vexception->getMessage());
    }

        return $vresponse;
}


$vxajax->register(XAJAX_FUNCTION, "openDistribution");
$vxajax->register(XAJAX_FUNCTION, "showRoutesList");
$vxajax->register(XAJAX_FUNCTION, "showProductsDistributionsList");
$vxajax->register(XAJAX_FUNCTION, "printProductsDistribution");
$vxajax->register(XAJAX_FUNCTION, "showProductsDistributionData");
$vxajax->register(XAJAX_FUNCTION, "showProductDistributionDetailData");
$vxajax->register(XAJAX_FUNCTION, "recordProductDistributionCutting");
$vxajax->register(XAJAX_FUNCTION, "recordProductReturnedAmount");
$vxajax->register(XAJAX_FUNCTION, "updateProductsDistributionData");
$vxajax->register(XAJAX_FUNCTION, "localStock");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>