<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotalSalePrice=0;

    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="ventas-productos-ruta-cantidad";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsSales= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsSales->addPage();

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);

    $vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    if ( (int)($_GET["vidRoute"])!=0 ){
        $vfilter.="AND c_route.id_route=" . (int)($_GET["vidRoute"]);;
    }
    $vroutes= new clscFLRoute();
    clscBLRoute::queryToDataBase($vroutes, $vfilter);
    $vroutesTotal=clscBLRoute::total($vroutes);
    for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
        if( $vrptProductsSales->getY()>=640 ){
            $vrptProductsSales->addPage();
            showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
        }
        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        $vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute. " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        if ( (int)($_GET["vproductsSaleType"])!=0 ){
            $vfilter.="AND p_productsale.id_operationType=" . (int)($_GET["vproductsSaleType"]) . " ";
        }
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1) ){
            $vsaleTotal=0;
            $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
            $vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
            $vrptProductsSales->x=80;
            $vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
            $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
            for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){
                if( $vrptProductsSales->getY()>=640 ){
                    $vrptProductsSales->addPage();
                    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                    $vpageNew=true;
                }
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                $vrptProductsSales->printMultiLine(75, 15, "Folio de Venta:" , 9, "B", "L", 0);
                $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                $vrptProductsSales->x=125;
                $vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale, 8, "", "L", 0);
                $vrptProductsSales->x=250;
                $vrptProductsSales->printMultiLine(70, 15, "Tipo de Venta:" , 9, "B", "L", 0);
                $vrptProductsSales->x=320;
                $vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->operationType->operationType, 8, "", "L", 0);
                $vrptProductsSales->x=420;
                $vrptProductsSales->printMultiLine(80, 15, "Total:" , 9, "B", "R", 0);
                $vrptProductsSales->x=500;
                $vrptProductsSales->printMultiLine(70, 15, "$ " . number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice, 2, ".", ","), 8, "", "R", 0);
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                $vrptProductsSales->printMultiLine(40, 15, "Cliente:" , 9, "B", "L", 0);
                if ( $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->personType->idPersonType==1 ){
                    $vflCustomer= new clspFLIndividualCustomer();
                    $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                    $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                    clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                    $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                }
                else{
                    $vflCustomer= new clspFLBusinessCustomer();
                    $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                    $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                    clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                    $vcustomerName=$vflCustomer->businessName;
                }
                $vrptProductsSales->x=90;
                $vrptProductsSales->printMultiLine(330, 15, $vcustomerName, 8, "", "L", 0);
                $vrptProductsSales->x=420;
                $vrptProductsSales->printMultiLine(80, 15, "Fecha de Venta:", 9, "B", "R", 0);
                $vrptProductsSales->x=500;
                $vrptProductsSales->printMultiLine(70, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->productSaleDate, 8, "", "R", 0);
                $vsaleTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
            }
            if( $vproductsSalesDetailsTotal>0 ){
                $vrptProductsSales->x=420;  $vrptProductsSales->y+=20;
                $vrptProductsSales->printMultiLine(80, 15, "Gran Total:", 9, "B", "R", 0);
                $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                $vrptProductsSales->x=500;
                $vrptProductsSales->printMultiLine(70, 15, "$ " . number_format($vsaleTotal, 2, ".", ","), 8, "B", "R", 0);
            }

            unset($vsaleTotal);
        }

        unset($vproductsSalesDetails);
    }
    $vrptProductsSales->showPage();

    unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
          $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de ventas de productos (Montos)";
}

function showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsSales, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsSales->x=50;	$vrptProductsSales->y=60;
        $vrptProductsSales->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Reporte de Ventas de Productos (Ruta) por Montos\"" , 10, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Periodo del ". trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]) . "\"" , 8, "B", "C", 0);

        $vrptProductsSales->x=50;	$vrptProductsSales->y+=12;
        $vrptProductsSales->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showFooter($vrptProductsSales, $vpageNumber)
 {
    try{
        $vrptProductsSales->x=50;  $vrptProductsSales->y=715;
        $vrptProductsSales->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsSales->y=105;
        $vrptProductsSales->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

?>
