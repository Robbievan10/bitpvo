<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
date_default_timezone_set('America/Mexico_City');


function showRoutesList()
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vroutesDeliverers= new clscFLRouteDealer();
        clscBLRouteDealer::queryToDataBase($vroutesDeliverers, "WHERE c_routedealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vroutesDeliverersTotal=clscBLRouteDealer::total($vroutesDeliverers);
        $vJSON='[{"text":"--Todas--", "value":"0"}';
        for ($vi=0; $vi<$vroutesDeliverersTotal; $vi++){
            $vJSON.=', {"text":"' . $vroutesDeliverers->routesDeliverers[$vi]->route->route .
                    '", "value":' . $vroutesDeliverers->routesDeliverers[$vi]->route->idRoute . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vroutesDeliverers, $vroutesDeliverersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar las rutas asignadas a repartidores, intente de nuevo");
	}
	
	return $vresponse;
 }

function printProductsSales($vfilterForm, $vreportType)
 {
    $vresponse= new xajaxResponse();
	   
	try{
	    switch ($vreportType){   
            case 1:  $vurl ="./controller/products-sales-route-amount1-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
                     $vurl.="&vproductsSaleType=" . $vfilterForm["cmrproductsSaleType"] . "&vidRoute=" . $vfilterForm["cmbproductsSalesRoute"];
                     break;
            case 2:  $vurl ="./controller/products-sales-route-amount2-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
                     $vurl.="&vidRoute=" . $vfilterForm["cmbproductsSalesRoute"];
                     break;
            default: $vurl ="./controller/products-sales-route-summary1-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
                     $vurl.="&vidRoute=" . $vfilterForm["cmbproductsSalesRoute"];
                     break;
        }
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vfilterForm, $vreportType, $vurl);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir las ventas de productos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showRoutesList");
$vxajax->register(XAJAX_FUNCTION, "printProductsSales");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>