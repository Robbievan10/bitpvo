<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");
date_default_timezone_set('America/Mexico_City');


function showExpenseTypesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter="WHERE c_expensetype.id_enterprise=" . $_SESSION['idEnterprise'];
        $vexpenseTypes= new clscFLExpenseType();
        clscBLExpenseType::queryToDataBase($vexpenseTypes, $vfilter);
        $vexpenseTypesTotal=clscBLExpenseType::total($vexpenseTypes);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vexpenseTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vexpenseTypes->expenseTypes[$vi]->expenseType .
                    '", "value":' . $vexpenseTypes->expenseTypes[$vi]->idExpenseType . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vfilter, $vexpenseTypes, $vexpenseTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de gasto, intente de nuevo");
	}
    
	return $vresponse;
 }

function showExpensesList()
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter ="WHERE c_expense.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vexpenses= new clscFLExpense();
        clscBLExpense::queryToDataBase($vexpenses, $vfilter);
        $vexpensesTotal=clscBLExpense::total($vexpenses);
        for ($vexpenseNumber=0; $vexpenseNumber<$vexpensesTotal; $vexpenseNumber++){
            if ( $vexpenseNumber==0 ){
                $vdata='<table id="vgrdexpensesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vexpenseName" style="text-align:center; font-weight:bold;">Gasto</th>
                                    <th data-field="vexpenseTypeName" style="text-align:center; font-weight:bold;">Tipo</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrexpense" onClick="showExpenseData('. $vexpenses->expenses[$vexpenseNumber]->idExpense . ');" title="Seleccionar Gasto" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vexpenses->expenses[$vexpenseNumber]->expense . '</td>';
            $vdata.='	<td>' . $vexpenses->expenses[$vexpenseNumber]->expenseType->expenseType . '</td>';
            $vdata.="</tr>";
        }
        if ( $vexpenseNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vexpensesList", "innerHTML", $vdata);
            $vresponse->script("setExpensesList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen gastos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vexpensesList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vexpenses, $vexpensesTotal, $vexpenseNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los gastos, intente de nuevo");
	}
	
	return $vresponse;
 }

function showExpenseData($vidExpense)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflExpense= new clspFLExpense();
		$vflExpense->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpense->idExpense=$vidExpense;
        switch(clspBLExpense::queryToDataBase($vflExpense)){
            case 0: $vresponse->alert("Los datos del gasto no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtconcept", $vresponse);
                    $vtext->setValue($vflExpense->expense);
                    $vresponse->script("vcmbExpenseTypesList.value(" . $vflExpense->expenseType->idExpenseType . ");");
                    $vstring= new clspString($vflExpense->description);
				    $vtextArea= new clspTextArea("txtdescription", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    $vresponse->script("enableExpenseButtons();");
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflExpense);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del gasto, intente de nuevo");
	}
    
    unset($vidExpense);
	return $vresponse;
 }

function addExpenseData($vexpenseForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflExpense= new clspFLExpense();
        $vflExpense->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpense->expenseType->idExpenseType=(int)($vexpenseForm["cmbexpenseTypesList"]);
        $vflExpense->expense=trim($vexpenseForm["txtconcept"]);
        $vflExpense->description=trim($vexpenseForm["txtdescription"]);
        switch(clspBLExpense::addToDataBase($vflExpense)){
            case 0:  $vresponse->alert("Imposible registrar el gasto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidExpense=" . $vflExpense->idExpense);
                     $vresponse->script("showExpensesList(0);");
                     $vresponse->alert("Los datos del gasto <" . $vflExpense->expense . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflExpense);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el gasto, intente de nuevo");
	}
	
    unset($vexpenseForm);
	return $vresponse;
 }

function updateExpenseData($vidExpense, $vexpenseForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflExpense= new clspFLExpense();
        $vflExpense->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpense->idExpense=$vidExpense;
        $vflExpense->expenseType->idExpenseType=(int)($vexpenseForm["cmbexpenseTypesList"]);
        $vflExpense->expense=trim($vexpenseForm["txtconcept"]);
        $vflExpense->description=trim($vexpenseForm["txtdescription"]);
        switch(clspBLExpense::updateInDataBase($vflExpense)){
            case 0: $vresponse->alert("Ningún dato se ha modificado del gasto");
                    break;
            case 1: $vresponse->script("showExpensesList(1);");
                    $vresponse->alert("Los datos del gasto han sido modificados correctamente");
                    break;
        }
        
        unset($vflExpense);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del gasto, intente de nuevo");
	}
	
    unset($vidExpense, $vexpenseForm);
	return $vresponse;
 }

function deleteExpenseData($vidExpense)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflExpense= new clspFLExpense();
		$vflExpense->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpense->idExpense=$vidExpense;
        switch(clspBLExpense::deleteInDataBase($vflExpense)){
            case 0:  $vresponse->alert("Imposible eliminar el gasto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidExpense=0;");
                     $vresponse->script("cleanExpenseFormFields();");
                     $vresponse->script("showExpensesList(0);");
                     $vresponse->alert("Los datos del gasto han sido eliminados correctamente");
                     break;
        }
        
        unset($vflExpense);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del gasto, intente de nuevo");
	}
	
    unset($vidExpense);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showExpenseTypesList");
$vxajax->register(XAJAX_FUNCTION, "showExpensesList");
$vxajax->register(XAJAX_FUNCTION, "showExpenseData");
$vxajax->register(XAJAX_FUNCTION, "addExpenseData");
$vxajax->register(XAJAX_FUNCTION, "updateExpenseData");
$vxajax->register(XAJAX_FUNCTION, "deleteExpenseData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>