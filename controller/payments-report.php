<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");

require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");
date_default_timezone_set('America/Mexico_City');


function showProvidersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vindividualsProviders= new clscFLIndividualProvider();
        $vfilter="WHERE c_individualprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualProvider::queryToDataBase($vindividualsProviders, $vfilter);
        $vindividualsProvidersTotal=clscBLIndividualProvider::total($vindividualsProviders);
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsProvidersTotal; $vi++){
            $vJSON.=', {"text":"' . $vindividualsProviders->individualsProviders[$vi]->name . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->firstName . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->lastName .
                    '", "value":' . $vindividualsProviders->individualsProviders[$vi]->idProvider . '}';
        }
        
        $vbusinessProviders= new clscFLBusinessProvider();
        $vfilter="WHERE c_businessprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessProvider::queryToDataBase($vbusinessProviders, $vfilter);
        $vbusinessProvidersTotal=clscBLBusinessProvider::total($vbusinessProviders);
        for ($vi=0; $vi<$vbusinessProvidersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessProviders->businessProviders[$vi]->businessName .
                    '", "value":' . $vbusinessProviders->businessProviders[$vi]->idProvider . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vindividualsProviders, $vfilter, $vindividualsProvidersTotal, $vJSON, $vi, $vbusinessProviders, $vbusinessProvidersTotal);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los provedores, intente de nuevo");
	}

	return $vresponse;
 }
 
function showCustomersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vindividualsCustomers->individualsCustomers[$vi]->name . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->firstName . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName .
                    '", "value":' . $vindividualsCustomers->individualsCustomers[$vi]->idCustomer . '}';
        }
        
        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessCustomers->businessCustomers[$vi]->businessName .
                    '", "value":' . $vbusinessCustomers->businessCustomers[$vi]->idCustomer . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vindividualsCustomers, $vfilter, $vindividualsCustomersTotal, $vJSON, $vi, $vbusinessCustomers, $vbusinessCustomersTotal);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
	}
	
	return $vresponse;
 }

function showDeliverersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vdeliverers= new clscFLDealer();
        clscBLDealer::queryToDataBase($vdeliverers, "WHERE c_dealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vdeliverersTotal=clscBLDealer::total($vdeliverers);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vdeliverersTotal; $vi++){
            $vJSON.=', {"text":"' . $vdeliverers->deliverers[$vi]->name . ' ' .
                                    $vdeliverers->deliverers[$vi]->firstName . ' ' .
                                    $vdeliverers->deliverers[$vi]->lastName .
                    '", "value":' . $vdeliverers->deliverers[$vi]->idDealer . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vdeliverers, $vdeliverersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los repartidores, intente de nuevo");
	}

	return $vresponse;
 }

function printPayments($vfilterForm)
 {
    $vresponse= new xajaxResponse();
	$userType = 0;

	try{


    $vmenu='<ul class="nav-notification clearfix">';
    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }




        if ( ((int)($vfilterForm["cmrpaymentType"]))==1){
            if($userType!=2){
                //$vresponse->alert("Acceso denegado");
            }else{
                $vidListData=$vfilterForm["cmbprovidersList"];
            }

        }
        else if ( ((int)($vfilterForm["cmrpaymentType"]))==2 ){
            $vidListData=$vfilterForm["cmbcustomersList"];
        }
        else{
            if($userType!=2){
                //$vresponse->alert("Acceso denegado");
            }else{
            $vidListData=$vfilterForm["cmbdeliverersList"];
            }
        }
       




        $vurl ="./controller/payments-report-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
        $vurl.="&vpaymentType=" . $vfilterForm["cmrpaymentType"] . "&vidListData=" . $vidListData;
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vurl);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir los pagos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProvidersList");
$vxajax->register(XAJAX_FUNCTION, "showCustomersList");
$vxajax->register(XAJAX_FUNCTION, "showDeliverersList");
$vxajax->register(XAJAX_FUNCTION, "printPayments");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>