<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
date_default_timezone_set('America/Mexico_City');


try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="depositos-bancarios";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptBanksDeposit= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptBanksDeposit->addPage();
    
    $vreportSubtitle="Del " . trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]);
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptBanksDeposit, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
    
    $vamountTotal=0;
    $vfilter ="WHERE p_bankdeposit.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    $vfilter.="AND (p_bankdeposit.flddepositDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
    if ( (int)($_GET["vbank"])!=0 ){
        $vfilter.="AND p_bankdeposit.id_bank=" . (int)($_GET["vbank"]) . " ";
    }
    $vbanksDeposits= new clscFLBankDeposit();
    clscBLBankDeposit::queryToDataBase($vbanksDeposits, $vfilter);
    $vbanksDepositsTotal=clscBLBankDeposit::total($vbanksDeposits);
    for ($vbankDepositNumber=0; $vbankDepositNumber<$vbanksDepositsTotal; $vbankDepositNumber++){
        if( $vrptBanksDeposit->getY()>=640 ){
            $vrptBanksDeposit->addPage();
            showPage($vrptBanksDeposit, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
            $vpageNew=true;
        }
        $vrptBanksDeposit->x=50;   $vrptBanksDeposit->y+=10;
        $vrptBanksDeposit->printMultiLine(32, 20, "Fecha:" , 8, "B", "L", 0);
        $vrptBanksDeposit->x=82;
        $vrptBanksDeposit->printMultiLine(68, 20, $vbanksDeposits->banksDeposits[$vbankDepositNumber]->depositDate, 7, "", "L", 0);
        $vrptBanksDeposit->x=150;
        $vrptBanksDeposit->printMultiLine(33, 20, "Banco:" , 8, "B", "L", 0);
        $vrptBanksDeposit->x=183;
        $vrptBanksDeposit->printMultiLine(127, 20, $vbanksDeposits->banksDeposits[$vbankDepositNumber]->bank->name, 7, "", "L", 0);
        $vrptBanksDeposit->x=310;
        $vrptBanksDeposit->printMultiLine(68, 20, "N�mero de Cta.:" , 8, "B", "L", 0);
        $vrptBanksDeposit->x=378;
        $vrptBanksDeposit->printMultiLine(92, 20, $vbanksDeposits->banksDeposits[$vbankDepositNumber]->accountNumber, 7, "", "L", 0);
        $vrptBanksDeposit->x=470;
        $vrptBanksDeposit->printMultiLine(33, 20, "Monto:" , 8, "B", "L", 0);
        $vrptBanksDeposit->x=503;
        $vamount=$vbanksDeposits->banksDeposits[$vbankDepositNumber]->amount;
        $vamountTotal+=$vamount;
        $vrptBanksDeposit->printMultiLine(67, 20,"$ " . number_format($vamount, 2, ".", ","), 7, "", "R", 0);
        $vrptBanksDeposit->x=50;   $vrptBanksDeposit->y+=15;
        $vrptBanksDeposit->printMultiLine(59, 20, "Responsable:" , 8, "B", "L", 0);
        $vrptBanksDeposit->x=109;
        $vrptBanksDeposit->printMultiLine(201, 20, $vbanksDeposits->banksDeposits[$vbankDepositNumber]->responsibleName, 7, "", "L", 0);
        $vrptBanksDeposit->x=310;
        $vrptBanksDeposit->printMultiLine(50, 20, "Referencia:" , 8, "B", "L", 0);
        $vrptBanksDeposit->x=360;
        $vrptBanksDeposit->printMultiLine(210, 20, $vbanksDeposits->banksDeposits[$vbankDepositNumber]->referenceName, 7, "", "L",  0);
        $vrptBanksDeposit->y+=20;
        $vrptBanksDeposit->printLine(50, $vrptBanksDeposit->y, 570, $vrptBanksDeposit->y, 0.2);
    }
    if( $vbanksDepositsTotal>0 ){
        $vrptBanksDeposit->x=50;  $vrptBanksDeposit->y+=10;
        $vrptBanksDeposit->printMultiLine(453, 15, "Total:", 9, "B", "R", 0);
        $vrptBanksDeposit->x=503;
        $vrptBanksDeposit->printMultiLine(67, 15, "$" . number_format($vamountTotal, 2, ".", ","), 8, "B", "R", 0);
    }
    $vrptBanksDeposit->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptBanksDeposit, $vreportSubtitle,
          $vflEnterpriseUser, $vamountTotal, $vfilter, $vbanksDeposits, $vbanksDepositsTotal, $vbankDepositNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de dep�sitos bancarios";
}

function showPage($vrptBanksDeposit, $vflEnterpriseUser, $vdate, $vreportSubtitle, $vpageNumber)
 {
    try{
        showHeader($vrptBanksDeposit, $vflEnterpriseUser, $vdate, $vreportSubtitle);
        showFooter($vrptBanksDeposit, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptBanksDeposit, $vflEnterpriseUser, $vdate, $vreportSubtitle)
 {
    try{
        $vrptBanksDeposit->x=50;	$vrptBanksDeposit->y=60;
        $vrptBanksDeposit->printMultiLine(520, 20, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptBanksDeposit->x=50;	$vrptBanksDeposit->y+=14;
        $vrptBanksDeposit->printMultiLine(520, 20, "\"Reporte de Dep�sitos Bancarios\"" , 10, "B", "C", 0);
        $vrptBanksDeposit->x=50;	$vrptBanksDeposit->y+=14;
        $vrptBanksDeposit->printMultiLine(520, 20, $vreportSubtitle, 10, "B", "C", 0);
        $vrptBanksDeposit->x=50;	$vrptBanksDeposit->y+=12;
        $vrptBanksDeposit->printMultiLine(520, 20, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptBanksDeposit, $vpageNumber)
 {
    try{
        $vrptBanksDeposit->x=50;  $vrptBanksDeposit->y=715;
        $vrptBanksDeposit->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptBanksDeposit->y=120;
        $vrptBanksDeposit->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>