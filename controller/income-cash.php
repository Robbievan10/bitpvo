<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");


require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
date_default_timezone_set('America/Mexico_City');


function printIncomeExpensesCash($vfilterForm)
 {
    $vresponse= new xajaxResponse();
       
    try{
        $vurl ="./controller/income-cash-rtp.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vurl);
       }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de imprimir los pagos, intente de nuevo");
    }
    
    unset($vfilterForm);
    return $vresponse;
 }



function report(){
    $vresponse= new xajaxResponse();
       
    $vresponse->alert("reporte");
    return $vresponse;
}


function showRoutesList()
 {
    $vresponse= new xajaxResponse();
    //$vroute = 0;
    //$vcashMovement;
    /*try{
        $vcashMovement=getOpenCash();
        if ( $vcashMovement!=null ){
            $vcashMovement = $vcashMovement->cash->cash;
        }
        else{
            $vcashMovement="";
        }
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de obtener el nombre de la caja aperturada, intente de nuevo");
        $vresponse->setReturnValue("");
    }

*/
/*    try{

    $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND c_route.fldroute= '" . $vcashMovement . "' ";

    $vroutes= new clscFLRoute();
    clscBLRoute::queryToDataBase($vroutes, $vfilter, 1);
    $vroutesTotal=clscBLRoute::total($vroutes);




        $vroute = $vroutes->routes[0]->idRoute; 
        
    }
    catch (Exception $vexception){
                $vresponse->setReturnValue("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }


*/
    /*
    try{



        $vfilter ="WHERE c_routecustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_routecustomer.id_route=" . $vroute;
        $vroutesCustomers= new clscFLRouteCustomer();
        clscBLRouteCustomer::queryToDataBase($vroutesCustomers, $vfilter);
        $vroutesCustomersTotal=clscBLRouteCustomer::total($vroutesCustomers);
        
        
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }

*/




    
    try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){
                $vJSON.=', {"text":"' . strtoupper($vindividualsCustomers->individualsCustomers[$vi]->name) . ' ' .
                                    strtoupper($vindividualsCustomers->individualsCustomers[$vi]->firstName) . ' ' .strtoupper(
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName) .'-'. strtoupper($vindividualsCustomers->individualsCustomers[$vi]->locality).
                    '", "value":' . $vindividualsCustomers->individualsCustomers[$vi]->idCustomer . '}';
            

        }
        
        
        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessCustomers->businessCustomers[$vi]->businessName .
                    '", "value":' . $vbusinessCustomers->businessCustomers[$vi]->idCustomer . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
        unset($vindividualsCustomers, $vfilter, $vindividualsCustomersTotal, $vJSON, $vi, $vbusinessCustomers, $vbusinessCustomersTotal);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }
    
    return $vresponse;
 }



function printProductsSales($vfilterForm, $vreportType)
 {
    $vresponse= new xajaxResponse();
       
    try{
        switch ($vreportType){   
            case 1:  $vurl ="./controller/products-sales-customer-amount1-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
                     $vurl.="&vproductsSaleType=" . $vfilterForm["cmrproductsSaleType"] . "&vidRoute=" . $vfilterForm["cmbproductsSalesRoute"];
                     break;
            case 2:  $vurl ="./controller/products-sales-customer-amount2-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
                     $vurl.="&vidRoute=" . $vfilterForm["cmbproductsSalesRoute"];
                     break;
            default: $vurl ="./controller/products-sales-customer-summary1-rpt.php?vstartDate=" . $vfilterForm["dtpckrstartDate"] . "&vendDate=" . $vfilterForm["dtpckrendDate"];
                     $vurl.="&vidRoute=" . $vfilterForm["cmbproductsSalesRoute"];
                     break;
        }
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
        
        unset($vfilterForm, $vreportType, $vurl);
       }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de imprimir las ventas de productos, intente de nuevo");
    }
    
    unset($vfilterForm);
    return $vresponse;
 }

function exit_()
 {
    $vresponse= new xajaxResponse();
    
    session_destroy();
    $vresponse->redirect("./");
    
    return $vresponse;
 }

$vxajax->register(XAJAX_FUNCTION, "showRoutesList");
$vxajax->register(XAJAX_FUNCTION, "report");
$vxajax->register(XAJAX_FUNCTION, "printIncomeExpensesCash");
$vxajax->register(XAJAX_FUNCTION, "showRoutesList");
$vxajax->register(XAJAX_FUNCTION, "printProductsSales");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>
