<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLState.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLState.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");
date_default_timezone_set('America/Mexico_City');


function showStatesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vstates= new clscFLState();
        clscBLState::queryToDataBase($vstates, "");
        $vstatesTotal=clscBLState::total($vstates);
	
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vstatesTotal; $vi++){
            $vJSON.=', {"text":"' . $vstates->states[$vi]->state .
                    '", "value":' . $vstates->states[$vi]->idState . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vstates, $vstatesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados, intente de nuevo");
	}

	return $vresponse;
 }

function showMunicipalitiesList($vidState)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vmunicipalities= new clscFLMunicipality();
        clscBLMunicipality::queryToDataBase($vmunicipalities, "WHERE c_municipality.id_state=" . $vidState);
        $vmunicipalitiesTotal=clscBLMunicipality::total($vmunicipalities);
	
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vmunicipalitiesTotal; $vi++){
            $vJSON.=', {"text":"' . $vmunicipalities->municipalities[$vi]->municipality .
                    '", "value":' . $vmunicipalities->municipalities[$vi]->idMunicipality . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vmunicipalities, $vmunicipalitiesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados, intente de nuevo");
	}
    
    unset($vidState);
	return $vresponse;
 }

function showCustomerData($vidCustomer)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflCustomerData= new clspFLCustomer();
		$vflCustomerData->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCustomerData->idCustomer=$vidCustomer;
        clspBLCustomer::queryToDataBase($vflCustomerData);
        if ( $vflCustomerData->personType->idPersonType==1 ){
            $vflCustomer= new clspFLIndividualCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflCustomerData->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflCustomerData->idCustomer;
            $vqueryStatus=clspBLIndividualCustomer::queryToDataBase($vflCustomer);
        }
        else{
            $vflCustomer= new clspFLBusinessCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflCustomerData->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflCustomerData->idCustomer;
            $vqueryStatus=clspBLBusinessCustomer::queryToDataBase($vflCustomer);
        }
        switch($vqueryStatus){
            case 0: $vresponse->alert("Los datos del cliente no se encuentran registrados");
                    break;
            case 1: $vresponse->script('xajax.$("cmrindividual").disabled=true;
                                        xajax.$("cmrbusiness").disabled=true;');
                    if ( $vflCustomerData->personType->idPersonType==1 ){
                        $vresponse->script('xajax.$("cmrindividual").checked=true;
                                            setPersonData();');
                        $vtext= new clspText("txtkey", $vresponse);
                        $vtext->setValue($vflCustomer->key);
                        $vtext= new clspText("txtname", $vresponse);
                        $vtext->setValue($vflCustomer->name);
                        $vtext= new clspText("txtfirstName", $vresponse);
                        $vtext->setValue($vflCustomer->firstName);
                        $vtext= new clspText("txtlastName", $vresponse);
                        $vtext->setValue($vflCustomer->lastName);
                    }
                    else{
                        $vresponse->script('xajax.$("cmrbusiness").checked=true;
                                            setPersonData();');
                        $vtext= new clspText("txtname", $vresponse);
                        $vtext->setValue($vflCustomer->businessName);
                    }
                    $vtext= new clspText("txtrfc", $vresponse);
                    $vtext->setValue($vflCustomer->rfc);
                    $vtext= new clspText("txthomoclave", $vresponse);
                    $vtext->setValue($vflCustomer->homoclave);
                    $vresponse->script("vcmbStatesList.value(" . $vflCustomer->municipality->state->idState . "); showMunicipalitiesList();
                                        vcmbMunicipalitiesList.value(" . $vflCustomer->municipality->idMunicipality . ");");
                    $vtext= new clspText("txtlocality", $vresponse);
                    $vtext->setValue($vflCustomer->locality);
                    $vtext= new clspText("txtstreet", $vresponse);
                    $vtext->setValue($vflCustomer->street);
                    $vtext= new clspText("txtnumber", $vresponse);
                    $vtext->setValue($vflCustomer->number);
                    $vtext= new clspText("txtpostCode", $vresponse);
                    $vtext->setValue($vflCustomer->postCode);
                    $vtext= new clspText("txtphoneNumber", $vresponse);
                    $vtext->setValue($vflCustomer->phoneNumber);
                    $vtext= new clspText("txtmovilNumber", $vresponse);
                    $vtext->setValue($vflCustomer->movilNumber);
                    $vtext= new clspText("txtemail", $vresponse);
                    $vtext->setValue($vflCustomer->email);
                    $vstring= new clspString($vflCustomer->observation);
				    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflCustomerData, $vflCustomer, $vqueryStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del cliente, intente de nuevo");
	}
    
    unset($vidCustomer);
	return $vresponse;
 }

function addCustomerData($vpersonType, $vcustomerKey, $vcustomerForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        if ( $vpersonType==1 ){
            $vflCustomer= new clspFLIndividualCustomer();
            $vflCustomer->name=trim($vcustomerForm["txtname"]);
            $vflCustomer->firstName=trim($vcustomerForm["txtfirstName"]);
            $vflCustomer->lastName=trim($vcustomerForm["txtlastName"]);
            $vname= $vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;          
        }
        else{
            $vflCustomer= new clspFLBusinessCustomer();
            $vflCustomer->businessName=trim($vcustomerForm["txtname"]);
            $vname= $vflCustomer->businessName;   
        }
        $vflCustomer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCustomer->personType->idPersonType=$vpersonType;
        $vflCustomer->municipality->state->idState=(int)($vcustomerForm["cmbstatesList"]);
        $vflCustomer->municipality->idMunicipality=(int)($vcustomerForm["cmbmunicipalitiesList"]);
        $vflCustomer->key=trim($vcustomerKey);
        $vflCustomer->rfc=trim($vcustomerForm["txtrfc"]);
        $vflCustomer->homoclave=trim($vcustomerForm["txthomoclave"]);
        $vflCustomer->locality=trim($vcustomerForm["txtlocality"]);
        $vflCustomer->street=trim($vcustomerForm["txtstreet"]);
        $vflCustomer->number=trim($vcustomerForm["txtnumber"]);
        $vflCustomer->postCode=trim($vcustomerForm["txtpostCode"]);
        $vflCustomer->phoneNumber=trim($vcustomerForm["txtphoneNumber"]);
        $vflCustomer->movilNumber=trim($vcustomerForm["txtmovilNumber"]);
        $vflCustomer->email=trim($vcustomerForm["txtemail"]);
        $vflCustomer->observation=trim($vcustomerForm["txtobservation"]);
        if ( $vflCustomer->personType->idPersonType==1 ){
            $vaddStatus=clspBLIndividualCustomer::addToDataBase($vflCustomer);
        }
        else{
            $vaddStatus=clspBLBusinessCustomer::addToDataBase($vflCustomer);
        }
        switch($vaddStatus){
            case -2: $vresponse->alert("Imposible registrar al cliente, el número ya se encuentra registrado");
                     break;
            case -1: $vresponse->alert("Imposible registrar al cliente, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible registrar al cliente físico, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidCustomer=" . $vflCustomer->idCustomer);
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Cliente");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Cliente");
                     $vresponse->script('xajax.$("cmrindividual").disabled=true;
                                         xajax.$("cmrbusiness").disabled=true;');
                     $vresponse->alert("Los datos del cliente <" . $vname . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflCustomer, $vname, $vaddStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar al cliente, intente de nuevo" . $vexception->getMessage());
	}
	
    unset($vpersonType, $vcustomerForm);
	return $vresponse;
 }

function updateCustomerData($vidCustomer, $vcustomerKey,  $vcustomerForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflCustomerData= new clspFLCustomer();
		$vflCustomerData->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCustomerData->idCustomer=$vidCustomer;
        clspBLCustomer::queryToDataBase($vflCustomerData);
        if ( $vflCustomerData->personType->idPersonType==1 ){
            $vflCustomer= new clspFLIndividualCustomer();
            $vflCustomer->name=trim($vcustomerForm["txtname"]);
            $vflCustomer->firstName=trim($vcustomerForm["txtfirstName"]);
            $vflCustomer->lastName=trim($vcustomerForm["txtlastName"]);            
        }
        else{
            $vflCustomer= new clspFLBusinessCustomer();
            $vflCustomer->businessName=trim($vcustomerForm["txtname"]);            
        }
        $vflCustomer->enterprise->idEnterprise=$vflCustomerData->enterprise->idEnterprise;
        $vflCustomer->idCustomer=$vflCustomerData->idCustomer;
        $vflCustomer->personType->idPersonType=$vflCustomerData->personType->idPersonType;
        $vflCustomer->municipality->state->idState=(int)($vcustomerForm["cmbstatesList"]);
        $vflCustomer->municipality->idMunicipality=(int)($vcustomerForm["cmbmunicipalitiesList"]);
        $vflCustomer->key=trim($vcustomerKey);
        $vflCustomer->rfc=trim($vcustomerForm["txtrfc"]);
        $vflCustomer->homoclave=trim($vcustomerForm["txthomoclave"]);
        $vflCustomer->locality=trim($vcustomerForm["txtlocality"]);
        $vflCustomer->street=trim($vcustomerForm["txtstreet"]);
        $vflCustomer->number=trim($vcustomerForm["txtnumber"]);
        $vflCustomer->postCode=trim($vcustomerForm["txtpostCode"]);
        $vflCustomer->phoneNumber=trim($vcustomerForm["txtphoneNumber"]);
        $vflCustomer->movilNumber=trim($vcustomerForm["txtmovilNumber"]);
        $vflCustomer->email=trim($vcustomerForm["txtemail"]);
        $vflCustomer->observation=trim($vcustomerForm["txtobservation"]);
        if ( $vflCustomerData->personType->idPersonType==1 ){
            $vupdateStatus=clspBLIndividualCustomer::updateInDataBase($vflCustomer);
        }
        else{
            $vupdateStatus=clspBLBusinessCustomer::updateInDataBase($vflCustomer);
        }
        switch($vupdateStatus){
            case -1: $vresponse->alert("Imposible modificar el número del cliente, ya se encuentra registrado");
                     break;
            case 0:  $vresponse->alert("Ningún dato se ha modificado del cliente");
                     break;
            case 1:  $vresponse->alert("Los datos del cliente han sido modificados correctamente");
                     break;
        }
        
        unset($vflCustomerData, $vflCustomer, $vupdateStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del cliente, intente de nuevo");
	}
	
    unset($vidCustomer, $vcustomerForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showStatesList");
$vxajax->register(XAJAX_FUNCTION, "showMunicipalitiesList");
$vxajax->register(XAJAX_FUNCTION, "showCustomerData");
$vxajax->register(XAJAX_FUNCTION, "addCustomerData");
$vxajax->register(XAJAX_FUNCTION, "updateCustomerData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>