<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
date_default_timezone_set('America/Mexico_City');


function showProductLowTypesList($vtype)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vproductLowTypes= new clscFLProductLowType();
        clscBLProductLowType::queryToDataBase($vproductLowTypes,"");
        $vproductLowTypesTotal=clscBLProductLowType::total($vproductLowTypes);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        if ( $vtype==2 ){
            $vJSON='[{"text":"--Todos--", "value":"0"}';
        }
        for ($vi=0; $vi<$vproductLowTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vproductLowTypes->productLowTypes[$vi]->productLowType .
                    '", "value":' . $vproductLowTypes->productLowTypes[$vi]->idProductLowType . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vtype, $vproductLowTypes, $vproductLowTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de bajas de productos, intente de nuevo");
	}
    
	return $vresponse;
 }

function showProductsList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vproducts= new clscFLProduct();
        clscBLProduct::queryToDataBase($vproducts, "WHERE c_product.id_enterprise=" . $_SESSION['idEnterprise']);
        $vproductsTotal=clscBLProduct::total($vproducts);
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vproductsTotal; $vi++){
            $vJSON.=', {"text":"' . strtoupper($vproducts->products[$vi]->name).
                    '", "value":' . $vproducts->products[$vi]->idProduct .
                    ', "key":"' . $vproducts->products[$vi]->key .
                    '", "barCode":"' . $vproducts->products[$vi]->barCode . '"}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vproducts, $vproductsTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los productos, intente de nuevo");
	}

	return $vresponse;
 }

function showProductsLowFiltersList($vfilterForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vfilter ="WHERE p_productlow.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_productlow.fldproductLowDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }
        if ( strcmp(trim($vfilterForm["txtproductLowFolio"]), "")!=0 ){
            $vfilter.="AND p_productlow.id_productLow LIKE '%" . trim($vfilterForm["txtproductLowFolio"]) . "%' ";
        }
        if ( (int)($vfilterForm["cmbproductLowTypeList1"])!=0 ){
            $vfilter.="AND p_productlow.id_productLowType=" . (int)($vfilterForm["cmbproductLowTypeList1"]) . " ";
        }
        $vproductsLow= new clscFLProductLow();
        clscBLProductLow::queryToDataBase($vproductsLow, $vfilter);
        $vproductsLowTotal=clscBLProductLow::total($vproductsLow);
        for ($vproductsLowNumber=0; $vproductsLowNumber<$vproductsLowTotal; $vproductsLowNumber++){
            if ( $vproductsLowNumber==0 ){
                $vdata='<table id="vgrdproductsLowFiltersList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductLowFolio" style="text-align:center; font-weight:bold;">Folio</th>
                                    <th data-field="vproductLowDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vproductLowProvider" style="text-align:center; font-weight:bold;">Tipo</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproductsLow" onClick="showProductsLowData(\''. $vproductsLow->productsLow[$vproductsLowNumber]->idProductLow . '\');" title="Seleccionar Baja de Productos" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vproductsLow->productsLow[$vproductsLowNumber]->idProductLow. '</td>';
            $vdata.='	<td>' . $vproductsLow->productsLow[$vproductsLowNumber]->productLowDate . '</td>';
            $vdata.='	<td>' . $vproductsLow->productsLow[$vproductsLowNumber]->productLowType->productLowType . '</td>';
            $vdata.="</tr>";
        }
        if ( $vproductsLowNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsLowFiltersList", "innerHTML", $vdata);
            $vresponse->script("setProductsLowFiltersList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen bajas de productos registradas.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsLowFiltersList", "innerHTML", $vdata);
        }    
        
        unset($vfilter, $vproductsLow, $vproductsLowTotal, $vproductsLowNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar las bajas de productos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function showProductsLowList($vresponse)
 {
    try{
        $vtotals=0;
        $vproductsLow=$_SESSION['vproductsLowList'];
        $vproductsLowTotal=count($vproductsLow);
        for ($vi=0; $vi<$vproductsLowTotal; $vi++){
            if ( $vi==0 ){
                $vdata='<table id="vgrdproductsLowList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductName" style="text-align:center; font-weight:bold;">Producto</th>
                                    <th data-field="vproductAmount" style="text-align:center; font-weight:bold;">Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproduct" onClick="setProductId('. $vproductsLow[$vi][0] . ');" title="Seleccionar Producto" />
                            <span class="custom-radio"></span>
                            </td>';
            $vdata.='	<td>' . $vproductsLow[$vi][1]. '</td>';
            $vdata.='	<td>' . number_format($vproductsLow[$vi][2], 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vproductsLowTotal>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsLowList", "innerHTML", $vdata);
            $vresponse->script("setProductsLowList();
                                visThereListProducts=true;
                                vcmdDeleteProduct.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen productos en la baja.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsLowList", "innerHTML", $vdata);
            $vresponse->script("visThereListProducts=false;");
        }
    
        unset($vtotals, $vproductsLow, $vproductsLowTotal, $vi, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los productos de la baja, intente de nuevo");
	}  
 }

function showProductsLowData($vidProductLow)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflProductLow= new clspFLProductLow();
        $vflProductLow->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductLow->idProductLow=$vidProductLow;
        switch(clspBLProductLow::queryToDataBase($vflProductLow)){
            case 0: $vresponse->alert("Los datos de la entrada de productos no se encuentran registrados");
                    break;
            case 1: $vresponse->script("vidAction=0;
                                        xajax.$('cmdnewSaveProductsLow').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";                                
                                        enableProductsLowDataButtons(0);
                                        cleanProductsLowFormFields(1);
                                        disableProductsLowFormFields();
                                        disableAddProductButtons(1);
                                        vidProductLow='" . $vflProductLow->idProductLow . "';");
                    $vresponse->assign("vproductsLowFolio", "innerHTML", $vflProductLow->idProductLow);
                    $vtext= new clspText("dtpckrproductsLowDate", $vresponse);
                    $vtext->setValue($vflProductLow->productLowDate);                    
                    $vresponse->script("vcmbProductLowTypeList.value(" . $vflProductLow->productLowType->idProductLowType . ");");
                    
                    $vfilter ="WHERE p_productlowdetail.id_enterprise=" . $vflProductLow->enterprise->idEnterprise . " ";
                    $vfilter.="AND p_productlowdetail.id_productLow='" . $vflProductLow->idProductLow . "'";
                    $vproductsLowDetails= new clscFLProductLowDetail();
                    clscBLProductLowDetail::queryToDataBase($vproductsLowDetails, $vfilter);
                    $vlowProductsTotal=clscBLProductLowDetail::total($vproductsLowDetails);
                    $vproductsLow=array();
                    for($vi=0; $vi<$vlowProductsTotal; $vi++){
                        $vproduct=array();
                        array_push($vproduct, $vproductsLowDetails->productsLowDetails[$vi]->product->idProduct);
                        array_push($vproduct, $vproductsLowDetails->productsLowDetails[$vi]->product->name);
                        array_push($vproduct, $vproductsLowDetails->productsLowDetails[$vi]->lowAmount);
                        array_push($vproduct, $vproductsLowDetails->productsLowDetails[$vi]->lowWeightInKg);
                        array_push($vproduct, $vproductsLowDetails->productsLowDetails[$vi]->purchasePrice);
                        array_push($vproduct, $vproductsLowDetails->productsLowDetails[$vi]->salePrice);
                        array_push($vproductsLow, $vproduct);
                        
                        unset($vproduct);    
                    }
                    $_SESSION['vproductsLowList']=$vproductsLow;
                    showProductsLowList($vresponse);
                    
                    unset($vtext, $vfilter, $vproductsLowDetails, $vlowProductsTotal, $vproductsLow, $vi);
                    break;
        }
	   
       unset($vflProductLow);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la baja de productos, intente de nuevo");
	}
    
    unset($vidProductLow);
	return $vresponse;
 }
/*
function printProductsEntry($vidProductEntry)
 {
    $vresponse= new xajaxResponse();
	   
	try{
        $vurl="./controller/products-entry-rpt.php?vidProductEntry=" . $vidProductEntry ;
		$vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de imprimir la entrada de productos, intente de nuevo");
	}
	
    unset($vidProductEntry);
	return $vresponse;
 }
*/
function addProductsLowData($vproductsLowForm, $vproductsLowForm1)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflProductLow= new clspFLProductLow();
        $vflProductLow->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductLow->productLowType->idProductLowType=(int)($vproductsLowForm["cmbproductLowTypeList"]);
        $vflProductLow->productLowDate=trim($vproductsLowForm["dtpckrproductsLowDate"]);
        $vflProductLow->observation=trim($vproductsLowForm1["txtobservation"]);
        
        $vproductsLow=$_SESSION['vproductsLowList'];
        $vproductsLowTotal=count($vproductsLow);
        $vproductsLowDetails= new clscFLProductLowDetail();
        for($vi=0; $vi<$vproductsLowTotal; $vi++){
            $vflProductLowDetail= new clspFLProductLowDetail();
            $vflProductLowDetail->product->idProduct=(int)($vproductsLow[$vi][0]);
            $vflProductLowDetail->lowAmount=(float)($vproductsLow[$vi][2]);
            $vflProductLowDetail->lowWeightInKg=(float)($vproductsLow[$vi][3]);
            $vflProductLowDetail->purchasePrice=(float)($vproductsLow[$vi][4]);
            $vflProductLowDetail->salePrice=(float)($vproductsLow[$vi][5]);
            clscBLProductLowDetail::add($vproductsLowDetails, $vflProductLowDetail);
            
            unset($vflProductLowDetail);
        }
        if ( clspBLProductLow::addToDataBase($vflProductLow, $vproductsLowDetails)==1 ){
            $vresponse->script("vidAction=0;
                                vidProductLow='" . $vflProductLow->idProductLow . "';
                                xajax.$('cmdnewSaveProductsLow').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";
                                enableProductsLowDataButtons(0);
                                disableProductsLowFormFields();
                                disableAddProductButtons(1);");
            $vresponse->assign("vproductsLowFolio", "innerHTML", $vflProductLow->idProductLow);
            $vresponse->alert("La baja de productos ha sido registrada correctamente"); 
        }
        else{
            $vresponse->alert("Imposible registrar la baja de productos, intente de nuevo");
        }
        
        unset($vflProductLow, $vproductsLow, $vproductsLowTotal, $vproductsLowDetails, $vi);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar la baja de productos, intente de nuevo");
	}
	
    unset($vproductsLowForm);
	return $vresponse;
 }

function addProductToProductsLowList($vproductsAddForm)
 {
	$vresponse= new xajaxResponse();
    	   
	try{
        $vproductsLow=$_SESSION['vproductsLowList'];
        if ( getProductIndexOnList((int)($vproductsAddForm["cmbproductsList"]), $vproductsLow)==-1 ){
            $vflProduct= new clspFLProduct();
            $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
            $vflProduct->idProduct=(int)($vproductsAddForm["cmbproductsList"]);
            clspBLProduct::queryToDataBase($vflProduct);
            $vproduct= array();
            array_push($vproduct, $vflProduct->idProduct);
            array_push($vproduct, strtoupper($vflProduct->name));
            array_push($vproduct, (float)(str_replace(",", "", trim($vproductsAddForm["txtproductLowAmount"]))));
            array_push($vproduct, $vflProduct->weightInKg);
            array_push($vproduct, $vflProduct->purchasePrice);
            array_push($vproduct, $vflProduct->salePrice);
            
            array_push($vproductsLow, $vproduct);
            $_SESSION['vproductsLowList']=$vproductsLow;
            showProductsLowList($vresponse);

            unset($vflProduct, $vproduct);
        }
        else{
            $vresponse->alert("El producto ya se encuentra agregado a la lista de baja de productos.");
        }  
            
        unset($vproductsAddForm, $vproductsLow);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de agregar el producto a la baja, intente de nuevo");
	}
	
    unset($vproductsAddForm);
	return $vresponse;
 }

function updateProductsLowData($vidProductLow, $vproductsLowForm, $vproductsLowForm1)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProductLow= new clspFLProductLow();
        $vflProductLow->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductLow->idProductLow=$vidProductLow;
        $vflProductLow->productLowType->idProductLowType=(int)($vproductsLowForm["cmbproductLowTypeList"]);
        $vflProductLow->productLowDate=trim($vproductsLowForm["dtpckrproductsLowDate"]);
        $vflProductLow->observation=trim($vproductsLowForm1["txtobservation"]);
        
        $vproductsLow=$_SESSION['vproductsLowList'];
        $vproductsLowTotal=count($vproductsLow);
        $vproductsLowDetails= new clscFLProductLowDetail();
        for($vi=0; $vi<$vproductsLowTotal; $vi++){
            $vflProductLowDetail= new clspFLProductLowDetail();
            $vflProductLowDetail->product->idProduct=(int)($vproductsLow[$vi][0]);
            $vflProductLowDetail->lowAmount=(float)($vproductsLow[$vi][2]);
            $vflProductLowDetail->lowWeightInKg=(float)($vproductsLow[$vi][3]);
            $vflProductLowDetail->purchasePrice=(float)($vproductsLow[$vi][4]);
            $vflProductLowDetail->salePrice=(float)($vproductsLow[$vi][5]);
            clscBLProductLowDetail::add($vproductsLowDetails, $vflProductLowDetail);
            
            unset($vflProductLowDetail);
        }
        if ( clspBLProductLow::updateInDataBase($vflProductLow, $vproductsLowDetails)==1){
            $vresponse->alert("Los datos de la baja de productos han sido modificados correctamente");
            $vresponse->script("vidAction=0;
                                xajax.$('cmdnewSaveProductsLow').innerHTML=\"<span class='k-icon k-i-plus'></span>&nbsp;Nuevo\";                                
                                enableProductsLowDataButtons(0);
                                disableProductsLowFormFields();
                                disableAddProductButtons(1);");
        }
        else{
            $vresponse->alert("Imposible modificar los datos de la baja de productos, intente de nuevo" . $v);
        }
        
        unset($vflProductLow, $vproductsLow, $vproductsLowTotal, $vproductsEntriesDetails, $vi);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos de la baja de productos, intente de nuevo");
	}
	
    unset($vidProductLow, $vproductsLowForm);
	return $vresponse;
 }

function deleteProductsLowData($vidProductLow)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vflProductLow= new clspFLProductLow();
		$vflProductLow->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductLow->idProductLow=$vidProductLow;
        if ( clspBLProductLow::deleteInDataBase($vflProductLow)==1 ){
            $vresponse->script("vidProductLow='';
                                cancelNewSaveProductsLow();");
            $vresponse->alert("Los datos de la baja de productos ha sido eliminados correctamente"); 
        }
        else{
            $vresponse->alert("Imposible eliminar la baja de productos, intente de nuevo");
        }
        
        unset($vflProductLow);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos de la baja de productos, intente de nuevo");
	}
	
    unset($vidProductLow);
	return $vresponse;
 }

function deleteProductOfProductsLowList($vidProduct)
 {
    $vresponse= new xajaxResponse();
    	   
	try{
        $vproductsLow=$_SESSION['vproductsLowList'];
        $vproductIndexFound=getProductIndexOnList($vidProduct, $vproductsLow);
        if ( $vproductIndexFound!=-1 ){
            array_splice($vproductsLow, $vproductIndexFound, 1);
            $_SESSION['vproductsLowList']=$vproductsLow;
            showProductsLowList($vresponse);
            $vresponse->alert("El producto ha sido eliminado correctamente de la baja");
        }
        
        unset($vproductsLow, $vproductIndexFound);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar al producto de la entrada, intente de nuevo");
	}
	
    unset($vidProduct);
	return $vresponse;
 }

function getProductIndexOnList($vidProduct, $vproductsLow)
 {
    try{
        $vproductIndexFound=-1;
        $vproductsLowTotal=count($vproductsLow);
        for ($vi=0; $vi<$vproductsLowTotal; $vi++){
            if ( $vidProduct==$vproductsLow[$vi][0] ){
                $vproductIndexFound=$vi;
                $vi=$vproductsLowTotal;
            }
        }
        
        unset($vproductsLowTotal, $vi);
        return $vproductIndexFound;
    }
	catch (Exception $vexception){
		throw new Exception($vexception->getMessage(), $vexception->getCode());
	}
    
    unset($vproductsLow);
 }

function cleanProductsLowList()
 {
	$vresponse= new xajaxResponse();
    
    $_SESSION['vproductsLowList']=array();
    $vresponse->assign("vproductsLowList", "innerHTML", "");
    $vresponse->script("visThereListProducts=false;");
        
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProductLowTypesList");
$vxajax->register(XAJAX_FUNCTION, "showProductsList");
$vxajax->register(XAJAX_FUNCTION, "showProductsLowFiltersList");
$vxajax->register(XAJAX_FUNCTION, "showProductsLowData");
//$vxajax->register(XAJAX_FUNCTION, "printProductsEntry");
$vxajax->register(XAJAX_FUNCTION, "addProductsLowData");
$vxajax->register(XAJAX_FUNCTION, "addProductToProductsLowList");
$vxajax->register(XAJAX_FUNCTION, "updateProductsLowData");
$vxajax->register(XAJAX_FUNCTION, "deleteProductsLowData");
$vxajax->register(XAJAX_FUNCTION, "deleteProductOfProductsLowList");
$vxajax->register(XAJAX_FUNCTION, "cleanProductsLowList");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>