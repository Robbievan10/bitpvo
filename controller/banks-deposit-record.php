<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBank.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");


function showBanksList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter ="WHERE c_bank.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vbanks= new clscFLBank();
        clscBLBank::queryToDataBase($vbanks, $vfilter);
        $vbanksTotal=clscBLBank::total($vbanks);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vbanksTotal; $vi++){
            $vJSON.=', {"text":"' . $vbanks->banks[$vi]->name .
                    '", "value":' . $vbanks->banks[$vi]->idBank  . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vbanks, $vbanksTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los bancos, intente de nuevo");
	}

	return $vresponse;
 }

function showBankDepositData($vidBank, $vidBankDeposit)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflBankDeposit= new clspFLBankDeposit();
		$vflBankDeposit->bank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflBankDeposit->bank->idBank=$vidBank;
        $vflBankDeposit->idBankDeposit=$vidBankDeposit;
        switch(clspBLBankDeposit::queryToDataBase($vflBankDeposit)){
            case 0: $vresponse->alert("Los datos del depósito bancario no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("dtpckrdepositDate", $vresponse);
                    $vtext->setValue($vflBankDeposit->depositDate);
                    $vresponse->script("vcmbBanksList.value(" . $vflBankDeposit->bank->idBank . ");
                                        vcmbBanksList.enable(false);");
                    $vtext= new clspText("txtaccountNumber", $vresponse);
                    $vtext->setValue($vflBankDeposit->accountNumber);
                    $vtext= new clspText("txtamount", $vresponse);
                    $vtext->setValue(number_format($vflBankDeposit->amount, 2, ".", ","));
                    $vtext= new clspText("txtresponsibleName", $vresponse);
                    $vtext->setValue($vflBankDeposit->responsibleName);
                    $vtext= new clspText("txtreferenceName", $vresponse);
                    $vtext->setValue($vflBankDeposit->referenceName);
                    $vstring= new clspString($vflBankDeposit->observation);
				    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflBankDeposit);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del depósito bancario, intente de nuevo");
	}
    
    unset($vidBank, $vidBankDeposit);
	return $vresponse;
 }

function addBankDepositData($vbankDepositForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflBankDeposit= new clspFLBankDeposit();
        $vflBankDeposit->bank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflBankDeposit->bank->idBank=(int)($vbankDepositForm["cmbbanksList"]);
        $vflBankDeposit->accountNumber=trim($vbankDepositForm["txtaccountNumber"]);
        $vflBankDeposit->amount=(float)(str_replace(",", "", trim($vbankDepositForm["txtamount"])));
        $vflBankDeposit->responsibleName=trim($vbankDepositForm["txtresponsibleName"]);
        $vflBankDeposit->referenceName=trim($vbankDepositForm["txtreferenceName"]);
        $vflBankDeposit->depositDate=trim($vbankDepositForm["dtpckrdepositDate"]);
        $vflBankDeposit->observation=trim($vbankDepositForm["txtobservation"]);
        switch(clspBLBankDeposit::addToDataBase($vflBankDeposit)){
            case 0:  $vresponse->alert("Imposible registrar el depósito bancario, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidBank=" . $vflBankDeposit->bank->idBank . ";" .
                                        "vidBankDeposit=" . $vflBankDeposit->idBankDeposit);
                     $vresponse->script("vcmbBanksList.enable(false);");
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Depósito Bancario");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Depósito Bancario");
                     $vresponse->alert("Los datos del depósito bancario han sido registrados correctamente");
                     break;
        }
        
        unset($vflBankDeposit);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el depósito bancario, intente de nuevo");
	}
	
    unset($vbankDepositForm);
	return $vresponse;
 }

function updateBankDepositData($vidBank, $vidBankDeposit, $vbankDepositForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vflBankDeposit= new clspFLBankDeposit();
        $vflBankDeposit->bank->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflBankDeposit->bank->idBank=$vidBank;
        $vflBankDeposit->idBankDeposit=$vidBankDeposit;
        $vflBankDeposit->accountNumber=trim($vbankDepositForm["txtaccountNumber"]);
        $vflBankDeposit->amount=(float)(str_replace(",", "", trim($vbankDepositForm["txtamount"])));
        $vflBankDeposit->responsibleName=trim($vbankDepositForm["txtresponsibleName"]);
        $vflBankDeposit->referenceName=trim($vbankDepositForm["txtreferenceName"]);
        $vflBankDeposit->depositDate=trim($vbankDepositForm["dtpckrdepositDate"]);
        $vflBankDeposit->observation=trim($vbankDepositForm["txtobservation"]);
        switch(clspBLBankDeposit::updateInDataBase($vflBankDeposit)){
            case 0: $vresponse->alert("Ningún dato se ha modificado del depósito bancario");
                    break;
            case 1: $vresponse->alert("Los datos del depósito bancario han sido modificados correctamente");
                    break;
        }
        
        unset($vflBankDeposit);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del depósito bancario, intente de nuevo");
	}
	
    unset($vidBank, $vidBankDeposit, $vbankDepositForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showBanksList");
$vxajax->register(XAJAX_FUNCTION, "showBankDepositData");
$vxajax->register(XAJAX_FUNCTION, "addBankDepositData");
$vxajax->register(XAJAX_FUNCTION, "updateBankDepositData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>