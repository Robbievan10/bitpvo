<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");


function showDeliverersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vdeliverers= new clscFLDealer();
        clscBLDealer::queryToDataBase($vdeliverers, "WHERE c_dealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vdeliverersTotal=clscBLDealer::total($vdeliverers);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vdeliverersTotal; $vi++){
            $vJSON.=', {"text":"' . $vdeliverers->deliverers[$vi]->name . ' ' .
                                    $vdeliverers->deliverers[$vi]->firstName . ' ' .
                                    $vdeliverers->deliverers[$vi]->lastName .
                    '", "value":' . $vdeliverers->deliverers[$vi]->idDealer . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vdeliverers, $vdeliverersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los repartidores, intente de nuevo");
	}

	return $vresponse;
 }

function showLoanData($vidDealer, $vidLoan)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflLoan= new clspFLLoan();
		$vflLoan->dealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflLoan->dealer->idDealer=$vidDealer;
        $vflLoan->idLoan=$vidLoan;
        switch(clspBLLoan::queryToDataBase($vflLoan)){
            case 0: $vresponse->alert("Los datos del prestamo no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("dtpckrrecordDate", $vresponse);
                    $vtext->setValue($vflLoan->recordDate);
                    $vresponse->script("vcmbDeliverersList.value(" . $vflLoan->dealer->idDealer . ");
                                        vcmbDeliverersList.enable(false);");
                    $vtext= new clspText("txtamount", $vresponse);
                    $vtext->setValue(number_format( $vflLoan->amount, 2, ".", ","));
                    $vstring= new clspString($vflLoan->observation);
				    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflLoan);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del prestamo, intente de nuevo");
	}
    
    unset($vidLoan);
	return $vresponse;
 }

function addLoanData($vloanForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflLoan= new clspFLLoan();
        $vflLoan->dealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflLoan->dealer->idDealer=(int)($vloanForm["cmbdeliverersList"]);
        $vflLoan->recordDate=trim($vloanForm["dtpckrrecordDate"]);
        $vflLoan->amount=(float)(str_replace(",", "", trim($vloanForm["txtamount"])));
        $vflLoan->observation=trim($vloanForm["txtobservation"]);
        switch(clspBLLoan::addToDataBase($vflLoan)){
            case 0:  $vresponse->alert("Imposible registrar el prestamo, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidDealer=" . $vflLoan->dealer->idDealer . ";" .
                                        "vidLoan=" . $vflLoan->idLoan);
                     $vresponse->script("vcmbDeliverersList.enable(false);");
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Detalle");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Detalle");
                     $vresponse->alert("Los datos del prestamo han sido registrados correctamente");
                     break;
        }
        
        unset($vflLoan);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el prestamo, intente de nuevo");
	}
	
    unset($vloanForm);
	return $vresponse;
 }

function updateLoanData($vidDealer, $vidLoan, $vloanForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflLoan= new clspFLLoan();
		$vflLoan->dealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflLoan->dealer->idDealer=$vidDealer;
        $vflLoan->idLoan=$vidLoan;
        $vflLoan->recordDate=trim($vloanForm["dtpckrrecordDate"]);
        $vflLoan->amount=(float)(str_replace(",", "", trim($vloanForm["txtamount"])));
        $vflLoan->observation=trim($vloanForm["txtobservation"]);
        switch(clspBLLoan::updateInDataBase($vflLoan)){
            case 0: $vresponse->alert("Ningún dato se ha modificado del prestamo");
                    break;
            case 1: $vresponse->alert("Los datos del prestamo han sido modificados correctamente");
                    break;
        }
        
        unset($vflLoan);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del prestamo, intente de nuevo");
	}
	
    unset($vidDealer, $vidLoan, $vloanForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showDeliverersList");
$vxajax->register(XAJAX_FUNCTION, "showLoanData");
$vxajax->register(XAJAX_FUNCTION, "addLoanData");
$vxajax->register(XAJAX_FUNCTION, "updateLoanData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>