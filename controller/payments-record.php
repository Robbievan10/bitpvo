<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");


require_once 'Mobile_Detect.php';



require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");


$deudaTotalGlobal = 0;


function showProvidersList()
 {

        $vresponse= new xajaxResponse();


    try{
        $vindividualsProviders= new clscFLIndividualProvider();
        $vfilter="WHERE c_individualprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualProvider::queryToDataBase($vindividualsProviders, $vfilter);
        $vindividualsProvidersTotal=clscBLIndividualProvider::total($vindividualsProviders);
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsProvidersTotal; $vi++){
            $vJSON.=', {"text":"' . $vindividualsProviders->individualsProviders[$vi]->name . ' ' .
                                    strtoupper($vindividualsProviders->individualsProviders[$vi]->firstName) . ' ' .
                                    strtoupper($vindividualsProviders->individualsProviders[$vi]->lastName) .
                    '", "value":' . strtoupper($vindividualsProviders->individualsProviders[$vi]->idProvider) . '}';
        }

        $vbusinessProviders= new clscFLBusinessProvider();
        $vfilter="WHERE c_businessprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessProvider::queryToDataBase($vbusinessProviders, $vfilter);
        $vbusinessProvidersTotal=clscBLBusinessProvider::total($vbusinessProviders);
        for ($vi=0; $vi<$vbusinessProvidersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessProviders->businessProviders[$vi]->businessName .
                    '", "value":' . $vbusinessProviders->businessProviders[$vi]->idProvider . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
        unset($vindividualsProviders, $vfilter, $vindividualsProvidersTotal, $vJSON, $vi, $vbusinessProviders, $vbusinessProvidersTotal);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los provedores, intente de nuevo");
    }



    return $vresponse;
 }


function getOpenCashName()
 {
    $vresponse;

    try{
        $vcashMovement=getOpenCash();
        if ( $vcashMovement!=null ){
            $vresponse->setReturnValue($vcashMovement->cash->cash);
        }
        else{
            $vresponse->setReturnValue("");
        }
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de obtener el nombre de la caja aperturada, intente de nuevo");
        $vresponse->setReturnValue("");
    }

    return $vresponse;
 }


function getOpenCash()
 {
    try{
        date_default_timezone_set('America/Mexico_City');
        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_cashmovement.id_cashMovementStatus=1 ";
        $vfilter.="AND p_cashmovement.id_user='" . $_SESSION['idUser'] . "' ";
        //$vfilter.="AND p_cashmovement.fldrecordDate='" . date("Y-m-d") . "'";
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryToDataBase($vcashMovements, $vfilter)==1 ){
            return $vcashMovements->cashMovements[0];
        }

        unset($vfilter, $vcashMovements);
        return null;
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }


function showCustomersList()
 {
    $vresponse= new xajaxResponse();

    $userType = 0;
    $cashID;
    $vroute;

try {

        $vmenu='<ul class="nav-notification clearfix">';
    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}


    if($userType==3){
        $vresponse->script("hideButtons();");
        $cashID = getOpenCash();


        $idcaja = $cashID;
        $folio_dist = null;

        $vroutes= new clscFLRoute();
        $vroute;
        $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_route.fldroute='" . $cashID->cash->cash . "'";
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal=clscBLRoute::total($vroutes);
        for ($vi=0; $vi<$vroutesTotal; $vi++){
                    $vroute = $vroutes->routes[$vi]->idRoute;
        }



        $vfilter ="WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter.="AND p_productdistribution.id_productDistribution='".$folio."'";
        //$vfilter.="ORDER BY p_productdistribution.idProductDistribution";
        $folio_dist = null;

        $vproductsDistributions= new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal=clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber=0; $vproductsDistributionNumber<$vproductsDistributionsTotal; $vproductsDistributionNumber++){
            $folio_dist = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
        }

if($folio_dist != null){

    try{

    $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND c_route.fldroute= '" . $cashID->cash->cash . "' ";

    $vroutes= new clscFLRoute();
    clscBLRoute::queryToDataBase($vroutes, $vfilter, 1);
    $vroutesTotal=clscBLRoute::total($vroutes);




        $vroute = $vroutes->routes[0]->idRoute;

    }
    catch (Exception $vexception){
                $vresponse->setReturnValue("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }
///////////////////////////////////////////////////////////////////////////////
        try{



        $vfilter ="WHERE c_routecustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_routecustomer.id_route='" . $vroute . "' ";



        $dw = date("N", time());
        $dw = 8;

        switch ($dw) {
            case 7:
                    $vfilter.="AND c_routecustomer.fldsunday=1";
                break;

            case 1:
                    $vfilter.="AND c_routecustomer.fldmonday=1";
                break;
            case 2:
                $vfilter.="AND c_routecustomer.fldtuesday=1";
                break;
            case 3:
                $vfilter.="AND c_routecustomer.fldwednesday=1";
                break;
            case 4:
                    $vfilter.="AND c_routecustomer.fldthursday=1";
                break;

            case 5:
                    $vfilter.="AND c_routecustomer.fldfriday=1";
                break;
            case 6:
                    $vfilter.="AND c_routecustomer.fldsaturday=1";
                break;

            default:
                break;
        }


        $vroutesCustomers= new clscFLRouteCustomer();
        clscBLRouteCustomer::queryToDataBase($vroutesCustomers, $vfilter);
        $vroutesCustomersTotal=clscBLRouteCustomer::total($vroutesCustomers);


    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }
        try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);

        $vJSON='[{"text":"--Todos--", "value":"0"}';
        /*if ( $vtype==2 ){
            $vJSON='[{"text":"--Todos--", "value":"0"}';
        }*/


        $listaClientesRuta;
        for ($vi1=0; $vi1<$vroutesCustomersTotal; $vi1++){
            $listaClientesRuta[$vi1]=$vroutesCustomers->routesCustomers[$vi1]->customer->idCustomer;

        }


        for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){
            if(in_array($vindividualsCustomers->individualsCustomers[$vi]->idCustomer, $listaClientesRuta)){
            //if($vindividualsCustomers->individualsCustomers[$vi]->idCustomer == $vroutesCustomers->routesCustomers[$vi1]->customer->idCustomer){
                $vJSON.=', {"text":"' . $vindividualsCustomers->individualsCustomers[$vi]->name . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->firstName . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName .
                    '", "value":' . $vindividualsCustomers->individualsCustomers[$vi]->idCustomer . '}';
            }

        }


        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){



            if(in_array($vbusinessCustomers->businessCustomers[$vi]->idCustomer, $listaClientesRuta)){
             $vJSON.=', {"text":"' . $vbusinessCustomers->businessCustomers[$vi]->businessName .
                    '", "value":' . $vbusinessCustomers->businessCustomers[$vi]->idCustomer . '}';
            }

        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
        unset($vindividualsCustomers, $vfilter, $vindividualsCustomersTotal, $vJSON, $vi, $vbusinessCustomers, $vbusinessCustomersTotal);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }

}

    }else{

        try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vindividualsCustomers->individualsCustomers[$vi]->name . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->firstName . ' ' .
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName .
                    '", "value":' . $vindividualsCustomers->individualsCustomers[$vi]->idCustomer . '}';
        }

        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){
            $vJSON.=', {"text":"' . $vbusinessCustomers->businessCustomers[$vi]->businessName .
                    '", "value":' . $vbusinessCustomers->businessCustomers[$vi]->idCustomer . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
        unset($vindividualsCustomers, $vfilter, $vindividualsCustomersTotal, $vJSON, $vi, $vbusinessCustomers, $vbusinessCustomersTotal);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }

    }

    return $vresponse;
 }

function showDeliverersList()
 {
    $vresponse= new xajaxResponse();

    try{
        $vdeliverers= new clscFLDealer();
        clscBLDealer::queryToDataBase($vdeliverers, "WHERE c_dealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vdeliverersTotal=clscBLDealer::total($vdeliverers);

        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vdeliverersTotal; $vi++){
            $vJSON.=', {"text":"' . $vdeliverers->deliverers[$vi]->name . ' ' .
                                    $vdeliverers->deliverers[$vi]->firstName . ' ' .
                                    $vdeliverers->deliverers[$vi]->lastName .
                    '", "value":' . $vdeliverers->deliverers[$vi]->idDealer . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
        unset($vdeliverers, $vdeliverersTotal, $vJSON, $vi);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los repartidores, intente de nuevo");
    }

    return $vresponse;
 }

function showProductsEntriesList($vfilterForm)
 {
    $vresponse= new xajaxResponse();
    $deudaTotal = 0;
    $pagosTotal = 0;

    try{
        $vfilter ="WHERE p_productentry.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND NOT p_productentry.id_operationStatus=2 ";
        if ( (int)($vfilterForm["cmbprovidersList"])!=0 ){
            $vfilter.="AND p_productentry.id_provider=" . (int)($vfilterForm["cmbprovidersList"]) . " ";
        }
        $vproductsEntries= new clscFLProductEntry();
        clscBLProductEntry::queryToDataBase($vproductsEntries, $vfilter);
        $vproductsEntriesTotal=clscBLProductEntry::total($vproductsEntries);
        for ($vproductsEntryNumber=0; $vproductsEntryNumber<$vproductsEntriesTotal; $vproductsEntryNumber++){
            if ( $vproductsEntryNumber==0 ){
                $vdata='<table id="vgrdproductsEntriesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductEntryDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vproductEntryDate" style="text-align:center; font-weight:bold;">Folio</th>
                                    <th data-field="vproductEntryProvider" style="text-align:center; font-weight:bold;">Proveedor</th>
                                    <th data-field="vtotalAmount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproductsEntry" onClick="showProductsEntryPaymentsList(\''. $vproductsEntries->productsEntries[$vproductsEntryNumber]->idProductEntry . '\');" title="Seleccionar Entrada de Productos" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vproductsEntries->productsEntries[$vproductsEntryNumber]->productEntryDate . '</td>';

            if ( $vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->personType->idPersonType==1 ){
                $vflProvider= new clspFLIndividualProvider();
                $vflProvider->enterprise->idEnterprise=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->enterprise->idEnterprise;
                $vflProvider->idProvider=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->idProvider;
                clspBLIndividualProvider::queryToDataBase($vflProvider);
                $vproviderName=$vflProvider->name . ' ' . $vflProvider->firstName . ' ' . $vflProvider->lastName;
            }
            else{
                $vflProvider= new clspFLBusinessProvider();
                $vflProvider->enterprise->idEnterprise=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->enterprise->idEnterprise;
                $vflProvider->idProvider=$vproductsEntries->productsEntries[$vproductsEntryNumber]->provider->idProvider;
                clspBLBusinessProvider::queryToDataBase($vflProvider);
                $vproviderName=$vflProvider->businessName;
            }
            $vdata.='   <td>' . $vproductsEntries->productsEntries[$vproductsEntryNumber]->saleFolio . '</td>';
            $vdata.='   <td>' . $vproviderName . '</td>';

            $vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vproductsEntries->productsEntries[$vproductsEntryNumber]->enterprise->idEnterprise . " ";
            $vfilter.="AND p_productentrydetail.id_productEntry='" . $vproductsEntries->productsEntries[$vproductsEntryNumber]->idProductEntry . "'";
            $vproductsEntriesDetails= new clscFLProductEntryDetail();
            clscBLProductEntryDetail::queryToDataBase($vproductsEntriesDetails, $vfilter);
            $vdata.='   <td>$ ' . number_format(clscBLProductEntryDetail::totalPrice($vproductsEntriesDetails), 2, ".", ",") . '</td>';
            $deudaTotal += clscBLProductEntryDetail::totalPrice($vproductsEntriesDetails);
            $pagosTotal += ProductsEntryPaymentsList($vproductsEntries->productsEntries[$vproductsEntryNumber]->idProductEntry);

            $vdata.="</tr>";

            unset($vflProvider, $vproviderName, $vfilter, $vproductsEntriesDetails);
        }
        $vresponse->assign("deudaTotal", "innerHTML", number_format( $deudaTotal-$pagosTotal, 2, ".", ","));
        if ( $vproductsEntryNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsEntriesList", "innerHTML", $vdata);
            $vresponse->script("setProductsEntriesList();
                                setPaymentsSection();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen entradas de productos por pagar.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsEntriesList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsSection();");
        }
        unset($vfilter, $vproductsEntries, $vproductsEntriesTotal, $vproductsEntryNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar las entradas de productos, intente de nuevo");
    }

    unset($vfilterForm);
    return $vresponse;
 }

function showProductsSalesList($vfilterForm)
 {
    $vresponse= new xajaxResponse();
    $deudaTotal = 0;
    $pagosTotal = 0;


    $userType = 0;
    $cashID;
    $vroute;
    $filtro = false;

    $vroutesCustomersTotal;

    $userType = 0;
    $cashID;
    $vroute;

try {

        $vmenu='<ul class="nav-notification clearfix">';
    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}


    if($userType==3){

// CASH BEGIN
    $cashID = getOpenCash();



    try{

    $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND c_route.fldroute= '" . $cashID->cash->cash . "' ";

    $vroutes= new clscFLRoute();
    clscBLRoute::queryToDataBase($vroutes, $vfilter, 1);
    $vroutesTotal=clscBLRoute::total($vroutes);




        $vroute = $vroutes->routes[0]->idRoute;

    }
    catch (Exception $vexception){
                $vresponse->setReturnValue("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }

    try{



        $vfilter ="WHERE c_routecustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_routecustomer.id_route='" . $vroute."' ";

        $dw = date("N", time());
        $dw = 8;

        switch ($dw) {
            case 7:
                    $vfilter.="AND c_routecustomer.fldsunday=1";
                break;

            case 1:
                    $vfilter.="AND c_routecustomer.fldmonday=1";
                break;
            case 2:
                $vfilter.="AND c_routecustomer.fldtuesday=1";
                break;
            case 3:
                $vfilter.="AND c_routecustomer.fldwednesday=1";
                break;
            case 4:
                    $vfilter.="AND c_routecustomer.fldthursday=1";
                break;

            case 5:
                    $vfilter.="AND c_routecustomer.fldfriday=1";
                break;
            case 6:
                    $vfilter.="AND c_routecustomer.fldsaturday=1";
                break;

            default:
                break;
        }

    $vroutesCustomers= new clscFLRouteCustomer();
    clscBLRouteCustomer::queryToDataBase($vroutesCustomers, $vfilter);
    $vroutesCustomersTotal=clscBLRouteCustomer::total($vroutesCustomers);


    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los clientes, intente de nuevo");
    }
// CASH END



        $listaClientesRuta;
        for ($vi1=0; $vi1<$vroutesCustomersTotal; $vi1++){
            $listaClientesRuta[$vi1]=$vroutesCustomers->routesCustomers[$vi1]->customer->idCustomer;

        }



    try{
        $vfilter ="WHERE p_productsale.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND NOT p_productsale.id_operationStatus=2 ";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        if ( (int)($vfilterForm["cmbcustomersList"])!=0 ){
            $vfilter.="AND p_productsale.id_customer=" . (int)($vfilterForm["cmbcustomersList"]) . " ";
        }
        $vproductsSales= new clscFLProductSale();
        clscBLProductSale::queryToDataBase($vproductsSales, $vfilter);
        $vproductsSalesTotal=clscBLProductSale::total($vproductsSales);
        for ($vproductsSaleNumber=0; $vproductsSaleNumber<$vproductsSalesTotal; $vproductsSaleNumber++){
            // FOR BEGIN
            if ( $vproductsSaleNumber==0 ){
                $vdata='<table id="vgrdproductsSalesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductSaleDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vproductSaleCustomer" style="text-align:center; font-weight:bold;">Cliente</th>
                                    <th data-field="vtotalAmount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            if ( $vproductsSales->productsSales[$vproductsSaleNumber]->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer);
                $vcustomerName=strtoupper($vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName);
            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer);
                $vcustomerName=strtoupper($vflCustomer->businessName);
            }
            if(in_array($vflCustomer->idCustomer, $listaClientesRuta)){

            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproductsSale" onClick="showProductsSalePaymentsList(\''. $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . '\');" title="Seleccionar Venta de Productos" />
                            <span class="custom-radio"></span>
                        </td>';

            $vdata.='   <td>' . $vproductsSales->productsSales[$vproductsSaleNumber]->productSaleDate . '</td>';

            $vdata.='   <td>' . strtoupper($vcustomerName) . '</td>';

            $vfilter ="WHERE p_productsaledetail.id_enterprise=".$vproductsSales->productsSales[$vproductsSaleNumber]->enterprise->idEnterprise." ";
            $vfilter.="AND p_productsaledetail.id_productSale='" . $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . "'";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
            $vdata.='   <td>$ ' . number_format(clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails)-ProductsSalePaymentsList($vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale), 2, ".", ",") .  '</td>';
            $deudaTotal += clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails);
            $pagosTotal += ProductsSalePaymentsList($vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale);
            $vdata.="</tr>";


            }
            unset($vflCustomer, $vcustomerName, $vfilter, $vproductsSalesDetails);

            //FOR END

        }

        $vresponse->assign("deudaTotal", "innerHTML", number_format( $deudaTotal - $pagosTotal, 2, ".", ","));
        if ( $vproductsSaleNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsSalesList", "innerHTML", $vdata);
            $vresponse->script("setProductsSalesList();
                                setPaymentsSection();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen ventas de productos por cobrar.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsSalesList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsSection();");
        }
        unset($vfilter, $vproductsSales, $vproductsSalesTotal, $vproductsSaleNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar las ventas de productos, intente de nuevo");
    }
    unset($vfilterForm);


    }else{
    try{
        $vfilter ="WHERE p_productsale.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND NOT p_productsale.id_operationStatus=2 ";
        $vfilter.="AND p_productsale.fldcanceled=0 ";


        if ( (int)($vfilterForm["cmbcustomersList"])!=0 ){
            $vfilter.="AND p_productsale.id_customer=" . (int)($vfilterForm["cmbcustomersList"]) . " ";
        }


        $vproductsSales= new clscFLProductSale();
        clscBLProductSale::queryToDataBase($vproductsSales, $vfilter);
        $vproductsSalesTotal=clscBLProductSale::total($vproductsSales);
        for ($vproductsSaleNumber=0; $vproductsSaleNumber<$vproductsSalesTotal; $vproductsSaleNumber++){
            if ( $vproductsSaleNumber==0 ){
                $vdata='<table id="vgrdproductsSalesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductSaleDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vproductSaleCustomer" style="text-align:center; font-weight:bold;">Cliente</th>
                                    <th data-field="vtotalAmount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproductsSale" onClick="showProductsSalePaymentsList(\''. $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . '\');" title="Seleccionar Venta de Productos" />
                            <span class="custom-radio"></span>
                        </td>';

            $vdata.='   <td>' . $vproductsSales->productsSales[$vproductsSaleNumber]->productSaleDate . '</td>';
            if ( $vproductsSales->productsSales[$vproductsSaleNumber]->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer);
                $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSales->productsSales[$vproductsSaleNumber]->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer);
                $vcustomerName=$vflCustomer->businessName;
            }
            $vdata.='   <td>' . strtoupper($vcustomerName) . '</td>';

            $vfilter ="WHERE p_productsaledetail.id_enterprise=".$vproductsSales->productsSales[$vproductsSaleNumber]->enterprise->idEnterprise." ";
            $vfilter.="AND p_productsaledetail.id_productSale='" . $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . "'";
            $vproductsSalesDetails= new clscFLProductSaleDetail();
            clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
            $vdata.='   <td>$ ' . number_format(clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails), 2, ".", ",") .  '</td>';
            $deudaTotal += clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails);
            $pagosTotal += ProductsSalePaymentsList($vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale);
            $vdata.="</tr>";

            unset($vflCustomer, $vcustomerName, $vfilter, $vproductsSalesDetails);
        }

        $vresponse->assign("deudaTotal", "innerHTML", number_format( $deudaTotal - $pagosTotal, 2, ".", ","));
        if ( $vproductsSaleNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsSalesList", "innerHTML", $vdata);
            $vresponse->script("setProductsSalesList();
                                setPaymentsSection();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen ventas de productos por cobrar.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsSalesList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsSection();");
        }
        unset($vfilter, $vproductsSales, $vproductsSalesTotal, $vproductsSaleNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar las ventas de productos, intente de nuevo");
    }
    unset($vfilterForm);

    }
    return $vresponse;
 }

function showLoansList($vfilterForm)
 {
    $vresponse= new xajaxResponse();
    $deudaTotal=0;
    $pagosTotal = 0;
    try{
        $vfilter ="WHERE p_loan.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND NOT p_loan.id_operationStatus=2 ";
        if ( (int)($vfilterForm["cmbdeliverersList"])!=0 ){
            $vfilter.="AND p_loan.id_dealer=" . (int)($vfilterForm["cmbdeliverersList"]) . " ";
        }
        $vloans= new clscFLLoan();
        clscBLLoan::queryToDataBase($vloans, $vfilter);
        $vloansTotal=clscBLLoan::total($vloans);
        for ($vloanNumber=0; $vloanNumber<$vloansTotal; $vloanNumber++){
            if ( $vloanNumber==0 ){
                $vdata='<table id="vgrdloansList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vloanDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vloanDeliverers" style="text-align:center; font-weight:bold;">Repartidores</th>
                                    <th data-field="vtotalAmount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrloan" onClick="showLoanPaymentsList('.
                            $vloans->loans[$vloanNumber]->dealer->idDealer . ',' .
                            $vloans->loans[$vloanNumber]->idLoan . ');" title="Seleccionar Prestamo" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vloans->loans[$vloanNumber]->recordDate. '</td>';
            $vdata.='   <td>' . strtoupper($vloans->loans[$vloanNumber]->dealer->name) . ' ' .  strtoupper($vloans->loans[$vloanNumber]->dealer->firstName) . ' ' .
                                strtoupper($vloans->loans[$vloanNumber]->dealer->lastName) . '</td>';
            $vdata.='   <td>$ ' . number_format( $vloans->loans[$vloanNumber]->amount, 2, ".", ",") . '</td>';
            $deudaTotal += $vloans->loans[$vloanNumber]->amount;
            $vdata.="</tr>";
            $pagosTotal += PaymentsList($vloans->loans[$vloanNumber]->dealer->idDealer,$vloans->loans[$vloanNumber]->idLoan);
        }

            $vresponse->assign("deudaTotal", "innerHTML", number_format( $deudaTotal - $pagosTotal, 2, ".", ","));

        if ( $vloanNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vloansList", "innerHTML", $vdata);
            $vresponse->script("setLoansList();
                                setPaymentsSection();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen prestamos por cobrar.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vloansList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsSection();");
        }
        unset($vfilter, $vloans, $vloansTotal, $vloanNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los prestamos, intente de nuevo");
    }

    unset($vfilterForm);
    return $vresponse;
 }

function showProductsEntryPaymentsList($vidProductEntry)
 {
    $vresponse= new xajaxResponse();

    try{
        $vfilter ="WHERE p_productentrypayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productentrypayment.id_productEntry='" . $vidProductEntry . "'";
        $vproductsEntriesPayments= new clscFLProductEntryPayment();
        clscBLProductEntryPayment::queryToDataBase($vproductsEntriesPayments, $vfilter);
        $vproductsEntryPaymentsTotal=clscBLProductEntryPayment::total($vproductsEntriesPayments);
        for ($vproductEntryPaymentNumber=0; $vproductEntryPaymentNumber<$vproductsEntryPaymentsTotal; $vproductEntryPaymentNumber++){
            if ( $vproductEntryPaymentNumber==0 ){
                $vdata='<table id="vgrdpaymentsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vpaymentDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vamount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }

            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrpayment" onClick="setIdPayment('.
                            $vproductsEntriesPayments->productsEntriesPayments[$vproductEntryPaymentNumber]->idPayment .
                            ');" title="Seleccionar Pago" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vproductsEntriesPayments->productsEntriesPayments[$vproductEntryPaymentNumber]->paymentDate. '</td>';
            $vdata.='   <td>$ ' . number_format($vproductsEntriesPayments->productsEntriesPayments[$vproductEntryPaymentNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vproductEntryPaymentNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vpaymentsList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsList();
                                vcmdDeletePayment.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen pagos de la entrada de productos.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vpaymentsList", "innerHTML", $vdata);
            $vresponse->script("vcmdDeletePayment.enable(false);");
        }
        unset($vfilter, $vproductsEntriesPayments, $vproductsEntryPaymentsTotal, $vproductEntryPaymentNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los pagos de la entrada de productos, intente de nuevo");
    }

    unset($vidProductEntry);
    return $vresponse;
 }

function showProductsSalePaymentsList($vidProductSale)
 {
    $vresponse= new xajaxResponse();

    try{
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productsalepayment.id_productSale='" . $vidProductSale . "'";
        $vproductsSalesPayments= new clscFLProductSalePayment();
        clscBLProductSalePayment::queryToDataBase($vproductsSalesPayments, $vfilter);
        $vproductsSalePaymentsTotal=clscBLProductSalePayment::total($vproductsSalesPayments);
        for ($vproductSalePaymentNumber=0; $vproductSalePaymentNumber<$vproductsSalePaymentsTotal; $vproductSalePaymentNumber++){
            if ( $vproductSalePaymentNumber==0 ){
                $vdata='<table id="vgrdpaymentsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vpaymentDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vamount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrpayment" onClick="setIdPayment('.
                            $vproductsSalesPayments->productsSalesPayments[$vproductSalePaymentNumber]->idPayment .
                            ');" title="Seleccionar Pago" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vproductsSalesPayments->productsSalesPayments[$vproductSalePaymentNumber]->paymentDate. '</td>';
            $vdata.='   <td>$ ' . number_format($vproductsSalesPayments->productsSalesPayments[$vproductSalePaymentNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vproductSalePaymentNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vpaymentsList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsList();
                                vcmdDeletePayment.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen pagos de la venta de productos.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vpaymentsList", "innerHTML", $vdata);
            $vresponse->script("vcmdDeletePayment.enable(false);");
        }
        unset($vfilter, $vproductsSalesPayments, $vproductsSalePaymentsTotal, $vproductSalePaymentNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los pagos de la venta de productos, intente de nuevo");
    }

    unset($vidProductSale);
    return $vresponse;
 }

function showLoanPaymentsList($vidDealer, $vidLoan)
 {
    $pagosTotal = 0;
    $vresponse= new xajaxResponse();

    try{
        $vfilter ="WHERE p_loanpayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_loanpayment.id_dealer=" . $vidDealer . " ";
        $vfilter.="AND p_loanpayment.id_loan=" . $vidLoan . " ";
        $vloansPayments= new clscFLLoanPayment();
        clscBLLoanPayment::queryToDataBase($vloansPayments, $vfilter);
        $vloanPaymentsTotal=clscBLLoanPayment::total($vloansPayments);
        for ($vloanPaymentNumber=0; $vloanPaymentNumber<$vloanPaymentsTotal; $vloanPaymentNumber++){
            if ( $vloanPaymentNumber==0 ){
                $vdata='<table id="vgrdpaymentsList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vpaymentDate" style="text-align:center; font-weight:bold;">Fecha</th>
                                    <th data-field="vamount" style="text-align:center; font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrpayment" onClick="setIdPayment('.
                            $vloansPayments->loansPayments[$vloanPaymentNumber]->idPayment .
                            ');" title="Seleccionar Pago" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vloansPayments->loansPayments[$vloanPaymentNumber]->paymentDate. '</td>';
            $vdata.='   <td>$ ' . number_format($vloansPayments->loansPayments[$vloanPaymentNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }



        if ( $vloanPaymentNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vpaymentsList", "innerHTML", $vdata);
            $vresponse->script("setPaymentsList();
                                vcmdDeletePayment.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen pagos del prestamo.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vpaymentsList", "innerHTML", $vdata);
            $vresponse->script("vcmdDeletePayment.enable(false);");
        }
        unset($vfilter, $vloansPayments, $vloanPaymentsTotal, $vloanPaymentNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los pagos del prestamo, intente de nuevo". $vexception->getMessage());
    }



    unset($vidDealer, $vidLoan);
    return $vresponse;
 }

function recordProductEntryPayment($vidProductEntry, $vproductEntryPaymentForm)
 {
    $vresponse= new xajaxResponse();

    try{
        $vflProductEntryPayment= new clspFLProductEntryPayment();
        $vflProductEntryPayment->productEntry->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductEntryPayment->productEntry->idProductEntry=$vidProductEntry;
        $vflProductEntryPayment->enterprise->idEnterprise=$vflProductEntryPayment->productEntry->enterprise->idEnterprise;
        $vflProductEntryPayment->paymentType->idPaymentType=1;
        $vflProductEntryPayment->paymentDate=trim($vproductEntryPaymentForm["dtpckrpaymentDate"]);
        $vflProductEntryPayment->amount=(float)(str_replace(",", "", trim($vproductEntryPaymentForm["txtamount"])));
        switch(clspBLProductEntryPayment::addToDataBase($vflProductEntryPayment)){
            case -1: $vresponse->alert("Imposible registrar el pago de la entrada de productos, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible registrar el pago de la entrada de productos, el(los) monto(s) del(os) pago(s) excede al monto de la entrada de productos");
                     break;
            case 1:  $vresponse->script("xajax_showProductsEntryPaymentsList('" . $vidProductEntry .  "');");
                     $vresponse->alert("El pago de la entrada de productos ha sido registrado correctamente");
                     break;
        }

        unset($vflProductEntryPayment);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de registrar el pago de la entrada de productos, intente de nuevo");
    }

    unset($vidProductEntry, $vproductEntryPaymentForm);

    return $vresponse;
 }

function recordProductSalePayment($vidProductSale, $vproductSalePaymentForm)
 {
    $vresponse= new xajaxResponse();


        try {

        $vmenu='<ul class="nav-notification clearfix">';
    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}

if($userType == 3){

    if(getOpenCash()!=null){
    try{
        $vflProductSalePayment= new clspFLProductSalePayment();
        $vflProductSalePayment->productSale->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductSalePayment->productSale->idProductSale=$vidProductSale;
        $vflProductSalePayment->enterprise->idEnterprise=$vflProductSalePayment->productSale->enterprise->idEnterprise;
        $vflProductSalePayment->paymentType->idPaymentType=2;
        $vflProductSalePayment->paymentDate=trim($vproductSalePaymentForm["dtpckrpaymentDate"]);
        $vflProductSalePayment->amount=floatval(str_replace(",", "", trim($vproductSalePaymentForm["txtamount"])));
        switch(clspBLProductSalePayment::addToDataBase($vflProductSalePayment)){
            case -1: $vresponse->alert("Imposible registrar el pago de la venta de productos, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible registrar el pago de la venta de productos, el(los) monto(s) del(os) pago(s) excede al monto de la venta de productos");
                     break;
            case 1:  $vresponse->script("xajax_showProductsSalePaymentsList('" . $vidProductSale .  "');");
                     $vresponse->alert("El pago de la venta de productos ha sido registrado correctamente");
                     $cantidad = $vflProductSalePayment->amount;
                     $folio = $vidProductSale;
                     $respuesta = crearReciboPago($cantidad,$folio);
                    $vresponse->script("imprimirReciboPago('".$respuesta."');");
                     break;
        }

        unset($vflProductSalePayment);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de registrar el pago de la venta de productos, intente de nuevo");
    }

    unset($vidProductSale, $vproductSalePaymentForm);
                //$vresponse->script("showProductsSalesList();");
    }else{
        $vresponse->alert("Caja cerrada");
    }

}

if($userType == 6 || $userType == 2){
  try{
      $vflProductSalePayment= new clspFLProductSalePayment();
      $vflProductSalePayment->productSale->enterprise->idEnterprise=$_SESSION['idEnterprise'];
      $vflProductSalePayment->productSale->idProductSale=$vidProductSale;
      $vflProductSalePayment->enterprise->idEnterprise=$vflProductSalePayment->productSale->enterprise->idEnterprise;
      $vflProductSalePayment->paymentType->idPaymentType=2;
      $vflProductSalePayment->paymentDate=trim($vproductSalePaymentForm["dtpckrpaymentDate"]);
      $vflProductSalePayment->amount=floatval(str_replace(",", "", trim($vproductSalePaymentForm["txtamount"])));
      switch(clspBLProductSalePayment::addToDataBase($vflProductSalePayment)){
          case -1: $vresponse->alert("Imposible registrar el pago de la venta de productos, intente de nuevo");
                   break;
          case 0:  $vresponse->alert("Imposible registrar el pago de la venta de productos, el(los) monto(s) del(os) pago(s) excede al monto de la venta de productos");
                   break;
          case 1:  $vresponse->script("xajax_showProductsSalePaymentsList('" . $vidProductSale .  "');");
                   $vresponse->alert("El pago de la venta de productos ha sido registrado correctamente");
                   $cantidad = $vflProductSalePayment->amount;
                   $folio = $vidProductSale;
                   $respuesta = crearReciboPago($cantidad,$folio);
                  $vresponse->script("imprimirReciboPago('".$respuesta."');");
                   break;
      }

      unset($vflProductSalePayment);
  }
  catch (Exception $vexception){
      $vresponse->alert("Ocurrió un error al tratar de registrar el pago de la venta de productos, intente de nuevo");
  }

  unset($vidProductSale, $vproductSalePaymentForm);

}


    return $vresponse;
 }

 function crearReciboPago($cantidad,$folio){
    $respuesta = $folio;
    return $respuesta;
 }




function recordLoanPayment($vidDealer, $vidLoan, $vloanPaymentForm)
 {
    $vresponse= new xajaxResponse();

    try{
        $vflLoanPayment= new clspFLLoanPayment();
        $vflLoanPayment->loan->dealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflLoanPayment->loan->dealer->idDealer=$vidDealer;
        $vflLoanPayment->loan->idLoan=$vidLoan;
        $vflLoanPayment->enterprise->idEnterprise=$vflLoanPayment->loan->dealer->enterprise->idEnterprise;
        $vflLoanPayment->paymentType->idPaymentType=3;
        $vflLoanPayment->paymentDate=trim($vloanPaymentForm["dtpckrpaymentDate"]);
        $vflLoanPayment->amount=(float)(str_replace(",", "", trim($vloanPaymentForm["txtamount"])));
        switch(clspBLLoanPayment::addToDataBase($vflLoanPayment)){
            case -1: $vresponse->alert("Imposible registrar el pago del prestamo, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible registrar el pago del prestamo, el(los) monto(s) del(os) pago(s) excede al monto del prestamo");
                     break;
            case 1:  $vresponse->script("xajax_showLoanPaymentsList(" . $vidDealer . ", " . $vidLoan .  ");");
                     $vresponse->alert("El pago del prestamo ha sido registrado correctamente");
                     break;
        }

        unset($vflLoanPayment);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de registrar el pago del prestamo, intente de nuevo");
    }

    unset($vidDealer, $vidLoan, $vloanPaymentForm);
    return $vresponse;
 }

function deleteProductsEntryPaymentData($vidPayment, $vidProductEntry)
 {
    $vresponse= new xajaxResponse();

    try{
        $vflProductEntryPayment= new clspFLProductEntryPayment();
        $vflProductEntryPayment->idPayment=$vidPayment;
        $vflProductEntryPayment->productEntry->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductEntryPayment->productEntry->idProductEntry=$vidProductEntry;
        $vflProductEntryPayment->enterprise->idEnterprise=$vflProductEntryPayment->productEntry->enterprise->idEnterprise;
        if ( clspBLProductEntryPayment::deleteInDataBase($vflProductEntryPayment)==1 ){
            $vresponse->script("xajax_showProductsEntryPaymentsList('" . $vidProductEntry .  "');");
            $vresponse->alert("Los datos del pago de la entrada de productos han sido eliminados correctamente");
        }
        else{
            $vresponse->alert("Imposible eliminar el pago de la entrada de productos, intente de nuevo");
        }

        unset($vflProductEntryPayment);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de eliminar los datos del pago de la entrada de productos, intente de nuevo");
    }

    unset($vidPayment, $vidProductEntry);
    return $vresponse;
 }

function deleteProductsSalePaymentData($vidPayment, $vidProductSale)
 {
    $vresponse= new xajaxResponse();


    try {

        $vmenu='<ul class="nav-notification clearfix">';
    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}

if($userType != 3){
        try{
        $vflProductSalePayment= new clspFLProductSalePayment();
        $vflProductSalePayment->idPayment=$vidPayment;
        $vflProductSalePayment->productSale->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductSalePayment->productSale->idProductSale=$vidProductSale;
        $vflProductSalePayment->enterprise->idEnterprise=$vflProductSalePayment->productSale->enterprise->idEnterprise;
        if ( clspBLProductSalePayment::deleteInDataBase($vflProductSalePayment)==1 ){
            $vresponse->script("xajax_showProductsSalePaymentsList('" . $vidProductSale .  "');");
            $vresponse->alert("Los datos del pago de la venta de productos han sido eliminados correctamente");
        }
        else{
            $vresponse->alert("Imposible eliminar el pago de la venta de productos, intente de nuevo");
        }

        unset($vflProductSalePayment);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de eliminar los datos del pago de la venta de productos, intente de nuevo");
    }
}else{
        $vresponse->alert("Imposible eliminar el pago de la venta de productos, no tiene permisos suficientes.");
}



    unset($vidPayment, $vidProductSale);
    return $vresponse;
 }

function deleteLoanPaymentData($vidPayment, $vidDealer, $vidLoan)
 {
    $vresponse= new xajaxResponse();

    try{
        $vflLoanPayment= new clspFLLoanPayment();
        $vflLoanPayment->idPayment=$vidPayment;
        $vflLoanPayment->loan->dealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflLoanPayment->loan->dealer->idDealer=$vidDealer;
        $vflLoanPayment->loan->idLoan=$vidLoan;
        $vflLoanPayment->enterprise->idEnterprise=$vflLoanPayment->loan->dealer->enterprise->idEnterprise;
        if ( clspBLLoanPayment::deleteInDataBase($vflLoanPayment)==1 ){
            $vresponse->script("xajax_showLoanPaymentsList(" . $vidDealer . ", " . $vidLoan .  ");");
            $vresponse->alert("Los datos del pago del prestamo han sido eliminados correctamente");
        }
        else{
            $vresponse->alert("Imposible eliminar el pago del prestamo, intente de nuevo");
        }

        unset($vflLoanPayment);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de eliminar los datos del pago del prestamo, intente de nuevo");
    }

    unset($vidPayment, $vidDealer, $vidLoan);
    return $vresponse;
 }

function exit_()
 {
    $vresponse= new xajaxResponse();

    session_destroy();
    $vresponse->redirect("./");

    return $vresponse;
 }


function PaymentsList($vidDealer, $vidLoan)
 {
    $pagosTotal = 0;
    $vresponse= new xajaxResponse();

    try{
        $vfilter ="WHERE p_loanpayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_loanpayment.id_dealer=" . $vidDealer . " ";
        $vfilter.="AND p_loanpayment.id_loan=" . $vidLoan . " ";
        $vloansPayments= new clscFLLoanPayment();
        clscBLLoanPayment::queryToDataBase($vloansPayments, $vfilter);
        $vloanPaymentsTotal=clscBLLoanPayment::total($vloansPayments);
        for ($vloanPaymentNumber=0; $vloanPaymentNumber<$vloanPaymentsTotal; $vloanPaymentNumber++){
            $pagosTotal += $vloansPayments->loansPayments[$vloanPaymentNumber]->amount;
        }

    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los pagos del prestamo, intente de nuevo". $vexception->getMessage());
    }

    unset($vidDealer, $vidLoan);
    return $pagosTotal;
 }



function ProductsEntryPaymentsList($vidProductEntry)
 {
    $vresponse= new xajaxResponse();
    $pagosTotal = 0;
    try{
        $vfilter ="WHERE p_productentrypayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productentrypayment.id_productEntry='" . $vidProductEntry . "'";
        $vproductsEntriesPayments= new clscFLProductEntryPayment();
        clscBLProductEntryPayment::queryToDataBase($vproductsEntriesPayments, $vfilter);
        $vproductsEntryPaymentsTotal=clscBLProductEntryPayment::total($vproductsEntriesPayments);
        for ($vproductEntryPaymentNumber=0; $vproductEntryPaymentNumber<$vproductsEntryPaymentsTotal; $vproductEntryPaymentNumber++){

            $pagosTotal +=$vproductsEntriesPayments->productsEntriesPayments[$vproductEntryPaymentNumber]->amount;
        }
        unset($vfilter, $vproductsEntriesPayments, $vproductsEntryPaymentsTotal, $vproductEntryPaymentNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los pagos de la entrada de productos, intente de nuevo");
    }

    unset($vidProductEntry);
    return $pagosTotal;
 }

function ProductsSalePaymentsList($vidProductSale)
 {
    $vresponse= new xajaxResponse();
    $pagosTotal = 0;

    try{
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productsalepayment.id_productSale='" . $vidProductSale . "'";
        $vproductsSalesPayments= new clscFLProductSalePayment();
        clscBLProductSalePayment::queryToDataBase($vproductsSalesPayments, $vfilter);
        $vproductsSalePaymentsTotal=clscBLProductSalePayment::total($vproductsSalesPayments);
        for ($vproductSalePaymentNumber=0; $vproductSalePaymentNumber<$vproductsSalePaymentsTotal; $vproductSalePaymentNumber++){

            $pagosTotal += $vproductsSalesPayments->productsSalesPayments[$vproductSalePaymentNumber]->amount;

        }

        unset($vfilter, $vproductsSalesPayments, $vproductsSalePaymentsTotal, $vproductSalePaymentNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los pagos de la venta de productos, intente de nuevo");
    }

    unset($vidProductSale);
    return $pagosTotal;
 }






function printPayments($x,$y,$cliente,$folio)
 {
    $vresponse= new xajaxResponse();
    $ultimoPago;
    $datos = "";

$detect = new Mobile_Detect;

if( !$detect->isAndroidOS()){
        try{
        if ( ((int)($x))==1 ){
            $vidListData=$y;
        }
        else if ( ((int)($x)==2 )){
            $vidListData=$y;
        }
        else{
            $vidListData=$y;
        }
        date_default_timezone_set('America/Mexico_city');

        $vurl ="./controller/payments-report-rpt.php?vstartDate=01/01/1900&vendDate=" .date('d-m-Y');
        $vurl.="&vpaymentType=" . $x . "&vidListData=" . $cliente;
        $vresponse->script("window.open('$vurl', '_blank', 'menubar=no');");

        unset($vurl);
       }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de imprimir los pagos, intente de nuevo");
    }

    unset($vfilterForm);
}else{


    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);

        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter =" AND p_productsalepayment.id_productSale='" .$folio. "' ";

        $vfilter.=" AND p_productsale.id_customer=" .$cliente. " ";
        $vpayments= new clscFLProductSalePayment();
        clscBLProductSalePayment::queryToDataBase($vpayments, $vfilter);
        $vpaymentsTotal=clscBLProductSalePayment::total($vpayments);


    for ($vpaymentNumber=0; $vpaymentNumber<$vpaymentsTotal; $vpaymentNumber++){
        $ultimoPago = $vpayments->productsSalesPayments[$vpaymentNumber]->idPayment;
    }
        //$vresponse->alert($vpaymentsTotal);



    if(file_exists ( dirname(dirname(__FILE__)) . "/pagos/".$ultimoPago.".html" )){
        $datos = $ultimoPago.".html";
    }else{

/////////////////////////////// Inicio Prueba json en html
//--------------------------------------------------------

    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");

    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);


    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND p_productsaledetail.id_productSale='" . $folio. "'";
    $vproductsSalesDetails= new clscFLProductSaleDetail();
    clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter, 1);
    $vsaleProductsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);

    $vflProductSale= new clspFLProductSale();
    $vflProductSale->enterprise->idEnterprise=$vflEnterpriseUser->enterprise->idEnterprise;
    $vflProductSale->idProductSale=$folio;

            $datos .= '{"nombre_empresa":"';
            $datos .= $vflEnterpriseUser->enterprise->enterprise;
            $datos .= '",';

            $datos .= '"telefono_empresa":"';
            $datos .= $vflEnterpriseUser->enterprise->phoneNumber;
            $datos .= '",';

            $datos .= '"Nota_venta":"Recibo de pago",';
            $datos .= '"Folio": "';
            $datos .= $vproductsSalesDetails->productsSalesDetails[0]->productSale->idProductSale;

            $datos .= '",';
            $datos .= '"Fecha": "';
            $datos .= $vproductsSalesDetails->productsSalesDetails[0]->productSale->productSaleDate;

            $datos .= '",';
            $datos .= '"Cliente": "';

            if ( $vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                $datos .= $vflCustomer->name;
                $datos .= ' ';
                $datos .= $vflCustomer->firstName;
                $datos .= ' ';
                $datos .= $vflCustomer->lastName;


            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->enterprise->idEnterprise;
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[0]->productSale->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->businessName;
                $datos .= $vflCustomer->businessName;
            }

            $datos .= '",';
            $datos .= '"Tipo_venta": "';
            $datos .= $vproductsSalesDetails->productsSalesDetails[0]->productSale->operationType->operationType;

            $datos .= '",';
            $datos .= '"Vendedor": "';
            $vflUser= new clspFLUser();
            $vflUser->idUser=trim($_SESSION['idUser']);
            clspBLUser::queryToDataBase($vflUser);
            $datos .= $vflUser->name . ' ' . $vflUser->firstName . ' ' . $vflUser->lastName;

            $datos .= '",';

            $datos .= '"detalle" : [';
            unset($vflCustomer, $vcustomerName);


            $totalPagado = 0;

    for ($vpaymentNumber=0; $vpaymentNumber<$vpaymentsTotal; $vpaymentNumber++){


           $producto = $vpayments->productsSalesPayments[$vpaymentNumber]->paymentDate;

            $datos .= '{"producto":"';
            $datos .= $producto;
            $datos .= '","cantidad" : "';
            $cantidad = $vpayments->productsSalesPayments[$vpaymentNumber]->amount;
            $datos .= $cantidad;
            $datos .= '", "precio" : "';
            $totalPagado += $cantidad;

            //$precio = number_format($vproductsSalesDetails->productsSalesDetails[$vproductNumber]->salePrice, 2, ".", ",");
            //$precio = floatval($precio);
            $precio = NULL;
            $datos .=$precio;
            $datos .= '","subtotal":"';


            //$subtotal = number_format(clspBLProductSaleDetail::totalSalePrice($vproductsSalesDetails->productsSalesDetails[$vproductNumber]), 2, ".", ",");
            $subtotal = NULL;
            $datos .= $subtotal;
            $datos .= '"}';



            if($vpaymentNumber == ($vpaymentsTotal-1)){
                            $datos .= '],';
            }else{
                            $datos .= ',';
            }

    }

            //$datos . substr($datos, 0, $datos.length()-1);

            $datos .= '"Total": "';
            $datos .= number_format(clscBLProductSaleDetail::totalSalePrice($vproductsSalesDetails)-$totalPagado, 2, ".", ",");
            $datos .= '","Mensaje":"';
            $datos .= '\r\n';
            $datos .= 'Gracias por su pago"}';






    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSale, $vflEnterpriseUser, $vfilter,
          $vproductsSalesDetails, $vsaleProductsTotal, $vproductNumber);
/////////////////////////////// Fin Prueba json en html

    $datos = utf8_encode($datos);
    $fp = fopen(dirname(dirname(__FILE__)) . "/pagos/".$ultimoPago.".html","wb");
    fwrite($fp,$datos);
    fclose($fp);

    }
    //---------------------------------------------------------
    $datos = 'p';
    $datos .= $ultimoPago;
    $datos .= '.html';
    $vresponse->assign("ticketDeVenta", "innerHTML",$datos);
    $vresponse->script("imprimirTicketMovil();");
}

    return $vresponse;
 }









$vxajax->register(XAJAX_FUNCTION, "showProvidersList");
$vxajax->register(XAJAX_FUNCTION, "showCustomersList");
$vxajax->register(XAJAX_FUNCTION, "showDeliverersList");
$vxajax->register(XAJAX_FUNCTION, "showProductsEntriesList");
$vxajax->register(XAJAX_FUNCTION, "showProductsSalesList");
$vxajax->register(XAJAX_FUNCTION, "showLoansList");
$vxajax->register(XAJAX_FUNCTION, "showProductsEntryPaymentsList");
$vxajax->register(XAJAX_FUNCTION, "ProductsEntryPaymentsList");
$vxajax->register(XAJAX_FUNCTION, "showProductsSalePaymentsList");
$vxajax->register(XAJAX_FUNCTION, "ProductsSalePaymentsList");
$vxajax->register(XAJAX_FUNCTION, "showLoanPaymentsList");
$vxajax->register(XAJAX_FUNCTION, "PaymentsList");
$vxajax->register(XAJAX_FUNCTION, "recordProductEntryPayment");
$vxajax->register(XAJAX_FUNCTION, "recordProductSalePayment");
$vxajax->register(XAJAX_FUNCTION, "recordLoanPayment");
$vxajax->register(XAJAX_FUNCTION, "deleteProductsEntryPaymentData");
$vxajax->register(XAJAX_FUNCTION, "deleteProductsSalePaymentData");
$vxajax->register(XAJAX_FUNCTION, "deleteLoanPaymentData");
$vxajax->register(XAJAX_FUNCTION, "printPayments");
$vxajax->register(XAJAX_FUNCTION, "imprimirPagosMovil");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->register(XAJAX_FUNCTION, "getOpenCash");
$vxajax->register(XAJAX_FUNCTION, "getOpenCashName");
$vxajax->processRequest();

?>
