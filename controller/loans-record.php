<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");
date_default_timezone_set('America/Mexico_City');

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLOperationStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLOperationStatus.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLLoan.php");


function showOperationStatusList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $voperationStatus= new clscFLOperationStatus();
        clscBLOperationStatus::queryToDataBase($voperationStatus, "");
        $voperationStatusTotal=clscBLOperationStatus::total($voperationStatus);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$voperationStatusTotal; $vi++){
            $vJSON.=', {"text":"' . $voperationStatus->operationStatus[$vi]->operationStatus .
                    '", "value":' . $voperationStatus->operationStatus[$vi]->idOperationStatus . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($voperationStatus, $voperationStatusTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados de prestamo, intente de nuevo");
	}

	return $vresponse;
 }

function showDeliverersList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vdeliverers= new clscFLDealer();
        clscBLDealer::queryToDataBase($vdeliverers, "WHERE c_dealer.id_enterprise=" . $_SESSION['idEnterprise']);
        $vdeliverersTotal=clscBLDealer::total($vdeliverers);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vdeliverersTotal; $vi++){
            $vJSON.=', {"text":"' . $vdeliverers->deliverers[$vi]->name . ' ' .
                                    $vdeliverers->deliverers[$vi]->firstName . ' ' .
                                    $vdeliverers->deliverers[$vi]->lastName .
                    '", "value":' . $vdeliverers->deliverers[$vi]->idDealer . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vdeliverers, $vdeliverersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los repartidores, intente de nuevo");
	}

	return $vresponse;
 }

function showLoansRecordList($vfilterForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter ="WHERE p_loan.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_loan.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }
        if ( (int)($vfilterForm["cmboperationStatusList"])!=0 ){
            $vfilter.="AND p_loan.id_operationStatus=" . (int)($vfilterForm["cmboperationStatusList"]) . " ";
        }
        if ( (int)($vfilterForm["cmbdeliverersList"])!=0 ){
            $vfilter.="AND p_loan.id_dealer=" . (int)($vfilterForm["cmbdeliverersList"]) . " ";
        }
        
        $vloans= new clscFLLoan();
        clscBLLoan::queryToDataBase($vloans, $vfilter);
        $vloansTotal=clscBLLoan::total($vloans);
        for ($vloanNumber=0; $vloanNumber<$vloansTotal; $vloanNumber++){
            if ( $vloanNumber==0 ){
                $vdata='<table id="vgrdloansList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vloanRecordDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vdealerName" style="text-align:center; font-weight:bold;">Repartidor</th>
                                    <th data-field="vloanAmount" style="text-align:center;font-weight:bold;">Monto</th>
                                    <th data-field="vloanStatus" style="text-align:center;font-weight:bold;">Estado</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrloan" onClick="setLoanIds('. 
                            $vloans->loans[$vloanNumber]->dealer->idDealer . ',' .
                            $vloans->loans[$vloanNumber]->idLoan . ');" title="Seleccionar Prestamo" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vloans->loans[$vloanNumber]->recordDate. '</td>';
            $vdata.='	<td>' . $vloans->loans[$vloanNumber]->dealer->name . ' ' .  $vloans->loans[$vloanNumber]->dealer->firstName . ' ' .
                                $vloans->loans[$vloanNumber]->dealer->lastName . '</td>';
            $vdata.='	<td>' . "$ " .  number_format( $vloans->loans[$vloanNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.='	<td>' . $vloans->loans[$vloanNumber]->operationStatus->operationStatus. '</td>';
            $vdata.="</tr>";
        }
        if ( $vloanNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vloansRecordList", "innerHTML", $vdata);
            $vresponse->script("setLoansList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen prestamos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vloansRecordList", "innerHTML", $vdata);
        }
        unset($vfilter, $vloans, $vloansTotal, $vloanNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los prestamos, intente de nuevo");
	}
	
    unset($vfilterForm);
	return $vresponse;
 }

function deleteLoanData($vidDealer, $vidLoan)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflLoan= new clspFLLoan();
        $vflLoan->dealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflLoan->dealer->idDealer=$vidDealer;
        $vflLoan->idLoan=$vidLoan;
        switch(clspBLLoan::deleteInDataBase($vflLoan)){
            case 0:  $vresponse->alert("Imposible eliminar el prestamo, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showLoansRecordList();");
                     $vresponse->alert("Los datos del prestamo han sido eliminados correctamente");
                     break;
        }
        
        unset($vflLoan);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del prestamo, intente de nuevo");
	}
	
    unset($vidDealer, $vidLoan);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showOperationStatusList");
$vxajax->register(XAJAX_FUNCTION, "showDeliverersList");
$vxajax->register(XAJAX_FUNCTION, "showLoansRecordList");
$vxajax->register(XAJAX_FUNCTION, "deleteLoanData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>