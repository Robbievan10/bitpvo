<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
date_default_timezone_set('America/Mexico_City');

function showCustomersList($vcustomerKey, $vfilterForm, $vpersonType)
 {
	$vresponse= new xajaxResponse();
	
	try{
        if ( strcmp(trim($vcustomerKey), "")!=0 ){
            $vfilter ="WHERE c_customer.id_customer=" . trim($vcustomerKey) . " ";
            $vfilter.="AND ";
        }
        else{
            $vfilter ="WHERE ";
        }
        
        if ( $vpersonType==1){
            $vfilter.="c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
            if ( strcmp(trim($vfilterForm["txtname"]), "")!=0 ){
                $vfilter.="AND c_individualcustomer.fldname LIKE '%" . trim($vfilterForm["txtname"]) . "%' ";
            }
            if ( strcmp(trim($vfilterForm["txtfirstName"]), "")!=0 ){
                $vfilter.="AND c_individualcustomer.fldfirstName LIKE '%" . trim($vfilterForm["txtfirstName"]) . "%' ";
            }
            if ( strcmp(trim($vfilterForm["txtlastName"]), "")!=0 ){
                $vfilter.="AND c_individualcustomer.fldlastName LIKE '%" . trim($vfilterForm["txtlastName"]) . "%' ";
            }


            $vcustomers= new clscFLIndividualCustomer();
            clscBLIndividualCustomer::queryToDataBase($vcustomers, $vfilter);
            $vcustomersTotal=clscBLIndividualCustomer::total($vcustomers);
        }
        else{
            $vfilter.="c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
            if ( strcmp(trim($vfilterForm["txtname"]), "")!=0 ){
                $vfilter.="AND c_businesscustomer.fldbusinessName LIKE '%" . trim($vfilterForm["txtname"]) . "%' ";
            }


            $vcustomers= new clscFLBusinessCustomer();
            clscBLBusinessCustomer::queryToDataBase($vcustomers, $vfilter);
            $vcustomersTotal=clscBLBusinessCustomer::total($vcustomers);
        }
        
        for ($vcustomerNumber=0; $vcustomerNumber<$vcustomersTotal; $vcustomerNumber++){
            if ( $vcustomerNumber==0 ){
                $vdata='<table id="vgrdcustomersList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vcustomerKey" style="text-align:center; font-weight:bold;">Número</th>
                                    <th data-field="vcustomerName" style="text-align:center; font-weight:bold;">Cliente</th>
                                    <th data-field="vcustomerMunicipalityName" style="text-align:center;font-weight:bold;">Municipio</th>
                                    <th data-field="vcustomerLocality" style="text-align:center;font-weight:bold;">Localidad</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            if ( $vpersonType== 1){
                $vidCustomer=$vcustomers->individualsCustomers[$vcustomerNumber]->idCustomer;
                $vcustomerKey=$vcustomers->individualsCustomers[$vcustomerNumber]->idCustomer;
                $vcustomerName=$vcustomers->individualsCustomers[$vcustomerNumber]->name . ' ' .
                               $vcustomers->individualsCustomers[$vcustomerNumber]->firstName . ' ' .
                               $vcustomers->individualsCustomers[$vcustomerNumber]->lastName;
                $vmunicipalityName=$vcustomers->individualsCustomers[$vcustomerNumber]->municipality->municipality;
                $vlocality=$vcustomers->individualsCustomers[$vcustomerNumber]->locality;
            }
            else{
                $vidCustomer=$vcustomers->businessCustomers[$vcustomerNumber]->idCustomer;
                $vcustomerKey=$vcustomers->businessCustomers[$vcustomerNumber]->idCustomer;
                $vcustomerName=$vcustomers->businessCustomers[$vcustomerNumber]->businessName;
                $vmunicipalityName=$vcustomers->businessCustomers[$vcustomerNumber]->municipality->municipality;
                $vlocality=$vcustomers->businessCustomers[$vcustomerNumber]->locality;
            }
            
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrcustomer" onClick="setCustomerId('. $vidCustomer . ');" title="Seleccionar Cliente" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vcustomerKey . '</td>';
            $vdata.='	<td>' . $vcustomerName . '</td>';
            $vdata.='	<td>' . $vmunicipalityName . '</td>';
            $vdata.='	<td>' . $vlocality . '</td>';
            $vdata.="</tr>";
            
            unset($vidCustomer, $vcustomerKey, $vcustomerName, $vmunicipalityName, $vlocality);
        }
        if ( $vcustomerNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcustomersList", "innerHTML", $vdata);
            $vresponse->script("setCustomersList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen clientes registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcustomersList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vcustomers, $vcustomersTotal, $vcustomerNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los clientes, intente de nuevo" . $vexception->getMessage());
	}
	
    unset($vcustomerKey, $vfilterForm, $vpersonType);
	return $vresponse;
 }

function deleteCustomerData($vidCustomer)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflCustomerData= new clspFLCustomer();
		$vflCustomerData->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCustomerData->idCustomer=$vidCustomer;
        clspBLCustomer::queryToDataBase($vflCustomerData);
        if ( $vflCustomerData->personType->idPersonType==1 ){
            $vflCustomer= new clspFLIndividualCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflCustomerData->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflCustomerData->idCustomer;
            $vdeleteStatus=clspBLIndividualCustomer::deleteInDataBase($vflCustomer);           
        }
        else{
            $vflCustomer= new clspFLBusinessCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflCustomerData->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflCustomerData->idCustomer;
            $vdeleteStatus=clspBLBusinessCustomer::deleteInDataBase($vflCustomer);           
        }
        switch($vdeleteStatus){
            case -1: $vresponse->alert("Imposible eliminar al cliente, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible eliminar al cliente físico, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showCustomersList();");
                     $vresponse->alert("Los datos del cliente han sido eliminados correctamente");
                     break;
        }
        
        unset($vflCustomerData, $vflCustomer, $vdeleteStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del cliente, intente de nuevo");
	}
	
    unset($vidCustomer);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showCustomersList");
$vxajax->register(XAJAX_FUNCTION, "deleteCustomerData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>