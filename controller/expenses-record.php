<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLExpenseDetail.php");



require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

require_once(dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCash.php");

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");


require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");


date_default_timezone_set('America/Mexico_City');


function showExpensesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter ="WHERE c_expense.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vexpenses= new clscFLExpense();
        clscBLExpense::queryToDataBase($vexpenses, $vfilter);
        $vexpensesTotal=clscBLExpense::total($vexpenses);
        
        $vJSON='[{"text":"--Todos--", "value":"0"}';
        for ($vi=0; $vi<$vexpensesTotal; $vi++){
            $vJSON.=', {"text":"' . $vexpenses->expenses[$vi]->expense .
                    '", "value":' . $vexpenses->expenses[$vi]->idExpense  . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vexpenses, $vexpensesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los gastos, intente de nuevo");
	}

	return $vresponse;
 }


function getOpenCash()
 {
    
    try{
        date_default_timezone_set('America/Mexico_City');
        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_cashmovement.id_cashMovementStatus=1 ";
        $vfilter.="AND p_cashmovement.id_user='" . $_SESSION['idUser'] . "' ";
        //$vfilter.="AND p_cashmovement.fldrecordDate='" . date("Y-m-d") . "'";
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryToDataBase($vcashMovements, $vfilter)==1 ){
            //return $vcashMovements->cashMovements[0];
            return $vcashMovements->cashMovements[0]->cash->idCash;
            //return $vcashMovements->cashMovements[0]->cash->cash;
            
        }

        unset($vfilter, $vcashMovements);
        return null;
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
    
 }



function getOpenCashName()
 {
    try{
        date_default_timezone_set('America/Mexico_City');
        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_cashmovement.id_cashMovementStatus=1 ";
        $vfilter.="AND p_cashmovement.id_user='" . $_SESSION['idUser'] . "' ";
        //$vfilter.="AND p_cashmovement.fldrecordDate='" . date("Y-m-d") . "'";
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryToDataBase($vcashMovements, $vfilter)==1 ){
            //return $vcashMovements->cashMovements[0];
            //return $vcashMovements->cashMovements[0]->cash->idCash;
            return $vcashMovements->cashMovements[0]->cash->cash;
            
        }

        unset($vfilter, $vcashMovements);
        return null;
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

 
function showRoutesList()
 {
    $vresponse= new xajaxResponse();

    $userType = 0;

  
try {

    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}


    if ($userType==3) {

                try{
            $vroutes= new clscFLRoute();

            //clscBLRoute::queryToDataBase($vroutes, "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']." AND c_route.fldroute='".getOpenCash()."'");
            clscBLRoute::queryToDataBase($vroutes, "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']." AND c_route.fldroute='".getOpenCashName()."'");
            $vroutesTotal=clscBLRoute::total($vroutes);
            $vJSON='[{"text":"--Todas--", "value":"0"}';
            for ($vi=0; $vi<$vroutesTotal; $vi++){
                $vJSON.=', {"text":"' . $vroutes->routes[$vi]->route .
                        '", "value":' . $vroutes->routes[$vi]->idRoute . '}';
            }
            $vJSON.="]";
            
            $vresponse->setReturnValue($vJSON);
            unset($vroutes, $vroutesTotal, $vJSON, $vi);
        }
        catch (Exception $vexception){
            $vresponse->alert("Ocurrió un error al tratar de listar las rutas, intente de nuevo");
        }


    }else{



            try{
        $vroutes= new clscFLRoute();
        clscBLRoute::queryToDataBase($vroutes, "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']);
        $vroutesTotal=clscBLRoute::total($vroutes);
        $vJSON='[{"text":"--Todas--", "value":"0"}';
        for ($vi=0; $vi<$vroutesTotal; $vi++){
            $vJSON.=', {"text":"' . $vroutes->routes[$vi]->route .
                    '", "value":' . $vroutes->routes[$vi]->idRoute . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
        unset($vroutes, $vroutesTotal, $vJSON, $vi);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar las rutas, intente de nuevo");
    }



    }

	

	
	return $vresponse;
 }

function showExpensesRecordList($vfilterForm)
 {
	$vresponse= new xajaxResponse();

        $userType = 0;
    $fechaInicio = NULL;
    $fechaFinal = NULL;
    $fechaInicial = NULL;
try {

    if( isset($_SESSION['idUser']) ){
        $vflUser= new clspFLUser();
        $vflUser->idUser=trim($_SESSION['idUser']);
        clspBLUser::queryToDataBase($vflUser);
        //$vresponse->alert($vflUser->idUser);
        $vfilter ="WHERE c_enterpriseuser.id_enterprise='" . $_SESSION['idEnterprise'] . "' ";
        $vfilter.="AND c_user.id_user='".$vflUser->idUser."'";
        $venterpriseUsers= new clscFLEnterpriseUser();
        clscBLEnterpriseUser::queryToDataBase($venterpriseUsers, $vfilter);
        $venterpriseUsersTotal=clscBLEnterpriseUser::total($venterpriseUsers);

        $userType = $venterpriseUsers->enterpriseUsers[0]->userType->idUserType;
    }

} catch (Exception $vexception) {
            $vresponse->alert($vexception->getMessage());
}

    if ($userType==3) {
        $ruta = NULL;
            try{


        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        //$vfilter.="AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
        //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
        //$vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";

        $vfilter.="AND p_cashmovement.id_cash= " .getOpenCash(). " ";

        //$vfilter .= "AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial)) . "' ";
        //$vfilter .= "AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
        $vfilter.= "AND p_cashmovement.fldcloseTime is NULL";
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vcashMovements, $vfilter) ){
            $fechaInicio=$vcashMovements->cashMovements[0]->CreatedOn;
            //$fechaFinal = date('Y-m-d');
            //$fechaInicial = $vcashMovements->cashMovements[count($vcashMovements)-1]->recordDate;

        }
        
            $vroutes= new clscFLRoute();
            clscBLRoute::queryToDataBase($vroutes, "WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']." AND c_route.fldroute='".getOpenCashName()."'");
            $vroutesTotal=clscBLRoute::total($vroutes);
            for ($vi=0; $vi<$vroutesTotal; $vi++){
                $ruta = $vroutes->routes[0]->idRoute;
            }
            
        $vfilter ="WHERE p_expense.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_expense.CreatedOn>'" .$fechaInicio. "' ";
        $vfilter.="AND p_expense.id_route=" .$ruta. " ";


/*
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_expense.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }else{
            $vfilter.="AND (p_expense.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";  
        }
        if ( (int)($vfilterForm["cmbexpensesList"])!=0 ){
            $vfilter.="AND p_expense.id_expense=" . (int)($vfilterForm["cmbexpensesList"]) . " ";
        }
        if ( (int)($vfilterForm["cmbroutesList"])!=0 ){
            $vfilter.="AND p_expense.id_route=" . (int)($vfilterForm["cmbroutesList"]) . " ";
        }
        */
        $vexpensesDetails= new clscFLExpenseDetail();
        clscBLExpenseDetail::queryToDataBase($vexpensesDetails, $vfilter);
        $vexpensesDetailsTotal=clscBLExpenseDetail::total($vexpensesDetails);
        for ($vexpenseDetailNumber=0; $vexpenseDetailNumber<$vexpensesDetailsTotal; $vexpenseDetailNumber++){
            if ( $vexpenseDetailNumber==0 ){
                $vdata='<table id="vgrdexpensesRecordList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vexpenseRecordDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vexpenseName" style="text-align:center; font-weight:bold;">Gasto</th>
                                    <th data-field="vrouteName" style="text-align:center; font-weight:bold;">Ruta</th>
                                    <th data-field="vexpenseAmount" style="text-align:center;font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrexpenseDetail" onClick="setExpenseDetailIds('. 
                            $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->expense->idExpense . ',' .
                            $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->idExpenseDetail . ');" title="Seleccionar Gasto" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->recordDate. '</td>';
            $vdata.='   <td>' . $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->expense->expense . '</td>';
            $vdata.='   <td>' . $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->route->route . '</td>';
            $vdata.='   <td>' . "$ " . number_format($vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vexpenseDetailNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vexpensesRecordList", "innerHTML", $vdata);
            $vresponse->script("setExpensesRecordList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen gastos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vexpensesRecordList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vexpensesDetails, $vexpensesDetailsTotal, $vexpenseDetailNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los gastos, intente de nuevo");
    }
    }else{
    try{
        $vfilter ="WHERE p_expense.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        if ( strcmp(trim($vfilterForm["dtpckrstartDate"]), "")!=0 ){
            $vfilter.="AND (p_expense.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrstartDate"])))  . "' ";
            $vfilter.="AND '" . date("Y-m-d", strtotime(trim($vfilterForm["dtpckrendDate"]))) . "') ";
        }
        if ( (int)($vfilterForm["cmbexpensesList"])!=0 ){
            $vfilter.="AND p_expense.id_expense=" . (int)($vfilterForm["cmbexpensesList"]) . " ";
        }
        if ( (int)($vfilterForm["cmbroutesList"])!=0 ){
            $vfilter.="AND p_expense.id_route=" . (int)($vfilterForm["cmbroutesList"]) . " ";
        }
        $vexpensesDetails= new clscFLExpenseDetail();
        clscBLExpenseDetail::queryToDataBase($vexpensesDetails, $vfilter);
        $vexpensesDetailsTotal=clscBLExpenseDetail::total($vexpensesDetails);
        for ($vexpenseDetailNumber=0; $vexpenseDetailNumber<$vexpensesDetailsTotal; $vexpenseDetailNumber++){
            if ( $vexpenseDetailNumber==0 ){
                $vdata='<table id="vgrdexpensesRecordList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vexpenseRecordDate" style="text-align:center;font-weight:bold;">Fecha</th>
                                    <th data-field="vexpenseName" style="text-align:center; font-weight:bold;">Gasto</th>
                                    <th data-field="vrouteName" style="text-align:center; font-weight:bold;">Ruta</th>
                                    <th data-field="vexpenseAmount" style="text-align:center;font-weight:bold;">Monto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrexpenseDetail" onClick="setExpenseDetailIds('. 
                            $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->expense->idExpense . ',' .
                            $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->idExpenseDetail . ');" title="Seleccionar Gasto" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='   <td>' . $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->recordDate. '</td>';
            $vdata.='   <td>' . $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->expense->expense . '</td>';
            $vdata.='   <td>' . $vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->route->route . '</td>';
            $vdata.='   <td>' . "$ " . number_format($vexpensesDetails->expensesDetails[$vexpenseDetailNumber]->amount, 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vexpenseDetailNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vexpensesRecordList", "innerHTML", $vdata);
            $vresponse->script("setExpensesRecordList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen gastos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vexpensesRecordList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vexpensesDetails, $vexpensesDetailsTotal, $vexpenseDetailNumber, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de mostrar los gastos, intente de nuevo");
    }
    }

	

	
    unset($vfilterForm);
	return $vresponse;
 }

function deleteExpenseDetailData($vidExpense, $vidExpenseDetail)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflExpenseDetail= new clspFLExpenseDetail();
        $vflExpenseDetail->expense->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflExpenseDetail->expense->idExpense=$vidExpense;
        $vflExpenseDetail->idExpenseDetail=$vidExpenseDetail;
        switch(clspBLExpenseDetail::deleteInDataBase($vflExpenseDetail)){
            case 0:  $vresponse->alert("Imposible eliminar el gasto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showExpensesRecordList();");
                     $vresponse->alert("Los datos del gasto han sido eliminados correctamente");
                     break;
        }
        
        unset($vflExpenseDetail);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del gasto, intente de nuevo");
	}
	
    unset($vidExpense, $vidExpenseDetail);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showExpensesList");
$vxajax->register(XAJAX_FUNCTION, "showRoutesList");
$vxajax->register(XAJAX_FUNCTION, "showExpensesRecordList");
$vxajax->register(XAJAX_FUNCTION, "deleteExpenseDetailData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>