<?php


$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");

date_default_timezone_set('America/Mexico_City');

function showCashList()
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vfilter="WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise'];
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal=clscBLCash::total($vcash);
        for ($vcashNumber=0; $vcashNumber<$vcashTotal; $vcashNumber++){
            if ( $vcashNumber==0 ){
                $vdata='<table id="vgrdcashList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vcashName" style="text-align:center; font-weight:bold;">Caja</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrcash" onClick="showCashData('. $vcash->cash[$vcashNumber]->idCash . ');" title="Seleccionar Caja" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vcash->cash[$vcashNumber]->cash . '</td>';
            $vdata.="</tr>";
        }
        if ( $vcashNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcashList", "innerHTML", $vdata);
            $vresponse->script("setCashList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen cajas registradas.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcashList", "innerHTML", $vdata);
        }
            
        unset($vfilter, $vcash, $vcashTotal, $vcashNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar las cajas, intente de nuevo");
	}
	
	return $vresponse;
 }

function showCashData($vidCash)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflCash= new clspFLCash();
        $vflCash->idCash=$vidCash;
        $vflCash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLCash::queryToDataBase($vflCash)){
            case 0: $vresponse->alert("Los datos de la caja no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtname", $vresponse);
                    $vtext->setValue($vflCash->cash);
                    $vresponse->script("enableCashButtons();");
                    
                    unset($vtext);
                    break;
        }
	   
       unset($vflCash);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la caja, intente de nuevo");
	}
    
    unset($vidCash);
	return $vresponse;
 }

function addCashData($vcashForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflCash= new clspFLCash();
        $vflCash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCash->cash=trim($vcashForm["txtname"]);
        switch(clspBLCash::addToDataBase($vflCash)){
            case 0:  $vresponse->alert("Imposible registrar la caja, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidCash=" . $vflCash->idCash);
                     $vresponse->script("showCashList(0);");
                     $vresponse->alert("Los datos de la caja <" . $vflCash->cash . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflCash);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar la caja, intente de nuevo");
	}
	
    unset($vcashForm);
	return $vresponse;
 }

function updateCashData($vidCash, $vcashForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflCash= new clspFLCash();
        $vflCash->idCash=$vidCash;
        $vflCash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCash->cash=trim($vcashForm["txtname"]);
        switch(clspBLCash::updateInDataBase($vflCash)){
            case 0: $vresponse->alert("Ningún dato se ha modificado de la caja");
                    break;
            case 1: $vresponse->script("showCashList(1);");
                    $vresponse->alert("Los datos de la caja han sido modificados correctamente");
                    break;
        }
        
        unset($vflCash);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos de la caja, intente de nuevo");
	}
	
    unset($vidCash, $vcashForm);
	return $vresponse;
 }

function deleteCashData($vidCash)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflCash= new clspFLCash();
        $vflCash->idCash=$vidCash;
        $vflCash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLCash::deleteInDataBase($vflCash)){
            case 0:  $vresponse->alert("Imposible eliminar la caja, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidCash=0;");
                     $vresponse->script("cleanCashFormFields();");
                     $vresponse->script("showCashList(0);");
                     $vresponse->alert("Los datos de la caja han sido eliminados correctamente");
                     break;
        }
        
        unset($vflCash);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos de la caja, intente de nuevo");
	}
	
    unset($vidCash);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showCashList");
$vxajax->register(XAJAX_FUNCTION, "showCashData");
$vxajax->register(XAJAX_FUNCTION, "addCashData");
$vxajax->register(XAJAX_FUNCTION, "updateCashData");
$vxajax->register(XAJAX_FUNCTION, "deleteCashData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>