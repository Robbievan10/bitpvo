<?php
date_default_timezone_set('America/Mexico_City');

$vuploadDirectory = '../uploads/images/products/';
$vuploadFileName = $vuploadDirectory . basename($_FILES['userfile']['name']);
$vfileExtensionType=substr($vuploadFileName, strlen($vuploadFileName)-4, 4);

$vreturn="error";
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $vuploadFileName)){
	$vbytes=openssl_random_pseudo_bytes(10);
 	$vfileName=bin2hex($vbytes) . $vfileExtensionType;
	if ( rename($vuploadFileName, $vuploadDirectory . $vfileName) ){
		$vreturn=$vfileName;
	}
	else{
		unlink($vuploadFileName);
	}
}
echo $vreturn;
?>