<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLState.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLState.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");
date_default_timezone_set('America/Mexico_City');


function showStatesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vstates= new clscFLState();
        clscBLState::queryToDataBase($vstates, "");
        $vstatesTotal=clscBLState::total($vstates);
	
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vstatesTotal; $vi++){
            $vJSON.=', {"text":"' . $vstates->states[$vi]->state .
                    '", "value":' . $vstates->states[$vi]->idState . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vstates, $vstatesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados, intente de nuevo");
	}

	return $vresponse;
 }

function showMunicipalitiesList($vidState)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vmunicipalities= new clscFLMunicipality();
        clscBLMunicipality::queryToDataBase($vmunicipalities, "WHERE c_municipality.id_state=" . $vidState);
        $vmunicipalitiesTotal=clscBLMunicipality::total($vmunicipalities);
	
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vmunicipalitiesTotal; $vi++){
            $vJSON.=', {"text":"' . $vmunicipalities->municipalities[$vi]->municipality .
                    '", "value":' . $vmunicipalities->municipalities[$vi]->idMunicipality . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vmunicipalities, $vmunicipalitiesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados, intente de nuevo");
	}
    
    unset($vidState);
	return $vresponse;
 }

function showDealerData($vidDealer)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflDealer= new clspFLDealer();
		$vflDealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflDealer->idDealer=$vidDealer;
        switch(clspBLDealer::queryToDataBase($vflDealer)){
            case 0: $vresponse->alert("Los datos del repartidor no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtname", $vresponse);
                    $vtext->setValue($vflDealer->name);
                    $vtext= new clspText("txtfirstName", $vresponse);
                    $vtext->setValue($vflDealer->firstName);
                    $vtext= new clspText("txtlastName", $vresponse);
                    $vtext->setValue($vflDealer->lastName);
                    $vresponse->script("vcmbStatesList.value(" . $vflDealer->municipality->state->idState . "); showMunicipalitiesList();
                                        vcmbMunicipalitiesList.value(" . $vflDealer->municipality->idMunicipality . ");");
                    $vtext= new clspText("txtlocality", $vresponse);
                    $vtext->setValue($vflDealer->locality);
                    $vtext= new clspText("txtstreet", $vresponse);
                    $vtext->setValue($vflDealer->street);
                    $vtext= new clspText("txtnumber", $vresponse);
                    $vtext->setValue($vflDealer->number);
                    $vtext= new clspText("txtpostCode", $vresponse);
                    $vtext->setValue($vflDealer->postCode);
                    $vtext= new clspText("txtphoneNumber", $vresponse);
                    $vtext->setValue($vflDealer->phoneNumber);
                    $vtext= new clspText("txtmovilNumber", $vresponse);
                    $vtext->setValue($vflDealer->movilNumber);
                    $vstring= new clspString($vflDealer->observation);
				    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del repartidor, intente de nuevo");
	}
    
    unset($vidDealer);
	return $vresponse;
 }

function addDealerData($vdealerKey, $vdealerForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflDealer= new clspFLDealer();
        $vflDealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflDealer->key=trim($vdealerKey);
        $vflDealer->name=trim($vdealerForm["txtname"]);
        $vflDealer->firstName=trim($vdealerForm["txtfirstName"]);
        $vflDealer->lastName=trim($vdealerForm["txtlastName"]);
        $vflDealer->municipality->state->idState=(int)($vdealerForm["cmbstatesList"]);
        $vflDealer->municipality->idMunicipality=(int)($vdealerForm["cmbmunicipalitiesList"]);
        $vflDealer->locality=trim($vdealerForm["txtlocality"]);
        $vflDealer->street=trim($vdealerForm["txtstreet"]);
        $vflDealer->number=trim($vdealerForm["txtnumber"]);
        $vflDealer->postCode=trim($vdealerForm["txtpostCode"]);
        $vflDealer->phoneNumber=trim($vdealerForm["txtphoneNumber"]);
        $vflDealer->movilNumber=trim($vdealerForm["txtmovilNumber"]);
        $vflDealer->observation=trim($vdealerForm["txtobservation"]);
        switch(clspBLDealer::addToDataBase($vflDealer)){
            case -1: $vresponse->alert("Imposible registrar al repartidor, el número ya se encuentra registrado");
                     break;
            case 0:  $vresponse->alert("Imposible registrar al repartidor, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidDealer=" . $vflDealer->idDealer);
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Repartidor");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Repartidor");
                     $vresponse->alert("Los datos del repartidor <" . $vflDealer->name . ' ' .
                                                                      $vflDealer->firstName . ' ' .
                                                                      $vflDealer->lastName . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar al repartidor, intente de nuevo");
	}
	
    unset($vdealerKey, $vdealerForm);
	return $vresponse;
 }

function updateDealerData($vidDealer, $vdealerKey, $vdealerForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflDealer= new clspFLDealer();
        $vflDealer->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflDealer->idDealer=$vidDealer;
        $vflDealer->key=trim($vdealerKey);
        $vflDealer->name=trim($vdealerForm["txtname"]);
        $vflDealer->firstName=trim($vdealerForm["txtfirstName"]);
        $vflDealer->lastName=trim($vdealerForm["txtlastName"]);            
        $vflDealer->municipality->state->idState=(int)($vdealerForm["cmbstatesList"]);
        $vflDealer->municipality->idMunicipality=(int)($vdealerForm["cmbmunicipalitiesList"]);
        $vflDealer->locality=trim($vdealerForm["txtlocality"]);
        $vflDealer->street=trim($vdealerForm["txtstreet"]);
        $vflDealer->number=trim($vdealerForm["txtnumber"]);
        $vflDealer->postCode=trim($vdealerForm["txtpostCode"]);
        $vflDealer->phoneNumber=trim($vdealerForm["txtphoneNumber"]);
        $vflDealer->movilNumber=trim($vdealerForm["txtmovilNumber"]);
        $vflDealer->observation=trim($vdealerForm["txtobservation"]);
        switch(clspBLDealer::updateInDataBase($vflDealer)){
            case -1: $vresponse->alert("Imposible modificar el número del repartidor, ya se encuentra registrado");
                     break;
            case 0:  $vresponse->alert("Ningún dato se ha modificado del repartidor");
                     break;
            case 1:  $vresponse->alert("Los datos del repartidor han sido modificados correctamente");
                     break;
        }
        
        unset($vflDealer);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del repartidor, intente de nuevo");
	}
	
    unset($vidDealer, $vdealerKey, $vdealerForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showStatesList");
$vxajax->register(XAJAX_FUNCTION, "showMunicipalitiesList");
$vxajax->register(XAJAX_FUNCTION, "showDealerData");
$vxajax->register(XAJAX_FUNCTION, "updateDealerData");
$vxajax->register(XAJAX_FUNCTION, "addDealerData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>