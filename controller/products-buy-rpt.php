<?php

ini_set('max_execution_time', 300); //300 seconds = 5 minutes


require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");

require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");


require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLEnterpriseUser.php");



date_default_timezone_set('America/Mexico_City');


    $listaProveedores = array();
    $listaProveedoresID = array();





try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $totalPrice = 0;
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="existencia-productos";
    $vorientation="L";
    $vpageSize="Letter";
    $vrptProductsStock= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsStock->addPage();


        if((int)($_GET["idProvider"])!= 0){

        try{
        $vindividualsProviders= new clscFLIndividualProvider();
        $vfilter="WHERE c_individualprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_individualprovider.id_provider='".(int)($_GET["idProvider"])."'";
        clscBLIndividualProvider::queryToDataBase($vindividualsProviders, $vfilter);
        $vindividualsProvidersTotal=clscBLIndividualProvider::total($vindividualsProviders);
        
        
        for ($vi=0; $vi<$vindividualsProvidersTotal; $vi++){

            array_push($listaProveedores, $vindividualsProviders->individualsProviders[$vi]->name . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->firstName . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->lastName);
            array_push($listaProveedoresID, $vindividualsProviders->individualsProviders[$vi]->idProvider);
        }
        
        $vbusinessProviders= new clscFLBusinessProvider();
        $vfilter="WHERE c_businessprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_businessprovider.id_provider='".(int)($_GET["idProvider"])."'";
        clscBLBusinessProvider::queryToDataBase($vbusinessProviders, $vfilter);
        $vbusinessProvidersTotal=clscBLBusinessProvider::total($vbusinessProviders);
        for ($vi=0; $vi<$vbusinessProvidersTotal; $vi++){
            array_push($listaProveedores, $vbusinessProviders->businessProviders[$vi]->businessName);
            array_push($listaProveedoresID, $vbusinessProviders->businessProviders[$vi]->idProvider);
                }
    }
    catch (Exception $vexception){
    }
        }else{

                    try{
        $vindividualsProviders= new clscFLIndividualProvider();
        $vfilter="WHERE c_individualprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLIndividualProvider::queryToDataBase($vindividualsProviders, $vfilter);
        $vindividualsProvidersTotal=clscBLIndividualProvider::total($vindividualsProviders);
        

        for ($vi=0; $vi<$vindividualsProvidersTotal; $vi++){

            array_push($listaProveedores, $vindividualsProviders->individualsProviders[$vi]->name . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->firstName . ' ' .
                                    $vindividualsProviders->individualsProviders[$vi]->lastName);
            array_push($listaProveedoresID, $vindividualsProviders->individualsProviders[$vi]->idProvider);
        }
        
        $vbusinessProviders= new clscFLBusinessProvider();
        $vfilter="WHERE c_businessprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        clscBLBusinessProvider::queryToDataBase($vbusinessProviders, $vfilter);
        $vbusinessProvidersTotal=clscBLBusinessProvider::total($vbusinessProviders);
        for ($vi=0; $vi<$vbusinessProvidersTotal; $vi++){
            array_push($listaProveedores, $vbusinessProviders->businessProviders[$vi]->businessName);
            array_push($listaProveedoresID, $vbusinessProviders->businessProviders[$vi]->idProvider);
                }
    }
    catch (Exception $vexception){
    }
        }

}
catch (Exception $vexception){
    echo "Ocurrió un error al tratar de mostrar el reporte de existencia de productos";
}

function showPage($vrptProductsStock, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsStock, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsStock, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsStock, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsStock->x=50;	$vrptProductsStock->y=50;
        $vrptProductsStock->printMultiLine(700, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsStock->x=50;	$vrptProductsStock->y+=14;
        $vrptProductsStock->printMultiLine(720, 15, "\"Reporte de Compras a Proveedores\"" , 10, "B", "C", 0);
        $vrptProductsStock->x=50;	$vrptProductsStock->y+=12;
        $vrptProductsStock->printMultiLine(700, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }



 }

function showFooter($vrptProductsStock, $vpageNumber)
 {
    try{
        $vrptProductsStock->x=50;  $vrptProductsStock->y=535;
        $vrptProductsStock->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsStock->y=86;
        $vrptProductsStock->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }



function localStock($product_id,$nombre_ruta,$folio){

    $vproductStock = 0;
    $vroute = 0;
    $idcaja = 0;
    $folio_dist;
    $fechaInicio;
    $fechaFin = NULL;

try {

        //$vfilter="WHERE c_cash.id_enterprise='" . $_SESSION['idEnterprise']."' ";
        //$vfilter.="AND c_cash.fldcash=".$caja."' ";
        //$vfilter.="AND c_cash.fldcash=" . $caja;

        $vfilter ="WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_cash.fldcash='" . strval($nombre_ruta) . "'";
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal=clscBLCash::total($vcash);
        for ($vi=0; $vi<$vcashTotal; $vi++){
                    $idcaja = $vcash->cash[$vi]->idCash;
        }

        $vroutes= new clscFLRoute();
        $vroute;
        $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_route.fldroute='" . strval($nombre_ruta) . "'";
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal=clscBLRoute::total($vroutes);
        for ($vi=0; $vi<$vroutesTotal; $vi++){
                    $vroute = $vroutes->routes[$vi]->idRoute;
        }

        $vfilter ="WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter.="AND p_productdistribution.id_productDistribution='".$folio."'";
        //$vfilter.="ORDER BY p_productdistribution.idProductDistribution";

        $vproductsDistributions= new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal=clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber=0; $vproductsDistributionNumber<$vproductsDistributionsTotal; $vproductsDistributionNumber++){
            if($folio == $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution){
            $folio_dist = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
            $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;

            if($vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate != NULL && $vproductsDistributionNumber < $vproductsDistributionsTotal-1){
                $fechaFin = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber + 1]->CreatedOn;
            }

            }

        }

        if($vproductsDistributionNumber == 0){
                $vresponse->alert("No hay distribucion para esta ruta.");
        }

        $vfilter ="WHERE p_productsale.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        //$vfilter.="AND p_productsale.id_user='" . $_SESSION['idUser'] . "' ";
        //$vfilter.="AND p_productsale.id_cash=" . $vcashMovement . " ";
        $vfilter.="AND p_productsale.id_cash=" . $idcaja . " ";
        $vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";

        if($fechaFin != NULL){
        $vfilter.="AND p_productsale.CreatedOn < '" .$fechaFin."' ";
        }
        $vfilter.="AND p_productsale.fldcanceled = 0";

        $vproductsSales= new clscFLProductSale();
        clscBLProductSale::queryToDataBase($vproductsSales, $vfilter);
        $vproductsSalesTotal=clscBLProductSale::total($vproductsSales);

        for ($vproductsSaleNumber=0; $vproductsSaleNumber<$vproductsSalesTotal; $vproductsSaleNumber++){

            $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION['idEnterprise']. " ";
             $vfilter.="AND p_productsaledetail.id_productSale='" . $vproductsSales->productsSales[$vproductsSaleNumber]->idProductSale . "'";
                    $vproductsSalesDetails= new clscFLProductSaleDetail();
                    clscBLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter);
                    $vsaleProductsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
                    $vproductsSale=array();
                    for($vi=0; $vi<$vsaleProductsTotal; $vi++){

                        if($product_id == $vproductsSalesDetails->productsSalesDetails[$vi]->product->idProduct){

                            $vproductStock+=$vproductsSalesDetails->productsSalesDetails[$vi]->saleAmount;

                        }
                    }


        }

} catch (Exception $vexception) {
}
    $stock = $vproductStock;
    return $stock;
}

?>
