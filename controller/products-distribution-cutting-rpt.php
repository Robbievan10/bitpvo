<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

date_default_timezone_set('America/Mexico_City');

try{
    $c_y = 0;
    $vtotal = 0;

    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-landscape.pdf";
    $voutFileName="distribucion-productos-registro";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsDistribution= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsDistribution->addPage();
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    
    showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    
    $vfilter ="WHERE p_productdistributiondetail.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND p_productdistributiondetail.id_productDistribution='" . $_GET["vidProductDistribution"] . "'";
    $vproductsDistributionsDetails= new clscFLProductDistributionDetail();
    clscBLProductDistributionDetail::queryToDataBase($vproductsDistributionsDetails, $vfilter, 1);
    $vdistributionProductsTotal=clscBLProductDistributionDetail::total($vproductsDistributionsDetails);
    for ($vproductNumber=0; $vproductNumber<$vdistributionProductsTotal; $vproductNumber++){
        if( $vrptProductsDistribution->getY()>=710 ){
            $vrptProductsDistribution->addPage();
            //showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if( ($vproductNumber==0) || ($vpageNew) ){
            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(110, 20, "Folio de Distribuci�n:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=160;
            $vrptProductsDistribution->printMultiLine(70, 20, $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->idProductDistribution, 9, "", "L", 0);
            $vrptProductsDistribution->x=250;
            $vrptProductsDistribution->printMultiLine(115, 20, "Fecha de Distribuci�n:", 10, "B", "R", 0);
            $vrptProductsDistribution->x=365;
            $vrptProductsDistribution->printMultiLine(60, 20, $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->productDistributionDate, 9, "", "R", 0);
            $vrptProductsDistribution->x=435;  
            $vrptProductsDistribution->printMultiLine(32, 20, "Ruta:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=467;
            $vrptProductsDistribution->printMultiLine(120, 20, $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->route->route, 9, "", "L", 0);

            /*
            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=20;
            $vrptProductsDistribution->printMultiLine(90, 20, "Inventario Inicial:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=140;
            $vdistributionTotal=clscBLProductDistributionDetail::distributionTotal($vproductsDistributionsDetails);
            $vrptProductsDistribution->printMultiLine(100, 20, "$ " . number_format($vdistributionTotal, 2, ".", ","), 9, "", "L", 0);
            $vrptProductsDistribution->x=220;
            $vrptProductsDistribution->printMultiLine(110, 20, "Total de Distribuci�n:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=330;
            $vdistributedTotal=clscBLProductDistributionDetail::distributedTotal($vproductsDistributionsDetails);
            $vrptProductsDistribution->printMultiLine(90, 20, "$ " . number_format($vdistributedTotal, 2, ".", ","), 9, "", "L", 0);
            $vrptProductsDistribution->x=420;
            $vrptProductsDistribution->printMultiLine(85, 20, "Total Preliminar:", 10, "B", "L", 0);
            $c_y = $vrptProductsDistribution->y;

            */


$totalPagado = 0;

$json = $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->observation;

if($json!=NULL && $json != ""){
$json = utf8_encode($json);
$data = json_decode($json, true);


    $dataArr;
    foreach($data as $dataArr){

            if($dataArr['pagado'] == "" || $dataArr['pagado'] == null){
            }else{
            $totalPagado+= preg_replace('/[^\d.]/', '', $dataArr['pagado']);
            $vtotal+=$totalPagado;
            }
    }    
}
  
            $vcreditTotal=$vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->creditTotal;
            $vdiscountTotal=$vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->discountTotal;
            $vdevolutionTotal=$vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->devolutionTotal;

            $vtotal =$totalPagado + $vdistributedTotal-($vcreditTotal+$vdiscountTotal+$vdevolutionTotal);


            $vrptProductsDistribution->x=505;
            $vrptProductsDistribution->printMultiLine(85,20,"$ " . number_format($vtotal,2,".",","),10,"B","L",0);





/*
            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=20;
            $vrptProductsDistribution->printMultiLine(86, 20, "Total de Cr�dito:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=136;
            $vrptProductsDistribution->printMultiLine(104, 20, "$ " . number_format($vcreditTotal, 2, ".", ","), 9, "", "L", 0);
            $vrptProductsDistribution->x=240;
            $vrptProductsDistribution->printMultiLine(104, 20, "Total de Descuento:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=344;
            $vrptProductsDistribution->printMultiLine(96, 20, "$ " . number_format($vdiscountTotal, 2, ".", ","), 9, "", "L", 0);
            $vrptProductsDistribution->x=420;
            $vrptProductsDistribution->printMultiLine(105, 20, "Total de Devoluci�n:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=525;
            $vrptProductsDistribution->printMultiLine(90, 20, "$ " . number_format($vdevolutionTotal, 2, ".", ","), 9, "", "L", 0);

            */

            $vrptProductsDistribution->x=10; $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(70, 20, "C�digo" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=80;   
            $vrptProductsDistribution->printMultiLine(200, 20, "Producto" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=280;
            $vrptProductsDistribution->printMultiLine(80, 20, "Inv. Inicial" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=360;
            $vrptProductsDistribution->printMultiLine(80, 20, "Distribuido" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=440;
            $vrptProductsDistribution->printMultiLine(80, 20, "Inv. Final" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=520;
            $vrptProductsDistribution->printMultiLine(80, 20, "Total" , 10, "B", "C", 1);
            $vpageNew=false;
            
            unset($vdistributionTotal, $vdistributedTotal, $vcreditTotal, $vdiscountTotal, $vdevolutionTotal);
        }
        $vrptProductsDistribution->x=10;   $vrptProductsDistribution->y+=20;
        $vrptProductsDistribution->printMultiLine(70, 20,strtoupper ( $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->product->key) , 8, "", "C", 1);
        $vrptProductsDistribution->x=80;           
        $vrptProductsDistribution->printMultiLine(200, 20,strtoupper ( $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->product->name) , 8, "", "L", 1);
        $vrptProductsDistribution->x=280;
        $vdistributionAmount=$vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->distributionAmount;
        $vrptProductsDistribution->printMultiLine(80, 20, number_format($vdistributionAmount, 2, ".", ",") , 8, "", "C", 1);
        $vrptProductsDistribution->x=360;
        $vdistributedAmount=$vdistributionAmount - $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->returnedAmount;
        $vrptProductsDistribution->printMultiLine(80, 20, number_format($vdistributedAmount, 2, ".", ",") , 8, "", "C", 1);
        $vrptProductsDistribution->x=440;
        $vrptProductsDistribution->printMultiLine(80, 20, number_format($vdistributionAmount - $vdistributedAmount, 2, ".", ",") , 8, "", "C", 1);
        $vrptProductsDistribution->x=520;
        $vrptProductsDistribution->printMultiLine(80, 20, "$ " . number_format($vdistributedAmount * $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->distributionPrice, 2, ".", ",") , 8, "", "C", 1);
        
        unset($vdistributionAmount, $vdistributedAmount);
    }




/*

        if ( strcmp($vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->observation, "")!=0 ){
            $vrptProductsDistribution->addPage();
            $vpageNew=true;
            $vrptProductsDistribution->x=50;  $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(90, 20, "Cobranza:" , 10, "B", "L", 0);

            $vrptProductsDistribution->x=10;   $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(300, 20, "Cliente" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=310;
            $vrptProductsDistribution->printMultiLine(90, 20, "Folio" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=400;
            $vrptProductsDistribution->printMultiLine(100, 20, "Monto" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=500;
            $vrptProductsDistribution->printMultiLine(100, 20, "Abono" , 10, "B", "C", 1);




$json = $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->observation;


$json = utf8_encode($json);
$data = json_decode($json, true);


    $totalPagado = 0;
    foreach($data as $dataArr){
        if( $vrptProductsDistribution->getY()>=600 ){
            $vrptProductsDistribution->addPage();
            //showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }

            $vrptProductsDistribution->y+=20;
            $vrptProductsDistribution->x=10;
            $cliente = utf8_decode($dataArr['cliente']);
            $vrptProductsDistribution->printMultiLine(300, 20, $cliente , 10, "B", "C", 1);
            $vrptProductsDistribution->x=310;
            $vrptProductsDistribution->printMultiLine(90, 20, $dataArr['folio'] , 10, "B", "C", 1);
            $vrptProductsDistribution->x=400;
            $vrptProductsDistribution->printMultiLine(100, 20, $dataArr['monto'] , 10, "B", "C", 1);
            $vrptProductsDistribution->x=500;
            if($dataArr['pagado'] == "" || $dataArr['pagado'] == null){
                $vrptProductsDistribution->printMultiLine(100, 20, "$0.00" , 10, "B", "C", 1);
            }else{
            $totalPagado+= preg_replace('/[^\d.]/', '', $dataArr['pagado']);
            $vtotal+=$totalPagado;
            $vrptProductsDistribution->printMultiLine(100, 20,'$ '. number_format(floatval(preg_replace('/[^\d.]/', '', $dataArr['pagado'])),2, ".", ",") , 10, "B", "C", 1);
            }





    }      


        }

        */




            //$c_y_anterior = $vrptProductsDistribution->y;
            //$vrptProductsDistribution->y=$c_y;
            /*$vrptProductsDistribution->x=505;
            $vrptProductsDistribution->printMultiLine(85,20,"$ " . number_format($vtotal,2,".",","),10,"B","L",0);*/
            //$vrptProductsDistribution->y = $c_y_anterior;



    if( $vdistributionProductsTotal>0 ){
        $vrptProductsDistribution->x=120;  $vrptProductsDistribution->y+=40;
        $vrptProductsDistribution->printMultiLine(400, 15, "Repartidor" , 10, "B", "C", 0);
        $vrptProductsDistribution->y+=50;
        $vrptProductsDistribution->printLine(150, $vrptProductsDistribution->y, 480, $vrptProductsDistribution->y, 0.2);
        $vrptProductsDistribution->x=120;
        $vdealerName=$vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->dealer->name . " " .
                     $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->dealer->firstName . " " .
                     $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->dealer->lastName;
        $vrptProductsDistribution->printMultiLine(400, 20, $vdealerName , 8, "", "C", 0);
        
        unset($vdealerName);
    }
    $vrptProductsDistribution->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsDistribution, $vflEnterpriseUser, $vfilter,
          $vproductsDistributionsDetails, $vdistributionProductsTotal, $vproductNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de distribuci�n de productos";
}

function showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsDistribution, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsDistribution, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsDistribution, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsDistribution->x=100;	$vrptProductsDistribution->y=10;
        $vrptProductsDistribution->printMultiLine(400, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsDistribution->x=170;	$vrptProductsDistribution->y+=14;
        $vrptProductsDistribution->printMultiLine(300, 15, "\"Reporte de Distribuci�n de Productos (Corte)\"" , 10, "B", "C", 0);
        //$vrptProductsDistribution->x=50;	$vrptProductsDistribution->y+=12;
        //$vrptProductsDistribution->printMultiLine(700, 15, "Fecha de Impresi�n: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsDistribution, $vpageNumber)
 {
    /*
    try{
        $vrptProductsDistribution->x=250;  $vrptProductsDistribution->y=715;
        $vrptProductsDistribution->printMultiLine(80, 20, "P�gina Num. " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsDistribution->y=86;
        $vrptProductsDistribution->printMultiLine(10, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }	*/				
 }



?>