<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");
$vxajax->configure( 'defaultMode', 'synchronous' );
date_default_timezone_set('America/Mexico_City');
date_default_timezone_set('America/Mexico_City');

require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");

require_once(dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRouteDealer.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRouteDealer.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSalePayment.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductDistribution.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductDistributionDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashMovement.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSale.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductSaleDetail.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashMovement.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCash.php");
require_once(dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCash.php");

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");



function showCashList()
 {
	$vresponse= new xajaxResponse();

    try{
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, "");
        $vcashTotal=clscBLCash::total($vcash);

        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vcashTotal; $vi++){
            $vJSON.=', {"text":"' . $vcash->cash[$vi]->cash .
                    '", "value":' . $vcash->cash[$vi]->idCash . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
		unset($vcash, $vcashTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar las cajas, intente de nuevo");
	}

	return $vresponse;
 }

function showCashiersList()
 {
	$vresponse= new xajaxResponse();

    try{
        $vusers= new clscFLUser();
        clscBLUser::queryToDataBase($vusers, "WHERE c_user.id_userType=2 OR c_user.id_userType=3 OR c_user.id_userType=6 OR c_user.id_userType=7");
        $vusersTotal=clscBLUser::total($vusers);

        $vJSON='[{"text":"--Seleccionar--", "value":""}';
        for ($vi=0; $vi<$vusersTotal; $vi++){
            $vJSON.=', {"text":"' . $vusers->users[$vi]->name . ' ' .
                                    $vusers->users[$vi]->firstName . ' ' .
                                    $vusers->users[$vi]->lastName .
                    '", "value":"' . $vusers->users[$vi]->idUser . '"}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
		unset($vusers, $vusersTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los usuarios, intente de nuevo");
	}

	return $vresponse;
 }

function showCashTypesList()
 {
	$vresponse= new xajaxResponse();

    try{
        $vcashTypes= new clscFLCashType();
        clscBLCashType::queryToDataBase($vcashTypes, "");
        $vcashTypesTotal=clscBLCashType::total($vcashTypes);

        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vcashTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vcashTypes->cashTypes[$vi]->cashType .
                    '", "value":' . $vcashTypes->cashTypes[$vi]->idCashType . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
		unset($vcashTypes, $vcashTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de efectivo, intente de nuevo");
	}

	return $vresponse;
 }

function showDocumentTypesList()
 {
	$vresponse= new xajaxResponse();

    try{
        $vdocumentTypes= new clscFLDocumentType();
        clscBLDocumentType::queryToDataBase($vdocumentTypes, "");
        $vdocumentTypesTotal=clscBLDocumentType::total($vdocumentTypes);

        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vdocumentTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vdocumentTypes->documentTypes[$vi]->documentType .
                    '", "value":' . $vdocumentTypes->documentTypes[$vi]->idDocumentType . '}';
        }
        $vJSON.="]";

        $vresponse->setReturnValue($vJSON);
		unset($vdocumentTypes, $vdocumentTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de documento, intente de nuevo");
	}

	return $vresponse;
 }

function showCashCountCashList($vresponse)
 {
    try{
        $vtotals=0;
        $vcashCountCash=$_SESSION['vcashCountCashList'];
        $vcashCountCashTotal=count($vcashCountCash);
        for ($vi=0; $vi<$vcashCountCashTotal; $vi++){
            if ( $vi==0 ){
                $vdata='<table id="vgrdcashCountCashList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vcashTypeName" style="text-align:center; font-weight:bold;">Tipo</th>
                                    <th data-field="vcashAmount" style="text-align:center; font-weight:bold;">Cantidad</th>
                                    <th data-field="vcashValue" style="text-align:center; font-weight:bold;">Valor</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrcash" onClick="setCashIds('. $vcashCountCash[$vi][0] . ', ' . $vcashCountCash[$vi][3] . ');" title="Seleccionar Efectivo" />
                            <span class="custom-radio"></span>
                            </td>';
            $vdata.='	<td>' . $vcashCountCash[$vi][1]. '</td>';
            $vdata.='	<td>' . number_format($vcashCountCash[$vi][2], 2, ".", ",") . '</td>';
            $vdata.='	<td>$ ' . number_format($vcashCountCash[$vi][3], 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vcashCountCashTotal>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcashCountCashList", "innerHTML", $vdata);
            $vresponse->script("setCashCountCashList();
                                visThereListCashCountCash=true;
                                vcmdDeleteCash.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existe efectivo en el arqueo de caja.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcashCountCashList", "innerHTML", $vdata);
            $vresponse->script("visThereListCashCountCash=false;");
        }

        unset($vtotals, $vcashCountCash, $vcashCountCashTotal, $vi, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar el efectivo en el arqueo de caja, intente de nuevo");
	}
 }

function showCashCountDocumentList($vresponse)
 {
    try{
        $totalDocumentos = 0;
        $vtotals=0;
        $vcashCountDocument=$_SESSION['vcashCountDocumentList'];
        $vcashCountDocumentTotal=count($vcashCountDocument);
        for ($vi=0; $vi<$vcashCountDocumentTotal; $vi++){
            if ( $vi==0 ){
                $vdata='<table id="vgrdcashCountDocumentList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vdocumentTypeName" style="text-align:center; font-weight:bold;">Tipo</th>
                                    <th data-field="vdocumentConcept" style="text-align:center; font-weight:bold;">Concepto</th>
                                    <th data-field="vdocumentValue" style="text-align:center; font-weight:bold;">Valor</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrdocument" onClick="setDocumentIds('. $vcashCountDocument[$vi][0] . ', \'' . $vcashCountDocument[$vi][2] . '\');" title="Seleccionar Documento" />
                            <span class="custom-radio"></span>
                            </td>';
            $vdata.='	<td>' . $vcashCountDocument[$vi][1] . '</td>';
            $vdata.='	<td>' . $vcashCountDocument[$vi][2] . '</td>';
            $vdata.='	<td>$ ' . number_format($vcashCountDocument[$vi][3], 2, ".", ",") . '</td>';
            $vdata.="</tr>";



        }
        if ( $vcashCountDocumentTotal>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcashCountDocumentList", "innerHTML", $vdata);
            $vresponse->script("setCashCountDocumentList();
                                visThereListCashCountDocument=true;
                                vcmdDele
                                teDocument.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen documentos en el arqueo de caja.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcashCountDocumentList", "innerHTML", $vdata);
            $vresponse->script("visThereListCashCountDocument=false;");
        }

        unset($vtotals, $vcashCountDocument, $vcashCountDocumentTotal, $vi, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los documentos en el arqueo de caja, intente de nuevo");
	}
 }


function totalDocument($vresponse)
 {
    try{
        $vtotals=0;
        $vcashCountDocument=$_SESSION['vcashCountDocumentList'];
        $vcashCountDocumentTotal=count($vcashCountDocument);
        for ($vi=0; $vi<$vcashCountDocumentTotal; $vi++){
            if ( $vi==0 ){
                $vdata='<table id="vgrdcashCountDocumentList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vdocumentTypeName" style="text-align:center; font-weight:bold;">Tipo</th>
                                    <th data-field="vdocumentConcept" style="text-align:center; font-weight:bold;">Concepto</th>
                                    <th data-field="vdocumentValue" style="text-align:center; font-weight:bold;">Valor</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrdocument" onClick="setDocumentIds('. $vcashCountDocument[$vi][0] . ', \'' . $vcashCountDocument[$vi][2] . '\');" title="Seleccionar Documento" />
                            <span class="custom-radio"></span>
                            </td>';
            $vdata.='   <td>' . $vcashCountDocument[$vi][1] . '</td>';
            $vdata.='   <td>' . $vcashCountDocument[$vi][2] . '</td>';
            $vdata.='   <td>$ ' . number_format($vcashCountDocument[$vi][3], 2, ".", ",") . '</td>';
            $vdata.="</tr>";
        }
        if ( $vcashCountDocumentTotal>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vcashCountDocumentList", "innerHTML", $vdata);
            $vresponse->script("setCashCountDocumentList();
                                visThereListCashCountDocument=true;
                                vcmdDeleteDocument.enable(false);");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen documentos en el arqueo de caja.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vcashCountDocumentList", "innerHTML", $vdata);
            $vresponse->script("visThereListCashCountDocument=false;");
        }

        unset($vtotals, $vcashCountDocument, $vcashCountDocumentTotal, $vi, $vdata);
    }
    catch (Exception $vexception){
        $vresponse->alert("Ocurrió un error al tratar de listar los documentos en el arqueo de caja, intente de nuevo");
    }
 }


function showCashCountData($vidCash, $vidCashCount)
 {
    $vresponse= new xajaxResponse();

    try{
        $vflCashCount= new clspFLCashCount();
        $vflCashCount->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashCount->cash->idCash=$vidCash;
        $vflCashCount->idCashCount=$vidCashCount;
        switch(clspBLCashCount::queryToDataBase($vflCashCount)){
            case 0: $vresponse->alert("El arqueo de caja no se encuentra registrado");
                    break;
            case 1: $vresponse->script("vcmbCashList.value(" . $vflCashCount->cash->idCash . ");
                                        vcmbCashList.enable(false);");
                    $vtext= new clspText("dtpckrrecordDate", $vresponse);
                    $vtext->setValue($vflCashCount->recordDate);
                    $vresponse->script("vcmbCashiersList.value('" . $vflCashCount->userCashier->idUser . "');");

                    $vfilter ="WHERE p_cashcountcash.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
                    $vfilter.="AND p_cashcountcash.id_cash=" . $vflCashCount->cash->idCash . " ";
                    $vfilter.="AND p_cashcountcash.id_cashCount=" . $vflCashCount->idCashCount;
                    $vflCashCountCashList= new clscFLCashCountCash();
                    clscBLCashCountCash::queryToDataBase($vflCashCountCashList, $vfilter);
                    $vcashCountCashTotal=clscBLCashCountCash::total($vflCashCountCashList);
                    $vcashCountCash= array();
                    for($vi=0; $vi<$vcashCountCashTotal; $vi++){
                        $vcash= array();
                        array_push($vcash, $vflCashCountCashList->cashCountCash[$vi]->cashType->idCashType);
                        array_push($vcash, $vflCashCountCashList->cashCountCash[$vi]->cashType->cashType);
                        array_push($vcash, $vflCashCountCashList->cashCountCash[$vi]->amount);
                        array_push($vcash, $vflCashCountCashList->cashCountCash[$vi]->value);
                        array_push($vcashCountCash, $vcash);

                        unset($vcash);
                    }
                    $_SESSION['vcashCountCashList']=$vcashCountCash;
                    showCashCountCashList($vresponse);

                    $vfilter ="WHERE p_cashcountdocument.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
                    $vfilter.="AND p_cashcountdocument.id_cash=" . $vflCashCount->cash->idCash . " ";
                    $vfilter.="AND p_cashcountdocument.id_cashCount=" . $vflCashCount->idCashCount;
                    $vflCashCountDocumentList= new clscFLCashCountDocument();
                    clscBLCashCountDocument::queryToDataBase($vflCashCountDocumentList, $vfilter);
                    $vcashCountDocumentTotal=clscBLCashCountDocument::total($vflCashCountDocumentList);
                    $vcashCountDocument= array();
                    for($vi=0; $vi<$vcashCountDocumentTotal; $vi++){
                        $vdocument= array();
                        array_push($vdocument, $vflCashCountDocumentList->cashCountDocument[$vi]->documentType->idDocumentType);
                        array_push($vdocument, $vflCashCountDocumentList->cashCountDocument[$vi]->documentType->documentType);
                        array_push($vdocument, $vflCashCountDocumentList->cashCountDocument[$vi]->concept);
                        array_push($vdocument, $vflCashCountDocumentList->cashCountDocument[$vi]->value);
                        array_push($vcashCountDocument, $vdocument);

                        unset($vdocument);
                    }
                    $_SESSION['vcashCountDocumentList']=$vcashCountDocument;
                    showCashCountDocumentList($vresponse);

                    $vstring= new clspString($vflCashCount->observation);
				    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());



                    unset($vtext, $vfilter, $vflCashCountCashList, $vcashCountCashTotal, $vcashCountCash, $vflCashCountDocumentList,
                          $vcashCountDocumentTotal, $vcashCountDocument, $vstring, $vtextArea);
                    break;
        }

       unset($vflExpenseDetail);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del arqueo de caja, intente de nuevo");
	}

    unset($vidCash, $vidCashCount);
	return $vresponse;
 }

function addCashCountData($vcashCountForm)
 {
	$vresponse= new xajaxResponse();

	try{
        $vflCashCount= new clspFLCashCount();
        $vflCashCount->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashCount->cash->idCash=$vcashCountForm["cmbcashList"];
        $vflCashCount->userAuditor->idUser=$_SESSION['idUser'];
        $vflCashCount->userCashier->idUser=$vcashCountForm["cmbcashiersList"];
        $vflCashCount->recordDate=trim($vcashCountForm["dtpckrrecordDate"]);
        $vflCashCount->observation=$vcashCountForm["txtobservation"];

        $vcashCountCash=$_SESSION['vcashCountCashList'];
        $vcashCountCashTotal=count($vcashCountCash);
        $vflCashCountCashList= new clscFLCashCountCash();
        for($vi=0; $vi<$vcashCountCashTotal; $vi++){
            $vflCashCountCash= new clspFLCashCountCash();
            $vflCashCountCash->cashCount->cash->enterprise->idEnterprise=$vflCashCount->cash->enterprise->idEnterprise;
            $vflCashCountCash->cashCount->cash->idCash=$vflCashCount->cash->idCash;
            $vflCashCountCash->cashType->idCashType=$vcashCountCash[$vi][0];
            $vflCashCountCash->amount=$vcashCountCash[$vi][2];
            $vflCashCountCash->value=$vcashCountCash[$vi][3];
            clscBLCashCountCash::add($vflCashCountCashList, $vflCashCountCash);

            unset($vflCashCountCash);
        }
        $vcashCountDocument=$_SESSION['vcashCountDocumentList'];
        $vcashCountDocumentTotal=count($vcashCountDocument);
        $vflCashCountDocumentList= new clscFLCashCountDocument();
        for($vi=0; $vi<$vcashCountDocumentTotal; $vi++){
            $vflCashCountDocument= new clspFLCashCountDocument();
            $vflCashCountDocument->cashCount->cash->enterprise->idEnterprise=$vflCashCount->cash->enterprise->idEnterprise;
            $vflCashCountDocument->cashCount->cash->idCash=$vflCashCount->cash->idCash;
            $vflCashCountDocument->documentType->idDocumentType=$vcashCountDocument[$vi][0];
            $vflCashCountDocument->concept=$vcashCountDocument[$vi][2];
            $vflCashCountDocument->value=$vcashCountDocument[$vi][3];
            clscBLCashCountDocument::add($vflCashCountDocumentList, $vflCashCountDocument);

            unset($vflCashCountDocument);
        }
        switch(clspBLCashCount::addToDataBase($vflCashCount, $vflCashCountCashList, $vflCashCountDocumentList)){
            case -3: $vresponse->alert("Imposible registrar el efectivo en el arqueo de caja, intente de nuevo");
                     break;
            case -2: $vresponse->alert("Imposible registrar el(los) documento(s) en el arqueo de caja, intente de nuevo");
                     break;
            case -1: $vresponse->alert("Imposible registrar el arqueo de caja, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible registrar el arqueo de caja, no existe efectivo y/o documento(s)");
                     break;
            case 1:  $vresponse->script("vidCash=" . $vflCashCount->cash->idCash . ";" .
                                        "vidCashCount=" . $vflCashCount->idCashCount);
                     $vresponse->script("vcmbCashList.enable(false);");
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Arqueo de Caja");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Arqueo de Caja");
                     $vresponse->alert("Los datos del arqueo de caja han sido registrados correctamente");
                     break;
        }

        unset($vflCashCount,$vcashCountCash, $vcashCountCashTotal, $vflCashCountCashList, $vi, $vcashCountDocument, $vcashCountDocumentTotal,
              $vflCashCountDocumentList);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el arqueo de caja, intente de nuevo");
	}

    unset($vcashCountForm);
	return $vresponse;
 }

function updateCashCountData($vidCash, $vidCashCount, $vcashCountForm)
 {
	$vresponse= new xajaxResponse();

	try{
        $vflCashCount= new clspFLCashCount();
        $vflCashCount->cash->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflCashCount->cash->idCash=$vidCash;
        $vflCashCount->idCashCount=$vidCashCount;
        $vflCashCount->userAuditor->idUser=$_SESSION['idUser'];
        $vflCashCount->userCashier->idUser=$vcashCountForm["cmbcashiersList"];
        $vflCashCount->recordDate=trim($vcashCountForm["dtpckrrecordDate"]);
        $vflCashCount->observation=$vcashCountForm["txtobservation"];

        $vcashCountCash=$_SESSION['vcashCountCashList'];
        $vcashCountCashTotal=count($vcashCountCash);
        $vflCashCountCashList= new clscFLCashCountCash();
        for($vi=0; $vi<$vcashCountCashTotal; $vi++){
            $vflCashCountCash= new clspFLCashCountCash();
            $vflCashCountCash->cashCount->cash->enterprise->idEnterprise=$vflCashCount->cash->enterprise->idEnterprise;
            $vflCashCountCash->cashCount->cash->idCash=$vflCashCount->cash->idCash;
            $vflCashCountCash->cashType->idCashType=$vcashCountCash[$vi][0];
            $vflCashCountCash->amount=$vcashCountCash[$vi][2];
            $vflCashCountCash->value=$vcashCountCash[$vi][3];
            clscBLCashCountCash::add($vflCashCountCashList, $vflCashCountCash);

            unset($vflCashCountCash);
        }
        $vcashCountDocument=$_SESSION['vcashCountDocumentList'];
        $vcashCountDocumentTotal=count($vcashCountDocument);
        $vflCashCountDocumentList= new clscFLCashCountDocument();
        for($vi=0; $vi<$vcashCountDocumentTotal; $vi++){
            $vflCashCountDocument= new clspFLCashCountDocument();
            $vflCashCountDocument->cashCount->cash->enterprise->idEnterprise=$vflCashCount->cash->enterprise->idEnterprise;
            $vflCashCountDocument->cashCount->cash->idCash=$vflCashCount->cash->idCash;
            $vflCashCountDocument->documentType->idDocumentType=$vcashCountDocument[$vi][0];
            $vflCashCountDocument->concept=$vcashCountDocument[$vi][2];
            $vflCashCountDocument->value=$vcashCountDocument[$vi][3];
            clscBLCashCountDocument::add($vflCashCountDocumentList, $vflCashCountDocument);

            unset($vflCashCountDocument);
        }
        switch(clspBLCashCount::updateInDataBase($vflCashCount, $vflCashCountCashList, $vflCashCountDocumentList)){
            case -2: $vresponse->alert("Imposible modificar el efectivo en el arqueo de caja, intente de nuevo");
                     break;
            case -1: $vresponse->alert("Imposible modificar el(los) documento(s) en el arqueo de caja, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Ningún dato se ha modificado del arqueo de caja");
                     break;
            case 1:  $vresponse->alert("Los datos del arqueo de caja han sido modificados correctamente");
                     break;
        }

        unset($vflCashCount, $vcashCountCash, $vcashCountCashTotal, $vflCashCountCashList, $vi, $vcashCountDocument, $vcashCountDocumentTotal,
              $vflCashCountDocumentList);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del arqueo de caja, intente de nuevo");
	}

    unset($vidCash, $vidCashCount, $vcashCountForm);
	return $vresponse;
 }

function addCashToCashCountCashList($value,$amount)
 {
	$vresponse= new xajaxResponse();
    for($i=0;$i<7;$i++){
	try{

        if($value[$i] == 1 || $value[$i] == 10){
        $vflCashType= new clspFLCashType();
        $vflCashType->idCashType=1;
        clspBLCashType::queryToDataBase($vflCashType);
        $vcashCountCash=$_SESSION['vcashCountCashList'];
        $tipo = 1;
        }
        else{
        $vflCashType= new clspFLCashType();
        $vflCashType->idCashType=2;
        clspBLCashType::queryToDataBase($vflCashType);
        $vcashCountCash=$_SESSION['vcashCountCashList'];
        $tipo = 2;
                }

        if ( getCashIndexOnList($tipo, (float)($value[$i]), $vcashCountCash)==-1 ){
            $vcash= array();
            array_push($vcash, $vflCashType->idCashType);
            array_push($vcash, $vflCashType->cashType);
            array_push($vcash, (float)(str_replace(",", "", $amount[$i])));
            array_push($vcash, (float)(str_replace(",", "", $value[$i])));

            array_push($vcashCountCash, $vcash);
            $_SESSION['vcashCountCashList']=$vcashCountCash;
            showCashCountCashList($vresponse);

            unset($vcash);
        }
        else{

        }

        unset($vflCashType, $vcashCountCash);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de agregar el efectivo al arqueo de caja, intente de nuevo");
	}
    }

    unset($value,$amount);
	return $vresponse;
 }

function addDocumentToCashCountDocumentList($vdocumentAddForm)
 {
	$vresponse= new xajaxResponse();

	try{
        $vflDocumentType= new clspFLDocumentType();
        $vflDocumentType->idDocumentType=(int)($vdocumentAddForm["cmbdocumentTypesList"]);
        clspBLDocumentType::queryToDataBase($vflDocumentType);
        $vcashCountDocument=$_SESSION['vcashCountDocumentList'];
        if ( getDocumentIndexOnList((int)($vdocumentAddForm["cmbdocumentTypesList"]), trim($vdocumentAddForm["txtdocumentConcept"]), $vcashCountDocument)==-1 ){
            $vdocument= array();
            array_push($vdocument, $vflDocumentType->idDocumentType);
            array_push($vdocument, $vflDocumentType->documentType);
            array_push($vdocument, trim($vdocumentAddForm["txtdocumentConcept"]));
            array_push($vdocument, (float)(str_replace(",", "", trim($vdocumentAddForm["txtdocumentValue"]))));

            array_push($vcashCountDocument, $vdocument);
            $_SESSION['vcashCountDocumentList']=$vcashCountDocument;
            showCashCountDocumentList($vresponse);

            unset($vdocument);
        }
        else{
            $vresponse->alert("El documento <" . $vflDocumentType->documentType . "> con concepto <" . $vdocumentAddForm["txtdocumentConcept"] . "> ya se encuentra agregado en la lista.");
        }

        unset($vflDocumentType, $vcashCountDocument);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de agregar el documento al arqueo de caja, intente de nuevo");
	}

    unset($vcashAddForm);
	return $vresponse;
 }

function deleteCashOfCashCountCashList($vidCashType, $vcashValue)
 {
    $vresponse= new xajaxResponse();

	try{
        $vcashCountCash=$_SESSION['vcashCountCashList'];
        $vcashIndexFound=getCashIndexOnList($vidCashType, $vcashValue, $vcashCountCash);
        if ( $vcashIndexFound!=-1 ){
            array_splice($vcashCountCash, $vcashIndexFound, 1);
            $_SESSION['vcashCountCashList']=$vcashCountCash;
            showCashCountCashList($vresponse);
            $vresponse->alert("El efectivo ha sido eliminado correctamente del arqueo de caja");
        }

        unset($vcashCountCash, $vcashIndexFound);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar el efectivo del arqueo de caja, intente de nuevo");
	}

    unset($vidCashType, $vcashValue);
	return $vresponse;
 }

function deleteDocumentOfCashCountDocumentList($vidDocumentType, $vdocumentValue)
 {
    $vresponse= new xajaxResponse();

	try{
        $vcashCountDocument=$_SESSION['vcashCountDocumentList'];
        $vdocumentIndexFound=getDocumentIndexOnList($vidDocumentType, $vdocumentValue, $vcashCountDocument);
        if ( $vdocumentIndexFound!=-1 ){
            array_splice($vcashCountDocument, $vdocumentIndexFound, 1);
            $_SESSION['vcashCountDocumentList']=$vcashCountDocument;
            showCashCountDocumentList($vresponse);
            $vresponse->alert("El documento ha sido eliminado correctamente del arqueo de caja");
        }

        unset($vcashCountDocument, $vdocumentIndexFound);
	   }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar el documento del arqueo de caja, intente de nuevo");
	}

    unset($vidDocumentType, $vdocumentValue);
	return $vresponse;
 }

function getCashIndexOnList($vcashType, $vcashValue, $vcashCountCash)
 {
    try{
        $vcashIndexFound=-1;
        $vcashCountCashTotal=count($vcashCountCash);
        for ($vi=0; $vi<$vcashCountCashTotal; $vi++){
            if ( ($vcashType==$vcashCountCash[$vi][0]) && ($vcashValue==$vcashCountCash[$vi][3]) ){
                $vcashIndexFound=$vi;
                $vi=$vcashCountCashTotal;
            }
        }

        unset($vcashCountCashTotal, $vi);
        return $vcashIndexFound;
    }
	catch (Exception $vexception){
		throw new Exception($vexception->getMessage(), $vexception->getCode());
	}

    unset($vcashType, $vcashValue, $vcashCountCash);
 }

function getDocumentIndexOnList($vdocumentType, $vdocumentConcept, $vcashCountDocument)
 {
    try{
        $vdocumentIndexFound=-1;
        $vcashCountDocumentTotal=count($vcashCountDocument);
        for ($vi=0; $vi<$vcashCountDocumentTotal; $vi++){
            if ( ($vdocumentType==$vcashCountDocument[$vi][0]) && ($vdocumentConcept==$vcashCountDocument[$vi][2]) ){
                $vdocumentIndexFound=$vi;
                $vi=$vcashCountDocumentTotal;
            }
        }

        unset($vcashCountDocumentTotal, $vi);
        return $vdocumentIndexFound;
    }
	catch (Exception $vexception){
		throw new Exception($vexception->getMessage(), $vexception->getCode());
	}

    unset($vdocumentType, $vdocumentConcept, $vcashCountDocument);
 }

function cleanCashCountCashList()
 {
	$vresponse= new xajaxResponse();

    $_SESSION['vcashCountCashList']=array();
    $vresponse->assign("vcashCountCashList", "innerHTML", "");
    $vresponse->script("visThereListCashCountCash=false;");

	return $vresponse;
 }

function cleanCashCountDocumentList()
 {
	$vresponse= new xajaxResponse();

    $_SESSION['vcashCountDocumentList']=array();
    $vresponse->assign("vcashCountDocumentList", "innerHTML", "");
    $vresponse->script("visThereListCashCountDocument=false;");

	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();

	session_destroy();
	$vresponse->redirect("./");

	return $vresponse;
 }


function obtenerCaja($idcaja)
 {
    $vresponse = NULL;

    try{
        $vfilter = "WHERE c_cash.id_cash='".$idcaja."'";
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal=clscBLCash::total($vcash);


            $vresponse = $vcash->cash[0]->cash;

        unset($vcash, $vcashTotal, $vJSON, $vi);
    }
    catch (Exception $vexception){
    }

    return $vresponse;
 }


 function obtenerTotalCaja($fechaInicial,$fechaFinal,$idCaja){
    $vresponse= new xajaxResponse();

        //------------------------------------------------------Ventas de Cajas---------------------------------------------
    $vinitialCashByCash=0;
    $vcashSaleAmountByCash=0;
    $vcreditSaleAmountByCash=0;
    $fechaInicio = NULL;
    $fechaFinal = NULL;
    $fechaInicial = NULL;
    $nombreCaja = obtenerCaja($idCaja);
    $respuestaCobranza;
    $distribucion;
    $vflProductDistribution;
    $ruta;
    $vdata;
    $cobranzadata;
    $nombreClienteCobranza;
    $creditoFolio = [];
    $creditoCliente = [];
    $creditoCantidad = [];
    $creditoFecha = [];

    $cobranzaFolio = [];
    $cobranzaCliente = [];
    $cobranzaCantidad = [];
    $cobranzaFecha = [];


    $gastosFolio = [];
    $gastosConcepto = [];
    $gastosCantidad = [];
    $gastosFecha = [];

    $z;


    //$vfilter ="WHERE c_routecustomer.id_route IS NULL ";
    //$vfilter.="AND p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";



        $vfilter ="WHERE c_cash.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_cash.fldcash='" . strval($nombreCaja) . "'";
        $vcash= new clscFLCash();
        clscBLCash::queryToDataBase($vcash, $vfilter);
        $vcashTotal=clscBLCash::total($vcash);
        for ($vi=0; $vi<$vcashTotal; $vi++){
                    $idcaja = $vcash->cash[$vi]->idCash;
        }


        $z = $idcaja;

        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        //$vfilter.="AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
        //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
        //$vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";
        $vfilter.="AND p_cashmovement.id_cash= " . $idCaja . " ";

        //$vfilter .= "AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial)) . "' ";
        //$vfilter .= "AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
        $vfilter.= "AND p_cashmovement.fldcloseTime is NULL";
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vcashMovements, $vfilter) ){
            $fechaInicio=$vcashMovements->cashMovements[0]->CreatedOn;
            $fechaFinal = date('Y-m-d');
            $fechaInicial = $vcashMovements->cashMovements[0]->recordDate;

        }







        $vroutes= new clscFLRoute();
        $vroute;
        $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise']. " ";
        $vfilter.="AND c_route.fldroute='" . strval($nombreCaja) . "'";
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal=clscBLRoute::total($vroutes);
        for ($vi=0; $vi<$vroutesTotal; $vi++){
                    $vroute = $vroutes->routes[$vi]->idRoute;
                    $ruta = $vroute;
        }


        /*

        $vfilter ="WHERE p_productdistribution.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productdistribution.id_route=" . $vroute . " ";
        //$vfilter.="AND p_productdistribution.id_productDistribution='".$folio."'";
        //$vfilter.="ORDER BY p_productdistribution.idProductDistribution";

        $vproductsDistributions= new clscFLProductDistribution();
        clscBLProductDistribution::queryToDataBase($vproductsDistributions, $vfilter);
        $vproductsDistributionsTotal=clscBLProductDistribution::total($vproductsDistributions);
        for ($vproductsDistributionNumber=0; $vproductsDistributionNumber<$vproductsDistributionsTotal; $vproductsDistributionNumber++){
            $fechaInicio = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->CreatedOn;
            $fechaInicial = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionDate;
            $fechaFinal = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->productDistributionCuttingDate;
            $distribucion = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber]->idProductDistribution;
            $vflProductDistribution = $vproductsDistributions->productsDistributions[$vproductsDistributionNumber];
        }

        */

/*
    try {
        //$vflProductDistribution = new clspFLProductDistribution();
        $vflProductDistribution->enterprise->idEnterprise = $_SESSION['idEnterprise'];
        $vflProductDistribution->idProductDistribution    = $distribucion;
        switch (clspBLProductDistribution::queryToDataBase($distribucion)) {
            case 0:
                $vresponse->alert("Los datos del reparto de productos no se encuentran registrados");
                break;
            case 1:
                //$timeStamp = $vflProductDistribution->CreatedOn;

                $vfilter = "WHERE p_productdistributiondetail.id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
                $vfilter .= "AND p_productdistributiondetail.id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
                $vproductsDistributionsDetails = new clscFLProductDistributionDetail();
                clscBLProductDistributionDetail::queryToDataBase($vproductsDistributionsDetails, $vfilter);

                if ($vflProductDistribution->observation === NULL || $vflProductDistribution->observation == "") {
                    $vdata = "";
                    $vresponse->assign("divCobranza", "innerHTML", $vdata);
                } else {
                    $vdata = '<table id="vgrdproductsSalesList">
                            <thead>
                                <tr>
                                    <th style="text-align:center; font-weight:bold;">Folio</th>
                                    <th data-field="vproductSaleCustomer" style="text-align:center; font-weight:bold;">Cliente</th>
                                    <th data-field="vtotalAmount" style="text-align:center; font-weight:bold;">Monto</th>
                                    <th data-field="vproductSaleDate" style="text-align:center; font-weight:bold;">Abono</th>

                                </tr>
                            </thead>';

                    $json = $vflProductDistribution->observation;
                    $json = utf8_encode($json);
                    $data = json_decode($json, true);


                    foreach ($data as $dataArr) {
                        $cliente = utf8_decode($dataArr['cliente']);
                        $fecha   = $dataArr['fecha'];
                        $folio   = $dataArr['folio'];
                        $folio   = preg_replace('/\s+/', '', $folio);
                        $folio   = str_replace(' ', '', $folio);
                        $monto   = $dataArr['monto'];
                        $monto   = str_replace('$', '', $monto);

                        $vdata .= '<tr>
                        <td data-field="vproductSaleDate">
                            ' . $folio . '
                        </td>';


                        $vdata .= '   <td data-field="vproductSaleCustomer">' . $cliente . '</td>';

                        $vdata .= '   <td data-field="vproductSaleDate">$ ' . $monto . '</td>';


                        $abono = ProductsSalePaymentsList($folio,$vflProductDistribution->CreatedOn);
                        $vdata .= '   <td data-field="vproductSaleDate"><input onblur="getdeliveryTotal()" type="text"   value="' . number_format($abono, 2, ".", ","). '"readonly>
                                </td>';
                        $vdata .= "</tr>";

                    }
                    $vdata .= '</tbody>
                </table>';
                $respuestaCobranza = $vdata;
                }
                unset($vtext, $vfilter, $vproductsDistributionsDetails);
                break;
        }

        unset($vflProductDistribution);
    }
    catch (Exception $vexception) {
        $vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la distribución de productos, intente de nuevo");
    }

*/




    $vfilter="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";
    $vfilter.="AND p_productsale.fldcanceled=0";
    $vproductsSalesDetails= new clscFLProductSaleDetail();
    clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsSalesDetails, $vfilter, 1);
    $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);


        $vfilter ="WHERE p_cashmovement.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        //$vfilter.="AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
        //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
        //$vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";
        $vfilter.="AND p_cashmovement.id_cash= " . $idCaja . " ";
        $vfilter .= "AND (p_cashmovement.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial)) . "' ";
        $vfilter .= "AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
        //$vfilter.= "AND p_cashmovement.fldcloseTime is NULL";
        $vinitialCash=0;
        $vcashMovements= new clscFLCashMovement();
        if ( clscBLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vcashMovements, $vfilter) ){
            $vinitialCash=$vcashMovements->cashMovements[0]->openAmount;
            $vinitialCashByCash+=$vinitialCash;
        }

     /*
     $vfilter ="WHERE c_routecustomer.id_route IS NOT NULL ";
     $vfilter="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
     //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
     //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
     $vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";
     $vfilter.="AND p_productsale.id_cash= " . $idCaja . " ";
     $vfilter.="AND p_productsale.id_operationType=1 ";
     $vfilter.="AND p_productsale.fldcanceled=0";
     */

    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND c_routecustomer.id_route= " . $ruta . " ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter.="AND p_productsale.id_cash= " . $idCaja . " ";
    $vfilter.="AND p_productsale.CreatedOn >'".$fechaInicio."' ";
    //$vfilter.="AND p_productsale.CreatedOn > " . $fechaInicio . " ";
    //$vfilter.="AND p_productsale.id_operationStatus!='2' ";
    $vfilter.="AND p_productsale.fldcanceled=0 ";
    $vfilter.="AND p_productsale.id_operationType=" . 1 . " ";


     $vcashSaleAmount=0;
     $vproductsCashSalesDetails= new clscFLProductSaleDetail();
     //if ( clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter) ){
     if (clscBLProductSaleDetail::queryDeliverersGroupByRouteToDataBase($vproductsCashSalesDetails, $vfilter)){
         $vcashSaleAmountByCash+=$vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
     }
     //}


/*
     $vfilter ="WHERE c_routecustomer.id_route IS NOT NULL ";
     $vfilter="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
     //$vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
     //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
     $vfilter.="AND p_productsale.CreatedOn > '" .$fechaInicio."' ";
     $vfilter.="AND p_productsale.id_cash= " . $idCaja . " ";
     $vfilter.="AND p_productsale.id_operationType=2 ";
     $vfilter.="AND p_productsale.fldcanceled=0";
     $vcashSaleAmount=0;
     $vproductsCashSalesDetails= new clscFLProductSaleDetail();
     //if ( clscBLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vproductsCashSalesDetails, $vfilter) ){
     if (clscBLProductSaleDetail::queryDeliverersGroupByRouteToDataBase($vproductsCashSalesDetails, $vfilter)){
         $vcreditSaleAmountByCash+=$vproductsCashSalesDetails->productsSalesDetails[0]->salePrice;
     }
     //}
*/



    //------------------------------------------------------Pagos por Cobros (Clientes)---------------------------------
    $vpaymentAmountByCustomers=0;
    $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    //$vfilter.="AND (p_payment.fldpaymentDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    //$vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter.="AND p_payment.CreatedOn > '" .$fechaInicio."' ";
    $vfilter.="AND p_productsale.id_operationType='2' ";
    $vfilter.="AND p_productsale.id_cash='" . $idCaja . "'";
    $vproductsSalesPayments= new clscFLProductSalePayment();
    clscBLProductSalePayment::queryGroupByCustomerToDataBase($vproductsSalesPayments, $vfilter, 1);
    $vproductsSalesPaymentsTotal=clscBLProductSalePayment::total($vproductsSalesPayments);
    for ($vproductsSalesPaymentsNumber=0; $vproductsSalesPaymentsNumber<$vproductsSalesPaymentsTotal; $vproductsSalesPaymentsNumber++){

          $vname=$vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->property["name"];
          if ( $vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->personType->idPersonType==1 ){
              $vname.=" " . $vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->property["firstName"] .
                      " " . $vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->customer->property["lastName"];
          }
          $nombreClienteCobranza = $vname;
          array_push($cobranzaFolio,$vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->idProductSale);
          array_push($cobranzaCantidad,number_format($vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->amount,2));
          array_push($cobranzaFecha,$vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->productSale->productSaleDate);
          array_push($cobranzaCliente,$vname);
          $vpaymentAmount=$vproductsSalesPayments->productsSalesPayments[$vproductsSalesPaymentsNumber]->amount;
          $vpaymentAmountByCustomers+=$vpaymentAmount;


        }


    //--------------------------------------------------FIN Pagos por Cobros (Clientes)---------------------------------




    //Inicio gastos
    $vexpenseAmountByName = 0;
    $vfilter ="WHERE p_expense.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND (p_expense.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter.="AND p_expense.CreatedOn >'".$fechaInicio."' ";
    $vfilter.= " AND p_expense.id_route='".$vroute."'";
    $vexpensesDetails= new clscFLExpenseDetail();
    clscBLExpenseDetail::queryGroupByNameToDataBase($vexpensesDetails, $vfilter, 1);
    $vexpensesDetailsTotal=clscBLExpenseDetail::total($vexpensesDetails);
    for ($vexpensesDetailsNumber=0; $vexpensesDetailsNumber<$vexpensesDetailsTotal; $vexpensesDetailsNumber++){

        $vexpenseAmount=$vexpensesDetails->expensesDetails[$vexpensesDetailsNumber]->amount;
        $vexpenseAmountByName+=$vexpenseAmount;

        array_push($gastosCantidad,number_format($vexpensesDetails->expensesDetails[$vexpensesDetailsNumber]->amount,2));
        array_push($gastosConcepto,$vexpensesDetails->expensesDetails[$vexpensesDetailsNumber]->expense->expense);
        array_push($gastosFolio,$vexpensesDetails->expensesDetails[$vexpensesDetailsNumber]->idExpenseDetail);
        array_push($gastosFecha,$vexpensesDetails->expensesDetails[$vexpensesDetailsNumber]->recordDate);

        //unset($vexpenseAmount);
    }

    //Fin gastos



//Inicio Créditos otorgados

$vgTotalSalePrice=0;

$vflEnterpriseUser= new clspFLEnterpriseUser();
$vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);

$vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
$vfilter.="AND c_route.id_route=" . $ruta;

$vroutes= new clscFLRoute();
clscBLRoute::queryToDataBase($vroutes, $vfilter);
$vroutesTotal=clscBLRoute::total($vroutes);

    $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    $vfilter.="AND c_routecustomer.id_route= " . $ruta . " ";
    $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime($fechaInicial))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime($fechaFinal)) . "') ";
    $vfilter.="AND p_productsale.id_cash= " . $idCaja . " ";
    $vfilter.="AND p_productsale.CreatedOn >'".$fechaInicio."' ";
    //$vfilter.="AND p_productsale.CreatedOn > " . $fechaInicio . " ";
    $vfilter.="AND p_productsale.id_operationStatus!='2' ";
    $vfilter.="AND p_productsale.fldcanceled=0 ";
    $vfilter.="AND p_productsale.id_operationType=" . 2 . " ";



    $vproductsSalesDetails= new clscFLProductSaleDetail();
    if ( clscBLProductSaleDetail::querySalesGroupByRouteToDataBase($vproductsSalesDetails, $vfilter, 1) ){
        $vsaleTotal=0;
        //$vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
        //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
        //$vrptProductsSales->x=80;
        //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
        $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
        for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){

            //$vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
            //$vrptProductsSales->printMultiLine(75, 15, "Folio de Venta:" , 9, "B", "L", 0);
            //$vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
            //$vrptProductsSales->x=125;
            //$vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale, 8, "", "L", 0);

            //$vrptProductsSales->x=250;
            //$vrptProductsSales->printMultiLine(70, 15, "Tipo de Venta:" , 9, "B", "L", 0);
            //$vrptProductsSales->x=320;
            //$vrptProductsSales->printMultiLine(60, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->operationType->operationType, 8, "", "L", 0);
            //$vrptProductsSales->x=420;
            //$vrptProductsSales->printMultiLine(80, 15, "Total:" , 9, "B", "R", 0);
            //$vrptProductsSales->x=500;
            //$vrptProductsSales->printMultiLine(70, 15, "$ " . number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice, 2, ".", ","), 8, "", "R", 0);
            //$vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
            //$vrptProductsSales->printMultiLine(40, 15, "Cliente:" , 9, "B", "L", 0);

            if ( $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->personType->idPersonType==1 ){
                $vflCustomer= new clspFLIndividualCustomer();
                $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                clspBLIndividualCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->name . ' ' . $vflCustomer->firstName . ' ' . $vflCustomer->lastName;
                $vcustomerName = $vcustomerName;
            }
            else{
                $vflCustomer= new clspFLBusinessCustomer();
                $vflCustomer->enterprise->idEnterprise=$_SESSION["idEnterprise"];
                $vflCustomer->idCustomer=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->customer->idCustomer;
                clspBLBusinessCustomer::queryToDataBase($vflCustomer, 1);
                $vcustomerName=$vflCustomer->businessName;
                $vcustomerName = $vcustomerName;
            }


            array_push($creditoFolio,$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->idProductSale);
            array_push($creditoCantidad,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2));
            array_push($creditoFecha,$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->productSaleDate);
            array_push($creditoCliente,$vcustomerName);

            $vcreditSaleAmountByCash+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
            //$vrptProductsSales->x=90;
            //$vrptProductsSales->printMultiLine(330, 15, $vcustomerName, 8, "", "L", 0);
            //$vrptProductsSales->x=420;
            //$vrptProductsSales->printMultiLine(80, 15, "Fecha de Venta:", 9, "B", "R", 0);
            //$vrptProductsSales->x=500;
            //$vrptProductsSales->printMultiLine(70, 15, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->productSale->productSaleDate, 8, "", "R", 0);
            $vsaleTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;
        }
        /*
        if( $vproductsSalesDetailsTotal>0 ){
            $vrptProductsSales->x=420;  $vrptProductsSales->y+=20;
            $vrptProductsSales->printMultiLine(80, 15, "Gran Total:", 9, "B", "R", 0);
            $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
            $vrptProductsSales->x=500;
            $vrptProductsSales->printMultiLine(70, 15, "$ " . number_format($vsaleTotal, 2, ".", ","), 8, "B", "R", 0);
        }
        */
        //$vdata = $vsaleTotal;

        unset($vsaleTotal);
    }

    unset($vproductsSalesDetails);


unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
      $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);





if (count($creditoFolio)>0) {
  $vdata = '<table id="tablaCredito">
    <tr>
      <th style="text-align:center;">Cliente</th>
      <th style="text-align:center;">Folio</th>
      <th style="text-align:center;">Fecha</th>
      <th style="text-align:center;">Cantidad</th>
    </tr>';

  for ($i=0; $i < count($creditoFolio); $i++) {
    $vdata.= '<tr>';
    $vdata.= '<td style="text-align:center;">';
    $vdata.= $creditoCliente[$i];
    $vdata.= '</td>';
    $vdata.= '<td style="text-align:center;">';
    $vdata.= $creditoFolio[$i];
    $vdata.= '</td>';
    $vdata.= '<td style="text-align:center;">';
    $vdata.= $creditoFecha[$i];
    $vdata.= '</td>';
    $vdata.= '<td style="text-align:center;">';
    $vdata.= $creditoCantidad[$i];
    $vdata.= '</td>';
    $vdata.='</tr>';

  }



  $vdata.='</table>';
}else{
  $vdata='<h3 style="text-align:center;">No hay créditos otorgados</h3>';
}

//Fin créditos otorgados



//Inicio cobranza
if (count($cobranzaFolio)>0) {
  $cobranzadata = '<table id="tablaCobranza">
    <tr>
      <th style="text-align:center;">Cliente</th>
      <th style="text-align:center;">Fecha</th>
      <th style="text-align:center;">Cantidad</th>
    </tr>';

  for ($i=0; $i < count($cobranzaFolio); $i++) {
    $cobranzadata.= '<tr>';
    $cobranzadata.= '<td style="text-align:center;">';
    $cobranzadata.= $cobranzaCliente[$i];
    $cobranzadata.= '</td>';
    $cobranzadata.= '<td style="text-align:center;">';
    $cobranzadata.= $cobranzaFecha[$i];
    $cobranzadata.= '</td>';
    $cobranzadata.= '<td style="text-align:center;">';
    $cobranzadata.= $cobranzaCantidad[$i];
    $cobranzadata.= '</td>';
    $cobranzadata.='</tr>';

  }



  $cobranzadata.='</table>';
}else{
  $cobranzadata='<h3 style="text-align:center;">Sin cobranza registrada</h3>';
}



//Fin cobranza



//Inicio Gastos




if (count($gastosFolio)>0) {
  $gastosdata = '<table id="tablaGastos">
    <tr>
      <th style="text-align:center;">Concepto</th>
      <th style="text-align:center;">Fecha</th>
      <th style="text-align:center;">Cantidad</th>
    </tr>';

  for ($i=0; $i < count($gastosFolio); $i++) {
    $gastosdata.= '<tr>';
    $gastosdata.= '<td style="text-align:center;">';
    $gastosdata.= $gastosConcepto[$i];
    $gastosdata.= '</td>';
    $gastosdata.= '<td style="text-align:center;">';
    $gastosdata.= $gastosFecha[$i];
    $gastosdata.= '</td>';
    $gastosdata.= '<td style="text-align:center;">';
    $gastosdata.= $gastosCantidad[$i];
    $gastosdata.= '</td>';
    $gastosdata.='</tr>';

  }

  $gastosdata.='</table>';
}else{
  $gastosdata='<h3 style="text-align:center;">Sin gastos</h3>';
}


//Fin Gastos

        if (!$vcashSaleAmountByCash) {
            $vcashSaleAmountByCash = 0;
        }


        
        
        $a = array(
        'contado' => number_format($vcashSaleAmountByCash,2),
        'credito' => number_format($vcreditSaleAmountByCash,2),
        'gastos' => number_format($vexpenseAmountByName,2),
        'pagos' => number_format($vpaymentAmountByCustomers,2),
        'saldoInicial' => number_format($vinitialCashByCash,2),
        'Total' => number_format($vcashSaleAmountByCash+$vinitialCashByCash+$vpaymentAmountByCustomers-$vexpenseAmountByName,2)
        );
        
        
           


        /*
        $a = array(
        'contado' => 1,
        'credito' => 1,
        'gastos' => 1,
        'pagos' => 1,
        'saldoInicial' =>1,
        'Total' => 1
        );
        */
        



        $json = json_encode($a);

        //$vresponse->alert($cobranzaCantidad);
        //$vresponse->alert($cobranzaFecha);
        //$vresponse->alert($cobranzaFolio);
        //$vresponse->alert($cobranzaCliente);

        $vresponse->assign("divCreditos", "innerHTML", $vdata);




        $vresponse->assign("divCobranza", "innerHTML", $cobranzadata);
        $vresponse->assign("divGastos", "innerHTML", $gastosdata);


        //$vresponse->setReturnValue($fechaInicio);
        //$vresponse->setReturnValue($fechaFinal);
        //$vresponse->setReturnValue($fechaInicial);
        $vresponse->setReturnValue($json);
        return $vresponse;
}

function showProductsDistributionData($vidProductDistribution)
{
    $ejecutar = false;
    $vresponse = new xajaxResponse();
    $vdata;



    unset($vidProductDistribution);
    return $vresponse;
}

function ProductsSalePaymentsList($vidProductSale,$timeStamp)
 {
    $vresponse= new xajaxResponse();
    $pagosTotal = 0;

    try{
        $vfilter ="WHERE p_productsalepayment.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_productsalepayment.id_productSale='" . $vidProductSale . "' ";
        $vfilter.="AND p_productsalepayment.CreatedOn>'" . $timeStamp . "'";
        $vproductsSalesPayments= new clscFLProductSalePayment();
        clscBLProductSalePayment::queryToDataBase($vproductsSalesPayments, $vfilter);
        $vproductsSalePaymentsTotal=clscBLProductSalePayment::total($vproductsSalesPayments);
        for ($vproductSalePaymentNumber=0; $vproductSalePaymentNumber<$vproductsSalePaymentsTotal; $vproductSalePaymentNumber++){

            $pagosTotal += $vproductsSalesPayments->productsSalesPayments[$vproductSalePaymentNumber]->amount;

        }

        unset($vfilter, $vproductsSalesPayments, $vproductsSalePaymentsTotal, $vproductSalePaymentNumber, $vdata);
    }
    catch (Exception $vexception){

    }

    unset($vidProductSale);
    return $pagosTotal;
 }



$vxajax->register(XAJAX_FUNCTION, "showCashList");
$vxajax->register(XAJAX_FUNCTION, "showCashiersList");
$vxajax->register(XAJAX_FUNCTION, "showCashTypesList");
$vxajax->register(XAJAX_FUNCTION, "showDocumentTypesList");
$vxajax->register(XAJAX_FUNCTION, "showCashCountData");
$vxajax->register(XAJAX_FUNCTION, "addCashCountData");
$vxajax->register(XAJAX_FUNCTION, "updateCashCountData");
$vxajax->register(XAJAX_FUNCTION, "addCashToCashCountCashList");
$vxajax->register(XAJAX_FUNCTION, "addDocumentToCashCountDocumentList");
$vxajax->register(XAJAX_FUNCTION, "deleteCashOfCashCountCashList");
$vxajax->register(XAJAX_FUNCTION, "deleteDocumentOfCashCountDocumentList");
$vxajax->register(XAJAX_FUNCTION, "cleanCashCountCashList");
$vxajax->register(XAJAX_FUNCTION, "cleanCashCountDocumentList");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->register(XAJAX_FUNCTION, "obtenerTotalCaja");
$vxajax->register(XAJAX_FUNCTION, "showProductsDistributionData");
$vxajax->register(XAJAX_FUNCTION, "ProductsSalePaymentsList");
$vxajax->processRequest();

?>
