<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
header('Content-Type: text/html; charset=utf-8');

date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotal=0;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-landscape.pdf";
    $voutFileName="distribucion-productos-registro";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsDistribution= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsDistribution->addPage();
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    
    showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    
    $vfilter ="WHERE p_productdistributiondetail.id_enterprise=" . $vflEnterpriseUser->enterprise->idEnterprise . " ";
    $vfilter.="AND p_productdistributiondetail.id_productDistribution='" . $_GET["vidProductDistribution"] . "'";
    $vproductsDistributionsDetails= new clscFLProductDistributionDetail();
    clscBLProductDistributionDetail::queryToDataBase($vproductsDistributionsDetails, $vfilter, 1);
    $vdistributionProductsTotal=clscBLProductDistributionDetail::total($vproductsDistributionsDetails);
    for ($vproductNumber=0; $vproductNumber<$vdistributionProductsTotal; $vproductNumber++){
        if( $vrptProductsDistribution->getY()>=690 ){
            $vrptProductsDistribution->addPage();
            //showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }
        if( ($vproductNumber==0) || ($vpageNew) ){
            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(110, 20, "Folio de Distribuci�n:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=160;
            $vrptProductsDistribution->printMultiLine(70, 20, $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->idProductDistribution, 9, "", "L", 0);
            $vrptProductsDistribution->x=300;
            $vrptProductsDistribution->printMultiLine(115, 20, "Fecha de Distribuci�n:", 10, "B", "R", 0);
            $vrptProductsDistribution->x=430;
            $vrptProductsDistribution->printMultiLine(60, 20, $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->productDistributionDate, 9, "", "R", 0);
            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=20;
            $vrptProductsDistribution->printMultiLine(35, 20, "Ruta:", 10, "B", "L", 0);
            $vrptProductsDistribution->x=85;
            $vrptProductsDistribution->printMultiLine(665, 20, $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->route->route, 9, "", "L", 0);
            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(60, 20, "C�digo" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=110;
            $vrptProductsDistribution->printMultiLine(200, 20, "Producto" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=310;
            $vrptProductsDistribution->printMultiLine(60, 20, "C. Pza." , 10, "B", "C", 1);
            $vrptProductsDistribution->x=370;
            $vrptProductsDistribution->printMultiLine(60, 20, "C. Kg." , 10, "B", "C", 1);
            $vrptProductsDistribution->x=430;
            $vrptProductsDistribution->printMultiLine(70, 20, "Precio Unit." , 10, "B", "C", 1);
            $vrptProductsDistribution->x=500;
            $vrptProductsDistribution->printMultiLine(80, 20, "Total" , 10, "B", "C", 1);
            $vpageNew=false;
        }
        $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=20;
        $vrptProductsDistribution->printMultiLine(60, 20, $vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->product->key , 8, "B", "C", 1);
        $vrptProductsDistribution->x=110;   
        $vrptProductsDistribution->printMultiLine(200, 20, strtoupper ($vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->product->name ), 8, "", "L", 1);
        $vrptProductsDistribution->x=310;
        $vdistributionAmount=$vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->distributionAmount;
        $vrptProductsDistribution->printMultiLine(60, 20, number_format($vdistributionAmount, 2, ".", ",") , 8, "B", "C", 1);
        $vrptProductsDistribution->x=370;
        $vdistributionWeightInkg=$vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->distributionWeightInkg;
        $vrptProductsDistribution->printMultiLine(60, 20, number_format($vdistributionAmount*$vdistributionWeightInkg, 2, ".", ",") , 8, "", "R", 1);
        $vrptProductsDistribution->x=430;
        $vdistributionPrice=$vproductsDistributionsDetails->productsDistributionsDetails[$vproductNumber]->distributionPrice;
        $vrptProductsDistribution->printMultiLine(70, 20, "$ " . number_format($vdistributionPrice, 2, ".", ",") , 8, "", "R", 1);
        $vrptProductsDistribution->x=500;
        $vtotal=$vdistributionAmount * $vdistributionPrice;
        $vrptProductsDistribution->printMultiLine(80, 20, "$ " . number_format($vtotal , 2, ".", ",") , 8, "", "R", 1);
        $vgTotal+=$vtotal;
        
        unset($vdistributionAmount, $vdistributionPrice, $vtotal);
    }
    if( $vdistributionProductsTotal>0 ){
        $vrptProductsDistribution->x=690;  $vrptProductsDistribution->y+=20;
        $vrptProductsDistribution->printMultiLine(80, 20, "$ " . number_format($vgTotal, 2, ".", ",") , 8, "B", "R", 1);



/*
        if ( strcmp($vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->observation, "")!=0 ){
            $vrptProductsDistribution->addPage();
            $vpageNew=true;
            $vrptProductsDistribution->x=50;  $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(90, 20, "Cobranza:" , 10, "B", "L", 0);

            $vrptProductsDistribution->x=50;   $vrptProductsDistribution->y+=30;
            $vrptProductsDistribution->printMultiLine(300, 20, "Cliente" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=350;
            $vrptProductsDistribution->printMultiLine(90, 20, "Folio" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=440;
            $vrptProductsDistribution->printMultiLine(90, 20, "Fecha" , 10, "B", "C", 1);
            $vrptProductsDistribution->x=530;
            $vrptProductsDistribution->printMultiLine(70, 20, "Monto" , 10, "B", "C", 1);




$json = $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->observation;


$json = utf8_encode($json);
$data = json_decode($json, true);


/*

    foreach($data as $dataArr){

        if( $vrptProductsDistribution->getY()>=600 ){
            $vrptProductsDistribution->addPage();
            //showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, ++$vpageNumber);
            $vpageNew=true;
        }

            $vrptProductsDistribution->y+=20;
            $vrptProductsDistribution->x=50;
            $cliente = utf8_decode($dataArr['cliente']);
            $vrptProductsDistribution->printMultiLine(300, 20, $cliente , 10, "B", "C", 1);
            $vrptProductsDistribution->x=350;
            $vrptProductsDistribution->printMultiLine(90, 20, $dataArr['folio'] , 10, "B", "C", 1);
            $vrptProductsDistribution->x=440;
            $vrptProductsDistribution->printMultiLine(90, 20,$dataArr['fecha'] , 10, "B", "C", 1);
            $vrptProductsDistribution->x=530;
            $vrptProductsDistribution->printMultiLine(70, 20, $dataArr['monto'] , 10, "B", "C", 1);


    }  
    */  
            
        //}

        



        $vrptProductsDistribution->x=50;  $vrptProductsDistribution->y+=40;
        $vrptProductsDistribution->printMultiLine(550, 15, "Repartidor" , 10, "B", "C", 0);
        $vrptProductsDistribution->y+=50;
        $vrptProductsDistribution->printLine(150, $vrptProductsDistribution->y, 500, $vrptProductsDistribution->y, 0.2);
        $vrptProductsDistribution->x=120;
        $vdealerName=$vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->dealer->name . " " .
                     $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->dealer->firstName . " " .
                     $vproductsDistributionsDetails->productsDistributionsDetails[0]->productDistribution->routeDealer->dealer->lastName;
        $vrptProductsDistribution->printMultiLine(400, 20, $vdealerName , 8, "", "C", 0);
        
        unset($vdealerName);
    }

    $vrptProductsDistribution->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vgTotal, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsDistribution, $vflEnterpriseUser,
          $vfilter, $vproductsDistributionsDetails, $vdistributionProductsTotal, $vproductNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de distribuci�n de productos";
}

function showPage($vrptProductsDistribution, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsDistribution, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsDistribution, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsDistribution, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsDistribution->x=50;	$vrptProductsDistribution->y=50;
        $vrptProductsDistribution->printMultiLine(500, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsDistribution->x=50;	$vrptProductsDistribution->y+=14;
        $vrptProductsDistribution->printMultiLine(500, 15, "\"Reporte de Distribuci�n de Productos (Registro)\"" , 10, "B", "C", 0);
        $vrptProductsDistribution->x=50;	$vrptProductsDistribution->y+=12;
        $vrptProductsDistribution->printMultiLine(540, 15, "Fecha de Impresi�n: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsDistribution, $vpageNumber)
 {
    try{
        $vrptProductsDistribution->x=50;  $vrptProductsDistribution->y=535;
        $vrptProductsDistribution->printMultiLine(700, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsDistribution->y=86;
        $vrptProductsDistribution->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>