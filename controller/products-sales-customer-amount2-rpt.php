<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessCustomer.php");
date_default_timezone_set('America/Mexico_City');

try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    $vgTotalSalePrice=0;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="ventas-productos-ruta-cantidad";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptProductsSales= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptProductsSales->addPage();
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
    

        $cliente;

    try{
        $vindividualsCustomers= new clscFLIndividualCustomer();
        $vfilter="WHERE c_individualcustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_individualcustomer.id_customer=" .(int)($_GET["vidRoute"]);
        clscBLIndividualCustomer::queryToDataBase($vindividualsCustomers, $vfilter);
        $vindividualsCustomersTotal=clscBLIndividualCustomer::total($vindividualsCustomers);

        if($vindividualsCustomersTotal>0){
         for ($vi=0; $vi<$vindividualsCustomersTotal; $vi++){

                $cliente = strtoupper($vindividualsCustomers->individualsCustomers[$vi]->name) . ' ' .
                                    strtoupper($vindividualsCustomers->individualsCustomers[$vi]->firstName) . ' ' .strtoupper(
                                    $vindividualsCustomers->individualsCustomers[$vi]->lastName) .'-'. strtoupper($vindividualsCustomers->individualsCustomers[$vi]->locality);
            

        }           
        }
        

        
        
        $vbusinessCustomers= new clscFLBusinessCustomer();
        $vfilter="WHERE c_businesscustomer.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND c_businesscustomer.id_customer=" .(int)($_GET["vidRoute"]);
        clscBLBusinessCustomer::queryToDataBase($vbusinessCustomers, $vfilter);
        $vbusinessCustomersTotal=clscBLBusinessCustomer::total($vbusinessCustomers);
        if($vbusinessCustomersTotal>0){
        for ($vi=0; $vi<$vbusinessCustomersTotal; $vi++){
            $cliente = $vbusinessCustomers->businessCustomers[$vi]->businessName;
        }            
        }

        
        //unset($vindividualsCustomers, $vfilter, $vindividualsCustomersTotal, $vJSON, $vi, $vbusinessCustomers, $vbusinessCustomersTotal);
    }
    catch (Exception $vexception){
    }
    /*$vfilter="WHERE c_route.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    if ( (int)($_GET["vidRoute"])!=0 ){
        $vfilter.="AND c_route.id_route=" . (int)($_GET["vidRoute"]);;
    }*/
    $vfilter="WHERE c_customer.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
    if ( (int)($_GET["vidRoute"])!=0 ){
        $vfilter.="AND c_customer.id_customer=" . (int)($_GET["vidRoute"]);
    }
    //$vroutes= new clscFLRoute();
    //clscBLRoute::queryToDataBase($vroutes, $vfilter);
    //$vroutesTotal=clscBLRoute::total($vroutes);
    //$vroutes= new clscFLRoute();
    /*$vroutes= new clscFLIndividualCustomer();
    //clscBLRoute::queryToDataBase($vroutes, $vfilter);
    clscBLIndividualCustomer::queryToDataBase($vroutes, $vfilter);
    //$vroutesTotal=clscBLRoute::total($vroutes);
    $vroutesTotal=clscBLIndividualCustomer::total($vroutes);

    if($vroutesTotal>0 || $vroutesTotal!=0){

    }*/
    $vroutesTotal=1;
    for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
        if( $vrptProductsSales->getY()>=640 ){
            $vrptProductsSales->addPage();
            showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
        }

         $vsaleAmountTotal = 0; 
        $vsaleWeightInKg = 0;

        $vrptProductsSales->x=50;
        $vrptProductsSales->y+=30;
        $vrptProductsSales->printMultiLine(500, 15,"VENTAS DE CONTADO ".$cliente, 9, "B", "L", 0);
        $vrptProductsSales->x=80;


        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        $vfilter.="AND p_productsale.id_operationType=1 ";
        $vfilter.="AND p_productsale.id_customer=".(int)($_GET["vidRoute"]). " ";
        //$vfilter.=" AND p_productsale.id_operationType=1";
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ) {
            $vsaleAmountTotal=0;
            $vsaleWeightInKg=0;
            $vprecioPromedio = 0;
            $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
            //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
            //$vrptProductsSales->x=80;
            //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
            $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
            for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){       
                if( $vrptProductsSales->getY()>=640 ){
                    $vrptProductsSales->addPage();
                    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                    $vpageNew=true;
                }

                if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                    $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                    $vrptProductsSales->x=100; 
                    $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                    $vrptProductsSales->x=400;
                    $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                    $vrptProductsSales->x=490;
                    $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                    $vpageNew=false;
                }
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                $vrptProductsSales->x=100;   
                //$vrptProductsSales->y+=20;
                $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                $vrptProductsSales->x=230;
                $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                $vrptProductsSales->x=300;
                $vrptProductsSales->printMultiLine(70, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 7, "", "R", 0);
                $vrptProductsSales->x=380;
                $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                $vrptProductsSales->x=480;
                $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);
                $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;


            }
            if( $vproductsSalesDetailsTotal>0 ){
                $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                $vrptProductsSales->printMultiLine(60, 15, "Gran Total:", 9, "B", "R", 0);
                $vrptProductsSales->x=480;
                $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


            }
        }


        $vsaleAmountTotal = 0; 
        $vsaleWeightInKg = 0;

        $vrptProductsSales->x=50;
        $vrptProductsSales->y+=30;
        $vrptProductsSales->printMultiLine(500, 15,"VENTAS A CREDITO ".$cliente, 9, "B", "L", 0);
        $vrptProductsSales->x=80;


        $vfilter ="WHERE p_productsaledetail.id_enterprise=" . $_SESSION["idEnterprise"] . " ";
        //$vfilter.="AND c_routecustomer.id_route= " . $vroutes->routes[$vrouteNumber]->idRoute . " ";
        $vfilter.="AND (p_productsale.fldproductSaleDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "')";
        $vfilter.="AND p_productsale.fldcanceled=0 ";
        $vfilter.="AND p_productsale.id_operationType=2 ";
        $vfilter.="AND p_productsale.id_customer=".(int)($_GET["vidRoute"]). " ";
        //$vfilter.=" AND p_productsale.id_operationType=1";
        $vproductsSalesDetails= new clscFLProductSaleDetail();
        if ( clscBLProductSaleDetail::queryProductsGroupByRouteToDataBase($vproductsSalesDetails, $vfilter) ) {
            $vsaleAmountTotal=0;
            $vsaleWeightInKg=0;
            $vprecioPromedio = 0;
            $vrptProductsSales->x=50;   $vrptProductsSales->y+=30;
            //$vrptProductsSales->printMultiLine(30, 15, "Ruta:" , 9, "B", "L", 0);
            //$vrptProductsSales->x=80;
            //$vrptProductsSales->printMultiLine(325, 15, $vroutes->routes[$vrouteNumber]->route, 8, "", "L", 0);
            $vproductsSalesDetailsTotal=clscBLProductSaleDetail::total($vproductsSalesDetails);
            for ($vproductsSalesDetailNumber=0; $vproductsSalesDetailNumber<$vproductsSalesDetailsTotal; $vproductsSalesDetailNumber++){       
                if( $vrptProductsSales->getY()>=640 ){
                    $vrptProductsSales->addPage();
                    showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, ++$vpageNumber);
                    $vpageNew=true;
                }

                if( ($vproductsSalesDetailNumber==0) || ($vpageNew) ){
                    $vrptProductsSales->x=50;   $vrptProductsSales->y+=15;
                    $vrptProductsSales->printMultiLine(50, 20, "Clave" , 8, "B", "C", 0);
                    $vrptProductsSales->x=100; 
                    $vrptProductsSales->printMultiLine(180, 20, "Producto" , 8, "B", "C", 0);
                    $vrptProductsSales->x=230;
                    $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Pz)" , 8, "B", "C", 0);
                    $vrptProductsSales->x=300;
                    $vrptProductsSales->printMultiLine(70, 20, "Cantidad (Kg)" , 8, "B", "C", 0);
                    $vrptProductsSales->x=400;
                    $vrptProductsSales->printMultiLine(90, 20, "Precio promedio ($)" , 8, "B", "C", 0);
                    $vrptProductsSales->x=490;
                    $vrptProductsSales->printMultiLine(70, 20, "Total ($)" , 8, "B", "C", 0);
                    $vpageNew=false;
                }
                $vrptProductsSales->x=50;   $vrptProductsSales->y+=20;
                $vrptProductsSales->printMultiLine(50, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->key , 7, "", "C", 0);
                $vrptProductsSales->x=100;   
                //$vrptProductsSales->y+=20;
                $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                $vrptProductsSales->printMultiLine(380, 20, strtoupper($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->product->name) , 7, "", "L", 0);
                $vrptProductsSales->x=230;
                $vrptProductsSales->printMultiLine(70, 20, number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount, 2, ".", ","), 7, "", "R", 0);
                $vrptProductsSales->x=300;
                $vrptProductsSales->printMultiLine(70, 20, $vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg, 7, "", "R", 0);
                $vrptProductsSales->x=380;
                $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice/$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount,2,".",","), 7, "", "R", 0);
                $vrptProductsSales->x=480;
                $vrptProductsSales->printMultiLine(70, 20,number_format($vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice,2,".",","), 7, "", "R", 0);
                $vsaleAmountTotal+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleAmount;
                $vsaleWeightInKg+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->saleWeightInKg;
                $vprecioPromedio+=$vproductsSalesDetails->productsSalesDetails[$vproductsSalesDetailNumber]->salePrice;


            }
            if( $vproductsSalesDetailsTotal>0 ){
                $vrptProductsSales->x=360;  $vrptProductsSales->y+=20;
                $vrptProductsSales->printLine(50, $vrptProductsSales->y, 570, $vrptProductsSales->y, 0.2);
                $vrptProductsSales->printMultiLine(60, 15, "Gran Total:", 9, "B", "R", 0);
                $vrptProductsSales->x=480;
                $vrptProductsSales->printMultiLine(70, 15, number_format($vprecioPromedio, 2, ".", ","), 8, "B", "R", 0);


            }
        }


            
            unset($vsaleAmountTotal, $vsaleWeightInKg);









        }
        
        unset($vproductsSalesDetails);
    //}
    $vrptProductsSales->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vgTotalSalePrice, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptProductsSales,
          $vflEnterpriseUser, $vfilter, $vroutes, $vroutesTotal, $vrouteNumber);
}
catch (Exception $vexception){
    //echo "Ocurrió un error al tratar de mostrar el reporte de ventas de productos (cantidades)";
    echo $vexception->getMessage();
}

function showPage($vrptProductsSales, $vflEnterpriseUser, $vdate, $vpageNumber)
 {
    try{
        showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate);
        showFooter($vrptProductsSales, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptProductsSales, $vflEnterpriseUser, $vdate)
 {
    try{
        $vrptProductsSales->x=50;	$vrptProductsSales->y=60;
        $vrptProductsSales->printMultiLine(520, 15, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Reporte de Ventas de Productos por Cantidades\"" , 10, "B", "C", 0);
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=14;
        $vrptProductsSales->printMultiLine(520, 15, "\"Periodo del ". trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]) . "\"" , 8, "B", "C", 0);
        
        $vrptProductsSales->x=50;	$vrptProductsSales->y+=12;
        $vrptProductsSales->printMultiLine(520, 15, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptProductsSales, $vpageNumber)
 {
    try{
        $vrptProductsSales->x=50;  $vrptProductsSales->y=715;
        $vrptProductsSales->printMultiLine(520, 20, "Página Número " . $vpageNumber , 6, "", "R", 0);
        $vrptProductsSales->y=105;
        $vrptProductsSales->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>