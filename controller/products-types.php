<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
date_default_timezone_set('America/Mexico_City');


function showProductsTypesList()
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter="WHERE c_producttype.id_enterprise=" . $_SESSION['idEnterprise'];
        $vproductTypes= new clscFLProductType();
        clscBLProductType::queryToDataBase($vproductTypes, $vfilter);
        $vproductTypesTotal=clscBLProductType::total($vproductTypes);
        for ($vproductTypeNumber=0; $vproductTypeNumber<$vproductTypesTotal; $vproductTypeNumber++){
            if ( $vproductTypeNumber==0 ){
                $vdata='<table id="vgrdproductsTypesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproductTypeName" style="text-align:center; font-weight:bold;">Tipo de Producto</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrproductType" onClick="showProductTypeData('. $vproductTypes->productTypes[$vproductTypeNumber]->idProductType . ');" title="Seleccionar Tipo de Producto" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vproductTypes->productTypes[$vproductTypeNumber]->productType . '</td>';
            $vdata.="</tr>";
        }
        if ( $vproductTypeNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vproductsTypesList", "innerHTML", $vdata);
            $vresponse->script("setProductTypesList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen tipos de productos registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vproductsTypesList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vproductTypes, $vproductTypesTotal, $vproductTypeNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los tipos de productos, intente de nuevo");
	}
	
	return $vresponse;
 }

function showProductTypeData($vidProductType)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflProductType= new clspFLProductType();
        $vflProductType->idProductType=$vidProductType;
        $vflProductType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLProductType::queryToDataBase($vflProductType)){
            case 0: $vresponse->alert("Los datos del tipo de producto no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtproductType", $vresponse);
                    $vtext->setValue($vflProductType->productType);
                    $vresponse->script("enableProductTypeButtons();");
                    
                    unset($vtext);
                    break;
        }
	   
       unset($vflProductType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del tipo de producto, intente de nuevo");
	}
    
    unset($vidProductType);
	return $vresponse;
 }

function addProductTypeData($vproductTypeForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
	    $vflProductType= new clspFLProductType();
        $vflProductType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductType->productType=trim($vproductTypeForm["txtproductType"]);
        switch(clspBLProductType::addToDataBase($vflProductType)){
            case 0:  $vresponse->alert("Imposible registrar el tipo de producto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidProductType=" . $vflProductType->idProductType);
                     $vresponse->script("showProductsTypesList(0);");
                     $vresponse->alert("Los datos del tipo de producto <" . $vflProductType->productType . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflProductType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el tipo de producto, intente de nuevo");
	}
	
    unset($vproductTypeForm);
	return $vresponse;
 }

function updateProductTypeData($vidProductType, $vproductTypeForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProductType= new clspFLProductType();
        $vflProductType->idProductType=$vidProductType;
        $vflProductType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProductType->productType=trim($vproductTypeForm["txtproductType"]);
        switch(clspBLProductType::updateInDataBase($vflProductType)){
            case 0: $vresponse->alert("Ningún dato se ha modificado del tipo de producto");
                    break;
            case 1: $vresponse->script("showProductsTypesList(1);");
                    $vresponse->alert("Los datos del tipo de producto han sido modificados correctamente");
                    break;
        }
        
        unset($vflProductType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del tipo de producto, intente de nuevo");
	}
	
    unset($vidProductType, $vproductTypeForm);
	return $vresponse;
 }

function deleteProductTypeData($vidProductType)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProductType= new clspFLProductType();
        $vflProductType->idProductType=$vidProductType;
        $vflProductType->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        switch(clspBLProductType::deleteInDataBase($vflProductType)){
            case 0:  $vresponse->alert("Imposible eliminar el tipo de producto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidProductType=0;");
                     $vresponse->script("cleanProductTypeFormFields();");
                     $vresponse->script("showProductsTypesList(0);");
                     $vresponse->alert("Los datos del tipo de producto han sido eliminados correctamente");
                     break;
        }
        
        unset($vflProductType);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del tipo producto, intente de nuevo");
	}
	
    unset($vidProductType);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProductsTypesList");
$vxajax->register(XAJAX_FUNCTION, "showProductTypeData");
$vxajax->register(XAJAX_FUNCTION, "addProductTypeData");
$vxajax->register(XAJAX_FUNCTION, "updateProductTypeData");
$vxajax->register(XAJAX_FUNCTION, "deleteProductTypeData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>