<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLState.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLState.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");
date_default_timezone_set('America/Mexico_City');


function showStatesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vstates= new clscFLState();
        clscBLState::queryToDataBase($vstates, "");
        $vstatesTotal=clscBLState::total($vstates);
	
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vstatesTotal; $vi++){
            $vJSON.=', {"text":"' . $vstates->states[$vi]->state .
                    '", "value":' . $vstates->states[$vi]->idState . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vstates, $vstatesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados, intente de nuevo");
	}

	return $vresponse;
 }

function showMunicipalitiesList($vidState)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vmunicipalities= new clscFLMunicipality();
        clscBLMunicipality::queryToDataBase($vmunicipalities, "WHERE c_municipality.id_state=" . $vidState);
        $vmunicipalitiesTotal=clscBLMunicipality::total($vmunicipalities);
	
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vmunicipalitiesTotal; $vi++){
            $vJSON.=', {"text":"' . $vmunicipalities->municipalities[$vi]->municipality .
                    '", "value":' . $vmunicipalities->municipalities[$vi]->idMunicipality . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vmunicipalities, $vmunicipalitiesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los estados, intente de nuevo");
	}
    
    unset($vidState);
	return $vresponse;
 }

function showProviderData($vidProvider)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflProviderData= new clspFLProvider();
		$vflProviderData->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProviderData->idProvider=$vidProvider;
        clspBLProvider::queryToDataBase($vflProviderData);
        if ( $vflProviderData->personType->idPersonType==1 ){
            $vflProvider= new clspFLIndividualProvider();
            $vflProvider->enterprise->idEnterprise=$vflProviderData->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflProviderData->idProvider;
            $vqueryStatus=clspBLIndividualProvider::queryToDataBase($vflProvider);
        }
        else{
            $vflProvider= new clspFLBusinessProvider();
            $vflProvider->enterprise->idEnterprise=$vflProviderData->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflProviderData->idProvider;
            $vqueryStatus=clspBLBusinessProvider::queryToDataBase($vflProvider);
        }
        switch($vqueryStatus){
            case 0: $vresponse->alert("Los datos del cliente no se encuentran registrados");
                    break;
            case 1: $vresponse->script('xajax.$("cmrindividual").disabled=true;
                                        xajax.$("cmrbusiness").disabled=true;');
                    if ( $vflProviderData->personType->idPersonType==1 ){
                        $vresponse->script('xajax.$("cmrindividual").checked=true;
                                            setPersonData();');
                        $vtext= new clspText("txtname", $vresponse);
                        $vtext->setValue($vflProvider->name);
                        $vtext= new clspText("txtfirstName", $vresponse);
                        $vtext->setValue($vflProvider->firstName);
                        $vtext= new clspText("txtlastName", $vresponse);
                        $vtext->setValue($vflProvider->lastName);
                    }
                    else{
                        $vresponse->script('xajax.$("cmrbusiness").checked=true;
                                            setPersonData();');
                        $vtext= new clspText("txtname", $vresponse);
                        $vtext->setValue($vflProvider->businessName);
                    }
                    $vtext= new clspText("txtrfc", $vresponse);
                    $vtext->setValue($vflProvider->rfc);
                    $vtext= new clspText("txthomoclave", $vresponse);
                    $vtext->setValue($vflProvider->homoclave);
                    $vresponse->script("vcmbStatesList.value(" . $vflProvider->municipality->state->idState . "); showMunicipalitiesList();
                                        vcmbMunicipalitiesList.value(" . $vflProvider->municipality->idMunicipality . ");");
                    $vtext= new clspText("txtlocality", $vresponse);
                    $vtext->setValue($vflProvider->locality);
                    $vtext= new clspText("txtstreet", $vresponse);
                    $vtext->setValue($vflProvider->street);
                    $vtext= new clspText("txtnumber", $vresponse);
                    $vtext->setValue($vflProvider->number);
                    $vtext= new clspText("txtpostCode", $vresponse);
                    $vtext->setValue($vflProvider->postCode);
                    $vtext= new clspText("txtphoneNumber", $vresponse);
                    $vtext->setValue($vflProvider->phoneNumber);
                    $vtext= new clspText("txtmovilNumber", $vresponse);
                    $vtext->setValue($vflProvider->movilNumber);
                    $vtext= new clspText("txtemail", $vresponse);
                    $vtext->setValue($vflProvider->email);
                    $vtext= new clspText("txtpageWeb", $vresponse);
                    $vtext->setValue($vflProvider->pageWeb);
                    $vstring= new clspString($vflProvider->observation);
				    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflProviderData, $vflProvider, $vqueryStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del proveedor, intente de nuevo");
	}
    
    unset($vidProvider);
	return $vresponse;
 }

function addProviderData($vpersonType, $vproviderKey, $vproviderForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        if ( $vpersonType==1 ){
            $vflProvider= new clspFLIndividualProvider();
            $vflProvider->name=trim($vproviderForm["txtname"]);
            $vflProvider->firstName=trim($vproviderForm["txtfirstName"]);
            $vflProvider->lastName=trim($vproviderForm["txtlastName"]);
            $vname= $vflProvider->name . ' ' . $vflProvider->firstName . ' ' . $vflProvider->lastName;          
        }
        else{
            $vflProvider= new clspFLBusinessProvider();
            $vflProvider->businessName=trim($vproviderForm["txtname"]);
            $vname= $vflProvider->businessName;   
        }
        $vflProvider->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProvider->personType->idPersonType=$vpersonType;
        $vflProvider->municipality->state->idState=(int)($vproviderForm["cmbstatesList"]);
        $vflProvider->municipality->idMunicipality=(int)($vproviderForm["cmbmunicipalitiesList"]);
        $vflProvider->key=trim($vproviderKey);
        $vflProvider->rfc=trim($vproviderForm["txtrfc"]);
        $vflProvider->homoclave=trim($vproviderForm["txthomoclave"]);
        $vflProvider->locality=trim($vproviderForm["txtlocality"]);
        $vflProvider->street=trim($vproviderForm["txtstreet"]);
        $vflProvider->number=trim($vproviderForm["txtnumber"]);
        $vflProvider->postCode=trim($vproviderForm["txtpostCode"]);
        $vflProvider->phoneNumber=trim($vproviderForm["txtphoneNumber"]);
        $vflProvider->movilNumber=trim($vproviderForm["txtmovilNumber"]);
        $vflProvider->email=trim($vproviderForm["txtemail"]);
        $vflProvider->pageWeb=trim($vproviderForm["txtpageWeb"]);
        $vflProvider->observation=trim($vproviderForm["txtobservation"]);
        if ( $vflProvider->personType->idPersonType==1 ){
            $vaddStatus=clspBLIndividualProvider::addToDataBase($vflProvider);
        }
        else{
            $vaddStatus=clspBLBusinessProvider::addToDataBase($vflProvider);
        }
        switch($vaddStatus){
            case -2: $vresponse->alert("Imposible registrar al proveedor, el número ya se encuentra registrado");
                     break;
            case -1: $vresponse->alert("Imposible registrar al proveedor, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible registrar al proveedor físico, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidProvider=" . $vflProvider->idProvider);
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Proveedor");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Proveedor");
                     $vresponse->script('xajax.$("cmrindividual").disabled=true;
                                         xajax.$("cmrbusiness").disabled=true;');
                     $vresponse->alert("Los datos del proveedor <" . $vname . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflProvider, $vname, $vaddStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar al proveedor, intente de nuevo");
	}
	
    unset($vpersonType, $vproviderKey, $vproviderForm);
	return $vresponse;
 }

function updateProviderData($vidProvider, $vproviderKey, $vproviderForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProviderData= new clspFLProvider();
		$vflProviderData->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProviderData->idProvider=$vidProvider;
        clspBLProvider::queryToDataBase($vflProviderData);
        if ( $vflProviderData->personType->idPersonType==1 ){
            $vflProvider= new clspFLIndividualProvider();
            $vflProvider->name=trim($vproviderForm["txtname"]);
            $vflProvider->firstName=trim($vproviderForm["txtfirstName"]);
            $vflProvider->lastName=trim($vproviderForm["txtlastName"]);            
        }
        else{
            $vflProvider= new clspFLBusinessProvider();
            $vflProvider->businessName=trim($vproviderForm["txtname"]);            
        }
        $vflProvider->enterprise->idEnterprise=$vflProviderData->enterprise->idEnterprise;
        $vflProvider->idProvider=$vflProviderData->idProvider;
        $vflProvider->personType->idPersonType=$vflProviderData->personType->idPersonType;
        $vflProvider->municipality->state->idState=(int)($vproviderForm["cmbstatesList"]);
        $vflProvider->municipality->idMunicipality=(int)($vproviderForm["cmbmunicipalitiesList"]);
        $vflProvider->key=trim($vproviderKey);
        $vflProvider->rfc=trim($vproviderForm["txtrfc"]);
        $vflProvider->homoclave=trim($vproviderForm["txthomoclave"]);
        $vflProvider->locality=trim($vproviderForm["txtlocality"]);
        $vflProvider->street=trim($vproviderForm["txtstreet"]);
        $vflProvider->number=trim($vproviderForm["txtnumber"]);
        $vflProvider->postCode=trim($vproviderForm["txtpostCode"]);
        $vflProvider->phoneNumber=trim($vproviderForm["txtphoneNumber"]);
        $vflProvider->movilNumber=trim($vproviderForm["txtmovilNumber"]);
        $vflProvider->email=trim($vproviderForm["txtemail"]);
        $vflProvider->pageWeb=trim($vproviderForm["txtpageWeb"]);
        $vflProvider->observation=trim($vproviderForm["txtobservation"]);
        if ( $vflProviderData->personType->idPersonType==1 ){
            $vupdateStatus=clspBLIndividualProvider::updateInDataBase($vflProvider);
        }
        else{
            $vupdateStatus=clspBLBusinessProvider::updateInDataBase($vflProvider);
        }
        switch($vupdateStatus){
            case -1: $vresponse->alert("Imposible modificar el número del proveedor, ya se encuentra registrado");
                     break;
            case 0:  $vresponse->alert("Ningún dato se ha modificado del proveedor");
                     break;
            case 1:  $vresponse->alert("Los datos del proveedor han sido modificados correctamente");
                     break;
        }
        
        unset($vflProviderData, $vflProvider, $vupdateStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del proveedor, intente de nuevo");
	}
	
    unset($vidProvider, $vproviderKey, $vproviderForm);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showStatesList");
$vxajax->register(XAJAX_FUNCTION, "showMunicipalitiesList");
$vxajax->register(XAJAX_FUNCTION, "showProviderData");
$vxajax->register(XAJAX_FUNCTION, "addProviderData");
$vxajax->register(XAJAX_FUNCTION, "updateProviderData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>