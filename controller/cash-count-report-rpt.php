<?php

require_once (dirname(dirname(__FILE__)) . "/controller/session.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspPDFReport.php");
date_default_timezone_set('America/Mexico_City');


try{
    date_default_timezone_set('America/Mexico_City');
    $vdate=date("m/d/Y h:i:s a");
    $vpageNumber=0;
    $vpageNew=false;
    
    $vsourceFile=dirname(dirname(__FILE__)) . "/reports/letter-portrait.pdf";
    $voutFileName="arqueos-cajas";
    $vorientation="P";
    $vpageSize="Letter";
    $vrptCashCount= new clspPDFReport($vsourceFile, $voutFileName, $vorientation, $vpageSize);
    $vrptCashCount->addPage();
    
    $vreportSubtitle="Del " . trim($_GET["vstartDate"]) . " al " . trim($_GET["vendDate"]);
    
    $vflEnterpriseUser= new clspFLEnterpriseUser();
    $vflEnterpriseUser->idUser=trim($_SESSION['idUser']);
    clspBLEnterpriseUser::queryToDataBase($vflEnterpriseUser, 1);
    showPage($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
    
    $vfilter ="WHERE p_cashcount.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
    $vfilter.="AND (p_cashcount.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
    $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
    if ( (int)($_GET["vcash"])!=0 ){
        $vfilter.="AND p_cashcount.id_cash=" . (int)($_GET["vcash"]) . " ";
    }
    $vcashCount= new clscFLCashCount();
    clscBLCashCount::queryGroupByCash($vcashCount, $vfilter);
    $vcashCountTotal=clscBLCashCount::total($vcashCount);
    for ($vcashCountNumber=0; $vcashCountNumber<$vcashCountTotal; $vcashCountNumber++){
        if( $vrptCashCount->getY()>=640 ){
            $vrptCashCount->addPage();
            showPage($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
            $vpageNew=true;
        }
        $vrptCashCount->x=50;   $vrptCashCount->y+=20;
        $vrptCashCount->printMultiLine(28, 20, "Caja:" , 9, "B", "L", 0);
        $vrptCashCount->x=78;
        $vrptCashCount->printMultiLine(492, 20, $vcashCount->cashCount[$vcashCountNumber]->cash->cash, 8, "", "L", 0);
        //------------------------------------------------------Efectivo----------------------------------------------------
        $vcashAmountTotal=0;
        $vfilter ="WHERE p_cashcountcash.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_cashcountcash.id_cash=" . $vcashCount->cashCount[$vcashCountNumber]->cash->idCash . " ";
        $vfilter.="AND (p_cashcount.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vcashCountCash= new clscFLCashCountCash();
        clscBLCashCountCash::queryCashGroupByValue($vcashCountCash, $vfilter);
        $vcashCountCashTotal=clscBLCashCountCash::total($vcashCountCash);
        for ($vcashCountCashNumber=0; $vcashCountCashNumber<$vcashCountCashTotal; $vcashCountCashNumber++){
            if( $vrptCashCount->getY()>=640 ){
                $vrptCashCount->addPage();
                showPage($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
                $vpageNew=true;
            }
            if( ($vcashCountCashNumber==0) || ($vpageNew) ){
                $vrptCashCount->x=150;   $vrptCashCount->y+=15;
                $vrptCashCount->printMultiLine(70, 15, "Concepto", 8, "B", "L", 1);
                $vrptCashCount->x=220;
                $vrptCashCount->printMultiLine(70, 15, "Valor", 8, "B", "C", 1);
                $vrptCashCount->x=290;
                $vrptCashCount->printMultiLine(80, 15, "Cantidad", 8, "B", "C", 1);
                $vrptCashCount->x=370;
                $vrptCashCount->printMultiLine(100, 15, "Total", 8, "B", "C", 1);
                $vpageNew=false;
            }
            $vrptCashCount->x=150;   $vrptCashCount->y+=15;
            $vrptCashCount->printMultiLine(70, 15, $vcashCountCash->cashCountCash[$vcashCountCashNumber]->cashType->cashType, 8, "", "L", 1);
            $vrptCashCount->x=220;
            $vrptCashCount->printMultiLine(70, 15, "$ " . number_format($vcashCountCash->cashCountCash[$vcashCountCashNumber]->value, 2, ".", ","), 8, "", "R", 1);
            $vrptCashCount->x=290;
            $vrptCashCount->printMultiLine(80, 15, number_format($vcashCountCash->cashCountCash[$vcashCountCashNumber]->amount, 2, ".", ","), 8, "", "R", 1);
            $vrptCashCount->x=370;
            $vcashAmount=$vcashCountCash->cashCountCash[$vcashCountCashNumber]->value*$vcashCountCash->cashCountCash[$vcashCountCashNumber]->amount;
            $vcashAmountTotal+=$vcashAmount;
            $vrptCashCount->printMultiLine(100, 15, "$ " . number_format($vcashAmount, 2, ".", ","), 8, "", "R", 1);
        }
        if( $vcashCountTotal>0 ){
            $vrptCashCount->x=150;  $vrptCashCount->y+=15;
            $vrptCashCount->printMultiLine(220, 15, "Subtotal:", 8, "B", "R", 1);
            $vrptCashCount->x=370;
            $vrptCashCount->printMultiLine(100, 15, "$" . number_format($vcashAmountTotal, 2, ".", ","), 8, "", "R", 1);
        }
        //------------------------------------------------------Documentos---------------------------------------------------
        $vdocumentAmountTotal=0;
        $vfilter ="WHERE p_cashcountdocument.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vfilter.="AND p_cashcountdocument.id_cash=" . $vcashCount->cashCount[$vcashCountNumber]->cash->idCash . " ";
        $vfilter.="AND (p_cashcount.fldrecordDate BETWEEN '" . date("Y-m-d", strtotime(trim($_GET["vstartDate"])))  . "' ";
        $vfilter.="AND '" . date("Y-m-d", strtotime(trim($_GET["vendDate"]))) . "') ";
        $vcashCountDocument= new clscFLCashCountDocument();
        clscBLCashCountDocument::queryDocumentGroupByValue($vcashCountDocument, $vfilter, 1);
        $vcashCountDocumentTotal=clscBLCashCountDocument::total($vcashCountDocument);
        for ($vcashCountDocumentNumber=0; $vcashCountDocumentNumber<$vcashCountDocumentTotal; $vcashCountDocumentNumber++){
            if( $vrptCashCount->getY()>=640 ){
                $vrptCashCount->addPage();
                showPage($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle, ++$vpageNumber);
                $vpageNew=true;
            }
            if( ($vcashCountDocumentNumber==0) || ($vpageNew) ){
                $vrptCashCount->x=150;   $vrptCashCount->y+=15;
                $vrptCashCount->printMultiLine(70, 15, "Concepto", 8, "B", "L", 1);
                $vrptCashCount->x=220;
                $vrptCashCount->printMultiLine(70, 15, "Valor", 8, "B", "C", 1);
                $vrptCashCount->x=290;
                $vrptCashCount->printMultiLine(80, 15, "Cantidad", 8, "B", "C", 1);
                $vrptCashCount->x=370;
                $vrptCashCount->printMultiLine(100, 15, "Total", 8, "B", "C", 1);
                $vpageNew=false;
            }
            $vrptCashCount->x=150;   $vrptCashCount->y+=15;
            $vrptCashCount->printMultiLine(70, 15, $vcashCountDocument->cashCountDocument[$vcashCountDocumentNumber]->documentType->documentType, 8, "", "L", 1);
            $vrptCashCount->x=220;
            $vdocumentAmount=$vcashCountDocument->cashCountDocument[$vcashCountDocumentNumber]->value;
            $vdocumentAmountTotal+=$vdocumentAmount;
            $vrptCashCount->printMultiLine(70, 15, "$ " . number_format($vdocumentAmount, 2, ".", ","), 8, "", "R", 1);
            $vrptCashCount->x=290;
            $vrptCashCount->printMultiLine(80, 15, number_format($vcashCountDocument->cashCountDocument[$vcashCountDocumentNumber]->property["total"], 2, ".", ","), 8, "", "R", 1);
            $vrptCashCount->x=370;
            $vrptCashCount->printMultiLine(100, 15, "$ " . number_format($vdocumentAmount, 2, ".", ","), 8, "", "R", 1);
        }
        if( $vcashCountTotal>0 ){
            $vrptCashCount->x=150;  $vrptCashCount->y+=15;
            $vrptCashCount->printMultiLine(220, 15, "Subtotal:", 8, "B", "R", 1);
            $vrptCashCount->x=370;
            $vrptCashCount->printMultiLine(100, 15, "$" . number_format($vdocumentAmountTotal, 2, ".", ","), 8, "", "R", 1);
        }
        $vrptCashCount->x=150;  $vrptCashCount->y+=15;
        $vrptCashCount->printMultiLine(220, 15, "Total:", 8, "B", "R", 1);
        $vrptCashCount->x=370;
        $vrptCashCount->printMultiLine(100, 15, "$" . number_format($vcashAmountTotal+$vdocumentAmountTotal, 2, ".", ","), 8, "", "R", 1);
        
        unset($vcashAmountTotal, $vcashCountCash, $vcashCountCashTotal, $vcashCountCashNumber, $vdocumentAmountTotal, $vcashCountDocument,
              $vcashCountDocumentTotal, $vcashCountDocumentNumber);
    }
    $vrptCashCount->showPage();
    
    unset($vdate, $vpageNumber, $vpageNew, $vsourceFile, $voutFileName, $vorientation, $vpageSize, $vrptCashCount, $vreportSubtitle,
          $vflEnterpriseUser, $vfilter, $vcashCount, $vcashCountTotal, $vcashCountNumber);
}
catch (Exception $vexception){
    echo "Ocurri� un error al tratar de mostrar el reporte de arqueos de cajas";
}

function showPage($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle, $vpageNumber)
 {
    try{
        showHeader($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle);
        showFooter($vrptCashCount, $vpageNumber);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }
 }

function showHeader($vrptCashCount, $vflEnterpriseUser, $vdate, $vreportSubtitle)
 {
    try{
        $vrptCashCount->x=50;	$vrptCashCount->y=60;
        $vrptCashCount->printMultiLine(520, 20, $vflEnterpriseUser->enterprise->enterprise , 14, "B", "C", 0);
        $vrptCashCount->x=50;	$vrptCashCount->y+=14;
        $vrptCashCount->printMultiLine(520, 20, "\"Reporte de Arqueos de Caja\"" , 10, "B", "C", 0);
        $vrptCashCount->x=50;	$vrptCashCount->y+=14;
        $vrptCashCount->printMultiLine(520, 20, $vreportSubtitle, 10, "B", "C", 0);
        $vrptCashCount->x=50;	$vrptCashCount->y+=12;
        $vrptCashCount->printMultiLine(520, 20, "Fecha: " . $vdate , 6, "", "R", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }
 
function showFooter($vrptCashCount, $vpageNumber)
 {
    try{
        $vrptCashCount->x=50;  $vrptCashCount->y=715;
        $vrptCashCount->printMultiLine(520, 20, "P�gina N�mero " . $vpageNumber , 6, "", "R", 0);
        $vrptCashCount->y=120;
        $vrptCashCount->printMultiLine(1, 20, "", 6, "", "c", 0);
    }
    catch (Exception $vexception){
        throw new Exception($vexception->getMessage(), $vexception->getCode());
    }					
 }

?>