<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

define("cimageUrl", "./uploads/images/products/");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");
date_default_timezone_set('America/Mexico_City');


function showProductTypesList()
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vfilter="WHERE c_producttype.id_enterprise=" . $_SESSION['idEnterprise'];
        $vproductTypes= new clscFLProductType();
        clscBLProductType::queryToDataBase($vproductTypes, $vfilter);
        $vproductTypesTotal=clscBLProductType::total($vproductTypes);
        
        $vJSON='[{"text":"--Seleccionar--", "value":"0"}';
        for ($vi=0; $vi<$vproductTypesTotal; $vi++){
            $vJSON.=', {"text":"' . $vproductTypes->productTypes[$vi]->productType .
                    '", "value":' . $vproductTypes->productTypes[$vi]->idProductType . '}';
        }
        $vJSON.="]";
        
        $vresponse->setReturnValue($vJSON);
		unset($vfilter, $vproductTypes, $vproductTypesTotal, $vJSON, $vi);
    }
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de listar los tipos de producto, intente de nuevo");
	}

	return $vresponse;
 }

function showProductData($vidProduct)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflProduct= new clspFLProduct();
		$vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->idProduct=$vidProduct;
        switch(clspBLProduct::queryToDataBase($vflProduct)){
            case 0: $vresponse->alert("Los datos del producto no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtkey", $vresponse);
                    $vtext->setValue($vflProduct->key);
                    $vtext= new clspText("txtbarCode", $vresponse);
                    $vtext->setValue($vflProduct->barCode);
                    $vresponse->script("vcmbProductTypesList.value(" . $vflProduct->productType->idProductType . ");");
                    $vtext= new clspText("txtname", $vresponse);
                    $vtext->setValue($vflProduct->name);
                    $vresponse->assign("vsalePrice", "innerHTML", "$ " . number_format($vflProduct->salePrice, 2, ".", ","));
                    $vtext= new clspText("txtpurchasePrice", $vresponse);
                    $vtext->setValue(number_format($vflProduct->purchasePrice, 2, ".", ","));
                    $vtext= new clspText("txtweightInKg", $vresponse);
                    $vtext->setValue(number_format($vflProduct->weightInKg, 2, ".", ","));
                    $vstring= new clspString($vflProduct->description);
				    $vtextArea= new clspTextArea("txtdescription", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    if( strcmp($vflProduct->image, "")!=0 ){
                        $vtext= new clspText("vimageName", $vresponse);
                        $vtext->setValue($vflProduct->image);
                        $vresponse->script("$('#vproductImage').attr('src', '" . cimageUrl . $vflProduct->image . "');");
                        $vresponse->script("vcmdUploadProductImage.enable(false);");
                        $vresponse->script("vcmdDeleteProductImage.enable(true);");
                    }
                    else{
                        $vresponse->script("vcmdUploadProductImage.enable(true);");
                        $vresponse->script("vcmdDeleteProductImage.enable(false);");
                    }
                    $vstring= new clspString($vflProduct->observation);
                    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflProduct);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos del producto, intente de nuevo");
	}
    
    unset($vidProduct);
	return $vresponse;
 }

function addProductData($vproductForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflProduct= new clspFLProduct();
        $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->productType->idProductType=(int)($vproductForm["cmbproductTypesList"]);
        $vflProduct->key=trim($vproductForm["txtkey"]);
        $vflProduct->barCode=trim($vproductForm["txtbarCode"]);
        $vflProduct->name=trim($vproductForm["txtname"]);
        $vflProduct->description=trim($vproductForm["txtdescription"]);
        $vflProduct->image=trim($vproductForm["vimageName"]);
        $vflProduct->purchasePrice=(float)(str_replace(",", "", trim($vproductForm["txtpurchasePrice"])));
        $vflProduct->weightInKg=(float)(str_replace(",", "", trim($vproductForm["txtweightInKg"])));
        $vflProduct->observation=trim($vproductForm["txtobservation"]);
        switch(clspBLProduct::addToDataBase($vflProduct)){
            case -2: $vresponse->alert("Imposible registrar el producto, el código de barra ya se encuentra registrada");
                     break;
            case -1: $vresponse->alert("Imposible registrar el producto, la clave ya se encuentra registrada");
                     break;
            case 0:  $vresponse->alert("Imposible registrar el producto, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidProduct=" . $vflProduct->idProduct);
                     $vresponse->assign("vpageTitle1", "innerHTML", "Modificación - Producto");
                     $vresponse->assign("vpageTitle2", "innerHTML", "Modificación - Producto");
                     $vresponse->alert("Los datos del producto <" . $vflProduct->name . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflProduct);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar el producto, intente de nuevo");
	}
	
    unset($vproductForm);
	return $vresponse;
 }

function updateProductData($vidProduct, $vproductForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProduct= new clspFLProduct();
        $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->idProduct=$vidProduct;
        $vflProduct->productType->idProductType=(int)($vproductForm["cmbproductTypesList"]);
        $vflProduct->key=trim($vproductForm["txtkey"]);
        $vflProduct->barCode=trim($vproductForm["txtbarCode"]);
        $vflProduct->name=trim($vproductForm["txtname"]);
        $vflProduct->description=trim($vproductForm["txtdescription"]);
        $vflProduct->image=trim($vproductForm["vimageName"]);
        $vflProduct->purchasePrice=(float)(str_replace(",", "", trim($vproductForm["txtpurchasePrice"])));
        $vflProduct->weightInKg=(float)(str_replace(",", "", trim($vproductForm["txtweightInKg"])));
        $vflProduct->observation=trim($vproductForm["txtobservation"]);
        switch(clspBLProduct::updateInDataBase($vflProduct)){
            case -2: $vresponse->alert("Imposible modificar el código de barras del producto, ya se encuentra registrado");
                     break;
            case -1: $vresponse->alert("Imposible modificar la clave del producto, ya se encuentra registrada");
                     break;
            case 0:  $vresponse->alert("Ningún dato se ha modificado del producto");
                     break;
            case 1:  $vresponse->alert("Los datos del producto han sido modificados correctamente");
                     break;
        }
        
        unset($vflProduct);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos del producto, intente de nuevo");
	}
	
    unset($vidProduct, $vproductForm);
	return $vresponse;
 }
/*
function isRegisteredProductByKey($vproductKey, $vidProduct)
 {
	$vresponse= new xajaxResponse();
	
    try{
        $vflProduct= new clspFLProduct();
        $vflProduct->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProduct->key=$vproductKey;
        if ( (clspBLProduct::queryToDataBaseByKey($vflProduct)==1) && ($vflProduct->idProduct!=$vidProduct) ){
            $vresponse->setReturnValue(true);
        }
        else{
            $vresponse->setReturnValue(false);
        }
        
		unset($vflProduct);
 	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de verificar el registro del producto por clave, intente de nuevo");
        $vresponse->setReturnValue(false);
	}
    
    unset($vproductKey);
	return $vresponse;
 }
*/
function deleteUploadImage($vfileName)
 {
	$vresponse= new xajaxResponse();
	
    try{
        if ( is_file(cimageUrl . $vfileName) ){
            if ( unlink(cimageUrl . $vfileName) ){
                $vtext= new clspText("vimageName", $vresponse);
                $vtext->setValue("");
                $vresponse->script("vcmdUploadProductImage.enable(true);");
                $vresponse->script("vcmdDeleteProductImage.enable(false);");
                $vresponse->script("$('#vproductImage').attr('src', './view/img/image.png');");
            }
            else{
                $vresponse->alert("Imposible eliminar la imagen cargada, intente de nuevo.");
            }
		}
        else{
		  $vtext= new clspText("vimageName", $vresponse);
		  $vtext->setValue("");
		  $vresponse->script("vcmdUpload.enable(true);");
		  $vresponse->script("vcmdRemove.enable(false);");
		  $vresponse->alert("Imposible eliminar la imagen cargada, no existe físicamente.");
        }
        
        unset($vtext);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar la imagen cargada, intente de nuevo");
	}
    
    unset($vfileName);
	return $vresponse;
 }
 
function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProductTypesList");
$vxajax->register(XAJAX_FUNCTION, "showProductData");
$vxajax->register(XAJAX_FUNCTION, "addProductData");
$vxajax->register(XAJAX_FUNCTION, "updateProductData");
//$vxajax->register(XAJAX_FUNCTION, "isRegisteredProductByKey");
$vxajax->register(XAJAX_FUNCTION, "deleteUploadImage");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>