<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLBusinessProvider.php");
date_default_timezone_set('America/Mexico_City');


function showProvidersList($vproviderKey, $vfilterForm, $vpersonType)
 {
	$vresponse= new xajaxResponse();
	
	try{
        if ( strcmp(trim($vproviderKey), "")!=0 ){
            $vfilter ="WHERE c_provider.fldkey LIKE '%" . trim($vproviderKey) . "%' ";
            $vfilter.="AND ";
        }
        else{
            $vfilter ="WHERE ";
        }
       
        if ( $vpersonType==1){
            $vfilter.="c_individualprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
            if ( strcmp(trim($vfilterForm["txtname"]), "")!=0 ){
                $vfilter.="AND c_individualprovider.fldname LIKE '%" . trim($vfilterForm["txtname"]) . "%' ";
            }
            if ( strcmp(trim($vfilterForm["txtfirstName"]), "")!=0 ){
                $vfilter.="AND c_individualprovider.fldfirstName LIKE '%" . trim($vfilterForm["txtfirstName"]) . "%' ";
            }
            if ( strcmp(trim($vfilterForm["txtlastName"]), "")!=0 ){
                $vfilter.="AND c_individualprovider.fldlastName LIKE '%" . trim($vfilterForm["txtlastName"]) . "%' ";
            }
            $vproviders= new clscFLIndividualProvider();
            clscBLIndividualProvider::queryToDataBase($vproviders, $vfilter);
            $vprovidersTotal=clscBLIndividualProvider::total($vproviders);
        }
        else{
            $vfilter.="c_businessprovider.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
            if ( strcmp(trim($vfilterForm["txtname"]), "")!=0 ){
                $vfilter.="AND c_businessprovider.fldbusinessName LIKE '%" . trim($vfilterForm["txtname"]) . "%' ";
            }
            $vproviders= new clscFLBusinessProvider();
            clscBLBusinessProvider::queryToDataBase($vproviders, $vfilter);
            $vprovidersTotal=clscBLBusinessProvider::total($vproviders);
        }
        
        for ($vproviderNumber=0; $vproviderNumber<$vprovidersTotal; $vproviderNumber++){
            if ( $vproviderNumber==0 ){
                $vdata='<table id="vgrdprovidersList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vproviderKey" style="text-align:center; font-weight:bold;">Número</th>
                                    <th data-field="vproviderName" style="text-align:center; font-weight:bold;">Proveedor</th>
                                    <th data-field="vproviderMunicipalityName" style="text-align:center;font-weight:bold;">Municipio</th>
                                    <th data-field="vproviderLocality" style="text-align:center;font-weight:bold;">Localidad</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            if ( $vpersonType== 1){
                $vidProvider=$vproviders->individualsProviders[$vproviderNumber]->idProvider;
                $vproviderKey=$vproviders->individualsProviders[$vproviderNumber]->key;
                $vproviderName=$vproviders->individualsProviders[$vproviderNumber]->name . ' ' .
                               $vproviders->individualsProviders[$vproviderNumber]->firstName . ' ' .
                               $vproviders->individualsProviders[$vproviderNumber]->lastName;
                $vmunicipalityName=$vproviders->individualsProviders[$vproviderNumber]->municipality->municipality;
                $vlocality=$vproviders->individualsProviders[$vproviderNumber]->locality;
            }
            else{
                $vidProvider=$vproviders->businessProviders[$vproviderNumber]->idProvider;
                $vproviderKey=$vproviders->businessProviders[$vproviderNumber]->key;
                $vproviderName=$vproviders->businessProviders[$vproviderNumber]->businessName;
                $vmunicipalityName=$vproviders->businessProviders[$vproviderNumber]->municipality->municipality;
                $vlocality=$vproviders->businessProviders[$vproviderNumber]->locality;
            }
            
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrprovider" onClick="setProviderId('. $vidProvider . ');" title="Seleccionar Proveedor" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vproviderKey . '</td>';
            $vdata.='	<td>' . $vproviderName . '</td>';
            $vdata.='	<td>' . $vmunicipalityName . '</td>';
            $vdata.='	<td>' . $vlocality . '</td>';
            $vdata.="</tr>";
            
            unset($vidProvider, $vproviderKey, $vproviderName, $vmunicipalityName, $vlocality);
        }
        if ( $vproviderNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vprovidersList", "innerHTML", $vdata);
            $vresponse->script("setProvidersList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen proveedores registrados.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vprovidersList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vproviders, $vprovidersTotal, $vproviderNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los proveedores, intente de nuevo" . $vexception->getMessage());
	}
	
    unset($vproviderKey, $vfilterForm, $vpersonType);
	return $vresponse;
 }

function deleteProviderData($vidProvider)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflProviderData= new clspFLProvider();
		$vflProviderData->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflProviderData->idProvider=$vidProvider;
        clspBLProvider::queryToDataBase($vflProviderData);
        if ( $vflProviderData->personType->idPersonType==1 ){
            $vflProvider= new clspFLIndividualProvider();
            $vflProvider->enterprise->idEnterprise=$vflProviderData->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflProviderData->idProvider;
            $vdeleteStatus=clspBLIndividualProvider::deleteInDataBase($vflProvider);           
        }
        else{
            $vflProvider= new clspFLBusinessProvider();
            $vflProvider->enterprise->idEnterprise=$vflProviderData->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflProviderData->idProvider;
            $vdeleteStatus=clspBLBusinessProvider::deleteInDataBase($vflProvider);           
        }
        switch($vdeleteStatus){
            case -1: $vresponse->alert("Imposible eliminar al proveedor, intente de nuevo");
                     break;
            case 0:  $vresponse->alert("Imposible eliminar al proveedor físico, intente de nuevo");
                     break;
            case 1:  $vresponse->script("showProvidersList();");
                     $vresponse->alert("Los datos del proveedor han sido eliminados correctamente");
                     break;
        }
        
        unset($vflProviderData, $vflProvider, $vdeleteStatus);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos del proveedor, intente de nuevo");
	}
	
    unset($vidProvider);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showProvidersList");
$vxajax->register(XAJAX_FUNCTION, "deleteProviderData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>