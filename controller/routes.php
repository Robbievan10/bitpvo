<?php

$vxajax_core = dirname(dirname(__FILE__)) . "/tools/xajax-0.6-beta1/xajax_core";
require_once($vxajax_core . "/xajax.inc.php");

$vxajax = new xajax();
$vxajax->configure("javascript URI", "tools/xajax-0.6-beta1");
$vxajax->configure("characterEncoding", "UTF-8");

require_once (dirname(dirname(__FILE__)) . "/controller/menu.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clscFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clscBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/fisic-layer/clspFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/business-layer/clspBLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspText.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspTextArea.php");
require_once (dirname(dirname(__FILE__)) . "/model/tools/clspString.php");
date_default_timezone_set('America/Mexico_City');


function showRoutesList()
 {
	$vresponse= new xajaxResponse();
	
	try{
	    $vfilter ="WHERE c_route.id_enterprise=" . $_SESSION['idEnterprise'] . " ";
        $vroutes= new clscFLRoute();
        clscBLRoute::queryToDataBase($vroutes, $vfilter);
        $vroutesTotal=clscBLRoute::total($vroutes);
        for ($vrouteNumber=0; $vrouteNumber<$vroutesTotal; $vrouteNumber++){
            if ( $vrouteNumber==0 ){
                $vdata='<table id="vgrdroutesList">
                            <thead>
                                <tr>
                                    <th data-field="vempty" style="text-align:center; font-weight:bold;">&nbsp;</th>
                                    <th data-field="vrouteName" style="text-align:center; font-weight:bold;">Ruta</th>
                                </tr>
                            </thead>
                            <tbody>';
            }
            $vdata.='<tr>
                        <td>
                            <input type="radio" name="cmrroute" onClick="showRouteData('. $vroutes->routes[$vrouteNumber]->idRoute . ');" title="Seleccionar Ruta" />
                            <span class="custom-radio"></span>
                        </td>';
            $vdata.='	<td>' . $vroutes->routes[$vrouteNumber]->route . '</td>';
            $vdata.="</tr>";
        }
        if ( $vrouteNumber>=1 ){
            $vdata.='</tbody>
                </table>';
            $vresponse->assign("vroutesList", "innerHTML", $vdata);
            $vresponse->script("setRoutesList();");
        }
        else{
            $vdata='<p>&nbsp;</p>
                    <p class="textCaption-4">No existen rutas registradas.<p>
                    <p>&nbsp;</p>';
            $vresponse->assign("vroutesList", "innerHTML", $vdata);
        }    
        unset($vfilter, $vroutes, $vroutesTotal, $vrouteNumber, $vdata);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar las rutas, intente de nuevo");
	}
	
	return $vresponse;
 }

function showRouteData($vidRoute)
 {
    $vresponse= new xajaxResponse();
	
    try{
        $vflRoute= new clspFLRoute();
		$vflRoute->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRoute->idRoute=$vidRoute;
        switch(clspBLRoute::queryToDataBase($vflRoute)){
            case 0: $vresponse->alert("Los datos de la ruta no se encuentran registrados");
                    break;
            case 1: $vtext= new clspText("txtname", $vresponse);
                    $vtext->setValue($vflRoute->route);
                    $vstring= new clspString($vflRoute->description);
				    $vtextArea= new clspTextArea("txtdescription", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    $vstring= new clspString($vflRoute->observation);
                    $vtextArea= new clspTextArea("txtobservation", $vresponse);
				    $vtextArea->setValue($vstring->getFilteredString());
                    
                    $vresponse->script("enableRouteButtons();");
                    
                    unset($vtext, $vstring, $vtextArea);
                    break;
        }
	   
       unset($vflRoute);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de mostrar los datos de la ruta, intente de nuevo");
	}
    
    unset($vidRoute);
	return $vresponse;
 }

function addRouteData($vrouteForm)
 {
	$vresponse= new xajaxResponse();
	   
	try{
        $vflRoute= new clspFLRoute();
        $vflRoute->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRoute->route=trim($vrouteForm["txtname"]);
        $vflRoute->description=trim($vrouteForm["txtdescription"]);
        $vflRoute->observation=trim($vrouteForm["txtobservation"]);
        switch(clspBLRoute::addToDataBase($vflRoute)){
            case 0:  $vresponse->alert("Imposible registrar la ruta, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidRoute=" . $vflRoute->idRoute);
                     $vresponse->script("showRoutesList(0);");
                     $vresponse->alert("Los datos de la ruta <" . $vflRoute->route . "> han sido registrados correctamente");
                     break;
        }
        
        unset($vflRoute);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de registrar la ruta, intente de nuevo");
	}
	
    unset($vrouteForm);
	return $vresponse;
 }

function updateRouteData($vidRoute, $vrouteForm)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflRoute= new clspFLRoute();
        $vflRoute->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRoute->idRoute=$vidRoute;
        $vflRoute->route=trim($vrouteForm["txtname"]);
        $vflRoute->description=trim($vrouteForm["txtdescription"]);
        $vflRoute->observation=trim($vrouteForm["txtobservation"]);
        switch(clspBLRoute::updateInDataBase($vflRoute)){
            case 0: $vresponse->alert("Ningún dato se ha modificado de la ruta");
                    break;
            case 1: $vresponse->script("showRoutesList(1);");
                    $vresponse->alert("Los datos de la ruta han sido modificados correctamente");
                    break;
        }
        
        unset($vflRoute);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de modificar los datos de la ruta, intente de nuevo");
	}
	
    unset($vidRoute, $vrouteForm);
	return $vresponse;
 }

function deleteRouteData($vidRoute)
 {
	$vresponse= new xajaxResponse();
	
	try{
        $vflRoute= new clspFLRoute();
		$vflRoute->enterprise->idEnterprise=$_SESSION['idEnterprise'];
        $vflRoute->idRoute=$vidRoute;
        switch(clspBLRoute::deleteInDataBase($vflRoute)){
            case 0:  $vresponse->alert("Imposible eliminar la ruta, intente de nuevo");
                     break;
            case 1:  $vresponse->script("vidRoute=0;");
                     $vresponse->script("cleanRouteFormFields();");
                     $vresponse->script("showRoutesList(0);");
                     $vresponse->alert("Los datos de la ruta han sido eliminados correctamente");
                     break;
        }
        
        unset($vflRoute);
	}
	catch (Exception $vexception){
		$vresponse->alert("Ocurrió un error al tratar de eliminar los datos de la ruta, intente de nuevo");
	}
	
    unset($vidRoute);
	return $vresponse;
 }

function exit_()
 {
	$vresponse= new xajaxResponse();
	
	session_destroy();
	$vresponse->redirect("./");
	
	return $vresponse;
 }


$vxajax->register(XAJAX_FUNCTION, "showRoutesList");
$vxajax->register(XAJAX_FUNCTION, "showRouteData");
$vxajax->register(XAJAX_FUNCTION, "addRouteData");
$vxajax->register(XAJAX_FUNCTION, "updateRouteData");
$vxajax->register(XAJAX_FUNCTION, "deleteRouteData");
$vxajax->register(XAJAX_FUNCTION, "exit_");
$vxajax->processRequest();

?>