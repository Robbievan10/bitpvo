<?php
	require_once("controller/session.php");
	require_once("controller/products-distribution-record.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        	
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
        
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>            
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body class="overflow-hidden" onLoad="_default();">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Movimientos</li>
						<li>Distribución de Productos</li>
						<li class="active">Registros</li>
					</ul>
				</div><!-- /breadcrumb-->
				<div class="padding-md">
					<div class="row">
						<div class="col-md-6 form-group">
							<div id="vwndwFindProductsDistributions">
								<div id="vfindProductsDistributionsData"></div>
							</div>
							<button type="button" id="cmdfindProductsDistributions" class="k-button" title="Buscar Distribuiciones de Productos">
								<span class="k-icon k-i-search"></span>&nbsp;Buscar
							</button>
							<button type="button" id="cmdprintProductsDistribution" class="k-button" title="Imprimir Distribución de Productos">
								<span class="fa fa-print fa-lg"></span>&nbsp;Imprimir
							</button>
						</div>
						<div style="text-align:right" class="col-md-6 form-group">
							<button type="button" id="cmdnewSaveProductsDistribution" class="k-button" title="Nueva Distribución de Productos">
								<span class="k-icon k-i-plus"></span>&nbsp;Nuevo
							</button>
							<button type="button" id="cmdupdateProductsDistribution" class="k-button" title="Modificar Distribución de Productos">
								<span class="k-icon k-i-pencil"></span>&nbsp;Modificar
							</button>
							<button type="button" id="cmddeleteProductsDistribution" class="k-button" title="Eliminar Distribución de Productos">
								<span class="k-icon k-i-close"></span>&nbsp;Eliminar
							</button>
							<button type="button" id="cmdcancelProductsDistribution" class="k-button" title="Cancelar Distribución de Productos">
								<span class="k-icon k-i-cancel"></span>&nbsp;Cancelar
							</button>
						</div>
					</div>
					<div class="panel panel-default">
						<div style="text-align:right" class="panel-heading">
							<h2><span id="vproductsDistributionTotalPrice" class="text-primary" hidden="true">$ 0.00</span></h2>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<form id="vproductsDistributionData" name="vproductsDistributionData" method="post" onSubmit="return false;">
									<div class="row">
										<div class="col-md-6 form-group">
											<span class="textCaption-2">Folio:</span>
											<div id="vproductsDistributionFolio" class="inline">&nbsp;</div>
										</div>
										<div style="text-align:right" class="col-md-6 form-group">
											<label for="dtpckrproductsDistributionDate">
												<span class="textCaption-2">Fecha:</span>
											</label>
											<input id="dtpckrproductsDistributionDate" name="dtpckrproductsDistributionDate" maxlength="10" style="width:37%;" disabled="disabled" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 form-group">
											<label for="cmbproductsDistributionRoute">
												<span class="textCaption-2">Ruta:</span>
											</label>
											<select id="cmbproductsDistributionRoute" name="cmbproductsDistributionRoute" style="width:95.3%" disabled="disabled"></select>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<span class="textCaption-2">Repartidor:</span>
											<div id="vrouteDealerName" style="width:100%" class="inline">&nbsp;</div>
										</div>
									</div>
									</form>
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->
					<div class="panel panel-default">
						<div class="panel-heading textCaption-1"><span class="text-info">Productos</span></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<div class="row">
										<div style="text-align:right" class="col-md-12 form-group">
											<div id="vwndwAddProduct">
												<div id="vaddProductData"></div>
											</div>
											
											<button type="button" id="cmdaddProduct" class="k-button" title="Agregar Producto">
												<span class="k-icon k-i-plus"></span>&nbsp;Agregar
											</button>
											<button type="button" id="cmddeleteProduct" class="k-button"  title="Eliminar Producto">
												<span class="k-icon k-i-close"></span>&nbsp;Eliminar
											</button>
										</div>
									</div>
									<div class="row">
										<div id="vproductsDistributionList" class="col-md-12">&nbsp;</div>
									</div>
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->
					<div class="panel panel-default" hidden>
						<div class="panel-heading textCaption-1"><span class="text-info">Otros</span></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<form id="vproductsDistributionData1" name="vproductsDistributionData1" method="post" onSubmit="return false;">
									<div class="row">
										<div class="col-md-12 form-group">
											<label for="txtobservation">
												<span class="textCaption-2">Observaciones:</span>
											</label>
											<textarea id="txtobservation" name="txtobservation" class="form-control" rows="3" disabled="disabled"></textarea>
										</div>
									</div>
									</form>                                          
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->

					<div class="panel panel-default">




						<div class="panel-heading textCaption-1"><span class="text-info">Cobranza</span>



						</div>

						<div class="panel-body">
						
							<div id="divCobranza" class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">

                                   
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div>



				</div><!-- /.padding-md -->
			</div><!-- /main-container -->

		</div>
		<!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
		
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/js/cultures/kendo.culture.es-MX.min.js"></script>
		<!-- DatePicker -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.calendar.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.datepicker.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Grid -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.columnsorter.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.pager.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.grid.js"></script>
		<!-- Window -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.window.js"></script>
        
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/products-distribution-record.js"></script>
        
        <?php $vxajax->printJavascript(); ?>

		<script>
			$(document).ready(function(){
				console.log('add js');
				$("#txtproductDistributionAmount").keypress(function(){
					console.log('keypress agregar');
				});
			});
		</script>
        
	</body>
</html>