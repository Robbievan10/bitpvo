<?php
    require_once("controller/session.php");
    require_once("controller/routes-chart.php");
?>
<script src="./view/js/highcharts.js"></script>
<script src="./view/js/exporting.js"></script>
<script type="text/javascript" src="./view/js/routes-chart.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div id="container" style="min-width: 310px; height: 50%; margin: 0 auto"></div>
<div id="container2" style="min-width: 310px; height: 50%; margin: 0 auto"></div>

        <!-- Bootstrap -->
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="./tools/bootstrap/js/bootstrap.min.js"></script>

        <!-- Endless -->
        <script src='./view/js/endless/modernizr.min.js'></script>
        <script src='./view/js/endless/pace.min.js'></script>
        <script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
        <script src='./view/js/endless/jquery.slimscroll.min.js'></script>
        <script src='./view/js/endless/jquery_cookie.min.js'></script>
        <script src="./view/js/endless/endless/endless.js"></script>

        <!-- Personalizado -->
        <script type="text/javascript" src="./view/js/menu.js"></script>
        <script type="text/javascript" src="./view/js/index.js"></script>
        <script type ="text/javascript">
        $( document ).ready(function() {
            initialize(1);
        });
        </script>
        <?php $vxajax->printJavascript(); ?>
