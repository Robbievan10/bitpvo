<?php
	require_once("controller/session.php");
	require_once("controller/routes-assign.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
        
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
  		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>          
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body class="overflow-hidden" onLoad="_default(<?php echo $_POST["vidRoute"] ?>);">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Catálogos</li>
						<li><a href="./frmroutes.php" title="Ir a Catálogo de Rutas">Rutas</a></li>
						<li class="active">Asignación de Ruta</li>                        
					</ul>
				</div><!-- /breadcrumb-->
				<div class="padding-md">
					<div class="panel panel-default">
						<div class="panel-heading textCaption-1"><span class="text-info">Repartidor</span></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-12 form-group">
											<span class="textCaption-2">Ruta:</span>
											<div id="vrouteName" class="inline">&nbsp;</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-9 form-group">
											<label for="cmbdeliverersList">
												<span class="textCaption-2">Repartidor:</span>
											</label>
											<select id="cmbdeliverersList" name="cmbdeliverersList" style="width:87.8%" ></select>
										</div>
										<div  style="text-align:right" class="col-md-3 form-group">
											<button type="button" id="cmdassignDealerToRoute" class="k-button" title="Asignar Repartidor a la Ruta">
												<span class="k-icon k-i-tick"></span>&nbsp;Asignar
											</button>
											<button type="button" id="cmddeallocateDealerToRoute" class="k-button" title="Desasignar Repartidor a la Ruta">
												<span class="k-icon k-i-close"></span>&nbsp;Desasignar
											</button>
										</div>
									</div>
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->
					<div class="panel panel-default">
						<div class="panel-heading textCaption-1"><span class="text-info">Clientes</span></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-6 form-group">
											<div id="vwndwAssignVisitDayToCustomer">
												<div id="vassignVisitDayToCustomerData"></div>
											</div>
											<button type="button" id="cmdassignVisitDayToCustomer" class="k-button" title="Asignar Días de Visita al Cliente">
												<span class="k-icon k-i-group"></span>&nbsp;Asignar
											</button>
										</div>
										<div style="text-align:right" class="col-md-6 form-group">
											<div id="vwndwaddCustomerToRoute">
												<div id="vaddCustomerToRouteData"></div>
											</div>
											<button type="button" id="cmdaddCustomerToRoute" class="k-button" title="Agregar Cliente a la Ruta">
												<span class="k-icon k-i-plus"></span>&nbsp;Agregar
											</button>
											<button type="button" id="cmddeleteCustomerToRoute" class="k-button" title="Eliminar Cliente a la Ruta">
												<span class="k-icon k-i-close"></span>&nbsp;Eliminar
											</button>
										</div>
									</div>
									<div class="row">
										<div id="vrouteCustomersList" class="col-md-12 form-group">&nbsp;</div>
									</div>
									<div class="row">
										<div style="text-align:right" class="col-md-12">
											<button type="button" id="cmdbackToRoutes" class="k-button" title="Regresar a Catálogo de Rutas">
												<span class="k-icon k-i-arrowhead-w"></span>&nbsp;Regresar
											</button>
										</div>
									</div>
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->
				</div><!-- /.padding-md -->
			</div><!-- /main-container -->

		</div>
		<!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
		
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.view.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Grid -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.columnsorter.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.pager.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.grid.js"></script>
		<!-- Window -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.window.js"></script>
        
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/routes-assign.js"></script>
        
		<?php $vxajax->printJavascript(); ?>
        
	</body>
</html>