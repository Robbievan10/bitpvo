<?php
	require_once("controller/session.php");
	require_once("controller/cash-count-record.php");



?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">

		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />

		<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>


	</head>
	<body class="overflow-hidden" onLoad="_default(<?php echo $_POST["vidCash"] . ', ' . $_POST["vidCashCount"] ?>);">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Registros</li>
						<li><a href="./frmcash-count.php" title="Ir a Registro de Arqueos de Caja">Arqueos de Caja</a></li>
						<li class="active"><span id="vpageTitle1">Registro</span></li>
					</ul>
				</div><!-- /breadcrumb-->
				<div class="padding-md">
					<div class="panel panel-default">
						<div class="panel-heading textCaption-1">
							<span id="vpageTitle2" class="text-info">Registro</span>
						</div>
						<div class="panel-heading textCaption-1">
							<h5 id="totalInicial">Saldo inicial : ---</h5>
							<h5 id="totalPagos">Cobranza : ---</h5>
							<h5 id="totalGastos">Gastos : ---</h5>
							<h5 id="totalContado">Contado : ---</h5>
							<h5 id="totalCredito">Crédito : ---</h5>
							<h5 id="totalCaja">Total : ---</h5>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<form id="vcashCountData" name="vcashCountData" method="post" onSubmit="return false;">
									<div class="panel-group" id="pnbrcashCount" role="tablist" aria-multiselectable="true">
										<div class="panel panel-default">
											<div class="panel-heading textCaption-1" role="tab" id="vheadingCashCountGeneralData" class="active">
												<span class="text-info">Datos Generales</span>
											</div>
											<div></div>
											<div id="vcashCountGeneralData" role="tabpanel" aria-labelledby="vheadingCashCountGeneralData">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-10 form-group">
															<label for="cmbcashList">
																<span class="textCaption-2">Caja:</span>
															</label>
															<select id="cmbcashList" name="cmbcashList" style="width:100%" ></select>
														</div>
														<div class="col-md-2 form-group">
															<label for="dtpckrrecordDate">
																<span class="textCaption-2">Fecha:</span>
															</label>
															<input id="dtpckrrecordDate" name="dtpckrrecordDate" maxlength="10" style="width:100%;" />
														</div>
													</div>
													<div class="row">
														<div class="col-md-12 form-group">
															<label for="cmbcashiersList">
																<span class="textCaption-2">Cajero:</span>
															</label>
															<select id="cmbcashiersList" name="cmbcashiersList" style="width:100%" ></select>
														</div>
													</div>
												</div>
                                        	</div>
										</div>








										<div class="panel panel-default">
											<div class="panel-heading textCaption-1" role="tab" id="vheadingCashListData">
												<h4 class="panel-title">
														<span class="text-info textCaption-1">Cobranza</span>
								        		</h4>
                                        	</div>
											<div id="divCobranza" class="panel panel-default" role="tabpanel" aria-labelledby="vheadingCashListData">
												<div class="panel-body">
													<div class="row">
														<div style="text-align:right" class="col-md-12 form-group">

																<div id="divCobranza">
																</div>
														</div>
													</div>
												</div>
                                        	</div>
										</div>






										<div class="panel panel-default">
											<div class="panel-heading textCaption-1" role="tab" id="vheadingCashListData">
												<h4 class="panel-title">
														<span class="text-info textCaption-1">Créditos otorgados</span>
								        		</h4>
                                        	</div>
											<div id="divCreditos" class="panel panel-default" role="tabpanel" aria-labelledby="vheadingCashListData">
												<div class="panel-body">
													<div class="row">
														<div style="text-align:right" class="col-md-12 form-group">

																<div id="divCreditos">
																</div>
														</div>
													</div>
												</div>
                                        	</div>
										</div>



										<div class="panel panel-default">
											<div class="panel-heading textCaption-1" role="tab" id="vheadingCashListData">
												<h4 class="panel-title">
														<span class="text-info textCaption-1">Gastos</span>
								        		</h4>
                                        	</div>
											<div id="divGastos" class="panel panel-default" role="tabpanel" aria-labelledby="vheadingCashListData">
												<div class="panel-body">
													<div class="row">
														<div style="text-align:right" class="col-md-12 form-group">

																<div id="divGastos">
																</div>
														</div>
													</div>
												</div>
                                        	</div>
										</div>



										<div class="panel panel-default">
											<div class="panel-heading textCaption-1" role="tab" id="vheadingCashListData">
												<h4 class="panel-title">
													<a class="collapsed" data-toggle="collapse" data-parent="#pnbrcashCount" href="#vcashListData" aria-expanded="false" aria-controls="vcashCountCashListData">
														<span class="text-info textCaption-1">Efectivo</span>
													</a>
								        		</h4>
                                        	</div>
											<div id="vcashListData" class="panel panel-default" role="tabpanel" aria-labelledby="vheadingCashListData">
												<div class="panel-body">
													<div class="row">
														<div style="text-align:right" class="col-md-12 form-group">
															<div id="vwndwAddCash">
																<div id="vaddCashData">



																</div>
															</div>

															<button type="button" id="cmddeleteCash" class="k-button"  title="Eliminar Efectivo">
																<span class="k-icon k-i-close"></span>&nbsp;Eliminar
															</button>



														</div>
													</div>






													<div class="row">
														<div id="vcashCountCashList" class="col-md-12">&nbsp;</div>
													</div>
												</div>
                                        	</div>
										</div>
										<div class="panel panel-default">
											<div class="panel-heading textCaption-1" role="tab" id="vheadingDocumentListData">
												<h4 class="panel-title">
													<a class="collapsed" data-toggle="collapse" data-parent="#pnbrcashCount" href="#vdocumentListData" aria-expanded="false" aria-controls="vcashCountDocumentListData" title="Clic para Ocultar/Desocultar Efectivo">
														<span class="text-info textCaption-1">Documentos</span>
													</a>
								        		</h4>
                                        	</div>
											<div id="vdocumentListData" class="panel panel-default" role="tabpanel" aria-labelledby="vheadingDocumentListData">
												<div class="panel-body">
													<div class="row">
														<div style="text-align:right" class="col-md-12 form-group">
															<div id="vwndwAddDocument">
																<div id="vaddDocumentData"></div>
															</div>

															<button type="button" id="cmddeleteDocument" class="k-button"  title="Eliminar Documento">
																<span class="k-icon k-i-close"></span>&nbsp;Eliminar
															</button>
														</div>
													</div>
													<div class="row">
														<div id="vcashCountDocumentList" class="col-md-12">&nbsp;</div>
													</div>
												</div>
											</div>
										</div>
    <h2 id="textoTotalDocumentos">Total documentos : $0.00 </h2>


										<div class="panel panel-default" hidden>
											<div class="panel-heading textCaption-1" role="tab" id="vheadingCashCountOthersData">
												<h4 class="panel-title">
													<a class="collapsed" data-toggle="collapse" data-parent="#pnbrcashCount" href="#vcashCountOthersData" aria-expanded="false" aria-controls="vcashCountOthersData1" title="Clic para Ocultar/Desocultar Otros">
														<span class="text-info textCaption-1">Otros</span>
													</a>
								        		</h4>
											</div>
                                            <div id="vcashCountOthersData" class="panel panel-default" role="tabpanel" aria-labelledby="vheadingCashCountOthersData" hidden>
												<div class="panel-body">
													<div class="row">
														<div class="col-md-12">
															<label for="txtobservation">
																<span class="textCaption-2">Observaciones:</span>
															</label>
															<textarea id="txtobservation" name="txtobservation" class="form-control" rows="2"></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									</form>
									    <h2 id="textoTotalGlobal">Total : $0.00 </h2>
                                        <h2 id="textoDiferencia">Diferencia : $0.00 </h2>
                                    <div class="row">
										<div class="col-md-6">
											<button type="button" id="cmdbackToCashCount" class="k-button" title="Regresar a Arqueos de Caja">
												<span class="k-icon k-i-arrowhead-w"></span>&nbsp;Regresar
											</button>
										</div>
										<div style="text-align:right" class="col-md-6">
											<button type="button" id="cmdnewCashCount" class="k-button" title="Nuevo Arqueo de Caja">
												<span class="k-icon k-i-plus"></span>&nbsp;Nuevo
											</button>
											<button type="button" id="cmdsaveCashCountData" class="k-button" title="Guardar Datos">
												<span class="k-icon k-insert"></span>&nbsp;Guardar
											</button>
										</div>
									</div>
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->
				</div><!-- /.padding-md -->
			</div><!-- /main-container -->

		<!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>

		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>

		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>

		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/js/cultures/kendo.culture.es-MX.min.js"></script>
		<!-- DatePicker -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.calendar.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.datepicker.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Grid -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.columnsorter.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.pager.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.grid.js"></script>
		<!-- Window -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.window.js"></script>

		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/cash-count-record.js"></script>

		<?php $vxajax->printJavascript(); ?>
		<script>openAddCash();</script>
		<script>openAddDocument();</script>


	</body>
</html>
