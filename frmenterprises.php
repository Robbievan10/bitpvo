<?php
	if ( ! isset($_SESSION['idUser']) )
	 {	session_start();	}
	require_once("controller/index.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
	</head>
	<body class="overflow-hidden" onLoad="_default();">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home active"></i>&nbsp;Inicio</li>
					</ul>
				</div><!-- /breadcrumb-->
				<div class="main-header clearfix">
					<div class="page-title">
						<h3 class="no-margin">Inicio</h3>
					</div><!-- /page-title -->
				</div><!-- /main-header -->
				<hr/>
				<div class="container">
					<div class="row">
						<div style="text-align:center" class="col-md-12 form-group">   
							<h4><strong class="text-danger inline">PVO</strong></h4>
							<p>&nbsp;</p>                           
						</div>
					</div>
					<div class="row">
						<div style="text-align:center" class="col-md-12 form-group">   
							<img src="./view/img/logo.png" class="img-responsive center-block" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">&nbsp;</div>
						<div style="text-align:justify" class="col-md-10 form-group"><h5>Sistema electónico
							que te permite administrar tu(s) punto(s) de ventas de manera eficaz y eficiente atravéz de la nube garantizandote
							la más alta fiabilidad en tus procesos de ventas.</h5>
						</div>
						<div class="col-md-1">&nbsp;</div>
					</div>
				</div>
			</div><!-- /main-container -->

		</div><!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" href="./frmlogin.php">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
        
		<!-- Bootstrap -->
		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/index.js"></script>
        
        <?php $vxajax->printJavascript(); ?>
        
	</body>
</html>