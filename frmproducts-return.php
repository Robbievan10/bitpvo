<?php
	require_once("controller/session.php");
	require_once("controller/products-sale.php");
	require_once 'Mobile_Detect.php';

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
        
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>            
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />


    <style type="text/css" media="print">
    @page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
    }

    body
    {
        margin: 1mm 1mm 1mm 1mm; /* margin you want for the content */
    }
    </style>

	</head>
	<body class="overflow-hidden" onLoad="_default();">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
<p hidden id="ticketDeVenta"></p>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Registros</li>
						<li class="active">Devoluciones</li>
					</ul>
				</div><!-- /breadcrumb-->
					<div class="row">
						<div class="col-md-6 form-group" style="width: 180%;">


							<div id="vwndwFindProductsSales" style="width: 180%;">
						<div id="vfindProductsSalesData" style="width: 180%;"></div>


							</div>
							<button type="button" id="cmdfindProductsSales" class="k-button" title="Buscar Ventas de Productos">
								<span class="k-icon k-i-search"></span>&nbsp;Buscar
							</button>
							<button type="button" id="cmdprintProductsSale" class="k-button" title="Imprimir Venta de Productos">
								<span class="fa fa-print fa-lg"></span>&nbsp;Imprimir
							</button>
							<button type="button" id="cmdcleanProductsSaleData" class="k-button" title="Limpiar Datos de la Venta de Productos">
								<span class="k-icon k-i-refresh"></span>&nbsp;Limpiar
							</button>
							<button type="button" id="cmdventaTotal" class="k-button" title="Consultar la venta total de esta caja.">
								<span class="k-icon k-i-calculator"></span>&nbsp;Consultar venta total
							</button>								

						</div>
						<div style="text-align:right" class="col-md-6 form-group">
							<button type="button" id="cmdpayProductsSale" class="k-button" title="Pagar Venta de Productos">
								<span class="k-icon k-i-tick"></span>&nbsp;Pagar
							</button>
							<button type="button" id="cmdcancelProductsSale" class="k-button" title="Cancelar Venta de Productos">
								<span class="k-icon k-i-cancel"></span>&nbsp;Cancelar
							</button>
						</div>
					</div>
					<div class="panel panel-default">
						<div style="text-align:right" class="panel-heading">
							<h2><span id="vproductsSaleTotalPrice" class="text-primary">$ 0.00</span></h2>
						</div>
						<div class="panel-body">
 									<form id="vproductsSaleData" name="vproductsSaleData" method="post" onSubmit="return false;">
									<div class="row">
										<div class="col-md-6 form-group">
                                            <span class="textCaption-2">Caja:</span>
                                            <div id="vcashName" class="inline">&nbsp;</div>
										</div>
										<div style="text-align:right" class="col-md-6 form-group">
											<label for="cmrcash" class="label-radio inline">
												<input type="radio" id="cmrcash" name="cmrproductsSaleType" value="1" checked="checked" />
												<span class="custom-radio"></span>
												Contado
											</label>
											<label for="cmrcredit" class="label-radio inline">
												<input type="radio" id="cmrcredit" name="cmrproductsSaleType" value="2" />
												<span class="custom-radio"></span>
												Crédito
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6 form-group">
											<span class="textCaption-2">Folio:</span>
											<div id="vproductsSaleFolio" class="inline">&nbsp;</div>
										</div>
										<div style="text-align:right" class="col-md-6 form-group">
											<label for="dtpckrproductsSaleDate">
												<span class="textCaption-2">Fecha:</span>
											</label>
											<input id="dtpckrproductsSaleDate" name="dtpckrproductsSaleDate" maxlength="10" style="width:37%;" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<label for="cmbproductsSaleCustomer">
												<span class="textCaption-2">Cliente:</span>
											</label>
											<select id="cmbproductsSaleCustomer" name="cmbproductsSaleCustomer" style="width:93.5%"></select>
										</div>
									</div>
									</form>
								<div class="col-md-1">&nbsp;</div>
						</div>
					</div><!-- /panel -->
						<div ><span class="text-info">Productos</span></div>
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="row">
												<div class="col-md-2 form-group">
													<label for="txtproductKey">
														<span class="textCaption-2">Clave:</span>
													</label>
													<input type="number" id="txtproductKey" name="txtproductKey" onkeypress="addProductToProductsSaleListByEnterKey(event);" style="width:100%;" class="form-control input-sm" />
												</div>
												<div class="col-md-2 form-group">
													<label for="txtproductBarCode">
														<span class="textCaption-2">Código de Barra:</span>
													</label>
													<input type="number" id="txtproductBarCode" name="txtproductBarCode" onkeypress="addProductToProductsSaleListByEnterBarCode(event);(event);" style="width:100%;" class="form-control input-sm" />
												</div>
												<div class="col-md-8 form-group">
													<label for="cmbproductsList">
														<span class="textCaption-2">Producto:</span>
													</label>
													<select id="cmbproductsList" name="cmbproductsList" style="width:100%" ></select>
												</div>
											</div>
											<div class="row">
												<form id="vaddProductFormDataByValue" name="vaddProductFormDataByValue" method="post" onSubmit="return false;">
												<div class="col-md-2 form-group">
													<label for="txtproductSaleAmount">
														<span class="textCaption-2">Cantidad:</span>
													</label>
													<input type="number" id="txtproductSaleAmount" name="txtproductSaleAmount" onkeypress="return setEntriesType(event, 'float');" style="width:100%;" class="form-control input-sm" />
												</div>
												<div class="col-md-2 form-group">
													<span class="textCaption-2">Precio Real:</span><br />
													<div id="vproductRealPrice" style="width:100%; text-align:center" class="textCaption-3">$ 0.00</div>
												</div>
												<div class="col-md-2 form-group">
													<label for="txtproductSalePrice">
														<span class="textCaption-2">Precio de Venta:</span>
													</label>
													<input type="number" id="txtproductSalePrice" name="txtproductSalePrice" onkeypress="return setEntriesType(event, 'float');" style="width:100%;" class="form-control input-sm" />
												</div>
												</form>
												<div id="vwndwAddProductToSaleByAction">
													<div id="vaddProductToSaleByActionData"></div>
												</div>                                    
												<div style="text-align:right" class="col-md-6 form-group">
													<button type="button" id="cmdaddProductToSaleByAction" style="visibility: hidden;" class="k-button">
														<span class="k-icon k-i-plus"></span>&nbsp;Agregar
													</button>
													<button type="button" id="cmdaddProductToSaleByValue" class="k-button" title="Agregar Producto a la Venta">
														<span class="k-icon k-i-plus"></span>&nbsp;Agregar
													</button>
												</div>
											</div>
										</div>
									<div class="row">
										<div id="vwndwUpdateProductDataInSale">
											<div id="vupdateProductDataInSaleData"></div>
										</div>
										<div style="text-align:right" class="col-md-12 form-group">
											<button type="button" id="cmdupdateProductDataInSale" class="k-button"  title="Modificar Datos del Producto en la Venta" style="display: none;">
												<span class="k-icon k-i-pencil"></span>&nbsp;Modificar
											</button>
											<button type="button" id="cmddeleteProductOfSale" class="k-button"  title="Eliminar Producto de la Venta">
												<span class="k-icon k-i-close"></span>&nbsp;Eliminar
											</button>
										</div>
									</div>
									<div class="row">
										<div id="vproductsSaleList" class="col-md-12">&nbsp;</div>
									</div>
								</div>
			</div><!-- /main-container -->

		</div>
		<!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
		
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/js/cultures/kendo.culture.es-MX.min.js"></script>
		<!-- DatePicker -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.calendar.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.datepicker.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Grid -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.columnsorter.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.pager.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.grid.js"></script>
		<!-- Window -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.window.js"></script>
        
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/products-sale.js"></script>
        
        <?php $vxajax->printJavascript(); ?>
        
	</body>
</html>