<?php
	require_once("controller/session.php");
	require_once("controller/my-profile.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Punto de Venta</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        		
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
                
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>        
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body class="overflow-hidden" onLoad="_default();">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li class="active">Mi Perfil</li>
					</ul>
				</div><!-- /breadcrumb-->
				<div class="padding-md">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading textCaption-1"><span class="text-info">Datos Generales</span></div>
								<div class="panel-body">
									<form id="vprofileData" name="vprofileData" method="post" onSubmit="return false;" style="padding:0; margin:0">
									<div class="form-group">
										<label for="txtname">Nombre</label>
										<input type="text" id="txtname" name="txtname" maxlength="30" class="form-control input-sm" placeholder="Nombre" />
									</div><!-- /form-group -->
									<div class="form-group">
										<label for="txtfirstName">Apellido Paterno</label>
										<input type="text" id="txtfirstName" name="txtfirstName" maxlength="30" class="form-control input-sm" placeholder="Apellido Paterno" />
									</div><!-- /form-group -->
									<div class="form-group">
										<label for="txtlastName">Apellido Materno</label>
										<input type="text" id="txtlastName" name="txtlastName" maxlength="30" class="form-control input-sm" placeholder="Apellido Materno" />
									</div><!-- /form-group -->
									</form>
									<div style="text-align:right">
										<button type="button" id="cmdsaveProfileData" class="k-button" title="Guardar Datos">
											<span class="k-icon k-insert"></span>&nbsp;Guardar
										</button>
									</div>
								</div>
							</div><!-- /panel -->
						</div><!-- /.col -->
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-heading textCaption-1"><span class="text-info">Datos Cuenta</span></div>
								<div class="panel-body">
									<div class="form-group">
										<span class="textCaption-2">Correo Electrónico:</span>
										<div id="vemail" class="inline">&nbsp;</div>
									</div>
									<div class="form-group">
										<span class="textCaption-2">Tipo de Usuario:</span>
										<div id="vuserType" class="inline">&nbsp;</div>
									</div>
									<div id="vwndwPasswordUpdate">
										<div id="vpasswordUpdateData"></div>
									</div>
									<div style="text-align:right">
										<button type="button" id="cmdupdatePassword" class="k-button" title="Modificar Contraseña">
											<span class="k-icon k-i-lock"></span>&nbsp;Contraseña
										</button>
									</div>
								</div>
							</div><!-- /panel -->
						</div><!-- /.col -->
					</div><!-- /.row -->
					<div class="row">
						<div id="venterprise" class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading textCaption-1"><span class="text-info">Datos Empresa</span></div>
								<div class="panel-body">
									<div class="form-group">
										<span class="textCaption-2">Nombre:</span>
										<div id="venterpriseName" class="inline">&nbsp;</div>
									</div>
									<div class="form-group">
										<span class="textCaption-2">Dirección:</span>
										<div id="venterpriseAddress" class="inline">&nbsp;</div>
									</div>
								</div>
							</div><!-- /panel -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.padding-md -->
			</div><!-- /main-container -->

		</div><!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
        
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>   
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Window -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.window.js"></script>
        
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/my-profile.js"></script>
        
        <?php $vxajax->printJavascript(); ?>
        
	</body>
</html>