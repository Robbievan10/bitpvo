<?php
	require_once("./controller/login.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Punto de Venta</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
                
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>                        
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="./view/css/box-effects.css" rel="stylesheet" type="text/css" media="screen" />        
	</head>
	<body>
		<div class="login-wrapper">
			<div class="text-center">
				<h2 class="fadeInUp animation-delay8" style="font-weight:bold">
				<span class="text-info">PVO</span> <span style="color:#ccc; text-shadow:0 1px #fff">Punto de venta online</span>
				</h2>
			</div>
			<div class="login-widget animation-delay1 effect-2">	
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<div class="pull-left">
							<i class="fa fa-lock fa-lg"></i>&nbsp;Login
						</div>
						<div class="pull-right">
							<a href="./" class="text-info"><i class="fa fa-home fa-lg"></i><strong>&nbsp;Inicio</strong></a>
						</div>
					</div>
					<div class="panel-body">
						<form id="vlogin" name="vlogin" method="post" onSubmit="return false;" class="form-login">
							<div class="form-group">
								<label for="txtuser">Correo Electrónico</label>
								<input type="text" id="txtuser" name="txtuser" maxlength="50" placeholder="Correo Electrónico" class="form-control input-sm bounceIn animation-delay2" />
							</div>
							<div class="form-group">
								<label for="txtpassword">Contraseña</label>
								<input type="password" id="txtpassword" name="txtpassword" maxlength="10" placeholder="Contraseña" class="form-control input-sm bounceIn animation-delay4" />
							</div>
							<div class="seperator"></div>
							<div class="form-group">
								<div id="vwndwRecoveryPassword">
									<div id="vrecoveryPasswordData"></div>
								</div>
								¿Olvidaste tu contraseña?<br/>
								Clickea <a id="vrecoveryPassword" onClick="openPasswordRecoveryWindow();" class="text-info"><strong>Aquí</strong></a> para recuperla
							</div>
						</form>
						<hr/>
						<button type="button" style="width:100%" onClick="login();" class="btn btn-info btn-large bounceIn animation-delay5">
							<i class="fa fa-sign-in"></i>&nbsp;Iniciar sesión
						</button>
					</div>
				</div><!-- /panel -->
			</div><!-- /login-widget -->
		</div><!-- /login-wrapper -->
        
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
          
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<!-- Window -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.window.js"></script>
         
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/login.js"></script>
        
		<?php $vxajax->printJavascript(); ?>
        
	</body>
</html>