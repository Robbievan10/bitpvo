function initialize($option){
	switch ($option) {
		case 1:
		xajax_rutas(1);
			break;
		case 2:
		xajax_rutas(2);
			break;
		case 3:
		xajax_rutas(3);
			break;
		default:
		xajax_rutas(1);
	}

	}



function rutas_semanal($valores,$valores2){

	//var valores = xajax_rutas();

	//var valores = [1,2,3,4,5,6,7];
	//console.log($valores[0].ruta);
	Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Venta semana pasada'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Lunes',
            'Martes',
            'Miércoles',
            'Jueves',
            'Viernes',
            'Sábado',
            'Domingo'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Cantidad en pesos ($)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>${point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: $valores2
});



Highcharts.chart('container2', {
	chart: {
			type: 'column'
	},
	title: {
			text: 'Venta semana actual'
	},
	subtitle: {
			text: ''
	},
	xAxis: {
			categories: [
					'Lunes',
					'Martes',
					'Miércoles',
					'Jueves',
					'Viernes',
					'Sábado',
					'Domingo'
			],
			crosshair: true
	},
	yAxis: {
			min: 0,
			title: {
					text: 'Cantidad en pesos ($)'
			}
	},
	tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>${point.y:.1f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
	},
	plotOptions: {
			column: {
					pointPadding: 0.2,
					borderWidth: 0
			}
	},
	series: $valores
});
}
