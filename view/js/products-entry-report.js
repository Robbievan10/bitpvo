var _$_462a = ["\x65\x73\x2D\x4D\x58", "\x63\x75\x6C\x74\x75\x72\x65", "\x6B\x65\x6E\x64\x6F\x44\x61\x74\x65\x50\x69\x63\x6B\x65\x72", "\x64\x61\x74\x61", "\x4D\x4D\x2F\x64\x64\x2F\x79\x79\x79\x79", "\x23\x64\x74\x70\x63\x6B\x72\x73\x74\x61\x72\x74\x44\x61\x74\x65", "\x23\x64\x74\x70\x63\x6B\x72\x65\x6E\x64\x44\x61\x74\x65", "\x6B\x65\x6E\x64\x6F\x42\x75\x74\x74\x6F\x6E", "\x23\x63\x6D\x64\x70\x72\x69\x6E\x74\x49\x6E\x63\x6F\x6D\x65\x45\x78\x70\x65\x6E\x73\x65\x73", "\x23\x63\x6D\x64\x63\x61\x6E\x63\x65\x6C\x46\x69\x6C\x74\x65\x72", "\x76\x66\x69\x6C\x74\x65\x72\x44\x61\x74\x61", "\x67\x65\x74\x46\x6F\x72\x6D\x56\x61\x6C\x75\x65\x73", "\x76\x61\x6C\x75\x65", "\x64\x74\x70\x63\x6B\x72\x73\x74\x61\x72\x74\x44\x61\x74\x65", "\x24", "\x2F", "\x64\x74\x70\x63\x6B\x72\x65\x6E\x64\x44\x61\x74\x65", "\x67\x65\x74\x44\x61\x74\x65", "\x73\x65\x74\x44\x61\x74\x65", "\x6D\x69\x6E", "\x6D\x61\x78", "", "\x50\x72\x6F\x70\x6F\x72\x63\x69\x6F\x6E\x65\x20\x6C\x61\x20\x46\x65\x63\x68\x61\x20\x49\x6E\x69\x63\x69\x61\x6C\x2E", "\x66\x6F\x63\x75\x73", "\x50\x72\x6F\x70\x6F\x72\x63\x69\x6F\x6E\x65\x20\x75\x6E\x61\x20\x46\x65\x63\x68\x61\x20\x49\x6E\x69\x63\x69\x61\x6C\x20\x76\xE1\x6C\x69\x64\x61\x2E", "\x66\x72\x6F\x6D\x43\x68\x61\x72\x43\x6F\x64\x65", "\x46\x6F\x72\x6D\x61\x74\x6F\x3A\x20\x27\x6D\x65\x73\x2F\x64\x69\x61\x2F\x61\xF1\x6F\x27", "\x50\x72\x6F\x70\x6F\x72\x63\x69\x6F\x6E\x65\x20\x6C\x61\x20\x46\x65\x63\x68\x61\x20\x46\x69\x6E\x61\x6C\x2E", "\x50\x72\x6F\x70\x6F\x72\x63\x69\x6F\x6E\x65\x20\x75\x6E\x61\x20\x46\x65\x63\x68\x61\x20\x46\x69\x6E\x61\x6C\x20\x76\xE1\x6C\x69\x64\x61\x2E"];
var vdtpckrstartDate;
var vdtpckrendDate;
var vcmdPrintIncomeExpenses;
var vcmdCancelFilter;

function _default() {
    menu(28);
    configureControls()
}

function configureControls() {
    setDates();
    setButtons()
}

function setDates() {
    kendo[_$_462a[1]](_$_462a[0]);
    vdtpckrstartDate = $(_$_462a[5])[_$_462a[2]]({
        value: new Date(),
        format: _$_462a[4],
        change: startDateChange
    })[_$_462a[3]](_$_462a[2]);
    vdtpckrendDate = $(_$_462a[6])[_$_462a[2]]({
        value: new Date(),
        format: _$_462a[4],
        change: endDateChange
    })[_$_462a[3]](_$_462a[2])
}

function setButtons() {
    $(_$_462a[8])[_$_462a[7]]({
        click: printIncomeExpenses
    });
    vcmdPrintIncomeExpenses = $(_$_462a[8])[_$_462a[3]](_$_462a[7]);
    $(_$_462a[9])[_$_462a[7]]({
        click: cancelFilter
    });
    vcmdCancelFilter = $(_$_462a[9])[_$_462a[3]](_$_462a[7])
}

function printIncomeExpenses() {
    if (validateIncomeExpensesFilters()) {
        xajax_printIncomeExpenses(xajax[_$_462a[11]](_$_462a[10]))
    }
}

function cancelFilter() {
    var b = new Date();
    var a = new Array(2);
    a = getDateArray(b);
    xajax[_$_462a[14]](_$_462a[13])[_$_462a[12]] = a[1] + _$_462a[15] + a[0] + _$_462a[15] + a[2];
    xajax[_$_462a[14]](_$_462a[16])[_$_462a[12]] = a[1] + _$_462a[15] + a[0] + _$_462a[15] + a[2]
}

function startDateChange() {
    var d = vdtpckrstartDate[_$_462a[12]](),
        c = vdtpckrendDate[_$_462a[12]]();
    if (d) {
        d = new Date(d);
        d[_$_462a[18]](d[_$_462a[17]]());
        vdtpckrendDate[_$_462a[19]](d)
    } else {
        if (c) {
            vdtpckrstartDate[_$_462a[20]](new Date(c))
        } else {
            c = new Date();
            vdtpckrstartDate[_$_462a[20]](c);
            vdtpckrendDate[_$_462a[19]](c)
        }
    }
}

function endDateChange() {
    var c = vdtpckrendDate[_$_462a[12]](),
        d = vdtpckrstartDate[_$_462a[12]]();
    if (c) {
        c = new Date(c);
        c[_$_462a[18]](c[_$_462a[17]]());
        vdtpckrstartDate[_$_462a[20]](c)
    } else {
        if (d) {
            vdtpckrendDate[_$_462a[19]](new Date(d))
        } else {
            c = new Date();
            vdtpckrstartDate[_$_462a[20]](c);
            vdtpckrendDate[_$_462a[19]](c)
        }
    }
}

function validateIncomeExpensesFilters() {
    var e = true;
    if (Trim(xajax[_$_462a[14]](_$_462a[13])[_$_462a[12]]) == _$_462a[21]) {
        alert(_$_462a[22]);
        xajax[_$_462a[14]](_$_462a[13])[_$_462a[23]]();
        e = false
    } else {
        if (!IsDate(Trim(xajax[_$_462a[14]](_$_462a[13])[_$_462a[12]]))) {
            alert(_$_462a[24] + String[_$_462a[25]](13) + _$_462a[26]);
            xajax[_$_462a[14]](_$_462a[13])[_$_462a[23]]();
            e = false
        } else {
            if (Trim(xajax[_$_462a[14]](_$_462a[16])[_$_462a[12]]) == _$_462a[21]) {
                alert(_$_462a[27]);
                xajax[_$_462a[14]](_$_462a[16])[_$_462a[23]]();
                e = false
            } else {
                if (!IsDate(Trim(xajax[_$_462a[14]](_$_462a[16])[_$_462a[12]]))) {
                    alert(_$_462a[28] + String[_$_462a[25]](13) + _$_462a[26]);
                    xajax[_$_462a[14]](_$_462a[16])[_$_462a[23]]();
                    e = false
                }
            }
        }
    };
    return e
}