<?php
/**
 * Obtiene el detalle de una meta especificada por
 * su identificador "idMeta"
 */

require 'Devices.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['imei'])) {

        // Obtener parámetro idMeta
        $parametro = $_GET['imei'];

        // Tratar retorno
        $retorno = Devices::getById($parametro);


        if ($retorno) {

            $device["status"] = "1";
            $device["data"] = $retorno;
            // Enviar objeto json de la meta
            print json_encode($device);
        } else {
            // Enviar respuesta de error general
            print json_encode(
                array(
                    'status' => '2',
                    'mensaje' => 'No se obtuvo el registro'
                )
            );
        }

    } else {
        // Enviar respuesta de error
        print json_encode(
            array(
                'status' => '3',
                'mensaje' => 'Se necesita un identificador'
            )
        );
    }
}

?>