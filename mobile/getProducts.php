<?php
/**
 * Obtiene el detalle de una meta especificada por
 * su identificador "idMeta"
 */

require 'Stock.php';


if ($_SERVER['REQUEST_METHOD'] == 'GET') {



        // Tratar retorno
        $retorno = Stock::getProducts();


        if ($retorno) {

            $device["status"] = "1";
            $device["data"] = $retorno;
            // Enviar objeto json de la meta
            print json_encode($device);
        } else {
            // Enviar respuesta de error general
            print json_encode(
                array(
                    'status' => '2',
                    'mensaje' => 'No se obtuvo el registro'
                )
            );
        }


}

?>