<?php
/**
 * Obtiene el detalle de una meta especificada por
 * su identificador "idMeta"
 */

require_once (dirname(dirname(__FILE__)) . "/mobile/LoginController.php");


if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['usr'])) {

        // Obtener parámetro idMeta
        $usr = $_GET['usr'];
        $pwd = $_GET['pwd'];

        // Tratar retorno
        $retorno = LoginController::getLogin($usr,$pwd);


        if ($retorno) {

            $response["status"] = "1";
            $response["data"] = $retorno;
            // Enviar objeto json de la meta
            //$status = array();
            //$status["status"] = "1";
            //$retorno["status"] = "1";
            print json_encode($response);
        } else {
            // Enviar respuesta de error general
            print json_encode(
                array(
                    'status' => '2',
                    'data' => 'No se obtuvo el registro'
                )
            );
        }

    } else {
        // Enviar respuesta de error
        print json_encode(
            array(
                'status' => '3',
                'data' => 'Se necesita un identificador'
            )
        );
    }
}

?>
