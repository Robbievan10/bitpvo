<?php
/**
 * Obtiene el detalle de una meta especificada por
 * su identificador "idMeta"
 */

require_once (dirname(dirname(__FILE__)) . "/mobile/LoginController.php");


if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if (isset($_GET['usr']) && isset($_GET['pwd']) && isset($_GET['newpwd'])) {

        // Obtener parámetro idMeta
        $usr = $_GET['usr'];
        $pwd = $_GET['pwd'];
        $newpwd = $_GET['newpwd'];

        // Tratar retorno
        $retorno = LoginController::updatePwd($usr,$pwd ,$newpwd);


        if ($retorno) {

            $response["status"] = "1";
            //$response = $retorno;
            print json_encode($response);
        } else {
            // Enviar respuesta de error general
            print json_encode(
                array(
                    'status' => '2'
                )
            );
        }

    } else {
        // Enviar respuesta de error
        print json_encode(
            array(
                'status' => '3'
            )
        );
    }
}

?>
