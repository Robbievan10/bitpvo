<?php

/**
 * Representa el la estructura de las metas
 * almacenadas en la base de datos
 */
require 'Database.php';

class LoginController
{
    function __construct()
    {
    }

    /**
     * Retorna en la fila especificada de la tabla 'devices'
     *
     * @param $idMeta Identificador del registro
     * @return array Datos del registro
     */
    public static function getAll()
    {
        $consulta = "SELECT * FROM devices";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Obtiene los campos de una meta con un identificador
     * determinado
     *
     * @param $imei Identificador de la meta
     * @return mixed
     */
    public static function getLogin($usr,$pwd)
    {
        // Consulta de la meta
        $consulta = "SELECT id_user,
                            fldfirstName,
                            fldlastName,
                            fldname,
                            id_userStatus,
                            id_userType
                             FROM c_user WHERE id_user = '$usr' AND fldpassword = '$pwd'";


        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();
            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            // Aquí puedes clasificar el error dependiendo de la excepción
            // para presentarlo en la respuesta Json
            return -1;
        }
    }

    /**
     * Actualiza un registro de la bases de datos basado
     * en los nuevos valores relacionados con un identificador
     *
     * @param $idMeta      identificador
     * @param $titulo      nuevo titulo
     * @param $descripcion nueva descripcion
     * @param $fechaLim    nueva fecha limite de cumplimiento
     * @param $categoria   nueva categoria
     * @param $prioridad   nueva prioridad
     */
    public static function update(
        $id,
        $imei,
        $status
    )
    {
        // Creando consulta UPDATE
        $consulta = "UPDATE devices" .
            " SET imei=?, status=? " .
            "WHERE id=?";

        // Preparar la sentencia
        $cmd = Database::getInstance()->getDb()->prepare($consulta);

        // Relacionar y ejecutar la sentencia
        $cmd->execute(array($imei, $status, $id));

        return $cmd;
    }




    public static function updatePwd(
    $usr,
    $pwd,
    $newpwd
)
{
    // Creando consulta UPDATE
    $consulta = "UPDATE c_user SET fldpassword='$newpwd' WHERE id_user='$usr' AND fldpassword='$pwd'";

    // Preparar la sentencia
    $cmd = Database::getInstance()->getDb()->prepare($consulta);

    // Relacionar y ejecutar la sentencia
    $cmd->execute();

    return $cmd->rowCount();
}

    /**
     * Insertar una nueva meta
     *
     * @param $titulo      titulo del nuevo registro
     * @param $descripcion descripción del nuevo registro
     * @param $fechaLim    fecha limite del nuevo registro
     * @param $categoria   categoria del nuevo registro
     * @param $prioridad   prioridad del nuevo registro
     * @return PDOStatement
     */
    public static function insert(
        $imei,
        $status
    )
    {
        // Sentencia INSERT
        $comando = "INSERT INTO devices ( " .
            "imei," .
            " status)" .
            " VALUES( ?,?)";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(
            array(
                $imei,
                $status
            )
        );

    }

    /**
     * Eliminar el registro con el identificador especificado
     *
     * @param $idMeta identificador de la meta
     * @return bool Respuesta de la eliminación
     */
    public static function delete($imei)
    {
        // Sentencia DELETE
        $comando = "DELETE FROM devices WHERE imei=?";

        // Preparar la sentencia
        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(array($imei));
    }
}

?>
