function AGREGAR_OPCION(mcombo, mtexto, mvalor) //Agrega una opci�n en un combo (List 'Select')
{	
	var mobj_option = new Option(mtexto, mvalor);
	xajax.$(mcombo).options.add(mobj_option);
}

function LIMPIAR_OPCIONES(mcombo)  //Elimina las opciones en un combo (List 'Select')
{
	xajax.$(mcombo).options.length = 0;
}

function PONER_ID(melemento, mid)	//Establece un identificador a un elemento
{	
	xajax.$(melemento).id=mid;
}

function PONER_VALOR(melemento, mvalor)	//Estable un valor a un elemento
{
	var mcadena_filtrada, mlista;
	
	//switch(document.getElementById(melemento).type)
	switch(xajax.$(melemento).type)
	 {
		case "textarea":	mlista = mvalor.split("");
							mcadena_filtrada="";
							for(mi=0; mi<mvalor.length; mi++)
							 {
								 switch (mvalor.charCodeAt(mi))
								  {
								  	case 127:	mcadena_filtrada=mcadena_filtrada + String.fromCharCode(13);
												break;
									default:	mcadena_filtrada= mcadena_filtrada + mlista[mi];
												break;
								  }
							 }
							mvalor=mcadena_filtrada;
							break;
	 }
	 xajax.$(melemento).value=mvalor;
	//document.getElementById(melemento).value=mvalor;
}

function HABILITAR(melemento)	//Habilita a un elemento
{
	xajax.$(melemento).disabled=false;
}

function INHABILITAR(melemento)	//Deshabilita a un elemento
{
	xajax.$(melemento).disabled=true;
}

function ENFOCAR(melemento)	//Enfoca a un elemento
{
	xajax.$(melemento).focus();
}

function SELECCIONAR(mchk)	//Seleccionar un elemento
{
	xajax.$(mchk).checked=true;
}

function DESELECCIONAR(mchk)	//Deseleccionar un elemento
{
	xajax.$(mchk).checked=false;
}

function REGRESA_ESTADO_SELECCION(mchk)	//Regresa el estado de selecci�n de un elemento
{
	return xajax.$(mchk).checked;
}

function SELECCIONAR_OPCION(mcmb, mfondo)	//Establecer una opcion en un combo (List 'Select')
{
	mlongitud=parseInt(xajax.$(mcmb).length);
	for (mi=0; mi<mlongitud; mi++)
	 {
		 if ( Trim(xajax.$(mcmb).options[mi].value)==mfondo )
		  {
			  xajax.$(mcmb).selectedIndex=mi;
		  }
	 }	
}

function MOSTRAR_DIV(mdiv) //Muestra una ventana(Div) espec�ficada
{
	//Establecer la propiedad 'visibilidad' del div a 'visible'
	xajax.$(mdiv).style.visibility="visible";
}

function OCULTAR_DIV(mdiv) //Cierra una ventana(Div) especificada
{
	//Establecer la propiedad 'visibilidad' del div a 'oculto'
	xajax.$(mdiv).style.visibility="hidden";
}









function GET_XMLHTTP() //Crea una instancia del objeto XmlHttp
{
 var mXmlHttp=false;	//Crear una variable para establecer una instancia v�lida del ActiveX (XmlHttp)
 	
	//Checar si estamos usando internet explorer
 	try {
			//Establecer la instacia xmlhttp, con la versi�n javascript superior que 5.
     	 	mXmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} 
 	catch (e) 
		{
			//Si no, entonces usar el objeto active x anterior
     		try {
					//Establecer la instancia xmlhttp, para internet explorer.	
		  	 		mXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		 		} 
	 		catch (e) 
	     		{
		  	 		//Establecer la instancia xmlhttp inv�lida (no internet explorer)
		  	 		mXmlHttp = false;
		 		}
		}
		
	//Si no estamos usando IE
 	if (!mXmlHttp && typeof XMLHttpRequest != 'undefined')
  	 {
		 //Establecer la instancia xmlhttp, usando un explorar diferente a internet explorer
		 mXmlHttp = new XMLHttpRequest();
  	 }
 
 	//Regresar la instancia xmlhttp
  	return mXmlHttp;
}

function PROCESA_AJAX(mPaginaServidor, mObjeto, mGetOrPost, mCadena)//Procesa un XMLHttpRequest.
{
  var mXmlHttp;	//Crear una variable para establecer la instancia de un ActiveX (XmlHttp)
  
  	//Establecer una instancia XmlHttp
  	mXmlHttp=GET_XMLHTTP();
	 
  	//Verificar el tipo de solicitud	 
  	if (mGetOrPost == "GET")
   	 {
		//Abrir la conexi�n a la p�gina servidor
		mXmlHttp.open("GET", mPaginaServidor, false);
		//Establecer el estado de la conexi�n
    	mXmlHttp.onreadystatechange = function() {
													if (mXmlHttp.readyState==1) 
													 {
														 mObjeto.innerHTML="Cargando.......";
         											 }
													//Verificar el estado de la conexi�n
													if (mXmlHttp.readyState == 4 && mXmlHttp.status == 200) 
											   		 {
														 //Mostrar la informaci�n solicitada
														 mObjeto.innerHTML = mXmlHttp.responseText;	 
     										   		 }
   											 	 };
		//Enviar un valor nulo
		mXmlHttp.send(null);
   	 } 
  	else
   	 {
		//Abrir la conexi�n a la p�gina servidor
		mXmlHttp.open("POST", mPaginaServidor, true);
		//Establecer el tipo de encabezado
    	mXmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
		//Establecer el estado de la conexi�n
		mXmlHttp.onreadystatechange = function() {
													if (mXmlHttp.readyState==1) 
													 {
														 mObjeto.innerHTML="Cargando.......";
         											 }
													//Verificar el estado de la conexi�n
											  		if (mXmlHttp.readyState == 4 && mXmlHttp.status == 200)
											   		 {
														//Mostrar la informaci�n solicitada
														mObjeto.innerHTML = mXmlHttp.responseText;
											   		 }
   											 	 };
 		//Enviar la lista de los objetos
		mXmlHttp.send(mCadena);
   	 }
}
 
function SOLICITAR_INFORMACION(mTipo, mpg) //Solicita la Informaci�n especificada (FIBAP)
{
  var mPaginaServidor;	//Crear una variable para establecer el nombre de la p�gina servidor
  var mVariables;		//Crear una variable para establecer el nombre y valor de cada una de las variables que se enviar�n al servidor
  var mObjeto;			//Crera una variable para establecer el objeto donde se desplegar� la informaci�n
   
  	//Seleccionar el tipo de informaci�n a desplegar
  	switch(mTipo)
	 {
		case 1:		//Establecer la p�gina 'Descripci�n' de una FIBAP
	   		   		mPaginaServidor="funciones/busqueda.php";
					mVariables="mpg=" + mpg;
					mNombreObjeto="mdiv";
					//mVariables="mCveDependencia=" + mCveDependencia + "&mFolio=" + mFolio;
	   		   		break;
		case 2:		mPaginaServidor="funciones/DOS.php";
					mNombreObjeto="mFiltro_Datos";
	   		      	//Mostrar la ventana de Estructura de Financiamiento (Proyectos) de una FIBAP
					mVariables="mCveDependencia=1";
					MOSTRAR_DIV("mFiltro");
					break;
	 }   
	
  	//Establecer el objeto donde se desplegar� la informaci�n
  	mObjeto = document.getElementById(mNombreObjeto);
  	//Procesar la solicitud AJAX
  	PROCESA_AJAX(mPaginaServidor, mObjeto, "POST",mVariables);
}



function OCULTAR_VENTANA(mNombre_Ventana) //Cierra una ventana(Div) especificada
{
	//Establecer la propiedad 'visibilidad' del div a 'oculto'
	document.getElementById(mNombre_Ventana).style.visibility="hidden";
	//Cerrar las ventanas "hijas"
	switch (mNombre_Ventana)
	 {
		case "mDetalle_Cobertura":		document.getElementById("mDetalle_Cobertura_MB").style.visibility="hidden";				break;
		case "mEstructura_Financiamiento_P":
										document.getElementById("mEstructura_Financiamiento_PA").style.visibility="hidden";
										if (document.getElementById("mEstructura_Financiamiento_PAG"))
									 	 {
									 		document.getElementById("mEstructura_Financiamiento_PAG").style.visibility="hidden";
									 	 }
																																break;
		case "mEstructura_Financiamiento_PA":
										document.getElementById("mEstructura_Financiamiento_PAG").style.visibility="hidden";	break;
	 }
}






function Trim(str)	//Elimina espacios a la derecha e izquierda de una cadena
{
	 var resultStr = "";
	 resultStr = TrimLeft(str);
	 resultStr = TrimRight(resultStr);
	 return resultStr;
}

function TrimLeft(str)	//Elimina espacios a la izquierda de una cadena
{
	 var resultStr = "";
	 var i = len = 0;
	 
	 if (str+"" == "undefined" || str == null) 
		return "";
		str += "";
	
	 if (str.length == 0) 
		resultStr = "";
	 else
	  { 
		len = str.length;
		while ((i <= len) && (str.charAt(i) == " "))
			i++;
			resultStr = str.substring(i, len);
	  }
	 return resultStr;
}

function TrimRight(str)	//Elimina espacios a la derecha de una cadena
{
	 var resultStr = "";
	 var i = 0;
	 if (str+"" == "undefined" || str == null) 
		return "";
		str += "";
	 if (str.length == 0) 
		resultStr = "";
	 else 
	  {
		i = str.length - 1;
		while ((i >= 0) && (str.charAt(i) == " "))
			i--;
			resultStr = str.substring(0, i + 1);
	  }
	 return resultStr; 
}

function IsDate(dateStr)	//Verifica fecha v�lida
{
	var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var matchArray = dateStr.match(datePat); 
	var datestatus=true;
	
	datemsg="";
	
	if (matchArray == null || matchArray[1]==null || matchArray[2]==null || matchArray[3]==null)
	 {
		datemsg="----- Please enter date as mm/dd/yyyy " + "\n";
		return false;
	 }
	
	month = matchArray[1];
	day = matchArray[3];
	year = matchArray[5];
	
	if (month < 1 || month > 12) 
	 { 
		datemsg=datemsg + "----- Month must be between 1 and 12." + "\n";
		return false;
	 }
	if (day < 1 || day > 31) 
	 {
		datemsg=datemsg + "----- Day must be between 1 and 31." + "\n";
		return false;
	 }
	
	if ((month==4 || month==6 || month==9 || month==11) && day==31) 
	 {
		datemsg=datemsg + "----- Month " + month + " doesn`t have 31 days!" + "\n";
		return false;
	 }
	
	if (month == 2) 
	 {
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap)) 
		 {
			datemsg=datemsg + "----- February " + year + " doesn`t have " + day + " days!" + "\n";
			return false;
		 }
	 }
	return true;
}

function IsInteger (s)	//Verifica entero v�lido
{
	var i;

	if (IsEmpty(s))
	if (IsInteger.arguments.length == 1) return 0;
	else return (IsInteger.arguments[1] == true);

	for (i = 0; i < s.length; i++)
	 {
		var c = s.charAt(i);
		if (!IsDigit(c)) return false;
	 }
	return true;
}

function IsEmpty(s)	//Verifica vacio
{
	return ((s == null) || (s.length == 0))
}

function IsDigit (c)	//Verifica digito

{
	return ((c >= "0") && (c <= "9"))
}