<?php
$mxajax_core = dirname(dirname(__FILE__)) . "/utilerias/xajax/xajax_core";
require_once($mxajax_core . "/xajax.inc.php");

$mxajax = new xajax();
$mxajax->configure("javascript URI", "./utilerias/xajax");
$mxajax->setCharEncoding('UTF-8');

require_once("index_clase.php");
require_once("clases.php");
 
function LLENAR_ORGANO_ADMVO_1($cmb)
 {
	$mproc= new INDEX();
	$mproc->LLENAR_ORGANO_ADMVO_1($cmb, 0);
	return $mproc;
 }
function LLENAR_ORGANO_ADMVO_2($cmb, $morgano_admvo)
 {
	$mproc= new INDEX();
	$mproc->LLENAR_ORGANO_ADMVO_2($cmb, $morgano_admvo);
	return $mproc;
 }
function LLENAR_ORGANO_ADMVO_3($cmb, $morgano_admvo)
 {
	$mproc= new INDEX();
	$mproc->LLENAR_ORGANO_ADMVO_3($cmb, $morgano_admvo, 0);
	return $mproc;
 }
function LLENAR_ORGANO_ADMVO_4($cmb, $morgano_admvo)
 {
	$mproc= new INDEX();
	$mproc->LLENAR_ORGANO_ADMVO_4($cmb, $morgano_admvo, 0);
	return $mproc;
 }
function LLENAR_ORGANO_ADMVO_5($cmb, $morgano_admvo)
 {	
	$mproc= new INDEX();
	$mproc->LLENAR_ORGANO_ADMVO_5($cmb, $morgano_admvo, 0);
	return $mproc;
 }

function MOSTRAR_ORGANO_ADMVO($mform_organo_admvo, $mnivel)
 {	
	$mproc= new INDEX();
	$morgano_admvo= array();
	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
	$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
	$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
	$morgano_admvo[3]=trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
	$morgano_admvo[4]=trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);

	$mproc->MOSTRAR_ORGANO_ADMVO($morgano_admvo, $mnivel);
	return $mproc;
 } 
function LISTAR_FUNCION($morgano_admvo, $mpg, $mnivel)
 {
 	$mproc= new INDEX();
	$mproc->LISTAR_FUNCION($morgano_admvo, $mpg, $mnivel);
	return $mproc;
 }
function MOSTRAR_FUNCION($mform_datos, $mcve_funcion, $mnivel)
 {
 	$mproc= new INDEX();
	$mproc->MOSTRAR_FUNCION($mform_datos, $mcve_funcion, $mnivel);
	return $mproc;
 } 
function MOSTRAR_OPCIONES_IMPRESION($mcontenedor)
 {
 	$mproc= new INDEX();
	$mproc->MOSTRAR_OPCIONES_IMPRESION($mcontenedor);
	return $mproc;
 }
function MOSTRAR_PDF($mtipo, $morgano_admvo1)
 {
	$mproc= new INDEX();
	$mproc->MOSTRAR_PDF($mtipo, $morgano_admvo1);
	return $mproc;
 }

function GUARDAR_NUEVO_ORGANO($mform_organo_admvo, $mform_datos)
 {
	$mproc= new INDEX();
	$mproc->GUARDAR_NUEVO_ORGANO($mform_organo_admvo, $mform_datos);
	return $mproc;
 }
function GUARDAR_MODIFICACION_ORGANO($mform_organo_admvo, $mform_datos)
 {
	$mproc= new INDEX();
	$mproc->GUARDAR_MODIFICACION_ORGANO($mform_organo_admvo, $mform_datos);
	return $mproc;
 }
function GUARDAR_NUEVO_FUNCION($mform_organo_admvo, $mform_datos)
 {
	$mproc= new INDEX();
	$mproc->GUARDAR_NUEVO_FUNCION($mform_organo_admvo, $mform_datos);
	return $mproc;
 }
function GUARDAR_MODIFICACION_FUNCION($mform_organo_admvo, $mform_datos, $mcve_funcion)
 {
	$mproc= new INDEX();
	$mproc->GUARDAR_MODIFICACION_FUNCION($mform_organo_admvo, $mform_datos, $mcve_funcion);
	return $mproc;
 } 

function ELIMINAR_ORGANO($mform_organo_admvo)
 {
	$mproc= new INDEX();
	$mproc->ELIMINAR_ORGANO($mform_organo_admvo, $mproc->REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo), 0);
	return $mproc;
 } 
function ELIMINAR_FUNCION($mform_organo_admvo, $mcve_funcion)
 {
	$mproc= new INDEX();
	$mproc->ELIMINAR_FUNCION($mform_organo_admvo, $mcve_funcion);
	return $mproc;
 } 
 
function INICIALIZAR_DATOS_ORGANO()
 {
	$mproc= new INDEX();
	
	$mcmd= new BUTTON("cmdNuevo_Guardar_Organo", $mproc);
	$mcmd->SET_VALUE("Guardar");
	$mtxt= new TEXT("txt_Tipo_Guardar_Organo", $mproc);
	$mtxt->SET_VALUE("1");
	
	$mcmd= new BUTTON("cmdModificar_Organo", $mproc);
	$mcmd->DISABLED();
	$mcmd= new BUTTON("cmdEliminar_Organo", $mproc);
	$mcmd->DISABLED();
	$mcmd= new BUTTON("cmdCancelar_Organo", $mproc);
	$mcmd->ENABLED();
	
	$mproc->LIMPIAR_TEXTOS_ORGANO();
	$mproc->HABILITAR_TEXTOS_ORGANO();
	$mtxt= new TEXT("txtOrgano", $mproc);
	$mtxt->FOCUS();
	
	$mproc->script("xajax_CANCELAR_FUNCION(1);");
	$mcmd= new BUTTON("cmdNuevo_Guardar_Funcion", $mproc);
	$mcmd->DISABLED();
	$mproc->assign("mListado_Funcion","innerHTML","");
	return $mproc;	
 }
function INICIALIZAR_DATOS_FUNCION()
 {
	$mproc= new INDEX();
	
	$mboton_nuevo_g= new BUTTON("cmdNuevo_Guardar_Funcion", $mproc);
	$mboton_nuevo_g->SET_VALUE("Guardar");
	$mboton_nuevo_g->ENABLED("Guardar");
	$mtxt_tipo_go= new TEXT("txt_Tipo_Guardar_Funcion", $mproc);
	$mtxt_tipo_go->SET_VALUE("1");
	
	$mboton_modificar= new BUTTON("cmdModificar_Funcion", $mproc);
	$mboton_modificar->DISABLED();
	$mboton_eliminar= new BUTTON("cmdEliminar_Funcion", $mproc);
	$mboton_eliminar->DISABLED();
	$mboton_cancelar= new BUTTON("cmdCancelar_Funcion", $mproc);
	$mboton_cancelar->ENABLED();
	
	$mproc->LIMPIAR_TEXTOS_FUNCION();
	$mproc->HABILITAR_TEXTOS_FUNCION();
	$mproc->LLENAR_FUNCION("cmbFuncion");
	$mtxt_titulo= new TEXT("txtTitulo_Funcion", $mproc);
	$mtxt_titulo->FOCUS();
	return $mproc;	
 }

function CANCELAR_ORGANO($mform_organo_admvo)
 {
	$mproc= new INDEX();
	
	$mcmd= new BUTTON("cmdNuevo_Guardar_Organo", $mproc);
	$mcmd->SET_VALUE("Nuevo");
	$mcmd->ENABLED();
	$mtxt= new TEXT("txt_Tipo_Guardar_Organo", $mproc);
	$mtxt->SET_VALUE("0");
	
	$mcmd= new BUTTON("cmdModificar_Organo", $mproc);
	$mcmd->DISABLED();
	$mcmd= new BUTTON("cmdEliminar_Organo", $mproc);
	$mcmd->DISABLED();	
	$mcmd= new BUTTON("cmdCancelar_Organo", $mproc);
	$mcmd->DISABLED();
	
	$morgano_admvo= array();
	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
	$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
	$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
	$morgano_admvo[3]=trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
	$morgano_admvo[4]=trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);
	
	$mproc->MOSTRAR_ORGANO_ADMVO($morgano_admvo, $mproc->REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo));
	$mproc->INHABILITAR_TEXTOS_ORGANO();
	
	return $mproc;
 }
function CANCELAR_FUNCION($mtipo)
 {
	$mproc= new INDEX();
	
	$mboton_nuevo_g= new BUTTON("cmdNuevo_Guardar_Funcion", $mproc);
	$mboton_nuevo_g->SET_VALUE("Nuevo");
	if ($mtipo==0)
	 {	$mboton_nuevo_g->ENABLED();	}
 	else
	 {	$mboton_nuevo_g->DISABLED();	}
	 
	$mtxt= new TEXT("txt_Tipo_Guardar_Funcion", $mproc);
	$mtxt->SET_VALUE("0");
		
	$mboton_modificar= new BUTTON("cmdModificar_Funcion", $mproc);
	$mboton_modificar->DISABLED();
	$mboton_eliminar= new BUTTON("cmdEliminar_Funcion", $mproc);
	$mboton_eliminar->DISABLED();
	$mboton_cancelar= new BUTTON("cmdCancelar_Funcion", $mproc);
	$mboton_cancelar->DISABLED();
	
	$mselect= new SELECT("cmbFuncion", $mproc);				
	$mselect->CLEAR_OPTIONS();
	
	$mproc->LIMPIAR_TEXTOS_FUNCION();
	$mproc->INHABILITAR_TEXTOS_FUNCION();
	return $mproc;
 }

function HABILITAR_MODIFICAR_ORGANO()
{
	$mproc= new INDEX();
	$mboton_nuevo_g= new BUTTON("cmdNuevo_Guardar_Organo", $mproc);
	$mboton_nuevo_g->SET_VALUE("Guardar");
	$mtxt= new TEXT("txt_Tipo_Guardar_Organo", $mproc);
	$mtxt->SET_VALUE("2");
	
	$mboton_cancelar= new BUTTON("cmdCancelar_Organo", $mproc);
	$mboton_cancelar->ENABLED();

	$mproc->HABILITAR_TEXTOS_ORGANO();
	
	$mtxt_organo= new TEXT("txtOrgano", $mproc);
	$mtxt_organo->FOCUS();	
	return $mproc;
}

function HABILITAR_MODIFICAR_FUNCION()
{
	$mproc= new INDEX();
	$mboton_nuevo_g= new BUTTON("cmdNuevo_Guardar_Funcion", $mproc);
	$mboton_nuevo_g->SET_VALUE("Guardar");
	$mtxt= new TEXT("txt_Tipo_Guardar_Funcion", $mproc);
	$mtxt->SET_VALUE("2");
	
	$mboton_cancelar= new BUTTON("cmdCancelar_Funcion", $mproc);
	$mboton_cancelar->ENABLED();

	$mproc->HABILITAR_TEXTOS_FUNCION();
	
	$mtxt_organo= new TEXT("txtTitulo_Funcion", $mproc);
	$mtxt_organo->FOCUS();	
	return $mproc;
}

$mxajax->registerFunction("LLENAR_ORGANO_ADMVO_1");
$mxajax->registerFunction("LLENAR_ORGANO_ADMVO_2");
$mxajax->registerFunction("LLENAR_ORGANO_ADMVO_3");
$mxajax->registerFunction("LLENAR_ORGANO_ADMVO_4");
$mxajax->registerFunction("LLENAR_ORGANO_ADMVO_5");

$mxajax->registerFunction("MOSTRAR_ORGANO_ADMVO");
$mxajax->registerFunction("LISTAR_FUNCION");
$mxajax->registerFunction("MOSTRAR_FUNCION");
$mxajax->registerFunction("MOSTRAR_OPCIONES_IMPRESION");

$mxajax->registerFunction("GUARDAR_NUEVO_ORGANO");
$mxajax->registerFunction("GUARDAR_MODIFICACION_ORGANO");
$mxajax->registerFunction("GUARDAR_NUEVO_FUNCION");
$mxajax->registerFunction("GUARDAR_MODIFICACION_FUNCION");

$mxajax->registerFunction("ELIMINAR_ORGANO");
$mxajax->registerFunction("ELIMINAR_FUNCION");

$mxajax->registerFunction("INICIALIZAR_DATOS_ORGANO");
$mxajax->registerFunction("INICIALIZAR_DATOS_FUNCION"); 	

$mxajax->registerFunction("CANCELAR_ORGANO");
$mxajax->registerFunction("CANCELAR_FUNCION");
$mxajax->registerFunction("HABILITAR_MODIFICAR_ORGANO");
$mxajax->registerFunction("HABILITAR_MODIFICAR_FUNCION");

$mxajax->registerFunction("MOSTRAR_PDF");

$mxajax->processRequest();
?>