<?php

class BASE_DATOS
{
	protected 	$mlink, $mdb;
	protected 	$mresponse;
    private 	$mservidor, $mnombre_usuario, $mpassword, $mbase_datos;

    public function __construct($mresponse)
     {	
		//Nombre del servidor
        $this->mservidor= 'localhost';
		//Nombre de usuario
        $this->mnombre_usuario= 'root';
		//Password
        $this->mpassword= 'edwuard';
		//Nombre de la Base de Datos
		$this->mbase_datos='directorio_telefonico';
		//Manejador de AJAX
		$this->mresponse=$mresponse;
     }
	public function ABRIR_BASE_DATOS_MYSQL($mtipo) //Establece la conexi�n a una Base de Datos MySql
	 {
		//Abrir la conexi�n al Sistema Manejador de Base de Datos (Mysql)
		$this->mlink= mysql_connect($this->mservidor, $this->mnombre_usuario, $this->mpassword);
		try {
				//Verificar si no se pudo establecer la conexi�n
				if ( ! $this->mlink)
				 {
					//Establecer los mensajes de error
					$exceptionstring = "Ocurrio un error al tratar de conectarse a la Base de Datos.";
					//Crear una nueva exepci�n (Error)
					throw new exception ($exceptionstring);
				 }
				else
				 {	
					//Seleccionar la Base de Datos
					$this->mdb= mysql_select_db($this->mbase_datos, $this->mlink);
					try {
							//Verificar si no se pudo establecer la Base de Datos
							if (! $this->mdb)
							 {
								//Establecer los mensajes de error
								$exceptionstring = "Ocurrio un error al tratar de seleccionar la Base de Datos.";
								//Crear una nueva exepci�n (Error)
								throw new exception ($exceptionstring);
							 }

							if ( $mtipo==0 )
							 {	mysql_query ("SET NAMES 'utf8'");	}

							return $this->mlink;
						}
					catch (Exception $e)
						{
							//Mostrar el mensaje de error
							$this->mresponse->alert($e->getmessage());
						}				
				 }
			} 
		catch (Exception $e)
			{
				//Mostrar el mensaje de error
				$this->mresponse->alert($e->getmessage());
			}
	 }
	public function CERRAR_BASE_DATOS_MYSQL() //Cierra la conexi�n de una Base de Datos MySql
	 {
		$this->__destruct(); 
	 }
	public function __destruct()
	 {
	 } 
}

class SELECT
{  
	protected $mcombo, $mresponse;

	public function __construct($mcombo, $mresponse)
	 {
		$this->mcombo=$mcombo;
		$this->mresponse=$mresponse;
	 }
		
	public function CLEAR_OPTIONS()
     {  
	 	$this->mresponse->script("LIMPIAR_OPCIONES('" . $this->mcombo . "');");
     }
    public function ADD_OPTION($moption_text, $moption_value)
     {  
	 	$this->mresponse->script("AGREGAR_OPCION('" . $this->mcombo . "', '". $moption_text . "', '". $moption_value . "');");	
     }
	public function ENABLED()
	 {
	 	$this->mresponse->script("HABILITAR('" . $this->mcombo . "');");
	 }
	public function DISABLED()
	 {
	 	$this->mresponse->script("INHABILITAR('" . $this->mcombo . "');");
	 }
	public function SELECT_OPTION($moption_text) 
	 {
	 	$this->mresponse->script("SELECCIONAR_OPCION('" . $this->mcombo . "', '" . $moption_text . "');");
	 }
	
	public function __destruct()
	 {
	 } 
}

class BUTTON
{
	protected $mcmd, $mid, $mvalue, $mresponse;
	
	public function __construct($mcmd, $mresponse)
	 {
		$this->mcmd=$mcmd;
		$this->mresponse=$mresponse;
	 }
 	public function SET_ID($mid)
	 {
		$this->mid=$mid;
		$this->mresponse->script("PONER_ID('" .  $this->mcmd . "','" . $this->mid . "');");	
	 }
	public function SET_VALUE($mvalue) 
	 {
		$this->mvalue= $mvalue;
	 	$this->mresponse->script("PONER_VALOR('" . $this->mcmd . "','" . $this->mvalue . "');");
		//$this->mresponse->assign("" . $this->mcmd . "","value","" . $this->mvalue . "");
	 }
	public function ENABLED()
	 {
	 	$this->mresponse->script("HABILITAR('" . $this->mcmd . "');");
	 }
	public function DISABLED()
	 {
	 	$this->mresponse->script("INHABILITAR('" . $this->mcmd . "');");
	 }
	  
	public function __destruct()
	 {
	 } 
}

class TEXT
{
	protected $mtxt, $mvalue, $mresponse;

	public function __construct($mtxt, $mresponse)
	 {
		$this->mtxt=$mtxt;
		$this->mresponse=$mresponse;
	 }
	
	public function SET_VALUE($mvalue) 
	 {
		$this->mvalue= $mvalue;
	 	$this->mresponse->script("PONER_VALOR('" . $this->mtxt . "','" . $this->mvalue . "');");
	 } 
	public function ENABLED()
	 {
	 	$this->mresponse->script("HABILITAR('" . $this->mtxt . "');");
	 } 
	public function DISABLED()
	 {
	 	$this->mresponse->script("INHABILITAR('" . $this->mtxt . "');");
	 }
	public function FOCUS()
	 {
	 	$this->mresponse->script("ENFOCAR('" . $this->mtxt . "');");
	 }
	 
	public function __destruct()
	 {
	 }
}

class INPUT_TEXT extends TEXT
{
	public function __construct($minput_txt, $mresponse)
	 {
	 	parent::__construct($minput_txt, $mresponse);
	 }

	public function __destruct()
	 {
		parent::__destruct();
	 } 
}

class TEXT_AREA extends TEXT
{
	
	public function __construct($mtxt_area, $mresponse)
	 {
		parent::__construct($mtxt_area, $mresponse);
	 }
	 
	public function __destruct()
	 {
		parent::__destruct();
	 } 
}

class CHECK_BOX
{
	protected $mchk, $mvalue, $mresponse;
	
	public function __construct($mchk, $mresponse)
	 {
		$this->mchk=$mchk;
		$this->mresponse=$mresponse;
	 }
	public function SET_VALUE($mvalue)
	 {
		$this->mvalue= $mvalue;
	 	$this->mresponse->script("PONER_VALOR('" . $this->mchk . "','" . $this->mvalue . "');");
	 }
	public function ENABLED()
	 {
	 	$this->mresponse->script("HABILITAR('" . $this->mchk . "');");
	 }
	public function DISABLED()
	 {
	 	$this->mresponse->script("INHABILITAR('" . $this->mchk . "');");
	 }
	public function SELECTED()
	 {
	 	$this->mresponse->script("SELECCIONAR('" . $this->mchk . "');");
		$this->SET_VALUE("1");
	 }
	public function UNSELECTED()
	 {
	 	$this->mresponse->script("DESELECCIONAR('" . $this->mchk . "');");
		$this->SET_VALUE("0");
	 }
	 
	public function __destruct()
	 {
	 }
}

class RADIO
{
	protected $mchk, $mresponse;
	
	public function __construct($mchk, $mresponse)
	 {
		$this->mchk=$mchk;
		$this->mresponse=$mresponse;
	 }

	public function ENABLED()
	 {
	 	$this->mresponse->script("HABILITAR('" . $this->mchk . "');");
	 }
	public function DISABLED()
	 {
	 	$this->mresponse->script("INHABILITAR('" . $this->mchk . "');");
	 }
	public function SELECTED()
	 {
	 	$this->mresponse->script("SELECCIONAR('" . $this->mchk . "');");
		//$this->SET_VALUE("1");
	 }
	public function UNSELECTED()
	 {
	 	$this->mresponse->script("DESELECCIONAR('" . $this->mchk . "');");
		//$this->SET_VALUE("0");
	 }
	 
	public function __destruct()
	 {
	 }
}

class WINDOW
{
	protected $mname, $mresponse;
	
	public function __construct($mname, $mresponse)
	 {
		$this->mname=$mname;
		$this->mresponse=$mresponse;
	 }
	public function SHOW()
	 {
	 	$this->mresponse->script("MOSTRAR_DIV('" . $this->mname . "');");
	 }
	public function HIDE()
	 {
	 	$this->mresponse->script("OCULTAR_DIV('" . $this->mname . "');");
	 } 
	
	public function __destruct()
	 {
	 }
}
?>