<?php

class INDEX extends xajaxResponse
{
	function LLENAR_ORGANO_ADMVO_1($cmb, $mtipo_llenado)
	 {	
		$mselect= new SELECT($cmb, $this);
		$mbase_datos= new BASE_DATOS($this);
		
		$msql="SELECT cve_organo_1, cmporgano_1 FROM c_organo_1 ORDER BY cmporgano_1";
		
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
	 			$mselect->CLEAR_OPTIONS();
				$mcont=1;
				while($mreg= mysql_fetch_array($mquery))
				 {	
					$mselect->ADD_OPTION(trim($mreg["cmporgano_1"]), trim($mreg["cve_organo_1"]));
					if ($mcont==1)
					 {	$morgano_admvo= array(trim($mreg["cve_organo_1"]));	}
				 }
				 
				if( $mtipo_llenado==0 )
				 {
					$this->script("LLENAR_ORGANO_ADMVO(2);");
					$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, 1);
				 }
			 }
			else
			 {
			 	$this->LIMPIAR_ORGANOS_ADMVOS(1);
				$this->script("xajax_CANCELAR_ORGANO(xajax.getFormValues('mDatos'));");				
				$this->script("xajax_CANCELAR_FUNCION(1);");
				$this->assign("mListado_Funcion","innerHTML","");
				$mcmd= new BUTTON("cmdImprimir", $this);
				$mcmd->DISABLED();
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_ORGANOS_ADMVOS(1);
		 	$this->alert("Ocurrio un error al tratar de llenar el Nivel Administrativo 1." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
	 }
	 
	function LLENAR_ORGANO_ADMVO_2($cmb, $morgano_admvo)
	 {	
		$mselect= new SELECT($cmb, $this);
		$mbase_datos= new BASE_DATOS($this);
		
		$msql ="SELECT cve_organo_2, cmporgano_2 ";
		$msql.="FROM c_organo_2 ";
		$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
		$msql.="ORDER BY cmporgano_2";

		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
				$this->LIMPIAR_ORGANOS_ADMVOS(2);
				while($mreg= mysql_fetch_array($mquery))
				 {	$mselect->ADD_OPTION(trim($mreg["cmporgano_2"]), trim($mreg["cve_organo_2"]));	}
			 }
			else
			 {
				$this->LIMPIAR_ORGANOS_ADMVOS(2);
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
			$this->LIMPIAR_ORGANOS_ADMVOS(2);
		 	$this->alert("Ocurrio un error al tratar de llenar el Nivel Administrativo 2." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 }
	 
	function LLENAR_ORGANO_ADMVO_3($cmb, $morgano_admvo, $mtipo_llenado)
	 {	
		$mselect= new SELECT($cmb, $this);
		$mbase_datos= new BASE_DATOS($this);
		
		$msql ="SELECT cve_organo_3, cmporgano_3 ";
		$msql.="FROM c_organo_3 ";
		$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
		$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
		$msql.="ORDER BY cmporgano_3";

		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
				$this->LIMPIAR_ORGANOS_ADMVOS(3);
				while($mreg= mysql_fetch_array($mquery))
				 {	$mselect->ADD_OPTION(trim($mreg["cmporgano_3"]), trim($mreg["cve_organo_3"]));	}
			 }
			else
			 {
			 	$this->LIMPIAR_ORGANOS_ADMVOS(3);
			 }
			 
			if ($mtipo_llenado==0)
			 {	$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, 2);	}
			 
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_ORGANOS_ADMVOS(3);
		 	$this->alert("Ocurrio un error al tratar de llenar el Nivel Administrativo 3." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
	 }
	 
	function LLENAR_ORGANO_ADMVO_4($cmb, $morgano_admvo, $mtipo_llenado)
	 {	
		$mselect= new SELECT($cmb, $this);
		$mbase_datos= new BASE_DATOS($this);
		
		$msql ="SELECT cve_organo_4, cmporgano_4 ";
		$msql.="FROM c_organo_4 ";
		$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
		$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
		$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
		$msql.="ORDER BY cmporgano_4";

		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
				$this->LIMPIAR_ORGANOS_ADMVOS(4);
				while($mreg= mysql_fetch_array($mquery))
				 {	$mselect->ADD_OPTION(trim($mreg["cmporgano_4"]), trim($mreg["cve_organo_4"]));	}
			 }
			else
			 {
			 	$this->LIMPIAR_ORGANOS_ADMVOS(4);
			 }
			if ($mtipo_llenado==0)
			 {	$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, 3);	}
			 
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_ORGANOS_ADMVOS(4);
		 	$this->alert("Ocurrio un error al tratar de llenar el Nivel Administrativo 4." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
	 }
	 	 
	function LLENAR_ORGANO_ADMVO_5($cmb, $morgano_admvo, $mtipo_llenado)
	 {	
		$mselect= new SELECT($cmb, $this);
		$mbase_datos= new BASE_DATOS($this);
		
		$msql ="SELECT cve_organo_5, cmporgano_5 ";
		$msql.="FROM c_organo_5 ";
		$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
		$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
		$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
		$msql.="AND cve_organo_4='" . $morgano_admvo[3] . "' ";
		$msql.="ORDER BY cmporgano_5";

		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
				$this->LIMPIAR_ORGANOS_ADMVOS(5);
				while($mreg= mysql_fetch_array($mquery))
				 {	$mselect->ADD_OPTION(trim($mreg["cmporgano_5"]), trim($mreg["cve_organo_5"]));	}
			 }
			else
			 {
			 	$this->LIMPIAR_ORGANOS_ADMVOS(5);
			 }
			
			if ($mtipo_llenado==0 )
			 {	$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, 4);	}
			
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_ORGANOS_ADMVOS(5);
		 	$this->alert("Ocurrio un error al tratar de llenar el Nivel Administrativo 5." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
	 }
	 
	function LLENAR_FUNCION($cmb)
	 {	
		$mselect= new SELECT($cmb, $this);
		$mbase_datos= new BASE_DATOS($this);
		
		$msql ="SELECT cve_tipo_funcion, cmptipo_funcion ";
		$msql.="FROM c_tipo_funcion ";
		$msql.="ORDER BY cmptipo_funcion";

		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
	 			$mselect->CLEAR_OPTIONS();
				while($mreg= mysql_fetch_array($mquery))
				 {	$mselect->ADD_OPTION(trim($mreg["cmptipo_funcion"]), trim($mreg["cve_tipo_funcion"]));	}
			 }
			else
			 {
				$this->alert("No existe(n) funcion(es) para asignar.");
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->alert("Ocurrio un error al tratar de llenar la(s) funcion(es)." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
	 }
	 
	function GUARDAR_NUEVO_ORGANO($mform_organo_admvo, $mform_datos)
	 {
	 	//$this->alert( print_r($mform_organo_admvo, true));
		$mnivel=$this->REGRESA_NIVEL_ORGANO_ALTA($mform_organo_admvo);
		switch($mnivel)
		 {
		 	case 1: $mcve_organo_admvo=$this->REGRESA_CVE_MAX_ORGANO($mform_organo_admvo, 1);
					$minsert ="INSERT INTO c_organo_1(cve_organo_1, cmporgano_1 ";
					$mvalues ="VALUES('" . $mcve_organo_admvo . "'" ;
					break;
		 	case 2: $mcve_organo_admvo=$this->REGRESA_CVE_MAX_ORGANO($mform_organo_admvo, 2);
					$minsert ="INSERT INTO c_organo_2(cve_organo_1, cve_organo_2, cmporgano_2 ";
					$mvalues ="VALUES('" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					$mvalues.=", '" . $mcve_organo_admvo . "'" ;
					break;
		 	case 3: $mcve_organo_admvo=$this->REGRESA_CVE_MAX_ORGANO($mform_organo_admvo, 3);
					$minsert ="INSERT INTO c_organo_3(cve_organo_1, cve_organo_2, cve_organo_3, cmporgano_3 ";
					$mvalues ="VALUES('" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$mvalues.=", '" . $mcve_organo_admvo . "'" ;
					break;
		 	case 4: $mcve_organo_admvo=$this->REGRESA_CVE_MAX_ORGANO($mform_organo_admvo, 4);
					$minsert ="INSERT INTO c_organo_4(cve_organo_1, cve_organo_2, cve_organo_3, cve_organo_4, cmporgano_4 ";
					$mvalues ="VALUES('" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					$mvalues.=", '" . $mcve_organo_admvo . "'" ;
					break;
		 	case 5: $mcve_organo_admvo=$this->REGRESA_CVE_MAX_ORGANO($mform_organo_admvo, 5);
					$minsert ="INSERT INTO c_organo_5(cve_organo_1, cve_organo_2, cve_organo_3, cve_organo_4,  cve_organo_5";
					$minsert.=", cmporgano_5 ";
					$mvalues ="VALUES('" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "'";
					$mvalues.=", '" . $mcve_organo_admvo . "'" ;
					break;
		 }
		$minsert.=", cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular, cmptitulo_titular";
		$minsert.=", cmptelefono_titular, cmpextension_titular, cmpcelular_titular, cmpfax_titular, cmpe_mail_titular)";
		$mvalues.=", '" . trim($mform_datos["txtOrgano"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtApellido_Paterno_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtApellido_Materno_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtNombre_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtTitulo_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtTelefono_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtExtension_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtCelular_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtFax_Titular"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtE_Mail_Titular"]) . "')";

		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if ( $mquery=mysql_query($minsert . " " . $mvalues, $mconexion_mysql) )
		 {
			$mnum_rows=mysql_affected_rows($mconexion_mysql);
			if ( $mnum_rows==1 )
			 {
			 	$this->alert("El Organo Administrativo se ha guardado satisfactoriamente.");			
				switch ($mnivel)
				 {
					case 1:	$this->LLENAR_ORGANO_ADMVO_1("cmbOrgano_Admvo_1", 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_1", $this);
							$mcmb_organo_admvo->SELECT_OPTION($mcve_organo_admvo);
							break;
					case 2:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$this->LLENAR_ORGANO_ADMVO_2("cmbOrgano_Admvo_2",  $morgano_admvo);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_2", $this);
							$mcmb_organo_admvo->SELECT_OPTION($mcve_organo_admvo);
							break;
					case 3:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
							$this->LLENAR_ORGANO_ADMVO_3("cmbOrgano_Admvo_3",  $morgano_admvo, 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_3", $this);
							$mcmb_organo_admvo->SELECT_OPTION($mcve_organo_admvo);
							break;
					case 4:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
							$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
							$this->LLENAR_ORGANO_ADMVO_4("cmbOrgano_Admvo_4",  $morgano_admvo, 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_4", $this);
							$mcmb_organo_admvo->SELECT_OPTION($mcve_organo_admvo);
							break;
					case 5:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
							$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
							$morgano_admvo[3]=trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
							$morgano_admvo[4]=trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);
							$this->LLENAR_ORGANO_ADMVO_5("cmbOrgano_Admvo_5",  $morgano_admvo, 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_5", $this);
							$mcmb_organo_admvo->SELECT_OPTION($mcve_organo_admvo);
							break;
				 }
				$this->INHABILITAR_TEXTOS_ORGANO();
				$mcmd= new BUTTON("cmdImprimir", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdNuevo_Guardar_Organo", $this);
				$mcmd->SET_VALUE("Nuevo");
				$mcmd->ENABLED();
				$mtxt= new TEXT("txt_Tipo_Guardar_Organo", $this);
				$mtxt->SET_VALUE("0");
				$mcmd= new BUTTON("cmdModificar_Organo", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdEliminar_Organo", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdCancelar_Organo", $this);
				$mcmd->DISABLED();
				$mcmd= new BUTTON("cmdNuevo_Guardar_Funcion", $this);
				$mcmd->SET_VALUE("Nuevo");
				$mcmd->ENABLED();
				$mtxt= new TEXT("txt_Tipo_Guardar_Funcion", $this);
				$mtxt->SET_VALUE("0");
			 }
			else
			 {
				$this->alert("Ocurrio un error al tratar de guardar el Organo Administrativo." . chr(13) . "Intente de nuevo.");
			 }
		 }
		else
		 {
		 	$this->alert("Ocurrio un error al tratar de guardar el Organo Administrativo." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 }
	
	function GUARDAR_MODIFICACION_ORGANO($mform_organo_admvo, $mform_datos)
	 {
	 	$mnivel=$this->REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo);
		switch($mnivel)
		 {
		 	case 1: $msql ="UPDATE c_organo_1 ";
					$msql.="SET cmporgano_1='" . trim($mform_datos["txtOrgano"]) . "'";
					$msql_condicion="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					break;
		 	case 2: $msql ="UPDATE c_organo_2 ";
					$msql.="SET cmporgano_2='" . trim($mform_datos["txtOrgano"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					break;
		 	case 3: $msql ="UPDATE c_organo_3 ";
					$msql.="SET cmporgano_3='" . trim($mform_datos["txtOrgano"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					break;
		 	case 4: $msql ="UPDATE c_organo_4 ";
					$msql.="SET cmporgano_4='" . trim($mform_datos["txtOrgano"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql_condicion.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "'";
					break;
		 	case 5: $msql ="UPDATE c_organo_5 ";
					$msql.="SET cmporgano_5='" . trim($mform_datos["txtOrgano"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql_condicion.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql_condicion.="AND cve_organo_5='" . trim($mform_organo_admvo["cmbOrgano_Admvo_5"]) . "' ";
					break;
		 }
		$msql.=", cmpapellido_paterno_titular='" . trim($mform_datos["txtApellido_Paterno_Titular"]) . "'";
		$msql.=", cmpapellido_materno_titular='" . trim($mform_datos["txtApellido_Materno_Titular"]) . "'";
		$msql.=", cmpnombre_titular='" . trim($mform_datos["txtNombre_Titular"]) . "'";
		$msql.=", cmptitulo_titular='" . trim($mform_datos["txtTitulo_Titular"]) . "'";
		$msql.=", cmptelefono_titular='" . trim($mform_datos["txtTelefono_Titular"]) . "'";
		$msql.=", cmpextension_titular='" . trim($mform_datos["txtExtension_Titular"]) . "'";
		$msql.=", cmpcelular_titular='" . trim($mform_datos["txtCelular_Titular"]) . "'";
		$msql.=", cmpfax_titular='" . trim($mform_datos["txtFax_Titular"]) . "'";
		$msql.=", cmpe_mail_titular='" . trim($mform_datos["txtE_Mail_Titular"]) . "' ";
		$msql.=$msql_condicion;
		
		$mbase_datos= new BASE_DATOS($this);		
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if ( $mquery=mysql_query($msql, $mconexion_mysql) )
		 {
			$mnum_rows= mysql_affected_rows($mconexion_mysql);
			if ( $mnum_rows ==1 )
			 {	
			 	$this->alert("El Organo Administrativo se ha modificado satisfactoriamente.");
				switch ($mnivel)
				 {
					case 1:	$this->LLENAR_ORGANO_ADMVO_1("cmbOrgano_Admvo_1", 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_1", $this);
							$mcmb_organo_admvo->SELECT_OPTION(trim($mform_organo_admvo["cmbOrgano_Admvo_1"]));
							break;
					case 2:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$this->LLENAR_ORGANO_ADMVO_2("cmbOrgano_Admvo_2",  $morgano_admvo);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_2", $this);
							$mcmb_organo_admvo->SELECT_OPTION(trim($mform_organo_admvo["cmbOrgano_Admvo_2"]));
							$this->script("LLENAR_ORGANO_ADMVO(3);");
							break;
					case 3:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
							$this->LLENAR_ORGANO_ADMVO_3("cmbOrgano_Admvo_3",  $morgano_admvo, 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_3", $this);
							$mcmb_organo_admvo->SELECT_OPTION(trim($mform_organo_admvo["cmbOrgano_Admvo_3"]));
							$this->script("LLENAR_ORGANO_ADMVO(4);");
							break;
					case 4:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
							$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
							$this->LLENAR_ORGANO_ADMVO_4("cmbOrgano_Admvo_4",  $morgano_admvo, 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_4", $this);
							$mcmb_organo_admvo->SELECT_OPTION(trim($mform_organo_admvo["cmbOrgano_Admvo_4"]));
							$this->script("LLENAR_ORGANO_ADMVO(5);");
							break;
					case 5:	$morgano_admvo=array();
							$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
							$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
							$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
							$morgano_admvo[3]=trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
							$morgano_admvo[4]=trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);
							$this->LLENAR_ORGANO_ADMVO_5("cmbOrgano_Admvo_5",  $morgano_admvo, 1);
							$mcmb_organo_admvo= new SELECT("cmbOrgano_Admvo_5", $this);
							$mcmb_organo_admvo->SELECT_OPTION(trim($mform_organo_admvo["cmbOrgano_Admvo_5"]));
							$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo,5);
							break;
				 }
			 }
			else
			 {	
				$this->alert("Ningun dato se ha modificado del Organo Administrativo.");
			 }
			
			$this->INHABILITAR_TEXTOS_ORGANO();  
			 
			$mcmd= new BUTTON("cmdNuevo_Guardar_Organo", $this);
			$mcmd->SET_VALUE("Nuevo");
			$mcmd->ENABLED();
			$mtxt= new TEXT("txt_Tipo_Guardar_Organo", $this);
			$mtxt->SET_VALUE("0"); 
 			
			$mcmd= new BUTTON("cmdCancelar_Organo", $this);
			$mcmd->DISABLED();
		 }
		else
		 {
			$this->alert("Ocurrio un error al tratar de guardar la modificacion del Organo Administrativo." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 }
	
	function ELIMINAR_ORGANO($mform_organo_admvo, $mnivel, $mnum)
	 {
	 	$morgano_admvo=array();
		switch($mnivel)
		 {
		 	case 1:	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
			
					$msql_funcion ="DELETE FROM p_funcion_1 ";
					$msql_funcion.="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";

					$msql_organo ="DELETE FROM c_organo_1 ";
					$msql_organo.="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
					
					$this->ELIMINAR_ORGANO($mform_organo_admvo, 2, $mnum+1);
					break;
		 	case 2:	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					
					switch ($mnum)
					 {
					 	case 0: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "'";
								break;
						case 1: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
								break;
					 }
					$msql_funcion ="DELETE FROM p_funcion_2 ";
					$msql_funcion.=$msql_condicion;
					
					$msql_organo ="DELETE FROM c_organo_2 ";
					$msql_organo.=$msql_condicion;
					
					$this->ELIMINAR_ORGANO($mform_organo_admvo, 3, $mnum+1);
					break;
		 	case 3:	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					
					switch ($mnum)
					 {
					 	case 0: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
								$msql_condicion.="AND cve_organo_3='" . $morgano_admvo[2] . "'";
								break;
						case 1: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "'";
								break;
						case 2: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
								break;
					 }
					$msql_funcion ="DELETE FROM p_funcion_3 ";
					$msql_funcion.=$msql_condicion;
					
					$msql_organo ="DELETE FROM c_organo_3 ";
					$msql_organo.=$msql_condicion;
					
					$this->ELIMINAR_ORGANO($mform_organo_admvo, 4, $mnum+1);
					break;
		 	case 4:	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]=trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					
					switch ($mnum)
					 {
					 	case 0: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
								$msql_condicion.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
								$msql_condicion.="AND cve_organo_4='" . $morgano_admvo[3] . "'";
								break;
						case 1: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
								$msql_condicion.="AND cve_organo_3='" . $morgano_admvo[2] . "'";
								break;
						case 2: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "'";
								break;
						case 3: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
								break;
					 }
					$msql_funcion ="DELETE FROM p_funcion_4 ";
					$msql_funcion.=$msql_condicion;
					
					$msql_organo ="DELETE FROM c_organo_4 ";
					$msql_organo.=$msql_condicion;
					
					$this->ELIMINAR_ORGANO($mform_organo_admvo, 5, $mnum+1);
					break;
			case 5:	$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]=trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]=trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]=trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					$morgano_admvo[4]=trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);

					switch ($mnum)
					 {
					 	case 0: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
								$msql_condicion.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
								$msql_condicion.="AND cve_organo_4='" . $morgano_admvo[3] . "' ";
								$msql_condicion.="AND cve_organo_5='" . $morgano_admvo[4] . "'";
								break;
						case 1: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
								$msql_condicion.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
								$msql_condicion.="AND cve_organo_4='" . $morgano_admvo[3] . "'";
								break;
						case 2: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
								$msql_condicion.="AND cve_organo_3='" . $morgano_admvo[2] . "'";
								break;
						case 3: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
								$msql_condicion.="AND cve_organo_2='" . $morgano_admvo[1] . "'";
								break;
						case 4: $msql_condicion ="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
								break;
					 }
					$msql_funcion ="DELETE FROM p_funcion_5 ";
					$msql_funcion.=$msql_condicion;
					
					$msql_organo ="DELETE FROM c_organo_5 ";
					$msql_organo.=$msql_condicion;
					break;
		 }
		 
	 	$mbase_datos= new BASE_DATOS($this); 
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		$mquery_funcion=mysql_query($msql_funcion, $mconexion_mysql);
		if ( $mquery_organo=mysql_query($msql_organo, $mconexion_mysql) )
		 {
		 	if( $mnum == 0 )
			 {
				$mnum_rows= mysql_affected_rows($mconexion_mysql);
				if ( $mnum_rows == 1 )
				 {	
					$this->alert("El Organo Adminstrativo se ha eliminado satisfactoriamente."); 
					switch ($mnivel)
					 {
						case 1:	$this->LLENAR_ORGANO_ADMVO_1("cmbOrgano_Admvo_1", 0);
								break;
						case 2:	$this->LLENAR_ORGANO_ADMVO_2("cmbOrgano_Admvo_2",  $morgano_admvo);
								$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, 1);
								break;								
						case 3:	$this->LLENAR_ORGANO_ADMVO_3("cmbOrgano_Admvo_3",  $morgano_admvo, 0);
								break;
						case 4:	$this->LLENAR_ORGANO_ADMVO_4("cmbOrgano_Admvo_4",  $morgano_admvo, 0);
								break;
						case 5:	$this->LLENAR_ORGANO_ADMVO_5("cmbOrgano_Admvo_5",  $morgano_admvo, 0);
								break;
					 }
				 }
				else
				 {	
					$this->alert("Ocurrio un error al tratar de eliminar el Organo Administrativo." . chr(13) . "Intente de nuevo."); 
				 }
			 }
		 }
		else
		 {
		 	$this->alert("Ocurrio un error al tratar de eliminar el Organo Administrativo." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();		
	 }
		
	function GUARDAR_NUEVO_FUNCION($mform_organo_admvo, $mform_datos)
	 {
		$mnivel=$this->REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo);
		$morgano_admvo= array();
		switch($mnivel)
		 {
		 	case 1: $mcve_funcion=$this->REGRESA_CVE_MAX_FUNCION($mform_organo_admvo, 1);
					$minsert ="INSERT INTO p_funcion_1(cve_funcion_1, cve_organo_1";
					$mvalues ="VALUES('" . $mcve_funcion . "', '" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'" ;
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					break;
		 	case 2: $mcve_funcion=$this->REGRESA_CVE_MAX_FUNCION($mform_organo_admvo, 2);
					$minsert ="INSERT INTO p_funcion_2(cve_funcion_2, cve_organo_1, cve_organo_2";
					$mvalues ="VALUES('" . $mcve_funcion . "', '" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'" ;
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'" ;
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					break;
		 	case 3: $mcve_funcion=$this->REGRESA_CVE_MAX_FUNCION($mform_organo_admvo, 3);
					$minsert ="INSERT INTO p_funcion_3(cve_funcion_3, cve_organo_1, cve_organo_2, cve_organo_3";
					$mvalues ="VALUES('" . $mcve_funcion . "', '" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'" ;
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					break;
		 	case 4: $mcve_funcion=$this->REGRESA_CVE_MAX_FUNCION($mform_organo_admvo, 4);
					$minsert ="INSERT INTO p_funcion_4(cve_funcion_4, cve_organo_1, cve_organo_2, cve_organo_3, cve_organo_4";
					$mvalues ="VALUES('" . $mcve_funcion . "', '" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'" ;
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]= trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					break;
		 	case 5: $mcve_funcion=$this->REGRESA_CVE_MAX_FUNCION($mform_organo_admvo, 5);
					$minsert ="INSERT INTO p_funcion_5(cve_funcion_5, cve_organo_1, cve_organo_2, cve_organo_3, cve_organo_4";
					$minsert.=", cve_organo_5";
					$mvalues ="VALUES('" . $mcve_funcion . "', '" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'" ;
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "'";
					$mvalues.=", '" . trim($mform_organo_admvo["cmbOrgano_Admvo_5"]) . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]= trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					$morgano_admvo[4]= trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);
					break;
		 }
		$minsert.=", cmpapellido_paterno, cmpapellido_materno, cmpnombre, cve_tipo_funcion, cmptitulo, cmptelefono, cmpextension";
		$minsert.=", cmpcelular, cmpfax, cmpe_mail)";
		$mvalues.=", '" . trim($mform_datos["txtApellido_Paterno_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtApellido_Materno_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtNombre_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["cmbFuncion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtTitulo_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtTelefono_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtExtension_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtCelular_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtFax_Funcion"]) . "'";
		$mvalues.=", '" . trim($mform_datos["txtE_Mail_Funcion"]) . "')";

		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if ( $mquery=mysql_query($minsert . " " . $mvalues, $mconexion_mysql) )
		 {
			$mnum_rows=mysql_affected_rows($mconexion_mysql);
			if ( $mnum_rows==1 )
			 {
			 	$this->alert("La Funcion se ha guardado satisfactoriamente.");
				
				$mtxt= new INPUT_TEXT("txt_Cve_Funcion", $this);			$mtxt->SET_VALUE($mcve_funcion);
				$this->INHABILITAR_TEXTOS_FUNCION();
				$mcmd= new BUTTON("cmdNuevo_Guardar_Funcion", $this);
				$mcmd->SET_VALUE("Nuevo");
				$mcmd->ENABLED();
				$mtxt= new TEXT("txt_Tipo_Guardar_Funcion", $this);
				$mtxt->SET_VALUE("0");
				$mcmd= new BUTTON("cmdModificar_Funcion", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdEliminar_Funcion", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdCancelar_Funcion", $this);
				$mcmd->DISABLED();
				
				$this->LISTAR_FUNCION($morgano_admvo, 0, $mnivel);
			 }
			else
			 {
				$this->alert("Ocurrio un error al tratar de guardar la Funcion." . chr(13) . "Intente de nuevo.");
			 }
		 }
		else
		 {
		 	$this->alert("Ocurrio un error al tratar de guardar la Funcion." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 }
	 
	function GUARDAR_MODIFICACION_FUNCION($mform_organo_admvo, $mform_datos, $mcve_funcion)
	 {
	 	$mnivel=$this->REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo);
		$morgano_admvo= array();
		switch($mnivel)
		 {
		 	case 1: $msql ="UPDATE p_funcion_1 ";
					$msql.="SET cmpapellido_paterno='" . trim($mform_datos["txtApellido_Paterno_Funcion"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_funcion_1='" . $mcve_funcion . "'";
					$morgano_admvo[0]=trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					break;
		 	case 2: $msql ="UPDATE p_funcion_2 ";
					$msql.="SET cmpapellido_paterno='" . trim($mform_datos["txtApellido_Paterno_Funcion"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_funcion_2='" . $mcve_funcion . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					break;
		 	case 3: $msql ="UPDATE p_funcion_3 ";
					$msql.="SET cmpapellido_paterno='" . trim($mform_datos["txtApellido_Paterno_Funcion"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql_condicion.="AND cve_funcion_3='" . $mcve_funcion . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					break;
		 	case 4: $msql ="UPDATE p_funcion_4 ";
					$msql.="SET cmpapellido_paterno='" . trim($mform_datos["txtApellido_Paterno_Funcion"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql_condicion.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql_condicion.="AND cve_funcion_4='" . $mcve_funcion . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]= trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					break;
		 	case 5: $msql ="UPDATE p_funcion_5 ";
					$msql.="SET cmpapellido_paterno='" . trim($mform_datos["txtApellido_Paterno_Funcion"]) . "'";
					$msql_condicion ="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql_condicion.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql_condicion.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql_condicion.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql_condicion.="AND cve_organo_5='" . trim($mform_organo_admvo["cmbOrgano_Admvo_5"]) . "' ";
					$msql_condicion.="AND cve_funcion_5='" . $mcve_funcion . "'";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]= trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					$morgano_admvo[4]= trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);
					break;
		 }		
		$msql.=", cmpapellido_materno='" . trim($mform_datos["txtApellido_Materno_Funcion"]) . "'";
		$msql.=", cmpnombre='" . trim($mform_datos["txtNombre_Funcion"]) . "'";
		$msql.=", cve_tipo_funcion='" . trim($mform_datos["cmbFuncion"]) . "'";
		$msql.=", cmptitulo='" . trim($mform_datos["txtTitulo_Funcion"]) . "'";
		$msql.=", cmptelefono='" . trim($mform_datos["txtTelefono_Funcion"]) . "'";
		$msql.=", cmpextension='" . trim($mform_datos["txtExtension_Funcion"]) . "'";
		$msql.=", cmpcelular='" . trim($mform_datos["txtCelular_Funcion"]) . "'";
		$msql.=", cmpfax='" . trim($mform_datos["txtFax_Funcion"]) . "'";
		$msql.=", cmpe_mail='" . trim($mform_datos["txtE_Mail_Funcion"]) . "' ";
		$msql.=$msql_condicion;
			
		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if ( $mquery=mysql_query($msql, $mconexion_mysql) )
		 {
			$mnum_rows= mysql_affected_rows($mconexion_mysql);
			if ( $mnum_rows ==1 )
			 {	$this->alert("La Funcion se ha modificado satisfactoriamente.");	}
			else
			 {	$this->alert("Ningun dato se ha modificado de la Funcion.");	}
			
			$this->INHABILITAR_TEXTOS_FUNCION();  
			 
			$mcmd= new BUTTON("cmdNuevo_Guardar_Funcion", $this);
			$mcmd->SET_VALUE("Nuevo");
			$mcmd->ENABLED();
			$mtxt= new TEXT("txt_Tipo_Guardar_Funcion", $this);
			$mtxt->SET_VALUE("0"); 
 			
			$mcmd= new BUTTON("cmdCancelar_Funcion", $this);
			$mcmd->DISABLED();
			
			$this->LISTAR_FUNCION($morgano_admvo, 0, $mnivel);
		 }
		else
		 {
			$this->alert("Ocurrio un error al tratar de guardar la modificacion de la Funcion." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 } 

	function ELIMINAR_FUNCION($mform_organo_admvo, $mcve_funcion)
	 {
		$mnivel=$this->REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo);
		$morgano_admvo= array();
		switch($mnivel)
		 {
			case 1: $msql ="DELETE FROM p_funcion_1 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_funcion_1='" . $mcve_funcion . "' ";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					break;
			case 2: $msql ="DELETE FROM p_funcion_2 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_funcion_2='" . $mcve_funcion . "' ";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					break;
			case 3: $msql ="DELETE FROM p_funcion_3 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_funcion_3='" . $mcve_funcion . "' ";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					break;
			case 4: $msql ="DELETE FROM p_funcion_4 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql.="AND cve_funcion_4='" . $mcve_funcion . "' ";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]= trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					break;
			case 5: $msql ="DELETE FROM p_funcion_5 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql.="AND cve_organo_5='" . trim($mform_organo_admvo["cmbOrgano_Admvo_5"]) . "' ";
					$msql.="AND cve_funcion_5='" . $mcve_funcion . "' ";
					$morgano_admvo[0]= trim($mform_organo_admvo["cmbOrgano_Admvo_1"]);
					$morgano_admvo[1]= trim($mform_organo_admvo["cmbOrgano_Admvo_2"]);
					$morgano_admvo[2]= trim($mform_organo_admvo["cmbOrgano_Admvo_3"]);
					$morgano_admvo[3]= trim($mform_organo_admvo["cmbOrgano_Admvo_4"]);
					$morgano_admvo[4]= trim($mform_organo_admvo["cmbOrgano_Admvo_5"]);
					break;
		 }
			
		$mbase_datos= new BASE_DATOS($this); 
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if ( $mquery=mysql_query($msql, $mconexion_mysql) )
		 {
			$mnum_rows= mysql_affected_rows($mconexion_mysql);
			if ( $mnum_rows == 1 )
			 {	
				$this->LISTAR_FUNCION($morgano_admvo, 0, $mnivel);
				$this->script("xajax_CANCELAR_FUNCION(0);");
				$this->alert("La Funcion se ha eliminado satisfactoriamente.");
			 }
			else
			 {	
				$this->alert("Ocurrio un error al tratar de eliminar la Funcion." . chr(13) . "Intente de nuevo."); 
			 }
		 }
		else
		 {
		 	$this->alert("Ocurrio un error al tratar de eliminar la Funcion." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 }
	 
	function MOSTRAR_ORGANO_ADMVO($morgano_admvo, $mnivel)
	 {
	 	switch($mnivel)
		 {
			case 1: $msql ="SELECT cmporgano_1, cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular";
					$msql.=", cmptitulo_titular, cmptelefono_titular, cmpextension_titular, cmpcelular_titular, cmpfax_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_1 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
					break;
			case 2: $msql ="SELECT cmporgano_2, cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular";
					$msql.=", cmptitulo_titular, cmptelefono_titular, cmpextension_titular, cmpcelular_titular, cmpfax_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_2 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "'";
					break;
			case 3: $msql ="SELECT cmporgano_3, cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular";
					$msql.=", cmptitulo_titular, cmptelefono_titular, cmpextension_titular, cmpcelular_titular, cmpfax_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_3 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "'";
					break;
			case 4: $msql ="SELECT cmporgano_4, cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular";
					$msql.=", cmptitulo_titular, cmptelefono_titular, cmpextension_titular, cmpcelular_titular, cmpfax_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_4 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql.="AND cve_organo_4='" . $morgano_admvo[3] . "'";
					break;
			case 5: $msql ="SELECT cmporgano_5, cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular";
					$msql.=", cmptitulo_titular, cmptelefono_titular, cmpextension_titular, cmpcelular_titular, cmpfax_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_5 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql.="AND cve_organo_4='" . $morgano_admvo[3] . "' ";
					$msql.="AND cve_organo_5='" . $morgano_admvo[4] . "'";
					break;
		 }

		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
			$mnum_rows= mysql_num_rows($mquery);
			
			if($mnum_rows > 0 )
			 {
				$this->LIMPIAR_TEXTOS_ORGANO();
				$this->INHABILITAR_TEXTOS_ORGANO();
				
				$mreg= mysql_fetch_array($mquery);
				$mtxt= new INPUT_TEXT("txtOrgano", $this);						$mtxt->SET_VALUE(trim($mreg[0]));
				$mtxt= new INPUT_TEXT("txtApellido_Paterno_Titular", $this);	$mtxt->SET_VALUE(trim($mreg["cmpapellido_paterno_titular"]));
				$mtxt= new INPUT_TEXT("txtApellido_Materno_Titular", $this);	$mtxt->SET_VALUE(trim($mreg["cmpapellido_materno_titular"]));
				$mtxt= new INPUT_TEXT("txtNombre_Titular", $this);				$mtxt->SET_VALUE(trim($mreg["cmpnombre_titular"]));
				$mtxt= new INPUT_TEXT("txtTitulo_Titular", $this);				$mtxt->SET_VALUE(trim($mreg["cmptitulo_titular"]));
				$mtxt= new INPUT_TEXT("txtTelefono_Titular", $this);			$mtxt->SET_VALUE(trim($mreg["cmptelefono_titular"]));
				$mtxt= new INPUT_TEXT("txtExtension_Titular", $this);			$mtxt->SET_VALUE(trim($mreg["cmpextension_titular"]));
				$mtxt= new INPUT_TEXT("txtCelular_Titular", $this);				$mtxt->SET_VALUE(trim($mreg["cmpcelular_titular"]));
				$mtxt= new INPUT_TEXT("txtFax_Titular", $this);					$mtxt->SET_VALUE(trim($mreg["cmpfax_titular"]));
				$mtxt= new INPUT_TEXT("txtE_Mail_Titular", $this);				$mtxt->SET_VALUE(trim($mreg["cmpe_mail_titular"]));
				
				$mcmd= new BUTTON("cmdImprimir", $this);
				$mcmd->ENABLED();
				
				$mcmd= new BUTTON("cmdNuevo_Guardar_Organo", $this);
				$mcmd->SET_VALUE("Nuevo");
				$mcmd->ENABLED();
				$mtxt= new TEXT("txt_Tipo_Guardar_Organo", $this);
				$mtxt->SET_VALUE("0");
				
				$mcmd= new BUTTON("cmdModificar_Organo", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdEliminar_Organo", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdCancelar_Organo", $this);
				$mcmd->DISABLED();
				
				$this->script("xajax_CANCELAR_FUNCION(0);");
				
				$this->LISTAR_FUNCION($morgano_admvo, 0, $mnivel);
			 }
			else
			 {
				$this->LIMPIAR_TEXTOS_ORGANO();
				if ( $mnivel>1 )
				{
					$mnivel--;
					$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, $mnivel);
				}
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_TEXTOS_ORGANO();
			$this->alert("Ocurrio un error al mostrar los datos del Organo Administrativo." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 } 
	
	function LISTAR_FUNCION($morgano_admvo, $mpg, $mnivel)
	 {
		$mcantidad=4; // cantidad de resultados por p�gina
		$minicial = $mpg * $mcantidad;
		
		switch($mnivel)
		 {
			case 1:	$msql_datos ="SELECT p_funcion_1.cve_funcion_1, p_funcion_1.cmptitulo, p_funcion_1.cmpapellido_paterno";
					$msql_datos.=", p_funcion_1.cmpapellido_materno, p_funcion_1.cmpnombre, c_tipo_funcion.cmptipo_funcion ";
					$msql_datos.="FROM p_funcion_1 ";
					$msql_datos.="INNER JOIN c_tipo_funcion ON p_funcion_1.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql_datos.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql_datos.="ORDER BY cmpapellido_paterno, cmpapellido_materno, cmpnombre LIMIT $minicial, $mcantidad";

					$msql ="SELECT cve_funcion_1 ";
					$msql.="FROM p_funcion_1 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "'";
					break;
			case 2:	$msql_datos ="SELECT p_funcion_2.cve_funcion_2, p_funcion_2.cmptitulo, p_funcion_2.cmpapellido_paterno";
					$msql_datos.=", p_funcion_2.cmpapellido_materno, p_funcion_2.cmpnombre, c_tipo_funcion.cmptipo_funcion ";
					$msql_datos.="FROM p_funcion_2 ";
					$msql_datos.="INNER JOIN c_tipo_funcion ON p_funcion_2.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql_datos.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql_datos.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql_datos.="ORDER BY cmpapellido_paterno, cmpapellido_materno, cmpnombre LIMIT $minicial, $mcantidad";

					$msql ="SELECT cve_funcion_2 ";
					$msql.="FROM p_funcion_2 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "'";
					break;			
			case 3:	$msql_datos ="SELECT p_funcion_3.cve_funcion_3, p_funcion_3.cmptitulo, p_funcion_3.cmpapellido_paterno";
					$msql_datos.=", p_funcion_3.cmpapellido_materno, p_funcion_3.cmpnombre, c_tipo_funcion.cmptipo_funcion ";
					$msql_datos.="FROM p_funcion_3 ";
					$msql_datos.="INNER JOIN c_tipo_funcion ON p_funcion_3.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql_datos.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql_datos.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql_datos.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql_datos.="ORDER BY cmpapellido_paterno, cmpapellido_materno, cmpnombre LIMIT $minicial, $mcantidad";

					$msql ="SELECT cve_funcion_3 ";
					$msql.="FROM p_funcion_3 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "'";
					break;
			case 4:	$msql_datos ="SELECT p_funcion_4.cve_funcion_4, p_funcion_4.cmptitulo, p_funcion_4.cmpapellido_paterno";
					$msql_datos.=", p_funcion_4.cmpapellido_materno, p_funcion_4.cmpnombre, c_tipo_funcion.cmptipo_funcion ";
					$msql_datos.="FROM p_funcion_4 ";
					$msql_datos.="INNER JOIN c_tipo_funcion ON p_funcion_4.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql_datos.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql_datos.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql_datos.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql_datos.="AND cve_organo_4='" . $morgano_admvo[3] . "' ";
					$msql_datos.="ORDER BY cmpapellido_paterno, cmpapellido_materno, cmpnombre LIMIT $minicial, $mcantidad";

					$msql ="SELECT cve_funcion_4 ";
					$msql.="FROM p_funcion_4 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql.="AND cve_organo_4='" . $morgano_admvo[3] . "'";
					break;
			case 5:	$msql_datos ="SELECT p_funcion_5.cve_funcion_5, p_funcion_5.cmptitulo, p_funcion_5.cmpapellido_paterno";
					$msql_datos.=", p_funcion_5.cmpapellido_materno, p_funcion_5.cmpnombre, c_tipo_funcion.cmptipo_funcion ";
					$msql_datos.="FROM p_funcion_5 ";
					$msql_datos.="INNER JOIN c_tipo_funcion ON p_funcion_5.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql_datos.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql_datos.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql_datos.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql_datos.="AND cve_organo_4='" . $morgano_admvo[3] . "' ";
					$msql_datos.="AND cve_organo_5='" . $morgano_admvo[4] . "' ";
					$msql_datos.="ORDER BY cmpapellido_paterno, cmpapellido_materno, cmpnombre LIMIT $minicial, $mcantidad";

					$msql ="SELECT cve_funcion_5 ";
					$msql.="FROM p_funcion_5 ";
					$msql.="WHERE cve_organo_1='" . $morgano_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $morgano_admvo[1] . "' ";
					$msql.="AND cve_organo_3='" . $morgano_admvo[2] . "' ";
					$msql.="AND cve_organo_4='" . $morgano_admvo[3] . "' ";
					$msql.="AND cve_organo_5='" . $morgano_admvo[4] . "'";
					break;
		 }

		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
 			$mnum_rows= mysql_num_rows($mquery);
 			if(	$mnum_rows > 0 )
			 {
	 			$mcont=0;
				$mdatos='<table id="mListado" width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<thead>
								<td width="4%" align="center">&nbsp;</td>
								<td width="60%" align="left">Nombre</td>
								<td width="36%" align="left">Funci&oacute;n</td>
								</thead>
							</tr>
							<tbody>';
				$mquery_datos= mysql_query($msql_datos, $mconexion_mysql);
	 			while( $mreg= mysql_fetch_array($mquery_datos) )
				 {
					if ( (bcmod($mcont, 2)) ==0 )
			 		 {	$mColor_Fila="#FFFAF5";	$mclass="";}
					else
		 	 		 { 	$mColor_Fila="#FFFFFF";	$mclass='class="odd"';}
			 
			 		$mcve_funcion=trim($mreg[0]);
					$mdatos.='<tr '. $mclass . ' > 
								<td align="center"><input name="cmr" type="radio" onclick="xajax_MOSTRAR_FUNCION(xajax.getFormValues(\'mDatos\'), \''. $mcve_funcion .'\', \'' . $mnivel . '\');" /></td>
								<td align="left">' . trim($mreg["cmptitulo"]) . " " . trim($mreg["cmpnombre"]) . " " . trim($mreg["cmpapellido_paterno"])	. " " . trim($mreg["cmpapellido_materno"]) .  '</td>
								<td align="left">' . trim($mreg["cmptipo_funcion"]) . '</td>
							  </tr>';
					$mcont+=1;
				 }
	
				if ( ( bcmod($mnum_rows, 2) == 0) || ( bcmod($mnum_rows / $mcantidad, 2) != 0) )
				 { $mnum_rows-=1; }
				 
				$mpages = intval($mnum_rows / $mcantidad);
				$mdatos.='<tr>
							<tfoot>
							<td height="50" colspan="3" align="right"><p>';
				
				if ($mpg <> 0)
				 {
					$murl = $mpg - 1;
					$mdatos.="<a href='#' onclick='LISTAR_FUNCION($murl,$mnivel); return false;'><code>Anterior</code></a>&nbsp;";
				 }
				else 
				 {
					$mdatos.=" ";
				 }
							  
				for ($mi = 0; $mi<($mpages + 1); $mi++)
				 {
					$mnum=$mi+1;
					if ($mi == $mpg)
					 {	$mdatos.="<code><b>$mnum</b>&nbsp;</code>";	} 
					else
					 {	$mdatos.="<a href='#' onclick='LISTAR_FUNCION($mi,$mnivel); return false;'><code>$mnum</code></a>&nbsp;";	}
				 }
							
				if ($mpg < $mpages)
				 {
					$murl = $mpg + 1;
					$mdatos.="&nbsp;<a href='#' onclick='LISTAR_FUNCION($murl,$mnivel); return false;'><code>Siguiente</code></a>";
				 }
				$mdatos.=" </p></<td>
						   </tfoot>
						  </tr>
						  </tbody>
						  </table>";
			 }
			else
			 {
				$mdatos="";
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
			$mdatos='<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<thead>
							<td align="center" colspan="3"><p>&nbsp;</p>Ocurrio un error al tratar de mostrar las Funciones.<p>&nbsp;</p>
							</td>
							</thead>
						</tr>
					 </table>';
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();	 
		$this->assign("mListado_Funcion","innerHTML",$mdatos);
	 }
	
	function MOSTRAR_FUNCION($mform_organo_admvo, $mcve_funcion, $mnivel)
	 {
	 	switch($mnivel)
		 {
			case 1: $msql ="SELECT cmptitulo, cmpapellido_paterno, cmpapellido_materno, cmpnombre, cve_tipo_funcion, cmptelefono";
					$msql.=", cmpextension, cmpfax, cmpcelular, cmpe_mail ";
					$msql.="FROM p_funcion_1 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_funcion_1='" . $mcve_funcion . "'";
					break;
			case 2: $msql ="SELECT cmptitulo, cmpapellido_paterno, cmpapellido_materno, cmpnombre, cve_tipo_funcion, cmptelefono";
					$msql.=", cmpextension, cmpfax, cmpcelular, cmpe_mail ";
					$msql.="FROM p_funcion_2 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_funcion_2='" . $mcve_funcion . "'";
					break;
			case 3: $msql ="SELECT cmptitulo, cmpapellido_paterno, cmpapellido_materno, cmpnombre, cve_tipo_funcion, cmptelefono";
					$msql.=", cmpextension, cmpfax, cmpcelular, cmpe_mail ";
					$msql.="FROM p_funcion_3 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_funcion_3='" . $mcve_funcion . "'";
					break;
			case 4: $msql ="SELECT cmptitulo, cmpapellido_paterno, cmpapellido_materno, cmpnombre, cve_tipo_funcion, cmptelefono";
					$msql.=", cmpextension, cmpfax, cmpcelular, cmpe_mail ";
					$msql.="FROM p_funcion_4 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql.="AND cve_funcion_4='" . $mcve_funcion . "'";
					break;
			case 5: $msql ="SELECT cmptitulo, cmpapellido_paterno, cmpapellido_materno, cmpnombre, cve_tipo_funcion, cmptelefono";
					$msql.=", cmpextension, cmpfax, cmpcelular, cmpe_mail ";
					$msql.="FROM p_funcion_5 ";
					$msql.="WHERE cve_organo_1='" . trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='" . trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='" . trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_organo_4='" . trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql.="AND cve_organo_5='" . trim($mform_organo_admvo["cmbOrgano_Admvo_5"]) . "' ";
					$msql.="AND cve_funcion_5='" . $mcve_funcion . "'";
					break;
		 }

		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
			$mnum_rows= mysql_num_rows($mquery);
			if($mnum_rows > 0 )
			 {
				$this->LIMPIAR_TEXTOS_FUNCION();
				$this->INHABILITAR_TEXTOS_FUNCION();
				
				$mreg= mysql_fetch_array($mquery);
				$mtxt= new INPUT_TEXT("txtTitulo_Funcion", $this);				$mtxt->SET_VALUE(trim($mreg["cmptitulo"]));
				$mtxt= new INPUT_TEXT("txtApellido_Paterno_Funcion", $this);	$mtxt->SET_VALUE(trim($mreg["cmpapellido_paterno"]));
				$mtxt= new INPUT_TEXT("txtApellido_Materno_Funcion", $this);	$mtxt->SET_VALUE(trim($mreg["cmpapellido_materno"]));
				$mtxt= new INPUT_TEXT("txtNombre_Funcion", $this);				$mtxt->SET_VALUE(trim($mreg["cmpnombre"]));

				$this->LLENAR_FUNCION("cmbFuncion");
				$mcmb_funcion= new SELECT("cmbFuncion", $this);
				$mcmb_funcion->SELECT_OPTION(trim($mreg["cve_tipo_funcion"]));
				
				$mtxt= new INPUT_TEXT("txtTelefono_Funcion", $this);			$mtxt->SET_VALUE(trim($mreg["cmptelefono"]));
				$mtxt= new INPUT_TEXT("txtExtension_Funcion", $this);			$mtxt->SET_VALUE(trim($mreg["cmpextension"]));
				$mtxt= new INPUT_TEXT("txtFax", $this);							$mtxt->SET_VALUE(trim($mreg["cmpfax"]));
				$mtxt= new INPUT_TEXT("txtCelular_Funcion", $this);				$mtxt->SET_VALUE(trim($mreg["cmpcelular"]));
				$mtxt= new INPUT_TEXT("txtE_Mail_Funcion", $this);				$mtxt->SET_VALUE(trim($mreg["cmpe_mail"]));

				$mtxt= new INPUT_TEXT("txt_Cve_Funcion", $this);				$mtxt->SET_VALUE($mcve_funcion);
				$mcmd= new BUTTON("cmdNuevo_Guardar_Funcion", $this);
				$mcmd->SET_VALUE("Nuevo");
				$mcmd->ENABLED();
				$mtxt= new TEXT("txt_Tipo_Guardar_Funcion", $this);
				$mtxt->SET_VALUE("0");
				$mcmd= new BUTTON("cmdModificar_Funcion", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdEliminar_Funcion", $this);
				$mcmd->ENABLED();
				$mcmd= new BUTTON("cmdCancelar_Funcion", $this);
				$mcmd->DISABLED();
			 }
			else
			 {
				$this->LIMPIAR_TEXTOS_FUNCION();
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_TEXTOS_FUNCION();
			$this->alert("Ocurrio un error al mostrar los datos de la Funcion." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
	 }

	function MOSTRAR_OPCIONES_IMPRESION($mcontenedor)
	 {
		$mdatos='<table width="350" align="center"  bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="100%"><p>&nbsp;<p></td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr> 
								<td width="15%" align="center"><input type="radio" name="mOpcion" id="mOpcion" checked="checked" value="1" onclick="ESTABLECER_OPCION(1);" /></td>
								<td width="85%" align="left" class="Estilo1">Para Repartir.</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr> 
								<td width="15%" align="center"><input type="radio" name="mOpcion" id="mOpcion" onclick="ESTABLECER_OPCION(2);" /></td>
								<td width="85%" align="left" class="Estilo1">Para Recursos Humanos.</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%"><p>&nbsp;<p><p>&nbsp;<p></td>
					</tr>
					<tr>
						<td width="100%" align="center" class="Estilo1" colspan="2"><input name="cmdAceptar_Impresion" id="cmdAceptar_Impresion" type="button" value="Aceptar" id="0" class="Estilo2" onclick="MOSTRAR_PDF();" /></td>
					</tr>
				 </table>';
		$this->assign($mcontenedor,"innerHTML", $mdatos); 
	 }
	
	function MOSTRAR_PDF($mtipo, $morgano_admvo1)
	 {
		switch($mtipo)
		 {
			case 2:	$murl="reportes/rptpara_recursos_humanos.php?mcve_organo_admvo_1=" . $morgano_admvo1;
					$this->script("window.open('$murl', '_blank', 'menubar=no');");		break;
		 }
	 }

	function REGRESA_NIVEL_ORGANO_ALTA($mform_organo_admvo)
	 {
 		$mcmb_organo_admvo=array("cmbOrgano_Admvo_1","cmbOrgano_Admvo_2", "cmbOrgano_Admvo_3", "cmbOrgano_Admvo_4","cmbOrgano_Admvo_5");
		$mtamano_mdatos=count($mcmb_organo_admvo);
		
		for($mi=$mtamano_mdatos; $mi>=1; $mi--)
		 {
			switch ($mi)
			 {
				case 1:					if( (strcmp(trim($mform_organo_admvo[$mcmb_organo_admvo[$mi-1]]),"00000")!=0) )
				 		 				 {	return 2;	}
										break;
				case $mtamano_mdatos:	if( (strcmp(trim($mform_organo_admvo[$mcmb_organo_admvo[$mi-1]]),"000")!=0) )
										 {	return $mi; }
										break;
				default:				if( (strcmp(trim($mform_organo_admvo[$mcmb_organo_admvo[$mi-1]]),"000")!=0) )
							 			 {	return $mi+1; }
										break;
			 }
		 }
		return 1; 
	 }
	 
	function REGRESA_NIVEL_ORGANO_CONSULTA($mform_organo_admvo)	
	 {
	 	$mcmb_organo_admvo=array("cmbOrgano_Admvo_1","cmbOrgano_Admvo_2", "cmbOrgano_Admvo_3", "cmbOrgano_Admvo_4","cmbOrgano_Admvo_5");
		$mtamano_mdatos=count($mcmb_organo_admvo);
		
		for($mi=$mtamano_mdatos; $mi>=1; $mi--)
		 {
			if ($mi==1)
			 {
			 	
				if( (strcmp(trim($mform_organo_admvo[$mcmb_organo_admvo[$mi-1]]),"00000")!=0) )
				 {	return $mi; }
			 }
			else
			 {
				if( (strcmp(trim($mform_organo_admvo[$mcmb_organo_admvo[$mi-1]]),"000")!=0) )
				 {	return $mi; }
			 }
		 }
		return 1;  
	 }
	
	function REGRESA_CVE_MAX_ORGANO($mform_organo_admvo, $mnivel)
	 {	
		switch($mnivel)
		 {
			case 1: $msql ="SELECT MAX(cve_organo_1) AS Maximo ";
					$msql.="FROM c_organo_1";
					break;
			case 2: $msql ="SELECT MAX(cve_organo_2) AS Maximo ";
					$msql.="FROM c_organo_2 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					break;
			case 3: $msql ="SELECT MAX(cve_organo_3) AS Maximo ";
					$msql.="FROM c_organo_3 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					break;
			case 4: $msql ="SELECT MAX(cve_organo_4) AS Maximo ";
					$msql.="FROM c_organo_4 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$msql.="AND cve_organo_3='". trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					break;
			case 5: $msql ="SELECT MAX(cve_organo_5) AS Maximo ";
					$msql.="FROM c_organo_5 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					$msql.="AND cve_organo_3='". trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					$msql.="AND cve_organo_4='". trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "'";
					break;
		 }

		$mbase_datos= new BASE_DATOS($this); 
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
	 			$mreg= mysql_fetch_array($mquery);
				$mmax= $mreg["0"] + 1;
			 }
			else
			 {
				$mmax= 1;
			 }

		 	$mclave= "";
			if($mnivel==1)
			 {
			 	for($mi=strlen($mmax); $mi<=4;  $mi++)
				 {	$mclave.= "0";	}
			 }
			else  
			 {
			 	for($mi=strlen($mmax); $mi<=2;  $mi++)
				 { $mclave.= "0";	}
			 }
			$mclave.= $mmax;
			mysql_free_result($mquery);
		 }
		else
		 {
			$mclave= "-1";
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
		return $mclave;
	 }
	
	function REGRESA_CVE_MAX_FUNCION($mform_organo_admvo, $mnivel)
	 {	
		switch($mnivel)
		 {
			case 1: $msql ="SELECT MAX(cve_funcion_1) AS Maximo ";
					$msql.="FROM p_funcion_1 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "'";
					break;
			case 2: $msql ="SELECT MAX(cve_funcion_2) AS Maximo ";
					$msql.="FROM p_funcion_2 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "'";
					break;
			case 3: $msql ="SELECT MAX(cve_funcion_3) AS Maximo ";
					$msql.="FROM p_funcion_3 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='". trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "'";
					break;
			case 4: $msql ="SELECT MAX(cve_funcion_4) AS Maximo ";
					$msql.="FROM p_funcion_4 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='". trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_organo_4='". trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "'";
					break;
			case 5: $msql ="SELECT MAX(cve_funcion_5) AS Maximo ";
					$msql.="FROM p_funcion_5 ";
					$msql.="WHERE cve_organo_1='". trim($mform_organo_admvo["cmbOrgano_Admvo_1"]) . "' ";
					$msql.="AND cve_organo_2='". trim($mform_organo_admvo["cmbOrgano_Admvo_2"]) . "' ";
					$msql.="AND cve_organo_3='". trim($mform_organo_admvo["cmbOrgano_Admvo_3"]) . "' ";
					$msql.="AND cve_organo_4='". trim($mform_organo_admvo["cmbOrgano_Admvo_4"]) . "' ";
					$msql.="AND cve_organo_5='". trim($mform_organo_admvo["cmbOrgano_Admvo_5"]) . "'";
					break;
		 }

		$mbase_datos= new BASE_DATOS($this); 
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(0);
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
	 			$mreg= mysql_fetch_array($mquery);
				$mmax= $mreg["0"] + 1;
			 }
			else
			 {
				$mmax= 1;
			 }

		 	$mclave= "";
		 	for($mi=strlen($mmax); $mi<=2;  $mi++)
			 { $mclave.= "0";	}
			$mclave.= $mmax;
			mysql_free_result($mquery);
		 }
		else
		 {
			$mclave= "-1";
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
		return $mclave;
	 }

	function OBTENER_ORGANO_ADMVO_1($mcve_organo_admvo_1)
	 {
		$msql ="SELECT cmporgano_1, cmptitulo_titular, cmpapellido_paterno_titular, cmpapellido_materno_titular, cmpnombre_titular ";
		$msql.=", cmptelefono_titular, cmpextension_titular, cmpe_mail_titular ";
		$msql.="FROM c_organo_1 ";
		$msql.="WHERE cve_organo_1='" . $mcve_organo_admvo_1 . "'";

		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(1);

		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	$mreg= mysql_fetch_array($mquery);	}
			else
			 {	$mreg="";	}
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$mreg="-1";
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
		return $mreg;
	 }

	function OBTENER_ORGANO_ADMVO($mcve_organo_admvo)
	 {	
		switch(count($mcve_organo_admvo))
		 {
		 	case 1: $msql ="SELECT cmporgano_2, cve_organo_2, cmptitulo_titular, cmpapellido_paterno_titular";
					$msql.=", cmpapellido_materno_titular,cmpnombre_titular, cmptelefono_titular, cmpextension_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_2 ";
					$msql.="WHERE cve_organo_1='" . $mcve_organo_admvo[0] . "' ";
					$msql.="ORDER BY cmporgano_2";
					break;
		 	case 2: $msql ="SELECT cmporgano_3, cve_organo_3, cmptitulo_titular, cmpapellido_paterno_titular";
					$msql.=", cmpapellido_materno_titular,cmpnombre_titular, cmptelefono_titular, cmpextension_titular";
					$msql.=", cmpe_mail_titular ";
					$msql.="FROM c_organo_3 ";
					$msql.="WHERE cve_organo_1='" . $mcve_organo_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $mcve_organo_admvo[1] . "' ";
					$msql.="ORDER BY cmporgano_3";
					break;
					
		}
		
		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(1);

		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {
			 	$mi=0;
				while($mreg= mysql_fetch_array($mquery))
				 {	$morgano[$mi++]=$mreg;	}
			 }
			else
			 {
			 	$morgano="";	
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$morgano="-1";
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
		return $morgano;
	 }

	function OBTENER_FUNCIONES_ORGANO($mcve_organo_admvo)
	 {	
		switch(count($mcve_organo_admvo))
		 {
		 	case 1: $msql="SELECT p_funcion_1.cmptitulo, p_funcion_1.cmpapellido_paterno, p_funcion_1.cmpapellido_materno";
					$msql.=",  p_funcion_1.cmpnombre, p_funcion_1.cmptelefono, p_funcion_1.cmpextension, p_funcion_1.cmpe_mail";
					$msql.=",  c_tipo_funcion.cmptipo_funcion ";
					$msql.="FROM p_funcion_1 ";
					$msql.="INNER JOIN c_tipo_funcion ON p_funcion_1.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql.="WHERE cve_organo_1='" . $mcve_organo_admvo[0] . "'";
					break;
			case 2: $msql="SELECT p_funcion_2.cmptitulo, p_funcion_2.cmpapellido_paterno, p_funcion_2.cmpapellido_materno";
					$msql.=",  p_funcion_2.cmpnombre, p_funcion_2.cmptelefono, p_funcion_2.cmpextension, p_funcion_2.cmpe_mail";
					$msql.=",  c_tipo_funcion.cmptipo_funcion ";
					$msql.="FROM p_funcion_2 ";
					$msql.="INNER JOIN c_tipo_funcion ON p_funcion_2.cve_tipo_funcion = c_tipo_funcion.cve_tipo_funcion ";
					$msql.="WHERE cve_organo_1='" . $mcve_organo_admvo[0] . "' ";
					$msql.="AND cve_organo_2='" . $mcve_organo_admvo[1] . "'";
					break;
		}
		
		$mbase_datos= new BASE_DATOS($this);
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL(1);

		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {
			 	$mi=0;
				while($mreg= mysql_fetch_array($mquery))
				 {	$mfuncion[$mi++]=$mreg;	}
			 }
			else
			 {
			 	$mfuncion="";	
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$mfuncion="-1";
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL();
		return $mfuncion;
	 }

	function LIMPIAR_TEXTOS_ORGANO()
	 {
		$mtxt= new INPUT_TEXT("txtOrgano", $this);						$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtTitulo_Titular", $this);				$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtApellido_Paterno_Titular", $this);	$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtApellido_Materno_Titular", $this);	$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtNombre_Titular", $this);				$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtTelefono_Titular", $this);			$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtExtension_Titular", $this);			$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtFax_Titular", $this);					$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtCelular_Titular", $this);				$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtE_Mail_Titular", $this);				$mtxt->SET_VALUE("");
	 }

	function HABILITAR_TEXTOS_ORGANO()
	 {
		$mtxt= new INPUT_TEXT("txtOrgano", $this);						$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtTitulo_Titular", $this);				$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Paterno_Titular", $this);	$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Materno_Titular", $this);	$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtNombre_Titular", $this);				$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtTelefono_Titular", $this);			$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtExtension_Titular", $this);			$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtFax_Titular", $this);					$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtCelular_Titular", $this);				$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtE_Mail_Titular", $this);				$mtxt->ENABLED();
	 }

	function INHABILITAR_TEXTOS_ORGANO()
	 {
	 	$mtxt= new INPUT_TEXT("txtOrgano", $this);						$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtTitulo_Titular", $this);				$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Paterno_Titular", $this);	$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Materno_Titular", $this);	$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtNombre_Titular", $this);				$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtTelefono_Titular", $this);			$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtExtension_Titular", $this);			$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtFax_Titular", $this);					$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtCelular_Titular", $this);				$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtE_Mail_Titular", $this);				$mtxt->DISABLED();
	 }

	function LIMPIAR_TEXTOS_FUNCION()
	 {
		$mtxt= new INPUT_TEXT("txtTitulo_Funcion", $this);				$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtApellido_Paterno_Funcion", $this);	$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtApellido_Materno_Funcion", $this);	$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtNombre_Funcion", $this);				$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtTelefono_Funcion", $this);			$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtExtension_Funcion", $this);			$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtFax_Funcion", $this);					$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtCelular_Funcion", $this);				$mtxt->SET_VALUE("");
		$mtxt= new INPUT_TEXT("txtE_Mail_Funcion", $this);				$mtxt->SET_VALUE("");
	 }

	function HABILITAR_TEXTOS_FUNCION()
	 {
		$mtxt= new INPUT_TEXT("txtTitulo_Funcion", $this);				$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Paterno_Funcion", $this);	$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Materno_Funcion", $this);	$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtNombre_Funcion", $this);				$mtxt->ENABLED();
		$mcombo= new SELECT("cmbFuncion", $this);						$mcombo->ENABLED();
		$mtxt= new INPUT_TEXT("txtTelefono_Funcion", $this);			$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtExtension_Funcion", $this);			$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtFax_Funcion", $this);					$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtCelular_Funcion", $this);				$mtxt->ENABLED();
		$mtxt= new INPUT_TEXT("txtE_Mail_Funcion", $this);				$mtxt->ENABLED();
	 }

	function INHABILITAR_TEXTOS_FUNCION()
	 {
		$mtxt= new INPUT_TEXT("txtTitulo_Funcion", $this);				$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Paterno_Funcion", $this);	$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtApellido_Materno_Funcion", $this);	$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtNombre_Funcion", $this);				$mtxt->DISABLED();
		$mcombo= new SELECT("cmbFuncion", $this);						$mcombo->DISABLED();
		$mtxt= new INPUT_TEXT("txtTelefono_Funcion", $this);			$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtExtension_Funcion", $this);			$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtFax_Funcion", $this);					$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtCelular_Funcion", $this);				$mtxt->DISABLED();
		$mtxt= new INPUT_TEXT("txtE_Mail_Funcion", $this);				$mtxt->DISABLED();
	 }	 

	function LIMPIAR_ORGANOS_ADMVOS($mtipo)
	 {
	 	switch($mtipo)
		 {		 	
		 	case 1:	$mselect= new SELECT("cmbOrgano_Admvo_1", $this);		
					$mselect->CLEAR_OPTIONS();
					$mselect->ADD_OPTION("", "00000");
					$this->LIMPIAR_ORGANOS_ADMVOS(2);
					break;
					
		 	case 2:	$mselect= new SELECT("cmbOrgano_Admvo_2", $this);
					$mselect->CLEAR_OPTIONS();
					$mselect->ADD_OPTION("", "000");
					$this->LIMPIAR_ORGANOS_ADMVOS(3);
					break;
					
			case 3:	$mselect= new SELECT("cmbOrgano_Admvo_3", $this);
					$mselect->CLEAR_OPTIONS();
					$mselect->ADD_OPTION("", "000");
					$this->LIMPIAR_ORGANOS_ADMVOS(4);
					break;
			
			case 4:	$mselect= new SELECT("cmbOrgano_Admvo_4", $this);
					$mselect->CLEAR_OPTIONS();
					$mselect->ADD_OPTION("", "000");
					$this->LIMPIAR_ORGANOS_ADMVOS(5);
					break;
			
			case 5:	$mselect= new SELECT("cmbOrgano_Admvo_5", $this);
					$mselect->CLEAR_OPTIONS();
					$mselect->ADD_OPTION("", "000");
					break;
		 }
	 }
}
?>