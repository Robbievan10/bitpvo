<?php

class REPORTE extends xajaxResponse
 {
 	protected $mfpdi;
 	protected $mcve_organo_admvo_1, $mcve_organo_admvo_2;
	protected $mx=0, $my=0;

 	public function __construct($mfpdi, $mcve_organo_admvo_1)
     {
		$this->mfpdi=$mfpdi;
		$this->mcve_organo_admvo_1=$mcve_organo_admvo_1;
	 }

 	public function IMPRIMIR_ENCABEZADO()
	 {
		$this->mfpdi->SetFont("Arial", "B", 15); 
		$this->mfpdi->SetTextColor(0, 0, 0);
		$this->mfpdi->SetXY(255, 55);
		$mindex= new INDEX();
		$morgano_admvo_1=$mindex->OBTENER_ORGANO_ADMVO_1($this->mcve_organo_admvo_1);
		$this->mfpdi->Write(0, strtoupper($morgano_admvo_1["cmporgano_1"]));
	 }

	public function IMPRIMIR_DETALLE()
	 {
	 	$mcolor_fondo_1= array(array(255, 255, 0),
					 	 	   array(146, 208, 80),
							   array(153, 204, 255));
	 	$mcolor_fondo_2= array(255, 255, 55);

	 	$mindex= new INDEX();
		$morgano_admvo_1=$mindex->OBTENER_ORGANO_ADMVO_1($this->mcve_organo_admvo_1);
		$this->my=153;
		$this->IMPRIMIR_ORGANO($morgano_admvo_1, 25, $mcolor_fondo_1[0]);
		$this->my+=25;
		$this->IMPRIMIR_FUNCIONES(1);

		$morgano_admvo=array($this->mcve_organo_admvo_1);
		$morgano_admvo_2=$mindex->OBTENER_ORGANO_ADMVO($morgano_admvo);
		for($mi=0; $mi<count($morgano_admvo_2); $mi++)
		 {
		 	$this->IMPRIMIR_ORGANO($morgano_admvo_2[$mi], 25, $mcolor_fondo_1[$mi+1]);
			$this->my+=25;
			$this->mcve_organo_admvo_2=$morgano_admvo_2[$mi]["cve_organo_2"];
			$this->IMPRIMIR_FUNCIONES(2);
			
			$morgano_admvo=array($this->mcve_organo_admvo_1, $this->mcve_organo_admvo_2);
			$morgano_admvo_3=$mindex->OBTENER_ORGANO_ADMVO($morgano_admvo);
			for($mi=0; $mi<count($morgano_admvo_3); $mi++)
			 {
			 	$this->IMPRIMIR_ORGANO($morgano_admvo_3[$mi], 15, $mcolor_fondo_2);
				$this->my+=15;
			}


		 }
	 }

	public function IMPRIMIR_ORGANO($morgano, $malto, $mcolor_fondo)
	 {
		$this->IMPRIMIR_ENCABEZADO_ORGANO(strtoupper(trim($morgano[0])), $malto, $mcolor_fondo);
		$this->mx=273;
		$this->IMPRIMIR_DETALLE_ORGANO(170, $malto, trim($morgano["cmptitulo_titular"]) . " " .trim($morgano["cmpnombre_titular"]) . " " . trim($morgano["cmpapellido_paterno_titular"]) . " " . trim($morgano["cmpapellido_materno_titular"]));
		$this->mx+=170;
		$this->IMPRIMIR_DETALLE_ORGANO(79, $malto, trim($morgano["cmptelefono_titular"]));
		$this->mx+=79;
		$this->IMPRIMIR_DETALLE_ORGANO(57, $malto, trim($morgano["cmpextension_titular"]));
		$this->mx+=57;
		$this->IMPRIMIR_DETALLE_ORGANO(155, $malto, trim($morgano["cmpe_mail_titular"]));
	 }

	public function IMPRIMIR_FUNCIONES($mtipo)
	 {
		switch($mtipo)
		 {
			case 1:	$morgano_admvo=array($this->mcve_organo_admvo_1);
					break;
			case 2:	$morgano_admvo=array($this->mcve_organo_admvo_1, $this->mcve_organo_admvo_2);
					break;
		 }

	 	$mindex= new INDEX();
		$mfunciones=$mindex->OBTENER_FUNCIONES_ORGANO($morgano_admvo);
		for($mi=0; $mi<count($mfunciones); $mi++)
		 {
		 	$this->mx=58;
			$this->IMPRIMIR_DETALLE_FUNCION(215, trim($mfunciones[$mi]["cmptipo_funcion"]));

			if ( (is_null(trim($mfunciones[$mi]["cmptitulo"]))) || (strcmp(trim($mfunciones[$mi]["cmptitulo"]),"")==0) )
			 {
				$mnombre=trim($mfunciones[$mi]["cmpnombre"]) . " " . trim($mfunciones[$mi]["cmpapellido_paterno"]) . " " . trim($mfunciones[$mi]["cmpapellido_materno"]);
			 }
			else
			 {
			 	$mnombre=trim($mfunciones[$mi]["cmptitulo"]) . " " . trim($mfunciones[$mi]["cmpnombre"]) . " " . trim($mfunciones[$mi]["cmpapellido_paterno"]) . " " . trim($mfunciones[$mi]["cmpapellido_materno"]);
			 }

			$this->mx=273;
			$this->IMPRIMIR_DETALLE_FUNCION(170, $mnombre);
			$this->mx+=170;
			$this->IMPRIMIR_DETALLE_FUNCION(79, trim($mfunciones[$mi]["cmptelefono"]));
			$this->mx+=79;
			$this->IMPRIMIR_DETALLE_FUNCION(57, trim($mfunciones[$mi]["cmpextension"]));
			$this->mx+=57;
			$this->IMPRIMIR_DETALLE_FUNCION(155, trim($mfunciones[$mi]["cmpe_mail"]));
			$this->my+=15;
		 }
	 }

	public function IMPRIMIR_ENCABEZADO_ORGANO($mnombre, $malto, $mcolor_fondo)
	 {
		$this->mfpdi->SetFont("Arial", "B", 7); 
		$this->mfpdi->SetTextColor(0, 0, 0);
		$this->mfpdi->SetFillColor($mcolor_fondo[0], $mcolor_fondo[1], $mcolor_fondo[2]);
		$this->mfpdi->SetXY(58, $this->my);
		$this->mfpdi->Cell(215, $malto, $mnombre, 1, 2, 'C', 1);
	 }

	public function IMPRIMIR_DETALLE_ORGANO($mancho, $malto, $mnombre)
	 {
		$this->mfpdi->SetFont("Arial", "", 7); 
		$this->mfpdi->SetTextColor(0, 0, 0);
		$this->mfpdi->SetXY($this->mx, $this->my);
		$this->mfpdi->Cell($mancho, $malto, $mnombre, 1, 2, 'L', 0);
	 }

	public function IMPRIMIR_DETALLE_FUNCION($mancho, $mnombre)
	 {
		$this->mfpdi->SetFont("Arial", "", 7); 
		$this->mfpdi->SetTextColor(0, 0, 0);
		$this->mfpdi->SetXY($this->mx, $this->my);
		$this->mfpdi->Cell($mancho, 15, $mnombre, 1, 2, 'L', 0);
	 }
 }
?>