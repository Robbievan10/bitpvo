function DEFAULT()
 {
	//Nifty("div#mTitulo","tl bottom big fixed-height");
	LLENAR_ORGANO_ADMVO(1);
 }

function OPCIONES_NUEVO()
 {
	switch (parseInt(xajax.$('txt_Tipo_Guardar_Organo').value))
	 {
		case 0:	xajax_INICIALIZAR_DATOS_ORGANO();
				break;
		case 1: if (VALIDAR_DATOS_ORGANO())
				 {
					xajax_GUARDAR_NUEVO_ORGANO(xajax.getFormValues("mDatos"), xajax.getFormValues("mOrgano"));
				 }
				break;
		case 2: if (VALIDAR_DATOS_ORGANO())
				 {
					xajax_GUARDAR_MODIFICACION_ORGANO(xajax.getFormValues("mDatos"), xajax.getFormValues("mOrgano"));
				 }
				break;
	 }
 }

function OPCIONES_NUEVO_FUNCION()
 {
	switch (parseInt(xajax.$('txt_Tipo_Guardar_Funcion').value))
	 {
		case 0:	xajax_INICIALIZAR_DATOS_FUNCION();
				break;
		case 1: if (VALIDAR_DATOS_FUNCION())
				 {
					xajax_GUARDAR_NUEVO_FUNCION(xajax.getFormValues("mDatos"), xajax.getFormValues("mFuncion"));
				 }
				break;
		case 2: if (VALIDAR_DATOS_FUNCION())
				 {
					xajax_GUARDAR_MODIFICACION_FUNCION(xajax.getFormValues("mDatos"), xajax.getFormValues("mFuncion"), xajax.$('txt_Cve_Funcion').value);
				 }
				break;
	 }
 }

function LLENAR_ORGANO_ADMVO(mnivel)
 {
	var morgano_admvo= new Array(4);
	
	switch(mnivel)
	 {
		case 1:	xajax_LLENAR_ORGANO_ADMVO_1("cmbOrgano_Admvo_1");
				break;
		case 2:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				xajax_LLENAR_ORGANO_ADMVO_2("cmbOrgano_Admvo_2", morgano_admvo);
				break;
		case 3:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				xajax_LLENAR_ORGANO_ADMVO_3("cmbOrgano_Admvo_3", morgano_admvo);
				break;
		case 4:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				morgano_admvo[2]=Trim(xajax.$("cmbOrgano_Admvo_3").options[xajax.$("cmbOrgano_Admvo_3").selectedIndex].value);
				xajax_LLENAR_ORGANO_ADMVO_4("cmbOrgano_Admvo_4", morgano_admvo);
				break;
		case 5:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				morgano_admvo[2]=Trim(xajax.$("cmbOrgano_Admvo_3").options[xajax.$("cmbOrgano_Admvo_3").selectedIndex].value);
				morgano_admvo[3]=Trim(xajax.$("cmbOrgano_Admvo_4").options[xajax.$("cmbOrgano_Admvo_4").selectedIndex].value);
				xajax_LLENAR_ORGANO_ADMVO_5("cmbOrgano_Admvo_5", morgano_admvo);
				break;
	 }
 }

function LISTAR_FUNCION(mpg, mnivel)
 {
	var morgano_admvo= new Array(4);
	
	switch(mnivel)
	 {
		case 1:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				break;
		case 2:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				break;
		case 3:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				morgano_admvo[2]=Trim(xajax.$("cmbOrgano_Admvo_3").options[xajax.$("cmbOrgano_Admvo_3").selectedIndex].value);
				break;
		case 4:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				morgano_admvo[2]=Trim(xajax.$("cmbOrgano_Admvo_3").options[xajax.$("cmbOrgano_Admvo_3").selectedIndex].value);
				morgano_admvo[3]=Trim(xajax.$("cmbOrgano_Admvo_4").options[xajax.$("cmbOrgano_Admvo_4").selectedIndex].value);
				break;
		case 5:	morgano_admvo[0]=Trim(xajax.$("cmbOrgano_Admvo_1").options[xajax.$("cmbOrgano_Admvo_1").selectedIndex].value);
				morgano_admvo[1]=Trim(xajax.$("cmbOrgano_Admvo_2").options[xajax.$("cmbOrgano_Admvo_2").selectedIndex].value);
				morgano_admvo[2]=Trim(xajax.$("cmbOrgano_Admvo_3").options[xajax.$("cmbOrgano_Admvo_3").selectedIndex].value);
				morgano_admvo[3]=Trim(xajax.$("cmbOrgano_Admvo_4").options[xajax.$("cmbOrgano_Admvo_4").selectedIndex].value);
				morgano_admvo[4]=Trim(xajax.$("cmbOrgano_Admvo_4").options[xajax.$("cmbOrgano_Admvo_4").selectedIndex].value);
				break;
	 }
	xajax_LISTAR_FUNCION(morgano_admvo, mpg, mnivel);
 }
 
function ELIMINAR_ORGANO()
 {
	if ( confirm("�Desea realizar la eliminaci�n del Organo Administrativo?." + String.fromCharCode(13) + "Si elimina el Organo Administrativo eliminar� las funciones asignadas que le corresponde.") )
	 {	
		if ( confirm("�Esta realmente seguro de proceder?.") )
		 {	xajax_ELIMINAR_ORGANO(xajax.getFormValues('mDatos'));	}
	 }
	else
	 {	alert("La eliminaci�n del �rgano Administrativo ha sido cancelada.");	}
 }
 
function ELIMINAR_FUNCION()
 {
	if ( confirm("�Desea realizar la eliminaci�n de la Funci�n?.") )
	 {	xajax_ELIMINAR_FUNCION(xajax.getFormValues('mDatos'),  xajax.$('txt_Cve_Funcion').value);	}
	else
	 {	alert("La eliminaci�n de la Funci�n ha sido cancelada.");	}
 } 

function ABRIR_VENTANA($msesion)
 {
	var mventana;
	var mtitulo, mid, mcontenido;
	
	mtitulo="Opciones de Impresi�n";
	mid="mOpcion_Impresion";
	
	mventana = new Window({className:"mac_os_x", title: mtitulo, width:350, height:120, zIndex:100, destroyOnClose: true, recenterAuto:false});
	mventana.getContent().update("<div id='" + mid  + "' style='text-align:center'></div>");
	mventana.showCenter();
	
	xajax_MOSTRAR_OPCIONES_IMPRESION(mid);
 }

function ESTABLECER_OPCION(mtipo)
 {
	 xajax.$('mOpcion').value=mtipo; 
 }

function MOSTRAR_PDF()
 {
	 if ( (xajax.$('mOpcion').value!=null) && (xajax.$('mOpcion').value!=0) )
	 { 
	 	xajax_MOSTRAR_PDF(xajax.$('mOpcion').value, Trim(xajax.$("cmbOrgano_Admvo_1").value));
	 }
	else
	 {
		alert("Seleccione un Tipo de Reporte v�lido a imprimir.");
	 }
 }

function VALIDAR_DATOS_ORGANO()
 {
	if ( Trim(xajax.$("txtOrgano").value) == "" )
	 {
		 alert("Proporcione el Nombre del �rgano Administrativo.");
		 xajax.$("txtOrgano").focus();
		 return false;
	 }
	if ( Trim(xajax.$("txtTitulo_Titular").value) == "" )
	 {
		 alert("Proporcione el T�tulo.");
		 xajax.$("txtTitulo_Titular").focus();
		 return false;
	 }
	if ( Trim(xajax.$("txtApellido_Paterno_Titular").value) == "" )
	 {
		 alert("Proporcione el Apellido Paterno del Titular.");
		 xajax.$("txtApellido_Paterno_Titular").focus();
		 return false;
	 }
 	if ( Trim(xajax.$("txtApellido_Materno_Titular").value) == "" )
	 {
		 alert("Proporcione el Apellido Materno del Titular.");
		 xajax.$("txtApellido_Materno_Titular").focus();
		 return false;
	 }
	if ( Trim(xajax.$("txtNombre_Titular").value) == "" )
	 {
		 alert("Proporcione el Nombre del Titular.");
		 xajax.$("txtNombre_Titular").focus();
		 return false;
	 }
	if ( Trim(xajax.$("txtExtension_Titular").value) == "" )
	 {
		 alert("Proporcione la Extensi�n del Titular.");
		 xajax.$("txtExtension_Titular").focus();
		 return false;
	 } 
	return true;
 }

function VALIDAR_DATOS_FUNCION()
 {
	if ( Trim(xajax.$("txtApellido_Paterno_Funcion").value) == "" )
	 {
		 alert("Proporcione el Apellido Paterno del ocupante de la Funci�n.");
		 xajax.$("txtApellido_Paterno_Funcion").focus();
		 return false;
	 }
 	if ( Trim(xajax.$("txtApellido_Materno_Funcion").value) == "" )
	 {
		 alert("Proporcione el Apellido Materno del ocupante de la Funci�n.");
		 xajax.$("txtApellido_Materno_Funcion").focus();
		 return false;
	 }
	if ( Trim(xajax.$("txtNombre_Funcion").value) == "" )
	 {
		 alert("Proporcione el Nombre del ocupante de la Funci�n.");
		 xajax.$("txtNombre_Funcion").focus();
		 return false;
	 }
	return true;
 }