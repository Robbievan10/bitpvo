<?php 
if( (!isset($_GET["mcve_organo_admvo_1"])) )
 {
	die('<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" class="Estilo11">Imposible ver el reporte. Falta de parametros.</td>
			</tr>
		 </table>');
 }

$mcve_organo_admvo_1=trim($_GET["mcve_organo_admvo_1"]);

require_once dirname(dirname(__FILE__)) . "/utilerias/FPDI/fpdi.php";
$mxajax_core = dirname(dirname(__FILE__)) . "/utilerias/xajax/xajax_core";
require_once($mxajax_core . "/xajax.inc.php");
$mxajax = new xajax();
$mxajax->configure("javascript URI", "./utilerias/xajax");

require_once(dirname(dirname(__FILE__)) . "/funciones/reporte_clase.php");
require_once(dirname(dirname(__FILE__)) . "/funciones/index_clase.php");
require_once(dirname(dirname(__FILE__)) . "/funciones/clases.php");

$mfpdi= & new FPDI("L", "pt", "Letter");
$mfpdi->addpage();
$mfpdi-> setSourceFile("plantillas/para_recursos_humanos.pdf");
$mnomFile="para_recursos_humanos";
$mfpdi-> setAuthor("I.S.C Edwuard H. Cabrera Rodr�guez");
$mpag=$mfpdi -> importPage(1, "/MediaBox");
$mfpdi->useTemplate($mpag, 0, 0);

$mrptpara_recursos_humanos= new REPORTE($mfpdi, $mcve_organo_admvo_1);
$mrptpara_recursos_humanos->IMPRIMIR_ENCABEZADO();
$mrptpara_recursos_humanos->IMPRIMIR_DETALLE();
/*
$msql="SELECT cmporgano_1 FROM c_organo_1 ORDER BY cmporgano_1";
		
		$mconexion_mysql= $mbase_datos->ABRIR_BASE_DATOS_MYSQL();
		if( $mquery= mysql_query($msql, $mconexion_mysql) )
		 {
		 	$mnum_rows= mysql_num_rows($mquery);
		 	if(	$mnum_rows > 0 )
			 {	
	 			$mselect->CLEAR_OPTIONS();
				$mcont=1;
				while($mreg= mysql_fetch_array($mquery))
				 {	
					$mselect->ADD_OPTION(trim($mreg["cmporgano_1"]), trim($mreg["cve_organo_1"]));
					if ($mcont==1)
					 {	$morgano_admvo= array(trim($mreg["cve_organo_1"]));	}
				 }
				 
				if( $mtipo_llenado==0 )
				 {
					$this->script("LLENAR_ORGANO_ADMVO(2);");
					$this->MOSTRAR_ORGANO_ADMVO($morgano_admvo, 1);
				 }
			 }
			else
			 {
			 	$this->LIMPIAR_ORGANOS_ADMVOS(1);
				$this->script("xajax_CANCELAR_ORGANO(xajax.getFormValues('mDatos'));");				
				$this->script("xajax_CANCELAR_FUNCION(1);");
				$this->assign("mListado_Funcion","innerHTML","");
				$mcmd= new BUTTON("cmdImprimir", $this);
				$mcmd->DISABLED();
			 }
			mysql_free_result($mquery);
		 }
		else
		 {
		 	$this->LIMPIAR_ORGANOS_ADMVOS(1);
		 	$this->alert("Ocurrio un error al tratar de llenar el Nivel Administrativo 1." . chr(13) .  mysql_error() );
		 }
		$mbase_datos->CERRAR_BASE_DATOS_MYSQL(); 
*/

							/*$mfpdi 		-> SetFont('Arial','',12); 
							$mfpdi 		-> SetTextColor(255,128,0);
							$municipio 	=  "\municipio" ;
							$mfpdi 		-> SetXY(67, 45);
							$mfpdi  		-> Cell(150, 5, $municipio, 0, 0, 'C');
							
							$mfpdi 		-> SetTextColor(0,0,0);
							$mfpdi 		-> SetFont('Arial','',8); */
							/*
							//A partir de la posicion de la tabla, entran 40 columnas
							//38 caracteres por celda aprox
							//POS inicial 63
							$filaInicial 	= 64;
							
							$numero_enlaces	= $control -> total_regs_query($_SESSION['ultima_consulta']);
							$rs 			= $control -> exec_query($_SESSION['ultima_consulta']);
							
								while ( $row = mysql_fetch_array($rs) )
								{
										$mfpdi -> SetXY(25, 	$filaInicial);
										$mfpdi -> Write(0, 	$row['enlace']);
										$mfpdi -> SetXY(113, 	$filaInicial);
										$mfpdi -> Write(0, 	$row['telefono']);
										$mfpdi -> SetXY(176,	$filaInicial);
										$mfpdi -> Write(0,	$row['rfc']);
										$mfpdi -> SetXY(214,	$filaInicial);
										$mfpdi -> Write(0,	$row['clave_nominal']);
										$filaInicial += 4;
								}
							$control -> liberar($rs);
					break;
				case 2 :
							//Reporte Completo, Enlaces en Municipios Sistematizados
							$mfpdi			->AliasNbPages();
							$mfpdi 			-> setSourceFile('../rpt/enlaces_mpios.pdf');
							$mnomFile 		= "rptEnlacesMpios";
							$mfpdi 			-> setAuthor('Levi Fco Amanecer');
							$mpag 			= $mfpdi -> importPage(1, "/MediaBox");
							$mfpdi 			-> useTemplate($mpag,0,0);	
							$mfpdi 			-> SetTextColor(0,0,0);
							
							$numero_enlaces = $control -> total_registros();
							$numero_paginas = ceil ( $numero_enlaces / 40 );
							
							$mfpdi ->SetY(177);
							//Arial italic 8
							$mfpdi->SetFont('Arial', 'I', 8);
							//N�mero de p�gina
							$mfpdi->Cell(0,10,'Pagina ' . $mfpdi -> PageNo() . '/{nb}', 0, 0, 'C');
							
							$filaInicial 	= 60;
							$rs 			= $control -> exec_query($_SESSION['ultima_consulta']);
									
							$mfpdi 			-> SetFont('Arial', '', 7); 
							
									while ( $row = mysql_fetch_array($rs) ) {
											
										if ( $filaInicial <= 165 )
										{				
													$mfpdi -> SetXY(24, 	$filaInicial);
													$mfpdi -> Write(0,  	$row['enlace']);
													$mfpdi -> SetXY(85, 	$filaInicial);
													$mfpdi -> Write(0, 	$row['municipio_adscrito']);
													$mfpdi -> SetXY(135,  $filaInicial);
													$mfpdi -> Write(0,	$row['telefono']);
													$mfpdi -> SetXY(186,	$filaInicial);
													$mfpdi -> Write(0,	$row['rfc']);
													$mfpdi -> SetXY(220,	$filaInicial);
													$mfpdi -> Write(0,	$row['clave_nominal']);
													
													$filaInicial += 4;
										}
										else
										{
													$filaInicial = 60;
													
													$mfpdi 		-> addpage();
													$mfpdi 		-> setSourceFile('../rpt/enlaces_mpios.pdf');
													$mpag 		=  $mfpdi -> importPage(1, "/MediaBox");
													$mfpdi 		-> useTemplate($mpag, 0, 0);		
													$mfpdi 		-> SetY(177);
													$mfpdi		-> SetFont('Arial', 'I', 7);
													$mfpdi		-> Cell(0,10,'Pagina ' . $mfpdi -> PageNo() . '/{nb}', 0, 0, 'C');
													$mfpdi 		-> SetFont('Arial','',7); 
										}
									}
								$control -> liberar($rs);
					break;
				
				case 3 :
							//Reporte de los Delegados Regionales del Instituto Chiapas Solidario
							$mfpdi 			-> setSourceFile('../rpt/delegados_rg_chisol.pdf');
							$mnomFile		= "rptDelegadosRegionalesChisol";
							$mfpdi 			-> setAuthor('Levi Fco Amanecer');
							$mpag			= $mfpdi -> importPage(1, "/MediaBox");
							$mfpdi 			-> useTemplate($mpag, 0, 0);	
							$mfpdi 			-> SetTextColor(0, 0, 0);
							
							$mfpdi 			-> SetFont('Arial', '', 7); 
							
							//37 caracteres por celda aprox en el campo Delegado Regional
							
							$filaInicial 	= 65;
									
							$rs = $control -> exec_query($_SESSION['ultima_consulta']);
							
								while ( $row = mysql_fetch_array($rs) ) {
									
										$mfpdi -> SetXY(13,  $filaInicial);
										$mfpdi -> Write(0,   $row['delegado']);
										$mfpdi -> SetXY(75,  $filaInicial);
										$mfpdi -> Write(0,   $row['oficina']);
										$mfpdi -> SetXY(108, $filaInicial);
										$mfpdi -> Write(0,   $row['celular']);
										$mfpdi -> SetXY(138, $filaInicial);
										$mfpdi -> Write(0,   $row['celular2']);
										$mfpdi -> SetXY(164, $filaInicial);
										$mfpdi -> Write(0,   $row['nombre_region']);
									
									$filaInicial += 4;
								}
							$control -> liberar($rs);
					break;
				
				case 4 :
							//Reporte de Delegados Chiapas Solidario
							$mfpdi 			-> setSourceFile('../rpt/delegados_rg_chisol_mpios.pdf');
							$mnomFile		= "rptDelegadosRegionalesChisol";
							$mfpdi 			-> setAuthor('Levi Fco Amanecer');
							$mpag			= $mfpdi -> importPage(1, "/MediaBox");
							$mfpdi 			-> useTemplate($mpag, 0, 0);	
							$mfpdi 			-> SetTextColor(0, 0, 0);
							
							$mfpdi 			-> SetFont('Arial', '', 7); 
							
							$filaInicial	= 65;
							
							$rs = $control -> exec_query($_SESSION['ultima_consulta']);
							
							$mp	= $_SESSION['mp'];
							$mpios;
							
							while ($fila = mysql_fetch_array($rs))
							{	
								if ( ($mp == "TENEJAPA") && ($fila['nombre_region'] == "LAS MARGARITAS") )
									continue;
								if ( $fila['nombre_region'] == "SAN CRISTOBAL DE LAS CASAS")
									$fila['nombre_region'] = "SAN CRISTOBAL";
									
									$mfpdi -> SetXY(13, 	$filaInicial);
									$mfpdi -> Write(0, 	$fila['delegado']);
									$mfpdi -> SetXY(72,	$filaInicial);
									$mfpdi -> Write(0,	$fila['oficina']);
									$mfpdi -> SetXY(108,	$filaInicial);
									$mfpdi -> Write(0,	$fila['celular']);
									$mfpdi -> SetXY(138,	$filaInicial);
									$mfpdi -> Write(0,	$fila['celular2']);
									$mfpdi -> SetXY(164,	$filaInicial);
									$mfpdi -> Write(0,	$fila['nombre_region']);
								
									$mpios = $fila['mpios_abarca'];
									
									if ( $mp == "SUCHIAPA" )
										break;
							}
							
									$filaInicial	= 105;
									$mpios 			= split ('_', $mpios);
									
									for ($i = 0; $i <= count($mpios); $i++) {
											
											$mfpdi -> SetXY(30, $filaInicial);
											$mfpdi -> Cell (150, 5, $mpios[$i], 0 ,0, 'C');
											
											$filaInicial +=3;
									}
					break;
				case 5 :
							//Reporte Enlaces a fin de terminar Contrato
							$mfpdi 			-> setSourceFile('../rpt/enlaces_contrato.pdf');
							$mnomFile		= "rptEnlaces";
							$mfpdi 			-> setAuthor('Levi Fco Amanecer');
							$mpag			= $mfpdi -> importPage(1, "/MediaBox");
							$mfpdi 			-> useTemplate($mpag, 0, 0);	
							$mfpdi 			-> SetTextColor(0, 0, 0);
							
							$mfpdi 			-> SetFont('Arial', '', 7); 
							
							$filaInicial 	= 66;
							$rs 			= $control -> exec_query($_SESSION['ultima_consulta']);
									
									while ( $row = mysql_fetch_array($rs) ) {
											
										if ( $filaInicial <= 165 )
										{				
													$mfpdi -> SetXY(24, 	$filaInicial);
													$mfpdi -> Write(0,  	$row['enlace']);
													$mfpdi -> SetXY(85, 	$filaInicial);
													$mfpdi -> Write(0, 	$row['municipio_adscrito']);
													$mfpdi -> SetXY(135,  $filaInicial);
													$mfpdi -> Write(0,	$row['telefono']);
													$mfpdi -> SetXY(186,	$filaInicial);
													$mfpdi -> Write(0,	$row['rfc']);
													$mfpdi -> SetXY(220,	$filaInicial);
													$mfpdi -> Write(0,	$row['clave_nominal']);
													
													$filaInicial += 4;
										}
										else
										{
													$filaInicial = 60;
													
													$mfpdi 		-> addpage();
													$mfpdi 		-> setSourceFile('../rpt/enlaces_contrato.pdf');
													$mpag 		=  $mfpdi -> importPage(1, "/MediaBox");
													$mfpdi 		-> useTemplate($mpag, 0, 0);		
										}
									}
								$control -> liberar($rs);
					break;
			}
		
unset ($filaInicial, $rs, $numero_enlaces, $numero_paginas, $row, $municipio, $valor_get, $mpios, $mp);

$control -> __destruct();*/
$mfpdi	 -> Output($mnomFile, "I");
?>