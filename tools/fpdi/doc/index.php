<?php
	require_once("funciones/funciones_index.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<title></title> <!--Secretar&iacute;a de Infraestructura (Directorio Telef&oacute;nico).-->
	
	<link rel="stylesheet" href="utilerias/estilos/estilos.css" type="text/css" media="screen">
	<link rel="stylesheet" href="utilerias/estilos/screen.css" type="text/css"  media="screen, projection" />
	<link rel="stylesheet" href="utilerias/tab/css/tab-view.css" type="text/css" media="screen">
	<link href="utilerias/windows_js_1.3/themes/default.css" rel="stylesheet" type="text/css" />
	<link href="utilerias/windows_js_1.3/themes/mac_os_x.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="utilerias/tab/js/ajax.js"></script>
	<script type="text/javascript" src="utilerias/tab/js/tab-view.js"></script>
	<script type="text/javascript" src="utilerias/windows_js_1.3/javascripts/prototype.js"> </script>
	<script type="text/javascript" src="utilerias/windows_js_1.3/javascripts/window.js"> </script>
	
	<script type="text/javascript" src="funciones/funciones.js"></script>
	<script type="text/javascript" src="funciones/funciones_index.js"></script>
	<?php $mxajax->printJavascript();?>
</head>
<body onload="DEFAULT();">
<center>
<div id="header"></div>
<div id="layout">
   	<div id="head_container" class="Estilo50">Directorio Telef&oacute;nico.</div>
   	<div id="container">	
	<table width="780" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="100%">
			<form id="mDatos" name="mDatos" method="post" onsubmit="return false;">
			<table width="100%" border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td width="100%">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%" align="left" class="Estilo1">&Oacute;rgano Administrativo 1:</td>
							<td width="82%" align="left"><select name="cmbOrgano_Admvo_1" id="cmbOrgano_Admvo_1" class="Estilo3"></select></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%" align="left" class="Estilo1">&Oacute;rgano Administrativo 2:</td>
							<td width="82%" align="left"><select name="cmbOrgano_Admvo_2" id="cmbOrgano_Admvo_2" class="Estilo3" onchange="LLENAR_ORGANO_ADMVO(3);"></select></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%" align="left" class="Estilo1">&Oacute;rgano Administrativo 3:</td>
							<td width="82%" align="left"><select name="cmbOrgano_Admvo_3" id="cmbOrgano_Admvo_3" class="Estilo3" onchange="LLENAR_ORGANO_ADMVO(4);"></select></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%" align="left" class="Estilo1">&Oacute;rgano Administrativo 4:</td>
							<td width="82%" align="left"><select name="cmbOrgano_Admvo_4" id="cmbOrgano_Admvo_4" class="Estilo3" onchange="LLENAR_ORGANO_ADMVO(5);"></select></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%" align="left" class="Estilo1">&Oacute;rgano Administrativo 5:</td>
							<td width="82%" align="left"><select name="cmbOrgano_Admvo_5" id="cmbOrgano_Admvo_5" class="Estilo3" onchange="xajax_MOSTRAR_ORGANO_ADMVO(xajax.getFormValues('mDatos'), 5);"></select></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</form>
			</td>
		</tr>
		<tr>
			<td width="100%">&nbsp;</td>
		</tr>
		<tr>
			<td width="100%">
			<div id="mTabPrincipal">
				<div id="mTabOrgano" class="dhtmlgoodies_aTab">
				<form id="mOrgano" name="mOrgano" method="post" onsubmit="return false;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="18%" align="left"  class="Estilo1">&Oacute;rgano Administrativo:</td>
								<td width="82%" align="left"><input name="txtOrgano" id="txtOrgano" type="text" size="122" maxlength="80" class="Estilo2" disabled="True" / >
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">&nbsp;</td>
					</tr>							
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="1">
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="5%" align="left">&nbsp;</td>
								<td width="19%" align="center" class="Estilo1">Apellido Paterno:</td>
								<td width="19%" align="center" class="Estilo1">Apellido Materno:</td>
								<td width="19%" align="center" class="Estilo1">Nombre:</td>
								<td width="19%">&nbsp;</td>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="5%" align="left"><input name="txtTitulo_Titular" id="txtTitulo_Titular" type="text" size="5" maxlength="10" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%" align="left"><input name="txtApellido_Paterno_Titular" id="txtApellido_Paterno_Titular" type="text" size="25" maxlength="30" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%" align="left"><input name="txtApellido_Materno_Titular" id="txtApellido_Materno_Titular" type="text" size="25" maxlength="30" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%" align="left"><input name="txtNombre_Titular" id="txtNombre_Titular" type="text" size="25" maxlength="30" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%">&nbsp;</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="27%" align="left" class="Estilo1">Tel&eacute;fono:</td>
										<td width="73%" align="left"><input name="txtTelefono_Titular" id="txtTelefono_Titular" type="text" size="25" maxlength="12" class="Estilo2" disabled="True" / >
										</td>
									</tr>
								</table>
								</td>
								<td width="30%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="26%" align="left" class="Estilo1">Extensi&oacute;n:</td>
										<td width="74%" align="left"><input name="txtExtension_Titular" id="txtExtension_Titular" type="text" size="10" maxlength="10" class="Estilo2"  disabled="True"/ >
										</td>
									</tr>
								</table>
								</td>
								<td width="40%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="54%" align="right" class="Estilo1">Fax:</td>
										<td width="46%" align="right"><input name="txtFax_Titular" id="txtFax_Titular" type="text" size="23" maxlength="12" class="Estilo2"  disabled="True"/ >
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="27%" align="left" class="Estilo1">Celular:</td>
										<td width="73%" align="left"><input name="txtCelular_Titular" id="txtCelular_Titular" type="text" size="25" maxlength="13" class="Estilo2" disabled="True" / >
										</td>
									</tr>
								</table>
								</td>
								<td width="70%">
								<table width="100%" border="0" cellpadding="0" cellspacing="1">
									<tr>
										<td width="11%" align="left" class="Estilo1">E-Mail:</td>
										<td width="89%" align="left"><input name="txtE_Mail_Titular" id="txtE_Mail_Titular" type="text" size="50" maxlength="50" class="Estilo2"  disabled="True"/ >
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="1">
								<tr>
									<td width="72%" align="left" class="Estilo1"><input name="cmdImprimir" id="cmdImprimir" type="button" value="Imprimir" class="Estilo2" onclick="ABRIR_VENTANA();" disabled="True" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdNuevo_Guardar_Organo" id="cmdNuevo_Guardar_Organo" type="button" value="Nuevo" class="Estilo2" onclick="OPCIONES_NUEVO();" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdModificar_Organo" id="cmdModificar_Organo" type="button" value="Modificar" class="Estilo2" onclick="xajax_HABILITAR_MODIFICAR_ORGANO();" disabled="True" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdEliminar_Organo" id="cmdEliminar_Organo" type="button" value="Eliminar" class="Estilo2" onclick="ELIMINAR_ORGANO();" disabled="True" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdCancelar_Organo" id="cmdCancelar_Organo" type="button" value="Cancelar" class="Estilo2" onclick="xajax_CANCELAR_ORGANO(xajax.getFormValues('mDatos'));" disabled="True" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</form>
				</div>
				<div id="mTabFuncion" class="dhtmlgoodies_aTab">
				<form id="mFuncion" name="mOrgano" method="post" onsubmit="return false;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="1">
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="5%" align="left">&nbsp;</td>
								<td width="19%" align="center" class="Estilo1">Apellido Paterno:</td>
								<td width="19%" align="center" class="Estilo1">Apellido Materno:</td>
								<td width="19%" align="center" class="Estilo1">Nombre:</td>
								<td width="19%">&nbsp;</td>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="5%" align="left"><input name="txtTitulo_Funcion" id="txtTitulo_Funcion" type="text" size="5" maxlength="10" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%" align="left"><input name="txtApellido_Paterno_Funcion" id="txtApellido_Paterno_Funcion" type="text" size="25" maxlength="30" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%" align="left"><input name="txtApellido_Materno_Funcion" id="txtApellido_Materno_Funcion" type="text" size="25" maxlength="30" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%" align="left"><input name="txtNombre_Funcion" id="txtNombre_Funcion" type="text" size="25" maxlength="30" class="Estilo2" disabled="True" / >
								</td>
								<td width="19%">&nbsp;</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td width="8%" align="left" class="Estilo1">Funci&oacute;n:</td>
								<td width="92%" align="left"><select name="cmbFuncion" id="cmbFuncion" class="Estilo3" disabled="True"></select></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="27%" align="left" class="Estilo1">Tel&eacute;fono:</td>
										<td width="73%" align="left"><input name="txtTelefono_Funcion" id="txtTelefono_Funcion" type="text" size="25" maxlength="12" class="Estilo2" disabled="True" / >
										</td>
									</tr>
								</table>
								</td>
								<td width="30%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="26%" align="left" class="Estilo1">Extensi&oacute;n:</td>
										<td width="74%" align="left"><input name="txtExtension_Funcion" id="txtExtension_Funcion" type="text" size="10" maxlength="10" class="Estilo2"  disabled="True"/ >
										</td>
									</tr>
								</table>
								</td>
								<td width="40%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="54%" align="right" class="Estilo1">Fax:</td>
										<td width="46%" align="right"><input name="txtFax_Funcion" id="txtFax_Funcion" type="text" size="23" maxlength="12" class="Estilo2"  disabled="True"/ >
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30%">
								<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="27%" align="left" class="Estilo1">Celular:</td>
										<td width="73%" align="left"><input name="txtCelular_Funcion" id="txtCelular_Funcion" type="text" size="25" maxlength="13" class="Estilo2" disabled="True" / >
										</td>
									</tr>
								</table>
								</td>
								<td width="70%">
								<table width="100%" border="0" cellpadding="0" cellspacing="1">
									<tr>
										<td width="11%" align="left" class="Estilo1">E-Mail:</td>
										<td width="89%" align="left"><input name="txtE_Mail_Funcion" id="txtE_Mail_Funcion" type="text" size="50" maxlength="50" class="Estilo2"  disabled="True"/ >
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%"><div id="mListado_Funcion"></div></td>
					</tr>
					<tr>
						<td width="100%">
							<table width="100%" border="0" cellpadding="0" cellspacing="1">
								<tr>
									<td width="79%" align="right" class="Estilo1"><input name="cmdNuevo_Guardar_Funcion" id="cmdNuevo_Guardar_Funcion" type="button" value="Nuevo" class="Estilo2" onclick="OPCIONES_NUEVO_FUNCION();" disabled="True" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdModificar_Funcion" id="cmdModificar_Funcion" type="button" value="Modificar" class="Estilo2" onclick="xajax_HABILITAR_MODIFICAR_FUNCION();" disabled="True" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdEliminar_Funcion" id="cmdEliminar_Funcion" type="button" value="Eliminar" class="Estilo2" onclick="ELIMINAR_FUNCION();" disabled="True" /></td>
									<td width="7%" align="right" class="Estilo1"><input name="cmdCancelar_Funcion" id="cmdCancelar_Funcion" type="button" value="Cancelar" class="Estilo2" onclick="xajax_CANCELAR_FUNCION(0);" disabled="True" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</form>
				</div>
			</div>
			</td>
		</tr>
	</table>
	</div>
	<div id="footer">
    	<div id="page-bottom"></div>
    	<p class="f-left">Copyright &copy; 2008</p>
    	<p class="f-right">Tuxtla Guti&eacute;rrez Chiapas. | </p>
  	</div>
</div>

<input name="txt_Tipo_Guardar_Organo" id="txt_Tipo_Guardar_Organo" type="text" value="0" style="visibility:hidden" disabled="True"/ >
<input name="txt_Tipo_Guardar_Funcion" id="txt_Tipo_Guardar_Funcion" type="text" value="0" style="visibility:hidden" disabled="True"/ >
<input name="txt_Cve_Funcion" id="txt_Cve_Funcion" type="text" value="" style="visibility:hidden" disabled="True"/ >

<script type="text/javascript">
	initTabs('mTabPrincipal',Array('Titular','Funciones'),0,780,"");
</script>
</center>
</body>
</html>