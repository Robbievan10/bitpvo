<?php
	require_once("controller/session.php");
	require_once("controller/expenses-record.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
        
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>            
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
	</head>
	<body class="overflow-hidden" onLoad="_default();">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Registros</li>
						<li class="active">Gastos</li>
					</ul>
				</div><!-- /breadcrumb-->
					<div class="panel-group" id="pnbrexpense" role="tablist" aria-multiselectable="true">
							<div class="panel-heading" role="tab" id="vheadingExpenseFilterData">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#pnbrexpense" href="#vexpenseFilterData" aria-expanded="false" aria-controls="venterpriseAccountData" title="Clic para Ocultar/Desocultar Filtros">
										<span class="text-info textCaption-1">Filtros</span>
									</a>
								</h4>
							</div>
							<div id="vexpenseFilterData" class="panel-collapse collapse" role="tabpanel" aria-labelledby="vheadingExpenseFilterData">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-1">&nbsp;</div>
										<div class="col-md-10">
											<form id="vfilterData" name="vfilterData" method="post" onSubmit="return false;">
											<div class="row">
												<div class="col-md-2 form-group">
													<label for="dtpckrstartDate">
														<span class="textCaption-2">Fecha Inicial:</span>
													</label>
													<input id="dtpckrstartDate" name="dtpckrstartDate" maxlength="10" style="width:100%" />
												</div>
												<div class="col-md-2 form-group">
													<label for="dtpckrendDate">
														<span class="textCaption-2">Fecha Final:</span>
													</label>
													<input id="dtpckrendDate" name="dtpckrendDate" maxlength="10" style="width:100%" />
												</div>
												<div class="col-md-8 form-group">
													<label for="cmbexpensesList">
														<span class="textCaption-2">Gasto:</span>
													</label>
													<select id="cmbexpensesList" name="cmbexpensesList" style="text-align:left; width:100%" ></select>
												</div>
											</div>
                                            <div class="row">
												<div class="col-md-12 form-group">
													<label for="cmbroutesList">
														<span class="textCaption-2">Ruta:</span>
													</label>
													<select id="cmbroutesList" name="cmbroutesList" style="text-align:left; width:100%"></select>
												</div>
											</div>
											</form>
											<div class="row">
												<div style="text-align:center" class="col-md-12">
													<button type="button" id="cmdacceptFilter" class="k-button" title="Filtrar Datos">
														<span class="k-icon k-i-funnel"></span>&nbsp;Filtrar
													</button>
													<button type="button" id="cmdcancelFilter" class="k-button" title="Limpiar Filtros">
														<span class="k-icon k-i-funnel-clear"></span>&nbsp;Limpiar
													</button>
												</div>
											</div>
										</div>
										<div class="col-md-1">&nbsp;</div>
									</div>
								</div>
							</div>
							<div class="panel-heading" role="tab" id="vheadingExpenseListData" class="active">
								<h4 class="panel-title">
									<span class="text-info textCaption-1">Gastos</span>
								</h4>
							</div>
							<div id="vexpenseListData" role="tabpanel" aria-labelledby="vheadingExpenseListData">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-1">&nbsp;</div>
										<div class="col-md-10">
											<div class="row">
												<div style="text-align:right" class="col-md-12 form-group">
													<button type="button" id="cmdaddExpense" class="k-button" title="Agregar Gasto">
														<span class="k-icon k-i-plus"></span>&nbsp;Agregar
													</button>
													<button type="button" id="cmdupdateExpense" class="k-button" title="Modificar datos del Gasto">
														<span class="k-icon k-i-pencil"></span>&nbsp;Modificar
													</button>
													<button type="button" id="cmddeleteExpense" class="k-button" title="Eliminar Gasto">
														<span class="k-icon k-i-close"></span>&nbsp;Eliminar
													</button>
												</div>
											</div>
											<div class="row">
												<div id="vexpensesRecordList" class="col-md-12">&nbsp;</div>
											</div>
										</div>
										<div class="col-md-1">&nbsp;</div>
									</div>
								</div>
							</div>
					</div>
			</div><!-- /main-container -->

		</div>
		<!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
		
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.view.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/js/cultures/kendo.culture.es-MX.min.js"></script>
		<!-- DatePicker -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.calendar.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.datepicker.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
		<!-- Grid -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.columnsorter.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.pager.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.grid.js"></script>
                
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/expenses-record.js"></script>
        
		<?php $vxajax->printJavascript(); ?>
        
	</body>
</html>