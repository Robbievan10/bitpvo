<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLExpense
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflExpenses, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLExpense::queryToDataBase($vflExpenses, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflExpenses)
	 {
		try{
			return clscDLExpense::total($vflExpenses);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>