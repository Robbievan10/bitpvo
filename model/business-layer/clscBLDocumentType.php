<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLDocumentType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflDocumentTypes, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLDocumentType::queryToDataBase($vflDocumentTypes, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflDocumentTypes)
	 {
		try{
			return clscDLDocumentType::total($vflDocumentTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>