<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductSalePayment
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsSalesPayments, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductSalePayment::queryToDataBase($vflProductsSalesPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryByRouteToDataBase($vflProductsSalesPayments, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductSalePayment::queryByRouteToDataBase($vflProductsSalesPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryGroupByCustomerToDataBase($vflProductsSalesPayments, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSalePayment::queryGroupByCustomerToDataBase($vflProductsSalesPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsSalesPayments)
	 {
		try{
			return clscDLProductSalePayment::total($vflProductsSalesPayments);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>