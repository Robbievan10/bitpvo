<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLProductLowType
 {
    public function __construct() { }
	
    
    public static function queryToDataBase($vflProductLowType, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
			$vstatus=clspDLProductLowType::queryToDataBase($vflProductLowType, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>