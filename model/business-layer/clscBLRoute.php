<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLRoute
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflRoutes, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLRoute::queryToDataBase($vflRoutes, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflRoutes)
	 {
		try{
			return clscDLRoute::total($vflRoutes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>