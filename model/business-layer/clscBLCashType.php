<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLCashType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCashTypes, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLCashType::queryToDataBase($vflCashTypes, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflCashTypes)
	 {
		try{
			return clscDLCashType::total($vflCashTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>