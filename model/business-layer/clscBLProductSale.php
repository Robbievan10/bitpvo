<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductSale
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsSales, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSale::queryToDataBase($vflProductsSales, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsSales)
	 {
		try{
			return clscDLProductSale::total($vflProductsSales);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>