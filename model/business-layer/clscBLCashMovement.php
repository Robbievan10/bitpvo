<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLCashMovement
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCashMovements, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLCashMovement::queryToDataBase($vflCashMovements, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryMovementsGroupByCashAndCashierToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLCashMovement::queryMovementsGroupByCashAndCashierToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflCashMovements)
	 {
		try{
			return clscDLCashMovement::total($vflCashMovements);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>