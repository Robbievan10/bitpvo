<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashMovementStatus.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLCashMovementStatus
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCashMovementStatuses, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLCashMovementStatus::queryToDataBase($vflCashMovementStatuses, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflCashMovementStatuses)
	 {
		try{
			return clscDLCashMovementStatus::total($vflCashMovementStatuses);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>