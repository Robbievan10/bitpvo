<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLExpenseDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLExpenseDetail
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflExpensesDetails, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLExpenseDetail::queryToDataBase($vflExpensesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryGroupByNameToDataBase($vflExpensesDetails, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLExpenseDetail::queryGroupByNameToDataBase($vflExpensesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflExpensesDetails)
	 {
		try{
			return clscDLExpenseDetail::total($vflExpensesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function totalAmount($vflExpensesDetails)
	 {
		try{
			return clscDLExpenseDetail::totalAmount($vflExpensesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public function __destruct() { }
 }

?>