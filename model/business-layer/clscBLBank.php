<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLBank.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLBank
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflBanks, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLBank::queryToDataBase($vflBanks, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflBanks)
	 {
		try{
			return clscDLBank::total($vflBanks);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>