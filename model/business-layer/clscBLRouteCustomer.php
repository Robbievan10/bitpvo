<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLRouteCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLRouteCustomer
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflRoutesCustomers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLRouteCustomer::queryToDataBase($vflRoutesCustomers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflRoutesCustomers)
	 {
		try{
			return clscDLRouteCustomer::total($vflRoutesCustomers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>