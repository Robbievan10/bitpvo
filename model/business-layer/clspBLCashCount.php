<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLCashCount
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflCashCount, $vflCashCountCashList, $vflCashCountDocumentList)
	 {
		try{
		    $vcashCountCashTotal=clscDLCashCountCash::total($vflCashCountCashList);
            $vcashCountDocumentTotal=clscDLCashCountDocument::total($vflCashCountDocumentList);
            if ( $vcashCountCashTotal>0 || $vcashCountDocumentTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vmySql->startTransaction();
                if ( clspDLCashCount::addToDataBase($vflCashCount, $vmySql)==1 ){
                    if ( $vcashCountCashTotal>0 ){
                        if ( clscDLCashCountCash::addToDataBase($vflCashCountCashList, $vflCashCount, $vmySql)!=1 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -3;
                        }
                    }
                    if ( $vcashCountDocumentTotal>0 ){
                        if ( clscDLCashCountDocument::addToDataBase($vflCashCountDocumentList, $vflCashCount, $vmySql)!=1 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -2;
                        }
                    }
                }
                else{
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                unset($vmySql);
            }
            else{
                return 0;
            }	
			unset($vflCashCount, $vflCashCountCashList, $vflCashCountDocumentList);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateInDataBase($vflCashCount, $vflCashCountCashList, $vflCashCountDocumentList)
	 {
		try{
            $vcashCountCashTotal=clscDLCashCountCash::total($vflCashCountCashList);
            $vcashCountDocumentTotal=clscDLCashCountDocument::total($vflCashCountDocumentList);
            if ( $vcashCountCashTotal>0 || $vcashCountDocumentTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vmySql->startTransaction();
                
                clspDLCashCount::updateInDataBase($vflCashCount, $vmySql);
                
                $vfilter ="WHERE p_cashcountcash.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
                $vfilter.="AND p_cashcountcash.id_cash=" . $vflCashCount->cash->idCash . " ";
                $vfilter.="AND p_cashcountcash.id_cashCount=" . $vflCashCount->idCashCount;
                $vflPriorCashCountCashList= new clscFLCashCountCash();
                clscDLCashCountCash::queryToDataBase($vflPriorCashCountCashList, $vfilter, $vmySql);
                clscDLCashCountCash::deleteInDataBase($vflPriorCashCountCashList, $vmySql);
                if ( $vcashCountCashTotal>0 ){
                    if ( clscDLCashCountCash::addToDataBase($vflCashCountCashList, $vflCashCount, $vmySql)!=1 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -2;
                    }
                }
                
                $vfilter ="WHERE p_cashcountdocument.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
                $vfilter.="AND p_cashcountdocument.id_cash=" . $vflCashCount->cash->idCash . " ";
                $vfilter.="AND p_cashcountdocument.id_cashCount=" . $vflCashCount->idCashCount;
                $vflPriorCashCountDocumentList= new clscFLCashCountDocument();
                clscDLCashCountDocument::queryToDataBase($vflPriorCashCountDocumentList, $vfilter, $vmySql);
                clscDLCashCountDocument::deleteInDataBase($vflPriorCashCountDocumentList, $vmySql);
                if ( $vcashCountDocumentTotal>0 ){
                    if ( clscDLCashCountDocument::addToDataBase($vflCashCountDocumentList, $vflCashCount, $vmySql)!=1 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -1;
                    }
                }
                
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                unset($vmySql, $vfilter, $vflPriorCashCountCashList, $vcashCountCashTotal, $vflPriorCashCountDocumentList, $vcashCountDocumentTotal);
            }
            else{
                return 0;
            }
            
            unset($vcashCountCashTotal, $vcashCountDocumentTotal);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflCashCount)
	 {
		try{
            $vmySql= new clspMySql();
            $vmySql->openConnection();
            $vmySql->startTransaction();
            
            $vfilter ="WHERE p_cashcountcash.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
            $vfilter.="AND p_cashcountcash.id_cash=" . $vflCashCount->cash->idCash . " ";
            $vfilter.="AND p_cashcountcash.id_cashCount=" . $vflCashCount->idCashCount;
            $vflCashCountCashList= new clscFLCashCountCash();
            clscDLCashCountCash::queryToDataBase($vflCashCountCashList, $vfilter, $vmySql);
            if ( clscDLCashCountCash::total($vflCashCountCashList)>0 ){
                if ( clscDLCashCountCash::deleteInDataBase($vflCashCountCashList, $vmySql)!=1 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -2;
                }
            }
            
            $vfilter ="WHERE p_cashcountdocument.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
            $vfilter.="AND p_cashcountdocument.id_cash=" . $vflCashCount->cash->idCash . " ";
            $vfilter.="AND p_cashcountdocument.id_cashCount=" . $vflCashCount->idCashCount;
            $vflCashCountDocumentList= new clscFLCashCountDocument();
            clscDLCashCountDocument::queryToDataBase($vflCashCountDocumentList, $vfilter, $vmySql);
            if ( clscDLCashCountDocument::total($vflCashCountDocumentList)>0 ){
                if ( clscDLCashCountDocument::deleteInDataBase($vflCashCountDocumentList, $vmySql)!=1 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
            }
            
            if ( clspDLCashCount::deleteInDataBase($vflCashCount, $vmySql)==0 ){
                $vmySql->rollbackTransaction();
                $vmySql->closeConnection();
                return 0;
            }
            $vmySql->commitTransaction();
			$vmySql->closeConnection();
            
            unset($vmySql, $vfilter, $vflCashCountCashList, $vflCashCountDocumentList);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
     
	public static function queryToDataBase($vflCashCount)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCashCount::queryToDataBase($vflCashCount, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }

?>