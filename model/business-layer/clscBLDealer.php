<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLDealer
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflDeliverers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLDealer::queryToDataBase($vflDeliverers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflDeliverers)
	 {
		try{
			return clscDLDealer::total($vflDeliverers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>