<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLEnterpriseUser
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflEnterpriseUsers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLEnterpriseUser::queryToDataBase($vflEnterpriseUsers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflEnterpriseUsers)
	 {
		try{
			return clscDLEnterpriseUser::total($vflEnterpriseUsers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>