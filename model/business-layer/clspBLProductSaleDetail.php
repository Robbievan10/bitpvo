<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLProductSaleDetail
 {
    public function __construct() { }
	
    
    public static function totalSalePrice($vflProductSaleDetail)
	 {
		try{
			return clspDLProductSaleDetail::totalSalePrice($vflProductSaleDetail);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }

?>