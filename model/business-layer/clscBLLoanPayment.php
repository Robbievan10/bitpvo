<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLLoanPayment
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflLoansPayments, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLLoanPayment::queryToDataBase($vflLoansPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryGroupByDealerToDataBase($vflLoansPayments, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLLoanPayment::queryGroupByDealerToDataBase($vflLoansPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflLoansPayments)
	 {
		try{
			return clscDLLoanPayment::total($vflLoansPayments);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>