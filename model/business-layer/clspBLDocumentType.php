<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLDocumentType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLDocumentType
 {
    public function __construct() { }
	
    
	public static function queryToDataBase($vflDocumentType)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLDocumentType::queryToDataBase($vflDocumentType, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>