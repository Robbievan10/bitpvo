<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLState.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLState
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflStates, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLState::queryToDataBase($vflStates, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflStates)
	 {
		try{
			return clscDLState::total($vflStates);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>