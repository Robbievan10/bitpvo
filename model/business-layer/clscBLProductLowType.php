<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductLowType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductLowType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductLowTypes, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductLowType::queryToDataBase($vflProductLowTypes, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductLowTypes)
	 {
		try{
			return clscDLProductLowType::total($vflProductLowTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>