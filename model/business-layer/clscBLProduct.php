<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProduct
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProducts, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProduct::queryToDataBase($vflProducts, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProducts)
	 {
		try{
			return clscDLProduct::total($vflProducts);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>