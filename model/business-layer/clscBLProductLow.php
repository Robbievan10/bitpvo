<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductLow
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsLow, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductLow::queryToDataBase($vflProductsLow, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsLow)
	 {
		try{
			return clscDLProductLow::total($vflProductsLow);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>