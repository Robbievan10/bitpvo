<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLMunicipality.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLMunicipality
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflMunicipalities, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLMunicipality::queryToDataBase($vflMunicipalities, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflMunicipalities)
	 {
		try{
			return clscDLMunicipality::total($vflMunicipalities);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>