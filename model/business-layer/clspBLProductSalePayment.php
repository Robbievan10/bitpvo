<?php


require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLProductSaleDetail.php");

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductSaleDetail.php");

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLProductSalePayment.php");

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductSalePayment.php");

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductSalePayment.php");

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductSale.php");

require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");



class clspBLProductSalePayment

 {

    public function __construct() { }

	

    

    public static function addToDataBase($vflProductSalePayment)

	 {

		try{

			$vmySql= new clspMySql();

			$vmySql->openConnection();

			

			$vfilter ="WHERE p_productsaledetail.id_enterprise=" . $vflProductSalePayment->productSale->enterprise->idEnterprise . " ";

			$vfilter.="AND p_productsaledetail.id_productSale='" . $vflProductSalePayment->productSale->idProductSale . "'";

			$vproductsSalesDetails= new clscFLProductSaleDetail();

            clscDLProductSaleDetail::queryToDataBase($vproductsSalesDetails, $vfilter, $vmySql);

			$vproductsSaleTotalPrice=clscDLProductSaleDetail::totalSalePrice($vproductsSalesDetails);

			

			$vfilter ="WHERE p_productsalepayment.id_enterprise=" . $vflProductSalePayment->productSale->enterprise->idEnterprise . " ";

			$vfilter.="AND p_productsalepayment.id_productSale='" . $vflProductSalePayment->productSale->idProductSale . "'";

			$vproductsSalesPayments= new clscFLProductSalePayment();

        	clscDLProductSalePayment::queryToDataBase($vproductsSalesPayments, $vfilter, $vmySql);

			$vproductsSalePaymentTotalAmount=clscDLProductSalePayment::totalAmount($vproductsSalesPayments);

			


if ( number_format($vproductsSaleTotalPrice, 2, '.', '')>=(number_format($vproductsSalePaymentTotalAmount, 2, '.', '') + number_format($vflProductSalePayment->amount, 2, '.', '')) ){
				$vmySql->startTransaction();

				if ( clspDLProductSalePayment::addToDataBase($vflProductSalePayment, $vmySql, 1)==0 ){

					$vmySql->rollbackTransaction();

					$vmySql->closeConnection();

					return -1;

				}

				$vflProductSalePayment->productSale->operationStatus->idOperationStatus=1;

				if (number_format($vproductsSaleTotalPrice, 2, '.', '')
 == number_format(($vproductsSalePaymentTotalAmount + $vflProductSalePayment->amount), 2, '.', '')){





					$vflProductSalePayment->productSale->operationStatus->idOperationStatus=2;

				}

				clspDLProductSale::updateStatusInDataBase($vflProductSalePayment->productSale, $vmySql);

				$vmySql->commitTransaction();

			}

			else{

				return 0;

			}

			$vmySql->closeConnection();

			

			unset($vmySql, $vfilter, $vproductsSalesDetails, $vproductsSaleTotalPrice, $vproductsSalesPayments,

				  $vproductsSalePaymentTotalAmount);

			return 1;

		}

		catch (Exception $vexception){

			throw new Exception($vexception->getMessage(), $vexception->getCode());

		}

	 }

     

    public static function deleteInDataBase($vflProductSalePayment)

	 {

		try{

			$vmySql= new clspMySql();

			$vmySql->openConnection();

			$vmySql->startTransaction();

			if ( clspDLProductSalePayment::deleteInDataBase($vflProductSalePayment, $vmySql, 1)==1 ){

				$vfilter ="WHERE p_productsalepayment.id_enterprise=" . $vflProductSalePayment->productSale->enterprise->idEnterprise . " ";

				$vfilter.="AND p_productsalepayment.id_productSale='" . $vflProductSalePayment->productSale->idProductSale . "'";

				$vproductsSalesPayments= new clscFLProductSalePayment();

        		clscDLProductSalePayment::queryToDataBase($vproductsSalesPayments, $vfilter, $vmySql);

				$vflProductSalePayment->productSale->operationStatus->idOperationStatus=1;

				if ( clscDLProductSalePayment::total($vproductsSalesPayments)==0 ){

					$vflProductSalePayment->productSale->operationStatus->idOperationStatus=0;		

				}

				clspDLProductSale::updateStatusInDataBase($vflProductSalePayment->productSale, $vmySql);

				

				unset($vfilter, $vproductsSalesPayments);

			}

			else{

				$vmySql->rollbackTransaction();

				$vmySql->closeConnection();

				return 0;

			}

			$vmySql->commitTransaction();

			$vmySql->closeConnection();

			

			unset($vmySql);

			return 1;

		}

		catch (Exception $vexception){

			throw new Exception($vexception->getMessage(), $vexception->getCode());

		}

	 }

	 

	 

	public function __destruct() { }

 }


?>
