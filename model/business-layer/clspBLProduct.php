<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductPrice.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductPrice.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLProduct
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflProduct)
	 {
		try{
            $vstatus=-1;
            $vflOtherProduct=new clspFLProduct();
            $vflOtherProduct->enterprise->idEnterprise=$vflProduct->enterprise->idEnterprise;
            $vflOtherProduct->idProduct=$vflProduct->idProduct;
            $vflOtherProduct->key=$vflProduct->key;
            if ( self::queryToDataBaseByKey($vflOtherProduct)==0 ){
                $vflOtherProduct->idProduct=$vflProduct->idProduct;
                $vflOtherProduct->barCode=$vflProduct->barCode;
                if ( self::queryToDataBaseByBarCode($vflOtherProduct)==0 ){
                    $vmySql= new clspMySql();
                    $vmySql->openConnection();
                    $vstatus=clspDLProduct::addToDataBase($vflProduct, $vmySql);
                    $vmySql->closeConnection();
			
                    unset($vmySql);
                }
                else{
                    $vstatus=-2;
                }
            }
            
            unset($vflOtherProduct);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProduct)
	 {
		try{
            $vstatus=-1;
            $vflOtherProduct=new clspFLProduct();
            $vflOtherProduct->enterprise->idEnterprise=$vflProduct->enterprise->idEnterprise;
            $vflOtherProduct->idProduct=$vflProduct->idProduct;
            $vflOtherProduct->key=$vflProduct->key;
            self::queryToDataBaseByKey($vflOtherProduct);
            if ( $vflProduct->idProduct==$vflOtherProduct->idProduct ){
                $vflOtherProduct->idProduct=$vflProduct->idProduct;
                $vflOtherProduct->barCode=$vflProduct->barCode;
                self::queryToDataBaseByBarCode($vflOtherProduct);
                if ( $vflProduct->idProduct==$vflOtherProduct->idProduct ){
                    $vmySql= new clspMySql();
                    $vmySql->openConnection();
                    $vstatus=clspDLProduct::updateInDataBase($vflProduct, $vmySql);
                    $vmySql->closeConnection();
                
                    unset($vmySql);
                }
                else{
                    $vstatus=-2;
                }
            }
            			
			unset($vflOtherProduct);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function recordSalePriceInDataBase($vflProduct)
	 {
		try{
            $vmySql= new clspMySql();
			$vmySql->openConnection();
            $vmySql->startTransaction();
            if ( clspDLProduct::updateSalePriceInDataBase($vflProduct, $vmySql)==1 ){
                $vflProductPrice= new clspFLProductPrice();
                $vflProductPrice->product= $vflProduct;
                $vflProductPrice->price= $vflProduct->salePrice;
                if ( clspDLProductPrice::addToDataBase($vflProductPrice, $vmySql)!=1 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return 0;
                }
                
                unset($vflProductPrice);
            }
            else{
                $vmySql->rollbackTransaction();
                $vmySql->closeConnection();
                return -1;
            }
			$vmySql->commitTransaction();
			$vmySql->closeConnection();
			
			unset($vmySql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProduct)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProduct::deleteInDataBase($vflProduct, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProduct)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProduct::queryToDataBase($vflProduct, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBaseByKey($vflProduct)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProduct::queryToDataBaseByKey($vflProduct, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByBarCode($vflProduct)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProduct::queryToDataBaseByBarCode($vflProduct, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public function __destruct() { }
 }

?>