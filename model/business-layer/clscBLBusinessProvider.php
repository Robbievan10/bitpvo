<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLBusinessProvider
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflBusinessProviders, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLBusinessProvider::queryToDataBase($vflBusinessProviders, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflBusinessProviders)
	 {
		try{
			return clscDLBusinessProvider::total($vflBusinessProviders);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>