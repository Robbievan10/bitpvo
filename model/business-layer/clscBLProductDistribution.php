<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductDistribution
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsDistributions, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductDistribution::queryToDataBase($vflProductsDistributions, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsDistributions)
	 {
		try{
			return clscDLProductDistribution::total($vflProductsDistributions);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>