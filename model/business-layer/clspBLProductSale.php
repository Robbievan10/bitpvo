<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductSalePayment.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLProductSale
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflProductSale, $vflProductsSalesDetails)
	 {
		try{
			$vsaleProductsTotal=clscDLProductSaleDetail::total($vflProductsSalesDetails);
            if ( $vsaleProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vmySql->startTransaction();
                if ( clspDLProductSale::addToDataBase($vflProductSale, $vmySql)==1 ){
                    for($vi=0; $vi<$vsaleProductsTotal; $vi++){
                        $vflProductsSalesDetails->productsSalesDetails[$vi]->productSale=$vflProductSale;
                        
                        $vflProduct= new clspFLProduct();
                        $vflProduct->enterprise->idEnterprise=$vflProductsSalesDetails->productsSalesDetails[$vi]->productSale->enterprise->idEnterprise;
                        $vflProduct->idProduct=$vflProductsSalesDetails->productsSalesDetails[$vi]->product->idProduct;
						clspDLProduct::queryToDataBase($vflProduct, $vmySql);
                        
                        $vflProductsSalesDetails->productsSalesDetails[$vi]->purchasePrice=$vflProduct->purchasePrice;
                        if ( clspDLProductSaleDetail::addToDataBase($vflProductsSalesDetails->productsSalesDetails[$vi], $vmySql)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -5;
                        }
                        
                        $vsaleAmount=$vflProductsSalesDetails->productsSalesDetails[$vi]->saleAmount;
                        $vweightInKg=$vflProduct->weightInKg;
						if ( $vflProduct->stock>=$vflProductsSalesDetails->productsSalesDetails[$vi]->saleAmount ){
							if ( clspDLProduct::decreaseStockInDataBase($vflProduct, $vmySql, $vsaleAmount, $vsaleAmount*$vweightInKg)==0 ){
                            	$vmySql->rollbackTransaction();
	                            $vmySql->closeConnection();
    	                        return -4;
        	                }
						}
						else{
							$vmySql->rollbackTransaction();
							$vmySql->closeConnection();
							throw new Exception("Imposible registrar la venta de productos.\nLa cantidad del producto <" . $vflProduct->name .
												"> excede al total en existencias <" . $vflProduct->stock . ">" , -3);
						}
						
						unset($vflProduct, $vsaleAmount, $vweightInKg);
                    }
                    if ( $vflProductSale->operationType->idOperationType==1 ){
                        $vflProductSalePayment= new clspFLProductSalePayment();
                        $vflProductSalePayment->productSale=$vflProductSale;
                        $vflProductSalePayment->enterprise->idEnterprise=$vflProductSalePayment->productSale->enterprise->idEnterprise;
                        $vflProductSalePayment->paymentType->idPaymentType=2;
                        $vflProductSalePayment->paymentDate=$vflProductSale->productSaleDate;
                        $vflProductSalePayment->amount=clscDLProductSaleDetail::totalSalePrice($vflProductsSalesDetails);
                        if ( clspDLProductSalePayment::addToDataBase($vflProductSalePayment, $vmySql, 1)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -2;
                        }
                        unset($vflProductSalePayment);
                    }
                }
                else{
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                unset($vmySql);
            }
            else{
                return 0;
            }	
			unset($vsaleProductsTotal);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	public static function cancelInDataBase($vflProductSale, $vflProductsSalesDetails)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vmySql->startTransaction();
			if ( clspDLProductSale::cancelInDataBase($vflProductSale, $vmySql)==1 ){
				$vsaleProductsTotal=clscDLProductSaleDetail::total($vflProductsSalesDetails);
            	if ( $vsaleProductsTotal>0 ){
					for($vi=0; $vi<$vsaleProductsTotal; $vi++){
						$vflProduct= new clspFLProduct();
                        $vflProduct->enterprise->idEnterprise=$vflProductsSalesDetails->productsSalesDetails[$vi]->productSale->enterprise->idEnterprise;
                        $vflProduct->idProduct=$vflProductsSalesDetails->productsSalesDetails[$vi]->product->idProduct;
						clspDLProduct::queryToDataBase($vflProduct, $vmySql);
                        $vsaleAmount=$vflProductsSalesDetails->productsSalesDetails[$vi]->saleAmount;
                        $vweightInKg=$vflProduct->weightInKg;
						if ( clspDLProduct::appendStockInDataBase($vflProduct, $vmySql, $vsaleAmount, $vsaleAmount*$vweightInKg)==0 ){
                            $vmySql->rollbackTransaction();
							$vmySql->closeConnection();
							return -2;
						}
						unset($vflProduct);
					}
					unset($vi);
				}
				else{
					$vmySql->rollbackTransaction();
					$vmySql->closeConnection();
					return -1;
				}
				unset($vsaleProductsTotal);
			}
			else{
				$vmySql->rollbackTransaction();
				$vmySql->closeConnection();
				return 0;
			}
			$vmySql->commitTransaction();
			$vmySql->closeConnection();
			
			unset($vmySql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductSale)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductSale::queryToDataBase($vflProductSale, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }

?>