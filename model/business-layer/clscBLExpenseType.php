<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLExpenseType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLExpenseType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflExpenseTypes, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLExpenseType::queryToDataBase($vflExpenseTypes, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflExpenseTypes)
	 {
		try{
			return clscDLExpenseType::total($vflExpenseTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>