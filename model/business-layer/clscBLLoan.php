<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLLoan
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflLoans, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLLoan::queryToDataBase($vflLoans, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflLoans)
	 {
		try{
			return clscDLLoan::total($vflLoans);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>