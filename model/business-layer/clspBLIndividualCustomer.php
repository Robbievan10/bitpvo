<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/business-layer/clspBLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLIndividualCustomer
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflIndividualCustomer)
	 {
		try{
            $vstatus=-2;
            $vflCustomer=new clspFLCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflIndividualCustomer->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflIndividualCustomer->idCustomer;
            $vflCustomer->key=$vflIndividualCustomer->key;
            if ( clspBLCustomer::queryToDataBaseByKey($vflCustomer)==0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vstatus=clspDLIndividualCustomer::addToDataBase($vflIndividualCustomer, $vmySql);
                $vmySql->closeConnection();
                
                unset($vmySql);
            }
			
			unset($vflCustomer);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateInDataBase($vflIndividualCustomer)
	 {
		try{
            $vstatus=-1;
            $vflCustomer=new clspFLCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflIndividualCustomer->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflIndividualCustomer->idCustomer;
            $vflCustomer->key=$vflIndividualCustomer->key;
            clspBLCustomer::queryToDataBaseByKey($vflCustomer);
            if ( $vflIndividualCustomer->idCustomer==$vflCustomer->idCustomer ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vstatus=clspDLIndividualCustomer::updateInDataBase($vflIndividualCustomer, $vmySql);
                $vmySql->closeConnection();
                
                unset($vmySql);
            }
			
			unset($vflCustomer);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function deleteInDataBase($vflIndividualCustomer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLIndividualCustomer::deleteInDataBase($vflIndividualCustomer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflIndividualCustomer, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
			$vstatus=clspDLIndividualCustomer::queryToDataBase($vflIndividualCustomer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }
 
?>