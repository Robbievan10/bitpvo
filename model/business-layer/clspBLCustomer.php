<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLCustomer
 {
	public function __construct() { }
	
    
	public static function queryToDataBase($vflCustomer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCustomer::queryToDataBase($vflCustomer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBaseByKey($vflCustomer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCustomer::queryToDataBaseByKey($vflCustomer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }
 
?>