<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLLoanPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLLoan.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLLoanPayment
 {
    public function __construct() { }


    public static function addToDataBase($vflLoanPayment)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();

			$vloan= new clspFLLoan();
            $vloan->dealer->enterprise->idEnterprise=$vflLoanPayment->loan->dealer->enterprise->idEnterprise;
            $vloan->dealer->idDealer=$vflLoanPayment->loan->dealer->idDealer;
            $vloan->idLoan=$vflLoanPayment->loan->idLoan;
            clspDLLoan::queryToDataBase($vloan, $vmySql);

			$vfilter ="WHERE p_loanpayment.id_enterprise=" . $vflLoanPayment->loan->dealer->enterprise->idEnterprise . " ";
			$vfilter.="AND p_loanpayment.id_dealer=" . $vflLoanPayment->loan->dealer->idDealer . " ";
            $vfilter.="AND p_loanpayment.id_loan=" . $vflLoanPayment->loan->idLoan . " ";
			$vloansPayments= new clscFLLoanPayment();
            clscDLLoanPayment::queryToDataBase($vloansPayments, $vfilter, $vmySql);
			$vloansPaymentTotalAmount=clscDLLoanPayment::totalAmount($vloansPayments);
			if ( number_format($vloan->amount, 2, '.', '')>=((number_format($vloansPaymentTotalAmount, 2, '.', '')) + (number_format($vflLoanPayment->amount, 2, '.', '')) )){
				$vmySql->startTransaction();
				if ( clspDLLoanPayment::addToDataBase($vflLoanPayment, $vmySql, 1)==0 ){
					$vmySql->rollbackTransaction();
					$vmySql->closeConnection();
					return -1;
				}
				$vflLoanPayment->loan->operationStatus->idOperationStatus=1;
				if (( number_format($vloan->amount, 2, '.', ''))==((number_format($vloansPaymentTotalAmount, 2, '.', '')) + (number_format($vflLoanPayment->amount, 2, '.', '')))){
					$vflLoanPayment->loan->operationStatus->idOperationStatus=2;
				}
				clspDLLoan::updateStatusInDataBase($vflLoanPayment->loan, $vmySql);
				$vmySql->commitTransaction();
			}
			else{
				return 0;
			}
			$vmySql->closeConnection();

			unset($vmySql, $vloan, $vfilter, $vloansPayments, $vloansPaymentTotalAmount);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function deleteInDataBase($vflLoanPayment)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vmySql->startTransaction();
			if ( clspDLLoanPayment::deleteInDataBase($vflLoanPayment, $vmySql, 1)==1 ){
				$vfilter ="WHERE p_loanpayment.id_enterprise=" . $vflLoanPayment->loan->dealer->enterprise->idEnterprise . " ";
                $vfilter.="AND p_loanpayment.id_dealer=" . $vflLoanPayment->loan->dealer->idDealer . " ";
                $vfilter.="AND p_loanpayment.id_loan=" . $vflLoanPayment->loan->idLoan . " ";
                $vloansPayments= new clscFLLoanPayment();
                clscDLLoanPayment::queryToDataBase($vloansPayments, $vfilter, $vmySql);
                $vflLoanPayment->loan->operationStatus->idOperationStatus=1;
				if ( clscDLLoanPayment::total($vloansPayments)==0 ){
					$vflLoanPayment->loan->operationStatus->idOperationStatus=0;
				}
				clspDLLoan::updateStatusInDataBase($vflLoanPayment->loan, $vmySql);

				unset($vfilter, $vloansPayments);
			}
			else{
				$vmySql->rollbackTransaction();
				$vmySql->closeConnection();
				return 0;
			}
			$vmySql->commitTransaction();
			$vmySql->closeConnection();

			unset($vmySql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


	public function __destruct() { }
 }

?>
