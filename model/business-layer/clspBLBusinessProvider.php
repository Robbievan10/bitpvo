<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/business-layer/clspBLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLBusinessProvider.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLBusinessProvider
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflBusinessProvider)
	 {
		try{
            $vstatus=-2;
            $vflProvider=new clspFLProvider();
            $vflProvider->enterprise->idEnterprise=$vflBusinessProvider->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflBusinessProvider->idProvider;
            $vflProvider->key=$vflBusinessProvider->key;
            if ( clspBLProvider::queryToDataBaseByKey($vflProvider)==0 ){
    			$vmySql= new clspMySql();
    			$vmySql->openConnection();
    			$vstatus=clspDLBusinessProvider::addToDataBase($vflBusinessProvider, $vmySql);
    			$vmySql->closeConnection();
    			
    			unset($vmySql);
            }
            
            unset($vflProvider);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateInDataBase($vflBusinessProvider)
	 {
		try{
            $vstatus=-1;
            $vflProvider=new clspFLProvider();
            $vflProvider->enterprise->idEnterprise=$vflBusinessProvider->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflBusinessProvider->idProvider;
            $vflProvider->key=$vflBusinessProvider->key;
            clspBLProvider::queryToDataBaseByKey($vflProvider);
            if ( $vflBusinessProvider->idProvider==$vflProvider->idProvider ){
    			$vmySql= new clspMySql();
    			$vmySql->openConnection();
    			$vstatus=clspDLBusinessProvider::updateInDataBase($vflBusinessProvider, $vmySql);
    			$vmySql->closeConnection();
    			
    			unset($vmySql);
            }
            
            unset($vflProvider);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBusinessProvider)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLBusinessProvider::deleteInDataBase($vflBusinessProvider, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBusinessProvider, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
			$vstatus=clspDLBusinessProvider::queryToDataBase($vflBusinessProvider, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }
 
?>