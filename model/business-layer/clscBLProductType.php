<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductTypes, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductType::queryToDataBase($vflProductTypes, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductTypes)
	 {
		try{
			return clscDLProductType::total($vflProductTypes);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>