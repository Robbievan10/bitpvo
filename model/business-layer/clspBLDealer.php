<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLDealer
 {
	public function __construct() { }
	
    
     public static function addToDataBase($vflDealer)
	 {
		try{
            $vstatus=-1;
            $vflOtherDealer=new clspFLDealer();
            $vflOtherDealer->enterprise->idEnterprise=$vflDealer->enterprise->idEnterprise;
            $vflOtherDealer->idDealer=$vflDealer->idDealer;
            $vflOtherDealer->key=$vflDealer->key;
            if ( self::queryToDataBaseByKey($vflOtherDealer)==0 ){
    			$vmySql= new clspMySql();
    			$vmySql->openConnection();
    			$vstatus=clspDLDealer::addToDataBase($vflDealer, $vmySql);
    			$vmySql->closeConnection();
    			
    			unset($vmySql);
            }
            
            unset($vflOtherDealer);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflDealer)
	 {
		try{
            $vstatus=-1;
            $vflOtherDealer=new clspFLDealer();
            $vflOtherDealer->enterprise->idEnterprise=$vflDealer->enterprise->idEnterprise;
            $vflOtherDealer->idDealer=$vflDealer->idDealer;
            $vflOtherDealer->key=$vflDealer->key;
            self::queryToDataBaseByKey($vflOtherDealer);
            if ( $vflDealer->idDealer==$vflOtherDealer->idDealer ){
    			$vmySql= new clspMySql();
    			$vmySql->openConnection();
    			$vstatus=clspDLDealer::updateInDataBase($vflDealer, $vmySql);
    			$vmySql->closeConnection();
    			
    			unset($vmySql);
            }
            
            unset($vflOtherDealer);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflDealer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLDealer::deleteInDataBase($vflDealer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflDealer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLDealer::queryToDataBase($vflDealer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByKey($vflDealer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLDealer::queryToDataBaseByKey($vflDealer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }
 
