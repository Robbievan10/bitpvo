<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductDistribution.php");


class clspBLProductDistribution
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflProductDistribution, $vflProductsDistributionsDetails)
	 {
		try{
			$vdistributionProductsTotal=clscDLProductDistributionDetail::total($vflProductsDistributionsDetails);
			if ( $vdistributionProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vmySql->startTransaction();
                if ( clspDLProductDistribution::addToDataBase($vflProductDistribution, $vmySql)==1 ){
					for($vi=0; $vi<$vdistributionProductsTotal; $vi++){
                        $vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->productDistribution=$vflProductDistribution;
                        if ( clspDLProductDistributionDetail::addToDataBase($vflProductsDistributionsDetails->productsDistributionsDetails[$vi], $vmySql)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -2;
                        }
                    }
                }
                else{
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                unset($vmySql);
            }
            else{
                return 0;
            }	
			unset($vdistributionProductsTotal);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductDistribution, $vflProductsDistributionsDetails)
	 {
		try{
            $vdistributionProductsTotal=clscDLProductDistributionDetail::total($vflProductsDistributionsDetails);
            if ( $vdistributionProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                
                $vflPriorProductDistribution= new clspFLProductDistribution();
                $vflPriorProductDistribution->enterprise->idEnterprise=$vflProductDistribution->enterprise->idEnterprise;
                $vflPriorProductDistribution->idProductDistribution=$vflProductDistribution->idProductDistribution;
                clspDLProductDistribution::queryToDataBase($vflPriorProductDistribution, $vmySql);
                
                $vfilter ="WHERE p_productdistributiondetail.id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
                $vfilter.="AND p_productdistributiondetail.id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
                $vflPriorProductsDistributionsDetails= new clscFLProductDistributionDetail();
                clscDLProductDistributionDetail::queryToDataBase($vflPriorProductsDistributionsDetails, $vfilter, $vmySql);
                $vpriorDistributionProductsTotal=clscDLProductDistributionDetail::total($vflPriorProductsDistributionsDetails);
                
                $vmySql->startTransaction();
                clspDLProductDistribution::updateInDataBase($vflProductDistribution, $vmySql);
                for($vi=0; $vi<$vpriorDistributionProductsTotal; $vi++){
                    $vflPriorProductsDistributionsDetails->productsDistributionsDetails[$vi]->productDistribution=$vflPriorProductDistribution;
                    if ( clspDLProductDistributionDetail::deleteInDataBase($vflPriorProductsDistributionsDetails->productsDistributionsDetails[$vi], $vmySql)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -2;
                    }
                }
                for($vi=0; $vi<$vdistributionProductsTotal; $vi++){
                    $vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->productDistribution=$vflProductDistribution;
                    if ( clspDLProductDistributionDetail::addToDataBase($vflProductsDistributionsDetails->productsDistributionsDetails[$vi], $vmySql)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -1;
                    }
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                
                unset($vmySql, $vflPriorProductDistribution, $vfilter, $vpriorDistributionProductsTotal, $vi);
			}
            else{
                return 0;
            }	
            			
            unset($vdistributionProductsTotal);            			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function recordCuttingInDataBase($vflProductDistribution,$json)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductDistribution::recordCuttingInDataBase($vflProductDistribution, $vmySql,$json);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function openCuttingInDataBase($vflProductDistribution)
     {
        $json = NULL;
        try{
            $vmySql= new clspMySql();
            $vmySql->openConnection();
            $vstatus=clspDLProductDistribution::openCuttingInDataBase($vflProductDistribution, $vmySql);
            $vmySql->closeConnection();
            
            unset($vmySql);
            return $vstatus;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
     }

     
    public static function deleteInDataBase($vflProductDistribution)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            
            $vfilter ="WHERE p_productdistributiondetail.id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
            $vfilter.="AND p_productdistributiondetail.id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
            $vflProductsDistributionsDetails= new clscFLProductDistributionDetail();
            clscDLProductDistributionDetail::queryToDataBase($vflProductsDistributionsDetails, $vfilter, $vmySql);
            $vdistributionProductsTotal=clscDLProductDistributionDetail::total($vflProductsDistributionsDetails);
            
            $vmySql->startTransaction();
            for($vi=0; $vi<$vdistributionProductsTotal; $vi++){
                $vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->productDistribution=$vflProductDistribution;
                if ( clspDLProductDistributionDetail::deleteInDataBase($vflProductsDistributionsDetails->productsDistributionsDetails[$vi], $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
            }
            if ( clspDLProductDistribution::deleteInDataBase($vflProductDistribution, $vmySql)==0 ){
                $vmySql->rollbackTransaction();
                $vmySql->closeConnection();
                return 0;
            }
            $vmySql->commitTransaction();
			$vmySql->closeConnection();
			
			unset($vmySql, $vfilter, $vflProductsDistributionsDetails, $vdistributionProductsTotal);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductDistribution)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductDistribution::queryToDataBase($vflProductDistribution, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


     

	public function __destruct() { }
 }

?>