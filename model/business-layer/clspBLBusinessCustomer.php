<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/business-layer/clspBLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLBusinessCustomer
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflBusinessCustomer)
	 {
		try{
            $vstatus=-2;
            $vflCustomer=new clspFLCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflBusinessCustomer->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflBusinessCustomer->idCustomer;
            $vflCustomer->key=$vflBusinessCustomer->key;
            if ( clspBLCustomer::queryToDataBaseByKey($vflCustomer)==0 ){		  
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vstatus=clspDLBusinessCustomer::addToDataBase($vflBusinessCustomer, $vmySql);
                $vmySql->closeConnection();
                
                unset($vmySql);
            }
			
			unset($vflCustomer);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateInDataBase($vflBusinessCustomer)
	 {
		try{
            $vstatus=-1;
            $vflCustomer=new clspFLCustomer();
            $vflCustomer->enterprise->idEnterprise=$vflBusinessCustomer->enterprise->idEnterprise;
            $vflCustomer->idCustomer=$vflBusinessCustomer->idCustomer;
            $vflCustomer->key=$vflBusinessCustomer->key;
            clspBLCustomer::queryToDataBaseByKey($vflCustomer);
            if ( $vflBusinessCustomer->idCustomer==$vflCustomer->idCustomer ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vstatus=clspDLBusinessCustomer::updateInDataBase($vflBusinessCustomer, $vmySql);
                $vmySql->closeConnection();
			
                unset($vmySql);
            }
            
            unset($vflCustomer);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBusinessCustomer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLBusinessCustomer::deleteInDataBase($vflBusinessCustomer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBusinessCustomer, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
			$vstatus=clspDLBusinessCustomer::queryToDataBase($vflBusinessCustomer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
 
?>