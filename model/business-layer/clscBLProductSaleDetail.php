<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductSaleDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductSaleDetail
 {
	public function __construct() { }


	public static function queryToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSaleDetail::queryToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();

			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryProductsGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSaleDetail::queryProductsGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();

			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function querySalesGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSaleDetail::querySalesGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();

			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryCashAndCashiersGroupByNoRouteToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSaleDetail::queryCashAndCashiersGroupByNoRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();

			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryDeliverersGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSaleDetail::queryDeliverersGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();

			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductSaleDetail::queryByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();

			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }



   public static function queryByRouteToDataBaseCost($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
 {
  try{
    $vmySql= new clspMySql();
    $vmySql->openConnection($vconnectionType);
           $vstatus=clscDLProductSaleDetail::queryByRouteToDataBaseCost($vflProductsSalesDetails, $vfilter, $vmySql);
    $vmySql->closeConnection();

    unset($vmySql);
    return $vstatus;
  }
  catch (Exception $vexception){
    throw new Exception($vexception->getMessage(), $vexception->getCode());
  }
 }




   public static function queryByRouteToDataBaseWeight($vflProductsSalesDetails, $vfilter, $vconnectionType=0)
 {
  try{
    $vmySql= new clspMySql();
    $vmySql->openConnection($vconnectionType);
           $vstatus=clscDLProductSaleDetail::queryByRouteToDataBaseWeight($vflProductsSalesDetails, $vfilter, $vmySql);
    $vmySql->closeConnection();

    unset($vmySql);
    return $vstatus;
  }
  catch (Exception $vexception){
    throw new Exception($vexception->getMessage(), $vexception->getCode());
  }
 }



    public static function add($vflProductsSalesDetails, $vproductSaleDetail)
	 {
		try{
			clscDLProductSaleDetail::add($vflProductsSalesDetails, $vproductSaleDetail);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function total($vflProductsSalesDetails)
	 {
		try{
			return clscDLProductSaleDetail::total($vflProductsSalesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function totalSalePrice($vflProductsSalesDetails)
	 {
		try{
			return clscDLProductSaleDetail::totalSalePrice($vflProductsSalesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

 	public static function totalPurchasePrice($vflProductsSalesDetails)
	 {
		try{
			return clscDLProductSaleDetail::totalPurchasePrice($vflProductsSalesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public function __destruct() { }
 }

?>
