<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLEnterpriseUser.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLEnterpriseUser
 {
	public function __construct() { }
	
    
	public static function queryToDataBase($vflEnterpriseUser, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
			$vstatus=clspDLEnterpriseUser::queryToDataBase($vflEnterpriseUser, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }

?>