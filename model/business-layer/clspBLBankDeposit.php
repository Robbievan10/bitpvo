<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLBankDeposit
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflBankDeposit)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLBankDeposit::addToDataBase($vflBankDeposit, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBankDeposit)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLBankDeposit::updateInDataBase($vflBankDeposit, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBankDeposit)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLBankDeposit::deleteInDataBase($vflBankDeposit, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBankDeposit)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLBankDeposit::queryToDataBase($vflBankDeposit, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }
 
?>