<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLOperationStatus.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLOperationStatus
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflOperationStatus, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLOperationStatus::queryToDataBase($vflOperationStatus, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflOperationStatus)
	 {
		try{
			return clscDLOperationStatus::total($vflOperationStatus);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>