<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLIndividualCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLIndividualCustomer
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflIndividualsCustomers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLIndividualCustomer::queryToDataBase($vflIndividualsCustomers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflIndividualsCustomers)
	 {
		try{
			return clscDLIndividualCustomer::total($vflIndividualsCustomers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>