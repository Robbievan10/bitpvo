<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLProductDistributionDetail
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflProductDistributionDetail)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductDistributionDetail::addToDataBase($vflProductDistributionDetail, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProductDistributionDetail)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductDistributionDetail::deleteInDataBase($vflProductDistributionDetail, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateReturnedAmountInDataBase($vflProductDistributionDetail)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductDistributionDetail::updateReturnedAmountInDataBase($vflProductDistributionDetail, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductDistributionDetail)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductDistributionDetail::queryToDataBase($vflProductDistributionDetail, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }

?>