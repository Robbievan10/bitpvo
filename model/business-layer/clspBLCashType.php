<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCashType.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLCashType
 {
    public function __construct() { }
	
    
	public static function queryToDataBase($vflCashType)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCashType::queryToDataBase($vflCashType, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>