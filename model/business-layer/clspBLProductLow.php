<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLProductLow
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflProductLow, $vflProductsLowDetails)
	 {
		try{
		    $vlowProductsTotal=clscDLProductLowDetail::total($vflProductsLowDetails);
            if ( $vlowProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vmySql->startTransaction();
                if ( clspDLProductLow::addToDataBase($vflProductLow, $vmySql)==1 ){
                    for($vi=0; $vi<$vlowProductsTotal; $vi++){
                        $vflProductsLowDetails->productsLowDetails[$vi]->productLow=$vflProductLow;
                        if ( clspDLProductLowDetail::addToDataBase($vflProductsLowDetails->productsLowDetails[$vi], $vmySql)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -4;
                        }
                        $vflProduct= new clspFLProduct();
                        $vflProduct->enterprise->idEnterprise=$vflProductsLowDetails->productsLowDetails[$vi]->productLow->enterprise->idEnterprise;
                        $vflProduct->idProduct=$vflProductsLowDetails->productsLowDetails[$vi]->product->idProduct;
						clspDLProduct::queryToDataBase($vflProduct, $vmySql);
                        $vlowAmount=$vflProductsLowDetails->productsLowDetails[$vi]->lowAmount;
                        $vweightInKg=$vflProductsLowDetails->productsLowDetails[$vi]->lowWeightInKg;
                        if ( $vflProduct->stock>=$vflProductsLowDetails->productsLowDetails[$vi]->lowAmount ){
							if ( clspDLProduct::decreaseStockInDataBase($vflProduct, $vmySql, $vlowAmount, $vlowAmount*$vweightInKg)==0 ){
                            	$vmySql->rollbackTransaction();
	                            $vmySql->closeConnection();
    	                        return -3;
        	                }
						}
						else{
							$vmySql->rollbackTransaction();
							$vmySql->closeConnection();
							throw new Exception("Imposible registrar la baja de productos.\nLa cantidad del producto <" . $vflProduct->name .
												"> excede al total en existencias <" . $vflProduct->stock . ">" , -2);
						}
						
						unset($vflProduct, $vlowAmount, $vweightInKg);
                    }
                }
                else{
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                unset($vmySql);
            }
            else{
                return 0;
            }	
			unset($vlowProductsTotal);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateInDataBase($vflProductLow, $vflProductsLowDetails)
	 {
		try{
            $vlowProductsTotal=clscDLProductLowDetail::total($vflProductsLowDetails);
            if ( $vlowProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                
                $vflPriorProductLow= new clspFLProductLow();
                $vflPriorProductLow->enterprise->idEnterprise=$vflProductLow->enterprise->idEnterprise;
                $vflPriorProductLow->idProductLow=$vflProductLow->idProductLow;
                clspDLProductLow::queryToDataBase($vflPriorProductLow, $vmySql);
                
                $vfilter ="WHERE p_productlowdetail.id_enterprise=" . $vflProductLow->enterprise->idEnterprise . " ";
                $vfilter.="AND p_productlowdetail.id_productLow='" . $vflProductLow->idProductLow . "'";
                $vflPriorProductsLowDetails= new clscFLProductLowDetail();
                clscDLProductLowDetail::queryToDataBase($vflPriorProductsLowDetails, $vfilter, $vmySql);
                $vpriorLowProductsTotal=clscDLProductLowDetail::total($vflPriorProductsLowDetails);
                
                $vmySql->startTransaction();
                clspDLProductLow::updateInDataBase($vflProductLow, $vmySql);
                for($vi=0; $vi<$vpriorLowProductsTotal; $vi++){
                    $vflPriorProductsLowDetails->productsLowDetails[$vi]->productLow=$vflPriorProductLow;
                    if ( clspDLProductLowDetail::deleteInDataBase($vflPriorProductsLowDetails->productsLowDetails[$vi], $vmySql)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -5;
                    }
                    $vflProduct= new clspFLProduct();
                    $vflProduct->enterprise->idEnterprise=$vflPriorProductsLowDetails->productsLowDetails[$vi]->productLow->enterprise->idEnterprise;
                    $vflProduct->idProduct=$vflPriorProductsLowDetails->productsLowDetails[$vi]->product->idProduct;
                    $vlowAmount=$vflPriorProductsLowDetails->productsLowDetails[$vi]->lowAmount;
                    $vlowWeightInKg=$vflPriorProductsLowDetails->productsLowDetails[$vi]->lowWeightInKg;
                    if ( clspDLProduct::appendStockInDataBase($vflProduct, $vmySql, $vlowAmount, $vlowAmount*$vlowWeightInKg)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -4;
                    }
                    unset($vflProduct);
                }
                for($vi=0; $vi<$vlowProductsTotal; $vi++){
                    $vflProductsLowDetails->productsLowDetails[$vi]->productLow=$vflProductLow;
                    if ( clspDLProductLowDetail::addToDataBase($vflProductsLowDetails->productsLowDetails[$vi], $vmySql)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -3;
                    }
                    $vflProduct= new clspFLProduct();
                    $vflProduct->enterprise->idEnterprise=$vflProductsLowDetails->productsLowDetails[$vi]->productLow->enterprise->idEnterprise;
                    $vflProduct->idProduct=$vflProductsLowDetails->productsLowDetails[$vi]->product->idProduct;
                    clspDLProduct::queryToDataBase($vflProduct, $vmySql);
                    if ( $vflProduct->stock>=$vflProductsLowDetails->productsLowDetails[$vi]->lowAmount ){
                        $vlowAmount=$vflProductsLowDetails->productsLowDetails[$vi]->lowAmount;
                        $vlowWeightInKg=$vflProductsLowDetails->productsLowDetails[$vi]->lowWeightInKg;
                        if ( clspDLProduct::decreaseStockInDataBase($vflProduct, $vmySql, $vlowAmount, $vlowAmount*$vlowWeightInKg)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -2;
                        }
                    }
                    else{
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
							throw new Exception("Imposible registrar la baja de productos.\nLa cantidad del producto <" . $vflProduct->name .
												"> excede al total en existencias <" . $vflProduct->stock . ">" , -1);
                    }
                    
                    unset($vflProduct);
                } 
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                
                unset($vmySql, $vflPriorProductLow, $vfilter, $vflPriorProductsLowDetails, $vpriorLowProductsTotal, $vi);
			}
            else{
                return 0;
            }	
			unset($vlowProductsTotal);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflProductLow)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            
            $vfilter ="WHERE p_productlowdetail.id_enterprise=" . $vflProductLow->enterprise->idEnterprise . " ";
            $vfilter.="AND p_productlowdetail.id_productLow='" . $vflProductLow->idProductLow . "'";
            $vflProductsLowDetails= new clscFLProductLowDetail();
            clscDLProductLowDetail::queryToDataBase($vflProductsLowDetails, $vfilter, $vmySql);
            $vlowProductsTotal=clscDLProductLowDetail::total($vflProductsLowDetails);
            
            $vmySql->startTransaction();
            for($vi=0; $vi<$vlowProductsTotal; $vi++){
                $vflProductsLowDetails->productsLowDetails[$vi]->productLow=$vflProductLow;
                if ( clspDLProductLowDetail::deleteInDataBase($vflProductsLowDetails->productsLowDetails[$vi], $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -2;
                }
                $vflProduct= new clspFLProduct();
                $vflProduct->enterprise->idEnterprise=$vflProductsLowDetails->productsLowDetails[$vi]->productLow->enterprise->idEnterprise;
                $vflProduct->idProduct=$vflProductsLowDetails->productsLowDetails[$vi]->product->idProduct;
                $vlowAmount=$vflProductsLowDetails->productsLowDetails[$vi]->lowAmount;
                $vlowWeightInKg=$vflProductsLowDetails->productsLowDetails[$vi]->lowWeightInKg;
                if ( clspDLProduct::appendStockInDataBase($vflProduct, $vmySql, $vlowAmount, $vlowAmount*$vlowWeightInKg)==0 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
                unset($vflProduct);
            }
            if ( clspDLProductLow::deleteInDataBase($vflProductLow, $vmySql)==0 ){
                $vmySql->rollbackTransaction();
                $vmySql->closeConnection();
                return 0;
            }
            $vmySql->commitTransaction();
			$vmySql->closeConnection();
			
			unset($vmySql, $vfilter, $vflProductsLowDetails, $vlowProductsTotal, $vi);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
     }
     
	public static function queryToDataBase($vflProductLow)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductLow::queryToDataBase($vflProductLow, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }

?>