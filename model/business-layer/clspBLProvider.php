<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLProvider
 {
	public function __construct() { }
	
    
	public static function queryToDataBase($vflProvider)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProvider::queryToDataBase($vflProvider, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function queryToDataBaseByKey($vflProvider)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProvider::queryToDataBaseByKey($vflProvider, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }
 
?>