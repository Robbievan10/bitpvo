<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductEntryPayment
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsEntriesPayments, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductEntryPayment::queryToDataBase($vflProductsEntriesPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function queryGroupByProviderToDataBase($vflProductsEntriesPayments, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductEntryPayment::queryGroupByProviderToDataBase($vflProductsEntriesPayments, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
          
    public static function total($vflProductsEntriesPayments)
	 {
		try{
			return clscDLProductEntryPayment::total($vflProductsEntriesPayments);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>