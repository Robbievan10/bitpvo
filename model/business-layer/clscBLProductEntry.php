<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductEntry
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsEntries, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLProductEntry::queryToDataBase($vflProductsEntries, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsEntries)
	 {
		try{
			return clscDLProductEntry::total($vflProductsEntries);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>