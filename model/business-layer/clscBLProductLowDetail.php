<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductLowDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductLowDetail
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsLowDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductLowDetail::queryToDataBase($vflProductsLowDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryLowGroupByNameToDataBase($vflProductsLowDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductLowDetail::queryLowGroupByNameToDataBase($vflProductsLowDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function add($vflProductsLowDetails, $vproductLowDetail)
	 {
		try{
			clscDLProductLowDetail::add($vflProductsLowDetails, $vproductLowDetail);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsLowDetails)
	 {
		try{
			return clscDLProductLowDetail::total($vflProductsLowDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }

?>