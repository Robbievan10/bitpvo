<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductEntryDetail
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsEntriesDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductEntryDetail::queryToDataBase($vflProductsEntriesDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function add($vflProductsEntriesDetails, $vproductEntryDetail)
	 {
		try{
			clscDLProductEntryDetail::add($vflProductsEntriesDetails, $vproductEntryDetail);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsEntriesDetails)
	 {
		try{
			return clscDLProductEntryDetail::total($vflProductsEntriesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	public static function totalPrice($vflProductsEntriesDetails)
	 {
		try{
			return clscDLProductEntryDetail::totalPrice($vflProductsEntriesDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>