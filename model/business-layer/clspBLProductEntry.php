<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLProductEntry
 {
    public function __construct() { }
	
    
    public static function addToDataBase($vflProductEntry, $vflProductsEntriesDetails)
	 {
		try{
			$ventryProductsTotal=clscDLProductEntryDetail::total($vflProductsEntriesDetails);
            if ( $ventryProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                $vmySql->startTransaction();
                if ( clspDLProductEntry::addToDataBase($vflProductEntry, $vmySql)==1 ){
                    for($vi=0; $vi<$ventryProductsTotal; $vi++){
                        $vflProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry=$vflProductEntry;
                        if ( clspDLProductEntryDetail::addToDataBase($vflProductsEntriesDetails->productsEntriesDetails[$vi], $vmySql)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -4;
                        }
                        $vflProduct= new clspFLProduct();
                        $vflProduct->enterprise->idEnterprise=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry->enterprise->idEnterprise;
                        $vflProduct->idProduct=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->product->idProduct;
                        if ( clspDLProduct::appendStockInDataBase($vflProduct, $vmySql, $vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInPt, $vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInKg)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -3;
                        }
                        unset($vflProduct);
                    }
                    if ( $vflProductEntry->operationType->idOperationType==1 ){
                        $vflProductEntryPayment= new clspFLProductEntryPayment();
                        $vflProductEntryPayment->productEntry=$vflProductEntry;
                        $vflProductEntryPayment->enterprise->idEnterprise=$vflProductEntryPayment->productEntry->enterprise->idEnterprise;
                        $vflProductEntryPayment->paymentType->idPaymentType=1;
                        $vflProductEntryPayment->paymentDate=$vflProductEntry->productEntryDate;
                        $vflProductEntryPayment->amount=clscDLProductEntryDetail::totalPrice($vflProductsEntriesDetails);
                        if ( clspDLProductEntryPayment::addToDataBase($vflProductEntryPayment, $vmySql, 1)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -2;
                        }
                        unset($vflProductEntryPayment);
                    }
                }
                else{
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                unset($vmySql);
            }
            else{
                return 0;
            }	
			unset($ventryProductsTotal);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductEntry, $vflProductsEntriesDetails)
	 {
		try{
            $ventryProductsTotal=clscDLProductEntryDetail::total($vflProductsEntriesDetails);
            if ( $ventryProductsTotal>0 ){
                $vmySql= new clspMySql();
                $vmySql->openConnection();
                
                $vflPriorProductEntry= new clspFLProductEntry();
                $vflPriorProductEntry->enterprise->idEnterprise=$vflProductEntry->enterprise->idEnterprise;
                $vflPriorProductEntry->idProductEntry=$vflProductEntry->idProductEntry;
                clspDLProductEntry::queryToDataBase($vflPriorProductEntry, $vmySql);
                
                $vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
                $vfilter.="AND p_productentrydetail.id_productEntry='" . $vflProductEntry->idProductEntry . "'";
                $vflPriorProductsEntriesDetails= new clscFLProductEntryDetail();
                clscDLProductEntryDetail::queryToDataBase($vflPriorProductsEntriesDetails, $vfilter, $vmySql);
                $vpriorEntryProductsTotal=clscDLProductEntryDetail::total($vflPriorProductsEntriesDetails);
                
                $vmySql->startTransaction();
                clspDLProductEntry::updateInDataBase($vflProductEntry, $vmySql);
                for($vi=0; $vi<$vpriorEntryProductsTotal; $vi++){
                    $vflPriorProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry=$vflPriorProductEntry;
                    if ( clspDLProductEntryDetail::deleteInDataBase($vflPriorProductsEntriesDetails->productsEntriesDetails[$vi], $vmySql)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -8;
                    }
                    $vflProduct= new clspFLProduct();
                    $vflProduct->enterprise->idEnterprise=$vflPriorProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry->enterprise->idEnterprise;
                    $vflProduct->idProduct=$vflPriorProductsEntriesDetails->productsEntriesDetails[$vi]->product->idProduct;
                    if ( clspDLProduct::decreaseStockInDataBase($vflProduct, $vmySql, $vflPriorProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInPt, $vflPriorProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInKg)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -7;
                    }
                    unset($vflProduct);
                }
                for($vi=0; $vi<$ventryProductsTotal; $vi++){
                    $vflProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry=$vflProductEntry;
                    if ( clspDLProductEntryDetail::addToDataBase($vflProductsEntriesDetails->productsEntriesDetails[$vi], $vmySql)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -6;
                    }
                    $vflProduct= new clspFLProduct();
                    $vflProduct->enterprise->idEnterprise=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry->enterprise->idEnterprise;
                    $vflProduct->idProduct=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->product->idProduct;
                    if ( clspDLProduct::appendStockInDataBase($vflProduct, $vmySql, $vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInPt, $vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInKg)==0 ){
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -5;
                    }
                    unset($vflProduct);
                }
                
                $vfilter ="WHERE p_productentrypayment.id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
                $vfilter.="AND p_productentrypayment.id_productEntry='" . $vflProductEntry->idProductEntry . "'";
                $vproductsEntriesPayments= new clscFLProductEntryPayment();
                clscDLProductEntryPayment::queryToDataBase($vproductsEntriesPayments, $vfilter, $vmySql);
                if ( $vflPriorProductEntry->operationType->idOperationType==1 ){
                    if ( $vflProductEntry->operationType->idOperationType==1 ){
                        $vproductsEntriesPayments->productsEntriesPayments[0]->paymentDate=$vflProductEntry->productEntryDate;
                        $vproductsEntriesPayments->productsEntriesPayments[0]->amount=clscDLProductEntryDetail::totalPrice($vflProductsEntriesDetails);
                        clspDLProductEntryPayment::updateInDataBase($vproductsEntriesPayments->productsEntriesPayments[0], $vmySql);
                    }
                    else{
                        if ( clspDLProductEntryPayment::deleteInDataBase($vproductsEntriesPayments->productsEntriesPayments[0], $vmySql, 1)==0 ){
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -4;
                        }
                    }
                }
                else{
                    if ( $vflPriorProductEntry->operationStatus->idOperationStatus!=2 ){
                        if ( clscDLProductEntryPayment::total($vproductsEntriesPayments)==0 ){
                            if ( $vflProductEntry->operationType->idOperationType==1 ){
                                $vflProductEntryPayment= new clspFLProductEntryPayment();
                                $vflProductEntryPayment->productEntry=$vflProductEntry;
                                $vflProductEntryPayment->enterprise->idEnterprise=$vflProductEntryPayment->productEntry->enterprise->idEnterprise;
                                $vflProductEntryPayment->paymentType->idPaymentType=1;
                                $vflProductEntryPayment->paymentDate=$vflProductEntry->productEntryDate;
                                $vflProductEntryPayment->amount=clscDLProductEntryDetail::totalPrice($vflProductsEntriesDetails);
                                if ( clspDLProductEntryPayment::addToDataBase($vflProductEntryPayment, $vmySql, 1)==0 ){
                                    $vmySql->rollbackTransaction();
                                    $vmySql->closeConnection();
                                    return -3;
                                }
                                unset($vflProductEntryPayment);
                            }
                        }
                        else{
                            $vmySql->rollbackTransaction();
                            $vmySql->closeConnection();
                            return -2;
                        }
                    }
                    else{
                        $vmySql->rollbackTransaction();
                        $vmySql->closeConnection();
                        return -1;
                    }
                }
                $vmySql->commitTransaction();
                $vmySql->closeConnection();
                
                unset($vmySql, $vflPriorProductEntry, $vfilter, $vpriorEntryProductsTotal, $vi, $vproductsEntriesPayments);
			}
            else{
                return 0;
            }	
			unset($ventryProductsTotal);
			
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function deleteInDataBase($vflProductEntry)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            
            $vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
            $vfilter.="AND p_productentrydetail.id_productEntry='" . $vflProductEntry->idProductEntry . "'";
            $vflProductsEntriesDetails= new clscFLProductEntryDetail();
            clscDLProductEntryDetail::queryToDataBase($vflProductsEntriesDetails, $vfilter, $vmySql);
            $ventryProductsTotal=clscDLProductEntryDetail::total($vflProductsEntriesDetails);
            
            $vfilter ="WHERE p_productentrypayment.id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
            $vfilter.="AND p_productentrypayment.id_productEntry='" . $vflProductEntry->idProductEntry . "'";
            $vproductsEntriesPayments= new clscFLProductEntryPayment();
            clscDLProductEntryPayment::queryToDataBase($vproductsEntriesPayments, $vfilter, $vmySql);
            $vproductsEntriesPaymentsTotal=clscDLProductEntryPayment::total($vproductsEntriesPayments);
            
            $vmySql->startTransaction();
            for($vi=0; $vi<$ventryProductsTotal; $vi++){
                $vflProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry=$vflProductEntry;
                if ( clspDLProductEntryDetail::deleteInDataBase($vflProductsEntriesDetails->productsEntriesDetails[$vi], $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -3;
                }
                $vflProduct= new clspFLProduct();
                $vflProduct->enterprise->idEnterprise=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->productEntry->enterprise->idEnterprise;
                $vflProduct->idProduct=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->product->idProduct;
                if ( clspDLProduct::decreaseStockInDataBase($vflProduct, $vmySql, $vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInPt, $vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInKg)==0 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -2;
                }
                unset($vflProduct);
            }
            for($vi=0; $vi<$vproductsEntriesPaymentsTotal; $vi++){
                if ( clspDLProductEntryPayment::deleteInDataBase($vproductsEntriesPayments->productsEntriesPayments[$vi], $vmySql, 1)==0 ){
                    $vmySql->rollbackTransaction();
                    $vmySql->closeConnection();
                    return -1;
                }
            }
            if ( clspDLProductEntry::deleteInDataBase($vflProductEntry, $vmySql)==0 ){
                $vmySql->rollbackTransaction();
                $vmySql->closeConnection();
                return 0;
            }
            $vmySql->commitTransaction();
			$vmySql->closeConnection();
			
			unset($vmySql, $vfilter, $vflProductsEntriesDetails, $ventryProductsTotal, $vproductsEntriesPayments, $vproductsEntriesPaymentsTotal, $vi);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductEntry)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLProductEntry::queryToDataBase($vflProductEntry, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     

	public function __destruct() { }
 }

?>