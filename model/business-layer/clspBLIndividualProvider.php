<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/business-layer/clspBLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLIndividualProvider
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflIndividualProvider)
	 {
		try{
            $vstatus=-2;
            $vflProvider=new clspFLProvider();
            $vflProvider->enterprise->idEnterprise=$vflIndividualProvider->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflIndividualProvider->idProvider;
            $vflProvider->key=$vflIndividualProvider->key;
            if ( clspBLProvider::queryToDataBaseByKey($vflProvider)==0 ){
    			$vmySql= new clspMySql();
    			$vmySql->openConnection();
    			$vstatus=clspDLIndividualProvider::addToDataBase($vflIndividualProvider, $vmySql);
    			$vmySql->closeConnection();
    			
    			unset($vmySql);
            }
            
            unset($vflProvider);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function updateInDataBase($vflIndividualProvider)
	 {
		try{
            $vstatus=-1;
            $vflProvider=new clspFLProvider();
            $vflProvider->enterprise->idEnterprise=$vflIndividualProvider->enterprise->idEnterprise;
            $vflProvider->idProvider=$vflIndividualProvider->idProvider;
            $vflProvider->key=$vflIndividualProvider->key;
            clspBLProvider::queryToDataBaseByKey($vflProvider);
            if ( $vflIndividualProvider->idProvider==$vflProvider->idProvider ){
    			$vmySql= new clspMySql();
    			$vmySql->openConnection();
    			$vstatus=clspDLIndividualProvider::updateInDataBase($vflIndividualProvider, $vmySql);
    			$vmySql->closeConnection();
    			
    			unset($vmySql);
            }
            
            unset($vflProvider);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflIndividualProvider)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLIndividualProvider::deleteInDataBase($vflIndividualProvider, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflIndividualProvider, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
			$vstatus=clspDLIndividualProvider::queryToDataBase($vflIndividualProvider, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }
 
?>