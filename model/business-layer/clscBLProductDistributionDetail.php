<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductDistributionDetail.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLProductDistributionDetail
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductsDistributionsDetails, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLProductDistributionDetail::queryToDataBase($vflProductsDistributionsDetails, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function add($vflProductsDistributionsDetails, $vproductDistributionDetail)
	 {
		try{
			clscDLProductDistributionDetail::add($vflProductsDistributionsDetails, $vproductDistributionDetail);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflProductsDistributionsDetails)
	 {
		try{
			return clscDLProductDistributionDetail::total($vflProductsDistributionsDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function distributionTotal($vflProductsDistributionsDetails)
	 {
		try{
			return clscDLProductDistributionDetail::distributionTotal($vflProductsDistributionsDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function distributedTotal($vflProductsDistributionsDetails)
	 {
		try{
			return clscDLProductDistributionDetail::distributedTotal($vflProductsDistributionsDetails);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>