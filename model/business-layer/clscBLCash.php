<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCash.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLCash
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCash, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLCash::queryToDataBase($vflCash, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflCash)
	 {
		try{
			return clscDLCash::total($vflCash);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>