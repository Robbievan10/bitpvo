<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntryDetail.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductEntryPayment.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clspBLProductEntryPayment
 {
    public function __construct() { }


    public static function addToDataBase($vflProductEntryPayment)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();

			$vfilter ="WHERE p_productentrydetail.id_enterprise=" . $vflProductEntryPayment->productEntry->enterprise->idEnterprise . " ";
			$vfilter.="AND p_productentrydetail.id_productEntry='" . $vflProductEntryPayment->productEntry->idProductEntry . "'";
			$vproductsEntriesDetails= new clscFLProductEntryDetail();
			clscDLProductEntryDetail::queryToDataBase($vproductsEntriesDetails, $vfilter, $vmySql);
			$vproductsEntryTotalPrice=clscDLProductEntryDetail::totalPrice($vproductsEntriesDetails);

			$vfilter ="WHERE p_productentrypayment.id_enterprise=" . $vflProductEntryPayment->productEntry->enterprise->idEnterprise . " ";
			$vfilter.="AND p_productentrypayment.id_productEntry='" . $vflProductEntryPayment->productEntry->idProductEntry . "'";
			$vproductsEntriesPayments= new clscFLProductEntryPayment();
        	clscDLProductEntryPayment::queryToDataBase($vproductsEntriesPayments, $vfilter, $vmySql);
			$vproductsEntryPaymentTotalAmount=clscDLProductEntryPayment::totalAmount($vproductsEntriesPayments);

			if ( number_format($vproductsEntryTotalPrice, 2, '.', '')>=(number_format($vproductsEntryPaymentTotalAmount, 2, '.', '') + number_format($vflProductEntryPayment->amount, 2, '.', '')) ){
				$vmySql->startTransaction();
				if ( clspDLProductEntryPayment::addToDataBase($vflProductEntryPayment, $vmySql, 1)==0 ){
					$vmySql->rollbackTransaction();
					$vmySql->closeConnection();
					return -1;
				}
				$vflProductEntryPayment->productEntry->operationStatus->idOperationStatus=1;
				if ( number_format($vproductsEntryTotalPrice, 2, '.', '')==(number_format($vproductsEntryPaymentTotalAmount, 2, '.', '') + number_format($vflProductEntryPayment->amount, 2, '.', '')) ){
					$vflProductEntryPayment->productEntry->operationStatus->idOperationStatus=2;
				}
				clspDLProductEntry::updateStatusInDataBase($vflProductEntryPayment->productEntry, $vmySql);
				$vmySql->commitTransaction();
			}
			else{
				return 0;
			}
			$vmySql->closeConnection();

			unset($vmySql, $vfilter, $vproductsEntriesDetails, $vproductsEntryTotalPrice, $vproductsEntriesPayments,
				  $vproductsEntryPaymentTotalAmount);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function deleteInDataBase($vflProductEntryPayment)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vmySql->startTransaction();
			if ( clspDLProductEntryPayment::deleteInDataBase($vflProductEntryPayment, $vmySql, 1)==1 ){
				$vfilter ="WHERE p_productentrypayment.id_enterprise=" . $vflProductEntryPayment->productEntry->enterprise->idEnterprise . " ";
				$vfilter.="AND p_productentrypayment.id_productEntry='" . $vflProductEntryPayment->productEntry->idProductEntry . "'";
				$vproductsEntriesPayments= new clscFLProductEntryPayment();
        		clscDLProductEntryPayment::queryToDataBase($vproductsEntriesPayments, $vfilter, $vmySql);
				$vflProductEntryPayment->productEntry->operationStatus->idOperationStatus=1;
				if ( clscDLProductEntryPayment::total($vproductsEntriesPayments)==0 ){
					$vflProductEntryPayment->productEntry->operationStatus->idOperationStatus=0;
				}
				clspDLProductEntry::updateStatusInDataBase($vflProductEntryPayment->productEntry, $vmySql);

				unset($vfilter, $vproductsEntriesPayments);
			}
			else{
				$vmySql->rollbackTransaction();
				$vmySql->closeConnection();
				return 0;
			}
			$vmySql->commitTransaction();
			$vmySql->closeConnection();

			unset($vmySql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


	public function __destruct() { }
 }

?>
