<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLUser.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLUser
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflUsers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLUser::queryToDataBase($vflUsers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflUsers)
	 {
		try{
			return clscDLUser::total($vflUsers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>