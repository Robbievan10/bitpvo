<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLBankDeposit.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLBankDeposit
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflBanksDeposits, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLBankDeposit::queryToDataBase($vflBanksDeposits, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflBanksDeposits)
	 {
		try{
			return clscDLBankDeposit::total($vflBanksDeposits);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    /*
    public static function totalAmount($vflBanksDeposits)
	 {
		try{
			return clscDLBankDeposit::totalAmount($vflBanksDeposits);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    */
	public function __destruct() { }
 }

?>