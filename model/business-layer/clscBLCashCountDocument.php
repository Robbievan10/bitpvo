<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLCashCountDocument
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCashCountDocumentList, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLCashCountDocument::queryToDataBase($vflCashCountDocumentList, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryDocumentGroupByValue($vflCashCountDocumentList, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLCashCountDocument::queryDocumentGroupByValue($vflCashCountDocumentList, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function add($vflCashCountDocumentList, $vflCashCountDocument)
	 {
        try{
            clscDLCashCountDocument::add($vflCashCountDocumentList, $vflCashCountDocument);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
     
    public static function total($vflCashCountDocumentList)
	 {
		try{
			return clscDLCashCountDocument::total($vflCashCountDocumentList);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>