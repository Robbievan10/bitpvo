<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLBusinessCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLBusinessCustomer
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflBusinessCustomers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLBusinessCustomer::queryToDataBase($vflBusinessCustomers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflBusinessCustomers)
	 {
		try{
			return clscDLBusinessCustomer::total($vflBusinessCustomers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>