<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clscFLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashMovement.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCashMovement.php");


class clspBLCashMovement
 {
    public function __construct() { }
	
    
    public static function addOpenToDataBase($vflCashMovement)
	 {
		try{
		    $vmySql= new clspMySql();
			$vmySql->openConnection();
            
            $vfilter ="WHERE p_cashmovement.id_enterprise=" . $vflCashMovement->cash->enterprise->idEnterprise . " ";
            $vfilter.="AND p_cashmovement.id_cash=" . $vflCashMovement->cash->idCash . " ";
            $vfilter.="AND p_cashmovement.id_cashMovementStatus=1 ";
            $vfilter.="AND p_cashmovement.fldrecordDate='" . $vflCashMovement->recordDate . "'";
            $vcashMovements= new clscFLCashMovement();
            clscDLCashMovement::queryToDataBase($vcashMovements, $vfilter, $vmySql);
            $vcashMovementsTotal=clscDLCashMovement::total($vcashMovements);
            if ( $vcashMovementsTotal==0 ){
                $vfilter ="WHERE p_cashmovement.id_enterprise=" . $vflCashMovement->cash->enterprise->idEnterprise . " ";
                $vfilter.="AND p_cashmovement.id_cashMovementStatus=1 ";
                $vfilter.="AND p_cashmovement.id_user='" . $vflCashMovement->user->idUser . "' ";
                $vfilter.="AND p_cashmovement.fldrecordDate='" . $vflCashMovement->recordDate . "'";
                $vcashMovements= new clscFLCashMovement();
                clscDLCashMovement::queryToDataBase($vcashMovements, $vfilter, $vmySql);
                $vcashMovementsTotal=clscDLCashMovement::total($vcashMovements);
                if ( $vcashMovementsTotal==0 ){
                    if ( clspDLCashMovement::addOpenToDataBase($vflCashMovement, $vmySql)==0 ){
                        return -2;
                        $vmySql->closeConnection();
                    }
                }
                else{
                    return -1;
                    $vmySql->closeConnection();        
                }
            }
            else{
                return 0;
                $vmySql->closeConnection();        
            }
            
			unset($vmySql, $vfilter, $vcashMovements, $vcashMovementsTotal);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function recordCloseToDataBase($vflCashMovement)
	 {
		try{
		    $vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCashMovement::recordCloseToDataBase($vflCashMovement, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflCashMovement)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCashMovement::deleteInDataBase($vflCashMovement, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function queryToDataBase($vflCashMovement)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLCashMovement::queryToDataBase($vflCashMovement, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct() { }
 }

?>