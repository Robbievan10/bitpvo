<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLRouteDealer
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflRoutesDealers, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLRouteDealer::queryToDataBase($vflRoutesDealers, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflRoutesDealers)
	 {
		try{
			return clscDLRouteDealer::total($vflRoutesDealers);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>