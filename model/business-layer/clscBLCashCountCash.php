<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLCashCountCash
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCashCountCashList, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLCashCountCash::queryToDataBase($vflCashCountCashList, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryCashGroupByValue($vflCashCountCashList, $vfilter, $vconnectionType=0)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection($vconnectionType);
            $vstatus=clscDLCashCountCash::queryCashGroupByValue($vflCashCountCashList, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public static function add($vflCashCountCashList, $vflCashCountCash)
	 {
        try{
            clscDLCashCountCash::add($vflCashCountCashList, $vflCashCountCash);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
     
    public static function total($vflCashCountCashList)
	 {
		try{
			return clscDLCashCountCash::total($vflCashCountCashList);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>