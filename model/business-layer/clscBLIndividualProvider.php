<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clscDLIndividualProvider.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");


class clscBLIndividualProvider
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflIndividualsProviders, $vfilter)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
            $vstatus=clscDLIndividualProvider::queryToDataBase($vflIndividualsProviders, $vfilter, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function total($vflIndividualsProviders)
	 {
		try{
			return clscDLIndividualProvider::total($vflIndividualsProviders);
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct() { }
 }

?>