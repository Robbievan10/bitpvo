<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLRouteDealer.php");
require_once (dirname(dirname(__FILE__)) . "/tools/clspMySql.php");

class clspBLRouteDealer
 {
	public function __construct() { }
	
    
    public static function addToDataBase($vflRouteDealer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLRouteDealer::addToDataBase($vflRouteDealer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflRouteDealer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLRouteDealer::deleteInDataBase($vflRouteDealer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflRouteDealer)
	 {
		try{
			$vmySql= new clspMySql();
			$vmySql->openConnection();
			$vstatus=clspDLRouteDealer::queryToDataBase($vflRouteDealer, $vmySql);
			$vmySql->closeConnection();
			
			unset($vmySql);
			return $vstatus;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    
	public function __destruct() { }
 }
 
?>