<?php

class clspDLBank
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflBank, $vmySql)
	 {
		try{
            $vflBank->idBank=self::getIdFromDataBase($vflBank, $vmySql);
            
            $vsql ="INSERT INTO c_bank(id_enterprise, id_bank, fldname) ";
			$vsql.="VALUES(" . $vflBank->enterprise->idEnterprise;
            $vsql.=", " . $vflBank->idBank;
			$vsql.=", '" . $vflBank->name . "')";
                        
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBank, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_bank ";
            $vsql.="SET fldname='" . $vflBank->name . "' ";
            $vsql.="WHERE id_enterprise=" . $vflBank->enterprise->idEnterprise . " ";
            $vsql.="AND id_bank=" . $vflBank->idBank;
			
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflBank, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_bank ";
            $vsql.="WHERE id_enterprise=" . $vflBank->enterprise->idEnterprise . " ";
            $vsql.="AND id_bank=" . $vflBank->idBank;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBank, $vmySql)
	 {
		try{
			$vsql ="SELECT c_bank.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_bank ";
            $vsql.="INNER JOIN c_enterprise ON c_bank.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE c_bank.id_enterprise=" . $vflBank->enterprise->idEnterprise . " ";
            $vsql.="AND c_bank.id_bank=" . $vflBank->idBank;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflBank->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflBank->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflBank->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflBank->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflBank->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflBank->enterprise->locality=trim($vrow["fldlocality"]);
                $vflBank->enterprise->street=trim($vrow["fldstreet"]);
                $vflBank->enterprise->number=trim($vrow["fldnumber"]);
                $vflBank->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflBank->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflBank->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflBank->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflBank->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflBank->name=trim($vrow["fldname"]);
                                				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    private static function getIdFromDataBase($vflBank, $vmySql)
	 {
		try{
            $vidBank=1;
			$vsql ="SELECT MAX(id_bank) + 1 AS id_bankNew ";
			$vsql.="FROM c_bank ";
            $vsql.="WHERE id_enterprise=" . $vflBank->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_bankNew"])>0) ){
                $vidBank=(int)($vrow["id_bankNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidBank;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
	public function __destruct(){ }
 }

?>