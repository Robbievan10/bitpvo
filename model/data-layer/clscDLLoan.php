<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLLoan.php");


class clscDLLoan
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflLoans, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_loan.*, c_dealer.*, c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate, c_enterprise.*";
            $vsql.=", c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate, c_operationstatus.fldoperationStatus ";
            $vsql.="FROM p_loan ";
			$vsql.="INNER JOIN c_dealer ON p_loan.id_enterprise=c_dealer.id_enterprise ";
            $vsql.="AND p_loan.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise ON c_dealer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="INNER JOIN c_operationstatus ON p_loan.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_loan.fldrecordDate DESC, c_dealer.fldfirstName, c_dealer.fldlastName, c_dealer.fldname";
            
            self::clean($vflLoans);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vloan= new clspFLLoan();
                $vloan->idLoan=(int)($vrow["p_loan.id_loan"]);
                $vloan->dealer->enterprise->idEnterprise=(int)($vrow["c_dealer.id_enterprise"]);
                $vloan->dealer->idDealer=(int)($vrow["c_dealer.id_dealer"]);
                $vloan->dealer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vloan->dealer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vloan->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vloan->dealer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vloan->dealer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vloan->dealer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vloan->dealer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vloan->dealer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vloan->dealer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vloan->dealer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vloan->dealer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vloan->dealer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vloan->dealer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vloan->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vloan->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vloan->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vloan->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vloan->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vloan->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vloan->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vloan->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vloan->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vloan->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vloan->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vloan->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vloan->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vloan->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vloan->operationStatus->idOperationStatus=(int)($vrow["p_loan.id_operationStatus"]);
                $vloan->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vloan->recordDate=date("m/d/Y", strtotime(trim($vrow["p_loan.fldrecordDate"])));
                $vloan->amount=(float)($vrow["p_loan.fldamount"]);
                $vloan->observation=trim($vrow["p_loan.fldobservation"]);
                
                self::add($vflLoans, $vloan);
                unset($vrow, $vloan);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflLoans, $vloan)
	 {
        try{
            array_push($vflLoans->loans, $vloan);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflLoans)
	 {
        try{
            return count($vflLoans->loans);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflLoans)
	 {
        try{
            $vflLoans->loans=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>