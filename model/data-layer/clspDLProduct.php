<?php

class clspDLProduct
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProduct, $vmySql)
	 {
		try{
            $vflProduct->idProduct=self::getIdFromDataBase($vflProduct, $vmySql);
            
            $vsql ="INSERT INTO c_product(id_enterprise, id_product, id_productType, fldkey, fldbarCode, fldname, flddescription";
            $vsql.=", fldimage, fldpurchasePrice, fldweightInKg, fldobservation) ";
			$vsql.="VALUES(" . $vflProduct->enterprise->idEnterprise;
            $vsql.=", " . $vflProduct->idProduct;
			$vsql.=", " . $vflProduct->productType->idProductType;
            $vsql.=", '" . $vflProduct->key . "'";
			$vsql.=", '" . $vflProduct->barCode . "'";
			$vsql.=", '" . $vflProduct->name . "'";
			$vsql.=", '" . $vflProduct->description . "'";
            $vsql.=", '" . $vflProduct->image . "'";
            $vsql.=", " . $vflProduct->purchasePrice;
            $vsql.=", " . $vflProduct->weightInKg;
			$vsql.=", '" . $vflProduct->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProduct, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_product ";
            $vsql.="SET id_productType=" . $vflProduct->productType->idProductType;
			$vsql.=", fldkey='" . $vflProduct->key . "'";
            $vsql.=", fldbarCode='" . $vflProduct->barCode . "'";
            $vsql.=", fldname='" . $vflProduct->name . "'";
            $vsql.=", flddescription='" . $vflProduct->description . "'";
            $vsql.=", fldimage='" . $vflProduct->image . "'";
            $vsql.=", fldpurchasePrice=" . $vflProduct->purchasePrice;
            $vsql.=", fldweightInKg=" . $vflProduct->weightInKg;
            $vsql.=", fldobservation='" . $vflProduct->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND id_product=" . $vflProduct->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateSalePriceInDataBase($vflProduct, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_product ";
            $vsql.="SET fldsalePrice=" . $vflProduct->salePrice . " ";
			$vsql.="WHERE id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND id_product=" . $vflProduct->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function appendStockInDataBase($vflProduct, $vmySql, $vamount, $vamountInKg)
	 {
		try{
			$vsql ="UPDATE c_product ";
            $vsql.="SET fldstock= fldstock + " . $vamount . " ";
            $vsql.=", fldstockInKg= fldstockInKg + " . $vamountInKg . " ";
			$vsql.="WHERE id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND id_product=" . $vflProduct->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function decreaseStockInDataBase($vflProduct, $vmySql, $vamount, $vamountInKg)
	 {
		try{
			$vsql ="UPDATE c_product ";
            $vsql.="SET fldstock= fldstock - " . $vamount . " ";
            $vsql.=", fldstockInKg= fldstockInKg - " . $vamountInKg . " ";
			$vsql.="WHERE id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND id_product=" . $vflProduct->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProduct, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_product ";
            $vsql.="WHERE id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND id_product=" . $vflProduct->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProduct, $vmySql)
	 {
		try{
			$vsql ="SELECT c_product.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_producttype.fldproductType ";
			$vsql.="FROM c_product ";
            $vsql.="INNER JOIN c_enterprise ON c_product.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
			$vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.="WHERE c_product.id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND c_product.id_product=" . $vflProduct->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflProduct->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflProduct->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflProduct->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflProduct->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflProduct->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflProduct->enterprise->locality=trim($vrow["fldlocality"]);
                $vflProduct->enterprise->street=trim($vrow["fldstreet"]);
                $vflProduct->enterprise->number=trim($vrow["fldnumber"]);
                $vflProduct->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflProduct->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflProduct->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflProduct->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflProduct->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflProduct->productType->idProductType=(int)($vrow["id_productType"]);
                $vflProduct->productType->enterprise=$vflProduct->enterprise;
                $vflProduct->productType->productType=trim($vrow["fldproductType"]);
                $vflProduct->key=trim($vrow["fldkey"]);
                $vflProduct->barCode=trim($vrow["fldbarCode"]);
                $vflProduct->name=trim($vrow["fldname"]);
                $vflProduct->description=trim($vrow["flddescription"]);
                $vflProduct->image=trim($vrow["fldimage"]);
                $vflProduct->purchasePrice=(float)($vrow["fldpurchasePrice"]);
                $vflProduct->salePrice=(float)($vrow["fldsalePrice"]);
                $vflProduct->weightInKg=(float)($vrow["fldweightInKg"]);
                $vflProduct->stock=(float)($vrow["fldstock"]);
                $vflProduct->stockInKg=(float)($vrow["fldstockInKg"]);
                $vflProduct->observation=trim($vrow["fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByKey($vflProduct, $vmySql)
	 {
		try{
			$vsql ="SELECT c_product.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_producttype.fldproductType ";
			$vsql.="FROM c_product ";
            $vsql.="INNER JOIN c_enterprise ON c_product.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
			$vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.="WHERE c_product.id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND c_product.fldkey='" . $vflProduct->key . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()>=1 ){
				$vrow=$vmySql->getData();
                $vflProduct->idProduct=(int)($vrow["id_product"]);
                $vflProduct->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflProduct->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflProduct->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflProduct->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflProduct->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflProduct->enterprise->locality=trim($vrow["fldlocality"]);
                $vflProduct->enterprise->street=trim($vrow["fldstreet"]);
                $vflProduct->enterprise->number=trim($vrow["fldnumber"]);
                $vflProduct->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflProduct->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflProduct->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflProduct->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflProduct->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflProduct->productType->idProductType=(int)($vrow["id_productType"]);
                $vflProduct->productType->productType=trim($vrow["fldproductType"]);
                $vflProduct->barCode=trim($vrow["fldbarCode"]);
                $vflProduct->name=trim($vrow["fldname"]);
                $vflProduct->description=trim($vrow["flddescription"]);
                $vflProduct->image=trim($vrow["fldimage"]);
                $vflProduct->purchasePrice=(float)($vrow["fldpurchasePrice"]);
                $vflProduct->salePrice=(float)($vrow["fldsalePrice"]);
                $vflProduct->weightInKg=(float)($vrow["fldweightInKg"]);
                $vflProduct->stock=(float)($vrow["fldstock"]);
                $vflProduct->stockInKg=(float)($vrow["fldstockInKg"]);
                $vflProduct->observation=trim($vrow["fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByBarCode($vflProduct, $vmySql)
	 {
		try{
			$vsql ="SELECT c_product.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_producttype.fldproductType ";
			$vsql.="FROM c_product ";
            $vsql.="INNER JOIN c_enterprise ON c_product.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
			$vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.="WHERE c_product.id_enterprise=" . $vflProduct->enterprise->idEnterprise . " ";
            $vsql.="AND c_product.fldbarCode='" . $vflProduct->barCode . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()>=1 ){
				$vrow=$vmySql->getData();
                $vflProduct->idProduct=(int)($vrow["id_product"]);
                $vflProduct->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflProduct->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflProduct->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflProduct->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflProduct->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflProduct->enterprise->locality=trim($vrow["fldlocality"]);
                $vflProduct->enterprise->street=trim($vrow["fldstreet"]);
                $vflProduct->enterprise->number=trim($vrow["fldnumber"]);
                $vflProduct->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflProduct->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflProduct->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflProduct->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflProduct->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflProduct->productType->idProductType=(int)($vrow["id_productType"]);
                $vflProduct->productType->productType=trim($vrow["fldproductType"]);
                $vflProduct->key=trim($vrow["fldkey"]);
                $vflProduct->name=trim($vrow["fldname"]);
                $vflProduct->description=trim($vrow["flddescription"]);
                $vflProduct->image=trim($vrow["fldimage"]);
                $vflProduct->purchasePrice=(float)($vrow["fldpurchasePrice"]);
                $vflProduct->salePrice=(float)($vrow["fldsalePrice"]);
                $vflProduct->weightInKg=(float)($vrow["fldweightInKg"]);
                $vflProduct->stock=(float)($vrow["fldstock"]);
                $vflProduct->stockInKg=(float)($vrow["fldstockInKg"]);
                $vflProduct->observation=trim($vrow["fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProduct, $vmySql)
	 {
		try{
            $vidProduct=1;
			$vsql ="SELECT MAX(id_product) + 1 AS id_productNew ";
			$vsql.="FROM c_product ";
            $vsql.="WHERE id_enterprise=" . $vflProduct->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productNew"])>0) ){
                $vidProduct=(int)($vrow["id_productNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProduct;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
	public function __destruct(){ }
 }

?>