<?php

class clspDLRoute
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflRoute, $vmySql)
	 {
		try{
            $vflRoute->idRoute=self::getIdFromDataBase($vflRoute, $vmySql);
            
            $vsql ="INSERT INTO c_route(id_enterprise, id_route, fldroute, flddescription, fldobservation) ";
			$vsql.="VALUES(" . $vflRoute->enterprise->idEnterprise;
            $vsql.=", " . $vflRoute->idRoute;
			$vsql.=", '" . $vflRoute->route . "'";
			$vsql.=", '" . $vflRoute->description . "'";
            $vsql.=", '" . $vflRoute->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflRoute, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_route ";
            $vsql.="SET fldroute='" . $vflRoute->route . "'";
            $vsql.=", flddescription='" . $vflRoute->description . "'";
            $vsql.=", fldobservation='" . $vflRoute->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflRoute->enterprise->idEnterprise . " ";
            $vsql.="AND id_route=" . $vflRoute->idRoute;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflRoute, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_route ";
            $vsql.="WHERE id_enterprise=" . $vflRoute->enterprise->idEnterprise . " ";
            $vsql.="AND id_route=" . $vflRoute->idRoute;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflRoute, $vmySql)
	 {
		try{
			$vsql ="SELECT c_route.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
			$vsql.="FROM c_route ";
            $vsql.="INNER JOIN c_enterprise ON c_route.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE c_route.id_enterprise=" . $vflRoute->enterprise->idEnterprise . " ";
            $vsql.="AND c_route.id_route=" . $vflRoute->idRoute;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflRoute->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflRoute->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflRoute->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflRoute->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflRoute->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflRoute->enterprise->locality=trim($vrow["fldlocality"]);
                $vflRoute->enterprise->street=trim($vrow["fldstreet"]);
                $vflRoute->enterprise->number=trim($vrow["fldnumber"]);
                $vflRoute->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflRoute->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflRoute->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflRoute->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflRoute->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflRoute->route=trim($vrow["fldroute"]);
                $vflRoute->description=trim($vrow["flddescription"]);
                $vflRoute->observation=trim($vrow["fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflRoute, $vmySql)
	 {
		try{
            $vidRoute=1;
			$vsql ="SELECT MAX(id_route) + 1 AS id_routeNew ";
			$vsql.="FROM c_route ";
            $vsql.="WHERE id_enterprise=" . $vflRoute->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_routeNew"])>0) ){
                $vidRoute=(int)($vrow["id_routeNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidRoute;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>