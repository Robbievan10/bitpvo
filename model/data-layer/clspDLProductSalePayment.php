<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLPayment.php");


class clspDLProductSalePayment
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflProductSalePayment, $vmySql, $vtype=0)
	 {
        try{
            if ( $vtype==0 ){
                $vmySql->startTransaction();
            }
            if ( clspDLPayment::addToDataBase($vflProductSalePayment, $vmySql)==1 ){
                $vsql ="INSERT INTO p_productsalepayment(id_enterprise, id_payment, id_productSale) ";
				$vsql.="VALUES(" . $vflProductSalePayment->productSale->enterprise->idEnterprise;
                $vsql.=", " . $vflProductSalePayment->idPayment;
                $vsql.=", '" . $vflProductSalePayment->productSale->idProductSale ."')";
                
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    if ( $vtype==0 ){        
                        $vmySql->rollbackTransaction();
                    }
                    return 0;
                }
            }
            else{
                if ( $vtype==0 ){
                    $vmySql->rollbackTransaction();
                }
                return -1;
            }
            if ( $vtype==0 ){
                $vmySql->commitTransaction();
            }
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
            if ( $vtype==0 ){
                $vmySql->rollbackTransaction();
            }
		    throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProductSalePayment, $vmySql, $vtype=0)
	 {
		try{
            $vsql ="DELETE FROM p_productsalepayment ";
            $vsql.="WHERE id_enterprise=" . $vflProductSalePayment->productSale->enterprise->idEnterprise . " ";
            $vsql.="AND id_payment=" . $vflProductSalePayment->idPayment . " ";
            $vsql.="AND id_productSale='" . $vflProductSalePayment->productSale->idProductSale . "'";
            
            if ( $vtype==0 ){
                $vmySql->startTransaction();
            }
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLPayment::deleteInDataBase($vflProductSalePayment, $vmySql)==0 ){
                    if ( $vtype==0 ){
                        $vmySql->rollbackTransaction();
                    }
                    return -1;
                }
            }
            else{
                if ( $vtype==0 ){  
                    $vmySql->rollbackTransaction();
                }
                return 0;
            }
            if ( $vtype==0 ){
                $vmySql->commitTransaction();
            }
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
		    if ( $vtype==0 ){
                $vmySql->rollbackTransaction();
            }
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>