<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductType.php");


class clscDLProductType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductTypes, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_producttype.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_producttype ";
            $vsql.="INNER JOIN c_enterprise ON c_producttype.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_producttype.fldproductType";
            
            self::clean($vflProductTypes);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductType= new clspFLProductType();
                $vproductType->idProductType=(int)($vrow["id_productType"]);
                $vproductType->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vproductType->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vproductType->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vproductType->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vproductType->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vproductType->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vproductType->enterprise->locality=trim($vrow["fldlocality"]);
                $vproductType->enterprise->street=trim($vrow["fldstreet"]);
                $vproductType->enterprise->number=trim($vrow["fldnumber"]);
                $vproductType->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vproductType->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vproductType->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vproductType->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vproductType->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vproductType->productType=trim($vrow["fldproductType"]);
                
                self::add($vflProductTypes, $vproductType);
                unset($vrow, $vproductType);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflProductTypes, $vproductType)
	 {
        try{
            array_push($vflProductTypes->productTypes, $vproductType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflProductTypes)
	 {
        try{
            return count($vflProductTypes->productTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductTypes)
	 {
        try{
            $vflProductTypes->productTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>