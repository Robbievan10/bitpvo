<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashMovementStatus.php");


class clscDLCashMovementStatus
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflCashMovementStatuses, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_cashmovementstatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY fldcashMovementStatus";
            
            self::clean($vflCashMovementStatuses);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcashMovementStatus= new clspFLCashMovementStatus();
                $vcashMovementStatus->idCashMovementStatus=(int)($vrow["id_cashMovementStatus"]);
                $vcashMovementStatus->cashMovementStatus=trim($vrow["fldcashMovementStatus"]);
                
                self::add($vflCashMovementStatuses, $vcashMovementStatus);
                unset($vrow, $vcashMovementStatus);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflCashMovementStatuses, $vcashMovementStatus)
	 {
        try{
            array_push($vflCashMovementStatuses->cashMovementStatuses, $vcashMovementStatus);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflCashMovementStatuses)
	 {
        try{
            return count($vflCashMovementStatuses->cashMovementStatuses);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflCashMovementStatuses)
	 {
        try{
            $vflCashMovementStatuses->cashMovementStatuses=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>