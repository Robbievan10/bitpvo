<?php

class clspDLRouteCustomer
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflRouteCustomer, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vsql ="INSERT INTO c_routecustomer(id_enterprise, id_route, id_customer, fldassignDate) ";
			$vsql.="VALUES(" . $vflRouteCustomer->route->enterprise->idEnterprise;
            $vsql.=", " . $vflRouteCustomer->route->idRoute;
			$vsql.=", " . $vflRouteCustomer->customer->idCustomer;
			$vsql.=", '" . date("Y-m-d", strtotime($vflRouteCustomer->assignDate)) . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflRouteCustomer, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE c_routecustomer ";
            $vsql.="SET fldassignDate='" . date("Y-m-d", strtotime($vflRouteCustomer->assignDate)) . "'";
			$vsql.=", fldsunday=" . $vflRouteCustomer->sunday;
            $vsql.=", fldmonday=" . $vflRouteCustomer->monday;
            $vsql.=", fldtuesday=" . $vflRouteCustomer->tuesday;
            $vsql.=", fldwednesday=" . $vflRouteCustomer->wednesday;
            $vsql.=", fldthursday=" . $vflRouteCustomer->thursday;
            $vsql.=", fldfriday=" . $vflRouteCustomer->friday;
            $vsql.=", fldsaturday=" . $vflRouteCustomer->saturday . " ";
			$vsql.="WHERE id_enterprise=" . $vflRouteCustomer->route->enterprise->idEnterprise . " ";
            $vsql.="AND id_route=" . $vflRouteCustomer->route->idRoute . " ";
            $vsql.="AND id_customer=" . $vflRouteCustomer->customer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflRouteCustomer, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_routecustomer ";
            $vsql.="WHERE id_enterprise=" . $vflRouteCustomer->route->enterprise->idEnterprise . " ";
            $vsql.="AND id_route=" . $vflRouteCustomer->route->idRoute . " ";
            $vsql.="AND id_customer=" . $vflRouteCustomer->customer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflRouteCustomer, $vmySql)
	 {
		try{
            $vsql ="SELECT c_routecustomer.*, c_route.*, c_routeenterprise.*, c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate";
            $vsql.=", c_customer.*, c_customerenterprise.*, c_customerenterprisemunicipality.fldmunicipality, c_customerenterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_customermunicipality.fldmunicipality, c_customerstate.fldstate ";
            $vsql.="FROM c_routecustomer ";
            $vsql.="INNER JOIN c_route ON c_routecustomer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routecustomer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_customer ON c_routecustomer.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise AS c_customerenterprise ON c_customer.id_enterprise=c_customerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_customerenterprisemunicipality ON c_customerenterprise.id_state=c_customerenterprisemunicipality.id_state ";
            $vsql.="AND c_customerenterprise.id_municipality=c_customerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_customerenterprisestate ON c_customerenterprisemunicipality.id_state=c_customerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="WHERE c_routecustomer.id_enterprise=" . $vflRouteCustomer->route->enterprise->idEnterprise . " ";
            $vsql.="AND c_routecustomer.id_route=" . $vflRouteCustomer->route->idRoute. " ";
            $vsql.="AND c_routecustomer.id_customer=" . $vflRouteCustomer->customer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflRouteCustomer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vflRouteCustomer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vflRouteCustomer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vflRouteCustomer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vflRouteCustomer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vflRouteCustomer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vflRouteCustomer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vflRouteCustomer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vflRouteCustomer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vflRouteCustomer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vflRouteCustomer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vflRouteCustomer->route->route=trim($vrow["c_route.fldroute"]);
                $vflRouteCustomer->route->description=trim($vrow["c_route.flddescription"]);
                $vflRouteCustomer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vflRouteCustomer->customer->enterprise->idEnterprise=(int)($vrow["c_routecustomer.id_enterprise"]);
                $vflRouteCustomer->customer->enterprise->municipality->state->idState=(int)($vrow["c_customerenterprise.id_state"]);
                $vflRouteCustomer->customer->enterprise->municipality->state->state=trim($vrow["c_customerenterprisestate.fldstate"]);
                $vflRouteCustomer->customer->enterprise->municipality->idMunicipality=(int)($vrow["c_customerenterprise.id_municipality"]);
                $vflRouteCustomer->customer->enterprise->municipality->municipality=trim($vrow["c_customerenterprisemunicipality.fldmunicipality"]);
                $vflRouteCustomer->customer->enterprise->enterprise=trim($vrow["c_customerenterprise.fldenterprise"]);
                $vflRouteCustomer->customer->enterprise->locality=trim($vrow["c_customerenterprise.fldlocality"]);
                $vflRouteCustomer->customer->enterprise->street=trim($vrow["c_customerenterprise.fldstreet"]);
                $vflRouteCustomer->customer->enterprise->number=trim($vrow["c_customerenterprise.fldnumber"]);                
                $vflRouteCustomer->customer->enterprise->phoneNumber=trim($vrow["c_customerenterprise.fldphoneNumber"]);
                $vflRouteCustomer->customer->enterprise->movilNumber=trim($vrow["c_customerenterprise.fldmovilNumber"]);
                $vflRouteCustomer->customer->enterprise->pageWeb=trim($vrow["c_customerenterprise.fldpageWeb"]);
				$vflRouteCustomer->customer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vflRouteCustomer->customer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflRouteCustomer->customer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vflRouteCustomer->customer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vflRouteCustomer->customer->locality=trim($vrow["c_customer.fldlocality"]);
                $vflRouteCustomer->customer->street=trim($vrow["c_customer.fldstreet"]);
                $vflRouteCustomer->customer->number=trim($vrow["c_customer.fldnumber"]);
                $vflRouteCustomer->customer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vflRouteCustomer->customer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vflRouteCustomer->customer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vflRouteCustomer->customer->email=trim($vrow["c_customer.fldemail"]);
                $vflRouteCustomer->customer->observation=trim($vrow["c_customer.fldobservation"]);
                $vflRouteCustomer->assignDate=date("m/d/Y", strtotime(trim($vrow["c_routecustomer.fldassignDate"])));
                $vflRouteCustomer->sunday=(int)($vrow["c_routecustomer.fldsunday"]);
                $vflRouteCustomer->monday=(int)($vrow["c_routecustomer.fldmonday"]);
                $vflRouteCustomer->tuesday=(int)($vrow["c_routecustomer.fldtuesday"]);
                $vflRouteCustomer->wednesday=(int)($vrow["c_routecustomer.fldwednesday"]);
                $vflRouteCustomer->thursday=(int)($vrow["c_routecustomer.fldthursday"]);
                $vflRouteCustomer->friday=(int)($vrow["c_routecustomer.fldfriday"]);
                $vflRouteCustomer->saturday=(int)($vrow["c_routecustomer.fldsaturday"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>