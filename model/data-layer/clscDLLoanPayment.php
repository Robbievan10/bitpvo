<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLLoanPayment.php");


class clscDLLoanPayment
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflLoansPayments, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_loanpayment.*, p_loan.*, c_dealer.*, c_dealerenterprise.*, c_dealerenterprisemunicipality.fldmunicipality";
            $vsql.=", c_dealerenterprisestate.fldstate, c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate, c_operationstatus.fldoperationStatus";
            $vsql.=", p_payment.*, p_paymententerprise.*, p_paymententerprisemunicipality.fldmunicipality, p_paymententerprisestate.fldstate";
            $vsql.=", c_paymenttype.fldpaymentType ";
            $vsql.="FROM p_loanpayment ";
			$vsql.="INNER JOIN p_loan ON p_loanpayment.id_enterprise=p_loan.id_enterprise ";
            $vsql.="AND p_loanpayment.id_dealer=p_loan.id_dealer ";
            $vsql.="AND p_loanpayment.id_loan=p_loan.id_loan ";
            $vsql.="INNER JOIN c_dealer ON p_loan.id_enterprise=c_dealer.id_enterprise ";
            $vsql.="AND p_loan.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="INNER JOIN c_operationstatus ON p_loan.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="INNER JOIN p_payment ON p_loanpayment.id_enterprise=p_payment.id_enterprise ";
            $vsql.="AND p_loanpayment.id_payment=p_payment.id_payment ";
            $vsql.="INNER JOIN c_enterprise AS p_paymententerprise ON p_payment.id_enterprise=p_paymententerprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS p_paymententerprisemunicipality ON p_paymententerprise.id_state=p_paymententerprisemunicipality.id_state ";
            $vsql.="AND p_paymententerprise.id_municipality=p_paymententerprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS p_paymententerprisestate ON p_paymententerprisemunicipality.id_state=p_paymententerprisestate.id_state ";
			$vsql.="INNER JOIN c_paymenttype ON p_payment.id_paymentType=c_paymenttype.id_paymentType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_loanpayment.id_enterprise, p_loanpayment.id_dealer, p_loanpayment.id_loan, p_loanpayment.id_payment";
            
            self::clean($vflLoansPayments);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vloanPayment= new clspFLLoanPayment();
                $vloanPayment->idPayment=(int)($vrow["p_loanpayment.id_payment"]);
                $vloanPayment->loan->idLoan=(int)($vrow["p_loanpayment.id_loan"]);
                $vloanPayment->loan->dealer->idDealer=(int)($vrow["p_loanpayment.id_dealer"]);
                $vloanPayment->loan->dealer->enterprise->idEnterprise=(int)($vrow["p_loanpayment.id_enterprise"]);
                $vloanPayment->loan->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vloanPayment->loan->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vloanPayment->loan->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vloanPayment->loan->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vloanPayment->loan->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vloanPayment->loan->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vloanPayment->loan->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vloanPayment->loan->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vloanPayment->loan->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vloanPayment->loan->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vloanPayment->loan->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vloanPayment->loan->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vloanPayment->loan->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vloanPayment->loan->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vloanPayment->loan->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vloanPayment->loan->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vloanPayment->loan->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vloanPayment->loan->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vloanPayment->loan->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vloanPayment->loan->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vloanPayment->loan->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vloanPayment->loan->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vloanPayment->loan->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vloanPayment->loan->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vloanPayment->loan->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vloanPayment->loan->operationStatus->idOperationStatus=(int)($vrow["p_loan.id_operationStatus"]);
                $vloanPayment->loan->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vloanPayment->loan->recordDate=date("m/d/Y", strtotime(trim($vrow["p_loan.fldrecordDate"])));
                $vloanPayment->loan->amount=(float)($vrow["p_loan.fldamount"]);
                $vloanPayment->loan->observation=trim($vrow["p_loan.fldobservation"]);
                $vloanPayment->enterprise->idEnterprise=(int)($vrow["p_loanpayment.id_enterprise"]);
				$vloanPayment->enterprise->municipality->state->idState=(int)($vrow["p_paymententerprise.id_state"]);
                $vloanPayment->enterprise->municipality->state->state=trim($vrow["p_paymententerprisestate.fldstate"]);
                $vloanPayment->enterprise->municipality->idMunicipality=(int)($vrow["p_paymententerprise.id_municipality"]);
                $vloanPayment->enterprise->municipality->municipality=trim($vrow["p_paymententerprisemunicipality.fldmunicipality"]);
                $vloanPayment->enterprise->enterprise=trim($vrow["p_paymententerprise.fldenterprise"]);
                $vloanPayment->enterprise->locality=trim($vrow["p_paymententerprise.fldlocality"]);
                $vloanPayment->enterprise->street=trim($vrow["p_paymententerprise.fldstreet"]);
                $vloanPayment->enterprise->number=trim($vrow["p_paymententerprise.fldnumber"]);
                $vloanPayment->enterprise->phoneNumber=trim($vrow["p_paymententerprise.fldphoneNumber"]);
                $vloanPayment->enterprise->movilNumber=trim($vrow["p_paymententerprise.fldmovilNumber"]);
                $vloanPayment->enterprise->pageWeb=trim($vrow["p_paymententerprise.fldpageWeb"]);
				$vloanPayment->paymentType->idPaymentType=(int)($vrow["p_payment.id_paymentType"]);
				$vloanPayment->paymentType->paymentType=trim($vrow["c_paymenttype.fldpaymentType"]);
                $vloanPayment->recordDate=date("m/d/Y", strtotime(trim($vrow["p_payment.fldrecordDate"])));
				$vloanPayment->paymentDate=date("m/d/Y", strtotime(trim($vrow["p_payment.fldpaymentDate"])));
				$vloanPayment->amount=(float)($vrow["p_payment.fldamount"]);
				
                self::add($vflLoansPayments, $vloanPayment);
                unset($vrow, $vloanPayment);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryGroupByDealerToDataBase($vflLoansPayments, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_loanpayment.id_dealer, c_dealer.fldfirstName, c_dealer.fldlastName, c_dealer.fldname"; 
            $vsql.=", SUM(p_payment.fldamount) AS amountTotal ";
            $vsql.="FROM p_loanpayment ";
            $vsql.="INNER JOIN p_loan ON p_loanpayment.id_enterprise=p_loan.id_enterprise "; 
            $vsql.="AND p_loanpayment.id_dealer=p_loan.id_dealer ";
            $vsql.="AND p_loanpayment.id_loan=p_loan.id_loan ";
            $vsql.="INNER JOIN c_dealer ON c_dealer.id_enterprise=p_loan.id_enterprise "; 
            $vsql.="AND c_dealer.id_dealer=p_loan.id_dealer ";
            $vsql.="INNER JOIN p_payment ON p_loanpayment.id_enterprise=p_payment.id_enterprise "; 
            $vsql.="AND p_loanpayment.id_payment=p_payment.id_payment ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_loanpayment.id_dealer ";
            $vsql.="ORDER BY c_dealer.fldname, c_dealer.fldfirstName, c_dealer.fldlastName";
            
            self::clean($vflLoansPayments);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vloanPayment= new clspFLLoanPayment();
                $vloanPayment->loan->dealer->idDealer=(int)($vrow["id_dealer"]);
                $vloanPayment->loan->dealer->firstName=trim($vrow["fldfirstName"]);
                $vloanPayment->loan->dealer->lastName=trim($vrow["fldlastName"]);
                $vloanPayment->loan->dealer->name=trim($vrow["fldname"]);
                $vloanPayment->amount=(float)($vrow["amountTotal"]);
                
                self::add($vflLoansPayments, $vloanPayment);
                unset($vrow, $vloanPayment);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflLoansPayments, $vloanPayment)
	 {
        try{
            array_push($vflLoansPayments->loansPayments, $vloanPayment);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflLoansPayments)
	 {
        try{
            return count($vflLoansPayments->loansPayments);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflLoansPayments)
	 {
        try{
            $vflLoansPayments->loansPayments=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
    public static function totalAmount($vflLoansPayments)
	 {
        try{
            $vtotalAmount=0;
            $vloansPaymentsTotal=self::total($vflLoansPayments);
            for($vi=0; $vi<$vloansPaymentsTotal; $vi++){
                $vtotalAmount+=$vflLoansPayments->loansPayments[$vi]->amount;
            }
            return $vtotalAmount;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>