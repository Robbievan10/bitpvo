<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductDistributionDetail.php");


class clscDLProductDistributionDetail
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsDistributionsDetails, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productdistributiondetail.*, p_productdistribution.*, c_productdistributionenterprise.*";
            $vsql.=", c_productdistributionenterprisemunicipality.fldmunicipality, c_productdistributionenterprisestate.fldstate, c_routedealer.*, c_route.*";
            $vsql.=", c_routeenterprise.*, c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate, c_dealer.*, c_dealerenterprise.*";
            $vsql.=", c_dealerenterprisemunicipality.fldmunicipality, c_dealerenterprisestate.fldstate, c_dealermunicipality.fldmunicipality";
			$vsql.=", c_dealerstate.fldstate, c_product.*, c_productenterprise.*, c_productenterprisemunicipality.fldmunicipality";
            $vsql.=", c_productenterprisestate.fldstate, c_producttype.fldproductType ";
            $vsql.="FROM p_productdistributiondetail ";
			$vsql.="INNER JOIN p_productdistribution ON p_productdistributiondetail.id_enterprise=p_productdistribution.id_enterprise ";
            $vsql.="AND p_productdistributiondetail.id_productDistribution=p_productdistribution.id_productDistribution ";
           	$vsql.="INNER JOIN c_enterprise AS c_productdistributionenterprise ON p_productdistribution.id_enterprise=c_productdistributionenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productdistributionenterprisemunicipality ON c_productdistributionenterprise.id_state=c_productdistributionenterprisemunicipality.id_state ";
            $vsql.="AND c_productdistributionenterprise.id_municipality=c_productdistributionenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productdistributionenterprisestate ON c_productdistributionenterprisemunicipality.id_state=c_productdistributionenterprisestate.id_state ";
            $vsql.="INNER JOIN c_routedealer ON p_productdistribution.id_enterprise=c_routedealer.id_enterprise ";
            $vsql.="AND p_productdistribution.id_route=c_routedealer.id_route ";
			$vsql.="INNER JOIN c_route ON c_routedealer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routedealer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="INNER JOIN c_product ON p_productdistributiondetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productdistributiondetail.id_product=c_product.id_product ";
            $vsql.="INNER JOIN c_enterprise AS c_productenterprise ON c_product.id_enterprise=c_productenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productenterprisemunicipality  ON c_productenterprise.id_state=c_productenterprisemunicipality.id_state ";
            $vsql.="AND c_productenterprise.id_municipality=c_productenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productenterprisestate ON c_productenterprisemunicipality.id_state=c_productenterprisestate.id_state ";
            $vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productdistributiondetail.id_enterprise, p_productdistributiondetail.id_productDistribution, p_productdistributiondetail.id_product";
            
            self::clean($vflProductsDistributionsDetails);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductDistributionDetail= new clspFLProductDistributionDetail();
                $vproductDistributionDetail->productDistribution->idProductDistribution=trim($vrow["p_productdistributiondetail.id_productDistribution"]);
                $vproductDistributionDetail->productDistribution->enterprise->idEnterprise=(int)($vrow["p_productdistributiondetail.id_enterprise"]);
                $vproductDistributionDetail->productDistribution->enterprise->municipality->state->idState=(int)($vrow["c_productdistributionenterprise.id_state"]);
                $vproductDistributionDetail->productDistribution->enterprise->municipality->state->state=trim($vrow["c_productdistributionenterprisestate.fldstate"]);
                $vproductDistributionDetail->productDistribution->enterprise->municipality->idMunicipality=(int)($vrow["c_productdistributionenterprise.id_municipality"]);
                $vproductDistributionDetail->productDistribution->enterprise->municipality->municipality=trim($vrow["c_productdistributionenterprisemunicipality.fldmunicipality"]);
                $vproductDistributionDetail->productDistribution->enterprise->enterprise=trim($vrow["c_productdistributionenterprise.fldenterprise"]);
                $vproductDistributionDetail->productDistribution->enterprise->locality=trim($vrow["c_productdistributionenterprise.fldlocality"]);
                $vproductDistributionDetail->productDistribution->enterprise->street=trim($vrow["c_productdistributionenterprise.fldstreet"]);
                $vproductDistributionDetail->productDistribution->enterprise->number=trim($vrow["c_productdistributionenterprise.fldnumber"]);
                $vproductDistributionDetail->productDistribution->enterprise->phoneNumber=trim($vrow["c_productdistributionenterprise.fldphoneNumber"]);
                $vproductDistributionDetail->productDistribution->enterprise->movilNumber=trim($vrow["c_productdistributionenterprise.fldmovilNumber"]);
                $vproductDistributionDetail->productDistribution->enterprise->pageWeb=trim($vrow["c_productdistributionenterprise.fldpageWeb"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->idRoute=(int)($vrow["p_productdistribution.id_route"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->idEnterprise=(int)($vrow["p_productdistribution.id_enterprise"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->route=trim($vrow["c_route.fldroute"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->description=trim($vrow["c_route.flddescription"]);
                $vproductDistributionDetail->productDistribution->routeDealer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->idDealer=(int)($vrow["c_routedealer.id_dealer"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->idEnterprise=(int)($vrow["c_routedealer.id_enterprise"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vproductDistributionDetail->productDistribution->routeDealer->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vproductDistributionDetail->productDistribution->routeDealer->assignDate=date("m/d/Y", strtotime(trim($vrow["c_routedealer.fldassignDate"])));
				$vproductDistributionDetail->productDistribution->productDistributionRecordDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionRecordDate"])));
				$vproductDistributionDetail->productDistribution->productDistributionDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionDate"])));
				$vproductDistributionDetail->productDistribution->productDistributionCuttingDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionCuttingDate"])));
				$vproductDistributionDetail->productDistribution->creditTotal=(float)($vrow["p_productdistribution.fldcreditTotal"]);
				$vproductDistributionDetail->productDistribution->discountTotal=(float)($vrow["p_productdistribution.flddiscountTotal"]);
                $vproductDistributionDetail->productDistribution->devolutionTotal=(float)($vrow["p_productdistribution.flddevolutionTotal"]);
                $vproductDistributionDetail->productDistribution->observation=trim($vrow["p_productdistribution.fldobservation"]);
                $vproductDistributionDetail->product->idProduct=(int)($vrow["p_productdistributiondetail.id_product"]);
                $vproductDistributionDetail->product->enterprise->idEnterprise=(int)($vrow["p_productdistributiondetail.id_enterprise"]);
                $vproductDistributionDetail->product->enterprise->municipality->state->idState=(int)($vrow["c_productenterprise.id_state"]);
                $vproductDistributionDetail->product->enterprise->municipality->state->state=trim($vrow["c_productenterprisestate.fldstate"]);
                $vproductDistributionDetail->product->enterprise->municipality->idMunicipality=(int)($vrow["c_productenterprise.id_municipality"]);
                $vproductDistributionDetail->product->enterprise->municipality->municipality=trim($vrow["c_productenterprisemunicipality.fldmunicipality"]);
                $vproductDistributionDetail->product->enterprise->enterprise=trim($vrow["c_productenterprise.fldenterprise"]);
                $vproductDistributionDetail->product->enterprise->locality=trim($vrow["c_productenterprise.fldlocality"]);
                $vproductDistributionDetail->product->enterprise->street=trim($vrow["c_productenterprise.fldstreet"]);
                $vproductDistributionDetail->product->enterprise->number=trim($vrow["c_productenterprise.fldnumber"]);
                $vproductDistributionDetail->product->enterprise->phoneNumber=trim($vrow["c_productenterprise.fldphoneNumber"]);
                $vproductDistributionDetail->product->enterprise->movilNumber=trim($vrow["c_productenterprise.fldmovilNumber"]);
                $vproductDistributionDetail->product->enterprise->pageWeb=trim($vrow["c_productenterprise.fldpageWeb"]);
                $vproductDistributionDetail->product->productType->idProductType=(int)($vrow["c_product.id_productType"]);
                $vproductDistributionDetail->product->productType->enterprise=$vproductDistributionDetail->productDistribution->enterprise;
                $vproductDistributionDetail->product->productType->productType=trim($vrow["c_producttype.fldproductType"]);
                $vproductDistributionDetail->product->key=trim($vrow["c_product.fldkey"]);
                $vproductDistributionDetail->product->barCode=trim($vrow["c_product.fldbarCode"]);
                $vproductDistributionDetail->product->name=trim($vrow["c_product.fldname"]);
                $vproductDistributionDetail->product->description=trim($vrow["c_product.flddescription"]);
                $vproductDistributionDetail->product->image=trim($vrow["c_product.fldimage"]);
                $vproductDistributionDetail->product->purchasePrice=(float)($vrow["c_product.fldpurchasePrice"]);
                $vproductDistributionDetail->product->salePrice=(float)($vrow["c_product.fldsalePrice"]);
                $vproductDistributionDetail->product->weightInKg=(float)($vrow["c_product.fldweightInKg"]);
                $vproductDistributionDetail->product->stock=(float)($vrow["c_product.fldstock"]);
                $vproductDistributionDetail->product->observation=trim($vrow["c_product.fldobservation"]);
                $vproductDistributionDetail->distributionAmount=(float)($vrow["p_productdistributiondetail.flddistributionAmount"]);
                $vproductDistributionDetail->distributionPrice=(float)($vrow["p_productdistributiondetail.flddistributionPrice"]);
                $vproductDistributionDetail->distributionWeightInkg=(float)($vrow["p_productdistributiondetail.flddistributionWeightInKg"]);
                $vproductDistributionDetail->returnedAmount=(float)($vrow["p_productdistributiondetail.fldreturnedAmount"]);
                
                self::add($vflProductsDistributionsDetails, $vproductDistributionDetail);
                unset($vrow, $vproductDistributionDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function add($vflProductsDistributionsDetails, $vproductDistributionDetail)
	 {
        try{
            array_push($vflProductsDistributionsDetails->productsDistributionsDetails, $vproductDistributionDetail);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsDistributionsDetails)
	 {
        try{
            return count($vflProductsDistributionsDetails->productsDistributionsDetails);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsDistributionsDetails)
	 {
        try{
            $vflProductsDistributionsDetails->productsDistributionsDetails=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function distributionTotal($vflProductsDistributionsDetails)
	 {
        try{
            $vdistributionTotal=0;
            $vproductsDistributionsDetailsTotal=self::total($vflProductsDistributionsDetails);
            for($vi=0; $vi<$vproductsDistributionsDetailsTotal; $vi++){
                $vdistributionTotal+=$vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionAmount *
                                     $vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionPrice;
            }
            return $vdistributionTotal;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function distributedTotal($vflProductsDistributionsDetails)
	 {
        try{
            $vdistributedTotal=0;
            $vproductsDistributionsDetailsTotal=self::total($vflProductsDistributionsDetails);
            for($vi=0; $vi<$vproductsDistributionsDetailsTotal; $vi++){
                $vdistributedTotal+=($vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionAmount -
                                     $vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->returnedAmount) *
                                     $vflProductsDistributionsDetails->productsDistributionsDetails[$vi]->distributionPrice;
            }
            return $vdistributedTotal;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public function __destruct(){ }
 }

?>