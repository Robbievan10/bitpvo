<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLState.php");


class clscDLState
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflStates, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY fldstate";
            
            self::clean($vflStates);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vstate= new clspFLState();
                $vstate->idState=(int)($vrow["id_state"]);
                $vstate->state=trim($vrow["fldstate"]);
                
                self::add($vflStates, $vstate);
                unset($vrow, $vstate);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflStates, $vstate)
	 {
        try{
            array_push($vflStates->states, $vstate);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflStates)
	 {
        try{
            return count($vflStates->states);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflStates)
	 {
        try{
            $vflStates->states=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>