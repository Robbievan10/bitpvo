<?php

class clspDLExpense
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflExpense, $vmySql)
	 {
		try{
            $vflExpense->idExpense=self::getIdFromDataBase($vflExpense, $vmySql);
            
            $vsql ="INSERT INTO c_expense(id_enterprise, id_expense, id_expenseType, fldexpense, flddescription) ";
			$vsql.="VALUES(" . $vflExpense->enterprise->idEnterprise;
            $vsql.=", " . $vflExpense->idExpense;
            $vsql.=", " . $vflExpense->expenseType->idExpenseType;
			$vsql.=", '" . $vflExpense->expense . "'";
            $vsql.=", '" . $vflExpense->description . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflExpense, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_expense ";
            $vsql.="SET id_expenseType=" . $vflExpense->expenseType->idExpenseType;
            $vsql.=", fldexpense='" . $vflExpense->expense . "'";
            $vsql.=", flddescription='" . $vflExpense->description . "'";
			$vsql.="WHERE id_enterprise=" . $vflExpense->enterprise->idEnterprise . " ";
            $vsql.="AND id_expense=" . $vflExpense->idExpense;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflExpense, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_expense ";
            $vsql.="WHERE id_enterprise=" . $vflExpense->enterprise->idEnterprise . " ";
            $vsql.="AND id_expense=" . $vflExpense->idExpense;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflExpense, $vmySql)
	 {
		try{
			$vsql ="SELECT c_expense.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_expensetype.fldexpenseType ";
			$vsql.="FROM c_expense ";
            $vsql.="INNER JOIN c_enterprise ON c_expense.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_expensetype ON c_expense.id_enterprise=c_expensetype.id_enterprise ";
            $vsql.="AND c_expense.id_expenseType=c_expensetype.id_expenseType ";
            $vsql.="INNER JOIN c_enterprise AS c_expensetypeenterprise ON c_expensetype.id_enterprise=c_expensetypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_expensetypemunicipality ON c_expensetypeenterprise.id_state=c_expensetypemunicipality.id_state ";
            $vsql.="AND c_expensetypeenterprise.id_municipality=c_expensetypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_expensetypestate ON c_expensetypemunicipality.id_state=c_expensetypestate.id_state ";
            $vsql.="WHERE c_expense.id_enterprise=" . $vflExpense->enterprise->idEnterprise . " ";
            $vsql.="AND c_expense.id_expense=" . $vflExpense->idExpense;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflExpense->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflExpense->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflExpense->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflExpense->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflExpense->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflExpense->enterprise->locality=trim($vrow["fldlocality"]);
                $vflExpense->enterprise->street=trim($vrow["fldstreet"]);
                $vflExpense->enterprise->number=trim($vrow["fldnumber"]);
                $vflExpense->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflExpense->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflExpense->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflExpense->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflExpense->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflExpense->expenseType->idExpenseType=(int)($vrow["id_expenseType"]);
                $vflExpense->expenseType->enterprise=$vflExpense->enterprise;
                $vflExpense->expenseType->expenseType=trim($vrow["fldexpenseType"]);
                $vflExpense->expense=trim($vrow["fldexpense"]);
                $vflExpense->description=trim($vrow["flddescription"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflExpense, $vmySql)
	 {
		try{
            $vidExpense=1;
			$vsql ="SELECT MAX(id_expense) + 1 AS id_expenseNew ";
			$vsql.="FROM c_expense ";
            $vsql.="WHERE id_enterprise=" . $vflExpense->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_expenseNew"])>0) ){
                $vidExpense=(int)($vrow["id_expenseNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidExpense;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>