<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductSaleDetail.php");


class clscDLProductSaleDetail
 {
    public function __construct() { }


    public static function queryToDataBase($vflProductsSalesDetails, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productsaledetail.*, p_productsale.*, c_productsaleenterprise.*, c_productsaleenterprisemunicipality.fldmunicipality";
			$vsql.=", c_productsaleenterprisestate.fldstate, c_customer.*, c_customerenterprise.*, c_customerenterprisemunicipality.fldmunicipality";
			$vsql.=", c_customerenterprisestate.fldstate, c_persontype.fldpersonType, c_customermunicipality.fldmunicipality";
			$vsql.=", c_customerstate.fldstate, c_operationtype.fldoperationType, c_operationstatus.fldoperationStatus, c_product.*";
            $vsql.=", c_productenterprise.*, c_productenterprisemunicipality.fldmunicipality, c_productenterprisestate.fldstate";
			$vsql.=", c_producttype.fldproductType ";
            $vsql.="FROM p_productsaledetail ";
			$vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
			$vsql.="INNER JOIN c_enterprise AS c_productsaleenterprise ON p_productsale.id_enterprise=c_productsaleenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productsaleenterprisemunicipality ON c_productsaleenterprise.id_state=c_productsaleenterprisemunicipality.id_state ";
            $vsql.="AND c_productsaleenterprise.id_municipality=c_productsaleenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productsaleenterprisestate ON c_productsaleenterprisemunicipality.id_state=c_productsaleenterprisestate.id_state ";
            $vsql.="INNER JOIN c_customer ON p_productsale.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise AS c_customerenterprise ON c_customer.id_enterprise=c_customerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_customerenterprisemunicipality ON c_customerenterprise.id_state=c_customerenterprisemunicipality.id_state ";
            $vsql.="AND c_customerenterprise.id_municipality=c_customerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_customerenterprisestate ON c_customerenterprisemunicipality.id_state=c_customerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productsale.id_operationStatus=c_operationstatus.id_operationStatus ";
			$vsql.="INNER JOIN c_product ON p_productsaledetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_product=c_product.id_product ";
            $vsql.="INNER JOIN c_enterprise AS c_productenterprise ON c_product.id_enterprise=c_productenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productenterprisemunicipality  ON c_productenterprise.id_state=c_productenterprisemunicipality.id_state ";
            $vsql.="AND c_productenterprise.id_municipality=c_productenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productenterprisestate ON c_productenterprisemunicipality.id_state=c_productenterprisestate.id_state ";
            $vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productsaledetail.id_enterprise, p_productsaledetail.id_productSale, p_productsaledetail.id_product";

            self::clean($vflProductsSalesDetails);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductSaleDetail= new clspFLProductSaleDetail();
				$vproductSaleDetail->productSale->idProductSale=trim($vrow["p_productsaledetail.id_productSale"]);
        $vproductSaleDetail->CreatedOn=trim($vrow["p_productsale.CreatedOn"]);
                $vproductSaleDetail->productSale->enterprise->idEnterprise=(int)($vrow["p_productsaledetail.id_enterprise"]);
                $vproductSaleDetail->productSale->enterprise->municipality->state->idState=(int)($vrow["c_productsaleenterprise.id_state"]);
                $vproductSaleDetail->productSale->enterprise->municipality->state->state=trim($vrow["c_productsaleenterprisestate.fldstate"]);
                $vproductSaleDetail->productSale->enterprise->municipality->idMunicipality=(int)($vrow["c_productsaleenterprise.id_municipality"]);
                $vproductSaleDetail->productSale->enterprise->municipality->municipality=trim($vrow["c_productsaleenterprisemunicipality.fldmunicipality"]);
                $vproductSaleDetail->productSale->enterprise->enterprise=trim($vrow["c_productsaleenterprise.fldenterprise"]);
                $vproductSaleDetail->productSale->enterprise->locality=trim($vrow["c_productsaleenterprise.fldlocality"]);
                $vproductSaleDetail->productSale->enterprise->street=trim($vrow["c_productsaleenterprise.fldstreet"]);
                $vproductSaleDetail->productSale->enterprise->number=trim($vrow["c_productsaleenterprise.fldnumber"]);
                $vproductSaleDetail->productSale->enterprise->phoneNumber=trim($vrow["c_productsaleenterprise.fldphoneNumber"]);
                $vproductSaleDetail->productSale->enterprise->movilNumber=trim($vrow["c_productsaleenterprise.fldmovilNumber"]);
                $vproductSaleDetail->productSale->enterprise->pageWeb=trim($vrow["c_productsaleenterprise.fldpageWeb"]);
                $vproductSaleDetail->productSale->customer->idCustomer=(int)($vrow["p_productsale.id_customer"]);
                $vproductSaleDetail->productSale->customer->enterprise->idEnterprise=(int)($vrow["p_productsale.id_enterprise"]);
                $vproductSaleDetail->productSale->customer->enterprise->municipality->state->idState=(int)($vrow["c_customerenterprise.id_state"]);
                $vproductSaleDetail->productSale->customer->enterprise->municipality->state->state=trim($vrow["c_customerenterprisestate.fldstate"]);
                $vproductSaleDetail->productSale->customer->enterprise->municipality->idMunicipality=(int)($vrow["c_customerenterprise.id_municipality"]);
                $vproductSaleDetail->productSale->customer->enterprise->municipality->municipality=trim($vrow["c_customerenterprisemunicipality.fldmunicipality"]);
                $vproductSaleDetail->productSale->customer->enterprise->enterprise=trim($vrow["c_customerenterprise.fldenterprise"]);
                $vproductSaleDetail->productSale->customer->enterprise->locality=trim($vrow["c_customerenterprise.fldlocality"]);
                $vproductSaleDetail->productSale->customer->enterprise->street=trim($vrow["c_customerenterprise.fldstreet"]);
                $vproductSaleDetail->productSale->customer->enterprise->number=trim($vrow["c_customerenterprise.fldnumber"]);
                $vproductSaleDetail->productSale->customer->enterprise->phoneNumber=trim($vrow["c_customerenterprise.fldphoneNumber"]);
                $vproductSaleDetail->productSale->customer->enterprise->movilNumber=trim($vrow["c_customerenterprise.fldmovilNumber"]);
                $vproductSaleDetail->productSale->customer->enterprise->pageWeb=trim($vrow["c_customerenterprise.fldpageWeb"]);
				$vproductSaleDetail->productSale->customer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vproductSaleDetail->productSale->customer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vproductSaleDetail->productSale->customer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vproductSaleDetail->productSale->customer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vproductSaleDetail->productSale->customer->locality=trim($vrow["c_customer.fldlocality"]);
                $vproductSaleDetail->productSale->customer->street=trim($vrow["c_customer.fldstreet"]);
                $vproductSaleDetail->productSale->customer->number=trim($vrow["c_customer.fldnumber"]);
                $vproductSaleDetail->productSale->customer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vproductSaleDetail->productSale->customer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vproductSaleDetail->productSale->customer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vproductSaleDetail->productSale->customer->email=trim($vrow["c_customer.fldemail"]);
                $vproductSaleDetail->productSale->customer->observation=trim($vrow["c_customer.fldobservation"]);
                $vproductSaleDetail->productSale->operationType->idOperationType=(int)($vrow["p_productsale.id_operationType"]);
                $vproductSaleDetail->productSale->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vproductSaleDetail->productSale->operationStatus->idOperationStatus=(int)($vrow["p_productsale.id_operationStatus"]);
                $vproductSaleDetail->productSale->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vproductSaleDetail->productSale->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldrecordDate"])));
                $vproductSaleDetail->productSale->productSaleDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldproductSaleDate"])));
                $vproductSaleDetail->productSale->canceled=(int)($vrow["p_productsale.fldcanceled"]);
				$vproductSaleDetail->product->idProduct=(int)($vrow["p_productsaledetail.id_product"]);
                $vproductSaleDetail->product->enterprise->idEnterprise=(int)($vrow["p_productsaledetail.id_enterprise"]);
                $vproductSaleDetail->product->enterprise->municipality->state->idState=(int)($vrow["c_productenterprise.id_state"]);
                $vproductSaleDetail->product->enterprise->municipality->state->state=trim($vrow["c_productenterprisestate.fldstate"]);
                $vproductSaleDetail->product->enterprise->municipality->idMunicipality=(int)($vrow["c_productenterprise.id_municipality"]);
                $vproductSaleDetail->product->enterprise->municipality->municipality=trim($vrow["c_productenterprisemunicipality.fldmunicipality"]);
                $vproductSaleDetail->product->enterprise->enterprise=trim($vrow["c_productenterprise.fldenterprise"]);
                $vproductSaleDetail->product->enterprise->locality=trim($vrow["c_productenterprise.fldlocality"]);
                $vproductSaleDetail->product->enterprise->street=trim($vrow["c_productenterprise.fldstreet"]);
                $vproductSaleDetail->product->enterprise->number=trim($vrow["c_productenterprise.fldnumber"]);
                $vproductSaleDetail->product->enterprise->phoneNumber=trim($vrow["c_productenterprise.fldphoneNumber"]);
                $vproductSaleDetail->product->enterprise->movilNumber=trim($vrow["c_productenterprise.fldmovilNumber"]);
                $vproductSaleDetail->product->enterprise->pageWeb=trim($vrow["c_productenterprise.fldpageWeb"]);
                $vproductSaleDetail->product->productType->idProductType=(int)($vrow["c_product.id_productType"]);
                $vproductSaleDetail->product->productType->enterprise=$vproductSaleDetail->productSale->enterprise;
                $vproductSaleDetail->product->productType->productType=trim($vrow["c_producttype.fldproductType"]);
                $vproductSaleDetail->product->key=trim($vrow["c_product.fldkey"]);
                $vproductSaleDetail->product->barCode=trim($vrow["c_product.fldbarCode"]);
                $vproductSaleDetail->product->name=trim($vrow["c_product.fldname"]);
                $vproductSaleDetail->product->description=trim($vrow["c_product.flddescription"]);
                $vproductSaleDetail->product->image=trim($vrow["c_product.fldimage"]);
                $vproductSaleDetail->product->purchasePrice=(float)($vrow["c_product.fldpurchasePrice"]);
                $vproductSaleDetail->product->salePrice=(float)($vrow["c_product.fldsalePrice"]);
                $vproductSaleDetail->product->weightInKg=(float)($vrow["c_product.fldweightInKg"]);
                $vproductSaleDetail->product->stock=(float)($vrow["c_product.fldstock"]);
                $vproductSaleDetail->product->observation=trim($vrow["c_product.fldobservation"]);
                $vproductSaleDetail->saleAmount=(float)($vrow["p_productsaledetail.fldsaleAmount"]);
                $vproductSaleDetail->saleWeightInKg=(float)($vrow["p_productsaledetail.fldsaleWeightInKg"]);
                $vproductSaleDetail->purchasePrice=(float)($vrow["p_productsaledetail.fldpurchasePrice"]);
                $vproductSaleDetail->realPrice=(float)($vrow["p_productsaledetail.fldrealPrice"]);
				//$vproductSaleDetail->salePrice=(float)($vrow["p_productsaledetail.fldsalePrice"]);
                $vproductSaleDetail->salePrice = number_format($vrow["p_productsaledetail.fldsalePrice"], 2, ".", ",");

                self::add($vflProductsSalesDetails, $vproductSaleDetail);
                unset($vrow, $vproductSaleDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

     public static function queryProductsGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql)
	  {
        try{
            $vsql ="SELECT c_product.fldname,c_product.fldkey, SUM(p_productsaledetail.fldsaleAmount) AS saleAmountTotal, SUM(p_productsaledetail.fldsaleAmount * p_productsaledetail.fldsalePrice) AS salePriceTotal, SUM(p_productsaledetail.fldsaleWeightInKg * p_productsaledetail.fldsaleAmount) AS saleWeightInKgTotal ";
            $vsql.="FROM p_productsaledetail ";
            $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
            $vsql.="INNER JOIN c_product ON p_productsaledetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_product= c_product.id_product ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY c_product.fldname ";

            self::clean($vflProductsSalesDetails);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSaleDetail= new clspFLProductSaleDetail();
                $vproductSaleDetail->product->name=trim($vrow["fldname"]);
                $vproductSaleDetail->saleAmount=(float)($vrow["saleAmountTotal"]);
                $vproductSaleDetail->salePrice=(float)($vrow["salePriceTotal"]);
                $vproductSaleDetail->product->key=trim($vrow["fldkey"]);
                //$vproductSaleDetail->saleWeightInKg=(float)($vrow["saleWeightInKgTotal"]);
                $vproductSaleDetail->saleWeightInKg=(float)($vrow["saleWeightInKgTotal"]);
                //$vproductSaleDetail->salePrice = number_format($vrow["p_productsaledetail.fldsalePrice"], 2, ".", ",");
                //$vproductSaleDetail->id = $vrow["p_productsale.id_customer"];

                self::add($vflProductsSalesDetails, $vproductSaleDetail);
                unset($vrow, $vproductSaleDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function querySalesGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_productsale.id_productSale, c_operationtype.fldoperationType, p_productsale.fldproductSaleDate";
            $vsql.=", p_productsale.id_customer, c_customer.id_personType ";
            $vsql.=", SUM(p_productsaledetail.fldsaleAmount * p_productsaledetail.fldsalePrice) AS salePriceTotal ";
            $vsql.="FROM p_productsaledetail ";
            $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
            $vsql.="INNER JOIN c_customer ON c_routecustomer.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND c_routecustomer.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_productsale.id_productSale ";

            self::clean($vflProductsSalesDetails);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSaleDetail= new clspFLProductSaleDetail();
                $vproductSaleDetail->productSale->idProductSale=trim($vrow["id_productSale"]);
                $vproductSaleDetail->productSale->operationType->operationType=trim($vrow["fldoperationType"]);
                $vproductSaleDetail->productSale->productSaleDate=date("m/d/Y", strtotime(trim($vrow["fldproductSaleDate"])));
                $vproductSaleDetail->productSale->customer->idCustomer=(int)($vrow["id_customer"]);
                $vproductSaleDetail->productSale->customer->personType->idPersonType=(int)($vrow["id_personType"]);
                $vproductSaleDetail->salePrice=(float)($vrow["salePriceTotal"]);

                self::add($vflProductsSalesDetails, $vproductSaleDetail);
                unset($vrow, $vproductSaleDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryCashAndCashiersGroupByNoRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_productsale.id_cash, c_cash.fldcash, p_productsale.id_user, c_user.fldfirstName, c_user.fldlastName, c_user.fldname";
            $vsql.=", SUM(p_productsaledetail.fldsaleAmount * p_productsaledetail.fldsalePrice) AS salePriceTotal ";
            $vsql.="FROM p_productsaledetail ";
            $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
            $vsql.="LEFT JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
            $vsql.="INNER JOIN c_cash ON p_productsale.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterpriseuser ON p_productsale.id_user=c_enterpriseuser.id_user ";
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_productsale.id_cash, p_productsale.id_user ";
            $vsql.="ORDER BY c_cash.fldcash, c_user.fldname, c_user.fldfirstName, c_user.fldlastName";

            self::clean($vflProductsSalesDetails);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSaleDetail= new clspFLProductSaleDetail();
                $vproductSaleDetail->productSale->cash->idCash=(int)($vrow["id_cash"]);
                $vproductSaleDetail->productSale->cash->cash=trim($vrow["fldcash"]);
                $vproductSaleDetail->productSale->user->idUser=trim($vrow["id_user"]);
                $vproductSaleDetail->productSale->user->firstName=trim($vrow["fldfirstName"]);
				$vproductSaleDetail->productSale->user->lastName=trim($vrow["fldlastName"]);
				$vproductSaleDetail->productSale->user->name=trim($vrow["fldname"]);
                $vproductSaleDetail->salePrice=(float)($vrow["salePriceTotal"]);

                //$vproductSaleDetail->product->key=trim($vrow["fldkey"]);
                self::add($vflProductsSalesDetails, $vproductSaleDetail);
                unset($vrow, $vproductSaleDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryDeliverersGroupByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT c_route.id_route, c_route.fldroute, c_routedealer.id_dealer, c_dealer.fldfirstName, c_dealer.fldlastName, c_dealer.fldname";
            $vsql.=", SUM(p_productsaledetail.fldsaleAmount * p_productsaledetail.fldsalePrice) AS salePriceTotal ";
            $vsql.="FROM p_productsaledetail ";
            $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
            $vsql.="INNER JOIN c_route ON c_routecustomer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routecustomer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_routedealer ON c_route.id_enterprise=c_routedealer.id_enterprise ";
            $vsql.="AND c_route.id_route=c_routedealer.id_route ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_enterprise=c_dealer.id_enterprise ";
            $vsql.="AND c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY c_route.id_route, c_routedealer.id_dealer ";
            $vsql.="ORDER BY c_route.fldroute, c_dealer.fldfirstName, c_dealer.fldlastName, c_dealer.fldname";

            self::clean($vflProductsSalesDetails);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSaleDetail= new clspFLProductSaleDetail();
                $vproductSaleDetail->productSale->idRoute=(int)($vrow["id_route"]);
                $vproductSaleDetail->productSale->route=trim($vrow["fldroute"]);
                $vproductSaleDetail->productSale->idDealer=(int)($vrow["id_dealer"]);
                $vproductSaleDetail->productSale->dealerFirstName=trim($vrow["fldfirstName"]);
				$vproductSaleDetail->productSale->dealerLastName=trim($vrow["fldlastName"]);
				$vproductSaleDetail->productSale->dealerName=trim($vrow["fldname"]);
                $vproductSaleDetail->salePrice=(float)($vrow["salePriceTotal"]);

                self::add($vflProductsSalesDetails, $vproductSaleDetail);
                unset($vrow, $vproductSaleDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryByRouteToDataBase($vflProductsSalesDetails, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT SUM(p_productsaledetail.fldsaleAmount * p_productsaledetail.fldsalePrice) AS salePriceTotal ";
            $vsql.="FROM p_productsaledetail ";
            $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
            $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
            $vsql.=$vfilter;

            self::clean($vflProductsSalesDetails);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSaleDetail= new clspFLProductSaleDetail();
                $vproductSaleDetail->salePrice=(float)($vrow["salePriceTotal"]);
                //$vproductSaleDetail->realPrice=(float)($vrow["fldsaleWeightInKg"]);

                self::add($vflProductsSalesDetails, $vproductSaleDetail);
                unset($vrow, $vproductSaleDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

   public static function queryByRouteToDataBaseCost($vflProductsSalesDetails, $vfilter, $vmySql)
  {
       try{
           $vsql ="SELECT SUM(p_productsaledetail.fldsaleAmount * p_productsaledetail.fldpurchasePrice) AS salePriceTotal ";
           $vsql.="FROM p_productsaledetail ";
           $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
           $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
           $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
           $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
           $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
           $vsql.=$vfilter;

           self::clean($vflProductsSalesDetails);

     $vmySql->executeSql($vsql);
           $vrowsTotal=$vmySql->getConsultedRowsNumber();
           for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
               $vrow=$vmySql->getData();
               $vproductSaleDetail= new clspFLProductSaleDetail();
               $vproductSaleDetail->realPrice=(float)($vrow["salePriceTotal"]);
               //$vproductSaleDetail->realPrice=(float)($vrow["salePriceTotal"]);

               self::add($vflProductsSalesDetails, $vproductSaleDetail);
               unset($vrow, $vproductSaleDetail);
           }
           if ( $vrowNumber<=0 ){
               return 0;
           }
     $vmySql->freeMemory();

     unset($vfilter, $vsql);
     return 1;
       }
       catch (Exception $vexception){
     throw new Exception($vexception->getMessage(), $vexception->getCode());
   }
  }




     public static function queryByRouteToDataBaseWeight($vflProductsSalesDetails, $vfilter, $vmySql)
  {
       try{
           $vsql ="SELECT SUM(p_productsaledetail.fldsaleWeightInKg) AS salePriceTotal ";
           $vsql.="FROM p_productsaledetail ";
           $vsql.="INNER JOIN p_productsale ON p_productsaledetail.id_enterprise=p_productsale.id_enterprise ";
           $vsql.="AND p_productsaledetail.id_productSale=p_productsale.id_productSale ";
           $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
           $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
           $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
           $vsql.=$vfilter;

           self::clean($vflProductsSalesDetails);

     $vmySql->executeSql($vsql);
           $vrowsTotal=$vmySql->getConsultedRowsNumber();
           for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
               $vrow=$vmySql->getData();
               $vproductSaleDetail= new clspFLProductSaleDetail();
               $vproductSaleDetail->realPrice=(float)($vrow["salePriceTotal"]);               //$vproductSaleDetail->realPrice=(float)($vrow["salePriceTotal"]);

               self::add($vflProductsSalesDetails, $vproductSaleDetail);
               unset($vrow, $vproductSaleDetail);
           }
           if ( $vrowNumber<=0 ){
               return 0;
           }
     $vmySql->freeMemory();

     unset($vfilter, $vsql);
     return 1;
       }
       catch (Exception $vexception){
     throw new Exception($vexception->getMessage(), $vexception->getCode());
   }
  }


	public static function add($vflProductsSalesDetails, $vproductSaleDetail)
	 {
        try{
            array_push($vflProductsSalesDetails->productsSalesDetails, $vproductSaleDetail);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function total($vflProductsSalesDetails)
	 {
        try{
            return count($vflProductsSalesDetails->productsSalesDetails);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function clean($vflProductsSalesDetails)
	 {
        try{
            $vflProductsSalesDetails->productsSalesDetails=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function totalSalePrice($vflProductsSalesDetails)
	 {
        try{
            $vtotalSalePrice=0;
            $vproductsSalesDetailsTotal=self::total($vflProductsSalesDetails);
            for($vi=0; $vi<$vproductsSalesDetailsTotal; $vi++){
                $vtotalSalePrice+=floatval($vflProductsSalesDetails->productsSalesDetails[$vi]->saleAmount *
                              $vflProductsSalesDetails->productsSalesDetails[$vi]->salePrice);
            }
            return floatval($vtotalSalePrice);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

     public static function totalPurchasePrice($vflProductsSalesDetails)
	 {
        try{
            $vtotalPurchasePrice=0;
            $vproductsSalesDetailsTotal=self::total($vflProductsSalesDetails);
            for($vi=0; $vi<$vproductsSalesDetailsTotal; $vi++){
                $vtotalPurchasePrice+=$vflProductsSalesDetails->productsSalesDetails[$vi]->saleAmount *
                              $vflProductsSalesDetails->productsSalesDetails[$vi]->purchasePrice;
            }
            return $vtotalPurchasePrice;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


    public function __destruct(){ }
 }

?>
