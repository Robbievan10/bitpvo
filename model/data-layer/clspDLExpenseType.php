<?php

class clspDLExpenseType
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflExpenseType, $vmySql)
	 {
		try{
            $vflExpenseType->idExpenseType=self::getIdFromDataBase($vflExpenseType, $vmySql);
            
            $vsql ="INSERT INTO c_expensetype(id_enterprise, id_expenseType, fldexpenseType) ";
			$vsql.="VALUES(" . $vflExpenseType->enterprise->idEnterprise;
            $vsql.=", " . $vflExpenseType->idExpenseType;
			$vsql.=", '" . $vflExpenseType->expenseType . "')";
                        
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflExpenseType, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_expensetype ";
            $vsql.="SET fldexpenseType='" . $vflExpenseType->expenseType . "' ";
            $vsql.="WHERE id_enterprise=" . $vflExpenseType->enterprise->idEnterprise . " ";
            $vsql.="AND id_expenseType=" . $vflExpenseType->idExpenseType;
			
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflExpenseType, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_expensetype ";
            $vsql.="WHERE id_enterprise=" . $vflExpenseType->enterprise->idEnterprise . " ";
            $vsql.="AND id_expenseType=" . $vflExpenseType->idExpenseType;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflExpenseType, $vmySql)
	 {
		try{
			$vsql ="SELECT c_expensetype.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_expensetype ";
            $vsql.="INNER JOIN c_enterprise ON c_expensetype.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE c_expensetype.id_enterprise=" . $vflExpenseType->enterprise->idEnterprise . " ";
            $vsql.="AND c_expensetype.id_expenseType=" . $vflExpenseType->idExpenseType;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflExpenseType->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflExpenseType->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflExpenseType->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflExpenseType->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflExpenseType->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflExpenseType->enterprise->locality=trim($vrow["fldlocality"]);
                $vflExpenseType->enterprise->street=trim($vrow["fldstreet"]);
                $vflExpenseType->enterprise->number=trim($vrow["fldnumber"]);
                $vflExpenseType->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflExpenseType->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflExpenseType->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflExpenseType->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflExpenseType->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflExpenseType->expenseType=trim($vrow["fldexpenseType"]);
                				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    private static function getIdFromDataBase($vflExpenseType, $vmySql)
	 {
		try{
            $vidExpenseType=1;
			$vsql ="SELECT MAX(id_expenseType) + 1 AS id_expenseTypeNew ";
			$vsql.="FROM c_expensetype ";
            $vsql.="WHERE id_enterprise=" . $vflExpenseType->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_expenseTypeNew"])>0) ){
                $vidExpenseType=(int)($vrow["id_expenseTypeNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidExpenseType;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
	public function __destruct(){ }
 }

?>