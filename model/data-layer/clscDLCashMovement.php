<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashMovement.php");


class clscDLCashMovement
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflCashMovements, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_cashmovement.*, c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate";
            $vsql.=", c_cashmovementstatus.fldcashMovementStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.="FROM p_cashmovement ";
            $vsql.="INNER JOIN c_cash ON p_cashmovement.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_cashmovement.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_cashmovementstatus ON p_cashmovement.id_cashMovementStatus=c_cashmovementstatus.id_cashMovementStatus ";
            $vsql.="INNER JOIN c_enterpriseuser ON p_cashmovement.id_user=c_enterpriseuser.id_user ";
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_cashmovement.fldrecordDate, p_cashmovement.fldopenTime, p_cashmovement.fldcloseTime";
            
            self::clean($vflCashMovements);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vcashMovement= new clspFLCashMovement();
                $vcashMovement->idCashMovement=(int)($vrow["p_cashmovement.id_cashMovement"]);
                $vcashMovement->cash->enterprise->idEnterprise=(int)($vrow["p_cashmovement.id_enterprise"]);
                $vcashMovement->cash->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vcashMovement->cash->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vcashMovement->cash->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vcashMovement->cash->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vcashMovement->cash->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vcashMovement->cash->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vcashMovement->cash->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vcashMovement->cash->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vcashMovement->cash->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vcashMovement->cash->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vcashMovement->cash->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vcashMovement->cash->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vcashMovement->cash->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vcashMovement->cash->idCash=(int)($vrow["p_cashmovement.id_cash"]);
                $vcashMovement->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vcashMovement->cashMovementStatus->idCashMovementStatus=(int)($vrow["p_cashmovement.id_cashMovementStatus"]);
                $vcashMovement->cashMovementStatus->cashMovementStatus=trim($vrow["c_cashmovementstatus.fldcashMovementStatus"]);
                $vcashMovement->user->idUser=trim($vrow["p_cashmovement.id_user"]);
                $vcashMovement->user->enterprise=$vcashMovement->cash->enterprise;
                $vcashMovement->user->userType->idUserType=(int)($vrow["c_user.id_userType"]);
				$vcashMovement->user->userType->userType=trim($vrow["c_usertype.flduserType"]);
				$vcashMovement->user->userStatus->idUserStatus=(int)($vrow["c_user.id_userStatus"]);
				$vcashMovement->user->userStatus->userStatus=trim($vrow["c_userstatus.flduserStatus"]);
				$vcashMovement->user->firstName=trim($vrow["c_user.fldfirstName"]);
				$vcashMovement->user->lastName=trim($vrow["c_user.fldlastName"]);
				$vcashMovement->user->name=trim($vrow["c_user.fldname"]);
                $vcashMovement->recordDate=date("m/d/Y", strtotime(trim($vrow["p_cashmovement.fldrecordDate"])));
                $vcashMovement->openTime=date("h:i:s a", strtotime(trim($vrow["p_cashmovement.fldopenTime"])));
                $vcashMovement->openAmount=(float)($vrow["p_cashmovement.fldopenAmount"]);
                $vcashMovement->closeTime=date("h:i:s a", strtotime(trim($vrow["p_cashmovement.fldcloseTime"])));
                $vcashMovement->closeAmount=(float)($vrow["p_cashmovement.fldcloseAmount"]);
                
                self::add($vflCashMovements, $vcashMovement);
                unset($vrow, $vcashMovement);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function queryMovementsGroupByCashAndCashierToDataBase($vflCashMovements, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_cashmovement.id_cash, c_cash.fldcash, p_cashmovement.id_user, c_user.fldfirstName, c_user.fldlastName, c_user.fldname, p_cashmovement.CreatedOn, p_cashmovement.fldrecordDate";
            $vsql.=", SUM(p_cashmovement.fldopenAmount) AS openAmountTotal ";
            $vsql.="FROM p_cashmovement ";
            $vsql.="INNER JOIN c_cash ON p_cashmovement.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterpriseuser ON p_cashmovement.id_user=c_enterpriseuser.id_user ";
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_cashmovement.id_cash, p_cashmovement.id_user ";
            $vsql.="ORDER BY c_cash.fldcash, c_user.fldname, c_user.fldfirstName, c_user.fldlastName";
            
            self::clean($vflCashMovements);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcashMovement= new clspFLCashMovement();
                $vcashMovement->cash->idCash=(int)($vrow["id_cash"]);
                $vcashMovement->cash->cash=trim($vrow["fldcash"]);
                $vcashMovement->user->idUser=trim($vrow["id_user"]);
                $vcashMovement->user->firstName=trim($vrow["fldfirstName"]);
				$vcashMovement->user->lastName=trim($vrow["fldlastName"]);
				$vcashMovement->user->name=trim($vrow["fldname"]);
                $vcashMovement->openAmount=(float)($vrow["openAmountTotal"]);
                $vcashMovement->CreatedOn=trim($vrow["CreatedOn"]);
                $vcashMovement->recordDate=trim($vrow["fldrecordDate"]);
                self::add($vflCashMovements, $vcashMovement);
                unset($vrow, $vcashMovement);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	private static function add($vflCashMovements, $vcashMovement)
	 {
        try{
            array_push($vflCashMovements->cashMovements, $vcashMovement);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflCashMovements)
	 {
        try{
            return count($vflCashMovements->cashMovements);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflCashMovements)
	 {
        try{
            $vflCashMovements->cashMovements=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>