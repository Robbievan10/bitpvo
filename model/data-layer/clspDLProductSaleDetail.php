<?php

class clspDLProductSaleDetail
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductSaleDetail, $vmySql)
	 {
		try{
            $vsql ="INSERT INTO p_productsaledetail(id_enterprise, id_productSale, id_product, fldsaleAmount, fldsaleWeightInKg, fldpurchasePrice";
            $vsql.=", fldrealPrice, fldsalePrice) ";
			$vsql.="VALUES(" . $vflProductSaleDetail->productSale->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductSaleDetail->productSale->idProductSale . "'";
            $vsql.=", " . $vflProductSaleDetail->product->idProduct;
            $vsql.=", " . $vflProductSaleDetail->saleAmount;
            $vsql.=", " . $vflProductSaleDetail->saleWeightInKg;
            $vsql.=", " . $vflProductSaleDetail->purchasePrice;
            $vsql.=", " . $vflProductSaleDetail->realPrice;
            $vsql.=", " . $vflProductSaleDetail->salePrice. ")";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
    public static function totalSalePrice($vflProductSaleDetail)
	 {
        try{
            return $vflProductSaleDetail->saleAmount*$vflProductSaleDetail->salePrice;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>