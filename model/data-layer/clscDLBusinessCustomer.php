<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBusinessCustomer.php");


class clscDLBusinessCustomer
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflBusinessCustomers, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_businesscustomer.*, c_customer.*, c_persontype.fldpersonType, c_businesscustomermunicipality.fldmunicipality";
            $vsql.=", c_businesscustomerstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_businesscustomer ";
            $vsql.="INNER JOIN c_customer ON c_businesscustomer.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND c_businesscustomer.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise ON c_customer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_businesscustomermunicipality ON c_customer.id_state=c_businesscustomermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_businesscustomermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_businesscustomerstate ON c_businesscustomermunicipality.id_state=c_businesscustomerstate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_businesscustomer.id_customer";
            
            self::clean($vflBusinessCustomers);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vbusinessCustomer= new clspFLBusinessCustomer();
                $vbusinessCustomer->businessName=trim($vrow["c_businesscustomer.fldbusinessName"]);
                $vbusinessCustomer->idCustomer=(int)($vrow["c_customer.id_customer"]);
                $vbusinessCustomer->enterprise->idEnterprise=(int)($vrow["c_customer.id_enterprise"]);
                $vbusinessCustomer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vbusinessCustomer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vbusinessCustomer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vbusinessCustomer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vbusinessCustomer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vbusinessCustomer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vbusinessCustomer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vbusinessCustomer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vbusinessCustomer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vbusinessCustomer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vbusinessCustomer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vbusinessCustomer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vbusinessCustomer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vbusinessCustomer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
                $vbusinessCustomer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vbusinessCustomer->municipality->state->idState=(int)($vrow["c_customer.id_state"]);
                $vbusinessCustomer->municipality->state->state=trim($vrow["c_businesscustomerstate.fldstate"]);
                $vbusinessCustomer->municipality->idMunicipality=(int)($vrow["c_customer.id_municipality"]);
                $vbusinessCustomer->municipality->municipality=trim($vrow["c_businesscustomermunicipality.fldmunicipality"]);
                $vbusinessCustomer->key=trim($vrow["c_customer.fldkey"]);
                $vbusinessCustomer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vbusinessCustomer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vbusinessCustomer->locality=trim($vrow["c_customer.fldlocality"]);
                $vbusinessCustomer->street=trim($vrow["c_customer.fldstreet"]);
                $vbusinessCustomer->number=trim($vrow["c_customer.fldnumber"]);
                $vbusinessCustomer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vbusinessCustomer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vbusinessCustomer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vbusinessCustomer->email=trim($vrow["c_customer.fldemail"]);
                $vbusinessCustomer->observation=trim($vrow["c_customer.fldobservation"]);
                
                self::add($vflBusinessCustomers, $vbusinessCustomer);
                unset($vrow, $vbusinessCustomer);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflBusinessCustomers, $vbusinessCustomer)
	 {
        try{
            array_push($vflBusinessCustomers->businessCustomers, $vbusinessCustomer);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflBusinessCustomers)
	 {
        try{
            return count($vflBusinessCustomers->businessCustomers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflBusinessCustomers)
	 {
        try{
            $vflBusinessCustomers->businessCustomers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>