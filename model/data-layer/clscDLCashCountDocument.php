<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashCountDocument.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCashCountDocument.php");


class clscDLCashCountDocument
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflCashCountDocumentList, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_cashcountdocument.*, p_cashcount.*, c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_userauditor.*";
            $vsql.=", c_userauditortype.flduserType, c_userauditorstatus.flduserStatus, c_usercashier.*, c_usercashiertype.flduserType";
            $vsql.=", c_usercashierstatus.flduserStatus, c_documenttype.flddocumentType ";
            $vsql.="FROM p_cashcountdocument ";
            $vsql.="INNER JOIN p_cashcount ON p_cashcountdocument.id_enterprise=p_cashcount.id_enterprise ";
            $vsql.="AND p_cashcountdocument.id_cash=p_cashcount.id_cash ";
            $vsql.="AND p_cashcountdocument.id_cashCount=p_cashcount.id_cashCount ";
            $vsql.="INNER JOIN c_cash ON p_cashcount.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_cashcount.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseuserauditor ON p_cashcount.id_userAuditor=c_enterpriseuserauditor.id_user ";
            $vsql.="INNER JOIN c_user AS c_userauditor ON c_enterpriseuserauditor.id_user=c_userauditor.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_userauditortype ON c_userauditor.id_userType=c_userauditortype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_userauditorstatus ON c_userauditor.id_userStatus=c_userauditorstatus.id_userStatus ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseusercashier ON p_cashcount.id_userCashier=c_enterpriseusercashier.id_user ";
            $vsql.="INNER JOIN c_user AS c_usercashier ON c_enterpriseusercashier.id_user=c_usercashier.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_usercashiertype ON c_usercashier.id_userType=c_usercashiertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_usercashierstatus ON c_usercashier.id_userStatus=c_usercashierstatus.id_userStatus ";
            $vsql.="INNER JOIN c_documenttype ON p_cashcountdocument.id_documentType=c_documenttype.id_documentType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_documenttype.id_documentType, p_cashcountdocument.fldconcept";
            
            self::clean($vflCashCountDocumentList);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vcashCountDocument= new clspFLCashCountDocument();
                $vcashCountDocument->idCashCountDocument=(int)($vrow["p_cashcountdocument.id_cashCountDocument"]);
                $vcashCountDocument->cashCount->idCashCount=(int)($vrow["p_cashcountdocument.id_cashCount"]);
                $vcashCountDocument->cashCount->cash->enterprise->idEnterprise=(int)($vrow["p_cashcountdocument.id_enterprise"]);
                $vcashCountDocument->cashCount->cash->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vcashCountDocument->cashCount->cash->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vcashCountDocument->cashCount->cash->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vcashCountDocument->cashCount->cash->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vcashCountDocument->cashCount->cash->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vcashCountDocument->cashCount->cash->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vcashCountDocument->cashCount->cash->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vcashCountDocument->cashCount->cash->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vcashCountDocument->cashCount->cash->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vcashCountDocument->cashCount->cash->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vcashCountDocument->cashCount->cash->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vcashCountDocument->cashCount->cash->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vcashCountDocument->cashCount->cash->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vcashCountDocument->cashCount->cash->idCash=(int)($vrow["p_cashcountdocument.id_cash"]);
                $vcashCountDocument->cashCount->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vcashCountDocument->cashCount->userAuditor->idUser=trim($vrow["p_cashcount.id_userAuditor"]);
                $vcashCountDocument->cashCount->userAuditor->enterprise=$vcashCountDocument->cashCount->cash->enterprise;
                $vcashCountDocument->cashCount->userAuditor->userType->idUserType=(int)($vrow["c_userauditor.id_userType"]);
				$vcashCountDocument->cashCount->userAuditor->userType->userType=trim($vrow["c_userauditortype.flduserType"]);
				$vcashCountDocument->cashCount->userAuditor->userStatus->idUserStatus=(int)($vrow["c_userauditor.id_userStatus"]);
				$vcashCountDocument->cashCount->userAuditor->userStatus->userStatus=trim($vrow["c_userauditorstatus.flduserStatus"]);
				$vcashCountDocument->cashCount->userAuditor->firstName=trim($vrow["c_userauditor.fldfirstName"]);
				$vcashCountDocument->cashCount->userAuditor->lastName=trim($vrow["c_userauditor.fldlastName"]);
				$vcashCountDocument->cashCount->userAuditor->name=trim($vrow["c_userauditor.fldname"]);
                $vcashCountDocument->cashCount->userCashier->idUser=trim($vrow["p_cashcount.id_userCashier"]);
                $vcashCountDocument->cashCount->userCashier->enterprise=$vcashCountDocument->cashCount->cash->enterprise;
                $vcashCountDocument->cashCount->userCashier->userType->idUserType=(int)($vrow["c_usercashier.id_userType"]);
				$vcashCountDocument->cashCount->userCashier->userType->userType=trim($vrow["c_usercashiertype.flduserType"]);
				$vcashCountDocument->cashCount->userCashier->userStatus->idUserStatus=(int)($vrow["c_usercashier.id_userStatus"]);
				$vcashCountDocument->cashCount->userCashier->userStatus->userStatus=trim($vrow["c_usercashierstatus.flduserStatus"]);
				$vcashCountDocument->cashCount->userCashier->firstName=trim($vrow["c_usercashier.fldfirstName"]);
				$vcashCountDocument->cashCount->userCashier->lastName=trim($vrow["c_usercashier.fldlastName"]);
				$vcashCountDocument->cashCount->userCashier->name=trim($vrow["c_usercashier.fldname"]);
                $vcashCountDocument->cashCount->recordDate=date("m/d/Y", strtotime(trim($vrow["p_cashcount.fldrecordDate"])));
                $vcashCountDocument->cashCount->observation=trim($vrow["p_cashcount.fldobservation"]);
                $vcashCountDocument->documentType->idDocumentType=(int)($vrow["p_cashcountdocument.id_documentType"]);
                $vcashCountDocument->documentType->documentType=trim($vrow["c_documenttype.flddocumentType"]);
                $vcashCountDocument->concept=trim($vrow["p_cashcountdocument.fldconcept"]);
                $vcashCountDocument->value=(float)($vrow["p_cashcountdocument.fldvalue"]);
                                
                self::add($vflCashCountDocumentList, $vcashCountDocument);
                unset($vrow, $vcashCountDocument);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function queryDocumentGroupByValue($vflCashCountDocumentList, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT c_documenttype.flddocumentType, COUNT(p_cashcountdocument.id_cashCountDocument) AS total";
            $vsql.=", SUM(p_cashcountdocument.fldvalue) AS amountTotal ";
            $vsql.="FROM p_cashcountdocument ";
            $vsql.="INNER JOIN p_cashcount ON p_cashcountdocument.id_enterprise=p_cashcount.id_enterprise ";
            $vsql.="AND p_cashcountdocument.id_cash=p_cashcount.id_cash ";
            $vsql.="AND p_cashcountdocument.id_cashCount=p_cashcount.id_cashCount ";
            $vsql.="INNER JOIN c_documenttype ON p_cashcountdocument.id_documentType=c_documenttype.id_documentType ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY c_documenttype.flddocumentType ";
            $vsql.="ORDER BY c_documenttype.flddocumentType";
            
            self::clean($vflCashCountDocumentList);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcashCountDocument= new clspFLCashCountDocument();
                $vcashCountDocument->documentType->documentType=trim($vrow["flddocumentType"]);
                $vcashCountDocument->total=(float)($vrow["total"]);
                $vcashCountDocument->value=(float)($vrow["amountTotal"]);
                
                self::add($vflCashCountDocumentList, $vcashCountDocument);
                unset($vrow, $vcashCountDocument);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function addToDataBase($vflCashCountDocumentList, $vflCashCount, $vmySql)
	 {
		try{
			for($vi=0; $vi<self::total($vflCashCountDocumentList); $vi++){
                $vflCashCountDocument= new clspFLCashCountDocument();
                $vflCashCountDocument=$vflCashCountDocumentList->cashCountDocument[$vi];
                $vflCashCountDocument->cashCount->idCashCount=$vflCashCount->idCashCount;
                if ( clspDLCashCountDocument::addToDataBase($vflCashCountDocument, $vmySql)==0 ){
                    return 0;
                }
                
                unset($vflCashCountDocument);
			}
            
			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflCashCountDocumentList, $vmySql)
	 {
		try{
			for($vi=0; $vi<self::total($vflCashCountDocumentList); $vi++){
                $vflCashCountDocument= new clspFLCashCountDocument();
                $vflCashCountDocument=$vflCashCountDocumentList->cashCountDocument[$vi];
                if ( clspDLCashCountDocument::deleteInDataBase($vflCashCountDocument, $vmySql)==0 ){
                    return 0;
                }
                
                unset($vflCashCountDocument);
			}
            
			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function add($vflCashCountDocumentList, $vflCashCountDocument)
	 {
        try{
            array_push($vflCashCountDocumentList->cashCountDocument, $vflCashCountDocument);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflCashCountDocumentList)
	 {
        try{
            return count($vflCashCountDocumentList->cashCountDocument);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflCashCountDocumentList)
	 {
        try{
            $vflCashCountDocumentList->cashCountDocument=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>