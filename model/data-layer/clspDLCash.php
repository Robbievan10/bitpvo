<?php

class clspDLCash
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflCash, $vmySql)
	 {
		try{
            $vflCash->idCash=self::getIdFromDataBase($vflCash, $vmySql);
            
            $vsql ="INSERT INTO c_cash(id_enterprise, id_cash, fldcash) ";
			$vsql.="VALUES(" . $vflCash->enterprise->idEnterprise;
            $vsql.=", " . $vflCash->idCash;
			$vsql.=", '" . $vflCash->cash . "')";
                        
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflCash, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_cash ";
            $vsql.="SET fldcash='" . $vflCash->cash . "' ";
            $vsql.="WHERE id_enterprise=" . $vflCash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCash->idCash;
			
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflCash, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_cash ";
            $vsql.="WHERE id_enterprise=" . $vflCash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCash->idCash;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function queryToDataBase($vflCash, $vmySql)
	 {
		try{
			$vsql ="SELECT c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE c_cash.id_enterprise=" . $vflCash->enterprise->idEnterprise . " ";
            $vsql.="AND c_cash.id_cash=" . $vflCash->idCash;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflCash->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflCash->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflCash->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflCash->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflCash->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflCash->enterprise->locality=trim($vrow["fldlocality"]);
                $vflCash->enterprise->street=trim($vrow["fldstreet"]);
                $vflCash->enterprise->number=trim($vrow["fldnumber"]);
                $vflCash->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflCash->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflCash->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflCash->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflCash->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflCash->cash=trim($vrow["fldcash"]);
                                				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    private static function getIdFromDataBase($vflCash, $vmySql)
	 {
		try{
            $vidCash=1;
			$vsql ="SELECT MAX(id_cash) + 1 AS id_cashNew ";
			$vsql.="FROM c_cash ";
            $vsql.="WHERE id_enterprise=" . $vflCash->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_cashNew"])>0) ){
                $vidCash=(int)($vrow["id_cashNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidCash;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
	public function __destruct(){ }
 }

?>