<?php

class clspDLExpenseDetail
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflExpenseDetail, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflExpenseDetail->idExpenseDetail=self::getIdFromDataBase($vflExpenseDetail, $vmySql);
            
            $vsql ="INSERT INTO p_expense(id_enterprise, id_expense, id_expenseDetail, id_route, fldrecordDate, fldamount, fldobservation) ";
			$vsql.="VALUES(" . $vflExpenseDetail->expense->enterprise->idEnterprise;
            $vsql.=", " . $vflExpenseDetail->expense->idExpense;
            $vsql.=", " . $vflExpenseDetail->idExpenseDetail;
            $vsql.=", " . $vflExpenseDetail->route->idRoute;
			$vsql.=", '" . date("Y-m-d", strtotime($vflExpenseDetail->recordDate)) . "'";
            $vsql.=", " . $vflExpenseDetail->amount;
            $vsql.=", '" . $vflExpenseDetail->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflExpenseDetail, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_expense ";
            $vsql.="SET id_route=" . $vflExpenseDetail->route->idRoute;
            $vsql.=", fldrecordDate='" . date("Y-m-d", strtotime($vflExpenseDetail->recordDate)) . "'";
            $vsql.=", fldamount=" . $vflExpenseDetail->amount;
            $vsql.=", fldobservation='" . $vflExpenseDetail->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflExpenseDetail->expense->enterprise->idEnterprise . " ";
            $vsql.="AND id_expense=" . $vflExpenseDetail->expense->idExpense . " ";
            $vsql.="AND id_expenseDetail=" . $vflExpenseDetail->idExpenseDetail;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflExpenseDetail, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_expense ";
            $vsql.="WHERE id_enterprise=" . $vflExpenseDetail->expense->enterprise->idEnterprise . " ";
            $vsql.="AND id_expense=" . $vflExpenseDetail->expense->idExpense . " ";
            $vsql.="AND id_expenseDetail=" . $vflExpenseDetail->idExpenseDetail;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflExpenseDetail, $vmySql)
	 {
		try{
			$vsql ="SELECT p_expense.*, c_expense.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_expensetype.fldexpenseType";
            $vsql.=", c_route.* ";
            $vsql.="FROM p_expense ";
			$vsql.="INNER JOIN c_expense ON p_expense.id_enterprise=c_expense.id_enterprise ";
            $vsql.="AND p_expense.id_expense=c_expense.id_expense ";
            $vsql.="INNER JOIN c_enterprise ON c_expense.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_expensetype ON c_expense.id_enterprise=c_expensetype.id_enterprise ";
            $vsql.="AND c_expense.id_expenseType=c_expensetype.id_expenseType ";
            $vsql.="INNER JOIN c_enterprise AS c_expensetypeenterprise ON c_expensetype.id_enterprise=c_expensetypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_expensetypemunicipality ON c_expensetypeenterprise.id_state=c_expensetypemunicipality.id_state ";
            $vsql.="AND c_expensetypeenterprise.id_municipality=c_expensetypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_expensetypestate ON c_expensetypemunicipality.id_state=c_expensetypestate.id_state ";
            $vsql.="INNER JOIN c_route ON p_expense.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND p_expense.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routemunicipality ON c_routeenterprise.id_state=c_routemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routestate ON c_routemunicipality.id_state=c_routestate.id_state ";
            $vsql.="WHERE p_expense.id_enterprise=" . $vflExpenseDetail->expense->enterprise->idEnterprise . " ";
            $vsql.="AND p_expense.id_expense=" . $vflExpenseDetail->expense->idExpense . " ";
            $vsql.="AND p_expense.id_expenseDetail=" . $vflExpenseDetail->idExpenseDetail;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflExpenseDetail->expense->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflExpenseDetail->expense->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflExpenseDetail->expense->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflExpenseDetail->expense->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflExpenseDetail->expense->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflExpenseDetail->expense->enterprise->locality=trim($vrow["fldlocality"]);
                $vflExpenseDetail->expense->enterprise->street=trim($vrow["fldstreet"]);
                $vflExpenseDetail->expense->enterprise->number=trim($vrow["fldnumber"]);
                $vflExpenseDetail->expense->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflExpenseDetail->expense->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflExpenseDetail->expense->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflExpenseDetail->expense->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflExpenseDetail->expense->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflExpenseDetail->expense->expenseType->idExpenseType=(int)($vrow["id_expenseType"]);
                $vflExpenseDetail->expense->expenseType->enterprise=$vflExpenseDetail->expense->enterprise;
                $vflExpenseDetail->expense->expenseType->expenseType=trim($vrow["fldexpenseType"]);
                $vflExpenseDetail->expense->expense=trim($vrow["fldexpense"]);
                $vflExpenseDetail->expense->description=trim($vrow["flddescription"]);
                $vflExpenseDetail->route->enterprise=$vflExpenseDetail->expense->enterprise;
                $vflExpenseDetail->route->idRoute=(int)($vrow["id_route"]);
                $vflExpenseDetail->route->route=trim($vrow["fldroute"]);
                $vflExpenseDetail->route->description=trim($vrow["flddescription"]);
                $vflExpenseDetail->route->observation=trim($vrow["fldobservation"]);
                $vflExpenseDetail->recordDate=date("m/d/Y", strtotime(trim($vrow["fldrecordDate"])));
                $vflExpenseDetail->amount=(float)($vrow["fldamount"]);
                $vflExpenseDetail->observation=trim($vrow["fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflExpenseDetail, $vmySql)
	 {
		try{
            $vidExpenseDetail=1;
			$vsql ="SELECT MAX(id_expenseDetail) + 1 AS id_expenseDetailNew ";
			$vsql.="FROM p_expense ";
            $vsql.="WHERE id_enterprise=" . $vflExpenseDetail->expense->enterprise->idEnterprise . " ";
            $vsql.="AND id_expense=" . $vflExpenseDetail->expense->idExpense;
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_expenseDetailNew"])>0) ){
                $vidExpenseDetail=(int)($vrow["id_expenseDetailNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidExpenseDetail;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>