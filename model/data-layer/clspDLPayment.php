<?php

class clspDLPayment
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflPayment, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflPayment->idPayment=self::getIdFromDataBase($vflPayment, $vmySql);
            
            $vsql ="INSERT INTO p_payment(id_enterprise, id_payment, id_paymentType, fldrecordDate, fldpaymentDate, fldamount) ";
			$vsql.="VALUES(" . $vflPayment->enterprise->idEnterprise;
            $vsql.=", " . $vflPayment->idPayment;
            $vsql.=", " . $vflPayment->paymentType->idPaymentType;
            $vsql.=", '" . date("Y-m-d", strtotime($vflPayment->recordDate)) . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflPayment->paymentDate)) . "'";
            $vsql.=", " . $vflPayment->amount . ")";
            
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflPayment, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_payment ";
            $vsql.="SET fldpaymentDate='" . date("Y-m-d", strtotime($vflPayment->paymentDate)) . "'";
			$vsql.=", fldamount=" . $vflPayment->amount . " ";
			$vsql.="WHERE id_enterprise=" . $vflPayment->enterprise->idEnterprise . " ";
            $vsql.="AND id_payment=" . $vflPayment->idPayment;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflPayment, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_payment ";
            $vsql.="WHERE id_enterprise=" . $vflPayment->enterprise->idEnterprise . " ";
            $vsql.="AND id_payment=" . $vflPayment->idPayment;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    private static function getIdFromDataBase($vflPayment, $vmySql)
	 {
		try{
            $vidPayment=1;
			$vsql ="SELECT MAX(id_payment) + 1 AS id_paymentNew ";
			$vsql.="FROM p_payment ";
            $vsql.="WHERE id_enterprise=" . $vflPayment->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_paymentNew"])>0) ){
                $vidPayment=(int)($vrow["id_paymentNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidPayment;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>