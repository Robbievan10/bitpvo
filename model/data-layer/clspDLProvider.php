<?php

class clspDLProvider
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProvider, $vmySql)
	 {
		try{
            $vflProvider->idProvider=self::getIdFromDataBase($vflProvider, $vmySql);
            
            $vsql ="INSERT INTO c_provider(id_enterprise, id_provider, id_personType, id_state, id_municipality, fldkey, fldrfc, fldhomoclave";
			$vsql.=", fldlocality, fldstreet , fldnumber, fldpostCode, fldphoneNumber, fldmovilNumber, fldemail, fldpageWeb, fldobservation) ";
			$vsql.="VALUES(" . $vflProvider->enterprise->idEnterprise;
            $vsql.=", " . $vflProvider->idProvider;
			$vsql.=", " . $vflProvider->personType->idPersonType;
			$vsql.=", " . $vflProvider->municipality->state->idState;
			$vsql.=", " . $vflProvider->municipality->idMunicipality;
            $vsql.=", '" . $vflProvider->key . "'";
			$vsql.=", '" . $vflProvider->rfc . "'";
			$vsql.=", '" . $vflProvider->homoclave . "'";
			$vsql.=", '" . $vflProvider->locality . "'";
			$vsql.=", '" . $vflProvider->street . "'";
			$vsql.=", '" . $vflProvider->number . "'";
			$vsql.=", '" . $vflProvider->postCode . "'";
            $vsql.=", '" . $vflProvider->phoneNumber . "'";
            $vsql.=", '" . $vflProvider->movilNumber . "'";
            $vsql.=", '" . $vflProvider->email . "'";
            $vsql.=", '" . $vflProvider->pageWeb . "'";
            $vsql.=", '" . $vflProvider->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProvider, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_provider ";
            $vsql.="SET id_state=" . $vflProvider->municipality->state->idState;
			$vsql.=", id_municipality=" . $vflProvider->municipality->idMunicipality;
            $vsql.=", fldkey='" . $vflProvider->key . "'";
			$vsql.=", fldrfc='" . $vflProvider->rfc . "'";
			$vsql.=", fldhomoclave='" . $vflProvider->homoclave . "'";
			$vsql.=", fldlocality='" . $vflProvider->locality . "'";
			$vsql.=", fldstreet='" . $vflProvider->street . "'";
			$vsql.=", fldnumber='" . $vflProvider->number . "'";
			$vsql.=", fldpostCode='" . $vflProvider->postCode . "'";
			$vsql.=", fldphoneNumber='" . $vflProvider->phoneNumber. "'";
            $vsql.=", fldmovilNumber='" . $vflProvider->movilNumber. "'";
            $vsql.=", fldemail='" . $vflProvider->email . "'";
            $vsql.=", fldpageWeb='" . $vflProvider->pageWeb . "'";
            $vsql.=", fldobservation='" . $vflProvider->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflProvider->enterprise->idEnterprise . " ";
            $vsql.="AND id_provider=" . $vflProvider->idProvider;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProvider, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_provider ";
            $vsql.="WHERE id_enterprise=" . $vflProvider->enterprise->idEnterprise . " ";
            $vsql.="AND id_provider=" . $vflProvider->idProvider;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProvider, $vmySql)
	 {
		try{
			$vsql ="SELECT c_provider.*, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_providermunicipality.fldmunicipality, c_providerstate.fldstate ";
			$vsql.="FROM c_provider ";
            $vsql.="INNER JOIN c_enterprise ON c_provider.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_providermunicipality ON c_provider.id_state=c_providermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_providermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_providerstate ON c_providermunicipality.id_state=c_providerstate.id_state ";
            $vsql.="WHERE c_provider.id_enterprise=" . $vflProvider->enterprise->idEnterprise . " ";
            $vsql.="AND c_provider.id_provider=" . $vflProvider->idProvider;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProvider->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflProvider->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflProvider->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflProvider->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflProvider->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflProvider->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflProvider->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflProvider->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflProvider->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflProvider->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflProvider->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflProvider->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflProvider->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
				$vflProvider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
				$vflProvider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflProvider->municipality->state->idState=(int)($vrow["c_provider.id_state"]);
                $vflProvider->municipality->state->state=trim($vrow["c_providerstate.fldstate"]);
                $vflProvider->municipality->idMunicipality=(int)($vrow["c_provider.id_municipality"]);
                $vflProvider->municipality->municipality=trim($vrow["c_providermunicipality.fldmunicipality"]);
                $vflProvider->key=trim($vrow["c_provider.fldkey"]);
                $vflProvider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vflProvider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vflProvider->locality=trim($vrow["c_provider.fldlocality"]);
                $vflProvider->street=trim($vrow["c_provider.fldstreet"]);
                $vflProvider->number=trim($vrow["c_provider.fldnumber"]);
                $vflProvider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vflProvider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vflProvider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vflProvider->email=trim($vrow["c_provider.fldemail"]);
                $vflProvider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vflProvider->observation=trim($vrow["c_provider.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByKey($vflProvider, $vmySql)
	 {
		try{
			$vsql ="SELECT c_provider.*, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_providermunicipality.fldmunicipality, c_providerstate.fldstate ";
			$vsql.="FROM c_provider ";
            $vsql.="INNER JOIN c_enterprise ON c_provider.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_providermunicipality ON c_provider.id_state=c_providermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_providermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_providerstate ON c_providermunicipality.id_state=c_providerstate.id_state ";
            $vsql.="WHERE c_provider.id_enterprise=" . $vflProvider->enterprise->idEnterprise . " ";
            $vsql.="AND c_provider.fldkey='" . $vflProvider->key . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProvider->idProvider=(int)($vrow["c_provider.id_provider"]);
                $vflProvider->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflProvider->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflProvider->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflProvider->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflProvider->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflProvider->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflProvider->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflProvider->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflProvider->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflProvider->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflProvider->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflProvider->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflProvider->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
				$vflProvider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
				$vflProvider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflProvider->municipality->state->idState=(int)($vrow["c_provider.id_state"]);
                $vflProvider->municipality->state->state=trim($vrow["c_providerstate.fldstate"]);
                $vflProvider->municipality->idMunicipality=(int)($vrow["c_provider.id_municipality"]);
                $vflProvider->municipality->municipality=trim($vrow["c_providermunicipality.fldmunicipality"]);
                $vflProvider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vflProvider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vflProvider->locality=trim($vrow["c_provider.fldlocality"]);
                $vflProvider->street=trim($vrow["c_provider.fldstreet"]);
                $vflProvider->number=trim($vrow["c_provider.fldnumber"]);
                $vflProvider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vflProvider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vflProvider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vflProvider->email=trim($vrow["c_provider.fldemail"]);
                $vflProvider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vflProvider->observation=trim($vrow["c_provider.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProvider, $vmySql)
	 {
		try{
            $vidProvider=1;
			$vsql ="SELECT MAX(id_provider) + 1 AS id_providerNew ";
			$vsql.="FROM c_provider ";
            $vsql.="WHERE id_enterprise=" . $vflProvider->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_providerNew"])>0) ){
                $vidProvider=(int)($vrow["id_providerNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProvider;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>