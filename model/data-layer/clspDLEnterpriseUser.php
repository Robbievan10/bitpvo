<?php

class clspDLEnterpriseUser
 {
	public function __construct() { }
    
	
    public static function queryToDataBase($vflEnterpriseUser, $vmySql)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus, c_enterprise.*, c_municipality.fldmunicipality";
            $vsql.=", c_state.fldstate ";
			$vsql.="FROM c_enterpriseuser "; 
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_enterprise ON c_enterpriseuser.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE c_enterpriseuser.id_user='" . $vflEnterpriseUser->idUser. "'";
			
            $vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflEnterpriseUser->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vflEnterpriseUser->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflEnterpriseUser->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflEnterpriseUser->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflEnterpriseUser->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflEnterpriseUser->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflEnterpriseUser->enterprise->locality=trim($vrow["fldlocality"]);
                $vflEnterpriseUser->enterprise->street=trim($vrow["fldstreet"]);
                $vflEnterpriseUser->enterprise->number=trim($vrow["fldnumber"]);                
                $vflEnterpriseUser->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflEnterpriseUser->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflEnterpriseUser->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflEnterpriseUser->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflEnterpriseUser->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflEnterpriseUser->userType->idUserType=(int)($vrow["id_userType"]);
				$vflEnterpriseUser->userType->userType=trim($vrow["flduserType"]);
				$vflEnterpriseUser->userStatus->idUserStatus=(int)($vrow["id_userStatus"]);
				$vflEnterpriseUser->userStatus->userStatus=trim($vrow["flduserStatus"]);
				$vflEnterpriseUser->firstName=trim($vrow["fldfirstName"]);
				$vflEnterpriseUser->lastName=trim($vrow["fldlastName"]);
				$vflEnterpriseUser->name=trim($vrow["fldname"]);
                	
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
   	
	public function __destruct(){ }
 }

?>