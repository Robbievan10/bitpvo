<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterpriseUser.php");


class clscDLEnterpriseUser
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflEnterpriseUsers, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus, c_enterprise.*, c_municipality.fldmunicipality";
            $vsql.=", c_state.fldstate ";
			$vsql.="FROM c_enterpriseuser "; 
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="INNER JOIN c_enterprise ON c_enterpriseuser.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_user.fldfirstName, c_user.fldlastName, c_user.fldname";
            
            self::clean($vflEnterpriseUsers);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $venterpriseUser= new clspFLEnterpriseUser();
                $venterpriseUser->idUser=trim($vrow["id_user"]);
                $venterpriseUser->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $venterpriseUser->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $venterpriseUser->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $venterpriseUser->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $venterpriseUser->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $venterpriseUser->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $venterpriseUser->enterprise->locality=trim($vrow["fldlocality"]);
                $venterpriseUser->enterprise->street=trim($vrow["fldstreet"]);
                $venterpriseUser->enterprise->number=trim($vrow["fldnumber"]);                
                $venterpriseUser->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $venterpriseUser->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $venterpriseUser->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $venterpriseUser->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $venterpriseUser->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $venterpriseUser->userType->idUserType=(int)($vrow["id_userType"]);
				$venterpriseUser->userType->userType=trim($vrow["flduserType"]);
				$venterpriseUser->userStatus->idUserStatus=(int)($vrow["id_userStatus"]);
				$venterpriseUser->userStatus->userStatus=trim($vrow["flduserStatus"]);
				$venterpriseUser->firstName=trim($vrow["fldfirstName"]);
				$venterpriseUser->lastName=trim($vrow["fldlastName"]);
				$venterpriseUser->name=trim($vrow["fldname"]);
                
                self::add($vflEnterpriseUsers, $venterpriseUser);
                unset($vrow, $venterpriseUser);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflEnterpriseUsers, $venterpriseUser)
	 {
        try{
            array_push($vflEnterpriseUsers->enterpriseUsers, $venterpriseUser);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflEnterpriseUsers)
	 {
        try{
            return count($vflEnterpriseUsers->enterpriseUsers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflEnterpriseUsers)
	 {
        try{
            $vflEnterpriseUsers->enterpriseUsers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>