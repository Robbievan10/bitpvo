<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashCount.php");


class clscDLCashCount
 {
    public function __construct() { }


    public static function queryToDataBase($vflCashCount, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_cashcount.*, c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_userauditor.*";
            $vsql.=", c_userauditortype.flduserType, c_userauditorstatus.flduserStatus, c_usercashier.*, c_usercashiertype.flduserType";
            $vsql.=", c_usercashierstatus.flduserStatus ";
            $vsql.="FROM p_cashcount ";
            $vsql.="INNER JOIN c_cash ON p_cashcount.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_cashcount.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseuserauditor ON p_cashcount.id_userAuditor=c_enterpriseuserauditor.id_user ";
            $vsql.="INNER JOIN c_user AS c_userauditor ON c_enterpriseuserauditor.id_user=c_userauditor.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_userauditortype ON c_userauditor.id_userType=c_userauditortype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_userauditorstatus ON c_userauditor.id_userStatus=c_userauditorstatus.id_userStatus ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseusercashier ON p_cashcount.id_userCashier=c_enterpriseusercashier.id_user ";
            $vsql.="INNER JOIN c_user AS c_usercashier ON c_enterpriseusercashier.id_user=c_usercashier.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_usercashiertype ON c_usercashier.id_userType=c_usercashiertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_usercashierstatus ON c_usercashier.id_userStatus=c_usercashierstatus.id_userStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_cashcount.fldrecordDate";

            self::clean($vflCashCount);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vcashCount= new clspFLCashCount();
                $vcashCount->idCashCount=(int)($vrow["p_cashcount.id_cashCount"]);
                $vcashCount->cash->enterprise->idEnterprise=(int)($vrow["p_cashcount.id_enterprise"]);
                $vcashCount->cash->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vcashCount->cash->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vcashCount->cash->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vcashCount->cash->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vcashCount->cash->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vcashCount->cash->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vcashCount->cash->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vcashCount->cash->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vcashCount->cash->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vcashCount->cash->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vcashCount->cash->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vcashCount->cash->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vcashCount->cash->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vcashCount->cash->idCash=(int)($vrow["p_cashcount.id_cash"]);
                $vcashCount->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vcashCount->userAuditor->idUser=trim($vrow["p_cashcount.id_userAuditor"]);
                $vcashCount->userAuditor->enterprise=$vcashCount->cash->enterprise;
                $vcashCount->userAuditor->userType->idUserType=(int)($vrow["c_userauditor.id_userType"]);
				$vcashCount->userAuditor->userType->userType=trim($vrow["c_userauditortype.flduserType"]);
				$vcashCount->userAuditor->userStatus->idUserStatus=(int)($vrow["c_userauditor.id_userStatus"]);
				$vcashCount->userAuditor->userStatus->userStatus=trim($vrow["c_userauditorstatus.flduserStatus"]);
				$vcashCount->userAuditor->firstName=trim($vrow["c_userauditor.fldfirstName"]);
				$vcashCount->userAuditor->lastName=trim($vrow["c_userauditor.fldlastName"]);
				$vcashCount->userAuditor->name=trim($vrow["c_userauditor.fldname"]);
                $vcashCount->userCashier->idUser=trim($vrow["p_cashcount.id_userCashier"]);
                $vcashCount->userCashier->enterprise=$vcashCount->cash->enterprise;
                $vcashCount->userCashier->userType->idUserType=(int)($vrow["c_usercashier.id_userType"]);
				$vcashCount->userCashier->userType->userType=trim($vrow["c_usercashiertype.flduserType"]);
				$vcashCount->userCashier->userStatus->idUserStatus=(int)($vrow["c_usercashier.id_userStatus"]);
				$vcashCount->userCashier->userStatus->userStatus=trim($vrow["c_usercashierstatus.flduserStatus"]);
				$vcashCount->userCashier->firstName=trim($vrow["c_usercashier.fldfirstName"]);
				$vcashCount->userCashier->lastName=trim($vrow["c_usercashier.fldlastName"]);
				$vcashCount->userCashier->name=trim($vrow["c_usercashier.fldname"]);
                $vcashCount->recordDate=date("m/d/Y", strtotime(trim($vrow["p_cashcount.fldrecordDate"])));
                $vcashCount->observation=$vrow["p_cashcount.fldobservation"];

                self::add($vflCashCount, $vcashCount);
                unset($vrow, $vcashCount);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryGroupByCash($vflCashCount, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_cashcount.id_cash, c_cash.fldcash ";
            $vsql.="FROM p_cashcount ";
            $vsql.="INNER JOIN c_cash ON p_cashcount.id_cash=c_cash.id_cash ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_cashcount.id_cash ";
            $vsql.="ORDER BY c_cash.fldcash";

            self::clean($vflCashCount);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcashCount= new clspFLCashCount();
                $vcashCount->cash->idCash=(int)($vrow["id_cash"]);
                $vcashCount->cash->cash=trim($vrow["fldcash"]);

                self::add($vflCashCount, $vcashCount);
                unset($vrow, $vcashCount);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function add($vflCashCount, $vcashCount)
	 {
        try{
            array_push($vflCashCount->cashCount, $vcashCount);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function total($vflCashCount)
	 {
        try{
            return count($vflCashCount->cashCount);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function clean($vflCashCount)
	 {
        try{
            $vflCashCount->cashCount=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


    public function __destruct(){ }
 }

?>
