<?php

class clspDLProductLowDetail
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductLowDetail, $vmySql)
	 {
		try{
            $vsql ="INSERT INTO p_productlowdetail(id_enterprise, id_productLow, id_product, fldlowAmount, fldlowWeightInKg";
            $vsql.=", fldpurchasePrice, fldsalePrice) ";
			$vsql.="VALUES(" . $vflProductLowDetail->productLow->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductLowDetail->productLow->idProductLow . "'";
            $vsql.=", " . $vflProductLowDetail->product->idProduct;
            $vsql.=", " . $vflProductLowDetail->lowAmount;
            $vsql.=", " . $vflProductLowDetail->lowWeightInKg;
            $vsql.=", " . $vflProductLowDetail->purchasePrice;
            $vsql.=", " . $vflProductLowDetail->salePrice . ")";
            
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProductLowDetail, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_productlowdetail ";
            $vsql.="WHERE id_enterprise=" . $vflProductLowDetail->productLow->enterprise->idEnterprise . " ";
            $vsql.="AND id_productLow='" . $vflProductLowDetail->productLow->idProductLow . "' ";
            $vsql.="AND id_product=" . $vflProductLowDetail->product->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>