<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLDocumentType.php");


class clscDLDocumentType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflDocumentTypes, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_documenttype ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY flddocumentType";
            
            self::clean($vflDocumentTypes);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vdocumentType= new clspFLDocumentType();
                $vdocumentType->idDocumentType=(int)($vrow["id_documentType"]);
                $vdocumentType->documentType=trim($vrow["flddocumentType"]);
                
                self::add($vflDocumentTypes, $vdocumentType);
                unset($vrow, $vdocumentType);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflDocumentTypes, $vdocumentType)
	 {
        try{
            array_push($vflDocumentTypes->documentTypes, $vdocumentType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflDocumentTypes)
	 {
        try{
            return count($vflDocumentTypes->documentTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflDocumentTypes)
	 {
        try{
            $vflDocumentTypes->documentTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>