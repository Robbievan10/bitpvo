<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLRoute.php");


class clscDLRoute
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflRoutes, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_route.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
			$vsql.="FROM c_route ";
            $vsql.="INNER JOIN c_enterprise ON c_route.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_route.fldroute";
            
            self::clean($vflRoutes);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vroute= new clspFLRoute();
                $vroute->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vroute->idRoute=(int)($vrow["id_route"]);
                $vroute->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vroute->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vroute->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vroute->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vroute->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vroute->enterprise->locality=trim($vrow["fldlocality"]);
                $vroute->enterprise->street=trim($vrow["fldstreet"]);
                $vroute->enterprise->number=trim($vrow["fldnumber"]);
                $vroute->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vroute->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vroute->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vroute->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vroute->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vroute->route=trim($vrow["fldroute"]);
                $vroute->description=trim($vrow["flddescription"]);
                $vroute->observation=trim($vrow["fldobservation"]);
                
                self::add($vflRoutes, $vroute);
                unset($vrow, $vroute);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflRoutes, $vroute)
	 {
        try{
            array_push($vflRoutes->routes, $vroute);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflRoutes)
	 {
        try{
            return count($vflRoutes->routes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflRoutes)
	 {
        try{
            $vflRoutes->routes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>