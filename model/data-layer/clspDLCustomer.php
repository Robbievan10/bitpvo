<?php

class clspDLCustomer
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflCustomer, $vmySql)
	 {
		try{
            $vflCustomer->idCustomer=self::getIdFromDataBase($vflCustomer, $vmySql);
            
            $vsql ="INSERT INTO c_customer(id_enterprise, id_customer, id_personType, id_state, id_municipality, fldkey, fldrfc, fldhomoclave";
			$vsql.=", fldlocality, fldstreet , fldnumber, fldpostCode, fldphoneNumber, fldmovilNumber, fldemail, fldobservation, factura) ";
			$vsql.="VALUES(" . $vflCustomer->enterprise->idEnterprise;
            $vsql.=", " . $vflCustomer->idCustomer;
			$vsql.=", " . $vflCustomer->personType->idPersonType;
			$vsql.=", " . $vflCustomer->municipality->state->idState;
			$vsql.=", " . $vflCustomer->municipality->idMunicipality;
            $vsql.=", '" . $vflCustomer->key . "'";
			$vsql.=", '" . $vflCustomer->rfc . "'";
			$vsql.=", '" . $vflCustomer->homoclave . "'";
			$vsql.=", '" . $vflCustomer->locality . "'";
			$vsql.=", '" . $vflCustomer->street . "'";
			$vsql.=", '" . $vflCustomer->number . "'";
			$vsql.=", '" . $vflCustomer->postCode . "'";
            $vsql.=", '" . $vflCustomer->phoneNumber . "'";
            $vsql.=", '" . $vflCustomer->movilNumber . "'";
            $vsql.=", '" . $vflCustomer->email . "'";
            $vsql.=", '" . $vflCustomer->observation . "'";
            $vsql.=", '" . $vflCustomer->factura . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflCustomer, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_customer ";
            $vsql.="SET id_state=" . $vflCustomer->municipality->state->idState;
			$vsql.=", id_municipality=" . $vflCustomer->municipality->idMunicipality;
            $vsql.=", fldkey='" . $vflCustomer->key . "'";
			$vsql.=", fldrfc='" . $vflCustomer->rfc . "'";
			$vsql.=", fldhomoclave='" . $vflCustomer->homoclave . "'";
			$vsql.=", fldlocality='" . $vflCustomer->locality . "'";
			$vsql.=", fldstreet='" . $vflCustomer->street . "'";
			$vsql.=", fldnumber='" . $vflCustomer->number . "'";
			$vsql.=", fldpostCode='" . $vflCustomer->postCode . "'";
			$vsql.=", fldphoneNumber='" . $vflCustomer->phoneNumber . "'";
            $vsql.=", fldmovilNumber='" . $vflCustomer->movilNumber . "'";
            $vsql.=", fldemail='" . $vflCustomer->email . "'";
            $vsql.=", fldemail='" . $vflCustomer->observation . "'";
            $vsql.=", fldobservation='" . $vflCustomer->factura . "' ";
			$vsql.="WHERE id_enterprise=" . $vflCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND id_customer=" . $vflCustomer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflCustomer, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_customer ";
            $vsql.="WHERE id_enterprise=" . $vflCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND id_customer=" . $vflCustomer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflCustomer, $vmySql)
	 {
		try{
			$vsql ="SELECT c_customer.*, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_customermunicipality.fldmunicipality, c_customerstate.fldstate ";
			$vsql.="FROM c_customer ";
            $vsql.="INNER JOIN c_enterprise ON c_customer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="WHERE c_customer.id_enterprise=" . $vflCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND c_customer.id_customer=" . $vflCustomer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflCustomer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflCustomer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflCustomer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflCustomer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflCustomer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflCustomer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflCustomer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflCustomer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflCustomer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflCustomer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflCustomer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflCustomer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflCustomer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
				$vflCustomer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vflCustomer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflCustomer->municipality->state->idState=(int)($vrow["c_customer.id_state"]);
                $vflCustomer->municipality->state->state=trim($vrow["c_customerstate.fldstate"]);
                $vflCustomer->municipality->idMunicipality=(int)($vrow["c_customer.id_municipality"]);
                $vflCustomer->municipality->municipality=trim($vrow["c_customermunicipality.fldmunicipality"]);
                $vflCustomer->key=trim($vrow["c_customer.fldkey"]);
                $vflCustomer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vflCustomer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vflCustomer->locality=trim($vrow["c_customer.fldlocality"]);
                $vflCustomer->street=trim($vrow["c_customer.fldstreet"]);
                $vflCustomer->number=trim($vrow["c_customer.fldnumber"]);
                $vflCustomer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vflCustomer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vflCustomer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vflCustomer->email=trim($vrow["c_customer.fldemail"]);
                $vflCustomer->observation=trim($vrow["c_customer.fldobservation"]);
                $vflCustomer->factura=trim($vrow["c_customer.factura"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByKey($vflCustomer, $vmySql)
	 {
		try{
			$vsql ="SELECT c_customer.*, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_customermunicipality.fldmunicipality, c_customerstate.fldstate ";
			$vsql.="FROM c_customer ";
            $vsql.="INNER JOIN c_enterprise ON c_customer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="WHERE c_customer.id_enterprise=" . $vflCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND c_customer.fldkey='" . $vflCustomer->key . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflCustomer->idCustomer=(int)($vrow["c_customer.id_customer"]);
                $vflCustomer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflCustomer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflCustomer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflCustomer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflCustomer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflCustomer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflCustomer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflCustomer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflCustomer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflCustomer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflCustomer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflCustomer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflCustomer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
				$vflCustomer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vflCustomer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflCustomer->municipality->state->idState=(int)($vrow["c_customer.id_state"]);
                $vflCustomer->municipality->state->state=trim($vrow["c_customerstate.fldstate"]);
                $vflCustomer->municipality->idMunicipality=(int)($vrow["c_customer.id_municipality"]);
                $vflCustomer->municipality->municipality=trim($vrow["c_customermunicipality.fldmunicipality"]);
                $vflCustomer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vflCustomer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vflCustomer->locality=trim($vrow["c_customer.fldlocality"]);
                $vflCustomer->street=trim($vrow["c_customer.fldstreet"]);
                $vflCustomer->number=trim($vrow["c_customer.fldnumber"]);
                $vflCustomer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vflCustomer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vflCustomer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vflCustomer->email=trim($vrow["c_customer.fldemail"]);
                $vflCustomer->observation=trim($vrow["c_customer.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflCustomer, $vmySql)
	 {
		try{
            $vidCustomer=1;
			$vsql ="SELECT MAX(id_customer) + 1 AS id_customerNew ";
			$vsql.="FROM c_customer ";
            $vsql.="WHERE id_enterprise=" . $vflCustomer->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_customerNew"])>0) ){
                $vidCustomer=(int)($vrow["id_customerNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidCustomer;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>