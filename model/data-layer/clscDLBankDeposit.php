<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBankDeposit.php");


class clscDLBankDeposit
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflBanksDeposits, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_bankdeposit.*, c_bank.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM p_bankdeposit ";
			$vsql.="INNER JOIN c_bank ON p_bankdeposit.id_enterprise=c_bank.id_enterprise ";
            $vsql.="AND p_bankdeposit.id_bank=c_bank.id_bank ";
            $vsql.="INNER JOIN c_enterprise ON c_bank.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_bankdeposit.fldrecordDate DESC, c_bank.fldname ASC";
            
            self::clean($vflBanksDeposits);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vbankDeposit= new clspFLBankDeposit();
                $vbankDeposit->bank->idBank=(int)($vrow["p_bankdeposit.id_bank"]);
                $vbankDeposit->bank->enterprise->idEnterprise=(int)($vrow["p_bankdeposit.id_enterprise"]);
                $vbankDeposit->bank->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vbankDeposit->bank->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vbankDeposit->bank->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vbankDeposit->bank->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vbankDeposit->bank->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vbankDeposit->bank->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vbankDeposit->bank->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vbankDeposit->bank->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vbankDeposit->bank->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vbankDeposit->bank->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vbankDeposit->bank->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vbankDeposit->bank->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vbankDeposit->bank->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vbankDeposit->bank->name=trim($vrow["c_bank.fldname"]);
                $vbankDeposit->idBankDeposit=(int)($vrow["p_bankdeposit.id_bankDeposit"]);
                $vbankDeposit->accountNumber=trim($vrow["p_bankdeposit.fldaccountNumber"]);
                $vbankDeposit->amount=(float)($vrow["p_bankdeposit.fldamount"]);
                $vbankDeposit->responsibleName=trim($vrow["p_bankdeposit.fldresponsibleName"]);
                $vbankDeposit->referenceName=trim($vrow["p_bankdeposit.fldreferenceName"]);
                $vbankDeposit->recordDate=date("m/d/Y", strtotime(trim($vrow["p_bankdeposit.fldrecordDate"])));
                $vbankDeposit->depositDate=date("m/d/Y", strtotime(trim($vrow["p_bankdeposit.flddepositDate"])));
                $vbankDeposit->observation=trim($vrow["p_bankdeposit.fldobservation"]);
                
                self::add($vflBanksDeposits, $vbankDeposit);
                unset($vrow, $vbankDeposit);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflBanksDeposits, $vbankDeposit)
	 {
        try{
            array_push($vflBanksDeposits->banksDeposits, $vbankDeposit);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflBanksDeposits)
	 {
        try{
            return count($vflBanksDeposits->banksDeposits);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflBanksDeposits)
	 {
        try{
            $vflBanksDeposits->banksDeposits=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    /*
    public static function totalAmount($vflBanksDeposits)
	 {
        try{
            $vtotalAmount=0;
            $vbanksDepositsTotal=self::total($vflBanksDeposits);
            for($vi=0; $vi<$vbanksDepositsTotal; $vi++){
                $vtotalAmount+=$vflBanksDeposits->banksDeposits[$vi]->amount;
            }
            return $vtotalAmount;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    */
    public function __destruct(){ }
 }

?>