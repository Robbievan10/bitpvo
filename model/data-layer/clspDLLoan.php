<?php

class clspDLLoan
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflLoan, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflLoan->idLoan=self::getIdFromDataBase($vflLoan, $vmySql);
            
            $vsql ="INSERT INTO p_loan(id_enterprise, id_dealer, id_loan, fldrecordDate, fldamount, fldobservation) ";
			$vsql.="VALUES(" . $vflLoan->dealer->enterprise->idEnterprise;
            $vsql.=", " . $vflLoan->dealer->idDealer;
            $vsql.=", " . $vflLoan->idLoan;
			$vsql.=", '" . date("Y-m-d", strtotime($vflLoan->recordDate)) . "'";
            $vsql.=", " . $vflLoan->amount;
            $vsql.=", '" . $vflLoan->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vsql, $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflLoan, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_loan ";
            $vsql.="SET fldrecordDate='" . date("Y-m-d", strtotime($vflLoan->recordDate)) . "'";
            $vsql.=", fldamount=" . $vflLoan->amount;
            $vsql.=", fldobservation='" . $vflLoan->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflLoan->dealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_dealer=" . $vflLoan->dealer->idDealer . " ";
            $vsql.="AND id_loan=" . $vflLoan->idLoan;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateStatusInDataBase($vflLoan, $vmySql)
	 {
		try{
			$vsql ="UPDATE p_loan ";
            $vsql.="SET id_operationStatus=" . $vflLoan->operationStatus->idOperationStatus . " ";
			$vsql.="WHERE id_enterprise=" . $vflLoan->dealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_dealer=" . $vflLoan->dealer->idDealer . " ";
            $vsql.="AND id_loan=" . $vflLoan->idLoan;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflLoan, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_loan ";
            $vsql.="WHERE id_enterprise=" . $vflLoan->dealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_dealer=" . $vflLoan->dealer->idDealer . " ";
            $vsql.="AND id_loan=" . $vflLoan->idLoan;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflLoan, $vmySql)
	 {
		try{
			$vsql ="SELECT p_loan.*, c_dealer.*, c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate, c_enterprise.*";
            $vsql.=", c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate, c_operationstatus.fldoperationStatus ";
            $vsql.="FROM p_loan ";
			$vsql.="INNER JOIN c_dealer ON p_loan.id_enterprise=c_dealer.id_enterprise ";
            $vsql.="AND p_loan.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise ON c_dealer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="INNER JOIN c_operationstatus ON p_loan.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="WHERE p_loan.id_enterprise=" . $vflLoan->dealer->enterprise->idEnterprise . " ";
            $vsql.="AND p_loan.id_dealer=" . $vflLoan->dealer->idDealer . " ";
            $vsql.="AND p_loan.id_loan=" . $vflLoan->idLoan;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflLoan->dealer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflLoan->dealer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflLoan->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflLoan->dealer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflLoan->dealer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflLoan->dealer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflLoan->dealer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflLoan->dealer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflLoan->dealer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflLoan->dealer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflLoan->dealer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflLoan->dealer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflLoan->dealer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflLoan->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vflLoan->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vflLoan->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vflLoan->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vflLoan->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vflLoan->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vflLoan->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vflLoan->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vflLoan->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vflLoan->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vflLoan->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vflLoan->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vflLoan->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vflLoan->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vflLoan->operationStatus->idOperationStatus=(int)($vrow["p_loan.id_operationStatus"]);
                $vflLoan->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vflLoan->recordDate=date("m/d/Y", strtotime(trim($vrow["p_loan.fldrecordDate"])));
                $vflLoan->amount=(float)($vrow["p_loan.fldamount"]);
                $vflLoan->observation=trim($vrow["p_loan.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflLoan, $vmySql)
	 {
		try{
            $vidLoan=1;
			$vsql ="SELECT MAX(id_loan) + 1 AS id_loanNew ";
			$vsql.="FROM p_loan ";
            $vsql.="WHERE id_enterprise=" . $vflLoan->dealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_dealer=" . $vflLoan->dealer->idDealer;
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_loanNew"])>0) ){
                $vidLoan=(int)($vrow["id_loanNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidLoan;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>