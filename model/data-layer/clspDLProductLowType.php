<?php

class clspDLProductLowType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflProductLowType, $vmySql)
	 {
		try{
			$vsql ="SELECT fldproductLowType ";
            $vsql.="FROM c_productlowtype ";
            $vsql.="WHERE id_productLowType=" . $vflProductLowType->idProductLowType;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflProductLowType->productLowType=trim($vrow["fldproductLowType"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>