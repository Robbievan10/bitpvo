<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashType.php");


class clscDLCashType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflCashTypes, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_cashtype ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY fldcashType";
            
            self::clean($vflCashTypes);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcashType= new clspFLCashType();
                $vcashType->idCashType=(int)($vrow["id_cashType"]);
                $vcashType->cashType=trim($vrow["fldcashType"]);
                
                self::add($vflCashTypes, $vcashType);
                unset($vrow, $vcashType);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflCashTypes, $vcashType)
	 {
        try{
            array_push($vflCashTypes->cashTypes, $vcashType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflCashTypes)
	 {
        try{
            return count($vflCashTypes->cashTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflCashTypes)
	 {
        try{
            $vflCashTypes->cashTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>