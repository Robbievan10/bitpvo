<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLOperationStatus.php");


class clscDLOperationStatus
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflOperationStatus, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_operationstatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY fldoperationStatus";
            
            self::clean($vflOperationStatus);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $voperationStatus= new clspFLOperationStatus();
                $voperationStatus->idOperationStatus=(int)($vrow["id_operationStatus"]);
                $voperationStatus->operationStatus=trim($vrow["fldoperationStatus"]);
                
                self::add($vflOperationStatus, $voperationStatus);
                unset($vrow, $voperationStatus);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflOperationStatus, $voperationStatus)
	 {
        try{
            array_push($vflOperationStatus->operationStatus, $voperationStatus);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflOperationStatus)
	 {
        try{
            return count($vflOperationStatus->operationStatus);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflOperationStatus)
	 {
        try{
            $vflOperationStatus->operationStatus=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>