<?php

class clspDLDealer
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflDealer, $vmySql)
	 {
		try{
            $vflDealer->idDealer=self::getIdFromDataBase($vflDealer, $vmySql);
            
            $vsql ="INSERT INTO c_dealer(id_enterprise, id_dealer, id_state, id_municipality, fldkey, fldname, fldfirstName, fldlastName";
			$vsql.=", fldlocality, fldstreet , fldnumber, fldpostCode, fldphoneNumber, fldmovilNumber, fldobservation) ";
			$vsql.="VALUES(" . $vflDealer->enterprise->idEnterprise;
            $vsql.=", " . $vflDealer->idDealer;
			$vsql.=", " . $vflDealer->municipality->state->idState;
			$vsql.=", " . $vflDealer->municipality->idMunicipality;
            $vsql.=", '" . $vflDealer->key . "'";
			$vsql.=", '" . $vflDealer->name . "'";
			$vsql.=", '" . $vflDealer->firstName . "'";
            $vsql.=", '" . $vflDealer->lastName . "'";
			$vsql.=", '" . $vflDealer->locality . "'";
			$vsql.=", '" . $vflDealer->street . "'";
			$vsql.=", '" . $vflDealer->number . "'";
			$vsql.=", '" . $vflDealer->postCode . "'";
            $vsql.=", '" . $vflDealer->phoneNumber . "'";
            $vsql.=", '" . $vflDealer->movilNumber . "'";
            $vsql.=", '" . $vflDealer->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflDealer, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_dealer ";
            $vsql.="SET id_state=" . $vflDealer->municipality->state->idState;
			$vsql.=", id_municipality=" . $vflDealer->municipality->idMunicipality;
            $vsql.=", fldkey='" . $vflDealer->key . "'";
			$vsql.=", fldname='" . $vflDealer->name . "'";
            $vsql.=", fldfirstName='" . $vflDealer->firstName . "'";
            $vsql.=", fldlastName='" . $vflDealer->lastName . "'";
            $vsql.=", fldlocality='" . $vflDealer->locality . "'";
			$vsql.=", fldstreet='" . $vflDealer->street . "'";
			$vsql.=", fldnumber='" . $vflDealer->number . "'";
			$vsql.=", fldpostCode='" . $vflDealer->postCode . "'";
			$vsql.=", fldphoneNumber='" . $vflDealer->phoneNumber . "'";
            $vsql.=", fldmovilNumber='" . $vflDealer->movilNumber . "'";
            $vsql.=", fldobservation='" . $vflDealer->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflDealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_dealer=" . $vflDealer->idDealer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflDealer, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_dealer ";
            $vsql.="WHERE id_enterprise=" . $vflDealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_dealer=" . $vflDealer->idDealer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflDealer, $vmySql)
	 {
		try{
			$vsql ="SELECT c_dealer.*, c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate, c_enterprise.*";
            $vsql.=", c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_dealer ";
            $vsql.="INNER JOIN c_enterprise ON c_dealer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="WHERE c_dealer.id_enterprise=" . $vflDealer->enterprise->idEnterprise . " ";
            $vsql.="AND c_dealer.id_dealer=" . $vflDealer->idDealer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflDealer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflDealer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflDealer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflDealer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflDealer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflDealer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflDealer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflDealer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflDealer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflDealer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflDealer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflDealer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflDealer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflDealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vflDealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vflDealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vflDealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vflDealer->key=trim($vrow["c_dealer.fldkey"]);
                $vflDealer->name=trim($vrow["c_dealer.fldname"]);
                $vflDealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vflDealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vflDealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vflDealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vflDealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vflDealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vflDealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vflDealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vflDealer->observation=trim($vrow["c_dealer.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBaseByKey($vflDealer, $vmySql)
	 {
		try{
			$vsql ="SELECT c_dealer.*, c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate, c_enterprise.*";
            $vsql.=", c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_dealer ";
            $vsql.="INNER JOIN c_enterprise ON c_dealer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="WHERE c_dealer.id_enterprise=" . $vflDealer->enterprise->idEnterprise . " ";
            $vsql.="AND c_dealer.fldkey='" . $vflDealer->key . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflDealer->idDealer=(int)($vrow["c_dealer.id_dealer"]);
                $vflDealer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflDealer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflDealer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflDealer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflDealer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflDealer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflDealer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflDealer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflDealer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflDealer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflDealer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflDealer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflDealer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflDealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vflDealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vflDealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vflDealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vflDealer->name=trim($vrow["c_dealer.fldname"]);
                $vflDealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vflDealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vflDealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vflDealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vflDealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vflDealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vflDealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vflDealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vflDealer->observation=trim($vrow["c_dealer.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflDealer, $vmySql)
	 {
		try{
            $vidDealer=1;
			$vsql ="SELECT MAX(id_dealer) + 1 AS id_dealerNew ";
			$vsql.="FROM c_dealer ";
            $vsql.="WHERE id_enterprise=" . $vflDealer->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_dealerNew"])>0) ){
                $vidDealer=(int)($vrow["id_dealerNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidDealer;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>