<?php

class clspDLCashCountCash
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflCashCountCash, $vmySql)
	 {
		try{
            $vflCashCountCash->idCashCountCash=self::getIdFromDataBase($vflCashCountCash, $vmySql);
            
            $vsql ="INSERT INTO p_cashcountcash(id_enterprise, id_cash, id_cashCount, id_cashCountCash, id_cashType, fldamount, fldvalue) ";
			$vsql.="VALUES(" . $vflCashCountCash->cashCount->cash->enterprise->idEnterprise;
            $vsql.=", " . $vflCashCountCash->cashCount->cash->idCash;
            $vsql.=", " . $vflCashCountCash->cashCount->idCashCount;
            $vsql.=", " . $vflCashCountCash->idCashCountCash;
            $vsql.=", " . $vflCashCountCash->cashType->idCashType;
            $vsql.=", " . $vflCashCountCash->amount;
            $vsql.=", " . $vflCashCountCash->value . ")";
            
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage() . $vsql, $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflCashCountCash, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_cashcountcash ";
            $vsql.="WHERE id_enterprise=" . $vflCashCountCash->cashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash='" . $vflCashCountCash->cashCount->cash->idCash . "' ";
            $vsql.="AND id_cashCount=" . $vflCashCountCash->cashCount->idCashCount . " ";
            $vsql.="AND id_cashCountCash=" . $vflCashCountCash->idCashCountCash;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflCashCountCash, $vmySql)
	 {
		try{
            $vidCashCountCash=1;
			$vsql ="SELECT MAX(id_cashCountCash) + 1 AS id_cashCountCashNew ";
			$vsql.="FROM p_cashcountcash ";
            $vsql.="WHERE id_enterprise=" . $vflCashCountCash->cashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashCountCash->cashCount->cash->idCash . " ";
            $vsql.="AND id_cashCount=" . $vflCashCountCash->cashCount->idCashCount . " ";
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_cashCountCashNew"])>0) ){
                $vidCashCountCash=(int)($vrow["id_cashCountCashNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidCashCountCash;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public function __destruct(){ }
 }

?>