<?php

class clspDLProductType
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductType, $vmySql)
	 {
		try{
            $vflProductType->idProductType=self::getIdFromDataBase($vflProductType, $vmySql);
            
            $vsql ="INSERT INTO c_producttype(id_enterprise, id_productType, fldproductType) ";
			$vsql.="VALUES(" . $vflProductType->enterprise->idEnterprise;
            $vsql.=", " . $vflProductType->idProductType;
			$vsql.=", '" . $vflProductType->productType . "')";
                        
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductType, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_producttype ";
            $vsql.="SET fldproductType='" . $vflProductType->productType . "' ";
            $vsql.="WHERE id_enterprise=" . $vflProductType->enterprise->idEnterprise . " ";
            $vsql.="AND id_productType=" . $vflProductType->idProductType;
			
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflProductType, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_producttype ";
            $vsql.="WHERE id_enterprise=" . $vflProductType->enterprise->idEnterprise . " ";
            $vsql.="AND id_productType=" . $vflProductType->idProductType;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductType, $vmySql)
	 {
		try{
			$vsql ="SELECT c_producttype.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_producttype ";
            $vsql.="INNER JOIN c_enterprise ON c_producttype.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE c_producttype.id_enterprise=" . $vflProductType->enterprise->idEnterprise . " ";
            $vsql.="AND c_producttype.id_productType=" . $vflProductType->idProductType;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflProductType->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vflProductType->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vflProductType->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vflProductType->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vflProductType->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vflProductType->enterprise->locality=trim($vrow["fldlocality"]);
                $vflProductType->enterprise->street=trim($vrow["fldstreet"]);
                $vflProductType->enterprise->number=trim($vrow["fldnumber"]);
                $vflProductType->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vflProductType->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vflProductType->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vflProductType->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vflProductType->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vflProductType->productType=trim($vrow["fldproductType"]);
                				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    private static function getIdFromDataBase($vflProductType, $vmySql)
	 {
		try{
            $vidProductType=1;
			$vsql ="SELECT MAX(id_productType) + 1 AS id_productTypeNew ";
			$vsql.="FROM c_producttype ";
            $vsql.="WHERE id_enterprise=" . $vflProductType->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productTypeNew"])>0) ){
                $vidProductType=(int)($vrow["id_productTypeNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProductType;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
	public function __destruct(){ }
 }

?>