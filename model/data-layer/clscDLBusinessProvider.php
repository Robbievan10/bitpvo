<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBusinessProvider.php");


class clscDLBusinessProvider
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflBusinessProviders, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_businessprovider.*, c_provider.*, c_persontype.fldpersonType, c_businessprovidermunicipality.fldmunicipality";
            $vsql.=", c_businessproviderstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_businessprovider ";
            $vsql.="INNER JOIN c_provider ON c_businessprovider.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND c_businessprovider.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise ON c_provider.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_businessprovidermunicipality ON c_provider.id_state=c_businessprovidermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_businessprovidermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_businessproviderstate ON c_businessprovidermunicipality.id_state=c_businessproviderstate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_businessprovider.fldbusinessName";
            
            self::clean($vflBusinessProviders);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vbusinessProvider= new clspFLBusinessProvider();
                $vbusinessProvider->businessName=trim($vrow["c_businessprovider.fldbusinessName"]);
                $vbusinessProvider->idProvider=(int)($vrow["c_provider.id_provider"]);
                $vbusinessProvider->enterprise->idEnterprise=(int)($vrow["c_provider.id_enterprise"]);
                $vbusinessProvider->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vbusinessProvider->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vbusinessProvider->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vbusinessProvider->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vbusinessProvider->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vbusinessProvider->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vbusinessProvider->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vbusinessProvider->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vbusinessProvider->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vbusinessProvider->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vbusinessProvider->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vbusinessProvider->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vbusinessProvider->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vbusinessProvider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
                $vbusinessProvider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vbusinessProvider->municipality->state->idState=(int)($vrow["c_provider.id_state"]);
                $vbusinessProvider->municipality->state->state=trim($vrow["c_businessproviderstate.fldstate"]);
                $vbusinessProvider->municipality->idMunicipality=(int)($vrow["c_provider.id_municipality"]);
                $vbusinessProvider->municipality->municipality=trim($vrow["c_businessprovidermunicipality.fldmunicipality"]);
                $vbusinessProvider->key=trim($vrow["c_provider.fldkey"]);
                $vbusinessProvider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vbusinessProvider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vbusinessProvider->locality=trim($vrow["c_provider.fldlocality"]);
                $vbusinessProvider->street=trim($vrow["c_provider.fldstreet"]);
                $vbusinessProvider->number=trim($vrow["c_provider.fldnumber"]);
                $vbusinessProvider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vbusinessProvider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vbusinessProvider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vbusinessProvider->email=trim($vrow["c_provider.fldemail"]);
                $vbusinessProvider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vbusinessProvider->observation=trim($vrow["c_provider.fldobservation"]);
                
                self::add($vflBusinessProviders, $vbusinessProvider);
                unset($vrow, $vbusinessProvider);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflBusinessProviders, $vbusinessProvider)
	 {
        try{
            array_push($vflBusinessProviders->businessProviders, $vbusinessProvider);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflBusinessProviders)
	 {
        try{
            return count($vflBusinessProviders->businessProviders);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflBusinessProviders)
	 {
        try{
            $vflBusinessProviders->businessProviders=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>