<?php

class clspDLProductEntryDetail
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductEntryDetail, $vmySql)
	 {
		try{
            $vsql ="INSERT INTO p_productentrydetail(id_enterprise, id_productEntry, id_product, fldpurchaseAmountInPt, fldpurchaseAmountInKg";
            $vsql.=", fldrealAmountInPt, fldrealAmountInKg, fldpurchasePrice) ";
			$vsql.="VALUES(" . $vflProductEntryDetail->productEntry->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductEntryDetail->productEntry->idProductEntry . "'";
            $vsql.=", " . $vflProductEntryDetail->product->idProduct;
            $vsql.=", " . $vflProductEntryDetail->purchaseAmountInPt;
            $vsql.=", " . $vflProductEntryDetail->purchaseAmountInKg;
            $vsql.=", " . $vflProductEntryDetail->realAmountInPt;
            $vsql.=", " . $vflProductEntryDetail->realAmountInKg;
			$vsql.=", " . $vflProductEntryDetail->purchasePrice . ")";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
     
    public static function deleteInDataBase($vflProductEntryDetail, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_productentrydetail ";
            $vsql.="WHERE id_enterprise=" . $vflProductEntryDetail->productEntry->enterprise->idEnterprise . " ";
            $vsql.="AND id_productEntry='" . $vflProductEntryDetail->productEntry->idProductEntry . "' ";
            $vsql.="AND id_product=" . $vflProductEntryDetail->product->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>