<?php

require_once (dirname(dirname(__FILE__)) . "/tools/clspString.php");


class clspDLProductDistribution
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductDistribution, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflProductDistribution->idProductDistribution=self::getIdFromDataBase($vflProductDistribution, $vmySql);
            
            $vsql ="INSERT INTO p_productdistribution(id_enterprise, id_productDistribution, id_route, fldproductDistributionRecordDate";
            $vsql.=", fldproductDistributionDate, fldcreditTotal, flddiscountTotal, flddevolutionTotal, fldobservation) ";
			$vsql.="VALUES(" . $vflProductDistribution->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductDistribution->idProductDistribution . "'";
			$vsql.=", " . $vflProductDistribution->routeDealer->route->idRoute;
			$vsql.=", '" . date("Y-m-d", strtotime($vflProductDistribution->productDistributionRecordDate)) . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflProductDistribution->productDistributionDate)) . "'";
			$vsql.=", " . $vflProductDistribution->creditTotal . " ";
			$vsql.=", " . $vflProductDistribution->discountTotal . " ";
            $vsql.=", " . $vflProductDistribution->devolutionTotal . " ";
            $vsql.=", '" . $vflProductDistribution->observation . "')";
            
			$vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductDistribution, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_productdistribution ";
            $vsql.="SET id_route=" . $vflProductDistribution->routeDealer->route->idRoute;
			$vsql.=", fldproductDistributionDate='" . date("Y-m-d", strtotime($vflProductDistribution->productDistributionDate)) . "'";
            $vsql.=", fldobservation='" . $vflProductDistribution->observation . "' ";
            $vsql.="WHERE id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function recordCuttingInDataBase($vflProductDistribution, $vmySql,$json)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_productdistribution ";
            $vsql.="SET fldproductDistributionCuttingDate='" . date("Y-m-d", strtotime($vflProductDistribution->productDistributionCuttingDate)) . "' ";
            $vsql.=", fldcreditTotal=" . $vflProductDistribution->creditTotal . " ";
            $vsql.=", flddiscountTotal=" . $vflProductDistribution->discountTotal . " ";
            $vsql.=", flddevolutionTotal=" . $vflProductDistribution->devolutionTotal . " ";
            $vsql.=", fldobservation=" . json_encode($json,JSON_UNESCAPED_UNICODE) . " ";

            $vsql.="WHERE id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }




    public static function openCuttingInDataBase($vflProductDistribution, $vmySql)
     {
        try{
            date_default_timezone_set('America/Mexico_City');
            $vsql ="UPDATE p_productdistribution ";
            $vsql.="SET fldproductDistributionCuttingDate=NULL ";
            $vsql.="WHERE id_enterprise='" . $vflProductDistribution->enterprise->idEnterprise . "' ";
            $vsql.="AND id_productDistribution='" . $vflProductDistribution->idProductDistribution . "' ";
            
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
            }
            
            unset($vsql);
            return 1;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
     }


    public static function recordCuttingInDataBase_noDate($vflProductDistribution, $vmySql,$json)
     {
        try{
            date_default_timezone_set('America/Mexico_City');
            $vsql ="UPDATE p_productdistribution ";
            $vsql.="SET fldproductDistributionCuttingDate='" .NULL. "' ";
            $vsql.=", fldcreditTotal=" . $vflProductDistribution->creditTotal . " ";
            $vsql.=", flddiscountTotal=" . $vflProductDistribution->discountTotal . " ";
            $vsql.=", flddevolutionTotal=" . $vflProductDistribution->devolutionTotal . " ";
            $vsql.=", fldobservation=" . json_encode($json,JSON_UNESCAPED_UNICODE) . " ";

            $vsql.="WHERE id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
            
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
            }
            
            unset($vsql);
            return 1;
        }
        catch (Exception $vexception){
            throw new Exception($vexception->getMessage(), $vexception->getCode());
        }
     }

    
    public static function deleteInDataBase($vflProductDistribution, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_productdistribution ";
            $vsql.="WHERE id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductDistribution, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productdistribution.*, c_productdistributionenterprise.*, c_productdistributionenterprisemunicipality.fldmunicipality";
			$vsql.=", c_productdistributionenterprisestate.fldstate, c_routedealer.*, c_route.*, c_routeenterprise.*";
			$vsql.=", c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate, c_dealer.*, c_dealerenterprise.*";
			$vsql.=", c_dealerenterprisemunicipality.fldmunicipality, c_dealerenterprisestate.fldstate, c_dealermunicipality.fldmunicipality";
			$vsql.=", c_dealerstate.fldstate ";
			$vsql.="FROM p_productdistribution ";
			$vsql.="INNER JOIN c_enterprise AS c_productdistributionenterprise ON p_productdistribution.id_enterprise=c_productdistributionenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productdistributionenterprisemunicipality ON c_productdistributionenterprise.id_state=c_productdistributionenterprisemunicipality.id_state ";
            $vsql.="AND c_productdistributionenterprise.id_municipality=c_productdistributionenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productdistributionenterprisestate ON c_productdistributionenterprisemunicipality.id_state=c_productdistributionenterprisestate.id_state ";
            $vsql.="INNER JOIN c_routedealer ON p_productdistribution.id_enterprise=c_routedealer.id_enterprise ";
            $vsql.="AND p_productdistribution.id_route=c_routedealer.id_route ";
			$vsql.="INNER JOIN c_route ON c_routedealer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routedealer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="WHERE p_productdistribution.id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND p_productdistribution.id_productDistribution='" . $vflProductDistribution->idProductDistribution . "'";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProductDistribution->enterprise->municipality->state->idState=(int)($vrow["c_productdistributionenterprise.id_state"]);
                $vflProductDistribution->enterprise->municipality->state->state=trim($vrow["c_productdistributionenterprisestate.fldstate"]);
                $vflProductDistribution->enterprise->municipality->idMunicipality=(int)($vrow["c_productdistributionenterprise.id_municipality"]);
                $vflProductDistribution->enterprise->municipality->municipality=trim($vrow["c_productdistributionenterprisemunicipality.fldmunicipality"]);
                $vflProductDistribution->enterprise->enterprise=trim($vrow["c_productdistributionenterprise.fldenterprise"]);
                $vflProductDistribution->enterprise->locality=trim($vrow["c_productdistributionenterprise.fldlocality"]);
                $vflProductDistribution->enterprise->street=trim($vrow["c_productdistributionenterprise.fldstreet"]);
                $vflProductDistribution->enterprise->number=trim($vrow["c_productdistributionenterprise.fldnumber"]);
                $vflProductDistribution->enterprise->phoneNumber=trim($vrow["c_productdistributionenterprise.fldphoneNumber"]);
                $vflProductDistribution->enterprise->movilNumber=trim($vrow["c_productdistributionenterprise.fldmovilNumber"]);
                $vflProductDistribution->enterprise->pageWeb=trim($vrow["c_productdistributionenterprise.fldpageWeb"]);
                $vflProductDistribution->routeDealer->route->idRoute=(int)($vrow["p_productdistribution.id_route"]);
                $vflProductDistribution->routeDealer->route->enterprise->idEnterprise=(int)($vrow["p_productdistribution.id_enterprise"]);
                $vflProductDistribution->routeDealer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vflProductDistribution->routeDealer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vflProductDistribution->routeDealer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vflProductDistribution->routeDealer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vflProductDistribution->routeDealer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vflProductDistribution->routeDealer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vflProductDistribution->routeDealer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vflProductDistribution->routeDealer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vflProductDistribution->routeDealer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vflProductDistribution->routeDealer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vflProductDistribution->routeDealer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vflProductDistribution->routeDealer->route->route=trim($vrow["c_route.fldroute"]);
                $vflProductDistribution->routeDealer->route->description=trim($vrow["c_route.flddescription"]);
                $vflProductDistribution->routeDealer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vflProductDistribution->routeDealer->dealer->idDealer=(int)($vrow["c_routedealer.id_dealer"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->idEnterprise=(int)($vrow["c_routedealer.id_enterprise"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vflProductDistribution->routeDealer->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vflProductDistribution->routeDealer->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vflProductDistribution->routeDealer->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vflProductDistribution->routeDealer->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vflProductDistribution->routeDealer->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vflProductDistribution->routeDealer->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vflProductDistribution->routeDealer->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vflProductDistribution->routeDealer->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vflProductDistribution->routeDealer->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vflProductDistribution->routeDealer->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vflProductDistribution->routeDealer->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vflProductDistribution->routeDealer->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vflProductDistribution->routeDealer->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vflProductDistribution->routeDealer->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vflProductDistribution->routeDealer->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vflProductDistribution->routeDealer->assignDate=date("m/d/Y", strtotime(trim($vrow["c_routedealer.fldassignDate"])));
				$vflProductDistribution->productDistributionRecordDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionRecordDate"])));
                $vflProductDistribution->CreatedOn=trim($vrow["p_productdistribution.CreatedOn"]);
				$vflProductDistribution->productDistributionDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionDate"])));
				$vflProductDistribution->productDistributionCuttingDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionCuttingDate"])));
				$vflProductDistribution->creditTotal=(float)($vrow["p_productdistribution.fldcreditTotal"]);
				$vflProductDistribution->discountTotal=(float)($vrow["p_productdistribution.flddiscountTotal"]);
                $vflProductDistribution->devolutionTotal=(float)($vrow["p_productdistribution.flddevolutionTotal"]);
                $vflProductDistribution->observation=trim($vrow["p_productdistribution.fldobservation"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProductDistribution, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vidProductDistribution="000001-". date("y"); ;
			$vsql ="SELECT MAX(CONVERT(SUBSTRING(id_productDistribution, 1, 6), SIGNED)) + 1 id_productDistributionNew ";
			$vsql.="FROM p_productdistribution ";
            $vsql.="WHERE id_enterprise=" . $vflProductDistribution->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productDistributionNew"])>0) ){
                $vstring= new clspString($vrow["id_productDistributionNew"]);
                $vidProductDistribution=$vstring->getAdjustedString(6) . "-" . date("y");;
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProductDistribution;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>