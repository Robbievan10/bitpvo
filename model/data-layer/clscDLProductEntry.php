<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntry.php");


class clscDLProductEntry
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsEntries, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productentry.*, c_productentryenterprise.*, c_productentryenterprisemunicipality.fldmunicipality, c_productentryenterprisestate.fldstate";
            $vsql.=", c_provider.*, c_providerenterprise.*, c_providerenterprisemunicipality.fldmunicipality, c_providerenterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_providermunicipality.fldmunicipality, c_providerstate.fldstate, c_operationtype.fldoperationType";
            $vsql.=", c_operationstatus.fldoperationStatus ";
            $vsql.="FROM p_productentry ";
			$vsql.="INNER JOIN c_enterprise AS c_productentryenterprise ON p_productentry.id_enterprise=c_productentryenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productentryenterprisemunicipality ON c_productentryenterprise.id_state=c_productentryenterprisemunicipality.id_state ";
            $vsql.="AND c_productentryenterprise.id_municipality=c_productentryenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productentryenterprisestate ON c_productentryenterprisemunicipality.id_state=c_productentryenterprisestate.id_state ";
            $vsql.="INNER JOIN c_provider ON p_productentry.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND p_productentry.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise AS c_providerenterprise ON c_provider.id_enterprise=c_providerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_providerenterprisemunicipality ON c_providerenterprise.id_state=c_providerenterprisemunicipality.id_state ";
            $vsql.="AND c_providerenterprise.id_municipality=c_providerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_providerenterprisestate ON c_providerenterprisemunicipality.id_state=c_providerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_providermunicipality ON c_provider.id_state=c_providermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_providermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_providerstate ON c_providermunicipality.id_state=c_providerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productentry.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productentry.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productentry.id_productEntry";
            
            self::clean($vflProductsEntries);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductEntry= new clspFLProductEntry();
                $vproductEntry->idProductEntry=trim($vrow["p_productentry.id_productEntry"]);
                $vproductEntry->enterprise->idEnterprise=(int)($vrow["p_productentry.id_enterprise"]);
                $vproductEntry->enterprise->municipality->state->idState=(int)($vrow["c_productentryenterprise.id_state"]);
                $vproductEntry->enterprise->municipality->state->state=trim($vrow["c_productentryenterprisestate.fldstate"]);
                $vproductEntry->enterprise->municipality->idMunicipality=(int)($vrow["c_productentryenterprise.id_municipality"]);
                $vproductEntry->enterprise->municipality->municipality=trim($vrow["c_productentryenterprisemunicipality.fldmunicipality"]);
                $vproductEntry->enterprise->enterprise=trim($vrow["c_productentryenterprise.fldenterprise"]);
                $vproductEntry->enterprise->locality=trim($vrow["c_productentryenterprise.fldlocality"]);
                $vproductEntry->enterprise->street=trim($vrow["c_productentryenterprise.fldstreet"]);
                $vproductEntry->enterprise->number=trim($vrow["c_productentryenterprise.fldnumber"]);
                $vproductEntry->enterprise->phoneNumber=trim($vrow["c_productentryenterprise.fldphoneNumber"]);
                $vproductEntry->enterprise->movilNumber=trim($vrow["c_productentryenterprise.fldmovilNumber"]);
                $vproductEntry->enterprise->pageWeb=trim($vrow["c_productentryenterprise.fldpageWeb"]);
                $vproductEntry->provider->idProvider=(int)($vrow["p_productentry.id_provider"]);
                $vproductEntry->provider->enterprise->idEnterprise=(int)($vrow["p_productentry.id_enterprise"]);
                $vproductEntry->provider->enterprise->municipality->state->idState=(int)($vrow["c_providerenterprise.id_state"]);
                $vproductEntry->provider->enterprise->municipality->state->state=trim($vrow["c_providerenterprisestate.fldstate"]);
                $vproductEntry->provider->enterprise->municipality->idMunicipality=(int)($vrow["c_providerenterprise.id_municipality"]);
                $vproductEntry->provider->enterprise->municipality->municipality=trim($vrow["c_providerenterprisemunicipality.fldmunicipality"]);
                $vproductEntry->provider->enterprise->enterprise=trim($vrow["c_providerenterprise.fldenterprise"]);
                $vproductEntry->provider->enterprise->locality=trim($vrow["c_providerenterprise.fldlocality"]);
                $vproductEntry->provider->enterprise->street=trim($vrow["c_providerenterprise.fldstreet"]);
                $vproductEntry->provider->enterprise->number=trim($vrow["c_providerenterprise.fldnumber"]);                
                $vproductEntry->provider->enterprise->phoneNumber=trim($vrow["c_providerenterprise.fldphoneNumber"]);
                $vproductEntry->provider->enterprise->movilNumber=trim($vrow["c_providerenterprise.fldmovilNumber"]);
                $vproductEntry->provider->enterprise->pageWeb=trim($vrow["c_providerenterprise.fldpageWeb"]);
				$vproductEntry->provider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
				$vproductEntry->provider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vproductEntry->provider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vproductEntry->provider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vproductEntry->provider->locality=trim($vrow["c_provider.fldlocality"]);
                $vproductEntry->provider->street=trim($vrow["c_provider.fldstreet"]);
                $vproductEntry->provider->number=trim($vrow["c_provider.fldnumber"]);
                $vproductEntry->provider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vproductEntry->provider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vproductEntry->provider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vproductEntry->provider->email=trim($vrow["c_provider.fldemail"]);
                $vproductEntry->provider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vproductEntry->provider->observation=trim($vrow["c_provider.fldobservation"]);
                $vproductEntry->operationType->idOperationType=(int)($vrow["p_productentry.id_operationType"]);
                $vproductEntry->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vproductEntry->operationStatus->idOperationStatus=(int)($vrow["p_productentry.id_operationStatus"]);
                $vproductEntry->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vproductEntry->saleFolio=trim($vrow["p_productentry.fldsaleFolio"]);
                $vproductEntry->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldrecordDate"])));
                $vproductEntry->productEntryDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldproductEntryDate"])));
                
                self::add($vflProductsEntries, $vproductEntry);
                unset($vrow, $vproductEntry);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflProductsEntries, $vproductEntry)
	 {
        try{
            array_push($vflProductsEntries->productsEntries, $vproductEntry);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsEntries)
	 {
        try{
            return count($vflProductsEntries->productsEntries);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsEntries)
	 {
        try{
            $vflProductsEntries->productsEntries=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>