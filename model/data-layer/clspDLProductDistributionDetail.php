<?php

class clspDLProductDistributionDetail
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductDistributionDetail, $vmySql)
	 {
		try{
            $vsql ="INSERT INTO p_productdistributiondetail(id_enterprise, id_productDistribution, id_product, flddistributionAmount";
            $vsql.=", flddistributionWeightInkg, flddistributionPrice) ";
			$vsql.="VALUES(" . $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductDistributionDetail->productDistribution->idProductDistribution . "'";
            $vsql.=", " . $vflProductDistributionDetail->product->idProduct;
            $vsql.=", " . $vflProductDistributionDetail->distributionAmount;
            $vsql.=", " . $vflProductDistributionDetail->distributionWeightInkg;
            $vsql.=", " . $vflProductDistributionDetail->distributionPrice. ")";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflProductDistributionDetail, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_productdistributiondetail ";
            $vsql.="WHERE id_enterprise=" . $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND id_productDistribution='" . $vflProductDistributionDetail->productDistribution->idProductDistribution . "' ";
            $vsql.="AND id_product=" . $vflProductDistributionDetail->product->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateReturnedAmountInDataBase($vflProductDistributionDetail, $vmySql)
	 {
		try{
			$vsql ="UPDATE p_productdistributiondetail ";
            $vsql.="SET fldreturnedAmount=" . $vflProductDistributionDetail->returnedAmount . " ";
			$vsql.="WHERE id_enterprise=" . $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND id_productDistribution='" . $vflProductDistributionDetail->productDistribution->idProductDistribution . "' ";
            $vsql.="AND id_product=" . $vflProductDistributionDetail->product->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function queryToDataBase($vflProductDistributionDetail, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productdistributiondetail.*, p_productdistribution.*, c_productdistributionenterprise.*";
            $vsql.=", c_productdistributionenterprisemunicipality.fldmunicipality, c_productdistributionenterprisestate.fldstate, c_routedealer.*, c_route.*";
            $vsql.=", c_routeenterprise.*, c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate, c_dealer.*, c_dealerenterprise.*";
            $vsql.=", c_dealerenterprisemunicipality.fldmunicipality, c_dealerenterprisestate.fldstate, c_dealermunicipality.fldmunicipality";
			$vsql.=", c_dealerstate.fldstate, c_product.*, c_productenterprise.*, c_productenterprisemunicipality.fldmunicipality";
            $vsql.=", c_productenterprisestate.fldstate, c_producttype.fldproductType ";
            $vsql.="FROM p_productdistributiondetail ";
			$vsql.="INNER JOIN p_productdistribution ON p_productdistributiondetail.id_enterprise=p_productdistribution.id_enterprise ";
            $vsql.="AND p_productdistributiondetail.id_productDistribution=p_productdistribution.id_productDistribution ";
           	$vsql.="INNER JOIN c_enterprise AS c_productdistributionenterprise ON p_productdistribution.id_enterprise=c_productdistributionenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productdistributionenterprisemunicipality ON c_productdistributionenterprise.id_state=c_productdistributionenterprisemunicipality.id_state ";
            $vsql.="AND c_productdistributionenterprise.id_municipality=c_productdistributionenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productdistributionenterprisestate ON c_productdistributionenterprisemunicipality.id_state=c_productdistributionenterprisestate.id_state ";
            $vsql.="INNER JOIN c_routedealer ON p_productdistribution.id_enterprise=c_routedealer.id_enterprise ";
            $vsql.="AND p_productdistribution.id_route=c_routedealer.id_route ";
			$vsql.="INNER JOIN c_route ON c_routedealer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routedealer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="INNER JOIN c_product ON p_productdistributiondetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productdistributiondetail.id_product=c_product.id_product ";
            $vsql.="INNER JOIN c_enterprise AS c_productenterprise ON c_product.id_enterprise=c_productenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productenterprisemunicipality  ON c_productenterprise.id_state=c_productenterprisemunicipality.id_state ";
            $vsql.="AND c_productenterprise.id_municipality=c_productenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productenterprisestate ON c_productenterprisemunicipality.id_state=c_productenterprisestate.id_state ";
            $vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.="WHERE p_productdistributiondetail.id_enterprise=" . $vflProductDistributionDetail->productDistribution->enterprise->idEnterprise . " ";
            $vsql.="AND p_productdistributiondetail.id_productDistribution='" . $vflProductDistributionDetail->productDistribution->idProductDistribution . "' ";
            $vsql.="AND p_productdistributiondetail.id_product=" . $vflProductDistributionDetail->product->idProduct;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProductDistributionDetail->productDistribution->enterprise->municipality->state->idState=(int)($vrow["c_productdistributionenterprise.id_state"]);
                $vflProductDistributionDetail->productDistribution->enterprise->municipality->state->state=trim($vrow["c_productdistributionenterprisestate.fldstate"]);
                $vflProductDistributionDetail->productDistribution->enterprise->municipality->idMunicipality=(int)($vrow["c_productdistributionenterprise.id_municipality"]);
                $vflProductDistributionDetail->productDistribution->enterprise->municipality->municipality=trim($vrow["c_productdistributionenterprisemunicipality.fldmunicipality"]);
                $vflProductDistributionDetail->productDistribution->enterprise->enterprise=trim($vrow["c_productdistributionenterprise.fldenterprise"]);
                $vflProductDistributionDetail->productDistribution->enterprise->locality=trim($vrow["c_productdistributionenterprise.fldlocality"]);
                $vflProductDistributionDetail->productDistribution->enterprise->street=trim($vrow["c_productdistributionenterprise.fldstreet"]);
                $vflProductDistributionDetail->productDistribution->enterprise->number=trim($vrow["c_productdistributionenterprise.fldnumber"]);
                $vflProductDistributionDetail->productDistribution->enterprise->phoneNumber=trim($vrow["c_productdistributionenterprise.fldphoneNumber"]);
                $vflProductDistributionDetail->productDistribution->enterprise->movilNumber=trim($vrow["c_productdistributionenterprise.fldmovilNumber"]);
                $vflProductDistributionDetail->productDistribution->enterprise->pageWeb=trim($vrow["c_productdistributionenterprise.fldpageWeb"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->idRoute=(int)($vrow["p_productdistribution.id_route"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->idEnterprise=(int)($vrow["p_productdistribution.id_enterprise"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->route=trim($vrow["c_route.fldroute"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->description=trim($vrow["c_route.flddescription"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->idDealer=(int)($vrow["c_routedealer.id_dealer"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->idEnterprise=(int)($vrow["c_routedealer.id_enterprise"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vflProductDistributionDetail->productDistribution->routeDealer->assignDate=date("m/d/Y", strtotime(trim($vrow["c_routedealer.fldassignDate"])));
				$vflProductDistributionDetail->productDistribution->productDistributionRecordDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionRecordDate"])));
				$vflProductDistributionDetail->productDistribution->productDistributionDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionDate"])));
				$vflProductDistributionDetail->productDistribution->productDistributionCuttingDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionCuttingDate"])));
				$vflProductDistributionDetail->productDistribution->creditTotal=(float)($vrow["p_productdistribution.fldcreditTotal"]);
				$vflProductDistributionDetail->productDistribution->discountTotal=(float)($vrow["p_productdistribution.flddiscountTotal"]);
                $vflProductDistributionDetail->productDistribution->devolutionTotal=(float)($vrow["p_productdistribution.flddevolutionTotal"]);
                $vflProductDistributionDetail->product->enterprise->idEnterprise=(int)($vrow["p_productdistributiondetail.id_enterprise"]);
                $vflProductDistributionDetail->product->enterprise->municipality->state->idState=(int)($vrow["c_productenterprise.id_state"]);
                $vflProductDistributionDetail->product->enterprise->municipality->state->state=trim($vrow["c_productenterprisestate.fldstate"]);
                $vflProductDistributionDetail->product->enterprise->municipality->idMunicipality=(int)($vrow["c_productenterprise.id_municipality"]);
                $vflProductDistributionDetail->product->enterprise->municipality->municipality=trim($vrow["c_productenterprisemunicipality.fldmunicipality"]);
                $vflProductDistributionDetail->product->enterprise->enterprise=trim($vrow["c_productenterprise.fldenterprise"]);
                $vflProductDistributionDetail->product->enterprise->locality=trim($vrow["c_productenterprise.fldlocality"]);
                $vflProductDistributionDetail->product->enterprise->street=trim($vrow["c_productenterprise.fldstreet"]);
                $vflProductDistributionDetail->product->enterprise->number=trim($vrow["c_productenterprise.fldnumber"]);
                $vflProductDistributionDetail->product->enterprise->phoneNumber=trim($vrow["c_productenterprise.fldphoneNumber"]);
                $vflProductDistributionDetail->product->enterprise->movilNumber=trim($vrow["c_productenterprise.fldmovilNumber"]);
                $vflProductDistributionDetail->product->enterprise->pageWeb=trim($vrow["c_productenterprise.fldpageWeb"]);
                $vflProductDistributionDetail->product->productType->idProductType=(int)($vrow["c_product.id_productType"]);
                $vflProductDistributionDetail->product->productType->enterprise=$vflProductDistributionDetail->productDistribution->enterprise;
                $vflProductDistributionDetail->product->productType->productType=trim($vrow["c_producttype.fldproductType"]);
                $vflProductDistributionDetail->product->key=trim($vrow["c_product.fldkey"]);
                $vflProductDistributionDetail->product->barCode=trim($vrow["c_product.fldbarCode"]);
                $vflProductDistributionDetail->product->name=trim($vrow["c_product.fldname"]);
                $vflProductDistributionDetail->product->description=trim($vrow["c_product.flddescription"]);
                $vflProductDistributionDetail->product->image=trim($vrow["c_product.fldimage"]);
                $vflProductDistributionDetail->product->purchasePrice=(float)($vrow["c_product.fldpurchasePrice"]);
                $vflProductDistributionDetail->product->salePrice=(float)($vrow["c_product.fldsalePrice"]);
                $vflProductDistributionDetail->product->weightInKg=(float)($vrow["c_product.fldweightInKg"]);
                $vflProductDistributionDetail->product->stock=(float)($vrow["c_product.fldstock"]);
                $vflProductDistributionDetail->product->observation=trim($vrow["c_product.fldobservation"]);
                $vflProductDistributionDetail->distributionAmount=(float)($vrow["p_productdistributiondetail.flddistributionAmount"]);
                $vflProductDistributionDetail->distributionPrice=(float)($vrow["p_productdistributiondetail.flddistributionPrice"]);
                $vflProductDistributionDetail->distributionWeightInkg=(float)($vrow["p_productdistributiondetail.flddistributionWeightInKg"]);
                $vflProductDistributionDetail->returnedAmount=(float)($vrow["p_productdistributiondetail.fldreturnedAmount"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>