<?php

require_once (dirname(dirname(__FILE__)) . "/tools/clspString.php");


class clspDLProductSale
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductSale, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflProductSale->idProductSale=self::getIdFromDataBase($vflProductSale, $vmySql);
            $vsql ="INSERT INTO p_productsale(id_enterprise, id_productSale, id_customer, id_operationType, id_operationStatus, id_cash, id_user";
            $vsql.=", fldrecordDate, fldproductSaleDate, fldcanceled,detalles) ";
			$vsql.="VALUES(" . $vflProductSale->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductSale->idProductSale. "'";
			$vsql.=", " . $vflProductSale->customer->idCustomer;
            $vsql.=", " . $vflProductSale->operationType->idOperationType;
            $vsql.=", " . $vflProductSale->operationStatus->idOperationStatus;
            $vsql.=", " . $vflProductSale->cash->idCash;
            $vsql.=", '" . $vflProductSale->user->idUser . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflProductSale->recordDate)) . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflProductSale->productSaleDate)) . "'";
            $vsql.=", '" . $vflProductSale->canceled . "'";
            $vsql.=", '" . $vflProductSale->detalle . "')";

			$vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function cancelInDataBase($vflProductSale, $vmySql)
	 {
		try{
			$vsql ="UPDATE p_productsale ";
            $vsql.="SET fldcanceled=1 ";
			$vsql.="WHERE id_enterprise=" . $vflProductSale->enterprise->idEnterprise . " ";
            $vsql.="AND id_productSale='" . $vflProductSale->idProductSale . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateStatusInDataBase($vflProductSale, $vmySql)
	 {
		try{
			$vsql ="UPDATE p_productsale ";
            $vsql.="SET id_operationStatus=" . $vflProductSale->operationStatus->idOperationStatus . " ";
			$vsql.="WHERE id_enterprise=" . $vflProductSale->enterprise->idEnterprise . " ";
            $vsql.="AND id_productSale='" . $vflProductSale->idProductSale . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	public static function queryToDataBase($vflProductSale, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productsale.*, c_productsaleenterprise.*, c_productsaleenterprisemunicipality.fldmunicipality, c_productsaleenterprisestate.fldstate";
            $vsql.=", c_customer.*, c_customerenterprise.*, c_customerenterprisemunicipality.fldmunicipality, c_customerenterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_customermunicipality.fldmunicipality, c_customerstate.fldstate, c_operationtype.fldoperationType";
            $vsql.=", c_operationstatus.fldoperationStatus, c_cash.*, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.="FROM p_productsale ";
			$vsql.="INNER JOIN c_enterprise AS c_productsaleenterprise ON p_productsale.id_enterprise=c_productsaleenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productsaleenterprisemunicipality ON c_productsaleenterprise.id_state=c_productsaleenterprisemunicipality.id_state ";
            $vsql.="AND c_productsaleenterprise.id_municipality=c_productsaleenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productsaleenterprisestate ON c_productsaleenterprisemunicipality.id_state=c_productsaleenterprisestate.id_state ";
            $vsql.="INNER JOIN c_customer ON p_productsale.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise AS c_customerenterprise ON c_customer.id_enterprise=c_customerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_customerenterprisemunicipality ON c_customerenterprise.id_state=c_customerenterprisemunicipality.id_state ";
            $vsql.="AND c_customerenterprise.id_municipality=c_customerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_customerenterprisestate ON c_customerenterprisemunicipality.id_state=c_customerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productsale.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="INNER JOIN c_cash ON p_productsale.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_productsale.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise AS c_cashenterprise ON c_cash.id_enterprise=c_cashenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_cashenterprisemunicipality ON c_cashenterprise.id_state=c_cashenterprisemunicipality.id_state ";
            $vsql.="AND c_cashenterprise.id_municipality=c_cashenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_cashenterprisestate ON c_cashenterprisemunicipality.id_state=c_cashenterprisestate.id_state ";
            $vsql.="INNER JOIN c_enterpriseuser ON p_productsale.id_user=c_enterpriseuser.id_user ";
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="WHERE p_productsale.id_enterprise=" . $vflProductSale->enterprise->idEnterprise . " ";
            $vsql.="AND p_productsale.id_productSale='" . $vflProductSale->idProductSale . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProductSale->enterprise->municipality->state->idState=(int)($vrow["c_productsaleenterprise.id_state"]);
                $vflProductSale->enterprise->municipality->state->state=trim($vrow["c_productsaleenterprisestate.fldstate"]);
                $vflProductSale->enterprise->municipality->idMunicipality=(int)($vrow["c_productsaleenterprise.id_municipality"]);
                $vflProductSale->enterprise->municipality->municipality=trim($vrow["c_productsaleenterprisemunicipality.fldmunicipality"]);
                $vflProductSale->enterprise->enterprise=trim($vrow["c_productsaleenterprise.fldenterprise"]);
                $vflProductSale->enterprise->locality=trim($vrow["c_productsaleenterprise.fldlocality"]);
                $vflProductSale->enterprise->street=trim($vrow["c_productsaleenterprise.fldstreet"]);
                $vflProductSale->enterprise->number=trim($vrow["c_productsaleenterprise.fldnumber"]);
                $vflProductSale->enterprise->phoneNumber=trim($vrow["c_productsaleenterprise.fldphoneNumber"]);
                $vflProductSale->enterprise->movilNumber=trim($vrow["c_productsaleenterprise.fldmovilNumber"]);
                $vflProductSale->enterprise->pageWeb=trim($vrow["c_productsaleenterprise.fldpageWeb"]);
                $vflProductSale->customer->idCustomer=(int)($vrow["p_productsale.id_customer"]);
                $vflProductSale->customer->enterprise->idEnterprise=(int)($vrow["p_productsale.id_enterprise"]);
                $vflProductSale->customer->enterprise->municipality->state->idState=(int)($vrow["c_customerenterprise.id_state"]);
                $vflProductSale->customer->enterprise->municipality->state->state=trim($vrow["c_customerenterprisestate.fldstate"]);
                $vflProductSale->customer->enterprise->municipality->idMunicipality=(int)($vrow["c_customerenterprise.id_municipality"]);
                $vflProductSale->customer->enterprise->municipality->municipality=trim($vrow["c_customerenterprisemunicipality.fldmunicipality"]);
                $vflProductSale->customer->enterprise->enterprise=trim($vrow["c_customerenterprise.fldenterprise"]);
                $vflProductSale->customer->enterprise->locality=trim($vrow["c_customerenterprise.fldlocality"]);
                $vflProductSale->customer->enterprise->street=trim($vrow["c_customerenterprise.fldstreet"]);
                $vflProductSale->customer->enterprise->number=trim($vrow["c_customerenterprise.fldnumber"]);                
                $vflProductSale->customer->enterprise->phoneNumber=trim($vrow["c_customerenterprise.fldphoneNumber"]);
                $vflProductSale->customer->enterprise->movilNumber=trim($vrow["c_customerenterprise.fldmovilNumber"]);
                $vflProductSale->customer->enterprise->pageWeb=trim($vrow["c_customerenterprise.fldpageWeb"]);
				$vflProductSale->customer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vflProductSale->customer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflProductSale->customer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vflProductSale->customer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vflProductSale->customer->locality=trim($vrow["c_customer.fldlocality"]);
                $vflProductSale->customer->street=trim($vrow["c_customer.fldstreet"]);
                $vflProductSale->customer->number=trim($vrow["c_customer.fldnumber"]);
                $vflProductSale->customer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vflProductSale->customer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vflProductSale->customer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vflProductSale->customer->email=trim($vrow["c_customer.fldemail"]);
                $vflProductSale->customer->observation=trim($vrow["c_customer.fldobservation"]);
                $vflProductSale->operationType->idOperationType=(int)($vrow["p_productsale.id_operationType"]);
                $vflProductSale->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vflProductSale->operationStatus->idOperationStatus=(int)($vrow["p_productsale.id_operationStatus"]);
                $vflProductSale->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vflProductSale->cash->enterprise=$vflProductSale->enterprise;
                $vflProductSale->cash->idCash=trim($vrow["p_productsale.id_cash"]);
                $vflProductSale->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vflProductSale->user->idUser=trim($vrow["p_productsale.id_user"]);
                $vflProductSale->user->enterprise=$vflProductSale->cash->enterprise;
                $vflProductSale->user->userType->idUserType=(int)($vrow["c_user.id_userType"]);
				$vflProductSale->user->userType->userType=trim($vrow["c_usertype.flduserType"]);
				$vflProductSale->user->userStatus->idUserStatus=(int)($vrow["c_user.id_userStatus"]);
				$vflProductSale->user->userStatus->userStatus=trim($vrow["c_userstatus.flduserStatus"]);
				$vflProductSale->user->firstName=trim($vrow["c_user.fldfirstName"]);
				$vflProductSale->user->lastName=trim($vrow["c_user.fldlastName"]);
				$vflProductSale->user->name=trim($vrow["c_user.fldname"]);
                $vflProductSale->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldrecordDate"])));
                $vflProductSale->productSaleDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldproductSaleDate"])));
                $vflProductSale->canceled=(int)($vrow["p_productsale.fldcanceled"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProductSale, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vidProductSale="000001-". date("y"); ;
			$vsql ="SELECT MAX(CONVERT(SUBSTRING(id_productSale, 1, 6), SIGNED)) + 1 id_productSaleNew ";
			$vsql.="FROM p_productsale ";
            $vsql.="WHERE id_enterprise=" . $vflProductSale->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productSaleNew"])>0) ){
                $vstring= new clspString($vrow["id_productSaleNew"]);
                $vidProductSale=$vstring->getAdjustedString(6) . "-" . date("y");;
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProductSale;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>