<?php

class clspDLDocumentType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflDocumentType, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_documenttype ";
            $vsql.="WHERE id_documentType=" . $vflDocumentType->idDocumentType;
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflDocumentType->documentType=trim($vrow["flddocumentType"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct(){ }
 }

?>