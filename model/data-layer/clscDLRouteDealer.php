<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLRouteDealer.php");


class clscDLRouteDealer
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflRoutesDeliverers, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_routedealer.*, c_route.*, c_routeenterprise.*, c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate";
            $vsql.=", c_dealer.*, c_dealerenterprise.*, c_dealerenterprisemunicipality.fldmunicipality, c_dealerenterprisestate.fldstate";
            $vsql.=", c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate ";
            $vsql.="FROM c_routedealer ";
            $vsql.="INNER JOIN c_route ON c_routedealer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routedealer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_routedealer.id_enterprise, c_routedealer.id_route, c_routedealer.id_dealer, c_routedealer.fldassignDate";
            
            self::clean($vflRoutesDeliverers);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vrouteDealer= new clspFLRouteDealer();
                $vrouteDealer->route->idRoute=(int)($vrow["c_routedealer.id_route"]);
                $vrouteDealer->route->enterprise->idEnterprise=(int)($vrow["c_routedealer.id_enterprise"]);
                $vrouteDealer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vrouteDealer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vrouteDealer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vrouteDealer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vrouteDealer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vrouteDealer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vrouteDealer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vrouteDealer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vrouteDealer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vrouteDealer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vrouteDealer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vrouteDealer->route->route=trim($vrow["c_route.fldroute"]);
                $vrouteDealer->route->description=trim($vrow["c_route.flddescription"]);
                $vrouteDealer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vrouteDealer->dealer->idDealer=(int)($vrow["c_routedealer.id_dealer"]);
                $vrouteDealer->dealer->enterprise->idEnterprise=(int)($vrow["c_routedealer.id_enterprise"]);
                $vrouteDealer->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vrouteDealer->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vrouteDealer->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vrouteDealer->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vrouteDealer->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vrouteDealer->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vrouteDealer->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vrouteDealer->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vrouteDealer->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vrouteDealer->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vrouteDealer->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vrouteDealer->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vrouteDealer->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vrouteDealer->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vrouteDealer->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vrouteDealer->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vrouteDealer->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vrouteDealer->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vrouteDealer->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vrouteDealer->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vrouteDealer->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vrouteDealer->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vrouteDealer->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vrouteDealer->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vrouteDealer->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vrouteDealer->assignDate=date("m/d/Y", strtotime(trim($vrow["c_routedealer.fldassignDate"])));
                
                self::add($vflRoutesDeliverers, $vrouteDealer);
                unset($vrow, $vrouteDealer);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflRoutesDeliverers, $vrouteDealer)
	 {
        try{
            array_push($vflRoutesDeliverers->routesDeliverers, $vrouteDealer);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflRoutesDeliverers)
	 {
        try{
            return count($vflRoutesDeliverers->routesDeliverers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflRoutesDeliverers)
	 {
        try{
            $vflRoutesDeliverers->routesDeliverers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>