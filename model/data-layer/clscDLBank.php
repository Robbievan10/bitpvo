<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBank.php");


class clscDLBank
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflBanks, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_bank.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_bank ";
            $vsql.="INNER JOIN c_enterprise ON c_bank.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_bank.fldname";
            
            self::clean($vflBanks);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vbank= new clspFLBank();
                $vbank->idBank=(int)($vrow["id_bank"]);
                $vbank->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vbank->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vbank->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vbank->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vbank->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vbank->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vbank->enterprise->locality=trim($vrow["fldlocality"]);
                $vbank->enterprise->street=trim($vrow["fldstreet"]);
                $vbank->enterprise->number=trim($vrow["fldnumber"]);
                $vbank->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vbank->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vbank->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vbank->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vbank->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vbank->name=trim($vrow["fldname"]);
                
                self::add($vflBanks, $vbank);
                unset($vrow, $vbank);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflBanks, $vbank)
	 {
        try{
            array_push($vflBanks->banks, $vbank);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflBanks)
	 {
        try{
            return count($vflBanks->banks);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflBanks)
	 {
        try{
            $vflBanks->banks=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>