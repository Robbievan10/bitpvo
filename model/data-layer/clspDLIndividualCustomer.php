<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCustomer.php");


class clspDLIndividualCustomer
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflIndividualCustomer, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            if ( clspDLCustomer::addToDataBase($vflIndividualCustomer, $vmySql)==1 ){
                $vsql ="INSERT INTO c_individualcustomer(id_enterprise, id_customer, fldname, fldfirstName, fldlastName) ";
				$vsql.="VALUES(" . $vflIndividualCustomer->enterprise->idEnterprise;
                $vsql.=", " . $vflIndividualCustomer->idCustomer;
                $vsql.=", '" . $vflIndividualCustomer->name ."'";
                $vsql.=", '" . $vflIndividualCustomer->firstName ."'";
				$vsql.=", '" . $vflIndividualCustomer->lastName ."')";
                
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    $vmySql->rollbackTransaction();
                    return 0;
                }
            }
            else{
                 $vmySql->rollbackTransaction();
                 return -1;
            }
            $vmySql->commitTransaction();
			
			unset( $vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflIndividualCustomer, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            $vupdateCustomerStatus=clspDLCustomer::updateInDataBase($vflIndividualCustomer, $vmySql);
            
			$vsql ="UPDATE c_individualcustomer ";
            $vsql.="SET fldname='" . $vflIndividualCustomer->name . "'";
            $vsql.=", fldfirstName='" . $vflIndividualCustomer->firstName . "'";
            $vsql.=", fldlastName='" . $vflIndividualCustomer->lastName . "' ";
            $vsql.="WHERE id_enterprise=" . $vflIndividualCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND id_customer=" . $vflIndividualCustomer->idCustomer;
            
            $vmySql->executeSql($vsql);
			if ( ($vupdateCustomerStatus==0) and ($vmySql->getAffectedRowsNumber()==0) ){
                $vmySql->rollbackTransaction();
                return 0;
			}
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflIndividualCustomer, $vmySql)
	 {
		try{
            $vsql ="DELETE FROM c_individualcustomer ";
            $vsql.="WHERE id_enterprise=" . $vflIndividualCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND id_customer=" . $vflIndividualCustomer->idCustomer;
            
            $vmySql->startTransaction();
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLCustomer::deleteInDataBase($vflIndividualCustomer, $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    return -1;
                }
            }
            else{
                $vmySql->rollbackTransaction();
                return 0;
            }
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function queryToDataBase($vflIndividualCustomer, $vmySql)
	 {
		try{
            $vsql ="SELECT c_individualcustomer.*, c_customer.*, c_persontype.fldpersonType, c_individualcustomermunicipality.fldmunicipality";
            $vsql.=", c_individualcustomerstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_individualcustomer ";
            $vsql.="INNER JOIN c_customer ON c_individualcustomer.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND c_individualcustomer.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise ON c_customer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_individualcustomermunicipality ON c_customer.id_state=c_individualcustomermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_individualcustomermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_individualcustomerstate ON c_individualcustomermunicipality.id_state=c_individualcustomerstate.id_state ";
            $vsql.="WHERE c_individualcustomer.id_enterprise=" . $vflIndividualCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND c_individualcustomer.id_customer=" . $vflIndividualCustomer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflIndividualCustomer->name=trim($vrow["c_individualcustomer.fldname"]);
                $vflIndividualCustomer->firstName=trim($vrow["c_individualcustomer.fldfirstName"]);
                $vflIndividualCustomer->lastName=trim($vrow["c_individualcustomer.fldlastName"]);
                $vflIndividualCustomer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflIndividualCustomer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflIndividualCustomer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflIndividualCustomer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflIndividualCustomer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflIndividualCustomer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflIndividualCustomer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflIndividualCustomer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflIndividualCustomer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflIndividualCustomer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflIndividualCustomer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflIndividualCustomer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflIndividualCustomer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflIndividualCustomer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
                $vflIndividualCustomer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflIndividualCustomer->municipality->state->idState=(int)($vrow["c_customer.id_state"]);
                $vflIndividualCustomer->municipality->state->state=trim($vrow["c_individualcustomerstate.fldstate"]);
                $vflIndividualCustomer->municipality->idMunicipality=(int)($vrow["c_customer.id_municipality"]);
                $vflIndividualCustomer->municipality->municipality=trim($vrow["c_individualcustomermunicipality.fldmunicipality"]);
                $vflIndividualCustomer->key=trim($vrow["c_customer.fldkey"]);
                $vflIndividualCustomer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vflIndividualCustomer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vflIndividualCustomer->locality=trim($vrow["c_customer.fldlocality"]);
                $vflIndividualCustomer->street=trim($vrow["c_customer.fldstreet"]);
                $vflIndividualCustomer->number=trim($vrow["c_customer.fldnumber"]);
                $vflIndividualCustomer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vflIndividualCustomer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vflIndividualCustomer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vflIndividualCustomer->email=trim($vrow["c_customer.fldemail"]);
                $vflIndividualCustomer->observation=trim($vrow["c_customer.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     	
	public function __destruct(){ }
 }

?>