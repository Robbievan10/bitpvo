<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLPayment.php");


class clspDLLoanPayment
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflLoanPayment, $vmySql, $vtype=0)
	 {
        try{
            if ( $vtype==0 ){
                $vmySql->startTransaction();
            }
            if ( clspDLPayment::addToDataBase($vflLoanPayment, $vmySql)==1 ){
                $vsql ="INSERT INTO p_loanpayment(id_enterprise, id_payment, id_dealer, id_loan) ";
				$vsql.="VALUES(" . $vflLoanPayment->loan->dealer->enterprise->idEnterprise;
                $vsql.=", " . $vflLoanPayment->idPayment;
                $vsql.=", " . $vflLoanPayment->loan->dealer->idDealer;
                $vsql.=", " . $vflLoanPayment->loan->idLoan . ")";
                
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    if ( $vtype==0 ){        
                        $vmySql->rollbackTransaction();
                    }
                    return 0;
                }
            }
            else{
                if ( $vtype==0 ){
                    $vmySql->rollbackTransaction();
                }
                return -1;
            }
            if ( $vtype==0 ){
                $vmySql->commitTransaction();
            }
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
            if ( $vtype==0 ){
                $vmySql->rollbackTransaction();
            }
		    throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflLoanPayment, $vmySql, $vtype=0)
	 {
		try{
            $vsql ="DELETE FROM p_loanpayment ";
            $vsql.="WHERE id_enterprise=" . $vflLoanPayment->loan->dealer->enterprise->idEnterprise . " ";
            $vsql.="AND id_payment=" . $vflLoanPayment->idPayment . " ";
            $vsql.="AND id_dealer=" . $vflLoanPayment->loan->dealer->idDealer . " ";
            $vsql.="AND id_loan=" . $vflLoanPayment->loan->idLoan;
            
            if ( $vtype==0 ){
                $vmySql->startTransaction();
            }
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLPayment::deleteInDataBase($vflLoanPayment, $vmySql)==0 ){
                    if ( $vtype==0 ){
                        $vmySql->rollbackTransaction();
                    }
                    return -1;
                }
            }
            else{
                if ( $vtype==0 ){  
                    $vmySql->rollbackTransaction();
                }
                return 0;
            }
            if ( $vtype==0 ){
                $vmySql->commitTransaction();
            }
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
		    if ( $vtype==0 ){
                $vmySql->rollbackTransaction();
            }
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>