<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProvider.php");


class clspDLBusinessProvider
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflBusinessProvider, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            if ( clspDLProvider::addToDataBase($vflBusinessProvider, $vmySql)==1 ){
                $vsql ="INSERT INTO c_businessprovider(id_enterprise, id_provider, fldbusinessName) ";
				$vsql.="VALUES(" . $vflBusinessProvider->enterprise->idEnterprise;
                $vsql.=", " . $vflBusinessProvider->idProvider;
                $vsql.=", '" . $vflBusinessProvider->businessName ."')";
                
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    $vmySql->rollbackTransaction();
                    return 0;
                }
            }
            else{
                 $vmySql->rollbackTransaction();
                 return -1;
            }
            $vmySql->commitTransaction();
			
			unset( $vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBusinessProvider, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            $vupdateProviderStatus=clspDLProvider::updateInDataBase($vflBusinessProvider, $vmySql);
            
			$vsql ="UPDATE c_businessprovider ";
            $vsql.="SET fldbusinessName='" . $vflBusinessProvider->businessName . "' ";
            $vsql.="WHERE id_enterprise=" . $vflBusinessProvider->enterprise->idEnterprise . " ";
            $vsql.="AND id_provider=" . $vflBusinessProvider->idProvider;
            
            $vmySql->executeSql($vsql);
			if ( ($vupdateProviderStatus==0) and ($vmySql->getAffectedRowsNumber()==0) ){
                $vmySql->rollbackTransaction();
                return 0;
			}
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBusinessProvider, $vmySql)
	 {
		try{
            $vsql ="DELETE FROM c_businessprovider ";
            $vsql.="WHERE id_enterprise=" . $vflBusinessProvider->enterprise->idEnterprise . " ";
            $vsql.="AND id_provider=" . $vflBusinessProvider->idProvider;
            
            $vmySql->startTransaction();
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLProvider::deleteInDataBase($vflBusinessProvider, $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    return -1;
                }
            }
            else{
                $vmySql->rollbackTransaction();
                return 0;
            }
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBusinessProvider, $vmySql)
	 {
		try{
            $vsql ="SELECT c_businessprovider.*, c_provider.*, c_persontype.fldpersonType, c_businessprovidermunicipality.fldmunicipality";
            $vsql.=", c_businessproviderstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_businessprovider ";
            $vsql.="INNER JOIN c_provider ON c_businessprovider.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND c_businessprovider.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise ON c_provider.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_businessprovidermunicipality ON c_provider.id_state=c_businessprovidermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_businessprovidermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_businessproviderstate ON c_businessprovidermunicipality.id_state=c_businessproviderstate.id_state ";
            $vsql.="WHERE c_businessprovider.id_enterprise=" . $vflBusinessProvider->enterprise->idEnterprise . " ";
            $vsql.="AND c_businessprovider.id_provider=" . $vflBusinessProvider->idProvider;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflBusinessProvider->businessName=trim($vrow["c_businessprovider.fldbusinessName"]);
                $vflBusinessProvider->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflBusinessProvider->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflBusinessProvider->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflBusinessProvider->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflBusinessProvider->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflBusinessProvider->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflBusinessProvider->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflBusinessProvider->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflBusinessProvider->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflBusinessProvider->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflBusinessProvider->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflBusinessProvider->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflBusinessProvider->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflBusinessProvider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
                $vflBusinessProvider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflBusinessProvider->municipality->state->idState=(int)($vrow["c_provider.id_state"]);
                $vflBusinessProvider->municipality->state->state=trim($vrow["c_businessproviderstate.fldstate"]);
                $vflBusinessProvider->municipality->idMunicipality=(int)($vrow["c_provider.id_municipality"]);
                $vflBusinessProvider->municipality->municipality=trim($vrow["c_businessprovidermunicipality.fldmunicipality"]);
                $vflBusinessProvider->key=trim($vrow["c_provider.fldkey"]);
                $vflBusinessProvider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vflBusinessProvider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vflBusinessProvider->locality=trim($vrow["c_provider.fldlocality"]);
                $vflBusinessProvider->street=trim($vrow["c_provider.fldstreet"]);
                $vflBusinessProvider->number=trim($vrow["c_provider.fldnumber"]);
                $vflBusinessProvider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vflBusinessProvider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vflBusinessProvider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vflBusinessProvider->email=trim($vrow["c_provider.fldemail"]);
                $vflBusinessProvider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vflBusinessProvider->observation=trim($vrow["c_provider.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>