<?php

require_once (dirname(dirname(__FILE__)) . "/tools/clspString.php");


class clspDLProductLow
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductLow, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflProductLow->idProductLow=self::getIdFromDataBase($vflProductLow, $vmySql);
            
            $vsql ="INSERT INTO p_productlow(id_enterprise, id_productLow, id_productLowType, fldrecordDate, fldproductLowDate,";
            $vsql.="fldobservation) ";
			$vsql.="VALUES(" . $vflProductLow->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductLow->idProductLow. "'";
			$vsql.=", " . $vflProductLow->productLowType->idProductLowType;
            $vsql.=", '" . date("Y-m-d", strtotime($vflProductLow->recordDate)) . "'";
			$vsql.=", '" . date("Y-m-d", strtotime($vflProductLow->productLowDate)). "'";
            $vsql.=", '" . $vflProductLow->observation. "')";
            
			$vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductLow, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_productlow ";
            $vsql.="SET id_productLowType=" . $vflProductLow->productLowType->idProductLowType;
			$vsql.=", fldproductLowDate='" . date("Y-m-d", strtotime($vflProductLow->productLowDate)) . "' ";
			$vsql.="WHERE id_enterprise=" . $vflProductLow->enterprise->idEnterprise . " ";
            $vsql.="AND id_productLow='" . $vflProductLow->idProductLow . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	     
    public static function deleteInDataBase($vflProductLow, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_productlow ";
            $vsql.="WHERE id_enterprise=" . $vflProductLow->enterprise->idEnterprise . " ";
            $vsql.="AND id_productLow='" . $vflProductLow->idProductLow . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductLow, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productlow.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_productlowtype.fldproductLowType ";
            $vsql.="FROM p_productlow ";
			$vsql.="INNER JOIN c_enterprise ON p_productlow.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_productlowtype ON p_productlow.id_productLowType=c_productlowtype.id_productLowType ";
            $vsql.="WHERE p_productlow.id_enterprise=" . $vflProductLow->enterprise->idEnterprise . " ";
            $vsql.="AND p_productlow.id_productLow='" . $vflProductLow->idProductLow . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProductLow->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflProductLow->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vflProductLow->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflProductLow->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vflProductLow->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflProductLow->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflProductLow->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflProductLow->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflProductLow->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflProductLow->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflProductLow->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflProductLow->productLowType->idProductLowType=(int)($vrow["p_productlow.id_productLowType"]);
                $vflProductLow->productLowType->productLowType=trim($vrow["c_productlowtype.fldproductLowType"]);
                $vflProductLow->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productlow.fldrecordDate"])));
                $vflProductLow->productLowDate=date("m/d/Y", strtotime(trim($vrow["p_productlow.fldproductLowDate"])));
                $vflProductLow->observation=trim($vrow["p_productlow.fldobservation"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProductLow, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vidProductLow="000001-". date("y"); ;
			$vsql ="SELECT MAX(CONVERT(SUBSTRING(id_productLow, 1, 6), SIGNED)) + 1 id_productLowNew ";
			$vsql.="FROM p_productlow ";
            $vsql.="WHERE id_enterprise=" . $vflProductLow->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productLowNew"])>0) ){
                $vstring= new clspString($vrow["id_productLowNew"]);
                $vidProductLow=$vstring->getAdjustedString(6) . "-" . date("y");;
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProductLow;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>