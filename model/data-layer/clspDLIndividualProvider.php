<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLProvider.php");


class clspDLIndividualProvider
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflIndividualProvider, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            if ( clspDLProvider::addToDataBase($vflIndividualProvider, $vmySql)==1 ){
                $vsql ="INSERT INTO c_individualprovider(id_enterprise, id_provider, fldname, fldfirstName, fldlastName) ";
				$vsql.="VALUES(" . $vflIndividualProvider->enterprise->idEnterprise;
                $vsql.=", " . $vflIndividualProvider->idProvider;
                $vsql.=", '" . $vflIndividualProvider->name ."'";
                $vsql.=", '" . $vflIndividualProvider->firstName ."'";
				$vsql.=", '" . $vflIndividualProvider->lastName ."')";
                
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    $vmySql->rollbackTransaction();
                    return 0;
                }
            }
            else{
                 $vmySql->rollbackTransaction();
                 return -1;
            }
            $vmySql->commitTransaction();
			
			unset( $vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflIndividualProvider, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            $vupdateProviderStatus=clspDLProvider::updateInDataBase($vflIndividualProvider, $vmySql);
            
			$vsql ="UPDATE c_individualprovider ";
            $vsql.="SET fldname='" . $vflIndividualProvider->name . "'";
            $vsql.=", fldfirstName='" . $vflIndividualProvider->firstName . "'";
            $vsql.=", fldlastName='" . $vflIndividualProvider->lastName . "' ";
            $vsql.="WHERE id_enterprise=" . $vflIndividualProvider->enterprise->idEnterprise . " ";
            $vsql.="AND id_provider=" . $vflIndividualProvider->idProvider;
            
            $vmySql->executeSql($vsql);
			if ( ($vupdateProviderStatus==0) and ($vmySql->getAffectedRowsNumber()==0) ){
                $vmySql->rollbackTransaction();
                return 0;
			}
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflIndividualProvider, $vmySql)
	 {
		try{
            $vsql ="DELETE FROM c_individualprovider ";
            $vsql.="WHERE id_enterprise=" . $vflIndividualProvider->enterprise->idEnterprise . " ";
            $vsql.="AND id_provider=" . $vflIndividualProvider->idProvider;
            
            $vmySql->startTransaction();
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLProvider::deleteInDataBase($vflIndividualProvider, $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    return -1;
                }
            }
            else{
                $vmySql->rollbackTransaction();
                return 0;
            }
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function queryToDataBase($vflIndividualProvider, $vmySql)
	 {
		try{
            $vsql ="SELECT c_individualprovider.*, c_provider.*, c_persontype.fldpersonType, c_individualprovidermunicipality.fldmunicipality";
            $vsql.=", c_individualproviderstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_individualprovider ";
            $vsql.="INNER JOIN c_provider ON c_individualprovider.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND c_individualprovider.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise ON c_provider.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_individualprovidermunicipality ON c_provider.id_state=c_individualprovidermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_individualprovidermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_individualproviderstate ON c_individualprovidermunicipality.id_state=c_individualproviderstate.id_state ";
            $vsql.="WHERE c_individualprovider.id_enterprise=" . $vflIndividualProvider->enterprise->idEnterprise . " ";
            $vsql.="AND c_individualprovider.id_provider=" . $vflIndividualProvider->idProvider;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflIndividualProvider->name=trim($vrow["c_individualprovider.fldname"]);
                $vflIndividualProvider->firstName=trim($vrow["c_individualprovider.fldfirstName"]);
                $vflIndividualProvider->lastName=trim($vrow["c_individualprovider.fldlastName"]);
                $vflIndividualProvider->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflIndividualProvider->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflIndividualProvider->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflIndividualProvider->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflIndividualProvider->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflIndividualProvider->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflIndividualProvider->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflIndividualProvider->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflIndividualProvider->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflIndividualProvider->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflIndividualProvider->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflIndividualProvider->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflIndividualProvider->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflIndividualProvider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
                $vflIndividualProvider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflIndividualProvider->municipality->state->idState=(int)($vrow["c_provider.id_state"]);
                $vflIndividualProvider->municipality->state->state=trim($vrow["c_individualproviderstate.fldstate"]);
                $vflIndividualProvider->municipality->idMunicipality=(int)($vrow["c_provider.id_municipality"]);
                $vflIndividualProvider->municipality->municipality=trim($vrow["c_individualprovidermunicipality.fldmunicipality"]);
                $vflIndividualProvider->key=trim($vrow["c_provider.fldkey"]);
                $vflIndividualProvider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vflIndividualProvider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vflIndividualProvider->locality=trim($vrow["c_provider.fldlocality"]);
                $vflIndividualProvider->street=trim($vrow["c_provider.fldstreet"]);
                $vflIndividualProvider->number=trim($vrow["c_provider.fldnumber"]);
                $vflIndividualProvider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vflIndividualProvider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vflIndividualProvider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vflIndividualProvider->email=trim($vrow["c_provider.fldemail"]);
                $vflIndividualProvider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vflIndividualProvider->observation=trim($vrow["c_provider.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     	
	public function __destruct(){ }
 }

?>