<?php

class clspDLCashType
 {
	public function __construct() { }
    
    
	public static function queryToDataBase($vflCashType, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_cashtype ";
            $vsql.="WHERE id_cashType=" . $vflCashType->idCashType;
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflCashType->cashType=trim($vrow["fldcashType"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
	public function __destruct(){ }
 }

?>