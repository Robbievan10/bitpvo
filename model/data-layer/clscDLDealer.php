<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLDealer.php");


class clscDLDealer
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflDeliverers, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_dealer.*, c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate, c_enterprise.*";
            $vsql.=", c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_dealer ";
            $vsql.="INNER JOIN c_enterprise ON c_dealer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_dealer.fldfirstName, c_dealer.fldlastName, c_dealer.fldname";
            
            self::clean($vflDeliverers);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vdealer= new clspFLDealer();
                $vdealer->enterprise->idEnterprise=(int)($vrow["c_dealer.id_enterprise"]);
                $vdealer->idDealer=(int)($vrow["c_dealer.id_dealer"]);
                $vdealer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vdealer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vdealer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vdealer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vdealer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vdealer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vdealer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vdealer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vdealer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vdealer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vdealer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vdealer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vdealer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vdealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vdealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vdealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vdealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vdealer->key=trim($vrow["c_dealer.fldkey"]);
                $vdealer->name=trim($vrow["c_dealer.fldname"]);
                $vdealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vdealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vdealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vdealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vdealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vdealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vdealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vdealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vdealer->observation=trim($vrow["c_dealer.fldobservation"]);
                
                self::add($vflDeliverers, $vdealer);
                unset($vrow, $vdealer);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflDeliverers, $vdealer)
	 {
        try{
            array_push($vflDeliverers->deliverers, $vdealer);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflDeliverers)
	 {
        try{
            return count($vflDeliverers->deliverers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflDeliverers)
	 {
        try{
            $vflDeliverers->deliverers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>