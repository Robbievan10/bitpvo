<?php

require_once (dirname(dirname(__FILE__)) . "/tools/clspString.php");


class clspDLProductEntry
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductEntry, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflProductEntry->idProductEntry=self::getIdFromDataBase($vflProductEntry, $vmySql);
            
            $vsql ="INSERT INTO p_productentry(id_enterprise, id_productEntry, id_provider, id_operationType, id_operationStatus, fldsaleFolio";
            $vsql.=", fldrecordDate, fldproductEntryDate) ";
			$vsql.="VALUES(" . $vflProductEntry->enterprise->idEnterprise;
            $vsql.=", '" . $vflProductEntry->idProductEntry. "'";
			$vsql.=", " . $vflProductEntry->provider->idProvider;
            $vsql.=", " . $vflProductEntry->operationType->idOperationType;
            $vsql.=", " . $vflProductEntry->operationStatus->idOperationStatus;
			$vsql.=", '" . $vflProductEntry->saleFolio . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflProductEntry->recordDate)) . "'";
			$vsql.=", '" . date("Y-m-d", strtotime($vflProductEntry->productEntryDate)). "')";
            
			$vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductEntry, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_productentry ";
            $vsql.="SET id_provider=" . $vflProductEntry->provider->idProvider;
			$vsql.=", id_operationType=" . $vflProductEntry->operationType->idOperationType;
            $vsql.=", id_operationStatus=" . $vflProductEntry->operationStatus->idOperationStatus;
            $vsql.=", fldsaleFolio='" . $vflProductEntry->saleFolio. "'";
            $vsql.=", fldproductEntryDate='" . date("Y-m-d", strtotime($vflProductEntry->productEntryDate)) . "' ";
			$vsql.="WHERE id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
            $vsql.="AND id_productEntry='" . $vflProductEntry->idProductEntry . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
    public static function updateStatusInDataBase($vflProductEntry, $vmySql)
	 {
		try{
			$vsql ="UPDATE p_productentry ";
            $vsql.="SET id_operationStatus=" . $vflProductEntry->operationStatus->idOperationStatus . " ";
			$vsql.="WHERE id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
            $vsql.="AND id_productEntry='" . $vflProductEntry->idProductEntry . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function deleteInDataBase($vflProductEntry, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_productentry ";
            $vsql.="WHERE id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
            $vsql.="AND id_productEntry='" . $vflProductEntry->idProductEntry . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflProductEntry, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productentry.*, c_productentryenterprise.*, c_productentryenterprisemunicipality.fldmunicipality, c_productentryenterprisestate.fldstate";
            $vsql.=", c_provider.*, c_providerenterprise.*, c_providerenterprisemunicipality.fldmunicipality, c_providerenterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_providermunicipality.fldmunicipality, c_providerstate.fldstate, c_operationtype.fldoperationType";
            $vsql.=", c_operationstatus.fldoperationStatus ";
            $vsql.="FROM p_productentry ";
			$vsql.="INNER JOIN c_enterprise AS c_productentryenterprise ON p_productentry.id_enterprise=c_productentryenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productentryenterprisemunicipality ON c_productentryenterprise.id_state=c_productentryenterprisemunicipality.id_state ";
            $vsql.="AND c_productentryenterprise.id_municipality=c_productentryenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productentryenterprisestate ON c_productentryenterprisemunicipality.id_state=c_productentryenterprisestate.id_state ";
            $vsql.="INNER JOIN c_provider ON p_productentry.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND p_productentry.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise AS c_providerenterprise ON c_provider.id_enterprise=c_providerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_providerenterprisemunicipality ON c_providerenterprise.id_state=c_providerenterprisemunicipality.id_state ";
            $vsql.="AND c_providerenterprise.id_municipality=c_providerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_providerenterprisestate ON c_providerenterprisemunicipality.id_state=c_providerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_providermunicipality ON c_provider.id_state=c_providermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_providermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_providerstate ON c_providermunicipality.id_state=c_providerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productentry.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productentry.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="WHERE p_productentry.id_enterprise=" . $vflProductEntry->enterprise->idEnterprise . " ";
            $vsql.="AND p_productentry.id_productEntry='" . $vflProductEntry->idProductEntry . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflProductEntry->enterprise->municipality->state->idState=(int)($vrow["c_productentryenterprise.id_state"]);
                $vflProductEntry->enterprise->municipality->state->state=trim($vrow["c_productentryenterprisestate.fldstate"]);
                $vflProductEntry->enterprise->municipality->idMunicipality=(int)($vrow["c_productentryenterprise.id_municipality"]);
                $vflProductEntry->enterprise->municipality->municipality=trim($vrow["c_productentryenterprisemunicipality.fldmunicipality"]);
                $vflProductEntry->enterprise->enterprise=trim($vrow["c_productentryenterprise.fldenterprise"]);
                $vflProductEntry->enterprise->locality=trim($vrow["c_productentryenterprise.fldlocality"]);
                $vflProductEntry->enterprise->street=trim($vrow["c_productentryenterprise.fldstreet"]);
                $vflProductEntry->enterprise->number=trim($vrow["c_productentryenterprise.fldnumber"]);
                $vflProductEntry->enterprise->phoneNumber=trim($vrow["c_productentryenterprise.fldphoneNumber"]);
                $vflProductEntry->enterprise->movilNumber=trim($vrow["c_productentryenterprise.fldmovilNumber"]);
                $vflProductEntry->enterprise->pageWeb=trim($vrow["c_productentryenterprise.fldpageWeb"]);
                $vflProductEntry->provider->idProvider=(int)($vrow["p_productentry.id_provider"]);
                $vflProductEntry->provider->enterprise->idEnterprise=(int)($vrow["p_productentry.id_enterprise"]);
                $vflProductEntry->provider->enterprise->municipality->state->idState=(int)($vrow["c_providerenterprise.id_state"]);
                $vflProductEntry->provider->enterprise->municipality->state->state=trim($vrow["c_providerenterprisestate.fldstate"]);
                $vflProductEntry->provider->enterprise->municipality->idMunicipality=(int)($vrow["c_providerenterprise.id_municipality"]);
                $vflProductEntry->provider->enterprise->municipality->municipality=trim($vrow["c_providerenterprisemunicipality.fldmunicipality"]);
                $vflProductEntry->provider->enterprise->enterprise=trim($vrow["c_providerenterprise.fldenterprise"]);
                $vflProductEntry->provider->enterprise->locality=trim($vrow["c_providerenterprise.fldlocality"]);
                $vflProductEntry->provider->enterprise->street=trim($vrow["c_providerenterprise.fldstreet"]);
                $vflProductEntry->provider->enterprise->number=trim($vrow["c_providerenterprise.fldnumber"]);                
                $vflProductEntry->provider->enterprise->phoneNumber=trim($vrow["c_providerenterprise.fldphoneNumber"]);
                $vflProductEntry->provider->enterprise->movilNumber=trim($vrow["c_providerenterprise.fldmovilNumber"]);
                $vflProductEntry->provider->enterprise->pageWeb=trim($vrow["c_providerenterprise.fldpageWeb"]);
				$vflProductEntry->provider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
				$vflProductEntry->provider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflProductEntry->provider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vflProductEntry->provider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vflProductEntry->provider->locality=trim($vrow["c_provider.fldlocality"]);
                $vflProductEntry->provider->street=trim($vrow["c_provider.fldstreet"]);
                $vflProductEntry->provider->number=trim($vrow["c_provider.fldnumber"]);
                $vflProductEntry->provider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vflProductEntry->provider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vflProductEntry->provider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vflProductEntry->provider->email=trim($vrow["c_provider.fldemail"]);
                $vflProductEntry->provider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vflProductEntry->provider->observation=trim($vrow["c_provider.fldobservation"]);
                $vflProductEntry->operationType->idOperationType=(int)($vrow["p_productentry.id_operationType"]);
                $vflProductEntry->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vflProductEntry->operationStatus->idOperationStatus=(int)($vrow["p_productentry.id_operationStatus"]);
                $vflProductEntry->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vflProductEntry->saleFolio=trim($vrow["p_productentry.fldsaleFolio"]);
                $vflProductEntry->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldrecordDate"])));
                $vflProductEntry->productEntryDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldproductEntryDate"])));
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProductEntry, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vidProductEntry="000001-". date("y"); ;
			$vsql ="SELECT MAX(CONVERT(SUBSTRING(id_productEntry, 1, 6), SIGNED)) + 1 id_productEntryNew ";
			$vsql.="FROM p_productentry ";
            $vsql.="WHERE id_enterprise=" . $vflProductEntry->enterprise->idEnterprise;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productEntryNew"])>0) ){
                $vstring= new clspString($vrow["id_productEntryNew"]);
                $vidProductEntry=$vstring->getAdjustedString(6) . "-" . date("y");;
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProductEntry;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>