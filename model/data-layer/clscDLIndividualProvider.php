<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLIndividualProvider.php");


class clscDLIndividualProvider
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflIndividualsProviders, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_individualprovider.*, c_provider.*, c_persontype.fldpersonType, c_individualprovidermunicipality.fldmunicipality";
            $vsql.=", c_individualproviderstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_individualprovider ";
            $vsql.="INNER JOIN c_provider ON c_individualprovider.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND c_individualprovider.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise ON c_provider.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_individualprovidermunicipality ON c_provider.id_state=c_individualprovidermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_individualprovidermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_individualproviderstate ON c_individualprovidermunicipality.id_state=c_individualproviderstate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_individualprovider.fldfirstName, c_individualprovider.fldlastName, c_individualprovider.fldname";
            
            self::clean($vflIndividualsProviders);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vindividualProvider= new clspFLIndividualProvider();
                $vindividualProvider->name=trim($vrow["c_individualprovider.fldname"]);
                $vindividualProvider->firstName=trim($vrow["c_individualprovider.fldfirstName"]);
                $vindividualProvider->lastName=trim($vrow["c_individualprovider.fldlastName"]);
                $vindividualProvider->idProvider=(int)($vrow["c_provider.id_provider"]);
                $vindividualProvider->enterprise->idEnterprise=(int)($vrow["c_provider.id_enterprise"]);
                $vindividualProvider->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vindividualProvider->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vindividualProvider->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vindividualProvider->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vindividualProvider->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vindividualProvider->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vindividualProvider->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vindividualProvider->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vindividualProvider->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vindividualProvider->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vindividualProvider->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vindividualProvider->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vindividualProvider->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vindividualProvider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
                $vindividualProvider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vindividualProvider->municipality->state->idState=(int)($vrow["c_provider.id_state"]);
                $vindividualProvider->municipality->state->state=trim($vrow["c_individualproviderstate.fldstate"]);
                $vindividualProvider->municipality->idMunicipality=(int)($vrow["c_provider.id_municipality"]);
                $vindividualProvider->municipality->municipality=trim($vrow["c_individualprovidermunicipality.fldmunicipality"]);
                $vindividualProvider->key=trim($vrow["c_provider.fldkey"]);
                $vindividualProvider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vindividualProvider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vindividualProvider->locality=trim($vrow["c_provider.fldlocality"]);
                $vindividualProvider->street=trim($vrow["c_provider.fldstreet"]);
                $vindividualProvider->number=trim($vrow["c_provider.fldnumber"]);
                $vindividualProvider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vindividualProvider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vindividualProvider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vindividualProvider->email=trim($vrow["c_provider.fldemail"]);
                $vindividualProvider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vindividualProvider->observation=trim($vrow["c_provider.fldobservation"]);
                
                self::add($vflIndividualsProviders, $vindividualProvider);
                unset($vrow, $vindividualProvider);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflIndividualsProviders, $vindividualProvider)
	 {
        try{
            array_push($vflIndividualsProviders->individualsProviders, $vindividualProvider);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflIndividualsProviders)
	 {
        try{
            return count($vflIndividualsProviders->individualsProviders);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflIndividualsProviders)
	 {
        try{
            $vflIndividualsProviders->individualsProviders=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>