<?php

class clspDLCashCount
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflCashCount, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflCashCount->idCashCount=self::getIdFromDataBase($vflCashCount, $vmySql);
            
            $vsql ="INSERT INTO p_cashcount(id_enterprise, id_cash, id_cashCount, id_userAuditor, id_userCashier, fldrecordDate, fldobservation) ";
			$vsql.="VALUES(" . $vflCashCount->cash->enterprise->idEnterprise;
            $vsql.=", " . $vflCashCount->cash->idCash;
            $vsql.=", " . $vflCashCount->idCashCount;
            $vsql.=", '" . $vflCashCount->userAuditor->idUser . "'";
            $vsql.=", '" . $vflCashCount->userCashier->idUser . "'";
			$vsql.=", '" . date("Y-m-d", strtotime($vflCashCount->recordDate)) . "'";
            $vsql.=", '" . $vflCashCount->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflCashCount, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_cashcount ";
            $vsql.="SET id_userAuditor='" . $vflCashCount->userAuditor->idUser . "'";
            $vsql.=", id_userCashier='" . $vflCashCount->userCashier->idUser . "'";
            $vsql.=", fldrecordDate='" . date("Y-m-d", strtotime($vflCashCount->recordDate)) . "' ";
            $vsql.=", fldobservation='" . $vflCashCount->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashCount->cash->idCash . " ";
            $vsql.="AND id_cashCount=" . $vflCashCount->idCashCount;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflCashCount, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_cashcount ";
            $vsql.="WHERE id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashCount->cash->idCash . " ";
            $vsql.="AND id_cashCount=" . $vflCashCount->idCashCount;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflCashCount, $vmySql)
	 {
		try{
			$vsql ="SELECT p_cashcount.*, c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_userauditor.*";
            $vsql.=", c_userauditortype.flduserType, c_userauditorstatus.flduserStatus, c_usercashier.*, c_usercashiertype.flduserType";
            $vsql.=", c_usercashierstatus.flduserStatus ";
            $vsql.="FROM p_cashcount ";
            $vsql.="INNER JOIN c_cash ON p_cashcount.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_cashcount.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseuserauditor ON p_cashcount.id_userAuditor=c_enterpriseuserauditor.id_user ";
            $vsql.="INNER JOIN c_user AS c_userauditor ON c_enterpriseuserauditor.id_user=c_userauditor.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_userauditortype ON c_userauditor.id_userType=c_userauditortype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_userauditorstatus ON c_userauditor.id_userStatus=c_userauditorstatus.id_userStatus ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseusercashier ON p_cashcount.id_userCashier=c_enterpriseusercashier.id_user ";
            $vsql.="INNER JOIN c_user AS c_usercashier ON c_enterpriseusercashier.id_user=c_usercashier.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_usercashiertype ON c_usercashier.id_userType=c_usercashiertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_usercashierstatus ON c_usercashier.id_userStatus=c_usercashierstatus.id_userStatus ";
            $vsql.="WHERE p_cashcount.id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND p_cashcount.id_cash=" . $vflCashCount->cash->idCash . " ";
            $vsql.="AND p_cashcount.id_cashCount=" . $vflCashCount->idCashCount;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflCashCount->cash->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflCashCount->cash->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vflCashCount->cash->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflCashCount->cash->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vflCashCount->cash->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflCashCount->cash->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflCashCount->cash->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflCashCount->cash->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflCashCount->cash->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflCashCount->cash->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflCashCount->cash->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflCashCount->cash->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflCashCount->cash->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflCashCount->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vflCashCount->userAuditor->idUser=trim($vrow["p_cashcount.id_userAuditor"]);
                $vflCashCount->userAuditor->enterprise=$vflCashCount->cash->enterprise;
                $vflCashCount->userAuditor->userType->idUserType=(int)($vrow["c_userauditor.id_userType"]);
				$vflCashCount->userAuditor->userType->userType=trim($vrow["c_userauditortype.flduserType"]);
				$vflCashCount->userAuditor->userStatus->idUserStatus=(int)($vrow["c_userauditor.id_userStatus"]);
				$vflCashCount->userAuditor->userStatus->userStatus=trim($vrow["c_userauditorstatus.flduserStatus"]);
				$vflCashCount->userAuditor->firstName=trim($vrow["c_userauditor.fldfirstName"]);
				$vflCashCount->userAuditor->lastName=trim($vrow["c_userauditor.fldlastName"]);
				$vflCashCount->userAuditor->name=trim($vrow["c_userauditor.fldname"]);
                $vflCashCount->userCashier->idUser=trim($vrow["p_cashcount.id_userCashier"]);
                $vflCashCount->userCashier->enterprise=$vflCashCount->cash->enterprise;
                $vflCashCount->userCashier->userType->idUserType=(int)($vrow["c_usercashier.id_userType"]);
				$vflCashCount->userCashier->userType->userType=trim($vrow["c_usercashiertype.flduserType"]);
				$vflCashCount->userCashier->userStatus->idUserStatus=(int)($vrow["c_usercashier.id_userStatus"]);
				$vflCashCount->userCashier->userStatus->userStatus=trim($vrow["c_usercashierstatus.flduserStatus"]);
				$vflCashCount->userCashier->firstName=trim($vrow["c_usercashier.fldfirstName"]);
				$vflCashCount->userCashier->lastName=trim($vrow["c_usercashier.fldlastName"]);
				$vflCashCount->userCashier->name=trim($vrow["c_usercashier.fldname"]);
                $vflCashCount->recordDate=date("m/d/Y", strtotime(trim($vrow["p_cashcount.fldrecordDate"])));
                $vflCashCount->observation=trim($vrow["p_cashcount.fldobservation"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflCashCount, $vmySql)
	 {
		try{
            $vidCashCount=1;
			$vsql ="SELECT MAX(id_cashCount) + 1 AS id_cashCountNew ";
			$vsql.="FROM p_cashcount ";
            $vsql.="WHERE id_enterprise=" . $vflCashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashCount->cash->idCash;
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_cashCountNew"])>0) ){
                $vidCashCount=(int)($vrow["id_cashCountNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidCashCount;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>