<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductDistribution.php");


class clscDLProductDistribution
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsDistributions, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productdistribution.*, c_productdistributionenterprise.*, c_productdistributionenterprisemunicipality.fldmunicipality";
			$vsql.=", c_productdistributionenterprisestate.fldstate, c_routedealer.*, c_route.*, c_routeenterprise.*";
			$vsql.=", c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate, c_dealer.*, c_dealerenterprise.*";
			$vsql.=", c_dealerenterprisemunicipality.fldmunicipality, c_dealerenterprisestate.fldstate, c_dealermunicipality.fldmunicipality";
			$vsql.=", c_dealerstate.fldstate ";
			$vsql.="FROM p_productdistribution ";
			$vsql.="INNER JOIN c_enterprise AS c_productdistributionenterprise ON p_productdistribution.id_enterprise=c_productdistributionenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productdistributionenterprisemunicipality ON c_productdistributionenterprise.id_state=c_productdistributionenterprisemunicipality.id_state ";
            $vsql.="AND c_productdistributionenterprise.id_municipality=c_productdistributionenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productdistributionenterprisestate ON c_productdistributionenterprisemunicipality.id_state=c_productdistributionenterprisestate.id_state ";
            $vsql.="INNER JOIN c_routedealer ON p_productdistribution.id_enterprise=c_routedealer.id_enterprise ";
            $vsql.="AND p_productdistribution.id_route=c_routedealer.id_route ";
			$vsql.="INNER JOIN c_route ON c_routedealer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routedealer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productdistribution.id_productDistribution";
            
            self::clean($vflProductsDistributions);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductDistribution= new clspFLProductDistribution();
                $vproductDistribution->idProductDistribution=trim($vrow["p_productdistribution.id_productDistribution"]);
                $vproductDistribution->CreatedOn=trim($vrow["p_productdistribution.CreatedOn"]);
                $vproductDistribution->enterprise->idEnterprise=(int)($vrow["p_productdistribution.id_enterprise"]);
                $vproductDistribution->enterprise->municipality->state->idState=(int)($vrow["c_productdistributionenterprise.id_state"]);
                $vproductDistribution->enterprise->municipality->state->state=trim($vrow["c_productdistributionenterprisestate.fldstate"]);
                $vproductDistribution->enterprise->municipality->idMunicipality=(int)($vrow["c_productdistributionenterprise.id_municipality"]);
                $vproductDistribution->enterprise->municipality->municipality=trim($vrow["c_productdistributionenterprisemunicipality.fldmunicipality"]);
                $vproductDistribution->enterprise->enterprise=trim($vrow["c_productdistributionenterprise.fldenterprise"]);
                $vproductDistribution->enterprise->locality=trim($vrow["c_productdistributionenterprise.fldlocality"]);
                $vproductDistribution->enterprise->street=trim($vrow["c_productdistributionenterprise.fldstreet"]);
                $vproductDistribution->enterprise->number=trim($vrow["c_productdistributionenterprise.fldnumber"]);
                $vproductDistribution->enterprise->phoneNumber=trim($vrow["c_productdistributionenterprise.fldphoneNumber"]);
                $vproductDistribution->enterprise->movilNumber=trim($vrow["c_productdistributionenterprise.fldmovilNumber"]);
                $vproductDistribution->enterprise->pageWeb=trim($vrow["c_productdistributionenterprise.fldpageWeb"]);
                $vproductDistribution->routeDealer->route->idRoute=(int)($vrow["p_productdistribution.id_route"]);
                $vproductDistribution->routeDealer->route->enterprise->idEnterprise=(int)($vrow["p_productdistribution.id_enterprise"]);
                $vproductDistribution->routeDealer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vproductDistribution->routeDealer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vproductDistribution->routeDealer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vproductDistribution->routeDealer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vproductDistribution->routeDealer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vproductDistribution->routeDealer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vproductDistribution->routeDealer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vproductDistribution->routeDealer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vproductDistribution->routeDealer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vproductDistribution->routeDealer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vproductDistribution->routeDealer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vproductDistribution->routeDealer->route->route=trim($vrow["c_route.fldroute"]);
                $vproductDistribution->routeDealer->route->description=trim($vrow["c_route.flddescription"]);
                $vproductDistribution->routeDealer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vproductDistribution->routeDealer->dealer->idDealer=(int)($vrow["c_routedealer.id_dealer"]);
                $vproductDistribution->routeDealer->dealer->enterprise->idEnterprise=(int)($vrow["c_routedealer.id_enterprise"]);
                $vproductDistribution->routeDealer->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vproductDistribution->routeDealer->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vproductDistribution->routeDealer->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vproductDistribution->routeDealer->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vproductDistribution->routeDealer->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vproductDistribution->routeDealer->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vproductDistribution->routeDealer->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vproductDistribution->routeDealer->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vproductDistribution->routeDealer->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vproductDistribution->routeDealer->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vproductDistribution->routeDealer->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vproductDistribution->routeDealer->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vproductDistribution->routeDealer->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vproductDistribution->routeDealer->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vproductDistribution->routeDealer->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vproductDistribution->routeDealer->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vproductDistribution->routeDealer->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vproductDistribution->routeDealer->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vproductDistribution->routeDealer->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vproductDistribution->routeDealer->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vproductDistribution->routeDealer->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vproductDistribution->routeDealer->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vproductDistribution->routeDealer->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vproductDistribution->routeDealer->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vproductDistribution->routeDealer->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                $vproductDistribution->routeDealer->assignDate=date("m/d/Y", strtotime(trim($vrow["c_routedealer.fldassignDate"])));
				$vproductDistribution->productDistributionRecordDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionRecordDate"])));
				$vproductDistribution->productDistributionDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionDate"])));
				$vproductDistribution->productDistributionCuttingDate=date("m/d/Y", strtotime(trim($vrow["p_productdistribution.fldproductDistributionCuttingDate"])));
				$vproductDistribution->creditTotal=(float)($vrow["p_productdistribution.fldcreditTotal"]);
				$vproductDistribution->discountTotal=(float)($vrow["p_productdistribution.flddiscountTotal"]);
                $vproductDistribution->devolutionTotal=(float)($vrow["p_productdistribution.flddevolutionTotal"]);
                $vproductDistribution->observation=trim($vrow["p_productdistribution.fldobservation"]);
				
                self::add($vflProductsDistributions, $vproductDistribution);
                unset($vrow, $vproductDistribution);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflProductsDistributions, $vproductDistribution)
	 {
        try{
            array_push($vflProductsDistributions->productsDistributions, $vproductDistribution);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsDistributions)
	 {
        try{
            return count($vflProductsDistributions->productsDistributions);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsDistributions)
	 {
        try{
            $vflProductsDistributions->productsDistributions=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>