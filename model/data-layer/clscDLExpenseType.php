<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLExpenseType.php");


class clscDLExpenseType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflExpenseTypes, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_expensetype.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_expensetype ";
            $vsql.="INNER JOIN c_enterprise ON c_expensetype.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY fldexpenseType";
            
            self::clean($vflExpenseTypes);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vexpenseType= new clspFLExpenseType();
                $vexpenseType->idExpenseType=(int)($vrow["id_expenseType"]);
                $vexpenseType->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vexpenseType->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vexpenseType->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vexpenseType->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vexpenseType->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vexpenseType->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vexpenseType->enterprise->locality=trim($vrow["fldlocality"]);
                $vexpenseType->enterprise->street=trim($vrow["fldstreet"]);
                $vexpenseType->enterprise->number=trim($vrow["fldnumber"]);
                $vexpenseType->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vexpenseType->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vexpenseType->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vexpenseType->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vexpenseType->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vexpenseType->expenseType=trim($vrow["fldexpenseType"]);
                
                self::add($vflExpenseTypes, $vexpenseType);
                unset($vrow, $vexpenseType);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflExpenseTypes, $vexpenseType)
	 {
        try{
            array_push($vflExpenseTypes->expenseTypes, $vexpenseType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflExpenseTypes)
	 {
        try{
            return count($vflExpenseTypes->expenseTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflExpenseTypes)
	 {
        try{
            $vflExpenseTypes->expenseTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>