<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductSalePayment.php");


class clscDLProductSalePayment
 {
    public function __construct() { }


    public static function queryToDataBase($vflProductsSalesPayments, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productsalepayment.*, p_productsale.*, c_productsaleenterprise.*, c_productsaleenterprisemunicipality.fldmunicipality";
            $vsql.=", c_productsaleenterprisestate.fldstate, c_customer.*, c_customerenterprise.*, c_customerenterprisemunicipality.fldmunicipality";
            $vsql.=", c_customerenterprisestate.fldstate, c_persontype.fldpersonType, c_customermunicipality.fldmunicipality, c_customerstate.fldstate";
            $vsql.=", c_operationtype.fldoperationType, c_operationstatus.fldoperationStatus, p_payment.*, p_paymententerprise.*";
            $vsql.=", p_paymententerprisemunicipality.fldmunicipality, p_paymententerprisestate.fldstate, c_paymenttype.fldpaymentType ";
            $vsql.="FROM p_productsalepayment ";
			$vsql.="INNER JOIN p_productsale ON p_productsalepayment.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsalepayment.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_enterprise AS c_productsaleenterprise ON p_productsale.id_enterprise=c_productsaleenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productsaleenterprisemunicipality ON c_productsaleenterprise.id_state=c_productsaleenterprisemunicipality.id_state ";
            $vsql.="AND c_productsaleenterprise.id_municipality=c_productsaleenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productsaleenterprisestate ON c_productsaleenterprisemunicipality.id_state=c_productsaleenterprisestate.id_state ";
            $vsql.="INNER JOIN c_customer ON p_productsale.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise AS c_customerenterprise ON c_customer.id_enterprise=c_customerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_customerenterprisemunicipality ON c_customerenterprise.id_state=c_customerenterprisemunicipality.id_state ";
            $vsql.="AND c_customerenterprise.id_municipality=c_customerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_customerenterprisestate ON c_customerenterprisemunicipality.id_state=c_customerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productsale.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="INNER JOIN p_payment ON p_productsalepayment.id_enterprise=p_payment.id_enterprise ";
            $vsql.="AND p_productsalepayment.id_payment=p_payment.id_payment ";
            $vsql.="INNER JOIN c_enterprise AS p_paymententerprise ON p_payment.id_enterprise=p_paymententerprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS p_paymententerprisemunicipality ON p_paymententerprise.id_state=p_paymententerprisemunicipality.id_state ";
            $vsql.="AND p_paymententerprise.id_municipality=p_paymententerprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS p_paymententerprisestate ON p_paymententerprisemunicipality.id_state=p_paymententerprisestate.id_state ";
			$vsql.="INNER JOIN c_paymenttype ON p_payment.id_paymentType=c_paymenttype.id_paymentType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productsalepayment.id_enterprise, p_productsalepayment.id_productSale, p_productsalepayment.id_payment";

            self::clean($vflProductsSalesPayments);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductSalePayment= new clspFLProductSalePayment();
				$vproductSalePayment->idPayment=(int)($vrow["p_productsalepayment.id_payment"]);
                $vproductSalePayment->productSale->idProductSale=trim($vrow["p_productsalepayment.id_productSale"]);
                $vproductSalePayment->productSale->enterprise->idEnterprise=(int)($vrow["p_productsalepayment.id_enterprise"]);
                $vproductSalePayment->productSale->enterprise->municipality->state->idState=(int)($vrow["c_productsaleenterprise.id_state"]);
                $vproductSalePayment->productSale->enterprise->municipality->state->state=trim($vrow["c_productsaleenterprisestate.fldstate"]);
                $vproductSalePayment->productSale->enterprise->municipality->idMunicipality=(int)($vrow["c_productsaleenterprise.id_municipality"]);
                $vproductSalePayment->productSale->enterprise->municipality->municipality=trim($vrow["c_productsaleenterprisemunicipality.fldmunicipality"]);
                $vproductSalePayment->productSale->enterprise->enterprise=trim($vrow["c_productsaleenterprise.fldenterprise"]);
                $vproductSalePayment->productSale->enterprise->locality=trim($vrow["c_productsaleenterprise.fldlocality"]);
                $vproductSalePayment->productSale->enterprise->street=trim($vrow["c_productsaleenterprise.fldstreet"]);
                $vproductSalePayment->productSale->enterprise->number=trim($vrow["c_productsaleenterprise.fldnumber"]);
                $vproductSalePayment->productSale->enterprise->phoneNumber=trim($vrow["c_productsaleenterprise.fldphoneNumber"]);
                $vproductSalePayment->productSale->enterprise->movilNumber=trim($vrow["c_productsaleenterprise.fldmovilNumber"]);
                $vproductSalePayment->productSale->enterprise->pageWeb=trim($vrow["c_productsaleenterprise.fldpageWeb"]);
				$vproductSalePayment->productSale->customer->idCustomer=(int)($vrow["p_productsale.id_customer"]);
                $vproductSalePayment->productSale->customer->enterprise->idEnterprise=(int)($vrow["p_productsale.id_enterprise"]);
                $vproductSalePayment->productSale->customer->enterprise->municipality->state->idState=(int)($vrow["c_customerenterprise.id_state"]);
                $vproductSalePayment->productSale->customer->enterprise->municipality->state->state=trim($vrow["c_customerenterprisestate.fldstate"]);
                $vproductSalePayment->productSale->customer->enterprise->municipality->idMunicipality=(int)($vrow["c_customerenterprise.id_municipality"]);
                $vproductSalePayment->productSale->customer->enterprise->municipality->municipality=trim($vrow["c_customerenterprisemunicipality.fldmunicipality"]);
                $vproductSalePayment->productSale->customer->enterprise->enterprise=trim($vrow["c_customerenterprise.fldenterprise"]);
                $vproductSalePayment->productSale->customer->enterprise->locality=trim($vrow["c_customerenterprise.fldlocality"]);
                $vproductSalePayment->productSale->customer->enterprise->street=trim($vrow["c_customerenterprise.fldstreet"]);
                $vproductSalePayment->productSale->customer->enterprise->number=trim($vrow["c_customerenterprise.fldnumber"]);
                $vproductSalePayment->productSale->customer->enterprise->phoneNumber=trim($vrow["c_customerenterprise.fldphoneNumber"]);
                $vproductSalePayment->productSale->customer->enterprise->movilNumber=trim($vrow["c_customerenterprise.fldmovilNumber"]);
                $vproductSalePayment->productSale->customer->enterprise->pageWeb=trim($vrow["c_customerenterprise.fldpageWeb"]);
				$vproductSalePayment->productSale->customer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vproductSalePayment->productSale->customer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vproductSalePayment->productSale->customer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vproductSalePayment->productSale->customer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vproductSalePayment->productSale->customer->locality=trim($vrow["c_customer.fldlocality"]);
                $vproductSalePayment->productSale->customer->street=trim($vrow["c_customer.fldstreet"]);
                $vproductSalePayment->productSale->customer->number=trim($vrow["c_customer.fldnumber"]);
                $vproductSalePayment->productSale->customer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vproductSalePayment->productSale->customer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vproductSalePayment->productSale->customer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vproductSalePayment->productSale->customer->email=trim($vrow["c_customer.fldemail"]);
                $vproductSalePayment->productSale->customer->observation=trim($vrow["c_customer.fldobservation"]);
				$vproductSalePayment->productSale->operationType->idOperationType=(int)($vrow["p_productsale.id_operationType"]);
                $vproductSalePayment->productSale->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vproductSalePayment->productSale->operationStatus->idOperationStatus=(int)($vrow["p_productsale.id_operationStatus"]);
                $vproductSalePayment->productSale->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vproductSalePayment->productSale->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldrecordDate"])));
                $vproductSalePayment->productSale->productSaleDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldproductSaleDate"])));
				$vproductSalePayment->productSale->canceled=(int)($vrow["p_productsale.fldcanceled"]);
				$vproductSalePayment->enterprise->idEnterprise=(int)($vrow["p_productsalepayment.id_enterprise"]);
				$vproductSalePayment->enterprise->municipality->state->idState=(int)($vrow["p_paymententerprise.id_state"]);
                $vproductSalePayment->enterprise->municipality->state->state=trim($vrow["p_paymententerprisestate.fldstate"]);
                $vproductSalePayment->enterprise->municipality->idMunicipality=(int)($vrow["p_paymententerprise.id_municipality"]);
                $vproductSalePayment->enterprise->municipality->municipality=trim($vrow["p_paymententerprisemunicipality.fldmunicipality"]);
                $vproductSalePayment->enterprise->enterprise=trim($vrow["p_paymententerprise.fldenterprise"]);
                $vproductSalePayment->enterprise->locality=trim($vrow["p_paymententerprise.fldlocality"]);
                $vproductSalePayment->enterprise->street=trim($vrow["p_paymententerprise.fldstreet"]);
                $vproductSalePayment->enterprise->number=trim($vrow["p_paymententerprise.fldnumber"]);
                $vproductSalePayment->enterprise->phoneNumber=trim($vrow["p_paymententerprise.fldphoneNumber"]);
                $vproductSalePayment->enterprise->movilNumber=trim($vrow["p_paymententerprise.fldmovilNumber"]);
                $vproductSalePayment->enterprise->pageWeb=trim($vrow["p_paymententerprise.fldpageWeb"]);
				$vproductSalePayment->paymentType->idPaymentType=(int)($vrow["p_payment.id_paymentType"]);
				$vproductSalePayment->paymentType->paymentType=trim($vrow["c_paymenttype.fldpaymentType"]);
				$vproductSalePayment->recordDate=date("m/d/Y", strtotime(trim($vrow["p_payment.fldrecordDate"])));
				$vproductSalePayment->paymentDate=date("m/d/Y", strtotime(trim($vrow["p_payment.fldpaymentDate"])));
				$vproductSalePayment->amount=(float)($vrow["p_payment.fldamount"]);

                self::add($vflProductsSalesPayments, $vproductSalePayment);
                unset($vrow, $vproductSalePayment);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryByRouteToDataBase($vflProductsSalesPayments, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT SUM(p_payment.fldamount) AS amountTotal ";
            $vsql.="FROM p_productsalepayment ";
            $vsql.="INNER JOIN p_productsale ON p_productsalepayment.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsalepayment.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_routecustomer ON p_productsale.id_enterprise=c_routecustomer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_routecustomer.id_customer ";
            $vsql.="INNER JOIN p_payment ON p_productsalepayment.id_enterprise=p_payment.id_enterprise ";
            $vsql.="AND p_productsalepayment.id_payment=p_payment.id_payment ";
            $vsql.=$vfilter;

            self::clean($vflProductsSalesPayments);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSalePayment= new clspFLProductSalePayment();
                $vproductSalePayment->amount=(float)($vrow["amountTotal"]);
                //$vproductSalePayment->CreatedOn=trim($vrow["p_payment.CreatedOn"]);

                self::add($vflProductsSalesPayments, $vproductSalePayment);
                unset($vrow, $vflProductsSalesPayments);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryGroupByCustomerToDataBase($vflProductsSalesPayments, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_productsale.id_customer, c_customer.id_personType";
            $vsql.=", IF(c_customer.id_personType=1, c_individualcustomer.fldname, c_businesscustomer.fldbusinessName) AS customerName";
            $vsql.=", IF(c_customer.id_personType=1, c_individualcustomer.fldfirstName, null) AS customerFirstName";
            $vsql.=", IF(c_customer.id_personType=1, c_individualcustomer.fldlastName, null) AS customerLastName";
            $vsql.=", SUM(p_payment.fldamount) AS amountTotal ";
            $vsql.="FROM p_productsalepayment ";
            $vsql.="INNER JOIN p_productsale ON p_productsalepayment.id_enterprise=p_productsale.id_enterprise ";
            $vsql.="AND p_productsalepayment.id_productSale=p_productsale.id_productSale ";
            $vsql.="INNER JOIN c_customer ON p_productsale.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_customer.id_customer ";
            $vsql.="LEFT JOIN c_individualcustomer ON c_customer.id_enterprise=c_individualcustomer.id_enterprise ";
            $vsql.="AND c_customer.id_customer=c_individualcustomer.id_customer ";
            $vsql.="LEFT JOIN c_businesscustomer ON c_customer.id_enterprise=c_businesscustomer.id_enterprise ";
            $vsql.="AND c_customer.id_customer=c_businesscustomer.id_customer ";
            $vsql.="INNER JOIN p_payment ON p_productsalepayment.id_enterprise=p_payment.id_enterprise ";
            $vsql.="AND p_productsalepayment.id_payment=p_payment.id_payment ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_productsale.id_customer ";
            $vsql.="ORDER BY customerName, customerFirstName, customerLastName";

            self::clean($vflProductsSalesPayments);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductSalePayment= new clspFLProductSalePayment();
                $vproductSalePayment->productSale->customer->idCustomer=(int)($vrow["id_customer"]);
                $vproductSalePayment->productSale->customer->personType->idPersonType=(int)($vrow["id_personType"]);
                $vproductSalePayment->productSale->customer->name=trim($vrow["customerName"]);
                if ( $vproductSalePayment->productSale->customer->personType->idPersonType==1 ){
                    $vproductSalePayment->productSale->customer->firstName=trim($vrow["customerFirstName"]);
                    $vproductSalePayment->productSale->customer->lastName=trim($vrow["customerLastName"]);
                }
                $vproductSalePayment->amount=(float)($vrow["amountTotal"]);

                self::add($vflProductsSalesPayments, $vproductSalePayment);
                unset($vrow, $vproductSalePayment);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function add($vflProductsSalesPayments, $vproductSalePayment)
	 {
        try{
            array_push($vflProductsSalesPayments->productsSalesPayments, $vproductSalePayment);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function total($vflProductsSalesPayments)
	 {
        try{
            return count($vflProductsSalesPayments->productsSalesPayments);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function clean($vflProductsSalesPayments)
	 {
        try{
            $vflProductsSalesPayments->productsSalesPayments=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	 public static function totalAmount($vflProductsSalesPayments)
	 {
        try{
            $vtotalAmount=0;
            $vproductsSalesPaymentsTotal=self::total($vflProductsSalesPayments);
            for($vi=0; $vi<$vproductsSalesPaymentsTotal; $vi++){
                $vtotalAmount+=$vflProductsSalesPayments->productsSalesPayments[$vi]->amount;
            }
            return $vtotalAmount;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


    public function __destruct(){ }
 }

?>
