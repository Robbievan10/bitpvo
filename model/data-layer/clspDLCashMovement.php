<?php

class clspDLCashMovement
 {
	public function __construct(){ }
    
    
	public static function addOpenToDataBase($vflCashMovement, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflCashMovement->idCashMovement=self::getIdFromDataBase($vflCashMovement, $vmySql);
            
            $vsql ="INSERT INTO p_cashmovement(id_enterprise, id_cash, id_cashMovement, id_cashMovementStatus, id_user, fldrecordDate";
            $vsql.=", fldopenTime, fldopenAmount) ";
			$vsql.="VALUES(" . $vflCashMovement->cash->enterprise->idEnterprise;
            $vsql.=", " . $vflCashMovement->cash->idCash;
            $vsql.=", " . $vflCashMovement->idCashMovement;
            $vsql.=", " . $vflCashMovement->cashMovementStatus->idCashMovementStatus;
            $vsql.=", '" . $vflCashMovement->user->idUser . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflCashMovement->recordDate)) . "'";
            $vsql.=", '" . date("H:i:s", strtotime($vflCashMovement->openTime)) . "'";
            $vsql.=", " . $vflCashMovement->openAmount . ")";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function recordCloseToDataBase($vflCashMovement, $vmySql)
	 {
		try{
		    date_default_timezone_set('America/Mexico_City');  
			$vsql ="UPDATE p_cashmovement ";
            $vsql.="SET id_cashMovementStatus=2";
            $vsql.=", fldcloseTime='" . date("H:i:s", strtotime($vflCashMovement->closeTime)) . "'";
            $vsql.=", fldcloseAmount=" . $vflCashMovement->closeAmount . " ";
            $vsql.="WHERE id_enterprise=" . $vflCashMovement->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashMovement->cash->idCash . " ";
            $vsql.="AND id_cashMovement=" . $vflCashMovement->idCashMovement;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflCashMovement, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_cashmovement ";
            $vsql.="WHERE id_enterprise=" . $vflCashMovement->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashMovement->cash->idCash . " ";
            $vsql.="AND id_cashMovement=" . $vflCashMovement->idCashMovement;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflCashMovement, $vmySql)
	 {
		try{
			$vsql ="SELECT p_cashmovement.*, c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate";
            $vsql.=", c_cashmovementstatus.fldcashMovementStatus, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.="FROM p_cashmovement ";
            $vsql.="INNER JOIN c_cash ON p_cashmovement.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_cashmovement.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_cashmovementstatus ON p_cashmovement.id_cashMovementStatus=c_cashmovementstatus.id_cashMovementStatus ";
            $vsql.="INNER JOIN c_enterpriseuser ON p_cashmovement.id_user=c_enterpriseuser.id_user ";
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.="WHERE p_cashmovement.id_enterprise=" . $vflCashMovement->cash->enterprise->idEnterprise . " ";
            $vsql.="AND p_cashmovement.id_cash=" . $vflCashMovement->cash->idCash . " ";
            $vsql.="AND p_cashmovement.id_cashMovement=" . $vflCashMovement->idCashMovement;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflCashMovement->cash->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflCashMovement->cash->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vflCashMovement->cash->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflCashMovement->cash->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vflCashMovement->cash->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflCashMovement->cash->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflCashMovement->cash->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflCashMovement->cash->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflCashMovement->cash->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflCashMovement->cash->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflCashMovement->cash->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflCashMovement->cash->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflCashMovement->cash->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflCashMovement->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vflCashMovement->cashMovementStatus->idCashMovementStatus=(int)($vrow["p_cashmovement.id_cashMovementStatus"]);
                $vflCashMovement->cashMovementStatus->cashMovementStatus=trim($vrow["c_cashmovementstatus.fldcashMovementStatus"]);
                $vflCashMovement->user->idUser=trim($vrow["p_cashmovement.id_user"]);
                $vflCashMovement->user->enterprise=$vflCashMovement->cash->enterprise;
                $vflCashMovement->user->userType->idUserType=(int)($vrow["c_user.id_userType"]);
				$vflCashMovement->user->userType->userType=trim($vrow["c_usertype.flduserType"]);
				$vflCashMovement->user->userStatus->idUserStatus=(int)($vrow["c_user.id_userStatus"]);
				$vflCashMovement->user->userStatus->userStatus=trim($vrow["c_userstatus.flduserStatus"]);
				$vflCashMovement->user->firstName=trim($vrow["c_user.fldfirstName"]);
				$vflCashMovement->user->lastName=trim($vrow["c_user.fldlastName"]);
				$vflCashMovement->user->name=trim($vrow["c_user.fldname"]);
                $vflCashMovement->recordDate=date("m/d/Y", strtotime(trim($vrow["p_cashmovement.fldrecordDate"])));
                $vflCashMovement->openTime=date("h:i:s a", strtotime(trim($vrow["p_cashmovement.fldopenTime"])));
                $vflCashMovement->openAmount=(float)($vrow["p_cashmovement.fldopenAmount"]);
                $vflCashMovement->closeTime=date("h:i:s a", strtotime(trim($vrow["p_cashmovement.fldcloseTime"])));
                $vflCashMovement->closeAmount=(float)($vrow["p_cashmovement.fldcloseAmount"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflCashMovement, $vmySql)
	 {
		try{
            $vidCashMovement=1;
			$vsql ="SELECT MAX(id_cashMovement) + 1 AS id_cashMovementNew ";
			$vsql.="FROM p_cashmovement ";
            $vsql.="WHERE id_enterprise=" . $vflCashMovement->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashMovement->cash->idCash;
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_cashMovementNew"])>0) ){
                $vidCashMovement=(int)($vrow["id_cashMovementNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidCashMovement;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>