<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashCountCash.php");
require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCashCountCash.php");


class clscDLCashCountCash
 {
    public function __construct() { }


    public static function queryToDataBase($vflCashCountCashList, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_cashcountcash.*, p_cashcount.*, c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_userauditor.*";
            $vsql.=", c_userauditortype.flduserType, c_userauditorstatus.flduserStatus, c_usercashier.*, c_usercashiertype.flduserType";
            $vsql.=", c_usercashierstatus.flduserStatus, c_cashtype.fldcashType ";
            $vsql.="FROM p_cashcountcash ";
            $vsql.="INNER JOIN p_cashcount ON p_cashcountcash.id_enterprise=p_cashcount.id_enterprise ";
            $vsql.="AND p_cashcountcash.id_cash=p_cashcount.id_cash ";
            $vsql.="AND p_cashcountcash.id_cashCount=p_cashcount.id_cashCount ";
            $vsql.="INNER JOIN c_cash ON p_cashcount.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_cashcount.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseuserauditor ON p_cashcount.id_userAuditor=c_enterpriseuserauditor.id_user ";
            $vsql.="INNER JOIN c_user AS c_userauditor ON c_enterpriseuserauditor.id_user=c_userauditor.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_userauditortype ON c_userauditor.id_userType=c_userauditortype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_userauditorstatus ON c_userauditor.id_userStatus=c_userauditorstatus.id_userStatus ";
            $vsql.="INNER JOIN c_enterpriseuser AS c_enterpriseusercashier ON p_cashcount.id_userCashier=c_enterpriseusercashier.id_user ";
            $vsql.="INNER JOIN c_user AS c_usercashier ON c_enterpriseusercashier.id_user=c_usercashier.id_user ";
            $vsql.="INNER JOIN c_usertype AS c_usercashiertype ON c_usercashier.id_userType=c_usercashiertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus AS c_usercashierstatus ON c_usercashier.id_userStatus=c_usercashierstatus.id_userStatus ";
            $vsql.="INNER JOIN c_cashtype ON p_cashcountcash.id_cashType=c_cashtype.id_cashType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_cashtype.id_cashType, p_cashcountcash.fldvalue";

            self::clean($vflCashCountCashList);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vcashCountCash= new clspFLCashCountCash();
                $vcashCountCash->idCashCountCash=(int)($vrow["p_cashcountcash.id_cashCountCash"]);
                $vcashCountCash->cashCount->idCashCount=(int)($vrow["p_cashcountcash.id_cashCount"]);
                $vcashCountCash->cashCount->cash->enterprise->idEnterprise=(int)($vrow["p_cashcountcash.id_enterprise"]);
                $vcashCountCash->cashCount->cash->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vcashCountCash->cashCount->cash->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vcashCountCash->cashCount->cash->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vcashCountCash->cashCount->cash->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vcashCountCash->cashCount->cash->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vcashCountCash->cashCount->cash->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vcashCountCash->cashCount->cash->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vcashCountCash->cashCount->cash->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vcashCountCash->cashCount->cash->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vcashCountCash->cashCount->cash->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vcashCountCash->cashCount->cash->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vcashCountCash->cashCount->cash->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vcashCountCash->cashCount->cash->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vcashCountCash->cashCount->cash->idCash=(int)($vrow["p_cashcountcash.id_cash"]);
                $vcashCountCash->cashCount->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vcashCountCash->cashCount->userAuditor->idUser=trim($vrow["p_cashcount.id_userAuditor"]);
                $vcashCountCash->cashCount->userAuditor->enterprise=$vcashCountCash->cashCount->cash->enterprise;
                $vcashCountCash->cashCount->userAuditor->userType->idUserType=(int)($vrow["c_userauditor.id_userType"]);
				$vcashCountCash->cashCount->userAuditor->userType->userType=trim($vrow["c_userauditortype.flduserType"]);
				$vcashCountCash->cashCount->userAuditor->userStatus->idUserStatus=(int)($vrow["c_userauditor.id_userStatus"]);
				$vcashCountCash->cashCount->userAuditor->userStatus->userStatus=trim($vrow["c_userauditorstatus.flduserStatus"]);
				$vcashCountCash->cashCount->userAuditor->firstName=trim($vrow["c_userauditor.fldfirstName"]);
				$vcashCountCash->cashCount->userAuditor->lastName=trim($vrow["c_userauditor.fldlastName"]);
				$vcashCountCash->cashCount->userAuditor->name=trim($vrow["c_userauditor.fldname"]);
                $vcashCountCash->cashCount->userCashier->idUser=trim($vrow["p_cashcount.id_userCashier"]);
                $vcashCountCash->cashCount->userCashier->enterprise=$vcashCountCash->cashCount->cash->enterprise;
                $vcashCountCash->cashCount->userCashier->userType->idUserType=(int)($vrow["c_usercashier.id_userType"]);
				$vcashCountCash->cashCount->userCashier->userType->userType=trim($vrow["c_usercashiertype.flduserType"]);
				$vcashCountCash->cashCount->userCashier->userStatus->idUserStatus=(int)($vrow["c_usercashier.id_userStatus"]);
				$vcashCountCash->cashCount->userCashier->userStatus->userStatus=trim($vrow["c_usercashierstatus.flduserStatus"]);
				$vcashCountCash->cashCount->userCashier->firstName=trim($vrow["c_usercashier.fldfirstName"]);
				$vcashCountCash->cashCount->userCashier->lastName=trim($vrow["c_usercashier.fldlastName"]);
				$vcashCountCash->cashCount->userCashier->name=trim($vrow["c_usercashier.fldname"]);
                $vcashCountCash->cashCount->recordDate=date("m/d/Y", strtotime(trim($vrow["p_cashcount.fldrecordDate"])));
                $vcashCountCash->cashCount->observation=trim($vrow["p_cashcount.fldobservation"]);
                $vcashCountCash->cashType->idCashType=(int)($vrow["p_cashcountcash.id_cashType"]);
                $vcashCountCash->cashType->cashType=trim($vrow["c_cashtype.fldcashType"]);
                $vcashCountCash->amount=(int)($vrow["p_cashcountcash.fldamount"]);
                $vcashCountCash->value=(float)($vrow["p_cashcountcash.fldvalue"]);

                self::add($vflCashCountCashList, $vcashCountCash);
                unset($vrow, $vcashCountCash);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function queryCashGroupByValue($vflCashCountCashList, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT c_cashtype.fldcashType, p_cashcountcash.fldvalue";
            $vsql.=", SUM(p_cashcountcash.fldamount) AS amountTotal ";
            $vsql.="FROM p_cashcountcash ";
            $vsql.="INNER JOIN p_cashcount ON p_cashcountcash.id_enterprise=p_cashcount.id_enterprise ";
            $vsql.="AND p_cashcountcash.id_cash=p_cashcount.id_cash ";
            $vsql.="AND p_cashcountcash.id_cashCount=p_cashcount.id_cashCount ";
            $vsql.="INNER JOIN c_cashtype ON p_cashcountcash.id_cashType=c_cashtype.id_cashType ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY c_cashtype.fldcashType, p_cashcountcash.fldvalue ";
            $vsql.="ORDER BY c_cashtype.fldcashType, p_cashcountcash.fldvalue";

            self::clean($vflCashCountCashList);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcashCountCash= new clspFLCashCountCash();
                $vcashCountCash->cashType->cashType=trim($vrow["fldcashType"]);
                $vcashCountCash->amount=(int)($vrow["amountTotal"]);
                $vcashCountCash->value=(float)($vrow["fldvalue"]);

                self::add($vflCashCountCashList, $vcashCountCash);
                unset($vrow, $vcashCountCash);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function addToDataBase($vflCashCountCashList, $vflCashCount, $vmySql)
	 {
		try{
			for($vi=0; $vi<self::total($vflCashCountCashList); $vi++){
                $vflCashCountCash= new clspFLCashCountCash();
                $vflCashCountCash=$vflCashCountCashList->cashCountCash[$vi];
                $vflCashCountCash->cashCount->idCashCount=$vflCashCount->idCashCount;
                if ( clspDLCashCountCash::addToDataBase($vflCashCountCash, $vmySql)==0 ){
                    return 0;
                }

                unset($vflCashCountCash);
			}

			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

    public static function deleteInDataBase($vflCashCountCashList, $vmySql)
	 {
		try{
			for($vi=0; $vi<self::total($vflCashCountCashList); $vi++){
                $vflCashCountCash= new clspFLCashCountCash();
                $vflCashCountCash=$vflCashCountCashList->cashCountCash[$vi];
                if ( clspDLCashCountCash::deleteInDataBase($vflCashCountCash, $vmySql)==0 ){
                    return 0;
                }

                unset($vflCashCountCash);
			}

			unset($vi);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function add($vflCashCountCashList, $vflCashCountCash)
	 {
        try{
            array_push($vflCashCountCashList->cashCountCash, $vflCashCountCash);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function total($vflCashCountCashList)
	 {
        try{
            return count($vflCashCountCashList->cashCountCash);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function clean($vflCashCountCashList)
	 {
        try{
            $vflCashCountCashList->cashCountCash=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


    public function __destruct(){ }
 }

?>
