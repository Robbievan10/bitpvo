<?php

class clspDLProductPrice
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflProductPrice, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflProductPrice->idProductPrice=self::getIdFromDataBase($vflProductPrice, $vmySql);
            
            $vsql ="INSERT INTO h_productprice(id_enterprise, id_product, id_productPrice, fldrecordDate, fldprice) ";
			$vsql.="VALUES(" . $vflProductPrice->product->enterprise->idEnterprise;
            $vsql.=", " . $vflProductPrice->product->idProduct;
			$vsql.=", " . $vflProductPrice->idProductPrice;
			$vsql.=", '" .  date("Y-m-d", strtotime($vflProductPrice->recordDate)) . "'";
			$vsql.=", " . $vflProductPrice->price . ")";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflProductPrice, $vmySql)
	 {
		try{
            $vidProductPrice=1;
			$vsql ="SELECT MAX(id_productPrice) + 1 AS id_productPriceNew ";
			$vsql.="FROM h_productprice ";
            $vsql.="WHERE id_enterprise=" . $vflProductPrice->product->enterprise->idEnterprise . " ";
            $vsql.="AND id_product=" . $vflProductPrice->product->idProduct;
			
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_productPriceNew"])>0) ){
                $vidProductPrice=(int)($vrow["id_productPriceNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidProductPrice;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>