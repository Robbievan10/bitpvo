<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLExpense.php");


class clscDLExpense
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflExpenses, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_expense.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_expensetype.fldexpenseType ";
			$vsql.="FROM c_expense ";
            $vsql.="INNER JOIN c_enterprise ON c_expense.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_expensetype ON c_expense.id_enterprise=c_expensetype.id_enterprise ";
            $vsql.="AND c_expense.id_expenseType=c_expensetype.id_expenseType ";
            $vsql.="INNER JOIN c_enterprise AS c_expensetypeenterprise ON c_expensetype.id_enterprise=c_expensetypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_expensetypemunicipality ON c_expensetypeenterprise.id_state=c_expensetypemunicipality.id_state ";
            $vsql.="AND c_expensetypeenterprise.id_municipality=c_expensetypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_expensetypestate ON c_expensetypemunicipality.id_state=c_expensetypestate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_expense.fldexpense, c_expensetype.fldexpenseType";
            
            self::clean($vflExpenses);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vexpense= new clspFLExpense();
                $vexpense->idExpense=(int)($vrow["id_expense"]);
                $vexpense->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vexpense->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vexpense->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vexpense->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vexpense->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vexpense->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vexpense->enterprise->locality=trim($vrow["fldlocality"]);
                $vexpense->enterprise->street=trim($vrow["fldstreet"]);
                $vexpense->enterprise->number=trim($vrow["fldnumber"]);
                $vexpense->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vexpense->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vexpense->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vexpense->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vexpense->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vexpense->expenseType->idExpenseType=(int)($vrow["id_expenseType"]);
                $vexpense->expenseType->enterprise=$vexpense->enterprise;
                $vexpense->expenseType->expenseType=trim($vrow["fldexpenseType"]);
                $vexpense->expense=trim($vrow["fldexpense"]);
                $vexpense->description=trim($vrow["flddescription"]);
                
                self::add($vflExpenses, $vexpense);
                unset($vrow, $vexpense);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflExpenses, $vexpense)
	 {
        try{
            array_push($vflExpenses->expenses, $vexpense);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflExpenses)
	 {
        try{
            return count($vflExpenses->expenses);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflExpenses)
	 {
        try{
            $vflExpenses->expenses=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>