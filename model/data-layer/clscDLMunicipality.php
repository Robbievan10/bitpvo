<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMunicipality.php");


class clscDLMunicipality
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflMunicipalities, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_municipality.*, c_state.fldstate ";
			$vsql.="FROM c_municipality ";
			$vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_municipality.fldmunicipality";
            
            self::clean($vflMunicipalities);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vmunicipality= new clspFLMunicipality();
                $vmunicipality->state->idState=(int)($vrow["id_state"]);
                $vmunicipality->state->state=trim($vrow["fldstate"]);
                $vmunicipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vmunicipality->municipality=trim($vrow["fldmunicipality"]);
                
                self::add($vflMunicipalities, $vmunicipality);
                unset($vrow, $vmunicipality);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflMunicipalities, $vmunicipality)
	 {
        try{
            array_push($vflMunicipalities->municipalities, $vmunicipality);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflMunicipalities)
	 {
        try{
            return count($vflMunicipalities->municipalities);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflMunicipalities)
	 {
        try{
            $vflMunicipalities->municipalities=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>