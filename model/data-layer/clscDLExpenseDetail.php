<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLExpenseDetail.php");


class clscDLExpenseDetail
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflExpensesDetails, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_expense.*, c_expense.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_expensetype.fldexpenseType";
            $vsql.=", c_route.* ";
            $vsql.="FROM p_expense ";
			$vsql.="INNER JOIN c_expense ON p_expense.id_enterprise=c_expense.id_enterprise ";
            $vsql.="AND p_expense.id_expense=c_expense.id_expense ";
            $vsql.="INNER JOIN c_enterprise ON c_expense.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_expensetype ON c_expense.id_enterprise=c_expensetype.id_enterprise ";
            $vsql.="AND c_expense.id_expenseType=c_expensetype.id_expenseType ";
            $vsql.="INNER JOIN c_enterprise AS c_expensetypeenterprise ON c_expensetype.id_enterprise=c_expensetypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_expensetypemunicipality ON c_expensetypeenterprise.id_state=c_expensetypemunicipality.id_state ";
            $vsql.="AND c_expensetypeenterprise.id_municipality=c_expensetypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_expensetypestate ON c_expensetypemunicipality.id_state=c_expensetypestate.id_state ";
            $vsql.="INNER JOIN c_route ON p_expense.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND p_expense.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routemunicipality ON c_routeenterprise.id_state=c_routemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routestate ON c_routemunicipality.id_state=c_routestate.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_expense.fldrecordDate DESC, c_expense.fldexpense ASC, c_expensetype.fldexpenseType ASC";
            
            self::clean($vflExpensesDetails);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vexpenseDetail= new clspFLExpenseDetail();
                $vexpenseDetail->expense->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vexpenseDetail->expense->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vexpenseDetail->expense->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vexpenseDetail->expense->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vexpenseDetail->expense->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vexpenseDetail->expense->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vexpenseDetail->expense->enterprise->locality=trim($vrow["fldlocality"]);
                $vexpenseDetail->expense->enterprise->street=trim($vrow["fldstreet"]);
                $vexpenseDetail->expense->enterprise->number=trim($vrow["fldnumber"]);
                $vexpenseDetail->expense->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vexpenseDetail->expense->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vexpenseDetail->expense->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vexpenseDetail->expense->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vexpenseDetail->expense->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vexpenseDetail->expense->expenseType->idExpenseType=(int)($vrow["id_expenseType"]);
                $vexpenseDetail->expense->expenseType->enterprise=$vexpenseDetail->expense->enterprise;
                $vexpenseDetail->expense->expenseType->expenseType=trim($vrow["fldexpenseType"]);
                $vexpenseDetail->expense->idExpense=(int)($vrow["id_expense"]);
                $vexpenseDetail->expense->expense=trim($vrow["fldexpense"]);
                $vexpenseDetail->expense->description=trim($vrow["flddescription"]);
                $vexpenseDetail->route->enterprise=$vexpenseDetail->expense->enterprise;
                $vexpenseDetail->route->idRoute=(int)($vrow["id_route"]);
                $vexpenseDetail->route->route=trim($vrow["fldroute"]);
                $vexpenseDetail->route->description=trim($vrow["flddescription"]);
                $vexpenseDetail->route->observation=trim($vrow["fldobservation"]);
                $vexpenseDetail->idExpenseDetail=(int)($vrow["id_expenseDetail"]);
                $vexpenseDetail->recordDate=date("m/d/Y", strtotime(trim($vrow["fldrecordDate"])));
                $vexpenseDetail->amount=(float)($vrow["fldamount"]);
                $vexpenseDetail->observation=trim($vrow["fldobservation"]);
                
                self::add($vflExpensesDetails, $vexpenseDetail);
                unset($vrow, $vexpenseDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function queryGroupByNameToDataBase($vflExpensesDetails, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT c_expense.id_expense, c_expense.fldexpense";
            $vsql.=", SUM(p_expense.fldamount) AS amountTotal ";
            $vsql.="FROM p_expense ";
            $vsql.="INNER JOIN c_expense ON p_expense.id_enterprise=c_expense.id_enterprise ";
            $vsql.="AND p_expense.id_expense=c_expense.id_expense ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY c_expense.id_expense ";
            $vsql.="ORDER BY c_expense.fldexpense";
            
            self::clean($vflExpensesDetails);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vexpenseDetail= new clspFLExpenseDetail();
                $vexpenseDetail->expense->idExpense=(int)($vrow["id_expense"]);
                $vexpenseDetail->expense->expense=trim($vrow["fldexpense"]);
                $vexpenseDetail->amount=(float)($vrow["amountTotal"]);
                
                self::add($vflExpensesDetails, $vexpenseDetail);
                unset($vrow, $vexpenseDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	private static function add($vflExpensesDetails, $vexpenseDetail)
	 {
        try{
            array_push($vflExpensesDetails->expensesDetails, $vexpenseDetail);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflExpensesDetails)
	 {
        try{
            return count($vflExpensesDetails->expensesDetails);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflExpensesDetails)
	 {
        try{
            $vflExpensesDetails->expensesDetails=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public static function totalAmount($vflExpensesDetails)
	 {
        try{
            $vtotalAmount=0;
            $vexpensesDetailsTotal=self::total($vflExpensesDetails);
            for($vi=0; $vi<$vexpensesDetailsTotal; $vi++){
                $vtotalAmount+=$vflExpensesDetails->expensesDetails[$vi]->amount;
            }
            return $vtotalAmount;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    public function __destruct(){ }
 }

?>