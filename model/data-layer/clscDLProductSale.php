<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductSale.php");


class clscDLProductSale
 {
    public function __construct() { }


    public static function queryToDataBase($vflProductsSales, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productsale.*, c_productsaleenterprise.*, c_productsaleenterprisemunicipality.fldmunicipality, c_productsaleenterprisestate.fldstate";
            $vsql.=", c_customer.*, c_customerenterprise.*, c_customerenterprisemunicipality.fldmunicipality, c_customerenterprisestate.fldstate";
            $vsql.=", c_persontype.fldpersonType, c_customermunicipality.fldmunicipality, c_customerstate.fldstate, c_operationtype.fldoperationType";
            $vsql.=", c_operationstatus.fldoperationStatus, c_cash.*, c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
            $vsql.="FROM p_productsale ";
			$vsql.="INNER JOIN c_enterprise AS c_productsaleenterprise ON p_productsale.id_enterprise=c_productsaleenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productsaleenterprisemunicipality ON c_productsaleenterprise.id_state=c_productsaleenterprisemunicipality.id_state ";
            $vsql.="AND c_productsaleenterprise.id_municipality=c_productsaleenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productsaleenterprisestate ON c_productsaleenterprisemunicipality.id_state=c_productsaleenterprisestate.id_state ";
            $vsql.="INNER JOIN c_customer ON p_productsale.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND p_productsale.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise AS c_customerenterprise ON c_customer.id_enterprise=c_customerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_customerenterprisemunicipality ON c_customerenterprise.id_state=c_customerenterprisemunicipality.id_state ";
            $vsql.="AND c_customerenterprise.id_municipality=c_customerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_customerenterprisestate ON c_customerenterprisemunicipality.id_state=c_customerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_customermunicipality ON c_customer.id_state=c_customermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_customermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_customerstate ON c_customermunicipality.id_state=c_customerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productsale.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productsale.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="INNER JOIN c_cash ON p_productsale.id_enterprise=c_cash.id_enterprise ";
            $vsql.="AND p_productsale.id_cash=c_cash.id_cash ";
            $vsql.="INNER JOIN c_enterprise AS c_cashenterprise ON c_cash.id_enterprise=c_cashenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_cashenterprisemunicipality ON c_cashenterprise.id_state=c_cashenterprisemunicipality.id_state ";
            $vsql.="AND c_cashenterprise.id_municipality=c_cashenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_cashenterprisestate ON c_cashenterprisemunicipality.id_state=c_cashenterprisestate.id_state ";
            $vsql.="INNER JOIN c_enterpriseuser ON p_productsale.id_user=c_enterpriseuser.id_user ";
            $vsql.="INNER JOIN c_user ON c_enterpriseuser.id_user=c_user.id_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productsale.id_productSale";

            self::clean($vflProductsSales);

			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductSale= new clspFLProductSale();
                $vproductSale->idProductSale=trim($vrow["p_productsale.id_productSale"]);
                //$vproductSale->CreatedOn=trim($vrow["p_productsale.CreatedOn"]);
                $vproductSale->enterprise->idEnterprise=(int)($vrow["p_productsale.id_enterprise"]);
                $vproductSale->enterprise->municipality->state->idState=(int)($vrow["c_productsaleenterprise.id_state"]);
                $vproductSale->enterprise->municipality->state->state=trim($vrow["c_productsaleenterprisestate.fldstate"]);
                $vproductSale->enterprise->municipality->idMunicipality=(int)($vrow["c_productsaleenterprise.id_municipality"]);
                $vproductSale->enterprise->municipality->municipality=trim($vrow["c_productsaleenterprisemunicipality.fldmunicipality"]);
                $vproductSale->enterprise->enterprise=trim($vrow["c_productsaleenterprise.fldenterprise"]);
                $vproductSale->enterprise->locality=trim($vrow["c_productsaleenterprise.fldlocality"]);
                $vproductSale->enterprise->street=trim($vrow["c_productsaleenterprise.fldstreet"]);
                $vproductSale->enterprise->number=trim($vrow["c_productsaleenterprise.fldnumber"]);
                $vproductSale->enterprise->phoneNumber=trim($vrow["c_productsaleenterprise.fldphoneNumber"]);
                $vproductSale->enterprise->movilNumber=trim($vrow["c_productsaleenterprise.fldmovilNumber"]);
                $vproductSale->enterprise->pageWeb=trim($vrow["c_productsaleenterprise.fldpageWeb"]);
                $vproductSale->customer->idCustomer=(int)($vrow["p_productsale.id_customer"]);
                $vproductSale->customer->enterprise->idEnterprise=(int)($vrow["p_productsale.id_enterprise"]);
                $vproductSale->customer->enterprise->municipality->state->idState=(int)($vrow["c_customerenterprise.id_state"]);
                $vproductSale->customer->enterprise->municipality->state->state=trim($vrow["c_customerenterprisestate.fldstate"]);
                $vproductSale->customer->enterprise->municipality->idMunicipality=(int)($vrow["c_customerenterprise.id_municipality"]);
                $vproductSale->customer->enterprise->municipality->municipality=trim($vrow["c_customerenterprisemunicipality.fldmunicipality"]);
                $vproductSale->customer->enterprise->enterprise=trim($vrow["c_customerenterprise.fldenterprise"]);
                $vproductSale->customer->enterprise->locality=trim($vrow["c_customerenterprise.fldlocality"]);
                $vproductSale->customer->enterprise->street=trim($vrow["c_customerenterprise.fldstreet"]);
                $vproductSale->customer->enterprise->number=trim($vrow["c_customerenterprise.fldnumber"]);
                $vproductSale->customer->enterprise->phoneNumber=trim($vrow["c_customerenterprise.fldphoneNumber"]);
                $vproductSale->customer->enterprise->movilNumber=trim($vrow["c_customerenterprise.fldmovilNumber"]);
                $vproductSale->customer->enterprise->pageWeb=trim($vrow["c_customerenterprise.fldpageWeb"]);
				$vproductSale->customer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
				$vproductSale->customer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vproductSale->customer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vproductSale->customer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vproductSale->customer->locality=trim($vrow["c_customer.fldlocality"]);
                $vproductSale->customer->street=trim($vrow["c_customer.fldstreet"]);
                $vproductSale->customer->number=trim($vrow["c_customer.fldnumber"]);
                $vproductSale->customer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vproductSale->customer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vproductSale->customer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vproductSale->customer->email=trim($vrow["c_customer.fldemail"]);
                $vproductSale->customer->observation=trim($vrow["c_customer.fldobservation"]);
                $vproductSale->operationType->idOperationType=(int)($vrow["p_productsale.id_operationType"]);
                $vproductSale->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vproductSale->operationStatus->idOperationStatus=(int)($vrow["p_productsale.id_operationStatus"]);
                $vproductSale->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vproductSale->cash->enterprise=$vproductSale->enterprise;
                $vproductSale->cash->idCash=trim($vrow["p_productsale.id_cash"]);
                $vproductSale->cash->cash=trim($vrow["c_cash.fldcash"]);
                $vproductSale->user->idUser=trim($vrow["p_productsale.id_user"]);
                $vproductSale->user->enterprise=$vproductSale->cash->enterprise;
                $vproductSale->user->userType->idUserType=(int)($vrow["c_user.id_userType"]);
				$vproductSale->user->userType->userType=trim($vrow["c_usertype.flduserType"]);
				$vproductSale->user->userStatus->idUserStatus=(int)($vrow["c_user.id_userStatus"]);
				$vproductSale->user->userStatus->userStatus=trim($vrow["c_userstatus.flduserStatus"]);
				$vproductSale->user->firstName=trim($vrow["c_user.fldfirstName"]);
				$vproductSale->user->lastName=trim($vrow["c_user.fldlastName"]);
				$vproductSale->user->name=trim($vrow["c_user.fldname"]);
                $vproductSale->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldrecordDate"])));
                $vproductSale->productSaleDate=date("m/d/Y", strtotime(trim($vrow["p_productsale.fldproductSaleDate"])));
                $vproductSale->canceled=(int)($vrow["p_productsale.fldcanceled"]);

                self::add($vflProductsSales, $vproductSale);
                unset($vrow, $vproductSale);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();

			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function add($vflProductsSales, $vproductSale)
	 {
        try{
            array_push($vflProductsSales->productsSales, $vproductSale);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	public static function total($vflProductsSales)
	 {
        try{
            return count($vflProductsSales->productsSales);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }

	private static function clean($vflProductsSales)
	 {
        try{
            $vflProductsSales->productsSales=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }


    public function __destruct(){ }
 }

?>
