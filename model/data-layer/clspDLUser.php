<?php

class clspDLUser
 {
	public function __construct() { }
    
	
	public static function updateInDataBase($vflUser, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_user ";
			$vsql.="SET fldfirstName='" . $vflUser->firstName . "'";
			$vsql.=", fldlastName='" . $vflUser->lastName . "'";
			$vsql.=", fldname='" . $vflUser->name . "' ";
			$vsql.="WHERE id_user='" . $vflUser->idUser . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function updatePasswordInDataBase($vflUser, $vpassword, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_user ";
			$vsql.="SET fldpassword='" . md5($vpassword) . "' ";
			$vsql.="WHERE id_user='" . $vflUser->idUser . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function updateStatusInDataBase($vflUser, $vidStatus, $vmySql)
	 {
		try{
			$vsql ="UPDATE c_user ";
			$vsql.="SET id_userStatus=" . $vidStatus . " ";
			$vsql.="WHERE id_user='" . $vflUser->idUser . "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
				return 0;
			}
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function queryToDataBase($vflUser, $vmySql)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
			$vsql.="FROM c_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
			$vsql.="WHERE c_user.id_user='" . $vflUser->idUser. "'";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getData();
                $vflUser->userType->idUserType=(int)($vrow["id_userType"]);
				$vflUser->userType->userType=trim($vrow["flduserType"]);
				$vflUser->userStatus->idUserStatus=(int)($vrow["id_userStatus"]);
				$vflUser->userStatus->userStatus=trim($vrow["flduserStatus"]);
				$vflUser->firstName=trim($vrow["fldfirstName"]);
				$vflUser->lastName=trim($vrow["fldlastName"]);
				$vflUser->name=trim($vrow["fldname"]);
					
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function verifyPasswordToDataBase($vflUser, $vpassword, $vmySql)
	 {
		try{
			$vsql ="SELECT id_userStatus ";
			$vsql.="FROM c_user ";
			$vsql.="WHERE id_user='" . $vflUser->idUser . "' ";
			$vsql.="AND fldpassword=MD5('" . $vpassword . "')";
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()!=1 ){
				return 0;
			}
			$vrow=$vmySql->getData();
			if ( ((int)(trim($vrow["id_userStatus"])))==0 ){
				return -1;
			}
			
			unset($vpassword, $vsql, $vrow);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	
	public function __destruct(){ }
 }

?>