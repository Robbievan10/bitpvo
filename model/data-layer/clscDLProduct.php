<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");


class clscDLProduct
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProducts, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_product.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_producttype.fldproductType ";
			$vsql.="FROM c_product ";
            $vsql.="INNER JOIN c_enterprise ON c_product.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
			$vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
			$vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_product.fldname";
            
            self::clean($vflProducts);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproduct= new clspFLProduct();
                $vproduct->idProduct=(int)($vrow["id_product"]);
                $vproduct->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vproduct->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vproduct->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vproduct->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vproduct->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vproduct->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vproduct->enterprise->locality=trim($vrow["fldlocality"]);
                $vproduct->enterprise->street=trim($vrow["fldstreet"]);
                $vproduct->enterprise->number=trim($vrow["fldnumber"]);
                $vproduct->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vproduct->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vproduct->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vproduct->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vproduct->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vproduct->productType->idProductType=(int)($vrow["id_productType"]);
                $vproduct->productType->enterprise=$vproduct->enterprise;
                $vproduct->productType->productType=trim($vrow["fldproductType"]);
                $vproduct->key=trim($vrow["fldkey"]);
                $vproduct->barCode=trim($vrow["fldbarCode"]);
                $vproduct->name=trim($vrow["fldname"]);
                $vproduct->description=trim($vrow["flddescription"]);
                $vproduct->image=trim($vrow["fldimage"]);
                $vproduct->purchasePrice=(float)($vrow["fldpurchasePrice"]);
                $vproduct->salePrice=(float)($vrow["fldsalePrice"]);
                $vproduct->weightInKg=(float)($vrow["fldweightInKg"]);
                $vproduct->stock=(float)($vrow["fldstock"]);
                $vproduct->stockInKg=(float)($vrow["fldstockInKg"]);
                $vproduct->observation=trim($vrow["fldobservation"]);
                
                self::add($vflProducts, $vproduct);
                unset($vrow, $vproduct);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflProducts, $vproduct)
	 {
        try{
            array_push($vflProducts->products, $vproduct);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflProducts)
	 {
        try{
            return count($vflProducts->products);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProducts)
	 {
        try{
            $vflProducts->products=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>