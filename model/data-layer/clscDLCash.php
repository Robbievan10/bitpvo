<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCash.php");


class clscDLCash
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflCash, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_cash.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM c_cash ";
            $vsql.="INNER JOIN c_enterprise ON c_cash.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_cash.fldcash";
            
            self::clean($vflCash);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vcash= new clspFLCash();
                $vcash->enterprise->idEnterprise=(int)($vrow["id_enterprise"]);
                $vcash->enterprise->municipality->state->idState=(int)($vrow["id_state"]);
                $vcash->enterprise->municipality->state->state=trim($vrow["fldstate"]);
                $vcash->enterprise->municipality->idMunicipality=(int)($vrow["id_municipality"]);
                $vcash->enterprise->municipality->municipality=trim($vrow["fldmunicipality"]);
                $vcash->enterprise->enterprise=trim($vrow["fldenterprise"]);
                $vcash->enterprise->locality=trim($vrow["fldlocality"]);
                $vcash->enterprise->street=trim($vrow["fldstreet"]);
                $vcash->enterprise->number=trim($vrow["fldnumber"]);
                $vcash->enterprise->phoneNumber=trim($vrow["fldphoneNumber"]);
                $vcash->enterprise->movilNumber=trim($vrow["fldmovilNumber"]);
                $vcash->enterprise->pageWeb=trim($vrow["fldpageWeb"]);
                $vcash->enterprise->avatarImage=trim($vrow["fldavatarImage"]);
                $vcash->enterprise->logoImage=trim($vrow["fldlogoImage"]);
                $vcash->idCash=(int)($vrow["id_cash"]);
                $vcash->cash=trim($vrow["fldcash"]);
                
                self::add($vflCash, $vcash);
                unset($vrow, $vcash);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflCash, $vcash)
	 {
        try{
            array_push($vflCash->cash, $vcash);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflCash)
	 {
        try{
            return count($vflCash->cash);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflCash)
	 {
        try{
            $vflCash->cash=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>