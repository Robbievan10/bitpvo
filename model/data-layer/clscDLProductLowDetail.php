<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductLowDetail.php");


class clscDLProductLowDetail
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsLowDetails, $vfilter, $vmySql)
	 {
		try{
            $vsql ="SELECT p_productlowdetail.*, p_productlow.*, c_productlowenterprise.*, c_productlowenterprisemunicipality.fldmunicipality";
            $vsql.=", c_productlowenterprisestate.fldstate, c_productlowtype.fldproductLowType, c_product.*, c_producttype.fldproductType ";
            $vsql.="FROM p_productlowdetail ";
			$vsql.="INNER JOIN p_productlow ON p_productlowdetail.id_enterprise=p_productlow.id_enterprise ";
            $vsql.="AND p_productlowdetail.id_productLow=p_productlow.id_productLow ";
            $vsql.="INNER JOIN c_enterprise AS c_productlowenterprise ON p_productlow.id_enterprise=c_productlowenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productlowenterprisemunicipality ON c_productlowenterprise.id_state=c_productlowenterprisemunicipality.id_state ";
            $vsql.="AND c_productlowenterprise.id_municipality=c_productlowenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productlowenterprisestate ON c_productlowenterprisemunicipality.id_state=c_productlowenterprisestate.id_state ";
            $vsql.="INNER JOIN c_productlowtype ON p_productlow.id_productLowType=c_productlowtype.id_productLowType ";
            $vsql.="INNER JOIN c_product ON p_productlowdetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productlowdetail.id_product=c_product.id_product ";
            $vsql.="INNER JOIN c_enterprise AS c_productenterprise ON c_product.id_enterprise=c_productenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productenterprisemunicipality  ON c_productenterprise.id_state=c_productenterprisemunicipality.id_state ";
            $vsql.="AND c_productenterprise.id_municipality=c_productenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productenterprisestate ON c_productenterprisemunicipality.id_state=c_productenterprisestate.id_state ";
			$vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productlowdetail.id_enterprise, p_productlowdetail.id_productLow, p_productlowdetail.id_product";
            
            self::clean($vflProductsLowDetails);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductLowDetail= new clspFLProductLowDetail();
                $vproductLowDetail->productLow->idProductLow=trim($vrow["p_productlowdetail.id_productLow"]);
                $vproductLowDetail->productLow->enterprise->idEnterprise=(int)($vrow["p_productlowdetail.id_enterprise"]);
                $vproductLowDetail->productLow->enterprise->municipality->state->idState=(int)($vrow["c_productlowenterprise.id_state"]);
                $vproductLowDetail->productLow->enterprise->municipality->state->state=trim($vrow["c_productlowenterprisestate.fldstate"]);
                $vproductLowDetail->productLow->enterprise->municipality->idMunicipality=(int)($vrow["c_productlowenterprise.id_municipality"]);
                $vproductLowDetail->productLow->enterprise->municipality->municipality=trim($vrow["c_productlowenterprisemunicipality.fldmunicipality"]);
                $vproductLowDetail->productLow->enterprise->enterprise=trim($vrow["c_productlowenterprise.fldenterprise"]);
                $vproductLowDetail->productLow->enterprise->locality=trim($vrow["c_productlowenterprise.fldlocality"]);
                $vproductLowDetail->productLow->enterprise->street=trim($vrow["c_productlowenterprise.fldstreet"]);
                $vproductLowDetail->productLow->enterprise->number=trim($vrow["c_productlowenterprise.fldnumber"]);
                $vproductLowDetail->productLow->enterprise->phoneNumber=trim($vrow["c_productlowenterprise.fldphoneNumber"]);
                $vproductLowDetail->productLow->enterprise->movilNumber=trim($vrow["c_productlowenterprise.fldmovilNumber"]);
                $vproductLowDetail->productLow->enterprise->pageWeb=trim($vrow["c_productlowenterprise.fldpageWeb"]);
                $vproductLowDetail->productLow->productLowType->idProductLowType=(int)($vrow["p_productlow.id_productLowType"]);
                $vproductLowDetail->productLow->productLowType->productLowType=trim($vrow["c_productlowtype.fldproductLowType"]);
                $vproductLowDetail->productLow->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productlow.fldrecordDate"])));
                $vproductLowDetail->productLow->productLowDate=date("m/d/Y", strtotime(trim($vrow["p_productlow.fldproductLowDate"])));
                $vproductLowDetail->productLow->observation=trim($vrow["p_productlow.fldobservation"]);
                $vproductLowDetail->product->idProduct=(int)($vrow["p_productlowdetail.id_product"]);
                $vproductLowDetail->product->enterprise=$vproductLowDetail->productLow->enterprise;
                $vproductLowDetail->product->productType->idProductType=(int)($vrow["c_product.id_productType"]);
                $vproductLowDetail->product->productType->enterprise=$vproductLowDetail->productLow->enterprise;
                $vproductLowDetail->product->productType->productType=trim($vrow["c_producttype.fldproductType"]);
                $vproductLowDetail->product->key=trim($vrow["c_product.fldkey"]);
                $vproductLowDetail->product->barCode=trim($vrow["c_product.fldbarCode"]);
                $vproductLowDetail->product->name=trim($vrow["c_product.fldname"]);
                $vproductLowDetail->product->description=trim($vrow["c_product.flddescription"]);
                $vproductLowDetail->product->image=trim($vrow["c_product.fldimage"]);
                $vproductLowDetail->product->purchasePrice=(float)($vrow["c_product.fldpurchasePrice"]);
                $vproductLowDetail->product->salePrice=(float)($vrow["c_product.fldsalePrice"]);
                $vproductLowDetail->product->weightInKg=(float)($vrow["c_product.fldweightInKg"]);
                $vproductLowDetail->product->stock=(float)($vrow["c_product.fldstock"]);
                $vproductLowDetail->product->observation=trim($vrow["c_product.fldobservation"]);
                $vproductLowDetail->lowAmount=(float)($vrow["p_productlowdetail.fldlowAmount"]);
                $vproductLowDetail->lowWeightInKg=(float)($vrow["p_productlowdetail.fldlowWeightInKg"]);
                $vproductLowDetail->purchasePrice=(float)($vrow["p_productlowdetail.fldpurchasePrice"]);
                $vproductLowDetail->salePrice=(float)($vrow["p_productlowdetail.fldsalePrice"]);
                
                self::add($vflProductsLowDetails, $vproductLowDetail);
                unset($vrow, $vproductLowDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function queryLowGroupByNameToDataBase($vflProductsLowDetails, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT c_product.fldname, SUM(p_productlowdetail.fldlowAmount) AS lowAmountTotal";
            $vsql.=", SUM(p_productlowdetail.fldlowWeightInKg) AS lowWeightInKgTotal "; 
            $vsql.="FROM p_productlowdetail ";
            $vsql.="INNER JOIN p_productlow ON p_productlowdetail.id_enterprise=p_productlow.id_enterprise "; 
            $vsql.="AND p_productlowdetail.id_productlow=p_productlow.id_productlow ";
            $vsql.="INNER JOIN c_product ON p_productlowdetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productlowdetail.id_product= c_product.id_product ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY c_product.fldname";
            
            self::clean($vflProductsLowDetails);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductLowDetail= new clspFLProductLowDetail();
                $vproductLowDetail->product->name=trim($vrow["fldname"]);
                $vproductLowDetail->lowAmount=(float)($vrow["lowAmountTotal"]);
                $vproductLowDetail->lowWeightInKg=(float)($vrow["lowWeightInKgTotal"]);
                
                self::add($vflProductsLowDetails, $vproductLowDetail);
                unset($vrow, $vproductLowDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	public static function add($vflProductsLowDetails, $vproductLowDetail)
	 {
        try{
            array_push($vflProductsLowDetails->productsLowDetails, $vproductLowDetail);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsLowDetails)
	 {
        try{
            return count($vflProductsLowDetails->productsLowDetails);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsLowDetails)
	 {
        try{
            $vflProductsLowDetails->productsLowDetails=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>