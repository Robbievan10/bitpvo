<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductLowType.php");


class clscDLProductLowType
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductLowTypes, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT * FROM c_productlowtype ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY id_productLowType";
            
            self::clean($vflProductLowTypes);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductLowType= new clspFLProductLowType();
                $vproductLowType->idProductLowType=(int)($vrow["id_productLowType"]);
                $vproductLowType->productLowType=trim($vrow["fldproductLowType"]);
                
                self::add($vflProductLowTypes, $vproductLowType);
                unset($vrow, $vproductLowType);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflProductLowTypes, $vproductLowType)
	 {
        try{
            array_push($vflProductLowTypes->productLowTypes, $vproductLowType);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflProductLowTypes)
	 {
        try{
            return count($vflProductLowTypes->productLowTypes);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductLowTypes)
	 {
        try{
            $vflProductLowTypes->productLowTypes=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>