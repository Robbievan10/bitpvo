<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLPayment.php");


class clspDLProductEntryPayment
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflProductEntryPayment, $vmySql, $vtype=0)
	 {
        try{
            if ( $vtype==0 ){
                $vmySql->startTransaction();
            }
            if ( clspDLPayment::addToDataBase($vflProductEntryPayment, $vmySql)==1 ){
                $vsql ="INSERT INTO p_productentrypayment(id_enterprise, id_payment, id_productEntry) ";
				$vsql.="VALUES(" . $vflProductEntryPayment->productEntry->enterprise->idEnterprise;
                $vsql.=", " . $vflProductEntryPayment->idPayment;
                $vsql.=", '" . $vflProductEntryPayment->productEntry->idProductEntry ."')";
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    if ( $vtype==0 ){        
                        $vmySql->rollbackTransaction();
                    }
                    return 0;
                }
				unset($vsql);
            }
            else{
                if ( $vtype==0 ){
                    $vmySql->rollbackTransaction();
                }
                return -1;
            }
            if ( $vtype==0 ){
                $vmySql->commitTransaction();
            }
			
			return 1;
		}
		catch (Exception $vexcepcion){
            if ( $vtype==0 ){
                $vmySql->rollbackTransaction();
            }
		    throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflProductEntryPayment, $vmySql)
	 {
		try{
            return clspDLPayment::updateInDataBase($vflProductEntryPayment, $vmySql);
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflProductEntryPayment, $vmySql, $vtype=0)
	 {
		try{
            $vsql ="DELETE FROM p_productentrypayment ";
            $vsql.="WHERE id_enterprise=" . $vflProductEntryPayment->productEntry->enterprise->idEnterprise . " ";
            $vsql.="AND id_payment=" . $vflProductEntryPayment->idPayment . " ";
            $vsql.="AND id_productEntry='" . $vflProductEntryPayment->productEntry->idProductEntry . "'";
            
            if ( $vtype==0 ){
                $vmySql->startTransaction();
            }
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLPayment::deleteInDataBase($vflProductEntryPayment, $vmySql)==0 ){
                    if ( $vtype==0 ){
                        $vmySql->rollbackTransaction();
                    }
                    return -1;
                }
            }
            else{
                if ( $vtype==0 ){  
                    $vmySql->rollbackTransaction();
                }
                return 0;
            }
            if ( $vtype==0 ){
                $vmySql->commitTransaction();
            }
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
		    if ( $vtype==0 ){
                $vmySql->rollbackTransaction();
            }
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     	
	public function __destruct(){ }
 }

?>