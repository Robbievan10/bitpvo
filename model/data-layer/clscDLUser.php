<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLUser.php");


class clscDLUser
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflUsers, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_user.*, c_usertype.flduserType, c_userstatus.flduserStatus ";
			$vsql.="FROM c_user ";
            $vsql.="INNER JOIN c_usertype ON c_user.id_userType=c_usertype.id_userType ";
			$vsql.="INNER JOIN c_userstatus ON c_user.id_userStatus=c_userstatus.id_userStatus ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY c_user.fldfirstName, c_user.fldlastName, c_user.fldname";
            
            self::clean($vflUsers);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vuser= new clspFLUser();
                $vuser->idUser=trim($vrow["id_user"]);
                $vuser->userType->idUserType=(int)($vrow["id_userType"]);
				$vuser->userType->userType=trim($vrow["flduserType"]);
				$vuser->userStatus->idUserStatus=(int)($vrow["id_userStatus"]);
				$vuser->userStatus->userStatus=trim($vrow["flduserStatus"]);
				$vuser->firstName=trim($vrow["fldfirstName"]);
				$vuser->lastName=trim($vrow["fldlastName"]);
				$vuser->name=trim($vrow["fldname"]);
                
                self::add($vflUsers, $vuser);
                unset($vrow, $vuser);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflUsers, $vuser)
	 {
        try{
            array_push($vflUsers->users, $vuser);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflUsers)
	 {
        try{
            return count($vflUsers->users);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflUsers)
	 {
        try{
            $vflUsers->users=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>