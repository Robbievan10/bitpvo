<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductLow.php");


class clscDLProductLow
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsLow, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productlow.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate, c_productlowtype.fldproductLowType ";
            $vsql.="FROM p_productlow ";
			$vsql.="INNER JOIN c_enterprise ON p_productlow.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="INNER JOIN c_productlowtype ON p_productlow.id_productLowType=c_productlowtype.id_productLowType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productlow.id_productLow";
            
            self::clean($vflProductsLow);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductLow= new clspFLProductLow();
                $vproductLow->idProductLow=trim($vrow["p_productlow.id_productLow"]);
                $vproductLow->enterprise->idEnterprise=(int)($vrow["p_productlow.id_enterprise"]);
                $vproductLow->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vproductLow->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vproductLow->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vproductLow->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vproductLow->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vproductLow->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vproductLow->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vproductLow->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vproductLow->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vproductLow->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vproductLow->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vproductLow->productLowType->idProductLowType=(int)($vrow["p_productlow.id_productLowType"]);
                $vproductLow->productLowType->productLowType=trim($vrow["c_productlowtype.fldproductLowType"]);
                $vproductLow->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productlow.fldrecordDate"])));
                $vproductLow->productLowDate=date("m/d/Y", strtotime(trim($vrow["p_productlow.fldproductLowDate"])));
                $vproductLow->observation=trim($vrow["p_productlow.fldobservation"]);
                
                self::add($vflProductsLow, $vproductLow);
                unset($vrow, $vproductLow);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflProductsLow, $vproductLow)
	 {
        try{
            array_push($vflProductsLow->productsLow, $vproductLow);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsLow)
	 {
        try{
            return count($vflProductsLow->productsLow);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsLow)
	 {
        try{
            $vflProductsLow->productsLow=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>