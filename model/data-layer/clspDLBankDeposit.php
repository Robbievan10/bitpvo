<?php

class clspDLBankDeposit
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflBankDeposit, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vflBankDeposit->idBankDeposit=self::getIdFromDataBase($vflBankDeposit, $vmySql);
            
            $vsql ="INSERT INTO p_bankdeposit(id_enterprise, id_bank, id_bankDeposit, fldaccountNumber, fldamount, fldresponsibleName";
            $vsql.=", fldreferenceName, fldrecordDate, flddepositDate, fldobservation) ";
			$vsql.="VALUES(" . $vflBankDeposit->bank->enterprise->idEnterprise;
            $vsql.=", " . $vflBankDeposit->bank->idBank;
            $vsql.=", " . $vflBankDeposit->idBankDeposit;
            $vsql.=", '" . $vflBankDeposit->accountNumber . "'";
            $vsql.=", " . $vflBankDeposit->amount;
            $vsql.=", '" . $vflBankDeposit->responsibleName . "'";
            $vsql.=", '" . $vflBankDeposit->referenceName . "'";
			$vsql.=", '" . date("Y-m-d", strtotime($vflBankDeposit->recordDate)) . "'";
            $vsql.=", '" . date("Y-m-d", strtotime($vflBankDeposit->depositDate)) . "'";
            $vsql.=", '" . $vflBankDeposit->observation . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBankDeposit, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
			$vsql ="UPDATE p_bankdeposit ";
            $vsql.="SET flddepositDate='" . date("Y-m-d", strtotime($vflBankDeposit->depositDate)) . "'";
            $vsql.=", fldaccountNumber='" . $vflBankDeposit->accountNumber . "' ";
            $vsql.=", fldamount=" . $vflBankDeposit->amount;
            $vsql.=", fldresponsibleName='" . $vflBankDeposit->responsibleName . "' ";
            $vsql.=", fldreferenceName='" . $vflBankDeposit->referenceName . "' ";
            $vsql.=", fldobservation='" . $vflBankDeposit->observation . "' ";
			$vsql.="WHERE id_enterprise=" . $vflBankDeposit->bank->enterprise->idEnterprise . " ";
            $vsql.="AND id_bank=" . $vflBankDeposit->bank->idBank . " ";
            $vsql.="AND id_bankdeposit=" . $vflBankDeposit->idBankDeposit;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBankDeposit, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_bankdeposit ";
            $vsql.="WHERE id_enterprise=" . $vflBankDeposit->bank->enterprise->idEnterprise . " ";
            $vsql.="AND id_bank=" . $vflBankDeposit->bank->idBank . " ";
            $vsql.="AND id_bankDeposit=" . $vflBankDeposit->idBankDeposit;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBankDeposit, $vmySql)
	 {
		try{
			$vsql ="SELECT p_bankdeposit.*, c_bank.*, c_enterprise.*, c_municipality.fldmunicipality, c_state.fldstate ";
            $vsql.="FROM p_bankdeposit ";
			$vsql.="INNER JOIN c_bank ON p_bankdeposit.id_enterprise=c_bank.id_enterprise ";
            $vsql.="AND p_bankdeposit.id_bank=c_bank.id_bank ";
            $vsql.="INNER JOIN c_enterprise ON c_bank.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality ON c_enterprise.id_state=c_municipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_municipality.id_municipality ";
            $vsql.="INNER JOIN c_state ON c_municipality.id_state=c_state.id_state ";
            $vsql.="WHERE p_bankdeposit.id_enterprise=" . $vflBankDeposit->bank->enterprise->idEnterprise . " ";
            $vsql.="AND p_bankdeposit.id_bank=" . $vflBankDeposit->bank->idBank . " ";
            $vsql.="AND p_bankdeposit.id_bankDeposit=" . $vflBankDeposit->idBankDeposit;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflBankDeposit->bank->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflBankDeposit->bank->enterprise->municipality->state->state=trim($vrow["c_state.fldstate"]);
                $vflBankDeposit->bank->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflBankDeposit->bank->enterprise->municipality->municipality=trim($vrow["c_municipality.fldmunicipality"]);
                $vflBankDeposit->bank->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflBankDeposit->bank->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflBankDeposit->bank->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflBankDeposit->bank->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);
                $vflBankDeposit->bank->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflBankDeposit->bank->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflBankDeposit->bank->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflBankDeposit->bank->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflBankDeposit->bank->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflBankDeposit->bank->name=trim($vrow["c_bank.fldname"]);
                $vflBankDeposit->accountNumber=trim($vrow["p_bankdeposit.fldaccountNumber"]);
                $vflBankDeposit->amount=(float)($vrow["p_bankdeposit.fldamount"]);
                $vflBankDeposit->responsibleName=trim($vrow["p_bankdeposit.fldresponsibleName"]);
                $vflBankDeposit->referenceName=trim($vrow["p_bankdeposit.fldreferenceName"]);
                $vflBankDeposit->recordDate=date("m/d/Y", strtotime(trim($vrow["p_bankdeposit.fldrecordDate"])));
                $vflBankDeposit->depositDate=date("m/d/Y", strtotime(trim($vrow["p_bankdeposit.flddepositDate"])));
                $vflBankDeposit->observation=trim($vrow["p_bankdeposit.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflBankDeposit, $vmySql)
	 {
		try{
            $vidBankDeposit=1;
			$vsql ="SELECT MAX(id_bankDeposit) + 1 AS id_bankDepositNew ";
			$vsql.="FROM p_bankdeposit ";
            $vsql.="WHERE id_enterprise=" . $vflBankDeposit->bank->enterprise->idEnterprise . " ";
            $vsql.="AND id_bank=" . $vflBankDeposit->bank->idBank;
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_bankDepositNew"])>0) ){
                $vidBankDeposit=(int)($vrow["id_bankDepositNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidBankDeposit;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>