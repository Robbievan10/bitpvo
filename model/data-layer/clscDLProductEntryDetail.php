<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntryDetail.php");


class clscDLProductEntryDetail
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsEntriesDetails, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productentrydetail.*, p_productentry.*, c_productentryenterprise.*, c_productentryenterprisemunicipality.fldmunicipality";
            $vsql.=", c_productentryenterprisestate.fldstate, c_provider.*, c_providerenterprise.*, c_providerenterprisemunicipality.fldmunicipality";
            $vsql.=", c_providerenterprisestate.fldstate, c_persontype.fldpersonType, c_providermunicipality.fldmunicipality, c_providerstate.fldstate";
            $vsql.=", c_operationtype.fldoperationType, c_operationstatus.fldoperationStatus, c_product.*, c_productenterprise.*";
            $vsql.=", c_productenterprisemunicipality.fldmunicipality, c_productenterprisestate.fldstate, c_producttype.fldproductType ";
            $vsql.="FROM p_productentrydetail ";
			$vsql.="INNER JOIN p_productentry ON p_productentrydetail.id_enterprise=p_productentry.id_enterprise ";
            $vsql.="AND p_productentrydetail.id_productEntry=p_productentry.id_productEntry ";
            $vsql.="INNER JOIN c_enterprise AS c_productentryenterprise ON p_productentry.id_enterprise=c_productentryenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productentryenterprisemunicipality ON c_productentryenterprise.id_state=c_productentryenterprisemunicipality.id_state ";
            $vsql.="AND c_productentryenterprise.id_municipality=c_productentryenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productentryenterprisestate ON c_productentryenterprisemunicipality.id_state=c_productentryenterprisestate.id_state ";
            $vsql.="INNER JOIN c_provider ON p_productentry.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND p_productentry.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise AS c_providerenterprise ON c_provider.id_enterprise=c_providerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_providerenterprisemunicipality ON c_providerenterprise.id_state=c_providerenterprisemunicipality.id_state ";
            $vsql.="AND c_providerenterprise.id_municipality=c_providerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_providerenterprisestate ON c_providerenterprisemunicipality.id_state=c_providerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_providermunicipality ON c_provider.id_state=c_providermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_providermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_providerstate ON c_providermunicipality.id_state=c_providerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productentry.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productentry.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="INNER JOIN c_product ON p_productentrydetail.id_enterprise=c_product.id_enterprise ";
            $vsql.="AND p_productentrydetail.id_product=c_product.id_product ";
            $vsql.="INNER JOIN c_enterprise AS c_productenterprise ON c_product.id_enterprise=c_productenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productenterprisemunicipality  ON c_productenterprise.id_state=c_productenterprisemunicipality.id_state ";
            $vsql.="AND c_productenterprise.id_municipality=c_productenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productenterprisestate ON c_productenterprisemunicipality.id_state=c_productenterprisestate.id_state ";
			$vsql.="INNER JOIN c_producttype ON c_product.id_enterprise=c_producttype.id_enterprise ";
            $vsql.="AND c_product.id_productType=c_producttype.id_productType ";
            $vsql.="INNER JOIN c_enterprise AS c_producttypeenterprise ON c_producttype.id_enterprise=c_producttypeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_producttypemunicipality ON c_producttypeenterprise.id_state=c_producttypemunicipality.id_state ";
            $vsql.="AND c_producttypeenterprise.id_municipality=c_producttypemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_producttypestate ON c_producttypemunicipality.id_state=c_producttypestate.id_state ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productentrydetail.id_enterprise, p_productentrydetail.id_productEntry, p_productentrydetail.id_product";
            
            self::clean($vflProductsEntriesDetails);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductEntryDetail= new clspFLProductEntryDetail();
                $vproductEntryDetail->productEntry->idProductEntry=trim($vrow["p_productentrydetail.id_productEntry"]);
                $vproductEntryDetail->productEntry->enterprise->idEnterprise=(int)($vrow["p_productentrydetail.id_enterprise"]);
                $vproductEntryDetail->productEntry->enterprise->municipality->state->idState=(int)($vrow["c_productentryenterprise.id_state"]);
                $vproductEntryDetail->productEntry->enterprise->municipality->state->state=trim($vrow["c_productentryenterprisestate.fldstate"]);
                $vproductEntryDetail->productEntry->enterprise->municipality->idMunicipality=(int)($vrow["c_productentryenterprise.id_municipality"]);
                $vproductEntryDetail->productEntry->enterprise->municipality->municipality=trim($vrow["c_productentryenterprisemunicipality.fldmunicipality"]);
                $vproductEntryDetail->productEntry->enterprise->enterprise=trim($vrow["c_productentryenterprise.fldenterprise"]);
                $vproductEntryDetail->productEntry->enterprise->locality=trim($vrow["c_productentryenterprise.fldlocality"]);
                $vproductEntryDetail->productEntry->enterprise->street=trim($vrow["c_productentryenterprise.fldstreet"]);
                $vproductEntryDetail->productEntry->enterprise->number=trim($vrow["c_productentryenterprise.fldnumber"]);
                $vproductEntryDetail->productEntry->enterprise->phoneNumber=trim($vrow["c_productentryenterprise.fldphoneNumber"]);
                $vproductEntryDetail->productEntry->enterprise->movilNumber=trim($vrow["c_productentryenterprise.fldmovilNumber"]);
                $vproductEntryDetail->productEntry->enterprise->pageWeb=trim($vrow["c_productentryenterprise.fldpageWeb"]);
                $vproductEntryDetail->productEntry->provider->idProvider=(int)($vrow["p_productentry.id_provider"]);
                $vproductEntryDetail->productEntry->provider->enterprise->idEnterprise=(int)($vrow["p_productentrydetail.id_enterprise"]);
                $vproductEntryDetail->productEntry->provider->enterprise->municipality->state->idState=(int)($vrow["c_providerenterprise.id_state"]);
                $vproductEntryDetail->productEntry->provider->enterprise->municipality->state->state=trim($vrow["c_providerenterprisestate.fldstate"]);
                $vproductEntryDetail->productEntry->provider->enterprise->municipality->idMunicipality=(int)($vrow["c_providerenterprise.id_municipality"]);
                $vproductEntryDetail->productEntry->provider->enterprise->municipality->municipality=trim($vrow["c_providerenterprisemunicipality.fldmunicipality"]);
                $vproductEntryDetail->productEntry->provider->enterprise->enterprise=trim($vrow["c_providerenterprise.fldenterprise"]);
                $vproductEntryDetail->productEntry->provider->enterprise->locality=trim($vrow["c_providerenterprise.fldlocality"]);
                $vproductEntryDetail->productEntry->provider->enterprise->street=trim($vrow["c_providerenterprise.fldstreet"]);
                $vproductEntryDetail->productEntry->provider->enterprise->number=trim($vrow["c_providerenterprise.fldnumber"]);                
                $vproductEntryDetail->productEntry->provider->enterprise->phoneNumber=trim($vrow["c_providerenterprise.fldphoneNumber"]);
                $vproductEntryDetail->productEntry->provider->enterprise->movilNumber=trim($vrow["c_providerenterprise.fldmovilNumber"]);
                $vproductEntryDetail->productEntry->provider->enterprise->pageWeb=trim($vrow["c_providerenterprise.fldpageWeb"]);
				$vproductEntryDetail->productEntry->provider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
				$vproductEntryDetail->productEntry->provider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vproductEntryDetail->productEntry->provider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vproductEntryDetail->productEntry->provider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vproductEntryDetail->productEntry->provider->locality=trim($vrow["c_provider.fldlocality"]);
                $vproductEntryDetail->productEntry->provider->street=trim($vrow["c_provider.fldstreet"]);
                $vproductEntryDetail->productEntry->provider->number=trim($vrow["c_provider.fldnumber"]);
                $vproductEntryDetail->productEntry->provider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vproductEntryDetail->productEntry->provider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vproductEntryDetail->productEntry->provider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vproductEntryDetail->productEntry->provider->email=trim($vrow["c_provider.fldemail"]);
                $vproductEntryDetail->productEntry->provider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vproductEntryDetail->productEntry->provider->observation=trim($vrow["c_provider.fldobservation"]);
                $vproductEntryDetail->productEntry->operationType->idOperationType=(int)($vrow["p_productentry.id_operationType"]);
                $vproductEntryDetail->productEntry->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vproductEntryDetail->productEntry->operationStatus->idOperationStatus=(int)($vrow["p_productentry.id_operationStatus"]);
                $vproductEntryDetail->productEntry->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vproductEntryDetail->productEntry->saleFolio=trim($vrow["p_productentry.fldsaleFolio"]);
                $vproductEntryDetail->productEntry->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldrecordDate"])));
                $vproductEntryDetail->productEntry->productEntryDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldproductEntryDate"])));
                $vproductEntryDetail->product->idProduct=(int)($vrow["p_productentrydetail.id_product"]);
                $vproductEntryDetail->product->enterprise->idEnterprise=(int)($vrow["p_productentrydetail.id_enterprise"]);
                $vproductEntryDetail->product->enterprise->municipality->state->idState=(int)($vrow["c_productenterprise.id_state"]);
                $vproductEntryDetail->product->enterprise->municipality->state->state=trim($vrow["c_productenterprisestate.fldstate"]);
                $vproductEntryDetail->product->enterprise->municipality->idMunicipality=(int)($vrow["c_productenterprise.id_municipality"]);
                $vproductEntryDetail->product->enterprise->municipality->municipality=trim($vrow["c_productenterprisemunicipality.fldmunicipality"]);
                $vproductEntryDetail->product->enterprise->enterprise=trim($vrow["c_productenterprise.fldenterprise"]);
                $vproductEntryDetail->product->enterprise->locality=trim($vrow["c_productenterprise.fldlocality"]);
                $vproductEntryDetail->product->enterprise->street=trim($vrow["c_productenterprise.fldstreet"]);
                $vproductEntryDetail->product->enterprise->number=trim($vrow["c_productenterprise.fldnumber"]);
                $vproductEntryDetail->product->enterprise->phoneNumber=trim($vrow["c_productenterprise.fldphoneNumber"]);
                $vproductEntryDetail->product->enterprise->movilNumber=trim($vrow["c_productenterprise.fldmovilNumber"]);
                $vproductEntryDetail->product->enterprise->pageWeb=trim($vrow["c_productenterprise.fldpageWeb"]);
                $vproductEntryDetail->product->productType->idProductType=(int)($vrow["c_product.id_productType"]);
                $vproductEntryDetail->product->productType->enterprise=$vproductEntryDetail->productEntry->enterprise;
                $vproductEntryDetail->product->productType->productType=trim($vrow["c_producttype.fldproductType"]);
                $vproductEntryDetail->product->key=trim($vrow["c_product.fldkey"]);
                $vproductEntryDetail->product->barCode=trim($vrow["c_product.fldbarCode"]);
                $vproductEntryDetail->product->name=trim($vrow["c_product.fldname"]);
                $vproductEntryDetail->product->description=trim($vrow["c_product.flddescription"]);
                $vproductEntryDetail->product->image=trim($vrow["c_product.fldimage"]);
                $vproductEntryDetail->product->purchasePrice=(float)($vrow["c_product.fldpurchasePrice"]);
                $vproductEntryDetail->product->salePrice=(float)($vrow["c_product.fldsalePrice"]);
                $vproductEntryDetail->product->weightInKg=(float)($vrow["c_product.fldweightInKg"]);
                $vproductEntryDetail->product->stock=(float)($vrow["c_product.fldstock"]);
                $vproductEntryDetail->product->observation=trim($vrow["c_product.fldobservation"]);
                $vproductEntryDetail->purchaseAmountInPt=(float)($vrow["p_productentrydetail.fldpurchaseAmountInPt"]);
                $vproductEntryDetail->purchaseAmountInKg=(float)($vrow["p_productentrydetail.fldpurchaseAmountInKg"]);
                $vproductEntryDetail->realAmountInPt=(float)($vrow["p_productentrydetail.fldrealAmountInPt"]);
                $vproductEntryDetail->realAmountInKg=(float)($vrow["p_productentrydetail.fldrealAmountInKg"]);
                $vproductEntryDetail->purchasePrice=(float)($vrow["p_productentrydetail.fldpurchasePrice"]);
                
                self::add($vflProductsEntriesDetails, $vproductEntryDetail);
                unset($vrow, $vproductEntryDetail);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function add($vflProductsEntriesDetails, $vproductEntryDetail)
	 {
        try{
            array_push($vflProductsEntriesDetails->productsEntriesDetails, $vproductEntryDetail);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsEntriesDetails)
	 {
        try{
            return count($vflProductsEntriesDetails->productsEntriesDetails);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsEntriesDetails)
	 {
        try{
            $vflProductsEntriesDetails->productsEntriesDetails=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function totalPrice($vflProductsEntriesDetails)
	 {
        try{
            $vtotalPrice=0;
            $vproductsEntriesDetailsTotal=self::total($vflProductsEntriesDetails);
            for($vi=0; $vi<$vproductsEntriesDetailsTotal; $vi++){
                $vtotalPrice+=$vflProductsEntriesDetails->productsEntriesDetails[$vi]->realAmountInPt *
                              $vflProductsEntriesDetails->productsEntriesDetails[$vi]->purchasePrice;
            }
            return $vtotalPrice;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>