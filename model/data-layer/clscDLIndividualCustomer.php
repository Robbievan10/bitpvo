<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLIndividualCustomer.php");


class clscDLIndividualCustomer
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflIndividualsCustomers, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT c_individualcustomer.*, c_customer.*, c_persontype.fldpersonType, c_individualcustomermunicipality.fldmunicipality";
            $vsql.=", c_individualcustomerstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_individualcustomer ";
            $vsql.="INNER JOIN c_customer ON c_individualcustomer.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND c_individualcustomer.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise ON c_customer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_individualcustomermunicipality ON c_customer.id_state=c_individualcustomermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_individualcustomermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_individualcustomerstate ON c_individualcustomermunicipality.id_state=c_individualcustomerstate.id_state ";
			$vsql.=$vfilter . " ";
			//$vsql.="ORDER BY c_individualcustomer.fldfirstName, c_individualcustomer.fldlastName, c_individualcustomer.fldname";
            $vsql.="ORDER BY c_individualcustomer.id_customer";

            self::clean($vflIndividualsCustomers);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vindividualCustomer= new clspFLIndividualCustomer();
                $vindividualCustomer->name=trim($vrow["c_individualcustomer.fldname"]);
                $vindividualCustomer->firstName=trim($vrow["c_individualcustomer.fldfirstName"]);
                $vindividualCustomer->lastName=trim($vrow["c_individualcustomer.fldlastName"]);
                $vindividualCustomer->idCustomer=(int)($vrow["c_customer.id_customer"]);
                $vindividualCustomer->enterprise->idEnterprise=(int)($vrow["c_customer.id_enterprise"]);
                $vindividualCustomer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vindividualCustomer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vindividualCustomer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vindividualCustomer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vindividualCustomer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vindividualCustomer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vindividualCustomer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vindividualCustomer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vindividualCustomer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vindividualCustomer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vindividualCustomer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vindividualCustomer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vindividualCustomer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vindividualCustomer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
                $vindividualCustomer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vindividualCustomer->municipality->state->idState=(int)($vrow["c_customer.id_state"]);
                $vindividualCustomer->municipality->state->state=trim($vrow["c_individualcustomerstate.fldstate"]);
                $vindividualCustomer->municipality->idMunicipality=(int)($vrow["c_customer.id_municipality"]);
                $vindividualCustomer->municipality->municipality=trim($vrow["c_individualcustomermunicipality.fldmunicipality"]);
                $vindividualCustomer->key=trim($vrow["c_customer.fldkey"]);
                $vindividualCustomer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vindividualCustomer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vindividualCustomer->locality=trim($vrow["c_customer.fldlocality"]);
                $vindividualCustomer->street=trim($vrow["c_customer.fldstreet"]);
                $vindividualCustomer->number=trim($vrow["c_customer.fldnumber"]);
                $vindividualCustomer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vindividualCustomer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vindividualCustomer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vindividualCustomer->email=trim($vrow["c_customer.fldemail"]);
                $vindividualCustomer->observation=trim($vrow["c_customer.fldobservation"]);
                
                self::add($vflIndividualsCustomers, $vindividualCustomer);
                unset($vrow, $vindividualCustomer);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function add($vflIndividualsCustomers, $vindividualCustomer)
	 {
        try{
            array_push($vflIndividualsCustomers->individualsCustomers, $vindividualCustomer);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}   
	 }
	
	public static function total($vflIndividualsCustomers)
	 {
        try{
            return count($vflIndividualsCustomers->individualsCustomers);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflIndividualsCustomers)
	 {
        try{
            $vflIndividualsCustomers->individualsCustomers=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
    
    public function __destruct(){ }
 }

?>