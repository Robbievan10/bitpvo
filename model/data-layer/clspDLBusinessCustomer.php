<?php

require_once (dirname(dirname(__FILE__)) . "/data-layer/clspDLCustomer.php");


class clspDLBusinessCustomer
 {
	public function __construct() { }
    
	
    public static function addToDataBase($vflBusinessCustomer, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            if ( clspDLCustomer::addToDataBase($vflBusinessCustomer, $vmySql)==1 ){
                $vsql ="INSERT INTO c_businesscustomer(id_enterprise, id_customer, fldbusinessName) ";
				$vsql.="VALUES(" . $vflBusinessCustomer->enterprise->idEnterprise;
                $vsql.=", " . $vflBusinessCustomer->idCustomer;
                $vsql.=", '" . $vflBusinessCustomer->businessName ."')";
                
                $vmySql->executeSql($vsql);
                if ( $vmySql->getAffectedRowsNumber()==0 ){
                    $vmySql->rollbackTransaction();
                    return 0;
                }
            }
            else{
                 $vmySql->rollbackTransaction();
                 return -1;
            }
            $vmySql->commitTransaction();
			
			unset( $vsql);
			return 1;
		}
		catch (Exception $vexcepcion){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function updateInDataBase($vflBusinessCustomer, $vmySql)
	 {
		try{
            $vmySql->startTransaction();
            $vupdateCustomerStatus=clspDLCustomer::updateInDataBase($vflBusinessCustomer, $vmySql);
            
			$vsql ="UPDATE c_businesscustomer ";
            $vsql.="SET fldbusinessName='" . $vflBusinessCustomer->businessName . "' ";
            $vsql.="WHERE id_enterprise=" . $vflBusinessCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND id_customer=" . $vflBusinessCustomer->idCustomer;
            
            $vmySql->executeSql($vsql);
			if ( ($vupdateCustomerStatus==0) and ($vmySql->getAffectedRowsNumber()==0) ){
                $vmySql->rollbackTransaction();
                return 0;
			}
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflBusinessCustomer, $vmySql)
	 {
		try{
            $vsql ="DELETE FROM c_businesscustomer ";
            $vsql.="WHERE id_enterprise=" . $vflBusinessCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND id_customer=" . $vflBusinessCustomer->idCustomer;
            
            $vmySql->startTransaction();
            $vmySql->executeSql($vsql);
            if ( $vmySql->getAffectedRowsNumber()==1 ){
                if ( clspDLCustomer::deleteInDataBase($vflBusinessCustomer, $vmySql)==0 ){
                    $vmySql->rollbackTransaction();
                    return -1;
                }
            }
            else{
                $vmySql->rollbackTransaction();
                return 0;
            }
            $vmySql->commitTransaction();
            
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
            $vmySql->rollbackTransaction();
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflBusinessCustomer, $vmySql)
	 {
		try{
            $vsql ="SELECT c_businesscustomer.*, c_customer.*, c_persontype.fldpersonType, c_businesscustomermunicipality.fldmunicipality";
            $vsql.=", c_businesscustomerstate.fldstate, c_enterprise.*, c_enterprisemunicipality.fldmunicipality, c_enterprisestate.fldstate ";
			$vsql.="FROM c_businesscustomer ";
            $vsql.="INNER JOIN c_customer ON c_businesscustomer.id_enterprise=c_customer.id_enterprise ";
            $vsql.="AND c_businesscustomer.id_customer=c_customer.id_customer ";
            $vsql.="INNER JOIN c_enterprise ON c_customer.id_enterprise=c_enterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_enterprisemunicipality ON c_enterprise.id_state=c_enterprisemunicipality.id_state ";
            $vsql.="AND c_enterprise.id_municipality=c_enterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_enterprisestate ON c_enterprisemunicipality.id_state=c_enterprisestate.id_state ";
			$vsql.="INNER JOIN c_persontype ON c_customer.id_personType=c_persontype.id_personType ";
			$vsql.="INNER JOIN c_municipality AS c_businesscustomermunicipality ON c_customer.id_state=c_businesscustomermunicipality.id_state ";
			$vsql.="AND c_customer.id_municipality=c_businesscustomermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_businesscustomerstate ON c_businesscustomermunicipality.id_state=c_businesscustomerstate.id_state ";
            $vsql.="WHERE c_businesscustomer.id_enterprise=" . $vflBusinessCustomer->enterprise->idEnterprise . " ";
            $vsql.="AND c_businesscustomer.id_customer=" . $vflBusinessCustomer->idCustomer;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflBusinessCustomer->businessName=trim($vrow["c_businesscustomer.fldbusinessName"]);
                $vflBusinessCustomer->enterprise->municipality->state->idState=(int)($vrow["c_enterprise.id_state"]);
                $vflBusinessCustomer->enterprise->municipality->state->state=trim($vrow["c_enterprisestate.fldstate"]);
                $vflBusinessCustomer->enterprise->municipality->idMunicipality=(int)($vrow["c_enterprise.id_municipality"]);
                $vflBusinessCustomer->enterprise->municipality->municipality=trim($vrow["c_enterprisemunicipality.fldmunicipality"]);
                $vflBusinessCustomer->enterprise->enterprise=trim($vrow["c_enterprise.fldenterprise"]);
                $vflBusinessCustomer->enterprise->locality=trim($vrow["c_enterprise.fldlocality"]);
                $vflBusinessCustomer->enterprise->street=trim($vrow["c_enterprise.fldstreet"]);
                $vflBusinessCustomer->enterprise->number=trim($vrow["c_enterprise.fldnumber"]);                
                $vflBusinessCustomer->enterprise->phoneNumber=trim($vrow["c_enterprise.fldphoneNumber"]);
                $vflBusinessCustomer->enterprise->movilNumber=trim($vrow["c_enterprise.fldmovilNumber"]);
                $vflBusinessCustomer->enterprise->pageWeb=trim($vrow["c_enterprise.fldpageWeb"]);
                $vflBusinessCustomer->enterprise->avatarImage=trim($vrow["c_enterprise.fldavatarImage"]);
                $vflBusinessCustomer->enterprise->logoImage=trim($vrow["c_enterprise.fldlogoImage"]);
                $vflBusinessCustomer->personType->idPersonType=(int)($vrow["c_customer.id_personType"]);
                $vflBusinessCustomer->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vflBusinessCustomer->municipality->state->idState=(int)($vrow["c_customer.id_state"]);
                $vflBusinessCustomer->municipality->state->state=trim($vrow["c_businesscustomerstate.fldstate"]);
                $vflBusinessCustomer->municipality->idMunicipality=(int)($vrow["c_customer.id_municipality"]);
                $vflBusinessCustomer->municipality->municipality=trim($vrow["c_businesscustomermunicipality.fldmunicipality"]);
                $vflBusinessCustomer->key=trim($vrow["c_customer.fldkey"]);
                $vflBusinessCustomer->rfc=trim($vrow["c_customer.fldrfc"]);
                $vflBusinessCustomer->homoclave=trim($vrow["c_customer.fldhomoclave"]);
                $vflBusinessCustomer->locality=trim($vrow["c_customer.fldlocality"]);
                $vflBusinessCustomer->street=trim($vrow["c_customer.fldstreet"]);
                $vflBusinessCustomer->number=trim($vrow["c_customer.fldnumber"]);
                $vflBusinessCustomer->postCode=trim($vrow["c_customer.fldpostCode"]);
                $vflBusinessCustomer->phoneNumber=trim($vrow["c_customer.fldphoneNumber"]);
                $vflBusinessCustomer->movilNumber=trim($vrow["c_customer.fldmovilNumber"]);
                $vflBusinessCustomer->email=trim($vrow["c_customer.fldemail"]);
                $vflBusinessCustomer->observation=trim($vrow["c_customer.fldobservation"]);
				
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
 	
    
	public function __destruct(){ }
 }

?>