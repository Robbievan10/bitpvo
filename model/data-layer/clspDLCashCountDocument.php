<?php

class clspDLCashCountDocument
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflCashCountDocument, $vmySql)
	 {
		try{
            $vflCashCountDocument->idCashCountDocument=self::getIdFromDataBase($vflCashCountDocument, $vmySql);
            
            $vsql ="INSERT INTO p_cashcountdocument(id_enterprise, id_cash, id_cashCount, id_cashCountDocument, id_documentType, fldconcept, fldvalue) ";
			$vsql.="VALUES(" . $vflCashCountDocument->cashCount->cash->enterprise->idEnterprise;
            $vsql.=", " . $vflCashCountDocument->cashCount->cash->idCash;
            $vsql.=", " . $vflCashCountDocument->cashCount->idCashCount;
            $vsql.=", " . $vflCashCountDocument->idCashCountDocument;
            $vsql.=", " . $vflCashCountDocument->documentType->idDocumentType;
            $vsql.=", '" . $vflCashCountDocument->concept . "'";
            $vsql.=", " . $vflCashCountDocument->value . ")";
            
            $vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflCashCountDocument, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM p_cashcountdocument ";
            $vsql.="WHERE id_enterprise=" . $vflCashCountDocument->cashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash='" . $vflCashCountDocument->cashCount->cash->idCash . "' ";
            $vsql.="AND id_cashCount=" . $vflCashCountDocument->cashCount->idCashCount . " ";
            $vsql.="AND id_cashCountDocument=" . $vflCashCountDocument->idCashCountDocument;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    private static function getIdFromDataBase($vflCashCountDocument, $vmySql)
	 {
		try{
            $vidCashCountDocument=1;
			$vsql ="SELECT MAX(id_cashCountDocument) + 1 AS id_cashCountDocumentNew ";
			$vsql.="FROM p_cashcountdocument ";
            $vsql.="WHERE id_enterprise=" . $vflCashCountDocument->cashCount->cash->enterprise->idEnterprise . " ";
            $vsql.="AND id_cash=" . $vflCashCountDocument->cashCount->cash->idCash . " ";
            $vsql.="AND id_cashCount=" . $vflCashCountDocument->cashCount->idCashCount . " ";
            
            $vmySql->executeSql($vsql);
            $vrow=$vmySql->getData();
			if ( (! is_null($vrow)) && ((int)($vrow["id_cashCountDocumentNew"])>0) ){
                $vidCashCountDocument=(int)($vrow["id_cashCountDocumentNew"]);
            }
			$vmySql->freeMemory();
            
            unset($vsql, $vrow);
            return $vidCashCountDocument;
		 }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public function __destruct(){ }
 }

?>