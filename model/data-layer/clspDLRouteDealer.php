<?php

class clspDLRouteDealer
 {
	public function __construct() { }
    
    
	public static function addToDataBase($vflRouteDealer, $vmySql)
	 {
		try{
            date_default_timezone_set('America/Mexico_City');
            $vsql ="INSERT INTO c_routedealer(id_enterprise, id_route, id_dealer, fldassignDate) ";
			$vsql.="VALUES(" . $vflRouteDealer->route->enterprise->idEnterprise;
            $vsql.=", " . $vflRouteDealer->route->idRoute;
			$vsql.=", " . $vflRouteDealer->dealer->idDealer;
			$vsql.=", '" . date("Y-m-d") . "')";
            
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public static function deleteInDataBase($vflRouteDealer, $vmySql)
	 {
		try{
			$vsql ="DELETE FROM c_routedealer ";
            $vsql.="WHERE id_enterprise=" . $vflRouteDealer->route->enterprise->idEnterprise . " ";
            $vsql.="AND id_route=" . $vflRouteDealer->route->idRoute;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getAffectedRowsNumber()==0 ){
                return 0;
			}
            
			unset($vsql);
			return 1;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public static function queryToDataBase($vflRouteDealer, $vmySql)
	 {
		try{
            $vsql ="SELECT c_routedealer.*, c_route.*, c_routeenterprise.*, c_routeenterprisemunicipality.fldmunicipality, c_routeenterprisestate.fldstate ";
            $vsql.=", c_dealer.*, c_dealerenterprise.*, c_dealerenterprisemunicipality.fldmunicipality, c_dealerenterprisestate.fldstate ";
            $vsql.=", c_dealermunicipality.fldmunicipality, c_dealerstate.fldstate ";
            $vsql.="FROM c_routedealer ";
            $vsql.="INNER JOIN c_route ON c_routedealer.id_enterprise=c_route.id_enterprise ";
            $vsql.="AND c_routedealer.id_route=c_route.id_route ";
            $vsql.="INNER JOIN c_enterprise AS c_routeenterprise ON c_route.id_enterprise=c_routeenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_routeenterprisemunicipality ON c_routeenterprise.id_state=c_routeenterprisemunicipality.id_state ";
            $vsql.="AND c_routeenterprise.id_municipality=c_routeenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_routeenterprisestate ON c_routeenterprisemunicipality.id_state=c_routeenterprisestate.id_state ";
            $vsql.="INNER JOIN c_dealer ON c_routedealer.id_dealer=c_dealer.id_dealer ";
            $vsql.="INNER JOIN c_enterprise AS c_dealerenterprise ON c_dealer.id_enterprise=c_dealerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_dealerenterprisemunicipality ON c_dealerenterprise.id_state=c_dealerenterprisemunicipality.id_state ";
            $vsql.="AND c_dealerenterprise.id_municipality=c_dealerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_dealerenterprisestate ON c_dealerenterprisemunicipality.id_state=c_dealerenterprisestate.id_state ";
			$vsql.="INNER JOIN c_municipality AS c_dealermunicipality ON c_dealer.id_state=c_dealermunicipality.id_state ";
			$vsql.="AND c_dealer.id_municipality=c_dealermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_dealerstate ON c_dealermunicipality.id_state=c_dealerstate.id_state ";
            $vsql.="WHERE c_routedealer.id_enterprise=" . $vflRouteDealer->route->enterprise->idEnterprise . " ";
            $vsql.="AND c_routedealer.id_route=" . $vflRouteDealer->route->idRoute;
			
			$vmySql->executeSql($vsql);
			if ( $vmySql->getConsultedRowsNumber()==1 ){
				$vrow=$vmySql->getDataAlias();
                $vflRouteDealer->route->enterprise->municipality->state->idState=(int)($vrow["c_routeenterprise.id_state"]);
                $vflRouteDealer->route->enterprise->municipality->state->state=trim($vrow["c_routeenterprisestate.fldstate"]);
                $vflRouteDealer->route->enterprise->municipality->idMunicipality=(int)($vrow["c_routeenterprise.id_municipality"]);
                $vflRouteDealer->route->enterprise->municipality->municipality=trim($vrow["c_routeenterprisemunicipality.fldmunicipality"]);
                $vflRouteDealer->route->enterprise->enterprise=trim($vrow["c_routeenterprise.fldenterprise"]);
                $vflRouteDealer->route->enterprise->locality=trim($vrow["c_routeenterprise.fldlocality"]);
                $vflRouteDealer->route->enterprise->street=trim($vrow["c_routeenterprise.fldstreet"]);
                $vflRouteDealer->route->enterprise->number=trim($vrow["c_routeenterprise.fldnumber"]);
                $vflRouteDealer->route->enterprise->phoneNumber=trim($vrow["c_routeenterprise.fldphoneNumber"]);
                $vflRouteDealer->route->enterprise->movilNumber=trim($vrow["c_routeenterprise.fldmovilNumber"]);
                $vflRouteDealer->route->enterprise->pageWeb=trim($vrow["c_routeenterprise.fldpageWeb"]);
                $vflRouteDealer->route->route=trim($vrow["c_route.fldroute"]);
                $vflRouteDealer->route->description=trim($vrow["c_route.flddescription"]);
                $vflRouteDealer->route->observation=trim($vrow["c_route.fldobservation"]);
                $vflRouteDealer->dealer->idDealer=(int)($vrow["c_routedealer.id_dealer"]);
                $vflRouteDealer->dealer->enterprise->municipality->state->idState=(int)($vrow["c_dealerenterprise.id_state"]);
                $vflRouteDealer->dealer->enterprise->municipality->state->state=trim($vrow["c_dealerenterprisestate.fldstate"]);
                $vflRouteDealer->dealer->enterprise->municipality->idMunicipality=(int)($vrow["c_dealerenterprise.id_municipality"]);
                $vflRouteDealer->dealer->enterprise->municipality->municipality=trim($vrow["c_dealerenterprisemunicipality.fldmunicipality"]);
                $vflRouteDealer->dealer->enterprise->enterprise=trim($vrow["c_dealerenterprise.fldenterprise"]);
                $vflRouteDealer->dealer->enterprise->locality=trim($vrow["c_dealerenterprise.fldlocality"]);
                $vflRouteDealer->dealer->enterprise->street=trim($vrow["c_dealerenterprise.fldstreet"]);
                $vflRouteDealer->dealer->enterprise->number=trim($vrow["c_dealerenterprise.fldnumber"]);
                $vflRouteDealer->dealer->enterprise->phoneNumber=trim($vrow["c_dealerenterprise.fldphoneNumber"]);
                $vflRouteDealer->dealer->enterprise->movilNumber=trim($vrow["c_dealerenterprise.fldmovilNumber"]);
                $vflRouteDealer->dealer->enterprise->pageWeb=trim($vrow["c_dealerenterprise.fldpageWeb"]);
                $vflRouteDealer->dealer->municipality->state->idState=(int)($vrow["c_dealer.id_state"]);
                $vflRouteDealer->dealer->municipality->state->state=trim($vrow["c_dealerstate.fldstate"]);
                $vflRouteDealer->dealer->municipality->idMunicipality=(int)($vrow["c_dealer.id_municipality"]);
                $vflRouteDealer->dealer->municipality->municipality=trim($vrow["c_dealermunicipality.fldmunicipality"]);
                $vflRouteDealer->dealer->name=trim($vrow["c_dealer.fldname"]);
                $vflRouteDealer->dealer->firstName=trim($vrow["c_dealer.fldfirstName"]);
                $vflRouteDealer->dealer->lastName=trim($vrow["c_dealer.fldlastName"]);
                $vflRouteDealer->dealer->locality=trim($vrow["c_dealer.fldlocality"]);
                $vflRouteDealer->dealer->street=trim($vrow["c_dealer.fldstreet"]);
                $vflRouteDealer->dealer->number=trim($vrow["c_dealer.fldnumber"]);
                $vflRouteDealer->dealer->postCode=trim($vrow["c_dealer.fldpostCode"]);
                $vflRouteDealer->dealer->phoneNumber=trim($vrow["c_dealer.fldphoneNumber"]);
                $vflRouteDealer->dealer->movilNumber=trim($vrow["c_dealer.fldmovilNumber"]);
                $vflRouteDealer->dealer->observation=trim($vrow["c_dealer.fldobservation"]);
                
				unset($vrow);
			}
			else{
				return 0;
			}
			$vmySql->freeMemory();
			
			unset($vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct(){ }
 }

?>