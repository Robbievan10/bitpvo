<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntryPayment.php");


class clscDLProductEntryPayment
 {
    public function __construct() { }
    
    
    public static function queryToDataBase($vflProductsEntriesPayments, $vfilter, $vmySql)
	 {
		try{
			$vsql ="SELECT p_productentrypayment.*, p_productentry.*, c_productentryenterprise.*, c_productentryenterprisemunicipality.fldmunicipality";
            $vsql.=", c_productentryenterprisestate.fldstate, c_provider.*, c_providerenterprise.*, c_providerenterprisemunicipality.fldmunicipality";
            $vsql.=", c_providerenterprisestate.fldstate, c_persontype.fldpersonType, c_providermunicipality.fldmunicipality, c_providerstate.fldstate";
            $vsql.=", c_operationtype.fldoperationType, c_operationstatus.fldoperationStatus, p_payment.*, p_paymententerprise.*";
            $vsql.=", p_paymententerprisemunicipality.fldmunicipality, p_paymententerprisestate.fldstate, c_paymenttype.fldpaymentType ";
            $vsql.="FROM p_productentrypayment ";
			$vsql.="INNER JOIN p_productentry ON p_productentrypayment.id_enterprise=p_productentry.id_enterprise ";
            $vsql.="AND p_productentrypayment.id_productEntry=p_productentry.id_productEntry ";
            $vsql.="INNER JOIN c_enterprise AS c_productentryenterprise ON p_productentry.id_enterprise=c_productentryenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_productentryenterprisemunicipality ON c_productentryenterprise.id_state=c_productentryenterprisemunicipality.id_state ";
            $vsql.="AND c_productentryenterprise.id_municipality=c_productentryenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_productentryenterprisestate ON c_productentryenterprisemunicipality.id_state=c_productentryenterprisestate.id_state ";
            $vsql.="INNER JOIN c_provider ON p_productentry.id_enterprise=c_provider.id_enterprise ";
            $vsql.="AND p_productentry.id_provider=c_provider.id_provider ";
            $vsql.="INNER JOIN c_enterprise AS c_providerenterprise ON c_provider.id_enterprise=c_providerenterprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS c_providerenterprisemunicipality ON c_providerenterprise.id_state=c_providerenterprisemunicipality.id_state ";
            $vsql.="AND c_providerenterprise.id_municipality=c_providerenterprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS c_providerenterprisestate ON c_providerenterprisemunicipality.id_state=c_providerenterprisestate.id_state ";
            $vsql.="INNER JOIN c_persontype ON c_provider.id_personType=c_persontype.id_personType ";
            $vsql.="INNER JOIN c_municipality AS c_providermunicipality ON c_provider.id_state=c_providermunicipality.id_state ";
			$vsql.="AND c_provider.id_municipality=c_providermunicipality.id_municipality ";
			$vsql.="INNER JOIN c_state AS c_providerstate ON c_providermunicipality.id_state=c_providerstate.id_state ";
            $vsql.="INNER JOIN c_operationtype ON p_productentry.id_operationType=c_operationtype.id_operationType ";
            $vsql.="INNER JOIN c_operationstatus ON p_productentry.id_operationStatus=c_operationstatus.id_operationStatus ";
            $vsql.="INNER JOIN p_payment ON p_productentrypayment.id_enterprise=p_payment.id_enterprise ";
            $vsql.="AND p_productentrypayment.id_payment=p_payment.id_payment ";
            $vsql.="INNER JOIN c_enterprise AS p_paymententerprise ON p_payment.id_enterprise=p_paymententerprise.id_enterprise ";
            $vsql.="INNER JOIN c_municipality AS p_paymententerprisemunicipality ON p_paymententerprise.id_state=p_paymententerprisemunicipality.id_state ";
            $vsql.="AND p_paymententerprise.id_municipality=p_paymententerprisemunicipality.id_municipality ";
            $vsql.="INNER JOIN c_state AS p_paymententerprisestate ON p_paymententerprisemunicipality.id_state=p_paymententerprisestate.id_state ";
			$vsql.="INNER JOIN c_paymenttype ON p_payment.id_paymentType=c_paymenttype.id_paymentType ";
            $vsql.=$vfilter . " ";
			$vsql.="ORDER BY p_productentrypayment.id_enterprise, p_productentrypayment.id_productEntry, p_productentrypayment.id_payment";
            
            self::clean($vflProductsEntriesPayments);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getDataAlias();
                $vproductEntryPayment= new clspFLProductEntryPayment();
				$vproductEntryPayment->idPayment=(int)($vrow["p_productentrypayment.id_payment"]);
                $vproductEntryPayment->productEntry->idProductEntry=trim($vrow["p_productentrypayment.id_productEntry"]);
                $vproductEntryPayment->productEntry->enterprise->idEnterprise=(int)($vrow["p_productentrypayment.id_enterprise"]);
                $vproductEntryPayment->productEntry->enterprise->municipality->state->idState=(int)($vrow["c_productentryenterprise.id_state"]);
                $vproductEntryPayment->productEntry->enterprise->municipality->state->state=trim($vrow["c_productentryenterprisestate.fldstate"]);
                $vproductEntryPayment->productEntry->enterprise->municipality->idMunicipality=(int)($vrow["c_productentryenterprise.id_municipality"]);
                $vproductEntryPayment->productEntry->enterprise->municipality->municipality=trim($vrow["c_productentryenterprisemunicipality.fldmunicipality"]);
                $vproductEntryPayment->productEntry->enterprise->enterprise=trim($vrow["c_productentryenterprise.fldenterprise"]);
                $vproductEntryPayment->productEntry->enterprise->locality=trim($vrow["c_productentryenterprise.fldlocality"]);
                $vproductEntryPayment->productEntry->enterprise->street=trim($vrow["c_productentryenterprise.fldstreet"]);
                $vproductEntryPayment->productEntry->enterprise->number=trim($vrow["c_productentryenterprise.fldnumber"]);
                $vproductEntryPayment->productEntry->enterprise->phoneNumber=trim($vrow["c_productentryenterprise.fldphoneNumber"]);
                $vproductEntryPayment->productEntry->enterprise->movilNumber=trim($vrow["c_productentryenterprise.fldmovilNumber"]);
                $vproductEntryPayment->productEntry->enterprise->pageWeb=trim($vrow["c_productentryenterprise.fldpageWeb"]);
                $vproductEntryPayment->productEntry->provider->idProvider=(int)($vrow["p_productentry.id_provider"]);
                $vproductEntryPayment->productEntry->provider->enterprise->idEnterprise=(int)($vrow["p_productentrypayment.id_enterprise"]);
                $vproductEntryPayment->productEntry->provider->enterprise->municipality->state->idState=(int)($vrow["c_providerenterprise.id_state"]);
                $vproductEntryPayment->productEntry->provider->enterprise->municipality->state->state=trim($vrow["c_providerenterprisestate.fldstate"]);
                $vproductEntryPayment->productEntry->provider->enterprise->municipality->idMunicipality=(int)($vrow["c_providerenterprise.id_municipality"]);
                $vproductEntryPayment->productEntry->provider->enterprise->municipality->municipality=trim($vrow["c_providerenterprisemunicipality.fldmunicipality"]);
                $vproductEntryPayment->productEntry->provider->enterprise->enterprise=trim($vrow["c_providerenterprise.fldenterprise"]);
                $vproductEntryPayment->productEntry->provider->enterprise->locality=trim($vrow["c_providerenterprise.fldlocality"]);
                $vproductEntryPayment->productEntry->provider->enterprise->street=trim($vrow["c_providerenterprise.fldstreet"]);
                $vproductEntryPayment->productEntry->provider->enterprise->number=trim($vrow["c_providerenterprise.fldnumber"]);                
                $vproductEntryPayment->productEntry->provider->enterprise->phoneNumber=trim($vrow["c_providerenterprise.fldphoneNumber"]);
                $vproductEntryPayment->productEntry->provider->enterprise->movilNumber=trim($vrow["c_providerenterprise.fldmovilNumber"]);
                $vproductEntryPayment->productEntry->provider->enterprise->pageWeb=trim($vrow["c_providerenterprise.fldpageWeb"]);
				$vproductEntryPayment->productEntry->provider->personType->idPersonType=(int)($vrow["c_provider.id_personType"]);
				$vproductEntryPayment->productEntry->provider->personType->personType=trim($vrow["c_persontype.fldpersonType"]);
                $vproductEntryPayment->productEntry->provider->rfc=trim($vrow["c_provider.fldrfc"]);
                $vproductEntryPayment->productEntry->provider->homoclave=trim($vrow["c_provider.fldhomoclave"]);
                $vproductEntryPayment->productEntry->provider->locality=trim($vrow["c_provider.fldlocality"]);
                $vproductEntryPayment->productEntry->provider->street=trim($vrow["c_provider.fldstreet"]);
                $vproductEntryPayment->productEntry->provider->number=trim($vrow["c_provider.fldnumber"]);
                $vproductEntryPayment->productEntry->provider->postCode=trim($vrow["c_provider.fldpostCode"]);
                $vproductEntryPayment->productEntry->provider->phoneNumber=trim($vrow["c_provider.fldphoneNumber"]);
                $vproductEntryPayment->productEntry->provider->movilNumber=trim($vrow["c_provider.fldmovilNumber"]);
                $vproductEntryPayment->productEntry->provider->email=trim($vrow["c_provider.fldemail"]);
                $vproductEntryPayment->productEntry->provider->pageWeb=trim($vrow["c_provider.fldpageWeb"]);
                $vproductEntryPayment->productEntry->provider->observation=trim($vrow["c_provider.fldobservation"]);
                $vproductEntryPayment->productEntry->operationType->idOperationType=(int)($vrow["p_productentry.id_operationType"]);
                $vproductEntryPayment->productEntry->operationType->operationType=trim($vrow["c_operationtype.fldoperationType"]);
                $vproductEntryPayment->productEntry->operationStatus->idOperationStatus=(int)($vrow["p_productentry.id_operationStatus"]);
                $vproductEntryPayment->productEntry->operationStatus->operationStatus=trim($vrow["c_operationstatus.fldoperationStatus"]);
                $vproductEntryPayment->productEntry->saleFolio=trim($vrow["p_productentry.fldsaleFolio"]);
                $vproductEntryPayment->productEntry->recordDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldrecordDate"])));
                $vproductEntryPayment->productEntry->productEntryDate=date("m/d/Y", strtotime(trim($vrow["p_productentry.fldproductEntryDate"])));
                $vproductEntryPayment->enterprise->idEnterprise=(int)($vrow["p_productentrypayment.id_enterprise"]);
				$vproductEntryPayment->enterprise->municipality->state->idState=(int)($vrow["p_paymententerprise.id_state"]);
                $vproductEntryPayment->enterprise->municipality->state->state=trim($vrow["p_paymententerprisestate.fldstate"]);
                $vproductEntryPayment->enterprise->municipality->idMunicipality=(int)($vrow["p_paymententerprise.id_municipality"]);
                $vproductEntryPayment->enterprise->municipality->municipality=trim($vrow["p_paymententerprisemunicipality.fldmunicipality"]);
                $vproductEntryPayment->enterprise->enterprise=trim($vrow["p_paymententerprise.fldenterprise"]);
                $vproductEntryPayment->enterprise->locality=trim($vrow["p_paymententerprise.fldlocality"]);
                $vproductEntryPayment->enterprise->street=trim($vrow["p_paymententerprise.fldstreet"]);
                $vproductEntryPayment->enterprise->number=trim($vrow["p_paymententerprise.fldnumber"]);
                $vproductEntryPayment->enterprise->phoneNumber=trim($vrow["p_paymententerprise.fldphoneNumber"]);
                $vproductEntryPayment->enterprise->movilNumber=trim($vrow["p_paymententerprise.fldmovilNumber"]);
                $vproductEntryPayment->enterprise->pageWeb=trim($vrow["p_paymententerprise.fldpageWeb"]);
				$vproductEntryPayment->paymentType->idPaymentType=(int)($vrow["p_payment.id_paymentType"]);
				$vproductEntryPayment->paymentType->paymentType=trim($vrow["c_paymenttype.fldpaymentType"]);
				$vproductEntryPayment->recordDate=date("m/d/Y", strtotime(trim($vrow["p_payment.fldrecordDate"])));
				$vproductEntryPayment->paymentDate=date("m/d/Y", strtotime(trim($vrow["p_payment.fldpaymentDate"])));
				$vproductEntryPayment->amount=(float)($vrow["p_payment.fldamount"]);
                
                self::add($vflProductsEntriesPayments, $vproductEntryPayment);
                unset($vrow, $vproductEntryPayment);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
		}
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
    public static function queryGroupByProviderToDataBase($vflProductsEntriesPayments, $vfilter, $vmySql)
	 {
        try{
            $vsql ="SELECT p_productentry.id_provider, c_provider.id_personType";
            $vsql.=", IF(c_provider.id_personType=1, c_individualprovider.fldname, c_businessprovider.fldbusinessName) AS providerName ";
            $vsql.=", IF(c_provider.id_personType=1, c_individualprovider.fldfirstName, null) AS providerFirstName ";
            $vsql.=", IF(c_provider.id_personType=1, c_individualprovider.fldlastName, null) AS providerLastName ";
            $vsql.=", SUM(p_payment.fldamount) AS amountTotal "; 
            $vsql.="FROM p_productentrypayment "; 
            $vsql.="INNER JOIN p_productentry ON p_productentrypayment.id_enterprise=p_productentry.id_enterprise ";  
            $vsql.="AND p_productentrypayment.id_productentry=p_productentry.id_productentry "; 
            $vsql.="INNER JOIN c_provider ON p_productentry.id_enterprise=c_provider.id_enterprise "; 
            $vsql.="AND p_productentry.id_provider=c_provider.id_provider "; 
            $vsql.="LEFT JOIN c_individualprovider ON c_provider.id_enterprise=c_individualprovider.id_enterprise "; 
            $vsql.="AND c_provider.id_provider=c_individualprovider.id_provider "; 
            $vsql.="LEFT JOIN c_businessprovider ON c_provider.id_enterprise=c_businessprovider.id_enterprise "; 
            $vsql.="AND c_provider.id_provider=c_businessprovider.id_provider "; 
            $vsql.="INNER JOIN p_payment ON p_productentrypayment.id_enterprise=p_payment.id_enterprise ";  
            $vsql.="AND p_productentrypayment.id_payment=p_payment.id_payment ";
            $vsql.=$vfilter . " ";
            $vsql.="GROUP BY p_productentry.id_provider ";
            $vsql.="ORDER BY providerName, providerFirstName, providerLastName";
            
            self::clean($vflProductsEntriesPayments);
            
			$vmySql->executeSql($vsql);
            $vrowsTotal=$vmySql->getConsultedRowsNumber();
            for($vrowNumber=0; $vrowNumber<$vrowsTotal; $vrowNumber++){
                $vrow=$vmySql->getData();
                $vproductEntryPayment= new clspFLProductEntryPayment();
                $vproductEntryPayment->productEntry->provider->idProvider=(int)($vrow["id_provider"]);
                $vproductEntryPayment->productEntry->provider->personType->idPersonType=(int)($vrow["id_personType"]);
                $vproductEntryPayment->productEntry->provider->name=trim($vrow["providerName"]);
                if ( $vproductEntryPayment->productEntry->provider->personType->idPersonType==1 ){
                    $vproductEntryPayment->productEntry->provider->firstName=trim($vrow["providerFirstName"]);
                    $vproductEntryPayment->productEntry->provider->lastName=trim($vrow["providerLastName"]);
                }
                $vproductEntryPayment->amount=(float)($vrow["amountTotal"]);
                
                self::add($vflProductsEntriesPayments, $vproductEntryPayment);
                unset($vrow, $vproductEntryPayment);
            }
            if ( $vrowNumber<=0 ){
                return 0;
            }
			$vmySql->freeMemory();
			
			unset($vfilter, $vsql);
			return 1;
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
    
	private static function add($vflProductsEntriesPayments, $vproductEntryPayment)
	 {
        try{
            array_push($vflProductsEntriesPayments->productsEntriesPayments, $vproductEntryPayment);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	public static function total($vflProductsEntriesPayments)
	 {
        try{
            return count($vflProductsEntriesPayments->productsEntriesPayments);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	
	private static function clean($vflProductsEntriesPayments)
	 {
        try{
            $vflProductsEntriesPayments->productsEntriesPayments=array();
        }
        catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
	 
	 public static function totalAmount($vflProductsEntriesPayments)
	 {
        try{
            $vtotalAmount=0;
            $vproductsEntriesPaymentsTotal=self::total($vflProductsEntriesPayments);
            for($vi=0; $vi<$vproductsEntriesPaymentsTotal; $vi++){
                $vtotalAmount+=$vflProductsEntriesPayments->productsEntriesPayments[$vi]->amount;
            }
            return $vtotalAmount;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
    public function __destruct(){ }
 }

?>