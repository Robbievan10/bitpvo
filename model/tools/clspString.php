<?php

class clspString
 {
	private $string;
	
	public function __construct($vstring)
	 {
		$this->string=trim($vstring);
		unset($vstring);
	 }
	
    
	public function getAdjustedString($vlength)
	 {
        $vadjustedString="";
		
	 	for($vi=$this->getLength(); $vi<=$vlength-1;  $vi++){
            $vadjustedString.="0";
        }

		unset($vlength, $vi);
		return $vadjustedString . $this->string;
	 }
     
	public function getLength()
	 {
		return strlen($this->string);
	 }
     
	public function getFilteredString()
	 {
		$vpreviousASCII=0;
		$vfilteredString="";
        
		for ($vi=0; $vi<$this->getFormattedLength("utf-8"); $vi++){	
            $vchar=$this->getFormattedString($vi, 1, "utf-8");
		 	$vascii=ord($vchar);
		 	if ( $vpreviousASCII!=13 ){			
                switch ($vascii){
                    case 10: $vfilteredString.=chr(127);
                             break;
					case 13: $vfilteredString.=chr(127);
                             break;
					default: $vfilteredString.=$vchar;	
                             break;
                }
            }
			$vpreviousASCII=$vascii;
        }
		
        unset($vpreviousASCII, $vi, $vchar, $vascii);
		return $vfilteredString;
	 }
     
	public function getFormattedLength($vformat)
	 {
		return mb_strlen($this->string, $vformat);
	 }
     
	public function getFormattedString($vstart, $vlength, $vformat)
	 {
		return mb_substr($this->string, $vstart, $vlength, $vformat);
	 }
     
	public function getStringMatch($vchar, $vinitialMatch, $vfinalMatch)
	 {
	 	$vcharAmount=0;
		$vstring="";

		for ($vi=0; $vi<$this->getLength(); $vi++){
		 	$vstringChar=substr($this->string, $vi, 1);
			if ( strcmp($vchar, $vstringChar)==0 ){
                $vcharAmount+=1;
            }
            if ( ($vcharAmount==$vinitialMatch) && ($vcharAmount<$vfinalMatch) && (strcmp($vchar, $vstringChar)!=0) ){
                $vstring.=$vstringChar;
            }
		}
		
		unset($vchar, $vinitialMatch, $vfinalMatch, $vcharAmount, $vi, $vstringChar);
		return $vstring;
	 }
	
    
	public function __destruct()
	 {
		unset($this->string);
	 }
 }

?>