<?php

class clspText
 {
	protected $name;
	protected $value;
	protected $response;
	
	public function __construct($vname, $vresponse)
	 {
		$this->name=trim($vname);
		$this->response=$vresponse;

		unset($vname, $vresponse);
	 }
	
	public function setValue($vvalue) 
	 {
		$this->value=trim($vvalue);
	 	$this->response->script("setValue('" . $this->name . "','" . $this->value . "');");
	 }
	public function enabled()
	 {
	 	$this->response->script("enabled('" . $this->name . "');");
	 } 
	public function disabled()
	 {
	 	$this->response->script("disabled('" . $this->name . "');");
	 }
	public function focus()
	 {
	 	$this->response->script("_focus('" . $this->name . "');");
	 }
	
	public function __destruct()
	 {
		unset($this->name, $this->value, $this->response);
	 }
 }

?>