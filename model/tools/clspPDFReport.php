<?php

require_once (dirname(dirname(dirname(__FILE__)))  . "/tools/fpdi/fpdi.php");


class clspPDFReport
 {
    private $fpdi;
    private $sourceFile;
    private $outFileName;
    public $x;
    public $y;
    
 	public function __construct($vsourceFile, $voutFileName, $vorientation, $vpageSize)
     {
		$this->fpdi= & new FPDI($vorientation, "pt", $vpageSize);
		$this->fpdi->setAuthor("PVO");
        $this->sourceFile=$vsourceFile;
        $this->outFileName=$voutFileName;
        $this->x=0;
        $this->y=0;
	 }

	
    public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
     
     
    public function addPage()
	 {
        try{
            $this->fpdi->addpage();
            $this->fpdi->setSourceFile($this->sourceFile);
            $this->fpdi->useTemplate($this->fpdi->importPage(1, "/MediaBox"), 0, 0);
            $this->x=0;
            $this->y=0;
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public function showPage($F = false)
	 {
        try{
        	if (!$F) {
        		$this->fpdi->Output($this->outFileName, "I");
        	}else{
        		$this->fpdi->Output($this->outFileName, "F");
        	}
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public function printImage($vpath, $vwidth, $vheight, $vformat)
	 {
        try{
            $this->fpdi->Image($vpath, $this->x, $this->y, $vwidth, $vheight, $vformat);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
	public function printMultiLine($vwidth, $vheight, $vtext, $vletterSize, $vletterBold, $vletterAlign, $vborderType)
	 {
        try{
            $this->fpdi->SetFont("Arial", $vletterBold, $vletterSize);
            $this->fpdi->SetTextColor(0, 0, 0);
            $this->fpdi->SetXY($this->x, $this->y);
            $this->fpdi->MultiCell($vwidth, $vheight, $vtext, $vborderType, $vletterAlign, 0);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public function printLine($vx1, $vy1, $vx2, $vy2, $vwidth)
	 {
        try{
            $this->fpdi->SetLineWidth($vwidth);
            $this->fpdi->Line($vx1, $vy1, $vx2, $vy2);
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
    public function getY()
	 {
        try{
            return $this->fpdi->GetY();
        }
		catch (Exception $vexception){
			throw new Exception($vexception->getMessage(), $vexception->getCode());
		}
	 }
     
     
	public function __destruct()
	 {
		unset($this->fpdi, $this->sourceFile, $this->outFileName, $this->x, $this->y);
	 }
 }

?>