<?php

class clspCheckbox
 {
	protected $name;
	protected $response;
	
	public function __construct($vname, $vresponse)
	 {
		$this->name=trim($vname);
		$this->response=$vresponse;

		unset($vname, $vresponse);
	 }
	 
    public function enabled()
	 {
	 	$this->response->script("enabled('" . $this->name . "');");
	 } 
	public function disabled()
	 {
	 	$this->response->script("disabled('" . $this->name . "');");
	 }
	public function checked()
	 {
	 	$this->response->script("checked('" . $this->name . "');");
	 }
	public function unchecked()
	 {
	 	$this->response->script("unchecked('" . $this->name . "');");
	 }
	
    public function __destruct()
	 {
		unset($this->name, $this->response);
	 }
 }

?>