<?php

require_once("clspText.php");

class clspTextArea extends clspText
 {
	
	public function __construct($vname, $vresponse)
	 {
		parent::__construct($vname, $vresponse);

		unset($vname, $vresponse);
	 }
	
	public function __destruct()
	 {
		parent::__destruct();
	 }
 }

?>