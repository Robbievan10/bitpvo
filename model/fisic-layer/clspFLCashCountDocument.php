<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLDocumentType.php");

class clspFLCashCountDocument
 {
	public $idCashCountDocument;
    public $cashCount;
    public $documentType;
    public $concept;
    public $value;
    public $property;
	
	public function __construct()
	 {
		$this->idCashCountDocument=0;
		$this->cashCount= new clspFLCashCount();
        $this->documentType= new clspFLDocumentType();
        $this->value=0;
        $this->property=array();
	 }
	
	public function __get($vproperty)
     { 
        if( isset($vproperty) ){
            if (array_key_exists($vproperty, $this->property)) {
                return $this->property[$vproperty];
            }
            else{
                throw new Exception("Property doesn't exist: $vproperty");
            }
        }
        else{
            return $this->vproperty;
        }
     }
    
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
            $this->property[$vproperty]=$vvalue;
        }
		else{
            $this->vproperty=$vvalue;
        }
	 }
	
	public function __destruct()
	 {
		unset($this->idCashCountDocument, $this->cashCount, $this->documentType, $this->concept, $this->value, $this->property);
	 }
 }

?>