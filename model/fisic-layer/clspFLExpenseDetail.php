<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLExpense.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLRoute.php");


class clspFLExpenseDetail
 {
	public $idExpenseDetail;
    public $expense;
    public $route;
    public $recordDate;
    public $amount;
    public $observation;
	
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idExpenseDetail=0;
		$this->expense= new clspFLExpense();
        $this->route= new clspFLRoute();
        $this->recordDate= date("Y-m-d");
        $this->amount=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idExpenseDetail, $this->expense, $this->route, $this->recordDate, $this->amount, $this->observation);
	 }
 }

?>