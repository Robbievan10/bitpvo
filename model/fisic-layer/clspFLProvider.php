<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPersonType.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMunicipality.php");


class clspFLProvider
 {
	public $idProvider;
    public $enterprise;
	public $personType;
	public $municipality;
    public $key;
	public $rfc;
	public $homoclave;
	public $locality;
	public $street;
	public $number;
	public $postCode;
	public $phoneNumber;
    public $movilNumber;
    public $email;
    public $pageWeb;
	public $observation;
    public $property;
		
	public function __construct()
	 {
		$this->idProvider=0;
		$this->enterprise= new clspFLEnterprise();
        $this->personType= new clspFLPersonType();
		$this->municipality= new clspFLMunicipality();
        $this->property=array();
	 }
	
	public function __get($vproperty)
     { 
        if( isset($vproperty) ){
            if (array_key_exists($vproperty, $this->property)) {
                return $this->property[$vproperty];
            }
            else{
                throw new Exception("Property doesn't exist: $vproperty");
            }
        }
        else{
            return $this->vproperty;
        }
     }
    
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
            $this->property[$vproperty]=$vvalue;
        }
		else{
            $this->vproperty=$vvalue;
        }
	 }
	
	public function __destruct()
	 {
		unset($this->idProvider, $this->enterprise, $this->personType, $this->municipality, $this->key, $this->rfc, $this->homoclave, 
              $this->locality, $this->street, $this->number, $this->postCode, $this->phoneNumber, $this->movilNumber,
              $this->email, $this->pageWeb, $this->observation, $this->property);
	 }
 }

?>