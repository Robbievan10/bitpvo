<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPayment.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntry.php");

class clspFLProductEntryPayment extends clspFLPayment
 {
	public $productEntry;
    
	public function __construct()
	 {
		parent::__construct();
        $this->productEntry= new clspFLProductEntry();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		parent::__destruct();
		unset($this->productEntry);
	 }
 }

?>