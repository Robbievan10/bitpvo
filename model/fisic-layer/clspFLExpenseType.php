<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");


class clspFLExpenseType
 {
	public $idExpenseType;
    public $enterprise;
	public $expenseType;
	
	public function __construct()
	 {
		$this->idExpenseType=0;
        $this->enterprise= new clspFLEnterprise();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->idExpenseType, $this->enterprise, $this->expenseType);
	 }
 }

?>