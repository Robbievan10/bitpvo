<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPersonType.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMunicipality.php");


class clspFLCustomer
 {
	public $idCustomer;
    public $enterprise;
	public $personType;
	public $municipality;
    public $key;
	public $rfc;
	public $homoclave;
	public $locality;
	public $street;
	public $number;
	public $postCode;
	public $phoneNumber;
    public $movilNumber;
    public $email;
	public $observation;
    public $property;
    public $factura;
		
	public function __construct()
	 {
		$this->idCustomer=0;
		$this->enterprise= new clspFLEnterprise();
        $this->personType= new clspFLPersonType();
		$this->municipality= new clspFLMunicipality();
		$this->factura=0;
        $this->property=array();
	 }
	
	public function __get($vproperty)
     { 
        if( isset($vproperty) ){
            if (array_key_exists($vproperty, $this->property)) {
                return $this->property[$vproperty];
            }
            else{
                throw new Exception("Property doesn't exist: $vproperty");
            }
        }
        else{
            return $this->vproperty;
        }
     }
    
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
            $this->property[$vproperty]=$vvalue;
        }
		else{
            $this->vproperty=$vvalue;
        }
	 }
	
	public function __destruct()
	 {
		unset($this->idCustomer, $this->enterprise, $this->personType, $this->municipality, $this->key, $this->rfc, $this->homoclave, 
              $this->locality, $this->street, $this->number, $this->postCode, $this->phoneNumber, $this->movilNumber,
              $this->email, $this->observation, $this->factura, $this->property);
	 }
 }

?>