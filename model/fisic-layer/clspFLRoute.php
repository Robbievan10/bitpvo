<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");


class clspFLRoute
 {
	public $idRoute;
    public $enterprise;
    public $route;
    public $description;
	public $observation;
		
	public function __construct()
	 {
		$this->idRoute=0;
		$this->enterprise= new clspFLEnterprise();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idRoute, $this->enterprise, $this->route, $this->description, $this->observation);
	 }
 }

?>