<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPaymentType.php");

class clspFLPayment
 {
	public $idPayment;
    public $enterprise;
    public $paymentType;
    public $recordDate;
	public $paymentDate;
    public $amount;
		
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idPayment=0;
		$this->enterprise= new clspFLEnterprise();
        $this->paymentType= new clspFLPaymentType();
        $this->recordDate= date("Y-m-d");
        $this->paymentDate= date("Y-m-d");
        $this->amount=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idPayment, $this->enterprise, $this->paymentType, $this->recordDate, $this->paymentDate, $this->amount);
	 }
 }

?>