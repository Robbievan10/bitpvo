<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLDealer.php");


class clspFLRouteDealer
 {
	public $route;
    public $dealer;
    public $assignDate;
    	
	public function __construct()
	 {
		$this->route= new clspFLRoute();
        $this->dealer= new clspFLDealer();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->route, $this->dealer, $this->assignDate);
	 }
 }

?>