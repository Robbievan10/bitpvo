<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProvider.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLOperationType.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLOperationStatus.php");


class clspFLProductEntry
 {
	public $idProductEntry;
    public $enterprise;
	public $provider;
	public $operationType;
	public $operationStatus;
	public $saleFolio;
	public $recordDate;
	public $productEntryDate;
		
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idProductEntry="000000-" . date("y");
		$this->enterprise= new clspFLEnterprise();
        $this->provider= new clspFLProvider();
		$this->operationType= new clspFLOperationType();
        $this->operationStatus= new clspFLOperationStatus();
        $this->recordDate= date("Y-m-d");
        $this->productEntryDate= date("Y-m-d");
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idProductEntry, $this->enterprise, $this->provider, $this->operationType, $this->operationStatus, $this->saleFolio,
              $this->recordDate, $this->productEntryDate);
	 }
 }

?>