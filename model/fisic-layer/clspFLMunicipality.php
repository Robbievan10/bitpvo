<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLState.php");


class clspFLMunicipality
 {
	public $idMunicipality;
	public $state;
	public $municipality;
	
	public function __construct()
	 {
		$this->idMunicipality=0;
		$this->state= new clspFLState();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	 
	public function __destruct()
	 {
		unset($this->idMunicipality, $this->state, $this->municipality);
	 }
 }

?>