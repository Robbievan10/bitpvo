<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLDealer.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLOperationStatus.php");


class clspFLLoan
 {
	public $idLoan;
    public $dealer;
    public $operationStatus;
    public $recordDate;
    public $amount;
    public $observation;
	
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idLoan=0;
		$this->dealer= new clspFLDealer();
        $this->operationStatus= new clspFLOperationStatus();
        $this->recordDate= date("Y-m-d");
        $this->amount=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idLoan, $this->dealer, $this->operationStatus, $this->recordDate, $this->amount, $this->observation);
	 }
 }

?>