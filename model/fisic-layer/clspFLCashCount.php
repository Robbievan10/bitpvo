<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterpriseUser.php");

class clspFLCashCount
 {
	public $idCashCount;
    public $cash;
    public $userAuditor;
    public $userCashier;
    public $recordDate;
    public $observation;

	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idCashCount=0;
		$this->cash= new clspFLCash();
        $this->userAuditor= new clspFLEnterpriseUser();
        $this->userCashier= new clspFLEnterpriseUser();
        $this->recordDate= date("Y-m-d");
        $this->observation = "";
	 }

	public function __get($vproperty)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }

	public function __destruct()
	 {
		unset($this->idCashCount, $this->cash, $this->userAuditor, $this->userCashier, $this->recordDate, $this->observation);
	 }
 }

?>
