<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");


class clspFLCash
 {
	public $idCash;
    public $enterprise;
	public $cash;
	
	public function __construct()
	 {
		$this->idCash=0;
        $this->enterprise= new clspFLEnterprise();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->idCash, $this->enterprise, $this->cash);
	 }
 }

?>