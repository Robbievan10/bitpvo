<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductType.php");


class clspFLProduct
 {
	public $idProduct;
    public $enterprise;
	public $productType;
    public $barCode;
    public $key;
	public $name;
    public $description;
    public $image;
    public $purchasePrice;
    public $salePrice;
    public $weightInKg;
    public $stock;
    public $stockInKg;
    public $observation;
    
	public function __construct()
	 {
		$this->idProduct=0;
		$this->key=0;
		$this->enterprise= new clspFLEnterprise();
		$this->productType= new clspFLProductType();
        $this->purchasePrice=0;
        $this->salePrice=0;
        $this->weightInKg=0;
        $this->stock=0;
        $this->stockInKg=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idProduct, $this->enterprise, $this->productType, $this->key, $this->barCode, $this->name,
              $this->description, $this->image, $this->purchasePrice, $this->salePrice, $this->weightInKg, $this->stock,
              $this->stockInKg, $this->observation);
	 }
 }

?>