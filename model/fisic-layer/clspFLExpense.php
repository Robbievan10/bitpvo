<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLExpenseType.php");


class clspFLExpense
 {
	public $idExpense;
    public $enterprise;
    public $expenseType;
    public $expense;
    public $description;
	
	public function __construct()
	 {
		$this->idExpense=0;
		$this->enterprise= new clspFLEnterprise();
        $this->expenseType= new clspFLExpenseType();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idExpense, $this->enterprise, $this->expenseType, $this->expense, $this->description);
	 }
 }

?>