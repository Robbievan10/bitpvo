<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");


class clspFLProductPrice
 {
	public $idProductPrice;
    public $product;
    public $recordDate;
    public $price;
    
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idProductPrice=0;
		$this->product= new clspFLProduct();
		$this->recordDate= date("Y-m-d");
        $this->price=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idProductPrice, $this->product, $this->recordDate, $this->price);
	 }
 }

?>