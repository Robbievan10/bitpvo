<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLRoute.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCustomer.php");


class clspFLRouteCustomer
 {
	public $route;
    public $customer;
    public $assignDate;
    public $sunday;
    public $monday;
    public $tuesday;
    public $wednesday;
    public $thursday;
    public $friday;
    public $saturday;
    
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->route= new clspFLRoute();
        $this->customer= new clspFLCustomer();
        $this->assignDate= date("Y-m-d");
        $this->sunday=0;
        $this->monday=0;
        $this->tuesday=0;
        $this->wednesday=0;
        $this->thursday=0;
        $this->friday=0;
        $this->saturday=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->route, $this->customer, $this->assignDate, $this->sunday, $this->monday, $this->tuesday, $this->wednesday, $this->thursday,
              $this->friday, $this->saturday);
	 }
 }

?>