<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLPayment.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductSale.php");

class clspFLProductSalePayment extends clspFLPayment
 {
	public $productSale;
    
	public function __construct()
	 {
		parent::__construct();
        $this->productSale= new clspFLProductSale();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		parent::__destruct();
		unset($this->productSale);
	 }
 }

?>