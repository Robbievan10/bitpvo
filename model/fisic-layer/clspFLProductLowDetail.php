<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductLow.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");


class clspFLProductLowDetail
 {
	public $productLow;
    public $product;
	public $lowAmount;
    public $lowWeightInKg;
    public $purchasePrice;
    public $salePrice;
   	
	public function __construct()
	 {
		$this->productLow= new clspFLProductLow();
        $this->product= new clspFLProduct();
        $this->lowAmount=0;
        $this->lowWeightInKg=0;
        $this->purchasePrice=0;
		$this->salePrice=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->productLow, $this->product, $this->lowAmount, $this->lowWeightInKg, $this->purchasePrice, $this->salePrice);
	 }
 }

?>