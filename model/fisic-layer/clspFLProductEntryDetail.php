<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductEntry.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");


class clspFLProductEntryDetail
 {
	public $productEntry;
    public $product;
	public $purchaseAmountInPt;
	public $purchaseAmountInKg;
    public $realAmountInPt;
    public $realAmountInKg;
    public $purchasePrice;
   	
	public function __construct()
	 {
		$this->productEntry= new clspFLProductEntry();
        $this->product= new clspFLProduct();
        $this->purchaseAmountInPt=0;
        $this->purchaseAmountInKg=0;
        $this->realAmountInPt=0;
        $this->realAmountInKg=0;
        $this->purchasePrice=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->productEntry, $this->product, $this->purchaseAmountInPt, $this->purchaseAmountInKg, $this->realAmountInPt,
              $this->realAmountInKg, $this->purchasePrice);
	 }
 }

?>