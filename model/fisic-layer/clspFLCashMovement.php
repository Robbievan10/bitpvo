<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashMovementStatus.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterpriseUser.php");

class clspFLCashMovement
 {
	public $idCashMovement;
    public $cash;
    public $cashMovementStatus;
    public $user;
    public $recordDate;
    public $openTime;
    public $openAmount;
    public $closeTime;
    public $closeAmount;
    public $CreatedOn;
	
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idCashMovement=0;
		$this->cash= new clspFLCash();
        $this->cashMovementStatus= new clspFLCashMovementStatus();
        $this->user= new clspFLEnterpriseUser();
        $this->recordDate= date("Y-m-d");
        $this->openTime= date("h:i:s a");
        $this->openAmount=0;
        $this->closeTime= date("h:i:s a");
        $this->closeAmount=0;
        $this->CreatedOn=null;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idCashMovement, $this->cash, $this->cashMovementStatus, $this->user, $this->recordDate, $this->openTime, $this->openAmount,
              $this->closeTime, $this->closeAmount,$this->CreatedOn);
	 }
 }

?>