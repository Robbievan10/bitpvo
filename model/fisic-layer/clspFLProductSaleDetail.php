<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductSale.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");


class clspFLProductSaleDetail
 {
	public $productSale;
    public $product;
	public $saleAmount;
    public $saleWeightInKg;
    public $purchasePrice;
    public $realPrice;
    public $salePrice;
    public $key;
   	public $CreatedOn;

	public function __construct()
	 {
		$this->productSale= new clspFLProductSale();
        $this->product= new clspFLProduct();
        $this->saleAmount=0;
        $this->saleWeightInKg=0;
        $this->purchasePrice=0;
		$this->realPrice=0;
		$this->salePrice=0;
		$this->key=0;
    $this->CreatedOn=null;
	 }

	public function __get($vproperty)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }

	public function __destruct()
	 {
		unset($this->productSale, $this->product, $this->saleAmount, $this->saleWeightInKg, $this->purchasePrice, $this->realPrice, $this->salePrice,$this->CreatedOn);
	 }
 }

?>
