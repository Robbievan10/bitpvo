<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductLowType.php");


class clspFLProductLow
 {
	public $idProductLow;
    public $enterprise;
	public $productLowType;
	public $recordDate;
	public $productLowDate;
    public $observation;
		
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idProductLow="000000-" . date("y");
		$this->enterprise= new clspFLEnterprise();
        $this->productLowType= new clspFLProductLowType();
        $this->recordDate= date("Y-m-d");
        $this->productLowDate= date("Y-m-d");
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idProductLow, $this->enterprise, $this->productLowType, $this->recordDate, $this->productLowDate, $this->observation);
	 }
 }

?>