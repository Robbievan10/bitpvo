<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLRouteDealer.php");


class clspFLProductDistribution
 {
	public $idProductDistribution;
	public $CreatedOn;
    public $enterprise;
	public $routeDealer;
	public $productDistributionRecordDate;
	public $productDistributionDate;
	public $productDistributionCuttingDate;
	public $creditTotal;
	public $discountTotal;
    public $devolutionTotal;
    public $observation;
    
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idProductDistribution="000000-" . date("y");
		$this->CreatedOn= '';
		$this->enterprise= new clspFLEnterprise();
        $this->routeDealer= new clspFLRouteDealer();
        $this->productDistributionRecordDate= date("Y-m-d");
        $this->productDistributionDate= date("Y-m-d");
		$this->productDistributionCuttingDate= date("Y-m-d");
		$this->creditTotal=0;
		$this->discountTotal=0;
        $this->devolutionTotal=0;
        $this->observation='';

	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idProductDistribution,$this->CreatedOn, $this->enterprise, $this->routeDealer, $this->productDistributionRecordDate,
			  $this->productDistributionDate, $this->productDistributionCuttingDate, $this->creditTotal, $this->discountTotal,
              $this->devolutionTotal, $this->observation);
	 }
 }

?>