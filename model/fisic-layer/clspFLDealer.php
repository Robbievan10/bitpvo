<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMunicipality.php");


class clspFLDealer
 {
	public $idDealer;
    public $enterprise;
	public $municipality;
    public $key;
	public $name;
    public $firstName;
    public $lastName;
	public $locality;
	public $street;
	public $number;
	public $postCode;
	public $phoneNumber;
    public $movilNumber;
	public $observation;
		
	public function __construct()
	 {
		$this->idDealer=0;
		$this->enterprise= new clspFLEnterprise();
		$this->municipality= new clspFLMunicipality();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idDealer, $this->enterprise, $this->municipality, $this->key, $this->name, $this->firstName, $this->lastName,
              $this->locality, $this->street, $this->number, $this->postCode, $this->phoneNumber, $this->movilNumber,
              $this->observation);
	 }
 }

?>