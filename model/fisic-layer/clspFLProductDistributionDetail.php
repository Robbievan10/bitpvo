<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProductDistribution.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLProduct.php");


class clspFLProductDistributionDetail
 {
	public $productDistribution;
    public $product;
	public $distributionAmount;
	public $distributionPrice;
    public $distributionWeightInkg;
    public $returnedAmount;
   	
	public function __construct()
	 {
		$this->productDistribution= new clspFLProductDistribution();
        $this->product= new clspFLProduct();
        $this->distributionAmount=0;
        $this->distributionPrice=0;
        $this->distributionWeightInkg=0;
        $this->returnedAmount=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->productDistribution, $this->product, $this->distributionAmount, $this->distributionPrice, $this->distributionWeightInkg,
              $this->returnedAmount);
	 }
 }

?>