<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLBank.php");


class clspFLBankDeposit
 {
	public $idBankDeposit;
    public $bank;
    public $accountNumber;
    public $amount;
    public $responsibleName;
    public $referenceName;
    public $recordDate;
    public $depositDate;
    public $observation;
	
	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idBankDeposit=0;
		$this->bank= new clspFLBank();
        $this->amount=0;
        $this->recordDate= date("Y-m-d");
        $this->depositDate= date("Y-m-d");
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idBankDeposit, $this->bank, $this->accountNumber, $this->amount, $this->responsibleName, $this->referenceName,
              $this->recordDate, $this->depositDate, $this->observation);
	 }
 }

?>