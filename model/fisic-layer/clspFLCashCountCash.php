<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashCount.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCashType.php");

class clspFLCashCountCash
 {
	public $idCashCountCash;
    public $cashCount;
    public $cashType;
    public $amount;
    public $value;
	
	public function __construct()
	 {
		$this->idCashCountCash=0;
		$this->cashCount= new clspFLCashCount();
        $this->cashType= new clspFLCashType();
        $this->amount=0;
        $this->value=0;
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		unset($this->idCashCountCash, $this->cashCount, $this->cashType, $this->amount, $this->value);
	 }
 }

?>