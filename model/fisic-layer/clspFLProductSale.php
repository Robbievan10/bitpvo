<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterprise.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCustomer.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLOperationType.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLOperationStatus.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCash.php");
require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLEnterpriseUser.php");


class clspFLProductSale
 {
	public $idProductSale;
    public $CreatedOn;
    public $enterprise;
	public $customer;
	public $operationType;
	public $operationStatus;
    public $cash;
    public $user;
    public $recordDate;
	public $productSaleDate;
    public $canceled;
    public $detalle;
    public $property;

	public function __construct()
	 {
        date_default_timezone_set('America/Mexico_City');
		$this->idProductSale="000000-" . date("y");
        $this->CreatedOn='';
		$this->enterprise= new clspFLEnterprise();
        $this->customer= new clspFLCustomer();
		$this->operationType= new clspFLOperationType();
        $this->operationStatus= new clspFLOperationStatus();
        $this->cash= new clspFLCash();
        $this->user= new clspFLEnterpriseUser();
        $this->recordDate= date("Y-m-d");
        $this->productSaleDate= date("Y-m-d");
        $this->canceled=0;
        $this->detalle ="";
        $this->property=array();
	 }

	public function __get($vproperty)
     {
        if( isset($vproperty) ){
            if (array_key_exists($vproperty, $this->property)) {
                return $this->property[$vproperty];
            }
            else{
                throw new Exception("Property doesn't exist: $vproperty");
            }
        }
        else{
            return $this->vproperty;
        }
     }

	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
            $this->property[$vproperty]=$vvalue;
        }
		else{
            $this->vproperty=$vvalue;
        }
	 }

	public function __destruct()
	 {
		unset($this->idProductSale,$this->CreatedOn, $this->enterprise, $this->customer, $this->operationType, $this->operationStatus, $this->cash, $this->user,
              $this->recordDate, $this->productSaleDate, $this->canceled,$this->detalle, $this->property,$this->CreatedOn);
	 }
 }

?>
