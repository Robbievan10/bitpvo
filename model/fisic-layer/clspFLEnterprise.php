<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLMunicipality.php");


class clspFLEnterprise
 {
	public $idEnterprise;
	public $municipality;
	public $enterprise;
	public $locality;
	public $street;
	public $number;
	public $phoneNumber;
    public $movilNumber;
    public $pageWeb;
    public $avatarImage;
    public $logoImage;
    	
	public function __construct()
	 {
		$this->idEnterprise=0;
        $this->municipality=new clspFLMunicipality();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			return $this->vproperty;
		}
	 }
	
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) ){
			throw new Exception("Property doesn't exist: $vproperty");
		}
		else{
			$this->vproperty=$vvalue;
		}
	 }
	
	public function __destruct()
	 {
		unset($this->idEnterprise, $this->municipality, $this->enterprise, $this->locality, $this->street, $this->number, 
              $this->phoneNumber, $this->movilNumber, $this->pageWeb, $this->avatarImage, $this->logoImage);
	 }   
 }

?>