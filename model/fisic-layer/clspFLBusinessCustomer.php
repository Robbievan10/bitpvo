<?php

require_once (dirname(dirname(__FILE__)) . "/fisic-layer/clspFLCustomer.php");


class clspFLBusinessCustomer extends clspFLCustomer
 {
	public $businessName;
    
	public function __construct()
	 {
		parent::__construct();
	 }
	
	public function __get($vproperty)
	 { 
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	return $this->vproperty;	}
	 }
	public function __set($vproperty, $vvalue)
	 {
		if( isset($vproperty) )
		 {	throw new Exception("Property doesn't exist: $vproperty");	}
		else
		 {	$this->vproperty=$vvalue;	}
	 }
	
	public function __destruct()
	 {
		parent::__destruct();
		unset($this->businessName);
	 }
 }

?>