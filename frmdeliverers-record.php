<?php
	require_once("controller/session.php");
	require_once("controller/deliverers-record.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Punto de Venta</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<!-- Bootstrap -->
		<link href="./tools/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        
		<!-- Endless -->
		<link href="./view/css/endless/font-awesome.min.css" rel="stylesheet">
		<link href="./view/css/endless/pace.css" rel="stylesheet">
		<link href="./view/css/endless/endless.min.css" rel="stylesheet">
		<link href="./view/css/endless/endless-skin.css" rel="stylesheet">
        
		<!-- Kendo UI -->
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.common.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.css" rel="stylesheet"/>
		<link href="./tools/kendoui-professional-2014.2.716/src/styles/web/kendo.blueopal.mobile.css" rel="stylesheet"/>            
		<!-- Personalizado -->
		<link href="./view/css/styles.css" rel="stylesheet" type="text/css" media="screen" />
        
	</head>
	<body class="overflow-hidden" onLoad="_default(<?php echo $_POST["vidDealer"] ?>);">
		<!-- Overlay Div -->
		<div id="overlay" class="transparent"></div>
		<div id="wrapper" class="preload">
			<div id="top-nav" class="skin-3 fixed">
				<div class="brand">
					<span>PVO</span>
					<span class="text-toggle"></span>
				</div><!-- /brand -->
				<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div>
					<?php getMenuHeader(); ?>
				</div>
			</div><!-- /top-nav-->
			<aside class="fixed skin-3">	
				<div class="sidebar-inner scrollable-sidebars">
					<?php getMenuSideBar(); ?>
				</div>
			</aside>
			<div id="main-container">
				<div id="breadcrumb">
					<ul class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="./" title="Ir a Inicio">&nbsp;Inicio</a></li>
						<li>Catálogos</li>
						<li><a href="./frmdeliverers.php" title="Ir a Catálogo de Repartidores">Repartidores</a></li>
						<li class="active"><span id="vpageTitle1">Registro - Repartidor</span></li>
					</ul>
				</div><!-- /breadcrumb-->
				<div class="padding-md">
					<div class="panel panel-default">
						<div class="panel-heading textCaption-1">
							<span id="vpageTitle2" class="text-info">
								Registro - Repartidor
							</span>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-1">&nbsp;</div>
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-2 form-group">
											<label for="txtkey">
												<span class="textCaption-2">Número:</span>
											</label>
											<input type="text" id="txtkey" name="txtkey" style="width:100%;" class="form-control input-sm" />
										</div>
										<div style="text-align:right;" class="col-md-10">&nbsp;</div>
									</div>
									<form id="vdealerData" name="vdealerData" method="post" onSubmit="return false;">
									<div class="panel panel-default">
										<div class="panel-heading textCaption-1"><span class="text-info">Datos Personales</span></div>
										<div class="panel-body">
											<div class="row">
												<div style="text-align:center" class="col-md-4 form-group">
													<label for="txtname">
														<span class="textCaption-2">Nombre:</span>
													</label>
													<input type="text" id="txtname" name="txtname" style="width:100%;" class="form-control input-sm" />
												</div>
												<div style="text-align:center" class="col-md-4 form-group">
													<label for="txtfirstName">
														<span class="textCaption-2">Apellido Paterno:</span>
													</label>
													<input type="text" id="txtfirstName" name="txtfirstName" style="width:100%;" class="form-control input-sm" />
												</div>
												<div style="text-align:center" class="col-md-4 form-group">
													<label for="txtlastName">
														<span class="textCaption-2">Apellido Materno:</span>
													</label>
													<input type="text" id="txtlastName" name="txtlastName" style="width:100%;" class="form-control input-sm" />
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading textCaption-1"><span class="text-info">Ubicación</span></div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-6 form-group">
													<label for="cmbstatesList">
														<span class="textCaption-2">Estado:</span>
													</label>
													<select id="cmbstatesList" name="cmbstatesList" style="width:86.5%" ></select>
												</div>
												<div style="text-align:right" class="col-md-6 form-group">
													<label for="txtlocality">
														<span class="textCaption-2">Localidad:</span>
													</label>
													<input type="text" id="txtlocality" name="txtlocality" style="width:75%;" class="form-control input-sm inline" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 form-group">
													<label for="cmbmunicipalitiesList">
														<span class="textCaption-2">Municipio:</span>
													</label>
													<div id="cmbmunicipalitiesListContainer" class="inline">
														<select id="cmbmunicipalitiesList" name="cmbmunicipalitiesList" style="width:82%" ></select>
													</div>
												</div>
												<div style="text-align:right" class="col-md-6 form-group">
													<label for="txtstreet">
														<span class="textCaption-2">Calle:</span>
													</label>
													<input type="text" id="txtstreet" name="txtstreet" style="width:75%;" class="form-control input-sm inline" />
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 form-group">
													<label for="txtnumber">
														<span class="textCaption-2">Número:</span>
													</label>
													<input type="text" id="txtnumber" name="txtnumber" style="width:30%;" class="form-control input-sm inline" />
												</div>
												<div style="text-align:right" class="col-md-6 form-group">
													<label for="txtpostCode">
														<span class="textCaption-2">Código Postal:</span>
													</label>
													<input type="text" id="txtpostCode" name="txtpostCode" style="width:75%;" class="form-control input-sm inline" />
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading textCaption-1"><span class="text-info">Contacto</span></div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-6 form-group">
													<label for="txtphoneNumber">
														<span class="textCaption-2">Número Telefónico:</span>
													</label>
													<input type="text" id="txtphoneNumber" name="txtphoneNumber" style="width:40%;" class="form-control input-sm inline" />
												</div>
												<div style="text-align:right" class="col-md-6">
													<label for="txtmovilNumber">
														<span class="textCaption-2">Número de Celular:</span>
													</label>
													<input type="text" id="txtmovilNumber" name="txtmovilNumber" style="width:40%;" class="form-control input-sm inline" />
												</div>
											</div>
										</div>
									</div>
									<div class="panel panel-default">
										<div class="panel-heading textCaption-1"><span class="text-info">Otros</span></div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<label for="txtobservation">
														<span class="textCaption-2">Observaciones:</span>
													</label>
													<textarea id="txtobservation" name="txtobservation" class="form-control" rows="2"></textarea>
												</div>
											</div>
										</div>
									</div>
									</form>
									<div class="row">
										<div class="col-md-6">
											<button type="button" id="cmdbackToDeliverers" class="k-button" title="Regresar a Catálogo de Repartidores">
												<span class="k-icon k-i-arrowhead-w"></span>&nbsp;Regresar
											</button>
										</div>
										<div style="text-align:right" class="col-md-6">
											<button type="button" id="cmdnewDealer" class="k-button" title="Nuevo Repartidor">
												<span class="k-icon k-i-plus"></span>&nbsp;Nuevo
											</button>
											<button type="button" id="cmdsaveDealerData" class="k-button" title="Guardar Datos">
												<span class="k-icon k-insert"></span>&nbsp;Guardar
											</button>
										</div>	
									</div>
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div><!-- /panel -->
				</div><!-- /.padding-md -->
			</div><!-- /main-container -->

		<!-- /wrapper -->
		<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
		<!-- Logout confirmation -->
		<div class="custom-popup width-100" id="logoutConfirm">
			<div class="padding-md">
				<h4 class="m-top-none">¿Desea salir del sistema?</h4>
			</div>

			<div class="text-center">
				<a class="btn btn-success m-right-sm" onClick="xajax_exit_();">Salir</a>
				<a class="btn btn-danger logoutConfirm_close">Cancelar</a>
			</div>
		</div>
		
		<!-- Bootstrap -->
		<script src="./tools/jquery/jquery-1.11.1.js"></script>
		<script src="./tools/bootstrap/js/bootstrap.min.js"></script>
        
		<!-- Endless -->
		<script src='./view/js/endless/modernizr.min.js'></script>
		<script src='./view/js/endless/pace.min.js'></script>
		<script src='./view/js/endless/jquery.popupoverlay.min.js'></script>
		<script src='./view/js/endless/jquery.slimscroll.min.js'></script>
		<script src='./view/js/endless/jquery_cookie.min.js'></script>
		<script src="./view/js/endless/endless/endless.js"></script>
        
		<!-- Kendo UI -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.core.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.data.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.popup.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.list.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.fx.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.userevents.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.draganddrop.js"></script>
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.mobile.scroller.js"></script>
		<!-- DropDownList -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.dropdownlist.js"></script>
		<!-- Button -->
		<script src="./tools/kendoui-professional-2014.2.716/src/js/kendo.button.js"></script>
        
		<!-- Personalizado -->
		<script type="text/javascript" src="./view/js/functions.js"></script>
		<script type="text/javascript" src="./view/js/menu.js"></script>
		<script type="text/javascript" src="./view/js/deliverers-record.js"></script>
        
		<?php $vxajax->printJavascript(); ?>
        
	</body>
</html>